# -*- encoding : utf-8 -*-

#~/.pgpass => hostname:port:database:username:password
#chmod 600 .pgpass
#pgpass = "#{host}:#{port}:#{database}:#{username}:#{password}"
#puts pgpass

#настройки
database = "youct_production"
username = "youct"
password = "YxV6QhKj"
host     = "youct.ru"
port     = 5432
pg_dump  = "/opt/local/lib/postgresql92/bin/pg_dump"
archive  = "./%s_%s.backup.bz2" % [database, Time.now.strftime('%Y-%m-%d')]
bzip     = "/usr/bin/bzip2"

cmd = "#{pg_dump} -Fc --host=#{host} --port=#{port} --compress=9 --encoding=utf-8 --username=#{username} #{database} | #{bzip} -9 > #{archive}"
system( cmd )
