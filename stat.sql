﻿select logs.sender_name_value, sl.phone, 
cog.name as "Оператор",
sl.created_at + interval '4 hours' as "sended", 
sl.updated_at + interval '4 hours' as "receive notification from sms center", 
regexp_replace(logs.content, E'[\\n\\r]+', ' ', 'g' ) as "content",

slds.name as "delivery_status", slde.name as "delivery_error_name", slde.code as "delivery_error_code",
sl.price, logs.sms_count, users.email, users.fio
 from send_logs sl, logs, send_log_delivery_statuses slds, send_log_delivery_errors slde, users, cell_operator_groups cog
where
sl.cell_operator_group_id = cog.id and
users.id = logs.user_id and
slds.id = sl.delivery_status_id and slde.id = sl.delivery_error_id and 
--logs.user_id = 41 and 
sl.log_id = 5787775 and
--(sl.created_at + interval '4 hours')::date >= '2013-12-01' and (sl.created_at + interval '4 hours')::date < '2014-01-01' and
sl.log_id = logs.id
order by sl.created_at


--select sl.* from send_logs sl, logs where user_id = 85