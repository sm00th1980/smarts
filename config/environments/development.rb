# -*- encoding : utf-8 -*-
Sms::Application.configure do
  # Settings specified here will take precedence over those in config/application.rb

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  config.eager_load = true

  # Show full error reports and disable caching
  config.consider_all_requests_local = true
  config.action_controller.perform_caching = false

  # Don't care if the mailer can't send
  config.action_mailer.raise_delivery_errors = false

  # Print deprecation notices to the Rails logger
  config.active_support.deprecation = :log

  # Only use best-standards-support built into browsers
  config.action_dispatch.best_standards_support = :builtin

  # Raise exception on mass assignment protection for Active Record models
  config.active_record.mass_assignment_sanitizer = :strict

  # Log the query plan for queries taking more than this (works
  # with SQLite, MySQL, and PostgreSQL)
  #config.active_record.auto_explain_threshold_in_seconds = 0.5

  # Do not compress assets
  config.assets.compress = false

  # Expands the lines which load the assets
  config.assets.debug = true

  config.log_level = :debug

  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = {
      address: 'smtp.gmail.com',
      port: 587,
      domain: 'gmail.com',
      authentication: 'plain',
      user_name: 'deyarov@gmail.com',
      password: 'post@nuclear',
      enable_starttls_auto: true
  }

  config.action_mailer.default_url_options = {:host => 'localhost:3000'}

  config.robokassa = {
      login: 'sm00th1980',
      secret: 'Criminal1980_1',
      secret2: 'Criminal1980_2'
  }

  #кому отсылать сообщения
  config.mail_sender = 'support@sms-info.smarts.ru'
  config.admin_emails = ['sm00th1980@mail.ru']

  config.kannel = {
      gold: {
          host: 'localhost:3000/kannel_send',
          user: 'gold',
          password: 's23dw2eI6x4C5f3',
          smsc: 'gold',
          callback_url: 'localhost:80/kannel/delivery_report'
      },
      bronze: {
          host: 'localhost:3000/kannel_send',
          user: 'bronze',
          password: 's23dw2eI6x4C5f3',
          smsc: 'bronze',
          callback_url: 'localhost:80/kannel/delivery_report'
      },
      megafon_fixed: {
          host: 'localhost:3000/kannel_send',
          user: 'megafon_fixed',
          password: 's23dw2eI6x4C5f3',
          smsc: 'megafon_fixed',
          callback_url: 'localhost:80/kannel/delivery_report'
      },
      megafon_multi: {
          host: 'localhost:3000/kannel_send',
          user: 'megafon_multi',
          password: 's23dw2eI6x4C5f3',
          smsc: 'megafon_multi',
          callback_url: 'localhost:80/kannel/delivery_report'
      }
  }

  config.site_name = 'sms-info.smarts.ru'

  config.rabbitmq = {
      hostname: '127.0.0.1',
      port: '5672',
      login: 'admin',
      password: 'admin',
      api_port: '15672'
  }
end
