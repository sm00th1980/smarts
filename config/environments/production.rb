# -*- encoding : utf-8 -*-
Sms::Application.configure do
  # Settings specified here will take precedence over those in config/application.rb

  # Code is not reloaded between requests
  config.cache_classes = true

  config.eager_load = true

  # Full error reports are disabled and caching is turned on
  config.consider_all_requests_local = false
  config.action_controller.perform_caching = true

  # Disable Rails's static asset server (Apache or nginx will already do this)
  config.serve_static_files = false

  # Compress JavaScripts and CSS
  config.assets.compress = false

  # Don't fallback to assets pipeline if a precompiled asset is missed
  config.assets.compile = true

  config.assets.initialize_on_precompile = false

  # Generate digests for assets URLs
  config.assets.digest = true

  config.log_level = :info

  # Defaults to nil and saved in location specified by config.assets.prefix
  # config.assets.manifest = YOUR_PATH

  # Specifies the header that your server uses for sending files
  # config.action_dispatch.x_sendfile_header = "X-Sendfile" # for apache
  # config.action_dispatch.x_sendfile_header = 'X-Accel-Redirect' # for nginx

  # Force all access to the app over SSL, use Strict-Transport-Security, and use secure cookies.
  # config.force_ssl = true

  # See everything in the log (default is :info)
  # config.log_level = :debug

  # Prepend all log lines with the following tags
  # config.log_tags = [ :subdomain, :uuid ]

  # Use a different logger for distributed setups
  # config.logger = ActiveSupport::TaggedLogging.new(SyslogLogger.new)

  # Use a different cache store in production
  # config.cache_store = :mem_cache_store

  # Enable serving of images, stylesheets, and JavaScripts from an asset server
  # config.action_controller.asset_host = "http://assets.example.com"

  # Precompile additional assets (application.js, application.css, and all non-JS/CSS are already added)
  # config.assets.precompile += %w( search.js )

  # Disable delivery errors, bad email addresses will be ignored
  # config.action_mailer.raise_delivery_errors = false

  # Enable threaded mode
  # config.threadsafe!

  # Enable locale fallbacks for I18n (makes lookups for any locale fall back to
  # the I18n.default_locale when a translation can not be found)
  config.i18n.fallbacks = true

  # Send deprecation notices to registered listeners
  config.active_support.deprecation = :notify

  # Log the query plan for queries taking more than this (works
  # with SQLite, MySQL, and PostgreSQL)
  # config.active_record.auto_explain_threshold_in_seconds = 0.5
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = {
      address: 'smtp.gmail.com',
      port: 587,
      domain: 'gmail.com',
      authentication: 'plain',
      user_name: 'deyarov@gmail.com',
      password: 'post@nuclear',
      enable_starttls_auto: true
  }

  config.action_mailer.default_url_options = {:host => 'localhost:3000'}

  config.middleware.use ExceptionNotification::Rack,
                        :email => {
                            :email_prefix => "[sms-info.smarts.ru] ",
                            :sender_address => %{"sms-info.smarts.ru notifier" <support@sms-info.smarts.ru>},
                            :exception_recipients => %w{sm00th1980@mail.ru kutuzov.i.a@gmail.com}
                        }

  config.robokassa = {
      login: 'sm00th1980',
      secret: 'Criminal1980_1',
      secret2: 'Criminal1980_2'
  }

  #кому отсылать сообщения
  config.mail_sender = 'support@sms-info.smarts.ru'
  config.admin_emails = ['sm00th1980@mail.ru', 'kutuzov.i.a@gmail.com', 'richurkina@lotus.samara-gsm.ru']

  config.kannel = {
      gold: {
          host: 'localhost:13003/cgi-bin/sendsms',
          user: 'gold',
          password: 's23dw2eI6x4C5f3',
          smsc: 'gold',
          callback_url: 'localhost:80/kannel/delivery_report'
      },
      bronze: {
          host: 'localhost:13003/cgi-bin/sendsms',
          user: 'bronze',
          password: 's23dw2eI6x4C5f3',
          smsc: 'bronze',
          callback_url: 'localhost:80/kannel/delivery_report'
      },
      megafon_fixed: {
          host: 'localhost:13003/kannel_send',
          user: 'megafon_fixed',
          password: 's23dw2eI6x4C5f3',
          smsc: 'megafon_fixed',
          callback_url: 'localhost:80/kannel/delivery_report'
      },
      megafon_multi: {
          host: 'localhost:13003/kannel_send',
          user: 'megafon_multi',
          password: 's23dw2eI6x4C5f3',
          smsc: 'megafon_multi',
          callback_url: 'localhost:80/kannel/delivery_report'
      }
  }

  config.site_name = 'sms-info.smarts.ru'

  config.rabbitmq = {
      hostname: '127.0.0.1',
      port: '5672',
      login: 'guest',
      password: '364m6reE',
      api_port: '15672'
  }
end
