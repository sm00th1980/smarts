# -*- encoding : utf-8 -*-
require 'rvm/capistrano'
require "whenever/capistrano"
require 'capistrano-unicorn'

set :rvm_type, :user

server_name='sms-info.smarts.ru' # Имя приложения

# Создаем переменные и присваиваем им значения
set :user, 'sm00th' # пользователь удалённого сервера
set :password, 'post@nuclear'
set :port, 222

#ssh_options[:forward_agent] = true
default_run_options[:pty] = true
ssh_options[:keys] = %w('~/.ssh/id_rsa')

set :repository, 'https://sm00th1980@bitbucket.org/sm00th1980/smarts.git'
set :repository_cache, "git_cache"
#set :ssh_options, { :forward_agent => true }

set :scm, "git"

# Set the deploy branch to the current branch
set :branch, 'smarts'

set :deploy_via, :checkout

set :server, server_name
set :application, 'sms-info.smarts.ru'
set :applicationdir, '/srv/www/sms-info.smarts.ru' # Директория приложения на удалённом сервере
set :use_sudo, false # не запускать команды под sudo

#хост удалённого сервера
server "144.76.222.27", :app, :web, :db, :task_server, :primary => true

# Директория, куда будет делаться checkout из репозитория
set :deploy_to, "#{applicationdir}"

task :after_restart, :roles => :app do
  run "touch #{File.join(current_path, 'tmp', 'restart.txt')}"
end

task :after_update_code, :roles => [:web, :db, :app] do
  run "cd #{applicationdir}/current && bundle"
  run "chmod 755 #{current_path}/public -R"
  run "chown -R nobody #{current_path}/tmp"
  run "(echo '0a'; echo \"ENV['RAILS_ENV'] ||= 'production'\"; echo '.'; echo 'wq') | ed -s #{current_path}/config/environment.rb"
end

after "deploy:finalize_update", "deploy:bundle"
after "deploy:bundle",          "deploy:compile_assets"
after "deploy:compile_assets",  "deploy:migrate_db"
after "deploy:migrate_db",      "deploy:restart_app"
after 'deploy:restart',         'unicorn:reload' # app IS NOT preloaded
after 'deploy:restart',         'unicorn:restart'  # app preloaded

namespace :deploy do
  desc "Installing libraries"
  task :bundle do
    run "cd #{release_path} && bundle install"
  end

  desc "Compiling assets"
  task :compile_assets do
    run "cd #{release_path} && bundle exec rake assets:precompile"
  end

  desc "Restarting application"
  task :restart_app do
    run "cd #{release_path} && touch tmp/restart.txt"
  end

  desc "Migrate database"
  task :migrate_db do
    run "cd #{release_path} && rake db:migrate RAILS_ENV=production"
  end
end
