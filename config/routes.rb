# -*- encoding : utf-8 -*-
Sms::Application.routes.draw do

  #ADMIN ZONE
  scope :admin do
    #/admin/users
    get 'users' => 'admin/users#index'
    resource :user, only: [] do
      get 'new' => 'admin/user#new', as: :new
      post 'new' => 'admin/user#create', as: :create

      get ':id' => 'admin/user#show', as: :show
      post ':id' => 'admin/user#update', as: :update
    end
    #/admin/users

    #/admin/tariffs
    resource :tariffs, only: [] do
      get ':id' => 'admin/tariffs#index', as: :index
      post ':id/create' => 'admin/tariffs#create', as: :create
      get ':id/new' => 'admin/tariffs#new', as: :new
      delete ':id/delete' => 'admin/tariffs#destroy', as: :delete
    end
    #/admin/tariffs

    #/admin/payments
    resource :payments, only: [] do
      get ':id' => 'admin/payments#index', as: :index
      post ':id/create' => 'admin/payments#create', as: :create
      get ':id/new' => 'admin/payments#new', as: :new
      delete ':id/delete' => 'admin/payments#destroy', as: :delete

      post ':id/approve' => 'admin/payments#approve', as: :approve
      post ':id/refuse' => 'admin/payments#refuse', as: :refuse
    end
    #/admin/payments

  end

  namespace :admin do

    #/admin/report
    namespace :report do
      match 'charges', via: [:get, :post]
      match 'payments', via: [:get, :post]
    end
    get 'report/charges/xls' => 'report#charges_xls'
    #/admin/report

    #/admin/operators
    get 'operators' => 'operators#index', as: :operators_index
    #/admin/operators

    #/admin/operator_tariffs
    get 'operator/:operator_id/tariffs' => 'operator_tariff#index', as: :operator_tariffs
    get 'operator/:operator_id/tariff/new' => 'operator_tariff#new', as: :operator_tariff_new
    post 'operator/:operator_id/tariff/create' => 'operator_tariff#create', as: :operator_tariff_create

    post 'operator/tariff/:operator_tariff_id/update' => 'operator_tariff#update', as: :operator_tariff_update
    get 'operator/tariff/:operator_tariff_id/edit' => 'operator_tariff#edit', as: :operator_tariff_edit
    delete 'operator/tariff/:operator_tariff_id' => 'operator_tariff#destroy', as: :operator_tariff_destroy
    #/admin/operator_tariffs

    #/admin/sender_names
    get 'sender_names' => 'sender_names#index'
    get 'sender_names/:id/accept' => 'sender_names#accept', as: :sender_name_accept
    get 'sender_names/:id/reject' => 'sender_names#reject', as: :sender_name_reject
    #/admin/sender_names

    #/admin/black_lists
    get 'black_lists' => 'black_lists#index'
    get 'black_lists/new'
    post 'black_lists' => 'black_lists#create', as: :black_lists_create
    delete 'black_lists/:id' => 'black_lists#destroy', as: :black_lists_delete
    #/admin/black_lists
  end
  #ADMIN ZONE

  #USER ZONE
  #/api
  namespace :api do
    get 'send', action: :send_sms
    get 'report', action: :report_sms
  end
  #/api

  #/api/faq
  get 'api/faq' => 'api_faq#index'
  #/api/faq

  #/sms
  get 'sms' => 'sms#index'
  #/sms

  #/send_sms
  match 'send_sms' => 'sms#send_sms', via: [:get, :post]
  #/send_sms

  #/message/sms_count
  match 'message/sms_count' => 'message#sms_count', via: :all
  #/message/sms_count

  #/message_price/calc
  match 'message_price/calc' => 'message_price#calc', via: :all
  #/message_price/calc

  #/autodelivery
  resource :autodelivery, only: [] do
    get '/' => 'autodelivery#index'
    get 'new' => 'autodelivery#new', as: :new
    get ':id' => 'autodelivery#show', as: :show
    post '/' => 'autodelivery#create', as: :create
    delete ':id' => 'autodelivery#destroy', as: :delete
    post ':id' => 'autodelivery#update', as: :update

    member do
      get ':id/activate' => 'autodelivery#activate', as: :activate
      get ':id/deactivate' => 'autodelivery#deactivate', as: :deactivate
      get ':id/run' => 'autodelivery#run', as: :run
    end
  end
  #/autodelivery

  #/groups
  get 'groups' => 'groups#index'
  #/groups

  #/group
  resource :group, only: [] do
    get 'new' => 'group#new', as: :new
    get ':id' => 'group#show', as: :show
    post '/' => 'group#create', as: :create
    delete ':id' => 'group#destroy', as: :delete
    post ':id' => 'group#update', as: :update

    member do
      get ':id/upload' => 'group#show_upload', as: :show_upload
      post ':id/upload' => 'group#upload', as: :upload
      post ':id/birthday_congratulation' => 'group#birthday_congratulation', as: :birthday_congratulation
    end
  end
  #/group

  #/client
  resource :client, only: [] do
    get 'new' => 'client#new', as: :new
    get ':id' => 'client#show', as: :show
    post '/' => 'client#create', as: :create
    delete ':id' => 'client#destroy', as: :delete
    post ':id' => 'client#update', as: :update

    member do
      post ':id/birthday_congratulation' => 'client#birthday_congratulation_update', as: :birthday_congratulation
    end
  end
  #/client

  #/sender_names
  get 'sender_names' => 'sender_names#index'

  resource :sender_name, only: [] do
    get 'new' => 'sender_name#new', as: :new
    post '/' => 'sender_name#create', as: :create
    delete ':id' => 'sender_name#destroy', as: :delete

    member do
      get ':id/use' => 'sender_name#use', as: :use
    end
  end
  #/sender_names

  #/change_password
  get 'change_password' => 'password#new', as: :new_password
  post 'change_password' => 'password#update', as: :update_password
  #/change_password

  #/login, /logout
  devise_for :users, :path => "/", :path_names => {:sign_in => 'login', :sign_out => 'logout'}, :controllers => {:sessions => "sessions"}
  #/login, /logout

  #/tariffs
  get 'tariffs' => 'tariffs#index'
  #/tariffs

  #/contact
  get 'contact' => 'contact#show', as: :show_contact
  post 'contact' => 'contact#update', as: :update_contact
  #/contact

  #/night_delivery
  get 'night_delivery' => 'night_delivery#show', as: :show_night_delivery
  post 'night_delivery' => 'night_delivery#update', as: :update_night_delivery
  #/night_delivery

  #/templates
  get 'templates' => 'templates#index'

  resource :template, only: [] do
    get 'new' => 'template#new', as: :new
    post '/' => 'template#create', as: :create
    delete ':id' => 'template#destroy', as: :delete
    get ':id' => 'template#show', as: :show
    post ':id' => 'template#update', as: :update
  end
  #/templates

  #/template_birthday
  resource :template_birthday, only: [] do
    get '/' => 'template_birthday#show', as: :show
    post '/' => 'template_birthday#update', as: :update
  end
  #/template_birthday

  #/blacklist
  resource :blacklist, only: [] do
    get 'new' => 'blacklist#new', as: :new
    post '/' => 'blacklist#create', as: :create
    delete ':id' => 'blacklist#destroy', as: :delete
    get '/' => 'blacklist#show', as: :show
  end
  #/blacklist

  #/report_by_date
  get 'report_by_date' => 'report_by_date#show', as: :show_report_by_date
  post 'report_by_date' => 'report_by_date#generate', as: :generate_report_by_date
  #/report_by_date

  #/logs
  get 'logs' => 'logs#index'

  resource :log, only: [] do
    member do
      get ':id' => 'log#show', as: :show
      get ':id/stop' => 'log#stop', as: :stop
      get ':id/report' => 'log#report', as: :report
      get ':id/delete_clients' => 'log#delete_clients', as: :delete_clients
      get ':id/new_delivery' => 'log#new_delivery', as: :new_delivery
    end
  end
  #/logs

  #root
  root to: 'welcome#index'
  #root

  #USER ZONE
end
