# -*- encoding : utf-8 -*-
SwitchUser.setup do |config|
  config.provider = :devise
  config.controller_guard = lambda { |current_user, request| current_user.admin? }
  config.redirect_path = lambda { |request, params| '/' }
end
