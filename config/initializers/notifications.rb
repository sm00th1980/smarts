# -*- encoding : utf-8 -*-
ActiveSupport::Notifications.subscribe 'sms.send' do |name, start, finish, id, _params|
  log = _params[:log]
  SMSService.delay.process_fresh(log)
end

ActiveSupport::Notifications.subscribe 'report.sended' do |name, start, finish, id, params|
  path = params[:path]
  FileUtils.rm(path) if File.exists?(path)
end

ActiveSupport::Notifications.subscribe 'group.recalculate.required' do |name, start, finish, id, _params|
  Calculator::Groups.drop_prices(_params[:group_ids])
end
