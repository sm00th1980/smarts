# -*- encoding : utf-8 -*-
require 'sneakers'

Sneakers.configure workers: 3,
                   amqp: "amqp://%s:%s@%s:%s" % [Rails.configuration.rabbitmq[:login], Rails.configuration.rabbitmq[:password], Rails.configuration.rabbitmq[:hostname], Rails.configuration.rabbitmq[:port]],
                   daemonize: true,
                   log: "%s/log/sneakers.log" % [Rails.root.to_s],
                   pid_path: "%s/tmp/pids/sneakers.pid" % [Rails.root.to_s],
                   prefetch: 1,
                   threads: 1,
                   timeout_job_after: 3600
