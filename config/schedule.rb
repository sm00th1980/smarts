# -*- encoding : utf-8 -*-
every 5.minute do
  runner "AutoDelivery.all.map{|delivery| delivery.run}"
end

every 1.hour do
  runner "BirthdayCongratulation.run"
end

every 1.minute do
  4.times do |index|
    key = "kannel_#{index+1}"
    runner "Notification::Collector.new('#{key}').perform"
  end
end

#каждую ночь в 3:00 чистим log_valid_phones
every '0 3 * * *' do
  runner "LogValidPhones.clean"
end

#каждую минуту подаём команду на перерасчёт логов за текущий день
every 1.minute do
  runner "Calculator::Logs.produce_recalculate"
end

#каждую минуту подаём команду на перерасчёт стоимости групп
every 1.minute do
  runner "Calculator::Groups.produce_recalculate"
end


#каждую ночь в 5:00 пересчитываем логи за предыдущий день
every '0 5 * * *' do
  runner "Log.recalculate(Date.today - 1.day, Date.today - 1.day)"
end

#каждое первой число в 8:00 рассчитываем балансы и логи за предыдущий период
every '0 8 1 * *' do
  runner "Log.recalculate(Date.today - 1.month, Date.today - 1.month)"
end

#каждое первой число в 00:01 создаём LogStat для следующего месяца
every '1 0 1 * *' do
  runner "Log.recalculate(Date.today + 1.month, Date.today + 1.month)"
end
