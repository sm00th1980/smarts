# -*- encoding : utf-8 -*-

def filter(line)
	line.split(' ')[1..-1].reject{|el| el == 'где'}
end

def has_n?(line)
	filter(line)[2][0] == 'N'
end

def extract_n(line)
	if has_n?(line)
		filter(line)[2].split('=').last
	end
end

def extract_region(line)
	if has_n?(line)
		filter(line)[3..-1].join(' ')
	else
		filter(line)[2..-1].join(' ')
	end
end

def extract_phone(line)
	[filter(line)[0], filter(line)[1]].join
end

def start_phone(phone)
	phone_ = phone.
		sub('X1', '0').sub('X2', '0').sub('X3', '0').sub('X4', '0').sub('X5', '0').sub('X6', '0').sub('X7', '0').sub('X8', '0').
		sub('Х1', '0').sub('Х2', '0').sub('Х3', '0').sub('Х4', '0').sub('Х5', '0').sub('Х6', '0').sub('Х7', '0').sub('Х8', '0')

	"7#{phone_}"
end

def end_phone(phone)
	phone_ = phone.
		sub('X1', '9').sub('X2', '9').sub('X3', '9').sub('X4', '9').sub('X5', '9').sub('X6', '9').sub('X7', '9').sub('X8', '9').
		sub('Х1', '9').sub('Х2', '9').sub('Х3', '9').sub('Х4', '9').sub('Х5', '9').sub('Х6', '9').sub('Х7', '9').sub('Х8', '9')

	"7#{phone_}"
end

def n_to_array(n)
	if n.split('').include? '-'	
		Range.new(n.split('-').first, n.split('-').last).to_a
	else
		n.split(',')
	end
end

def phones(line)
	str = extract_phone(line)

	phones_ = []
	n_to_array(extract_n(line)).each do |n|
		phones_ << str.sub('N', n)
	end

	phones_.map{|p| [start_phone(p), end_phone(p)]}
end

def print_phone(start_phone, end_phone, region)
	puts "MtsRange.create!(begin_phone: #{start_phone}, end_phone: #{end_phone}, region: '#{region}')"
end

File.open("mts_source.txt") do |file|
    file.each do |line|
    	if not has_n?(line)
 			print_phone(start_phone(extract_phone(line)), end_phone(extract_phone(line)), extract_region(line))
 		else
 			#puts "#{filter(line).inspect} => #{extract_phone(line)}, #{extract_n(line)}, #{phones(line)}, #{extract_region(line)}"	
 			phones(line).each do |phone|
 			 	print_phone(phone.first, phone.last, extract_region(line)) 
 			end
 		end
	end
end
