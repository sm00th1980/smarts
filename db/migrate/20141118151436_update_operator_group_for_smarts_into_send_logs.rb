# -*- encoding : utf-8 -*-
class UpdateOperatorGroupForSmartsIntoSendLogs < ActiveRecord::Migration
  def up
    Log.where('date(created_at) >= ?', Date.parse('2014-11-01')).find_each do |log|
      log.send_logs.where(cell_operator_group_id: CellOperatorGroup.other.id, channel_id: [Channel.gold, Channel.megafon]).find_each do |send_log|
        new_cell_operator_group = CellOperatorGroup.get_by_phone(send_log.phone)

        if new_cell_operator_group.id != send_log.cell_operator_group_id
          send_log.cell_operator_group = new_cell_operator_group
          send_log.save!
        end
      end

      log.send_logs.where(channel_id: Channel.bronze).find_each do |send_log|
        new_cell_operator_group = CellOperatorGroup.get_by_phone(send_log.phone)

        if new_cell_operator_group.id != send_log.cell_operator_group_id
          send_log.cell_operator_group = new_cell_operator_group
          send_log.save!
        end
      end
    end
  end

  def down
  end
end
