# -*- encoding : utf-8 -*-
class AddPrevLogIdIntoLogs < ActiveRecord::Migration
  def change
    add_column :logs, :prev_log_id, :integer, null: true
  end
end
