# -*- encoding : utf-8 -*-
class FillStatusIdIntoUsers < ActiveRecord::Migration
  def up

    User.where.not(blocked_at: nil).find_each do |user|
      user.status = UserStatus.blocked
      user.save!
    end

    User.where.not(deleted_at: nil).find_each do |user|
      user.status = UserStatus.deleted
      user.save!
    end

    User.where(deleted_at: nil, blocked_at: nil).find_each do |user|
      user.status = UserStatus.active
      user.save!
    end

    change_column :users, :status_id, :integer, null: false
  end
end
