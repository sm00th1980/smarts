# -*- encoding : utf-8 -*-
class AddBlockedAtIntoUsers < ActiveRecord::Migration
  def change
    add_column :users, :blocked_at, :timestamp
  end
end
