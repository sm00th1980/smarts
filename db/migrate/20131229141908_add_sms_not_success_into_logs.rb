# -*- encoding : utf-8 -*-
class AddSmsNotSuccessIntoLogs < ActiveRecord::Migration
  def change
    add_column :logs, :sms_sent_failure, :integer, default: 0
  end
end
