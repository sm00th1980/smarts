# -*- encoding : utf-8 -*-
class AddFakeIntoLogs < ActiveRecord::Migration
  def change
    add_column :logs, :fake, :boolean, :default => false
  end
end
