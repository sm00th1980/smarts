# -*- encoding : utf-8 -*-
class PermitForAllNightDelivery < ActiveRecord::Migration
  def up
    User.all.each do |user|
      user.update_attribute(:permit_night_delivery, true)
    end
  end

  def down
  end
end
