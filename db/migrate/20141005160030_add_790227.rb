# -*- encoding : utf-8 -*-
class Add790227 < ActiveRecord::Migration
  def up
    ural = CellOperator.find_by_name('УралСвязьИнформ')
    CellOperatorPhoneRange.create!(begin_phone: 79022700000, end_phone: 79022799999, cell_operator: ural, region: 'Свердловская область')
  end

  def down
  end
end
