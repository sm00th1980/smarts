# -*- encoding : utf-8 -*-
class ChangeTariffForExistsUsers < ActiveRecord::Migration
  def up
    new_tariff = Tariff.find_by_name('Тариф 1')
    UserTariff.all.each do |user_tariff|
      user_tariff.tariff = new_tariff
      user_tariff.save!
    end
  end

  def down
  end
end
