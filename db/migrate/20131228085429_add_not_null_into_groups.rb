# -*- encoding : utf-8 -*-
class AddNotNullIntoGroups < ActiveRecord::Migration
  def up
    change_column :groups, :user_id, :integer, null: false
    change_column :groups, :valid_phones_count, :integer, null: false, default: 0
  end

  def down
    change_column :groups, :user_id, :integer, null: true
    change_column :groups, :valid_phones_count, :integer, null: false
  end
end
