# -*- encoding : utf-8 -*-
class RenameSenderNameIntoLogs < ActiveRecord::Migration
  def change
    rename_column :logs, :sender_name, :sender_name_value
  end
end
