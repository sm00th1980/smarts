# -*- encoding : utf-8 -*-
class CreateUserTypes < ActiveRecord::Migration
  def change
    create_table :user_types do |t|
      t.text :name
      t.text :internal_name

      t.timestamps
    end
  end
end
