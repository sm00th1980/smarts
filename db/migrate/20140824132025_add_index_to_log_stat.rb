# -*- encoding : utf-8 -*-
class AddIndexToLogStat < ActiveRecord::Migration
  def change
    add_index :log_stats, [:user_id, :date, :cell_operator_group_id], unique: true
  end
end
