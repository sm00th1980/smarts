# -*- encoding : utf-8 -*-
class AddUserIdIntGroups < ActiveRecord::Migration
  def up
    add_column :groups, :user_id, :integer
    remove_column :clients, :user_id, :integer
  end

  def down
    remove_column :groups, :user_id, :integer
    add_column :clients, :user_id, :integer
  end
end
