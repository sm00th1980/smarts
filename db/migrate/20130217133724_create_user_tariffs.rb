# -*- encoding : utf-8 -*-
class CreateUserTariffs < ActiveRecord::Migration
  def change
    create_table :user_tariffs do |t|
      t.integer :user_id
      t.integer :tariff_id
      t.date    :begin_date
      t.date    :end_date

      t.timestamps
    end
  end
end
