# -*- encoding : utf-8 -*-
class AddPermitDefSenderNameIntoUsers < ActiveRecord::Migration
  def change
    add_column :users, :permit_def_sender_name, :boolean, default: true, null: false
  end
end
