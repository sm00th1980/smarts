# -*- encoding : utf-8 -*-
class AddGoldErrors < ActiveRecord::Migration
  def up
    remove_index :send_log_delivery_errors, [:code]

    SendLogDeliveryError.create!(eng_name: 'RETRIES_EXCEEDED',
                                 name: 'Превышено число попыток отправки. Скорость smpp линка недостаточна(throttling_error_58)',
                                 code: -5,
                                 channel: Channel.gold)

    SendLogDeliveryError.create!(eng_name: 'LOW_BALANCE',
                                 name: 'Сообщение не отослано. Текущий баланс слишком низкий',
                                 code: -4,
                                 channel: Channel.gold)

    SendLogDeliveryError.create!(eng_name: 'ERROR_DELIVERY_TO_KANNEL',
                                 name: 'Ошибка доставки через сервис kannel',
                                 code: -2,
                                 channel: Channel.gold)

    SendLogDeliveryError.create!(eng_name: 'ERROR_UNKNOWN',
                                 name: 'Сообщение отослано. Статус доставки неизвестен. Не пришло уведомление от СМС центра о доставке.',
                                 code: -1,
                                 channel: Channel.gold)

    SendLogDeliveryError.create!(eng_name: 'SUCCESS_DELIVERY',
                                 name: 'Сообщение доставлено без ошибок',
                                 code: 0,
                                 channel: Channel.gold)

    SendLogDeliveryError.create!(eng_name: 'ERROR_UNKNOWN_OR_NO_DELIVERY_REPORT',
                                 name: 'В процессе доставки сообщения произошла неизвестная платформе ошибка, либо оператор не предоставил ошибку в отчете о доставке',
                                 code: 1,
                                 channel: Channel.gold)

    SendLogDeliveryError.create!(eng_name: 'ERROR_PHONE_OUT_OF_NETWORK',
                                 name: 'Абонент находился вне доступа сети на протяжении всего времени попыток доставки SMS сообщения',
                                 code: 2,
                                 channel: Channel.gold)

    SendLogDeliveryError.create!(eng_name: 'ERROR_PHONE_IS_BLOCKED_OR_ROAMING',
                                 name: 'Абонент заблокирован, либо абонент поставил блокировку на прием SMS, либо абонент находится в роуминге, при том, что у него не подключена услуга приема SMS в роуминге',
                                 code: 3,
                                 channel: Channel.gold)

    SendLogDeliveryError.create!(eng_name: 'ERROR_GSM_TRANSPORT_LEVEL_IS_CORRUPTED',
                                 name: 'В процессе доставки SMS-сообщения произошла ошибка на транспортном уровне сигнальной сети',
                                 code: 4,
                                 channel: Channel.gold)

    SendLogDeliveryError.create!(eng_name: 'ERROR_PHONE_OUT_OF_MEMORY',
                                 name: 'Память телефона абонента переполнена',
                                 code: 5,
                                 channel: Channel.gold)

    SendLogDeliveryError.create!(eng_name: 'ERROR_INCOMPATIBLE_TERMINAL',
                                 name: 'У абонента не подключена услуга приема SMS-сообщений',
                                 code: 6,
                                 channel: Channel.gold)

    SendLogDeliveryError.create!(eng_name: 'ERROR_GSM_SWITCH_IS_NOT_AVAILABLE',
                                 name: 'Коммутационное оборудование, на котором зарегистрирован абонент, не отвечает',
                                 code: 7,
                                 channel: Channel.gold)

    SendLogDeliveryError.create!(eng_name: 'ERROR_PHONE_NUMBER_IS_NOT_VALID',
                                 name: 'Некорректный номер абонента, либо телефон абонента был выключен на протяжении очень долгого периода времени',
                                 code: 8,
                                 channel: Channel.gold)

    SendLogDeliveryError.create!(eng_name: 'ERROR_MESSAGE_WAS_REJECTED_BY_DUBLICATING',
                                 name: 'Сообщение было отброшено платформой, так как сработал механизмом отсечения дубликатов SMS-сообщений',
                                 code: 9,
                                 channel: Channel.gold)

    SendLogDeliveryError.create!(eng_name: 'ERROR_MESSAGE_WAS_REJECTED_BY_SPAM_FILTER',
                                 name: 'Сообщение было отброшено платформой, так как сработал один из фильтров SMS-сообщений. Например, сработал спам-фильтр',
                                 code: 10,
                                 channel: Channel.gold)

    SendLogDeliveryError.create!(eng_name: 'ERROR_GSM_ROUTING',
                                 name: 'Ошибка маршрутизации в конфигурации платформы',
                                 code: 11,
                                 channel: Channel.gold)

    add_index :send_log_delivery_errors, [:code, :channel_id], unique: true
  end

  def down
  end
end
