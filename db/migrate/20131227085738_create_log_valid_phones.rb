# -*- encoding : utf-8 -*-
class CreateLogValidPhones < ActiveRecord::Migration
  def change
    create_table :log_valid_phones do |t|
      t.integer :log_id
      t.column :valid_phone, :bigint

      t.timestamps
    end
  end
end
