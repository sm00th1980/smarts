# -*- encoding : utf-8 -*-
class AddRejectToMegafonIntoStatuses < ActiveRecord::Migration
  def up
    SendLogDeliveryStatus.create!(name: 'Рассылка на Мегафон через золотой канал запрещена', internal_name: 'reject to megafon via gold channel', kannel_id: -3)
  end

  def down
  end
end
