# -*- encoding : utf-8 -*-
class RenameTimeIntoAutodeliveries < ActiveRecord::Migration
  def change
    rename_column :auto_deliveries, :time, :hour
  end
end
