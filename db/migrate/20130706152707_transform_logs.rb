# -*- encoding : utf-8 -*-
class TransformLogs < ActiveRecord::Migration
  def up
    add_column :logs, :phones, :string_array
    add_column :logs, :client_ids, :integer_array

    Log.all.each do |log|
      log.phones = [log.phone_b] if log.phone_b.present?
      log.client_ids = [log.client_id] if log.client_id.present?

      log.save!
    end

    #remove_column :logs, :client_id  if column_exists? :logs, :client_id
    remove_column :logs, :phone_b    if column_exists? :logs, :phone_b
    remove_column :logs, :status     if column_exists? :logs, :status
    remove_column :logs, :price      if column_exists? :logs, :price
    remove_column :logs, :direction  if column_exists? :logs, :direction
    remove_column :logs, :message_id if column_exists? :logs, :message_id
    remove_column :logs, :response   if column_exists? :logs, :response
  end

  def down
    remove_column :logs, :phones
    remove_column :logs, :client_ids
  end
end
