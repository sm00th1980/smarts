# -*- encoding : utf-8 -*-
class CleanLogs < ActiveRecord::Migration
  def up
    Log.delete_all
    SendLog.delete_all
  end

  def down
  end
end
