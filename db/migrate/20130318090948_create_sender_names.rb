# -*- encoding : utf-8 -*-
class CreateSenderNames < ActiveRecord::Migration
  def change
    create_table :sender_names do |t|
      t.string  :name
      t.integer :user_id
      t.integer :status_id
      t.text    :invalid_reason

      t.timestamps
    end
  end
end
