# -*- encoding : utf-8 -*-
def dup_hash(ary)
  ary.inject(Hash.new(0)) { |h,e| h[e] += 1; h }.select {
      |k,v| v > 1 }.inject({}) { |r, e| r[e.first] = e.last; r }
end

class RemoveDublicatePhonesInGroups < ActiveRecord::Migration
  def up
    Group.all.each do |group|
      phones = group.clients.map{|client| client.phone}

      if phones.count != phones.uniq.count
        dup_hash(phones).keys.each do |dup_phone|
          index = 0
          group.clients.each do |client|
            if dup_phone == client.phone
              if index == 0
              else
                client.destroy
              end
            end

            index = index + 1
          end

        end

      end
    end
  end

  def down
  end
end
