# -*- encoding : utf-8 -*-
class DropTablesAdmins < ActiveRecord::Migration
  def up
    drop_table :admins
  end

  def down
  end
end
