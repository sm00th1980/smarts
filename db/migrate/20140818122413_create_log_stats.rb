# -*- encoding : utf-8 -*-
class CreateLogStats < ActiveRecord::Migration
  def change
    create_table :log_stats do |t|
      t.integer :user_id, null: false
      t.date    :date, null: false
      t.integer :cell_operator_group_id, null: false

      t.float   :price
      t.integer :sms_sent_success
      t.integer :sms_sent_failure

      t.timestamps
    end
  end
end
