# -*- encoding : utf-8 -*-
class AddNotNullOnStatusIdIntoSenderNames < ActiveRecord::Migration
  def up
    change_column :sender_names, :status_id, :integer, :null => false
  end

  def down
    change_column :sender_names, :status_id, :integer, :null => true
  end
end
