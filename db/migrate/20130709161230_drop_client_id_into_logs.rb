# -*- encoding : utf-8 -*-
class DropClientIdIntoLogs < ActiveRecord::Migration
  def up
    remove_column :logs, :client_id
  end

  def down
  end
end
