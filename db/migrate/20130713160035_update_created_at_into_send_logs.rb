# -*- encoding : utf-8 -*-
class UpdateCreatedAtIntoSendLogs < ActiveRecord::Migration
  def up
    SendLog.all.each do |send_log|
      send_log.created_at = send_log.log.created_at
      send_log.save!
    end
  end

  def down
  end
end
