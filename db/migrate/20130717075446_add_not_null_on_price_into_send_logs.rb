# -*- encoding : utf-8 -*-
class AddNotNullOnPriceIntoSendLogs < ActiveRecord::Migration
  def up
    change_column :send_logs, :price, :float, :null => false
  end

  def down
    change_column :send_logs, :price, :float, :null => true
  end
end
