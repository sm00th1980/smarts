# -*- encoding : utf-8 -*-
class AddIndexToDateIntoLogs < ActiveRecord::Migration
  def up
    ActiveRecord::Base.connection.execute("CREATE INDEX index_logs_on_date_user_channel ON logs (date(created_at), user_id, channel_id)");
  end

  def down
    ActiveRecord::Base.connection.execute("DROP INDEX index_logs_on_date_user_channel");
  end
end
