# -*- encoding : utf-8 -*-
class RenameColumnInSenderNames < ActiveRecord::Migration
  def up
    rename_column :sender_names, :name, :value
  end

  def down
    rename_column :sender_names, :value, :name
  end
end
