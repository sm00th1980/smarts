# -*- encoding : utf-8 -*-
class DropChannelIdIntoLogs < ActiveRecord::Migration
  def up
    remove_column :logs, :channel_id
  end

  def down
  end
end
