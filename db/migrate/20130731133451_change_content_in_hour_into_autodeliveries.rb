# -*- encoding : utf-8 -*-
class ChangeContentInHourIntoAutodeliveries < ActiveRecord::Migration
  def up
    AutoDelivery.all.each do |auto_delivery|
      auto_delivery.hour = auto_delivery.hour.split(':').first
      auto_delivery.save!
    end
  end

  def down
    AutoDelivery.all.each do |auto_delivery|
      auto_delivery.hour = "#{auto_delivery.hour}:00"
      auto_delivery.save!
    end
  end
end
