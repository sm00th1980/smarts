# -*- encoding : utf-8 -*-
class AddFunctionalIndexToLogs < ActiveRecord::Migration
  def change
    execute('create index index_logs_on_date_created_at on logs (date(created_at))')
  end
end
