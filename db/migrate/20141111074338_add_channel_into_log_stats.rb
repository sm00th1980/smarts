# -*- encoding : utf-8 -*-
class AddChannelIntoLogStats < ActiveRecord::Migration
  def change
    add_column :log_stats, :channel_id, :integer
  end
end
