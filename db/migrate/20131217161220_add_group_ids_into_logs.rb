# -*- encoding : utf-8 -*-
class AddGroupIdsIntoLogs < ActiveRecord::Migration
  def change
    add_column :logs, :group_ids, :integer_array
  end
end
