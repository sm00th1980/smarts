# -*- encoding : utf-8 -*-
class AddNotNullToDeliveryStatusIdIntoSendLogs < ActiveRecord::Migration
  def up
    change_column :send_logs, :delivery_status_id, :integer, null: false
  end

  def down
    change_column :send_logs, :delivery_status_id, :integer, null: true
  end
end
