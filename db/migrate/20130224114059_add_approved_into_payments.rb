# -*- encoding : utf-8 -*-
class AddApprovedIntoPayments < ActiveRecord::Migration
  def change
    add_column :payments, :approved, :timestamp
  end
end
