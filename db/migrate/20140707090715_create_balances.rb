# -*- encoding : utf-8 -*-
class CreateBalances < ActiveRecord::Migration
  def change
    create_table :balances do |t|
      t.integer :period, null: false
      t.integer :user_id, null: false
      t.float :payments, :default => 0.0, :null => false
      t.float :charges, :default => 0.0, :null => false

      t.timestamps
    end
  end
end
