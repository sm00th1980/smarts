# -*- encoding : utf-8 -*-
class FillLogStatAgainAfterError < ActiveRecord::Migration
  def up
    LogStat.calculate('2013-08-01'.to_date, '2014-12-31'.to_date)
  end

  def down
    LogStat.delete_all
  end
end
