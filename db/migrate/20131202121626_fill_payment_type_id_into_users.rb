# -*- encoding : utf-8 -*-
class FillPaymentTypeIdIntoUsers < ActiveRecord::Migration
  def up
    User.all.each do |user|
      user.payment_type = PaymentType.prepaid
      user.save!
    end
  end

  def down
    User.all.each do |user|
      user.payment_type = nil
      user.save!
    end
  end
end
