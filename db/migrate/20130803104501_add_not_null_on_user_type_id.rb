# -*- encoding : utf-8 -*-
class AddNotNullOnUserTypeId < ActiveRecord::Migration
  def up
    change_column :users, :user_type_id, :integer, :null => false
  end

  def down
    change_column :users, :user_type_id, :integer, :null => true
  end
end
