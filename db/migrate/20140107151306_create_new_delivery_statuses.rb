# -*- encoding : utf-8 -*-
class CreateNewDeliveryStatuses < ActiveRecord::Migration
  def up
    SendLogDeliveryStatus.unknown
    SendLogDeliveryStatus.delivered
    SendLogDeliveryStatus.failured
    SendLogDeliveryStatus.buffered
    SendLogDeliveryStatus.submit
    SendLogDeliveryStatus.rejected
  end

  def down
  end
end
