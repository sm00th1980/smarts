# -*- encoding : utf-8 -*-
class CreateCellOperatorUralsvyazinform < ActiveRecord::Migration
  def up
    CellOperator.create!(name: 'УралСвязьИнформ', cell_operator_group_id: CellOperatorGroup.ural.id)
  end

  def down
  end
end
