# -*- encoding : utf-8 -*-
class UpdateBalanceColumns < ActiveRecord::Migration
  def up
    change_column :balances, :saldo_in, :float, :default => 0.0
    change_column :balances, :payment,  :float, :default => 0.0
    change_column :balances, :charge,   :float, :default => 0.0

    Balance.all.each do |balance|
      balance.saldo_in = 0.0 if balance.saldo_in.blank?
      balance.payment  = 0.0 if balance.payment.blank?
      balance.charge   = 0.0 if balance.charge.blank?

      balance.save!
    end
  end

  def down
  end
end
