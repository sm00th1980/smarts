# -*- encoding : utf-8 -*-
class RenameRejectToMegafonStatus < ActiveRecord::Migration
  def up
    status = SendLogDeliveryStatus.find_by_kannel_id(-3)
    status.name = 'Рассылка на Мегафон запрещена.'
    status.internal_name = 'reject to megafon'
    status.save!
  end

  def down
  end
end
