# -*- encoding : utf-8 -*-
class FillNullSmsCountIntoLogs < ActiveRecord::Migration
  def up
    Log.where('sms_count is NULL').find_each do |log|
      log.sms_count = Message.sms_count(log.content)
      log.save!
    end

    change_column :logs, :sms_count, :integer, default: 1, null: false
  end

  def down
  end
end
