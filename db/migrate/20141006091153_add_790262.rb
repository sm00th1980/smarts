# -*- encoding : utf-8 -*-
class Add790262 < ActiveRecord::Migration
  def up
    old = CellOperatorPhoneRange.find_by_begin_phone_and_end_phone(79026200000, 79026209999)
    old.cell_operator = CellOperator.find_by_name('УралСвязьИнформ')
    old.save!

    old = CellOperatorPhoneRange.find_by_begin_phone_and_end_phone(79026210000, 79026219999)
    old.cell_operator = CellOperator.find_by_name('УралСвязьИнформ')
    old.save!

    old = CellOperatorPhoneRange.find_by_begin_phone_and_end_phone(79026220000, 79026249999)
    old.cell_operator = CellOperator.find_by_name('УралСвязьИнформ')
    old.save!

    old = CellOperatorPhoneRange.find_by_begin_phone_and_end_phone(79026250000, 79026269999)
    old.cell_operator = CellOperator.find_by_name('УралСвязьИнформ')
    old.save!

    old = CellOperatorPhoneRange.find_by_begin_phone_and_end_phone(79026270000, 79026279999)
    old.cell_operator = CellOperator.find_by_name('УралСвязьИнформ')
    old.save!

    old = CellOperatorPhoneRange.find_by_begin_phone_and_end_phone(79026280000, 79026289999)
    old.cell_operator = CellOperator.find_by_name('УралСвязьИнформ')
    old.save!

    old = CellOperatorPhoneRange.find_by_begin_phone_and_end_phone(79026290000, 79026299999)
    old.cell_operator = CellOperator.find_by_name('УралСвязьИнформ')
    old.save!
  end

  def down
  end
end
