# -*- encoding : utf-8 -*-
class CreateSendLogDeliveryErrors < ActiveRecord::Migration
  def change
    create_table :send_log_delivery_errors do |t|
      t.text :eng_name, null: false
      t.text :name, null: false
      t.integer :code, null: false

      t.timestamps
    end
  end
end
