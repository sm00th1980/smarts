# -*- encoding : utf-8 -*-
class AddNotNull2IntoPayments < ActiveRecord::Migration
  def up
    change_column :payments, :amount, :float, null: false
    change_column :payments, :date, :datetime, null: false
  end

  def down
    change_column :payments, :amount, :float, null: true
    change_column :payments, :date, :datetime, null: true
  end
end
