# -*- encoding : utf-8 -*-
class FillDeliveryStatusIdIntoSendLogs < ActiveRecord::Migration
  def up
    SendLog.all.each do |send_log|
      if send_log.response.blank?
        send_log.delivery_status_id = SendLogDeliveryStatus.unknown.id
      else
        status = from_xml(send_log.response)
        send_log.delivery_status_id = SendLogDeliveryStatus.send(status).id

      end
      send_log.save!
    end

  end

  def down
    SendLog.all.each do |send_log|
      send_log.delivery_status_id = nil
      send_log.save!
    end
  end

  private
  def from_xml(xml)
    ['delivered', 'error', 'failed', 'wait'].each do |status|
      return status if xml.include?(status)
    end
  end
end
