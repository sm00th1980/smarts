# -*- encoding : utf-8 -*-
class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.integer  :user_id
      t.float    :amount
      t.datetime :date
      t.string   :service

      t.timestamps
    end
  end
end
