# -*- encoding : utf-8 -*-
class FillLogTypeIdIntoLogs < ActiveRecord::Migration
  def up
    ActiveRecord::Base.connection.execute("update logs set log_type_id = #{LogType.manual.id}")
  end

  def down
    ActiveRecord::Base.connection.execute("update logs set log_type_id = null")
  end
end
