# -*- encoding : utf-8 -*-
class ChangePhoneTypeIntoClients < ActiveRecord::Migration
  def up
    ActiveRecord::Base.connection.execute("alter table clients alter column phone type bigint using phone::bigint")
    change_column :clients, :phone, :bigint, null: false
  end

  def down
  end
end
