# -*- encoding : utf-8 -*-
class FillBithdaySenderNameIntoUsers < ActiveRecord::Migration
  def up
    User.find_each do |user|
      user.birthday_sender_name = user.active_sender_name_value
      user.save!
    end
  end

  def down
  end
end
