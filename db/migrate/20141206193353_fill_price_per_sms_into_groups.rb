# -*- encoding : utf-8 -*-
class FillPricePerSmsIntoGroups < ActiveRecord::Migration
  def up
    Group.find_each do |group|
      group.clients.find_each do |client|
        group.gold_price_per_sms += Message.price_per_sms(Channel.gold, group.user, client.phone).round(2)
        group.bronze_price_per_sms += Message.price_per_sms(Channel.bronze, group.user, client.phone).round(2)

        group.save!
      end
    end
  end

  def down
    execute('update groups set gold_price_per_sms = 0')
    execute('update groups set bronze_price_per_sms = 0')
  end
end
