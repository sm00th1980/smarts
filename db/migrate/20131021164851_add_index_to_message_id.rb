# -*- encoding : utf-8 -*-
class AddIndexToMessageId < ActiveRecord::Migration
  def change
    add_index :send_logs, :message_id
  end
end
