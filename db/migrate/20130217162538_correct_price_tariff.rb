# -*- encoding : utf-8 -*-
class CorrectPriceTariff < ActiveRecord::Migration
  def up
    Tariff.all.each do |tariff|
      tariff.price_per_sms = tariff.price_per_sms/100.0
      tariff.save!
    end
  end

  def down
    Tariff.all.each do |tariff|
      tariff.price_per_sms = tariff.price_per_sms*100.0
      tariff.save!
    end
  end
end
