# -*- encoding : utf-8 -*-
class Add79028127902820 < ActiveRecord::Migration
  def up
    old = CellOperatorPhoneRange.find_by_begin_phone_and_end_phone(79028120000, 79028139999)
    old.cell_operator = CellOperator.find_by_name('УралСвязьИнформ')
    old.save!

    old = CellOperatorPhoneRange.find_by_begin_phone_and_end_phone(79028140000, 79028149999)
    old.cell_operator = CellOperator.find_by_name('УралСвязьИнформ')
    old.save!

    old = CellOperatorPhoneRange.find_by_begin_phone_and_end_phone(79028150000, 79028159999)
    old.cell_operator = CellOperator.find_by_name('УралСвязьИнформ')
    old.save!

    old = CellOperatorPhoneRange.find_by_begin_phone_and_end_phone(79028160000, 79028169999)
    old.cell_operator = CellOperator.find_by_name('УралСвязьИнформ')
    old.save!

    old = CellOperatorPhoneRange.find_by_begin_phone_and_end_phone(79028170000, 79028179999)
    old.cell_operator = CellOperator.find_by_name('УралСвязьИнформ')
    old.save!

    old = CellOperatorPhoneRange.find_by_begin_phone_and_end_phone(79028180000, 79028189999)
    old.cell_operator = CellOperator.find_by_name('УралСвязьИнформ')
    old.save!

    old = CellOperatorPhoneRange.find_by_begin_phone_and_end_phone(79028190000, 79028199999)
    old.cell_operator = CellOperator.find_by_name('УралСвязьИнформ')
    old.save!

    old = CellOperatorPhoneRange.find_by_begin_phone_and_end_phone(79028200000, 79028209999)
    old.cell_operator = CellOperator.find_by_name('УралСвязьИнформ')
    old.save!
  end

  def down
  end
end
