# -*- encoding : utf-8 -*-
class AddOperatorToUsers < ActiveRecord::Migration
  def up
    add_column :users, :operator, :boolean, :default => false

    User.find_by_email('bigtaksa@mail.ru').update_attribute(:operator, true)
    User.find_by_email('serafima@youct.ru').update_attribute(:operator, true)
    User.find_by_email('yura@youct.ru').update_attribute(:operator, true)
    User.find_by_email('sm00th1980@mail.ru').update_attribute(:operator, true)

    User.find_by_email('deyarov@gmail.com').delete
  end

  def down
    remove_column :users, :operator
  end
end
