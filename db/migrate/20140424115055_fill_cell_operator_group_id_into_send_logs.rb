# -*- encoding : utf-8 -*-
class FillCellOperatorGroupIdIntoSendLogs < ActiveRecord::Migration
  def up
    ActiveRecord::Base.connection.execute("update send_logs set cell_operator_group_id = #{CellOperatorGroup.other.id}")
  end

  def down
    ActiveRecord::Base.connection.execute("update send_logs set cell_operator_group_id = null")
  end
end
