# -*- encoding : utf-8 -*-
class AddPaymentForMercury < ActiveRecord::Migration
  def up
    user = User.find_by_email('mercury@youct.ru')
    payments = Payment.where(:user_id => user.id).where('date(created_at) = ?', Date.parse('2013-07-23'))
    if payments.size == 0
      Payment.create!(:user_id => user.id,  :amount => 100, :service => 'bank', :date => Time.now, :status => 'success', :approved => Time.now)
    end
  end

  def down
    user = User.find_by_email('mercury@youct.ru')
    Payment.where(:user_id => user.id).where('date(created_at) = ?', Date.parse('2013-07-23')).first.delete
  end
end
