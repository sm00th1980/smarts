# -*- encoding : utf-8 -*-
class DropOldLogValidPhones < ActiveRecord::Migration
  def up
    ActiveRecord::Base.connection.execute("delete from log_valid_phones where date(created_at) < '#{Date.today}'::date");
  end

  def down
  end
end
