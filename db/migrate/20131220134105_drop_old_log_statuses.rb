# -*- encoding : utf-8 -*-
class DropOldLogStatuses < ActiveRecord::Migration
  def up
    LogStatus.find_by_internal_name('in_work').destroy
    LogStatus.find(4).destroy #old check_failed
  end

  def down
  end
end
