# -*- encoding : utf-8 -*-
class FillPeriodIntoAutodeliveries < ActiveRecord::Migration
  def up
    AutoDelivery.where(:period_id => nil).each do |auto_delivery|
      auto_delivery.update_attribute(:period_id, AutoDeliveryPeriod.every_day.id)
      auto_delivery.update_attribute(:hour, Period::DEFAULT_HOUR)
    end
  end

  def down
  end
end
