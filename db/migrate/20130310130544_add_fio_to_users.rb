# -*- encoding : utf-8 -*-
class AddFioToUsers < ActiveRecord::Migration
  def change
    add_column :users, :fio, :string
    add_column :users, :description, :text
  end
end
