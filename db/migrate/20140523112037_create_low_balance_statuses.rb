# -*- encoding : utf-8 -*-
class CreateLowBalanceStatuses < ActiveRecord::Migration
  def up
    SendLogDeliveryStatus.low_balance
    SendLogDeliveryError.low_balance
  end

  def down
    SendLogDeliveryStatus.low_balance.delete
    SendLogDeliveryError.low_balance.delete
  end
end
