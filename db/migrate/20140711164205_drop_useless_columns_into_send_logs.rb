# -*- encoding : utf-8 -*-
class DropUselessColumnsIntoSendLogs < ActiveRecord::Migration
  def up
    remove_column :send_logs, :notification
    remove_column :send_logs, :response
  end

  def down
  end
end
