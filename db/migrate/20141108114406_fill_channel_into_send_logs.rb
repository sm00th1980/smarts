# -*- encoding : utf-8 -*-
class FillChannelIntoSendLogs < ActiveRecord::Migration
  def up
    ActiveRecord::Base.connection.execute("update send_logs set channel_id = (select channel_id from logs where id = log_id)")
  end

  def down
  end
end
