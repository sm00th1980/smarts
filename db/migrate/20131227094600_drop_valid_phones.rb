# -*- encoding : utf-8 -*-
class DropValidPhones < ActiveRecord::Migration
  def up
    remove_column :logs, :valid_phones_count
    remove_column :logs, :valid_phones
    remove_column :logs, :client_ids
  end

  def down
  end
end
