# -*- encoding : utf-8 -*-
class AddGroupInTable < ActiveRecord::Migration
  def up
    Group.create!(:name => 'Группа1')
    Group.create!(:name => 'Группа2')
    Group.create!(:name => 'Группа3')
  end

  def down
    Group.find_by_name('Группа1').delete
    Group.find_by_name('Группа2').delete
    Group.find_by_name('Группа3').delete
  end
end
