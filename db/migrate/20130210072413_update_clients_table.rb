# -*- encoding : utf-8 -*-
class UpdateClientsTable < ActiveRecord::Migration
  def change
    add_column :clients, :fio, :string
    add_column :clients, :description, :text
    add_column :clients, :phone, :string
    add_column :clients, :user_id, :integer
  end
end
