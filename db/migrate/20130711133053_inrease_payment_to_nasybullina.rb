# -*- encoding : utf-8 -*-
class InreasePaymentToNasybullina < ActiveRecord::Migration
  def up
    Payment.create!(:user_id => User.find_by_email('s.nasybullina@sisamara.ru').id,  :amount => 50.5, :service => 'manual', :date => Time.now, :status => 'success', :approved => Time.now)
  end

  def down
  end
end
