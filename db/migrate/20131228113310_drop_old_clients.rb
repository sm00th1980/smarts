# -*- encoding : utf-8 -*-
class DropOldClients < ActiveRecord::Migration
  def up
    old_group_ids = []
    group_ids = Client.select('group_id').group("group_id").map{|o| o.group_id}.sort
    group_ids.each do |group_id|
      group = Group.find_by_id(group_id)
      old_group_ids << group_id if group.nil?
    end

    Client.where(group_id: old_group_ids).delete_all
  end

  def down
  end
end
