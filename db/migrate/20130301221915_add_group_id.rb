# -*- encoding : utf-8 -*-
class AddGroupId < ActiveRecord::Migration
  def change
    add_column :clients, :group_id, :integer
  end
end
