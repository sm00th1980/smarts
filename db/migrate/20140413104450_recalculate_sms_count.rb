# -*- encoding : utf-8 -*-
class RecalculateSmsCount < ActiveRecord::Migration
  def up
    ActiveRecord::Base.connection.execute("update logs set sms_sent_success = (select sum(sms_count) from send_logs where logs.id = log_id and delivery_status_id = %s) where date(created_at) >= '2014-04-01'::date" % SendLogDeliveryStatus.delivered.id)
    ActiveRecord::Base.connection.execute("update logs set sms_sent_failure = (select sum(sms_count) from send_logs where logs.id = log_id and delivery_status_id <> %s) where date(created_at) >= '2014-04-01'::date" % SendLogDeliveryStatus.delivered.id)
    ActiveRecord::Base.connection.execute("update logs set price = (select sum(price) from send_logs where logs.id = log_id) where date(created_at) >= '2014-04-01'::date")

    ActiveRecord::Base.connection.execute("update logs set sms_sent_success = 0 where sms_sent_success is null")
    ActiveRecord::Base.connection.execute("update logs set sms_sent_failure = 0 where sms_sent_failure is null")
    ActiveRecord::Base.connection.execute("update logs set price = 0 where price is null")
  end

  def down
  end
end
