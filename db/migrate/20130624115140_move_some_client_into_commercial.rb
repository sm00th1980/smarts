# -*- encoding : utf-8 -*-
class MoveSomeClientIntoCommercial < ActiveRecord::Migration
  def up
    User.find_by_email('bigtaksa@mail.ru').update_attribute(:operator, false)
    User.find_by_email('serafima@youct.ru').update_attribute(:operator, false)
    User.find_by_email('yura@youct.ru').update_attribute(:operator, false)
  end

  def down
    User.find_by_email('bigtaksa@mail.ru').update_attribute(:operator, true)
    User.find_by_email('serafima@youct.ru').update_attribute(:operator, true)
    User.find_by_email('yura@youct.ru').update_attribute(:operator, true)
  end
end
