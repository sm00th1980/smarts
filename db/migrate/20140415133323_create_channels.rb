# -*- encoding : utf-8 -*-
class CreateChannels < ActiveRecord::Migration
  def change
    create_table :channels do |t|
      t.string :name, null: false
      t.string :internal_name, null: false

      t.timestamps
    end
  end
end
