# -*- encoding : utf-8 -*-
class AddIndexToLogValidPhones < ActiveRecord::Migration
  def change
    add_index :log_valid_phones, [:log_id]
  end
end
