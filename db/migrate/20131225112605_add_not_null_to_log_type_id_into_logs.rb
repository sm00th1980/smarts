# -*- encoding : utf-8 -*-
class AddNotNullToLogTypeIdIntoLogs < ActiveRecord::Migration
  def up
    change_column :logs, :log_type_id, :integer, null: false
  end

  def down
    change_column :logs, :log_type_id, :integer, null: true
  end
end
