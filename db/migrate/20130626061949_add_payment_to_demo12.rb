# -*- encoding : utf-8 -*-
class AddPaymentToDemo12 < ActiveRecord::Migration
  def up
    Payment.create!(:user_id => User.find_by_email('demo12@youct.ru').id,  :amount => 10, :service => 'manual', :date => Time.now, :status => 'success', :approved => Time.now)
  end

  def down
  end
end
