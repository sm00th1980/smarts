# -*- encoding : utf-8 -*-
class FillClientIdsIntoLogs < ActiveRecord::Migration
  def up
    SendLog.all.each do |send_log|
      if send_log.client_id
        send_log.log.update_attribute(:client_ids, [send_log.client_id])
      else
        send_log.log.update_attribute(:client_ids, [])
      end
    end
  end

  def down
    Log.all.each do |log|
      log.update_attribute(:client_ids, [])
    end
  end
end
