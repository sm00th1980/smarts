# -*- encoding : utf-8 -*-
class AddSuccessIntoPayments < ActiveRecord::Migration
  def up
    add_column :payments, :success, :boolean, default: false, null: false
  end

  def down
    remove_column :payments, :success
  end
end
