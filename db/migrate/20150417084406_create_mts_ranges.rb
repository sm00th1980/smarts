# -*- encoding : utf-8 -*-
class CreateMtsRanges < ActiveRecord::Migration
  def change
    create_table :mts_ranges do |t|
      t.integer "begin_phone", limit: 8, null: false
      t.integer "end_phone", limit: 8, null: false
      t.text "region", null: false
      
      t.timestamps null: false
    end
  end
end
