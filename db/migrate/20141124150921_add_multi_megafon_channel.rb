# -*- encoding : utf-8 -*-
class AddMultiMegafonChannel < ActiveRecord::Migration
  def up
    Channel.create!(name: 'Мегафон "Многоподпись"', internal_name: 'megafon_multi')

    megafon_fixed = Channel.find_by_internal_name(:megafon)
    megafon_fixed.name = 'Мегафон "Фиксированная подпись"'
    megafon_fixed.internal_name = :megafon_fixed

    megafon_fixed.save!
  end

  def down
  end
end
