# -*- encoding : utf-8 -*-
class AddValidPhonesCountIntoLogs < ActiveRecord::Migration
  def change
    add_column :logs, :valid_phones_count, :integer
  end
end
