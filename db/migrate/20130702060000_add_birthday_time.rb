# -*- encoding : utf-8 -*-
class AddBirthdayTime < ActiveRecord::Migration
  def change
    add_column :users, :birthday_time, :string, :default => '10:00'
  end
end
