# -*- encoding : utf-8 -*-
class CleanMioseenkoLog < ActiveRecord::Migration
  def up
    Log.where(user_id: 28, status: nil).where('date(created_at) = ?', Date.parse('2013-07-02')).delete_all
  end

  def down
  end
end
