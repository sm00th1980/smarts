# -*- encoding : utf-8 -*-
class AddGroupIdsIntoAutodeliveries < ActiveRecord::Migration
  def change
    add_column :auto_deliveries, :group_ids, :integer_array
  end
end
