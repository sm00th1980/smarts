# -*- encoding : utf-8 -*-
class RenameClientsIdsIntoAutodeliveries < ActiveRecord::Migration
  def change
    rename_column :auto_deliveries, :client_ids, :_client_ids
  end
end
