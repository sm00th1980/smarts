# -*- encoding : utf-8 -*-
class AddNewErrorsIntoLogs < ActiveRecord::Migration
  def change
    add_column :logs, :check_errors, :text_array
  end
end
