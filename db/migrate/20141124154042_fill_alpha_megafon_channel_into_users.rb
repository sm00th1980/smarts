# -*- encoding : utf-8 -*-
class FillAlphaMegafonChannelIntoUsers < ActiveRecord::Migration
  def up
    User.all.each do |user|
      if user.fixed_name_megafon?
        user.alpha_megafon_channel = Channel.megafon_fixed
      else
        user.alpha_megafon_channel = Channel.gold
      end
      user.save!
    end

    change_column :users, :alpha_megafon_channel_id, :integer, default: Channel.gold.id, null: false
  end

  def down
  end
end
