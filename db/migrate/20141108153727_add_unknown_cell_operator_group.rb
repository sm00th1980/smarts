# -*- encoding : utf-8 -*-
class AddUnknownCellOperatorGroup < ActiveRecord::Migration
  def up
    CellOperatorGroup.create!(name: 'Неизвестный', internal_name: :unknown, price_per_sms: 0)
  end

  def down
  end
end
