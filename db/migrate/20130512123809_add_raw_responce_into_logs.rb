# -*- encoding : utf-8 -*-
class AddRawResponceIntoLogs < ActiveRecord::Migration
  def change
    add_column :logs, :response, :text
  end
end
