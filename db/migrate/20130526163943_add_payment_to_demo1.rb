# -*- encoding : utf-8 -*-
class AddPaymentToDemo1 < ActiveRecord::Migration
  def up
    Payment.create!(:user_id => User.find_by_email('demo1@youct.ru').id,  :amount => 28, :service => 'manual', :date => Time.now, :status => 'success', :approved => Time.now)
  end

  def down
  end
end
