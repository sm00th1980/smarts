# -*- encoding : utf-8 -*-
class AddUniqIndexToDirectories < ActiveRecord::Migration
  def change
    add_index :payment_services, [:internal_name], uniq:true
    add_index :send_log_check_statuses, [:internal_name], uniq:true
    add_index :sender_name_statuses, [:internal_name], uniq:true
    add_index :user_types, [:internal_name], uniq:true
  end
end
