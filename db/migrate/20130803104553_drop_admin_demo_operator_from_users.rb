# -*- encoding : utf-8 -*-
class DropAdminDemoOperatorFromUsers < ActiveRecord::Migration
  def up
    remove_column :users, :admin
    remove_column :users, :demo
    remove_column :users, :operator
  end

  def down
  end
end
