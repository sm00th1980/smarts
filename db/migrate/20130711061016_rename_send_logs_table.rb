# -*- encoding : utf-8 -*-
class RenameSendLogsTable < ActiveRecord::Migration
  def up
    rename_table :send_log_statuses, :send_log_delivery_statuses
  end

  def down
    rename_table :send_log_delivery_statuses, :send_log_statuses
  end
end
