# -*- encoding : utf-8 -*-
class AddNotNullToCellOperatorGroupIdIntoSendLogs < ActiveRecord::Migration
  def change
    change_column :send_logs, :cell_operator_group_id, :integer, null: false
  end
end
