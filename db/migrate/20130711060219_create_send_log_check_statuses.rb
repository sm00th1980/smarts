# -*- encoding : utf-8 -*-
class CreateSendLogCheckStatuses < ActiveRecord::Migration
  def up
    create_table :send_log_check_statuses do |t|
      t.string :name
      t.string :internal_name

      t.timestamps
    end

    SendLogCheckStatus.create!(name: 'Некорректный номер телефона', internal_name: 'invalid_phone')
  end

  def down
    drop_table :send_log_check_statuses
  end
end
