# -*- encoding : utf-8 -*-
class Drop255Error < ActiveRecord::Migration
  def up
    SendLogDeliveryError.where(code: 255).each do |error|
      error.delete
    end
  end

  def down
  end
end
