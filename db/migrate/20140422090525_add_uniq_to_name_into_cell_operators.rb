# -*- encoding : utf-8 -*-
class AddUniqToNameIntoCellOperators < ActiveRecord::Migration
  def change
    add_index :cell_operators, :name, unique: true
  end
end
