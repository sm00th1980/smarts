# -*- encoding : utf-8 -*-
class ReplaceCellOperatorGroupToSmartsForBronzeChannel < ActiveRecord::Migration
  def up
    log_ids = Log.where(channel_id: Channel.bronze.id).map{|log| log.id}.uniq

    sql = "update send_logs set cell_operator_group_id = %s where log_id in %s" % [CellOperatorGroup.smarts.id, log_ids.to_s.sub('[', '(').sub(']', ')')]

    ActiveRecord::Base.connection.execute(sql)
  end

  def down
  end
end
