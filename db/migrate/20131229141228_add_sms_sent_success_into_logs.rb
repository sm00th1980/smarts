# -*- encoding : utf-8 -*-
class AddSmsSentSuccessIntoLogs < ActiveRecord::Migration
  def change
    add_column :logs, :sms_sent_success, :integer, default: 0
  end
end
