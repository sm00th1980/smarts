# -*- encoding : utf-8 -*-
class Add790279790280 < ActiveRecord::Migration
  def up
    old = CellOperatorPhoneRange.find_by_begin_phone_and_end_phone(79027900000, 79028099999)
    old.cell_operator = CellOperator.find_by_name('УралСвязьИнформ')
    old.save!
  end

  def down
  end
end
