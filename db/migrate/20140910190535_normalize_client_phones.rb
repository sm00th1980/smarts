# -*- encoding : utf-8 -*-
class NormalizeClientPhones < ActiveRecord::Migration
  include PhoneHelper

  def up
    ActiveRecord::Base.connection.execute("delete from clients where group_id not in (select id from groups)")

    Client.find_each do |client|
      client.phone = normalize_phone_number(client.phone)
      client.save!
    end
  end

  def down
  end
end
