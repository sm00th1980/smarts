# -*- encoding : utf-8 -*-
class Add79048707904889 < ActiveRecord::Migration
  def up
    old = CellOperatorPhoneRange.find_by_begin_phone_and_end_phone(79048700000, 79048729999)
    old.cell_operator = CellOperator.find_by_name('УралСвязьИнформ')
    old.save!

    old = CellOperatorPhoneRange.find_by_begin_phone_and_end_phone(79048730000, 79048739999)
    old.cell_operator = CellOperator.find_by_name('УралСвязьИнформ')
    old.save!

    old = CellOperatorPhoneRange.find_by_begin_phone_and_end_phone(79048740000, 79048749999)
    old.cell_operator = CellOperator.find_by_name('УралСвязьИнформ')
    old.save!

    old = CellOperatorPhoneRange.find_by_begin_phone_and_end_phone(79048750000, 79048779999)
    old.cell_operator = CellOperator.find_by_name('УралСвязьИнформ')
    old.save!

    old = CellOperatorPhoneRange.find_by_begin_phone_and_end_phone(79048780000, 79048869999)
    old.cell_operator = CellOperator.find_by_name('УралСвязьИнформ')
    old.save!

    old = CellOperatorPhoneRange.find_by_begin_phone_and_end_phone(79048870000, 79048899999)
    old.cell_operator = CellOperator.find_by_name('УралСвязьИнформ')
    old.save!
  end

  def down
  end
end
