# -*- encoding : utf-8 -*-
class CreateDemoUser < ActiveRecord::Migration
  def up
    User.create!({
                    :email => 'demo@youct.ru',
                    :fio => 'Демо пользователь',
                    :description => 'Демо пользователь',
                    :password => 'demodemo',
                    :password_confirmation => 'demodemo',
                    :demo => true
                })
  end

  def down
    User.find_by_email('demo@youct.ru').destroy
  end
end
