# -*- encoding : utf-8 -*-
class ChangeValidPhonesToBigintegerIntoLogs < ActiveRecord::Migration
  def up
    change_column :logs, :valid_phones, :decimal_array
  end

  def down
  end
end
