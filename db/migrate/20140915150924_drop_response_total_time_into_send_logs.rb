# -*- encoding : utf-8 -*-
class DropResponseTotalTimeIntoSendLogs < ActiveRecord::Migration
  def up
    remove_column :send_logs, :response_total_time
  end

  def down
  end
end
