# -*- encoding : utf-8 -*-
class FillClientIdsIntoAutodeliveries < ActiveRecord::Migration
  def up
    AutoDelivery.all.each do |delivery|
      delivery.client_ids = delivery._client_ids.split(',') if delivery._client_ids
      delivery.save!
    end
  end

  def down
    AutoDelivery.all.each do |delivery|
      delivery.client_ids = []
      delivery.save!
    end
  end
end
