# -*- encoding : utf-8 -*-
class AddActiveIntoSenderNames < ActiveRecord::Migration
  def change
    add_column :sender_names, :active, :boolean
  end
end
