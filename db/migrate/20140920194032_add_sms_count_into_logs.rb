# -*- encoding : utf-8 -*-
class AddSmsCountIntoLogs < ActiveRecord::Migration
  def change
    add_column :logs, :sms_count, :integer, default: 1
  end
end
