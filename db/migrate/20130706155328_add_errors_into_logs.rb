# -*- encoding : utf-8 -*-
class AddErrorsIntoLogs < ActiveRecord::Migration
  def change
    add_column :logs, :results, :text
  end
end
