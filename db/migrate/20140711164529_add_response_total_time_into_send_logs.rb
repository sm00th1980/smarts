# -*- encoding : utf-8 -*-
class AddResponseTotalTimeIntoSendLogs < ActiveRecord::Migration
  def change
    add_column :send_logs, :response_total_time, :float, null: true
  end
end
