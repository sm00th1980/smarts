# -*- encoding : utf-8 -*-
class AddBalanceToDemo < ActiveRecord::Migration
  def up
    Payment.create!(:user_id => User.find_by_email('demo9@youct.ru').id,  :amount => 29, :service => 'manual', :date => Time.now, :status => 'success', :approved => Time.now)
    Payment.create!(:user_id => User.find_by_email('demo10@youct.ru').id, :amount => 28, :service => 'manual', :date => Time.now, :status => 'success', :approved => Time.now)
  end

  def down
  end
end
