# -*- encoding : utf-8 -*-
class RenameRejectToMegafonIntoUsers < ActiveRecord::Migration
  def up
    rename_column :users, :reject_to_megafon_via_gold_channel, :reject_to_megafon
  end

  def down
  end
end
