# -*- encoding : utf-8 -*-
class DropLogStat177806 < ActiveRecord::Migration
  def up
    execute('DROP MATERIALIZED VIEW log_stat_177806 CASCADE')
    Calculator::Logs.recalculate(Date.today, Date.today)
  end

  def down
  end
end
