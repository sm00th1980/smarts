# -*- encoding : utf-8 -*-
class CreatePaymentTypes < ActiveRecord::Migration
  def up
    create_table :payment_types do |t|
      t.string :name
      t.string :internal_name

      t.timestamps
    end

    PaymentType.create(name: 'Предоплата(дебет)', internal_name: :prepaid)
    PaymentType.create(name: 'Постоплата(кредит)', internal_name: :postpaid)
  end

  def down
    drop_table :payment_types
  end

end
