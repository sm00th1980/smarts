# -*- encoding : utf-8 -*-
class DropOldTariffs < ActiveRecord::Migration
  def up
    Tariff.all.each do |tariff|
      if tariff.min_sms_count
        tariff.delete
      end
    end
  end

  def down
  end
end
