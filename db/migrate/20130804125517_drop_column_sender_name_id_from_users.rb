# -*- encoding : utf-8 -*-
class DropColumnSenderNameIdFromUsers < ActiveRecord::Migration
  def up
    remove_column :users, :sender_name_id
  end

  def down
    add_column :users, :sender_name_id, :integer
  end
end
