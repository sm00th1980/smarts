# -*- encoding : utf-8 -*-
class AddNotNullIntoLogValidPhones < ActiveRecord::Migration
  def up
    change_column :log_valid_phones, :log_id, :integer, null: false
  end

  def down
    change_column :log_valid_phones, :log_id, :integer, null: true
  end
end
