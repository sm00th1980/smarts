# -*- encoding : utf-8 -*-
class RenameUnknownStatus < ActiveRecord::Migration
  def up
    status = SendLogDeliveryStatus.find_by_internal_name('unknown status')
    status.name = 'Сообщение отослано. Статус доставки неизвестен. Не пришло уведомление от СМС центра о доставке.'
    status.save!

    status = SendLogDeliveryError.find_by_eng_name('ERROR_UNKNOWN')
    status.name = 'Сообщение отослано. Статус доставки неизвестен. Не пришло уведомление от СМС центра о доставке.'
    status.save!
  end

  def down
    status = SendLogDeliveryStatus.find_by_internal_name('unknown status')
    status.name = 'Статус доставки неизвестен'
    status.save!

    status = SendLogDeliveryError.find_by_eng_name('ERROR_UNKNOWN')
    status.name = 'Ошибка неизвестна'
    status.save!
  end
end
