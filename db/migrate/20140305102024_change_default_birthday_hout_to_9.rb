# -*- encoding : utf-8 -*-
class ChangeDefaultBirthdayHoutTo9 < ActiveRecord::Migration
  def up
    change_column :users, :birthday_hour, :integer, default: 9
  end

  def down
    change_column :users, :birthday_hour, :integer, default: nil
  end
end
