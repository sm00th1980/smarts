# -*- encoding : utf-8 -*-
class Add79505007950564 < ActiveRecord::Migration
  def up
    old = CellOperatorPhoneRange.find_by_begin_phone_and_end_phone(79505000000, 79505399999)
    old.cell_operator = CellOperator.find_by_name('УралСвязьИнформ')
    old.save!

    old = CellOperatorPhoneRange.find_by_begin_phone_and_end_phone(79505400000, 79505649999)
    old.cell_operator = CellOperator.find_by_name('УралСвязьИнформ')
    old.save!
  end

  def down
  end
end
