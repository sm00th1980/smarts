# -*- encoding : utf-8 -*-
class AddNotNullToInternalNameIntoDeliveryStatuses < ActiveRecord::Migration
  def change
    change_column :send_log_delivery_statuses, :internal_name, :string, null: false
  end
end
