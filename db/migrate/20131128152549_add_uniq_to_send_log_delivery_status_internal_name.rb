# -*- encoding : utf-8 -*-
class AddUniqToSendLogDeliveryStatusInternalName < ActiveRecord::Migration
  def change
    add_index :send_log_delivery_statuses, [:internal_name], uniq: true
  end
end
