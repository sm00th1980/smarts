# -*- encoding : utf-8 -*-
class RenameGroupIdsIntoAutodeliveries < ActiveRecord::Migration
  def change
    rename_column :auto_deliveries, :group_ids, :_group_ids
  end
end
