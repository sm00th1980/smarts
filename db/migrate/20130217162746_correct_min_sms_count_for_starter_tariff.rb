# -*- encoding : utf-8 -*-
class CorrectMinSmsCountForStarterTariff < ActiveRecord::Migration
  def up
    starter_tariff = Tariff.find_by_name('Базовый')
    starter_tariff.min_sms_count=1
    starter_tariff.save!
  end

  def down
    starter_tariff = Tariff.find_by_name('Базовый')
    starter_tariff.min_sms_count=0
    starter_tariff.save!
  end
end
