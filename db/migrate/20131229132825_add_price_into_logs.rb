# -*- encoding : utf-8 -*-
class AddPriceIntoLogs < ActiveRecord::Migration
  def change
    add_column :logs, :price, :float, default: 0
  end
end
