# -*- encoding : utf-8 -*-
class AddRejectToMtsDeliveryStatus < ActiveRecord::Migration
  def up
    SendLogDeliveryStatus.create!(name: 'Рассылка на МТС запрещена.', internal_name: 'reject to mts', kannel_id: -5)
  end

  def down
    SendLogDeliveryStatus.reject_to_mts.delete
  end
end
