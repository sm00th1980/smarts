# -*- encoding : utf-8 -*-
class AddNotNullIntoLogs < ActiveRecord::Migration
  def up
    change_column :logs, :sms_sent_success, :integer, null: false
    change_column :logs, :sms_sent_failure, :integer, null: false
    change_column :logs, :price, :float, null: false
  end

  def down
    change_column :logs, :sms_sent_success, :integer, null: true
    change_column :logs, :sms_sent_failure, :integer, null: true
    change_column :logs, :price, :float, null: true
  end
end
