# -*- encoding : utf-8 -*-
class RecalculatePriceForGoldChannel < ActiveRecord::Migration
  def up
    Log.where(channel_id: Channel.gold.id).where('price <= 0').each do |log|
      log.send_logs.each do |send_log|
        send_log.price = Message.calc_price_per_sms(Channel.gold, log.user, send_log.phone)
        send_log.save!
      end
    end
  end

  def down
  end
end
