# -*- encoding : utf-8 -*-
class AddUniqToInrenalNameForCellOperatorGroup < ActiveRecord::Migration
  def change
    add_index :cell_operator_groups, :internal_name, unique: true
    add_index :cell_operator_groups, :name, unique: true
  end
end
