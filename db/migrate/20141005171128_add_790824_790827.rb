# -*- encoding : utf-8 -*-
class Add790824790827 < ActiveRecord::Migration
  def up
    old = CellOperatorPhoneRange.find_by_begin_phone_and_end_phone(79082400000, 79082799999)
    old.cell_operator = CellOperator.find_by_name('УралСвязьИнформ')
    old.save!
  end

  def down
  end
end
