# -*- encoding : utf-8 -*-
class CreateCellOperatorGroups < ActiveRecord::Migration
  def change
    create_table :cell_operator_groups do |t|
      t.string :name, null: false
      t.string :internal_name, null: false
      t.float  :price_per_sms, :default => 0.0, :null => false

      t.timestamps
    end
  end
end
