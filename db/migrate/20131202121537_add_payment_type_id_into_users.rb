# -*- encoding : utf-8 -*-
class AddPaymentTypeIdIntoUsers < ActiveRecord::Migration
  def up
    add_column :users, :payment_type_id, :integer
  end

  def down
    remove_column :users, :payment_type_id
  end
end
