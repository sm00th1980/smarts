# -*- encoding : utf-8 -*-
class CleanDb < ActiveRecord::Migration
  def up
    AutoDelivery.delete_all
    Client.delete_all
    Group.delete_all
    Payment.delete_all
    SenderName.delete_all
    Template.delete_all
    UserTariff.delete_all
    User.delete_all
  end

  def down
  end
end
