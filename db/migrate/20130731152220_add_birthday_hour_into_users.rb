# -*- encoding : utf-8 -*-
class AddBirthdayHourIntoUsers < ActiveRecord::Migration
  def up
    add_column :users, :birthday_hour, :integer
    User.all.each do |user|

      user.update_attribute(:birthday_hour, user.birthday_time.split(':').first)
    end
  end

  def down
    remove_column :users, :birthday_hour
  end
end
