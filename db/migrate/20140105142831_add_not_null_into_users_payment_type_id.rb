# -*- encoding : utf-8 -*-
class AddNotNullIntoUsersPaymentTypeId < ActiveRecord::Migration
  def up
    change_column :users, :payment_type_id, :integer, null: false
  end

  def down
    change_column :users, :payment_type_id, :integer, null: true
  end
end
