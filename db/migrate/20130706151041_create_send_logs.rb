# -*- encoding : utf-8 -*-
class CreateSendLogs < ActiveRecord::Migration
  def change
    create_table :send_logs do |t|
      t.integer :log_id
      t.text :phone
      t.text :status
      t.float :price
      t.integer :message_id
      t.text :response
      t.integer :client_id

      t.timestamps
    end
  end
end
