# -*- encoding : utf-8 -*-
class FillStartedAtIntoLogs < ActiveRecord::Migration
  def up
    Log.pluck(:id).sort.each do |log_id|
      first_send_log = SendLog.where(log_id: log_id).order(:created_at)[0]
      if first_send_log
        started_at = first_send_log.created_at_before_type_cast
        sql = "update logs set started_at = '#{started_at}' where id=#{log_id}"
        ActiveRecord::Base.connection.execute(sql)
      end

      last_send_log = SendLog.where(log_id: log_id).order('created_at DESC')[0]
      if last_send_log
        finished_at = last_send_log.created_at_before_type_cast
        sql = "update logs set finished_at = '#{finished_at}' where id=#{log_id}"
        ActiveRecord::Base.connection.execute(sql)
      end
    end
  end

  def down
    ActiveRecord::Base.connection.execute("update logs set started_at = null")
    ActiveRecord::Base.connection.execute("update logs set finished_at = null")
  end
end
