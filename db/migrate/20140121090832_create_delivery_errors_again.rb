# -*- encoding : utf-8 -*-
class CreateDeliveryErrorsAgain < ActiveRecord::Migration
  def up
    SendLogDeliveryError.kannel_failured
    SendLogDeliveryError.unknown
    SendLogDeliveryError.no_error
    SendLogDeliveryError.error_unknown_subscriber
    SendLogDeliveryError.error_unidentified_subscriber
    SendLogDeliveryError.error_absent_subscriber_sm
    SendLogDeliveryError.error_illegal_subscriber
    SendLogDeliveryError.error_teleservice_not_provisioned
    SendLogDeliveryError.error_illegal_equipment
    SendLogDeliveryError.error_call_barred
    SendLogDeliveryError.error_facility_not_supported
    SendLogDeliveryError.error_absent_subscriber
    SendLogDeliveryError.error_incompatible_terminal
    SendLogDeliveryError.error_subscriber_busy_for_mt_sms
    SendLogDeliveryError.error_sm_delivery_failure
    SendLogDeliveryError.error_message_waiting_list_full
    SendLogDeliveryError.error_system_failure
    SendLogDeliveryError.error_data_missing
    SendLogDeliveryError.error_unexpected_data_value
    SendLogDeliveryError.error_resource_limitation
    SendLogDeliveryError.error_position_method_failure
    SendLogDeliveryError.error_unknown_alphabet
    SendLogDeliveryError.error_ussd_busy
    SendLogDeliveryError.hlr_timeout
    SendLogDeliveryError.vlr_timeout
    SendLogDeliveryError.no_server_link_route
    SendLogDeliveryError.spam_filter_action
    SendLogDeliveryError.limit_exceeded
  end

  def down
  end
end
