# -*- encoding : utf-8 -*-
class RenameKannelId < ActiveRecord::Migration
  def up
    rename_column :send_log_delivery_statuses, :kannel_id, :smarts_id
  end

  def down
    rename_column :send_log_delivery_statuses, :smarts_id, :kannel_id
  end
end
