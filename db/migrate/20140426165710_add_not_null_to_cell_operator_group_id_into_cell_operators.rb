# -*- encoding : utf-8 -*-
class AddNotNullToCellOperatorGroupIdIntoCellOperators < ActiveRecord::Migration
  def change
    change_column :cell_operators, :cell_operator_group_id, :integer, null: false
  end
end
