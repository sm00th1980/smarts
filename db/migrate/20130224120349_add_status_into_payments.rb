# -*- encoding : utf-8 -*-
class AddStatusIntoPayments < ActiveRecord::Migration
  def change
    add_column :payments, :status, :string
  end
end
