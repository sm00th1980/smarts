# -*- encoding : utf-8 -*-
class UpdateSmartsTarif < ActiveRecord::Migration
  def up
    CellOperatorGroup.smarts.update_attribute(:price_per_sms, 0.09)
  end

  def down
    CellOperatorGroup.smarts.update_attribute(:price_per_sms, 0)
  end
end
