# -*- encoding : utf-8 -*-
class AddPeriodIntoAutodelivery < ActiveRecord::Migration
  def change
    add_column :auto_deliveries, :period_id, :integer

    add_column :auto_deliveries, :minutes, :integer, :null => true #для рассылок каждый час
    add_column :auto_deliveries, :time, :string, :null => true #для рассылок каждый день
    add_column :auto_deliveries, :week_day_id, :integer, :null => true #для рассылок каждую неделю
  end
end
