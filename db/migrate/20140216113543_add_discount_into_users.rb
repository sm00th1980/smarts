# -*- encoding : utf-8 -*-
class AddDiscountIntoUsers < ActiveRecord::Migration
  def change
    add_column :users, :discount, :integer, :default => 0, :null => false
  end
end
