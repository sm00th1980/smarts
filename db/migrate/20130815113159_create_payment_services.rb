# -*- encoding : utf-8 -*-
class CreatePaymentServices < ActiveRecord::Migration
  def change
    create_table :payment_services do |t|
      t.string :name
      t.string :internal_name

      t.timestamps
    end
  end
end
