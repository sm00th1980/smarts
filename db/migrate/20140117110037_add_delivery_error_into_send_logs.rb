# -*- encoding : utf-8 -*-
class AddDeliveryErrorIntoSendLogs < ActiveRecord::Migration
  def change
    add_column :send_logs, :delivery_error_id, :integer
  end
end
