# -*- encoding : utf-8 -*-
class LoadNewMtsRanges < ActiveRecord::Migration
  def up
    cell_operator = CellOperator.find_by(id: 79)
    MtsRange.find_each do |new_range|
      CellOperatorPhoneRange.create!(begin_phone: new_range.begin_phone, end_phone: new_range.end_phone, region: new_range.region, cell_operator: cell_operator)
    end
  end
end
