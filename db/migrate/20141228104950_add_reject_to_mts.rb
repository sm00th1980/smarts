# -*- encoding : utf-8 -*-
class AddRejectToMts < ActiveRecord::Migration
  def change
    add_column :users, :reject_to_mts, :boolean, default: false, null: false
  end
end
