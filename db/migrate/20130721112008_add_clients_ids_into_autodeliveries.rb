# -*- encoding : utf-8 -*-
class AddClientsIdsIntoAutodeliveries < ActiveRecord::Migration
  def up
    add_column :auto_deliveries, :client_ids, :integer_array
  end
end
