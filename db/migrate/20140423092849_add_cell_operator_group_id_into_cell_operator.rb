# -*- encoding : utf-8 -*-
class AddCellOperatorGroupIdIntoCellOperator < ActiveRecord::Migration
  def up
    add_column :cell_operators, :cell_operator_group_id, :integer
    remove_column :cell_operators, :smarts
  end

  def down
    remove_column :cell_operators, :cell_operator_group_id
    add_column :cell_operators, :smarts, :boolean, default: false, null: false
  end
end
