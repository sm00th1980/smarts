# -*- encoding : utf-8 -*-
class RenamePermitAlphanumericInUsers < ActiveRecord::Migration
  def change
    rename_column :users, :permit_alphanumeric, :permit_alpha_sender_name
  end
end
