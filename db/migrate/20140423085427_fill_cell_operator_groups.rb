# -*- encoding : utf-8 -*-
class FillCellOperatorGroups < ActiveRecord::Migration
  def up
    CellOperatorGroup.create!(name: 'МТС', internal_name: :mts, price_per_sms: 0.31)
    CellOperatorGroup.create!(name: 'Билайн', internal_name: :beeline, price_per_sms: 0.26)
    CellOperatorGroup.create!(name: 'Мегафон', internal_name: :megafon, price_per_sms: 0.38)
    CellOperatorGroup.create!(name: 'Теле2', internal_name: :tele2, price_per_sms: 0.28)
    CellOperatorGroup.create!(name: 'Байкал Вестком', internal_name: :baikal, price_per_sms: 0.1)
    CellOperatorGroup.create!(name: 'Уралсвязьинформ', internal_name: :ural, price_per_sms: 0.33)
    CellOperatorGroup.create!(name: 'Енисейтелеком', internal_name: :enisey, price_per_sms: 0.31)
    CellOperatorGroup.create!(name: 'CDMA-операторы', internal_name: :cdma, price_per_sms: 1.14)
    CellOperatorGroup.create!(name: 'Остальные GSM операторы РФ', internal_name: :other, price_per_sms: 0.07)
    CellOperatorGroup.create!(name: 'СМАРТС', internal_name: :smarts, price_per_sms: 0.0)
  end

  def down
    CellOperatorGroup.delete_all
  end
end
