# -*- encoding : utf-8 -*-
class RecalcSendLogsWithNewOperators < ActiveRecord::Migration
  def up
    Log.where(channel_id: Channel.gold.id).where('date(created_at) >= ?', Date.parse('2014-09-01')).find_each do |log|
      log.send_logs.where(cell_operator_group_id: CellOperatorGroup.other.id).find_each do |send_log|
        send_log.cell_operator_group_id = CellOperator.operator_group(log.channel, send_log.phone).id
        send_log.price = send_log.sms_count * Message.price_per_sms(log.channel, log.user, send_log.phone)
        send_log.save!
      end
    end
  end

  def down
  end
end
