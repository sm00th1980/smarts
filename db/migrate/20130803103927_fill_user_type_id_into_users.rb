# -*- encoding : utf-8 -*-
class FillUserTypeIdIntoUsers < ActiveRecord::Migration
  def up
    User.all.each do |user|
      user.update_attribute(:user_type_id, UserType.admin.id)    if user.admin
      user.update_attribute(:user_type_id, UserType.demo.id)     if user.demo
      user.update_attribute(:user_type_id, UserType.operator.id) if user.operator

      user.update_attribute(:user_type_id, UserType.business.id) if not user.admin and not user.demo and not user.operator
    end
  end

  def down
    User.all.each do |user|
      user.update_attribute(:user_type_id, nil)
    end
  end
end
