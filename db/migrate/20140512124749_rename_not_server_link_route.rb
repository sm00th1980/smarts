# -*- encoding : utf-8 -*-
class RenameNotServerLinkRoute < ActiveRecord::Migration
  def up
    status = SendLogDeliveryError.find_by_code(3840)
    status.name = 'Нет маршрута для серверного линка'
    status.save!
  end

  def down
    status = SendLogDeliveryError.find_by_code(3840)
    status.name = 'Нет маршрута для серверверного линка'
    status.save!
  end
end
