# -*- encoding : utf-8 -*-
class AddTariffs < ActiveRecord::Migration
  def up
    Tariff.create!(:name => 'Базовый',    :price_per_sms => 50, :min_sms_count => 0,      :max_sms_count => 1000)
    Tariff.create!(:name => 'Средний',    :price_per_sms => 20, :min_sms_count => 1001,   :max_sms_count => 50000)
    Tariff.create!(:name => 'Крупный',    :price_per_sms => 10, :min_sms_count => 50001,  :max_sms_count => 500000)
    Tariff.create!(:name => 'Гигантский', :price_per_sms => 8,  :min_sms_count => 500001, :max_sms_count => 1000000)
  end

  def down
    Tariff.delete_all
  end
end
