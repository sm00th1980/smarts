# -*- encoding : utf-8 -*-
class AddValidPhoneIntoClients < ActiveRecord::Migration
  def change
    add_column :clients, :valid_phone, :boolean, default: false
  end
end
