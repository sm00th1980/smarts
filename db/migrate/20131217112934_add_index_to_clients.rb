# -*- encoding : utf-8 -*-
class AddIndexToClients < ActiveRecord::Migration
  def change
    add_index :clients, :group_id
  end
end
