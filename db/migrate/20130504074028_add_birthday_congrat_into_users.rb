# -*- encoding : utf-8 -*-
class AddBirthdayCongratIntoUsers < ActiveRecord::Migration
  def change
    add_column :users, :birthday_message, :text
    add_column :clients, :birthday_congratulation , :boolean, :default => false
  end
end
