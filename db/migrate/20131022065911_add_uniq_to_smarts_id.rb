# -*- encoding : utf-8 -*-
class AddUniqToSmartsId < ActiveRecord::Migration
  def change
    add_index :send_log_delivery_statuses, [:smarts_id], :uniq => true
  end
end
