# -*- encoding : utf-8 -*-
class AddSkipNightIntoUsers < ActiveRecord::Migration
  def change
    add_column :users, :skip_night, :boolean, default: true, null: false
  end
end
