# -*- encoding : utf-8 -*-
class AddChannelIdIntoErrors < ActiveRecord::Migration
  def change
    add_column :send_log_delivery_errors, :channel_id, :integer
  end
end
