# -*- encoding : utf-8 -*-
class ChangeTypeForLogsPhones < ActiveRecord::Migration
  include PhoneHelper

  def up
    add_column :logs, :phones_new, :bigint, array: true

    sql = %Q{
CREATE OR REPLACE FUNCTION normalize_phones(character varying(255)[]) RETURNS bigint[] AS $$
DECLARE
   _phones bigint[];
   _phone character varying(255);
BEGIN
    FOREACH _phone IN ARRAY $1
    LOOP
      _phone := (regexp_matches(_phone, '\d+'))[1];
      _phone := substring(_phone from 2 for length(_phone));
      _phone := concat('7', _phone);

      _phones := array_append(_phones, _phone::bigint);
    END LOOP;

    RETURN _phones;
END;
$$ LANGUAGE plpgsql;
          }

    execute(sql);
    execute('update logs set phones_new=normalize_phones(phones)');

    remove_column :logs, :phones
    rename_column :logs, :phones_new, :phones

    execute('DROP FUNCTION normalize_phones(character varying[])');
  end

  def down
    remove_column :logs, :phones_new
  end
end
