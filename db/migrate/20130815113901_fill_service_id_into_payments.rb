# -*- encoding : utf-8 -*-
class FillServiceIdIntoPayments < ActiveRecord::Migration
  def up
    Payment.all.each do |payment|
      payment.update_attribute(:service_id, PaymentService.bank.id)      if payment.attributes_before_type_cast["service"] == PaymentService.bank.internal_name
      payment.update_attribute(:service_id, PaymentService.manual.id)    if payment.attributes_before_type_cast["service"] == PaymentService.manual.internal_name
      payment.update_attribute(:service_id, PaymentService.robokassa.id) if payment.attributes_before_type_cast["service"] == PaymentService.robokassa.internal_name
    end
  end

  def down
    Payment.all.each do |payment|
      payment.update_attribute(:service_id, nil)
    end
  end
end
