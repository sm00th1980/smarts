# -*- encoding : utf-8 -*-
class DropFixedNameMegafonFromUsers < ActiveRecord::Migration
  def up
    remove_column :users, :fixed_name_megafon
  end

  def down
  end
end
