# -*- encoding : utf-8 -*-
class FillPriceIntoLogs < ActiveRecord::Migration
  def up
    Log.order(:id).each do |log|
      log.update_column(:price, SendLog.select("sum(price) as price").where(log_id: log.id)[0].price.to_f.round(2))
    end
  end

  def down
    ActiveRecord::Base.connection.execute("update logs set price = 0")
  end
end
