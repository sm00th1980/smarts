# -*- encoding : utf-8 -*-
class ChangePhoneTypeIntoLogs < ActiveRecord::Migration
  def up
    change_column :logs, :phone_b, :string
  end

  def down
    change_column :logs, :phone_b, :integer
  end
end
