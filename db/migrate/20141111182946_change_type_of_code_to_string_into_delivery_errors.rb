# -*- encoding : utf-8 -*-
class ChangeTypeOfCodeToStringIntoDeliveryErrors < ActiveRecord::Migration
  def up
    change_column :send_log_delivery_errors, :code, :string, null: false
  end

  def down
  end
end
