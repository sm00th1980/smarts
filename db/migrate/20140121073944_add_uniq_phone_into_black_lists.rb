# -*- encoding : utf-8 -*-
class AddUniqPhoneIntoBlackLists < ActiveRecord::Migration
  def change
    add_index :black_lists, [:user_id, :phone], unique: true
  end
end
