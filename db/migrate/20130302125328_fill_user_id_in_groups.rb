# -*- encoding : utf-8 -*-
class FillUserIdInGroups < ActiveRecord::Migration
  def up
    Group.all.each do |group|
      group.user = User.first
      group.save!
    end
  end

  def down
    Group.all.each do |group|
      group.user = nil
      group.save!
    end
  end
end
