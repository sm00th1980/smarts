# -*- encoding : utf-8 -*-
class AddChrageTypeIntoUsers < ActiveRecord::Migration
  def up
    add_column :users, :charge_type_id, :integer
  end

  def down
    remove_column :users, :charge_type_id
  end
end
