# -*- encoding : utf-8 -*-
class AddValidPhonesCountIntoGroups < ActiveRecord::Migration
  def change
    add_column :groups, :valid_phones_count, :integer
  end
end
