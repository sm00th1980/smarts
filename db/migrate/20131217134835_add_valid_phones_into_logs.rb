# -*- encoding : utf-8 -*-
class AddValidPhonesIntoLogs < ActiveRecord::Migration
  def change
    add_column :logs, :valid_phones, :integer_array
  end
end
