# -*- encoding : utf-8 -*-
class CreateAutoDeliveryPeriods < ActiveRecord::Migration
  def up
    create_table :auto_delivery_periods do |t|
      t.string :name
      t.string :period

      t.timestamps
    end

    AutoDeliveryPeriod.create!(:name => 'Каждый час',    :period => :every_hour)
    AutoDeliveryPeriod.create!(:name => 'Каждый день',   :period => :every_day)
    AutoDeliveryPeriod.create!(:name => 'Каждую неделю', :period => :every_week)
  end

  def down
    drop_table :auto_delivery_periods
  end
end
