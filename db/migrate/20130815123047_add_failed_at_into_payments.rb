# -*- encoding : utf-8 -*-
class AddFailedAtIntoPayments < ActiveRecord::Migration
  def change
    add_column :payments, :failed_at, :datetime
  end
end
