# -*- encoding : utf-8 -*-
class TransformDeliveryStatusIdIntoSendLogs < ActiveRecord::Migration
  def up
    ActiveRecord::Base.connection.execute("update send_logs set delivery_status_id = 24 where delivery_status_id = 13")
    ActiveRecord::Base.connection.execute("update send_logs set delivery_status_id = 26 where delivery_status_id in (21, 12, 11)")
    ActiveRecord::Base.connection.execute("update send_logs set delivery_status_id = 29 where delivery_status_id = 14")
    ActiveRecord::Base.connection.execute("update send_logs set delivery_status_id = 25 where delivery_status_id = 8")
  end

  def down
  end
end
