# -*- encoding : utf-8 -*-
class AddFixMegafonIntoUsers < ActiveRecord::Migration
  def change
    add_column :users, :fixed_name_megafon, :boolean, default: false, null: false
  end
end
