# -*- encoding : utf-8 -*-
class AddNameIntoTemplates < ActiveRecord::Migration
  def up
    add_column :templates, :name, :text

    Template.all.each do |template|
      template.name = template.text
      template.save!
    end
  end

  def down
    remove_column :templates, :name
  end
end
