# -*- encoding : utf-8 -*-
class RenameErrorUnknownIntoStatusUnknown < ActiveRecord::Migration
  def up
    SendLogDeliveryError.where(code: -1).each do |error|
      error.eng_name = 'STATUS_UNKNOWN'
      error.save!
    end
  end

  def down
  end
end
