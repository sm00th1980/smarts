# -*- encoding : utf-8 -*-
class AddNewIntoLogStatus < ActiveRecord::Migration
  def up
    LogStatus.create!(:name => 'Новая рассылка', :internal_name => 'new')
  end

  def down
    LogStatus.find_by_internal_name('new').delete
  end
end
