# -*- encoding : utf-8 -*-
class CreateSendLogStatuses < ActiveRecord::Migration
  def change
    create_table :send_log_statuses do |t|
      t.string :name
      t.string :internal_name

      t.timestamps
    end

    SenderNameStatus.create!(name: 'Некорректный номер телефона', internal_name: :invalid_phone)
    SenderNameStatus.create!(name: 'Некоррекное содержание сообщения', internal_name: :invalid_content)
    SenderNameStatus.create!(name: 'Некоррекное имя отправителя', internal_name: :invalid_sender_name)
  end
end
