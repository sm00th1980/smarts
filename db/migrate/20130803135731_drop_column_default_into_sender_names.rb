# -*- encoding : utf-8 -*-
class DropColumnDefaultIntoSenderNames < ActiveRecord::Migration
  def up
    remove_column :sender_names, :default
  end

  def down
    add_column :sender_names, :default, :boolean, :default => false
  end
end
