# -*- encoding : utf-8 -*-
class DropClientIdIntoSendLogs < ActiveRecord::Migration
  def up
    remove_column :send_logs, :client_id
  end

  def down
  end
end
