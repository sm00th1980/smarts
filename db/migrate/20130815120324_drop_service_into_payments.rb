# -*- encoding : utf-8 -*-
class DropServiceIntoPayments < ActiveRecord::Migration
  def up
    remove_column :payments, :service
  end

  def down
  end
end
