# -*- encoding : utf-8 -*-
class UpdateTo9HourForUsers < ActiveRecord::Migration
  def up
    User.where('birthday_hour is null').each do |user|
      user.birthday_hour = 9
      user.save!
    end
  end

  def down
  end
end
