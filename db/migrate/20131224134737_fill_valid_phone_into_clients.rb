# -*- encoding : utf-8 -*-
class FillValidPhoneIntoClients < ActiveRecord::Migration
  def up
    Client.order(:id).each do |client|
      client.update_column(:valid_phone, client.phone_valid?)
    end
  end

  def down
    ActiveRecord::Base.connection.execute("update clients set valid_phone = false")
  end
end
