# -*- encoding : utf-8 -*-
class ChangeColumnTypeForHourIntoAutodeliveries < ActiveRecord::Migration
  def up
    add_column :auto_deliveries, :new_hour, :integer

    AutoDelivery.all.each do |auto_delivery|
      auto_delivery.new_hour = auto_delivery.hour
      auto_delivery.save!
    end

    remove_column :auto_deliveries, :hour
    rename_column :auto_deliveries, :new_hour, :hour
  end

  def down

  end
end
