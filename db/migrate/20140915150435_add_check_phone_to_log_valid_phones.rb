# -*- encoding : utf-8 -*-
class AddCheckPhoneToLogValidPhones < ActiveRecord::Migration
  def up
    ActiveRecord::Base.connection.execute("ALTER TABLE log_valid_phones ADD CHECK (valid_phone between %i and %i)" % [79000000000, 79999999999])
    change_column :log_valid_phones, :valid_phone, :bigint, null: false
  end

  def down

  end
end
