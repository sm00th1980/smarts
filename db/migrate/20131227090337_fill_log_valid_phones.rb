# -*- encoding : utf-8 -*-
class FillLogValidPhones < ActiveRecord::Migration
  def up
    log_ids = Log.pluck(:id).sort

    log_ids.each do |log_id|
      SendLog.select('phone').where(log_id: log_id).group('phone').map{|s| s.phone}.each do |valid_phone|
        LogValidPhones.create!(log_id: log_id, valid_phone: valid_phone)
      end
    end
  end

  def down
    LogValidPhones.delete_all
  end
end
