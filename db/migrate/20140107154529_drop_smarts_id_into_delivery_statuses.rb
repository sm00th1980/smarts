# -*- encoding : utf-8 -*-
class DropSmartsIdIntoDeliveryStatuses < ActiveRecord::Migration
  def up
    remove_column :send_log_delivery_statuses, :smarts_id
  end

  def down
  end
end
