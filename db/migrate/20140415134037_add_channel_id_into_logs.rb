# -*- encoding : utf-8 -*-
class AddChannelIdIntoLogs < ActiveRecord::Migration
  def change
    add_column :logs, :channel_id, :integer, null: false, default: Channel.bronze.id
  end
end
