# -*- encoding : utf-8 -*-
class AddIndexCellOperatorIdIntoRanges < ActiveRecord::Migration
  def up
    change_column :cell_operator_phone_ranges, :cell_operator_id, :integer, null: false
  end

  def down
    change_column :cell_operator_phone_ranges, :cell_operator_id, :integer, null: true
  end
end
