# -*- encoding : utf-8 -*-
class DropOldDeliveryStatuses < ActiveRecord::Migration
  def up
    SendLogDeliveryStatus.where('kannel_id is null').each do |status|
      status.delete
    end
  end

  def down
  end
end
