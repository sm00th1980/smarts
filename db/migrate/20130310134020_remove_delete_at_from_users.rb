# -*- encoding : utf-8 -*-
class RemoveDeleteAtFromUsers < ActiveRecord::Migration
  def up
    remove_column :users, :deteted_at
  end

  def down
    add_column :users, :deteted_at, :timetstamp
  end
end
