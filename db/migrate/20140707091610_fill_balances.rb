# -*- encoding : utf-8 -*-
class FillBalances < ActiveRecord::Migration
  def up
    periods = [
        {begin_date: Date.parse('2013-11-01').at_beginning_of_month.to_date, end_date: Date.parse('2013-11-01').at_end_of_month.to_date},
        {begin_date: Date.parse('2013-12-01').at_beginning_of_month.to_date, end_date: Date.parse('2013-12-01').at_end_of_month.to_date},
        {begin_date: Date.parse('2014-01-01').at_beginning_of_month.to_date, end_date: Date.parse('2014-01-01').at_end_of_month.to_date},
        {begin_date: Date.parse('2014-02-01').at_beginning_of_month.to_date, end_date: Date.parse('2014-02-01').at_end_of_month.to_date},
        {begin_date: Date.parse('2014-03-01').at_beginning_of_month.to_date, end_date: Date.parse('2014-03-01').at_end_of_month.to_date},
        {begin_date: Date.parse('2014-04-01').at_beginning_of_month.to_date, end_date: Date.parse('2014-04-01').at_end_of_month.to_date},
        {begin_date: Date.parse('2014-05-01').at_beginning_of_month.to_date, end_date: Date.parse('2014-05-01').at_end_of_month.to_date},
        {begin_date: Date.parse('2014-06-01').at_beginning_of_month.to_date, end_date: Date.parse('2014-06-01').at_end_of_month.to_date}
    ]

    User.all.each do |user|
      periods.each do |period|
        payments = user.payments_sum(period[:begin_date], period[:end_date]).to_f
        charges  = Log.select("sum(price) as price").where(:user_id => user.id).where('date(created_at) >= ? and date(created_at) <= ?', period[:begin_date], period[:end_date]).first[:price].to_f
        if payments.round(2) > 0 or charges.round(2) > 0
          Balance.create!(period: period[:begin_date].strftime('%Y%m').to_i, user_id: user.id, payments: payments.round(2), charges: charges.round(2))
        end
      end
    end
  end

  def down
    Balance.delete_all
  end
end
