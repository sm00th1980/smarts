# -*- encoding : utf-8 -*-
class AddRejectToSmartsViaGoldChannelIntoUsers < ActiveRecord::Migration
  def change
    add_column :users, :reject_to_smarts_via_gold_channel, :boolean, default: false, null: false
  end
end
