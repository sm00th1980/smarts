# -*- encoding : utf-8 -*-
class AddLogStatusIdIntoLogs < ActiveRecord::Migration
  def up
    add_column :logs, :status_id, :integer

    Log.all.each do |log|
      log.status_id = LogStatus.completed.id
      log.save!
    end
  end

  def down
    remove_column :logs, :status_id
  end
end
