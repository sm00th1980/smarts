# -*- encoding : utf-8 -*-
class FillCellOperatorIdIntoRanges < ActiveRecord::Migration
  def up
    CellOperatorPhoneRange.all.each do |range|
      cell_operator = CellOperator.find_by_name(range.cell_operator_text)

      if cell_operator
        range.cell_operator_id = cell_operator.id
        range.save!
      end
    end
  end

  def down
    CellOperatorPhoneRange.all.each do |range|
      range.cell_operator_id = nil
      range.save!
    end
  end
end
