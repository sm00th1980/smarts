# -*- encoding : utf-8 -*-
class CreateCellOperators < ActiveRecord::Migration
  def change
    create_table :cell_operators do |t|
      t.string :name, null: false
      t.boolean :smarts, null: false, default: false

      t.timestamps
    end
  end
end
