# -*- encoding : utf-8 -*-
class Add790807790809 < ActiveRecord::Migration
  def up
    old = CellOperatorPhoneRange.find_by_begin_phone_and_end_phone(79080700000, 79080839999)
    old.cell_operator = CellOperator.find_by_name('УралСвязьИнформ')
    old.end_phone = 79080999999
    old.save!

    CellOperatorPhoneRange.find_by_begin_phone_and_end_phone(79080850000, 79080999999).delete
  end

  def down
  end
end
