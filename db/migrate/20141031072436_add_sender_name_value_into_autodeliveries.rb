# -*- encoding : utf-8 -*-
class AddSenderNameValueIntoAutodeliveries < ActiveRecord::Migration
  def change
    add_column :auto_deliveries, :sender_name_value, :text
  end
end
