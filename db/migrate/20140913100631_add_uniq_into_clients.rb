# -*- encoding : utf-8 -*-
class AddUniqIntoClients < ActiveRecord::Migration
  def change
    add_index :clients, [:group_id, :phone], unique: true
  end
end
