# -*- encoding : utf-8 -*-
class AddNotDeliveredTypeIntoLogTypes < ActiveRecord::Migration
  def up
    LogType.create!(name: 'По недоставленным', internal_name: 'by_not_delivered')
  end

  def down
    LogType.by_not_delivered.delete
  end
end
