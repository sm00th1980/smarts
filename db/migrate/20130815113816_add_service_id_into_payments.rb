# -*- encoding : utf-8 -*-
class AddServiceIdIntoPayments < ActiveRecord::Migration
  def change
    add_column :payments, :service_id, :integer
  end
end
