# -*- encoding : utf-8 -*-
class Add79512307951279 < ActiveRecord::Migration
  def up
    old = CellOperatorPhoneRange.find_by_begin_phone_and_end_phone(79512300000, 79512619999)
    old.cell_operator = CellOperator.find_by_name('УралСвязьИнформ')
    old.save!

    old = CellOperatorPhoneRange.find_by_begin_phone_and_end_phone(79512620000, 79512779999)
    old.cell_operator = CellOperator.find_by_name('УралСвязьИнформ')
    old.save!

    old = CellOperatorPhoneRange.find_by_begin_phone_and_end_phone(79512780000, 79512789999)
    old.cell_operator = CellOperator.find_by_name('УралСвязьИнформ')
    old.end_phone = 79512799999
    old.save!
  end

  def down
  end
end
