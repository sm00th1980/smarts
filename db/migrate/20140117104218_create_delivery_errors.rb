# -*- encoding : utf-8 -*-
class CreateDeliveryErrors < ActiveRecord::Migration

  def up
    SendLogDeliveryError.create(eng_name: 'SUCCESS_DELIVERY', name: 'Сообщение доставлено без ошибок.', code: 0)
    SendLogDeliveryError.create(eng_name: 'ERROR_UNKNOWN', name: 'Ошибка неизвестна', code: -1)
    SendLogDeliveryError.create(eng_name: 'ERROR_UNKNOWN_SUBSCRIBER', name: 'Номера адресата нет в HLR. Сообщение не доставлено из-за отсутствия номера или не активной SIM-карты', code: 1)
    SendLogDeliveryError.create(eng_name: 'ERROR_UNIDENTIFIED_SUBSCRIBER', name: 'Номер адресата имеется в HLR, но не зарегистрирован на коммутаторе, на который HLR ссылается.', code: 5)
    SendLogDeliveryError.create(eng_name: 'ERROR_ABSENT_SUBSCRIBER_SM', name: 'В момент доставки сообщения абонент выпал из сети', code: 6)
    SendLogDeliveryError.create(eng_name: 'ERROR_ILLEGAL_SUBSCRIBER', name: 'Используется запрещенная подпись отправителя', code: 9)
    SendLogDeliveryError.create(eng_name: 'ERROR_TELESERVICE_NOT_PROVISIONED', name: 'Для адресата не активирована услуга обмена короткими текстовыми сообщениями', code: 11)
    SendLogDeliveryError.create(eng_name: 'ERROR_ILLEGAL_EQUIPMENT', name: 'Номер адресата недопустим для обмена короткими текстовыми сообщениями (оборудование)', code: 12)
    SendLogDeliveryError.create(eng_name: 'ERROR_CALL_BARRED', name: 'Абонент заблокирован или поставлена добровольная блокировка', code: 13)
    SendLogDeliveryError.create(eng_name: 'ERROR_FACILITY_NOT_SUPPORTED', name: 'Аппарат адресата не поддерживает обмен короткими текстовыми сообщениями', code: 21)
    SendLogDeliveryError.create(eng_name: 'ERROR_ABSENT_SUBSCRIBER', name: 'Адресат не может принять короткое текстовое сообщение, т.к. телефон, на который отправлено SMS, в момент доставки был выключен или находился вне зоны действия сети. Сообщение сохраняется в центре коротких сообщений и доставляется в момент новой регистрации абонента в сети.', code: 27)
    SendLogDeliveryError.create(eng_name: 'ERROR_INCOMPATIBLE_TERMINAL', name: 'Аппарат абонента не поддерживает сервис смс', code: 28)
    SendLogDeliveryError.create(eng_name: 'ERROR_SUBSCRIBER_BUSY_FOR_MT_SMS', name: 'В момент доставки адресат был занят приемом или передачей другого короткого сообщения', code: 31)
    SendLogDeliveryError.create(eng_name: 'ERROR_SM_DELIVERY_FAILURE', name: 'Ошибка при доставке например, нарушение протокола передачи в результате сбоев, помех, возможна переполнена память на SIM карте', code: 32)
    SendLogDeliveryError.create(eng_name: 'ERROR_MESSAGE_WAITING_LIST_FULL', name: 'Переполнена память аппарата для приема смс', code: 33)
    SendLogDeliveryError.create(eng_name: 'ERROR_SYSTEM_FAILURE', name: 'На коммутаторе оператора-получателя смс нет свободных каналов для отправки сообщений. SMS не доставлено из-за системной ошибки (неверный формат номера, номер не абонента стандарта GSM)', code: 34)
    SendLogDeliveryError.create(eng_name: 'ERROR_DATA_MISSING', name: 'Часть данных в процессе передачи смс была утеряна', code: 35)
    SendLogDeliveryError.create(eng_name: 'ERROR_UNEXPECTED_DATA_VALUE', name: 'Неожиданное значение данных (как правило возникает в результате не верно сформированной длины смс)', code: 36)
    SendLogDeliveryError.create(eng_name: 'ERROR_RESOURCE_LIMITATION', name: 'Ошибки USSD-сервиса', code: 51)
    SendLogDeliveryError.create(eng_name: 'ERROR_POSITION_METHOD_FAILURE', name: 'Ошибки USSD-сервиса', code: 54)
    SendLogDeliveryError.create(eng_name: 'ERROR_UNKNOWN_ALPHABET', name: 'Ошибки USSD-сервиса', code: 71)
    SendLogDeliveryError.create(eng_name: 'ERROR_USSD_BUSY', name: 'Ошибки USSD-сервиса', code: 72)
    SendLogDeliveryError.create(eng_name: 'HLR_TIMEOUT', name: 'Нет ответа от HLR - неправильный номер, некорректная настройка на MSC оператора или роумингового партнера', code: 201)
    SendLogDeliveryError.create(eng_name: 'VLR_TIMEOUT', name: 'Нет ответа от VLR - неправильный номер, некорректная настройка на MSC оператора или роумингового партнера, либо абонент покинул зону действия указанного MSC', code: 202)
    SendLogDeliveryError.create(eng_name: 'NO_SERVER_LINK_ROUTE', name: 'Нет маршрута для серверверного линка', code: 3840)
    SendLogDeliveryError.create(eng_name: 'SPAM_FILTER_ACTION', name: 'Действие СПАМ-фильтров комплекса', code: 3841)
    SendLogDeliveryError.create(eng_name: 'LIMIT_EXCEEDED', name: 'Превышение ежемесячного лимита смс трафика', code: 3842)
  end

  def down
    SendLogDeliveryError.delete_all
  end
end
