# -*- encoding : utf-8 -*-
class AddYetAnotherTenDemoUsers < ActiveRecord::Migration
  def up
    passwords = %w{not_password gXspGo3b R73086b5 UTAcN57K pno8Fejq 0X15AAnX HtGbJc63 6sGg5Xsr P9PA1R4m W543aF1J xcWh40Xi}

    (1..10).each do |i|
      y = i + 10
      user = User.create!({
                       :email => "demo#{y}@youct.ru",
                       :fio => "Демо пользователь #{y}",
                       :description => "Демо пользователь #{y}",
                       :password => passwords[i],
                       :password_confirmation => passwords[i],
                       :demo => true
                   })
      Payment.create!(:user_id => user.id, :amount => 2.5, :service => 'manual', :date => Time.now, :status => 'success', :approved => Time.now)
    end

  end

  def down
  end
end
