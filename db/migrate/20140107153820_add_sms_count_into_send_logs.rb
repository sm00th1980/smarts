# -*- encoding : utf-8 -*-
class AddSmsCountIntoSendLogs < ActiveRecord::Migration
  def change
    add_column :send_logs, :sms_count, :integer, default: 1, null: false
  end
end
