# -*- encoding : utf-8 -*-
class RenameApprovedIntoPayemnts < ActiveRecord::Migration
  def up
    rename_column :payments, :approved, :approved_at
  end

  def down
    rename_column :payments, :approved_at, :approved
  end
end
