# -*- encoding : utf-8 -*-
class RenameSendLogStatusUnknown < ActiveRecord::Migration
  def up
    status = SendLogDeliveryStatus.find_by_kannel_id(-1)
    status.name = 'Сообщение отослано. Статус доставки неизвестен. Не пришло уведомление от СМС центра о доставке, либо уведомление не распознано.'
    status.save!
  end

  def down
    status = SendLogDeliveryStatus.find_by_kannel_id(-1)
    status.name = 'Сообщение отослано. Статус доставки неизвестен. Не пришло уведомление от СМС центра о доставке.'
    status.save!
  end
end
