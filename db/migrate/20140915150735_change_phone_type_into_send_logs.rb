# -*- encoding : utf-8 -*-
class ChangePhoneTypeIntoSendLogs < ActiveRecord::Migration
  def up
    ActiveRecord::Base.connection.execute("alter table send_logs alter column phone type bigint using phone::bigint")
  end

  def down
  end
end
