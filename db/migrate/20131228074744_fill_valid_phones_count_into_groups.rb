# -*- encoding : utf-8 -*-
class FillValidPhonesCountIntoGroups < ActiveRecord::Migration
  def up
    Group.all.each do |group|
      group.valid_phones_count = group.clients_with_valid_phone_count
      group.save!
    end
  end

  def down
    ActiveRecord::Base.connection.execute("update groups set valid_phones_count = null")
  end
end
