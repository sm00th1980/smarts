# -*- encoding : utf-8 -*-
class AddResultsIntoSendLogs < ActiveRecord::Migration
  def up
    add_column :send_logs, :results, :string_array
    remove_column :logs, :results
  end

  def down
    remove_column :send_logs, :results
    add_column :logs, :results, :text
  end
end
