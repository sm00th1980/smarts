# -*- encoding : utf-8 -*-
class AddIndexIntoBalances < ActiveRecord::Migration
  def change
    add_index :balances, [:user_id, :period], unique: true
  end
end
