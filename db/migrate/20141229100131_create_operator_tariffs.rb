# -*- encoding : utf-8 -*-
class CreateOperatorTariffs < ActiveRecord::Migration
  def change
    create_table :operator_tariffs do |t|
      t.integer :operator_id, null: false
      t.decimal :price_per_sms, precision: 15, scale: 2, null: false
      t.date :begin_date, null: false
      t.date :end_date

      t.timestamps
    end
  end
end
