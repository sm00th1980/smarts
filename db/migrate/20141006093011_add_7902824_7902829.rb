# -*- encoding : utf-8 -*-
class Add79028247902829 < ActiveRecord::Migration
  def up
    old = CellOperatorPhoneRange.find_by_begin_phone_and_end_phone(79028240000, 79028249999)
    old.cell_operator = CellOperator.find_by_name('УралСвязьИнформ')
    old.save!

    old = CellOperatorPhoneRange.find_by_begin_phone_and_end_phone(79028250000, 79028259999)
    old.cell_operator = CellOperator.find_by_name('УралСвязьИнформ')
    old.save!

    old = CellOperatorPhoneRange.find_by_begin_phone_and_end_phone(79028260000, 79028279999)
    old.cell_operator = CellOperator.find_by_name('УралСвязьИнформ')
    old.save!

    old = CellOperatorPhoneRange.find_by_begin_phone_and_end_phone(79028280000, 79028289999)
    old.cell_operator = CellOperator.find_by_name('УралСвязьИнформ')
    old.save!

    old = CellOperatorPhoneRange.find_by_begin_phone_and_end_phone(79028290000, 79028299999)
    old.cell_operator = CellOperator.find_by_name('УралСвязьИнформ')
    old.save!
  end

  def down
  end
end
