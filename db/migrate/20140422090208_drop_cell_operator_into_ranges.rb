# -*- encoding : utf-8 -*-
class DropCellOperatorIntoRanges < ActiveRecord::Migration
  def up
    remove_column :cell_operator_phone_ranges, :cell_operator_text
  end

  def down
    add_column :cell_operator_phone_ranges, :cell_operator_text, :text
  end
end
