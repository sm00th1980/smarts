# -*- encoding : utf-8 -*-
class FillUserStatuses < ActiveRecord::Migration
  def up
    UserStatus.create!(name: 'Активный', internal_name: :active)
    UserStatus.create!(name: 'Заблокированный', internal_name: :blocked)
    UserStatus.create!(name: 'Удалённый', internal_name: :deleted)
  end

  def down
    UserStatus.destroy_all
  end
end
