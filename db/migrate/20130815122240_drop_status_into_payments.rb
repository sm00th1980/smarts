# -*- encoding : utf-8 -*-
class DropStatusIntoPayments < ActiveRecord::Migration
  def up
    remove_column :payments, :status
  end

  def down
  end
end
