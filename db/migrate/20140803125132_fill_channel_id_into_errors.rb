# -*- encoding : utf-8 -*-
class FillChannelIdIntoErrors < ActiveRecord::Migration
  def up
    SendLogDeliveryError.all.each do |error|
      error.channel_id = Channel.bronze.id
      error.save!
    end

    change_column :send_log_delivery_errors, :channel_id, :integer, null: false
  end

  def down
  end
end
