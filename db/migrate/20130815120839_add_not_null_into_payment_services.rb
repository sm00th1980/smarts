# -*- encoding : utf-8 -*-
class AddNotNullIntoPaymentServices < ActiveRecord::Migration
  def up
    change_column :payment_services, :name, :string, null: false
    change_column :payment_services, :internal_name, :string, null: false
  end

  def down
    change_column :payment_services, :name, :string, null: true
    change_column :payment_services, :internal_name, :string, null: true
  end
end
