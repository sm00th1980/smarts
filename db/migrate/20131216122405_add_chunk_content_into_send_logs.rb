# -*- encoding : utf-8 -*-
class AddChunkContentIntoSendLogs < ActiveRecord::Migration
  def change
    add_column :send_logs, :chunk_content, :text
  end
end
