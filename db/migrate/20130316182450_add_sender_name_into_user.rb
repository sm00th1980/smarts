# -*- encoding : utf-8 -*-
class AddSenderNameIntoUser < ActiveRecord::Migration
  def change
    add_column :users, :sender_name, :string
  end
end
