# -*- encoding : utf-8 -*-
class AddPaymentToDemoUser < ActiveRecord::Migration
  def up
    Payment.create!(:user_id => User.demo.id, :amount => 2, :service => 'manual', :date => Time.now).approve!
  end

  def down
    Payment.where(:user_id => User.demo.id).delete_all
  end
end
