# -*- encoding : utf-8 -*-
class RenameSkipNightIntoUsers < ActiveRecord::Migration
  def change
    rename_column :users, :skip_night, :permit_night_delivery
  end
end
