# -*- encoding : utf-8 -*-
class ChangeStatusId < ActiveRecord::Migration
  def up
    SenderName.all.each do |sender_name|
      if sender_name.status_id == 4
        sender_name.update_attribute(:status_id, SenderNameStatus.accepted.id)
      end
    end
  end

  def down
  end
end
