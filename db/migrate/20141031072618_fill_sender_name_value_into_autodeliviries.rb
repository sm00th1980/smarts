# -*- encoding : utf-8 -*-
class FillSenderNameValueIntoAutodeliviries < ActiveRecord::Migration
  def up
    AutoDelivery.find_each do |autodelivery|
      autodelivery.sender_name_value = autodelivery.user.active_sender_name_value
      autodelivery.save!
    end
  end

  def down
  end
end
