# -*- encoding : utf-8 -*-
class FillUserTypes < ActiveRecord::Migration
  def up
    UserType.create!(:name => 'Демо', :internal_name => 'demo')
    UserType.create!(:name => 'Служебный', :internal_name => 'operator')
    UserType.create!(:name => 'Админ', :internal_name => 'admin')
    UserType.create!(:name => 'Коммерческий', :internal_name => 'business')
  end

  def down
    UserType.delete_all
  end
end
