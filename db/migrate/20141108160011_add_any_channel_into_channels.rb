# -*- encoding : utf-8 -*-
class AddAnyChannelIntoChannels < ActiveRecord::Migration
  def up
    Channel.create!(name: 'Любой', internal_name: :any)
  end

  def down
  end
end
