# -*- encoding : utf-8 -*-
class CreateChannelsContent < ActiveRecord::Migration
  def up
    Channel.gold
    Channel.bronze
  end

  def down
    Channel.delete_all
  end
end
