# -*- encoding : utf-8 -*-
class AddIndexToSendLogs < ActiveRecord::Migration
  def change
    add_index :send_logs, :log_id
    add_index :send_logs, :created_at
  end
end
