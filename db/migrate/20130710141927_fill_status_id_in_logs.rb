# -*- encoding : utf-8 -*-
class FillStatusIdInLogs < ActiveRecord::Migration
  def up
    Log.all.each do |log|
      log.status = LogStatus.completed
      log.save!
    end
  end

  def down
    Log.all.each do |log|
      log.status = nil
      log.save!
    end
  end
end
