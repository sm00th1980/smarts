# -*- encoding : utf-8 -*-
class AddNotNullForUserIdIntoAutodeliveries < ActiveRecord::Migration
  def up
    change_column :auto_deliveries, :user_id, :integer, :null => false
  end

  def down
    change_column :auto_deliveries, :user_id, :integer, :null => true
  end
end
