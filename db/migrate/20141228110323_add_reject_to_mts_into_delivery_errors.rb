# -*- encoding : utf-8 -*-
class AddRejectToMtsIntoDeliveryErrors < ActiveRecord::Migration
  def up
    SendLogDeliveryError.create!(name: 'Рассылка на МТС запрещена.', eng_name: 'REJECT_TO_MTS', code: '-9', channel: Channel.any)
  end

  def down
    SendLogDeliveryError::Any.reject_to_mts.delete
  end
end
