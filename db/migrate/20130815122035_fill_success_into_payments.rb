# -*- encoding : utf-8 -*-
class FillSuccessIntoPayments < ActiveRecord::Migration
  def up
    Payment.all.each do |payment|
      payment.update_attribute(:success, true) if payment.status == 'success'
    end
  end

  def down
    Payment.all.each do |payment|
      payment.update_attribute(:success, false)
    end
  end
end
