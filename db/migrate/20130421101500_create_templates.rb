# -*- encoding : utf-8 -*-
class CreateTemplates < ActiveRecord::Migration
  def change
    create_table :templates do |t|
      t.text :text
      t.integer :user_id

      t.timestamps
    end
  end
end
