# -*- encoding : utf-8 -*-
class ChangeDefaultValueForPermitNightDelivery < ActiveRecord::Migration
  def up
    change_column :users, :permit_night_delivery, :boolean, :default => false
  end

  def down
    change_column :users, :permit_night_delivery, :boolean, :default => true
  end
end
