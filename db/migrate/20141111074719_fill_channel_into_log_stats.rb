# -*- encoding : utf-8 -*-
class FillChannelIntoLogStats < ActiveRecord::Migration
  def up

    gold = [
        CellOperatorGroup.mts,
        CellOperatorGroup.beeline,
        CellOperatorGroup.megafon,
        CellOperatorGroup.tele2,
        CellOperatorGroup.baikal,
        CellOperatorGroup.ural,
        CellOperatorGroup.enisey,
        CellOperatorGroup.cdma,
        CellOperatorGroup.motiv,
        CellOperatorGroup.nss,
        CellOperatorGroup.other
    ]

    bronze = [CellOperatorGroup.smarts]

    ActiveRecord::Base.connection.execute("update log_stats set channel_id = %s where cell_operator_group_id in (%s)" % [Channel.gold.id, gold.map{|c| c.id}.join(',')])
    ActiveRecord::Base.connection.execute("update log_stats set channel_id = %s where cell_operator_group_id in (%s)" % [Channel.bronze.id, bronze.map{|c| c.id}.join(',')])
  end

  def down
  end
end
