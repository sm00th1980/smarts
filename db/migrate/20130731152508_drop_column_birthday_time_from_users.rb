# -*- encoding : utf-8 -*-
class DropColumnBirthdayTimeFromUsers < ActiveRecord::Migration
  def up
    remove_column :users, :birthday_time
  end

  def down
  end
end
