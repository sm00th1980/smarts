# -*- encoding : utf-8 -*-
class AddNotNullToPeriod < ActiveRecord::Migration
  def up
    change_column :auto_deliveries, :period_id, :integer, :null => false
  end

  def down
    change_column :auto_deliveries, :period_id, :integer, :null => true
  end
end
