# -*- encoding : utf-8 -*-
class DropPricePerSmsFromCellOperatorGroups < ActiveRecord::Migration
  def up
    remove_column :cell_operator_groups, :price_per_sms
  end

  def down
  end
end
