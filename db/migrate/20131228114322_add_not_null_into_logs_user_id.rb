# -*- encoding : utf-8 -*-
class AddNotNullIntoLogsUserId < ActiveRecord::Migration
  def up
    change_column :logs, :user_id, :integer, null: false
  end

  def down
    change_column :logs, :user_id, :integer, null: true
  end
end
