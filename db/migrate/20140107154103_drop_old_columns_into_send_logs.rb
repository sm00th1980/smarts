# -*- encoding : utf-8 -*-
class DropOldColumnsIntoSendLogs < ActiveRecord::Migration
  def up
    remove_column :send_logs, :message_id
    remove_column :send_logs, :chunk_content
  end

  def down
  end
end
