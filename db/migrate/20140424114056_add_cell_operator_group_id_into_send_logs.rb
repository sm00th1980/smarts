# -*- encoding : utf-8 -*-
class AddCellOperatorGroupIdIntoSendLogs < ActiveRecord::Migration
  def change
    add_column :send_logs, :cell_operator_group_id, :integer
  end
end
