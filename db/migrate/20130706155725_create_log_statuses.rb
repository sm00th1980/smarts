# -*- encoding : utf-8 -*-
class CreateLogStatuses < ActiveRecord::Migration
  def up
    create_table :log_statuses do |t|
      t.text :name
      t.text :internal_name

      t.timestamps
    end

    LogStatus.create!(:name => 'В работе', :internal_name => 'in_work')
    LogStatus.create!(:name => 'Завершено', :internal_name => 'completed')
  end

  def down
    drop_table :log_statuses
  end
end
