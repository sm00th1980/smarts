# -*- encoding : utf-8 -*-
class AddDefaultSenderName < ActiveRecord::Migration
  def change
    add_column :sender_names, :default, :boolean, :default => false
  end
end
