# -*- encoding : utf-8 -*-
class AddStartedAtIntoLogs < ActiveRecord::Migration
  def change
    add_column :logs, :started_at, :timestamp
    add_column :logs, :finished_at, :timestamp
  end
end
