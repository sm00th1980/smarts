# -*- encoding : utf-8 -*-
class AddDemoUsers < ActiveRecord::Migration
  def up
    User.where(:demo => true).delete_all

    passwords = %w{not_password yCQfEVuu yuzR5UGy gan6RTPZ GxUhndy5 GAKJBRaK sVeqGDq4 QahpWU83 jdBsgwQ7 5nHY5ybH YbcX6tEs}

    (1..10).each do |i|
      user = User.create!({
                       :email => "demo#{i}@youct.ru",
                       :fio => "Демо пользователь #{i}",
                       :description => "Демо пользователь #{i}",
                       :password => passwords[i],
                       :password_confirmation => passwords[i],
                       :demo => true
                   })
      Payment.create!(:user_id => user.id, :amount => 2, :service => 'manual', :date => Time.now, :status => 'success', :approved => Time.now)
    end

  end

  def down
    Payment.where(:user_id => User.where(:demo => true)).delete_all
    User.where(:demo => true).delete_all
  end
end
