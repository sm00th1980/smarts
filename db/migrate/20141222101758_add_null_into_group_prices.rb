# -*- encoding : utf-8 -*-
class AddNullIntoGroupPrices < ActiveRecord::Migration
  def up
    remove_column :groups, :gold_price_per_sms
    remove_column :groups, :bronze_price_per_sms

    add_column :groups, :gold_price_per_sms, :decimal, :precision => 15, :scale => 2, null: true
    add_column :groups, :bronze_price_per_sms, :decimal, :precision => 15, :scale => 2, null: true
  end

  def down
  end
end
