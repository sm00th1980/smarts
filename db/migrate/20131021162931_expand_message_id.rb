# -*- encoding : utf-8 -*-
class ExpandMessageId < ActiveRecord::Migration
  def up
    execute <<-SQL
      ALTER TABLE send_logs
      ALTER COLUMN message_id TYPE bigint USING message_id::bigint
    SQL
  end

  def down
    execute <<-SQL
      ALTER TABLE send_logs
      ALTER COLUMN message_id TYPE int USING message_id::int
    SQL
  end
end
