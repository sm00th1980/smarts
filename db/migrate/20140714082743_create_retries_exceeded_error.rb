# -*- encoding : utf-8 -*-
class CreateRetriesExceededError < ActiveRecord::Migration
  def up
    SendLogDeliveryError.retries_exceeded
  end

  def down
  end
end
