# -*- encoding : utf-8 -*-
class DropValidPhoneFromClients < ActiveRecord::Migration
  def up
    remove_column :clients, :valid_phone
  end

  def down
    add_column :clients, :valid_phone, :boolean, default: false
  end
end
