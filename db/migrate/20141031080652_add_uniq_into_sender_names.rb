# -*- encoding : utf-8 -*-
class AddUniqIntoSenderNames < ActiveRecord::Migration
  def change
    SenderName.find_each do |sender_name|
      _count = SenderName.where(value: sender_name.value, user_id: sender_name.user).count
      if _count > 1

        user_id = sender_name.user_id
        status_id = sender_name.status_id
        value = sender_name.value
        active = sender_name.active

        SenderName.where(value: value, user_id: user_id).delete_all
        SenderName.create!(value: value, user_id: user_id, status_id: status_id, active: active)
      end
    end

    add_index :sender_names, [:value, :user_id], unique: true
  end
end
