# -*- encoding : utf-8 -*-
class FillActive < ActiveRecord::Migration
  def up
    SenderName.all.each do |sender_name|
      if sender_name.status_id == 4
        sender_name.update_attribute(:active, true)
      end
    end
  end

  def down
  end
end
