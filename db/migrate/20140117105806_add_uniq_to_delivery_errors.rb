# -*- encoding : utf-8 -*-
class AddUniqToDeliveryErrors < ActiveRecord::Migration
  def change
    add_index :send_log_delivery_errors, [:code], :unique => true
  end
end
