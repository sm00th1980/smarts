# -*- encoding : utf-8 -*-
class CreateAutoDeliveries < ActiveRecord::Migration
  def change
    create_table :auto_deliveries do |t|
      t.string  :name
      t.text    :description
      t.boolean :active
      t.date    :start_date
      t.date    :stop_date
      t.integer :user_id
      t.text    :client_ids
      t.text    :group_ids
      t.text    :template

      t.timestamps
    end
  end
end
