# -*- encoding : utf-8 -*-
class CreateTariffs < ActiveRecord::Migration
  def change
    create_table :tariffs do |t|
      t.string  :name
      t.float   :price_per_sms
      t.integer :min_sms_count
      t.integer :max_sms_count

      t.timestamps
    end
  end
end
