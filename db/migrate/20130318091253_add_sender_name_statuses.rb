# -*- encoding : utf-8 -*-
class AddSenderNameStatuses < ActiveRecord::Migration
  def up
    SenderNameStatus.create!(:name => 'на проверке',           :internal_name => 'moderating')
    SenderNameStatus.create!(:name => 'проверено и одобрено',  :internal_name => 'valid')
    SenderNameStatus.create!(:name => 'проверено и отклонено', :internal_name => 'invalid')
    SenderNameStatus.create!(:name => 'задействовано',         :internal_name => 'active')
  end

  def down
    SenderNameStatus.delete_all
  end
end
