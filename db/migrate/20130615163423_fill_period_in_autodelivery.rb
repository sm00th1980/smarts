# -*- encoding : utf-8 -*-
class FillPeriodInAutodelivery < ActiveRecord::Migration
  def up
    period_every_day = AutoDeliveryPeriod.find_by_period('every_day')
    AutoDelivery.all.each do |delivery|
      delivery.period_id = period_every_day.id
      delivery.time = '10:00'

      delivery.save!
    end
  end

  def down
    AutoDelivery.all.each do |delivery|
      delivery.period_id = nil
      delivery.time = nil

      delivery.save!
    end
  end
end
