# -*- encoding : utf-8 -*-
class CreateWeekDays < ActiveRecord::Migration
  def up
    create_table :week_days do |t|
      t.string :name
      t.string :eng_name

      t.timestamps
    end

    WeekDay.create!(:name => 'Понедельник', :eng_name => :monday)
    WeekDay.create!(:name => 'Вторник',     :eng_name => :tuesday)
    WeekDay.create!(:name => 'Среда',       :eng_name => :wednesday)
    WeekDay.create!(:name => 'Четверг',     :eng_name => :thursday)
    WeekDay.create!(:name => 'Пятница',     :eng_name => :friday)
    WeekDay.create!(:name => 'Суббота',     :eng_name => :saturday)
    WeekDay.create!(:name => 'Воскресенье', :eng_name => :sunday)
  end

  def down
    drop_table :week_days
  end
end
