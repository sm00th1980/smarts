# -*- encoding : utf-8 -*-
class CreateUserStatuses < ActiveRecord::Migration
  def change
    create_table :user_statuses do |t|
      t.string :name, null: false
      t.string :internal_name, null: false

      t.timestamps null: false
    end
  end
end
