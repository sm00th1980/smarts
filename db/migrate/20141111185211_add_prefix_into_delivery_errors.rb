# -*- encoding : utf-8 -*-
class AddPrefixIntoDeliveryErrors < ActiveRecord::Migration
  def up
    SendLogDeliveryError.all.each do |delivery_error|

      if delivery_error.code.size == 1
        delivery_error.code = "00#{delivery_error.code}"
        delivery_error.save!
      end

      if delivery_error.code.size == 2 and delivery_error.code[0] != '-'
        delivery_error.code = "0#{delivery_error.code}"
        delivery_error.save!
      end

    end
  end

  def down
  end
end
