# -*- encoding : utf-8 -*-
class CreateLogStatMonths < ActiveRecord::Migration
  def up
    drop_table :log_stats

    first_date = Log.select('min(date(created_at)) as min').first[:min].to_date
    last_date = Log.select('max(date(created_at)) as max').first[:max].to_date

    dates = (first_date..last_date).map { |d| d.at_beginning_of_month }.uniq.sort

    dates.each { |date| LogStatManager.recreate_month_view(date) }
    LogStatManager.recreate_log_view
  end

  def down
  end

end
