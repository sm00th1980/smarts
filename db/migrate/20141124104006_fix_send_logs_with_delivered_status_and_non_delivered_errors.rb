# -*- encoding : utf-8 -*-
class FixSendLogsWithDeliveredStatusAndNonDeliveredErrors < ActiveRecord::Migration
  def up
    SendLog.
        where('date(created_at) >= ?', Date.parse('2014-11-01')).
        where(delivery_status_id: SendLogDeliveryStatus.delivered).
        where('delivery_error_id not in (?,?,?)', SendLogDeliveryError.gold_no_error.id, SendLogDeliveryError.bronze_no_error.id, SendLogDeliveryError::Any.success_delivered.id).find_each do |send_log|

      send_log.delivery_error_id = SendLogDeliveryError.gold_no_error.id if send_log.channel.gold?
      send_log.delivery_error_id = SendLogDeliveryError.bronze_no_error.id if send_log.channel.bronze?
      send_log.delivery_error_id = SendLogDeliveryError::Any.success_delivered.id if not send_log.channel.bronze? and not send_log.channel.gold?

      send_log.save!
    end

  end

  def down
  end
end
