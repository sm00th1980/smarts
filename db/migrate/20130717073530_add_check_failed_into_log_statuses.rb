# -*- encoding : utf-8 -*-
class AddCheckFailedIntoLogStatuses < ActiveRecord::Migration
  def up
    LogStatus.create!(name: 'Ошибка при проверке', internal_name: 'check_failed')
  end

  def down
    LogStatus.find_by_internal_name('check_failed').delete
  end
end
