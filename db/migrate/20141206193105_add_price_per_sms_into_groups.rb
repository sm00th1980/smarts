# -*- encoding : utf-8 -*-
class AddPricePerSmsIntoGroups < ActiveRecord::Migration
  def change
    add_column :groups, :gold_price_per_sms, :decimal, :precision => 15, :scale => 2, default: 0, null: false
    add_column :groups, :bronze_price_per_sms, :decimal, :precision => 15, :scale => 2, default: 0, null: false
  end
end
