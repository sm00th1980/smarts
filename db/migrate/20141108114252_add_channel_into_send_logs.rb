# -*- encoding : utf-8 -*-
class AddChannelIntoSendLogs < ActiveRecord::Migration
  def change
    add_column :send_logs, :channel_id, :integer
  end
end
