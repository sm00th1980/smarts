# -*- encoding : utf-8 -*-
class AddContactEmailIntoUsers < ActiveRecord::Migration
  def change
    add_column :users, :contact_email, :string, null: true
  end
end
