# -*- encoding : utf-8 -*-
class AddClientIdIntoLogs < ActiveRecord::Migration
  def change
    add_column :logs, :client_id, :integer, :null => true
  end
end
