# -*- encoding : utf-8 -*-
class AddNotNullIntoUsersChargeType < ActiveRecord::Migration
  def up
    change_column :users, :charge_type_id, :integer, null: false
  end

  def down
    change_column :users, :charge_type_id, :integer, null: true
  end
end
