# -*- encoding : utf-8 -*-
class AddSenderNameIdIntoUsers < ActiveRecord::Migration
  def up
    add_column :users, :sender_name_id, :integer
    remove_column :users, :sender_name
  end

  def down
    remove_column :users, :sender_name_id
    add_column :users, :sender_name, :string
  end
end
