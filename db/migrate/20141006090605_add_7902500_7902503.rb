# -*- encoding : utf-8 -*-
class Add79025007902503 < ActiveRecord::Migration
  def up
    old = CellOperatorPhoneRange.find_by_begin_phone_and_end_phone(79025000000, 79025039999)
    old.cell_operator = CellOperator.find_by_name('УралСвязьИнформ')
    old.save!
  end

  def down
  end
end
