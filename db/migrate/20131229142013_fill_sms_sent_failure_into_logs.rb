# -*- encoding : utf-8 -*-
class FillSmsSentFailureIntoLogs < ActiveRecord::Migration
  def up
    Log.order(:id).each do |log|
      sms_sent_failure = SendLog.select("count(*) as sms_sent_failure").where('delivery_status_id <> ?', SendLogDeliveryStatus.delivered).where(log_id: log.id)[0].sms_sent_failure.to_i
      log.update_column(:sms_sent_failure, sms_sent_failure)
    end
  end

  def down
    ActiveRecord::Base.connection.execute('update logs set sms_sent_failure = 0')
  end
end
