# -*- encoding : utf-8 -*-
class MakeStatusIdNotNullIntoLogs < ActiveRecord::Migration
  def up
    change_column :logs, :status_id, :integer, null: false
  end

  def down
    change_column :logs, :status_id, :integer, null: true
  end
end
