# -*- encoding : utf-8 -*-
class AddDeletedAtIntoUsers < ActiveRecord::Migration
  def change
    add_column :users, :deleted_at, :timestamp
  end
end
