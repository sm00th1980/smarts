# -*- encoding : utf-8 -*-
class ChangeIntoIntoLogStats < ActiveRecord::Migration
  def up
    remove_index :log_stats, [:user_id, :date, :cell_operator_group_id]
    add_index :log_stats, [:user_id, :date, :cell_operator_group_id, :channel_id], name: :index_unique, :unique => true
  end

  def down
    remove_index :log_stats, [:user_id, :date, :cell_operator_group_id, :channel_id]
    add_index :log_stats, [:user_id, :date, :cell_operator_group_id], :unique => true
  end
end
