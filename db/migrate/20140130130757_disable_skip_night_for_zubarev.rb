# -*- encoding : utf-8 -*-
class DisableSkipNightForZubarev < ActiveRecord::Migration
  def up
    zubarev = User.find_by_email('zubarev@mail.ru')
    if zubarev
      zubarev.update_attribute(:skip_night, false)
    end
  end

  def down
    zubarev = User.find_by_email('zubarev@mail.ru')
    if zubarev
      zubarev.update_attribute(:skip_night, true)
    end
  end
end
