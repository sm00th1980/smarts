# -*- encoding : utf-8 -*-
class AddRejectToMegafonViaBronzeChannel < ActiveRecord::Migration
  def change
    add_column :users, :reject_to_megafon_via_gold_channel, :boolean, default: false
  end
end
