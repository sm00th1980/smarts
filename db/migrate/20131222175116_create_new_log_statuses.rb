# -*- encoding : utf-8 -*-
class CreateNewLogStatuses < ActiveRecord::Migration
  def up
    LogStatus.checking
    LogStatus.ready_for_processing
    LogStatus.processing
    LogStatus.check_failed
  end

  def down
    LogStatus.checking.destroy
    LogStatus.ready_for_processing.destroy
    LogStatus.processing.destroy
    LogStatus.check_failed.destroy
  end
end
