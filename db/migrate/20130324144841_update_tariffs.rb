# -*- encoding : utf-8 -*-
class UpdateTariffs < ActiveRecord::Migration
  def up
    tariff = Tariff.find_by_name('Базовый')
    if tariff
      tariff.price_per_sms=0.25
      tariff.min_sms_count=1
      tariff.max_sms_count=5000
      tariff.save!
    end

    tariff = Tariff.find_by_name('Средний')
    if tariff
      tariff.price_per_sms=0.2
      tariff.min_sms_count=5001
      tariff.max_sms_count=20000
      tariff.save!
    end

    tariff = Tariff.find_by_name('Крупный')
    if tariff
      tariff.price_per_sms=0.15
      tariff.min_sms_count=20001
      tariff.max_sms_count=50000
      tariff.save!
    end

    tariff = Tariff.find_by_name('Гигантский')
    if tariff
      tariff.price_per_sms=0.12
      tariff.min_sms_count=50001
      tariff.max_sms_count=100000
      tariff.save!
    end

    Tariff.create!(:name => 'Превосходящий', :price_per_sms => 0.1,  :min_sms_count => 100001,  :max_sms_count => 500000)
    Tariff.create!(:name => 'Ошеломляющий',  :price_per_sms => 0.08, :min_sms_count => 500001,  :max_sms_count => 1000000)
    #Tariff.create!(:name => 'Невероятный',   :price_per_sms => 0,    :min_sms_count => 1000001, :max_sms_count => 0)
  end

  def down
    Tariff.delete_all

    Tariff.create!(:name => 'Базовый',    :price_per_sms => 50, :min_sms_count => 0,      :max_sms_count => 1000)
    Tariff.create!(:name => 'Средний',    :price_per_sms => 20, :min_sms_count => 1001,   :max_sms_count => 50000)
    Tariff.create!(:name => 'Крупный',    :price_per_sms => 10, :min_sms_count => 50001,  :max_sms_count => 500000)
    Tariff.create!(:name => 'Гигантский', :price_per_sms => 8,  :min_sms_count => 500001, :max_sms_count => 1000000)
  end
end
