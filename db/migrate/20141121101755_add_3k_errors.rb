# -*- encoding : utf-8 -*-
class Add3kErrors < ActiveRecord::Migration
  def up
    SendLogDeliveryError.gold_no_server_link_route
    SendLogDeliveryError.gold_spam_filter_action
    SendLogDeliveryError.gold_limit_exceeded
  end


  def down
  end
end
