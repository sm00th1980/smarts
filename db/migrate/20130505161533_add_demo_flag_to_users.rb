# -*- encoding : utf-8 -*-
class AddDemoFlagToUsers < ActiveRecord::Migration
  def change
    add_column :users, :demo, :boolean, :default => false
  end
end
