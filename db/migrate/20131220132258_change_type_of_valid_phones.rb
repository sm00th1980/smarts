# -*- encoding : utf-8 -*-
class ChangeTypeOfValidPhones < ActiveRecord::Migration
  def up
    change_column :logs, :valid_phones, :text_array
  end

  def down
  end
end
