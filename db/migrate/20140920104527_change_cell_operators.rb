# -*- encoding : utf-8 -*-
class ChangeCellOperators < ActiveRecord::Migration
  def up
    CellOperator.find_by_name("ЕКАТЕРИНБУРГ-2000").update_attribute(:cell_operator_group_id, CellOperatorGroup.motiv.id)
    CellOperator.find_by_name("Нижегородская сотовая связь").update_attribute(:cell_operator_group_id, CellOperatorGroup.nss.id)
  end

  def down
  end
end
