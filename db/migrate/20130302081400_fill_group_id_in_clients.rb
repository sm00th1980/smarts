# -*- encoding : utf-8 -*-
class FillGroupIdInClients < ActiveRecord::Migration
  def up
    deyarov = Client.find(18)
    deyarov.group = Group.find_by_name('Группа1')
    deyarov.save!

    zagaynov = Client.find(20)
    zagaynov.group = Group.find_by_name('Группа2')
    zagaynov.save!

    egipko = Client.find(21)
    egipko.group = Group.find_by_name('Группа3')
    egipko.save!
  end

  def down
    deyarov = Client.find(18)
    deyarov.group_id = nil
    deyarov.save!

    zagaynov = Client.find(20)
    zagaynov.group_id = nil
    zagaynov.save!

    egipko = Client.find(21)
    egipko.group_id = nil
    egipko.save!
  end
end
