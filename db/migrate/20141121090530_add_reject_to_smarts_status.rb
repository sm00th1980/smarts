# -*- encoding : utf-8 -*-
class AddRejectToSmartsStatus < ActiveRecord::Migration

  def up
    SendLogDeliveryStatus.create!(name: 'Рассылка на Смартс запрещена.', internal_name: 'reject to smarts', kannel_id: -4)
  end

end
