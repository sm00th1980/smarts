# -*- encoding : utf-8 -*-
class AddRejectToMegafonIntoDeliveryErrors < ActiveRecord::Migration
  def up
    SendLogDeliveryError.create!(eng_name: 'REJECT_TO_MEGAFON_VIA_GOLD_CHANNEL', name: 'Рассылка на Мегафон через золотой канал запрещена', code: -8, channel: Channel.gold)
  end

  def down
  end
end
