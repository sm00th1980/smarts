# -*- encoding : utf-8 -*-
class RemoveDefaultSenderName < ActiveRecord::Migration
  def up
    SenderName.where(:default => true).delete_all
  end

  def down
  end
end
