# -*- encoding : utf-8 -*-
class AddKannelIdIntoDeliveryStatus < ActiveRecord::Migration
  def change
    add_column :send_log_delivery_statuses, :kannel_id, :integer
  end
end
