# -*- encoding : utf-8 -*-
class AddNotNullToChannelIntoLogStats < ActiveRecord::Migration
  def up
    change_column :log_stats, :channel_id, :integer, null: false
  end

  def down
    change_column :log_stats, :channel_id, :integer, null: true
  end
end
