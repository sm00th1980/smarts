# -*- encoding : utf-8 -*-
class RenameTemplateIntoAutodeliveries < ActiveRecord::Migration
  def change
    rename_column :auto_deliveries, :template, :content
  end
end
