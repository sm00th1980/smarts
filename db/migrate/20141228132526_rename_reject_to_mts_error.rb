# -*- encoding : utf-8 -*-
class RenameRejectToMtsError < ActiveRecord::Migration
  def up
    SendLogDeliveryError.find_by_eng_name_and_code_and_channel_id('REJECT_TO_MTS', '-9', Channel.any.id).update_attribute(:name, 'Рассылка на МТС запрещена')
  end

  def down
    SendLogDeliveryError.find_by_eng_name_and_code_and_channel_id('REJECT_TO_MTS', '-9', Channel.any.id).update_attribute(:name, 'Рассылка на МТС запрещена.')
  end
end
