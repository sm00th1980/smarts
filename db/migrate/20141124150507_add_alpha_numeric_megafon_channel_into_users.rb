# -*- encoding : utf-8 -*-
class AddAlphaNumericMegafonChannelIntoUsers < ActiveRecord::Migration
  def change
    add_column :users, :alpha_megafon_channel_id, :integer
  end
end
