# -*- encoding : utf-8 -*-
class AddNotNullIntoClients < ActiveRecord::Migration
  def up
    change_column :clients, :group_id, :integer, null: false
    change_column :clients, :valid_phone, :boolean, null: false, default: false
  end

  def down
    change_column :clients, :group_id, :integer, null: true
    change_column :clients, :valid_phone, :boolean, null: true
  end
end
