# -*- encoding : utf-8 -*-
class Add790800 < ActiveRecord::Migration
  def up
    old = CellOperatorPhoneRange.find_by_begin_phone_and_end_phone(79080000000, 79080079999)
    old.cell_operator = CellOperator.find_by_name('УралСвязьИнформ')
    old.end_phone = 79080099999
    old.save!

    CellOperatorPhoneRange.find_by_begin_phone_and_end_phone(79080090000, 79080099999).delete
  end

  def down
  end
end
