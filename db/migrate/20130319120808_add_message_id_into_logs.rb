# -*- encoding : utf-8 -*-
class AddMessageIdIntoLogs < ActiveRecord::Migration
  def change
    add_column :logs, :message_id, :integer
  end
end
