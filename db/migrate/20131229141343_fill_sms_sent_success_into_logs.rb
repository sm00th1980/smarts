# -*- encoding : utf-8 -*-
class FillSmsSentSuccessIntoLogs < ActiveRecord::Migration
  def up
    Log.order(:id).each do |log|
      sms_sent_success = SendLog.select("count(*) as sms_success").where(delivery_status_id: SendLogDeliveryStatus.delivered).where(log_id: log.id)[0].sms_success.to_i
      log.update_column(:sms_sent_success, sms_sent_success)
    end
  end

  def down
    ActiveRecord::Base.connection.execute("update logs set sms_sent_success = 0")
  end
end
