# -*- encoding : utf-8 -*-
class DropTableBalance < ActiveRecord::Migration
  def up
    drop_table :balances
  end

  def down
  end
end
