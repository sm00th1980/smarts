# -*- encoding : utf-8 -*-
class LoadDataIntoSendLogs < ActiveRecord::Migration
  def up
    Log.all.each do |log|
      SendLog.create!(
          :log_id => log.id,
          :phone => log.phone_b,
          :status => log.attributes_before_type_cast[:status],
          :price => log.price,
          :message_id => log.message_id,
          :response => log.response,
          :client_id => log.client_id
      )
    end
  end

  def down
    SendLog.delete_all
  end
end
