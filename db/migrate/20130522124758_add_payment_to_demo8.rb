# -*- encoding : utf-8 -*-
class AddPaymentToDemo8 < ActiveRecord::Migration
  def up
    Payment.create!(:user_id => User.find_by_email('demo8@youct.ru').id,  :amount => 28, :service => 'manual', :date => Time.now, :status => 'success', :approved => Time.now)
  end

  def down
  end
end
