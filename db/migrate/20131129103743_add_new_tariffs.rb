# -*- encoding : utf-8 -*-
class AddNewTariffs < ActiveRecord::Migration
  def up
    Tariff.create(name: 'Тариф 1', price_per_sms: 0.18)
    Tariff.create(name: 'Тариф 2', price_per_sms: 0.16)
    Tariff.create(name: 'Тариф 3', price_per_sms: 0.10)
  end

  def down
  end
end
