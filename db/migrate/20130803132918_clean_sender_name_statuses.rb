# -*- encoding : utf-8 -*-
class CleanSenderNameStatuses < ActiveRecord::Migration
  def up
    valid = SenderNameStatus.find_by_internal_name('valid')
    valid.update_attribute(:internal_name, 'accepted')
    valid.update_attribute(:name, 'проверено и одобрено')

    invalid = SenderNameStatus.find_by_internal_name('invalid')
    invalid.update_attribute(:internal_name, 'rejected')
    invalid.update_attribute(:name, 'проверено и отклонено')


    SenderNameStatus.all.each do |sender_name_status|
      if not [SenderNameStatus.moderating, SenderNameStatus.accepted, SenderNameStatus.rejected].include? sender_name_status
        sender_name_status.delete
      end
    end
  end

  def down
  end
end
