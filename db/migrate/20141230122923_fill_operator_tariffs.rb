# -*- encoding : utf-8 -*-
class FillOperatorTariffs < ActiveRecord::Migration
  def up
    OperatorTariff.delete_all
    begin_date = Date.parse('2014-12-01')

    OperatorTariff.create!(operator: CellOperatorGroup.mts,     begin_date: begin_date, end_date: nil, price_per_sms: 0.99)
    OperatorTariff.create!(operator: CellOperatorGroup.beeline, begin_date: begin_date, end_date: nil, price_per_sms: 0.47)
    OperatorTariff.create!(operator: CellOperatorGroup.megafon, begin_date: begin_date, end_date: nil, price_per_sms: 1.04)
    OperatorTariff.create!(operator: CellOperatorGroup.tele2,   begin_date: begin_date, end_date: nil, price_per_sms: 0.28)
    OperatorTariff.create!(operator: CellOperatorGroup.baikal,  begin_date: begin_date, end_date: nil, price_per_sms: 0.10)
    OperatorTariff.create!(operator: CellOperatorGroup.ural,    begin_date: begin_date, end_date: nil, price_per_sms: 0.33)
    OperatorTariff.create!(operator: CellOperatorGroup.enisey,  begin_date: begin_date, end_date: nil, price_per_sms: 0.31)
    OperatorTariff.create!(operator: CellOperatorGroup.cdma,    begin_date: begin_date, end_date: nil, price_per_sms: 1.14)
    OperatorTariff.create!(operator: CellOperatorGroup.other,   begin_date: begin_date, end_date: nil, price_per_sms: 0.09)
    OperatorTariff.create!(operator: CellOperatorGroup.smarts,  begin_date: begin_date, end_date: nil, price_per_sms: 1.00)
    OperatorTariff.create!(operator: CellOperatorGroup.motiv,   begin_date: begin_date, end_date: nil, price_per_sms: 0.49)
    OperatorTariff.create!(operator: CellOperatorGroup.nss,     begin_date: begin_date, end_date: nil, price_per_sms: 0.79)
  end

  def down
  end
end
