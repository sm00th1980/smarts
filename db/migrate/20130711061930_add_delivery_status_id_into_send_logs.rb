# -*- encoding : utf-8 -*-
class AddDeliveryStatusIdIntoSendLogs < ActiveRecord::Migration
  def up
    add_column :send_logs, :delivery_status_id, :integer
    remove_column :send_logs, :status
  end

  def down
    remove_column :send_logs, :delivery_status_id
    add_column :send_logs, :status, :text
  end
end
