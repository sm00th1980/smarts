# -*- encoding : utf-8 -*-
class CreateExpiredStatus < ActiveRecord::Migration
  def up
    SendLogDeliveryError.expired
  end

  def down
  end
end
