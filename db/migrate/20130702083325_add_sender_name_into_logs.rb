# -*- encoding : utf-8 -*-
class AddSenderNameIntoLogs < ActiveRecord::Migration
  def change
    add_column :logs, :sender_name, :text
  end
end
