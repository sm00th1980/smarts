# -*- encoding : utf-8 -*-
class AddCheckToPhoneIntoClients < ActiveRecord::Migration
  def up
    ActiveRecord::Base.connection.execute("ALTER TABLE clients ADD CHECK (phone >= %i and phone <= %i)" % [79000000000, 79999999999])
  end

  def down

  end
end
