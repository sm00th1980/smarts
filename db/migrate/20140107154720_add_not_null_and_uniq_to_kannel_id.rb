# -*- encoding : utf-8 -*-
class AddNotNullAndUniqToKannelId < ActiveRecord::Migration
  def change
    change_column :send_log_delivery_statuses, :kannel_id, :integer, null: false
    add_index :send_log_delivery_statuses, [:kannel_id], unique: true
  end
end
