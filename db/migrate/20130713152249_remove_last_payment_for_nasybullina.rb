# -*- encoding : utf-8 -*-
class RemoveLastPaymentForNasybullina < ActiveRecord::Migration
  def up
    payments = Payment.where(:user_id => User.find_by_email('s.nasybullina@sisamara.ru').id)
    if payments.size == 2
      payments.last.delete
    end
  end

  def down
    Payment.create!(:user_id => User.find_by_email('s.nasybullina@sisamara.ru').id,  :amount => 50.5, :service => 'manual', :date => Time.now, :status => 'success', :approved => Time.now)
  end
end
