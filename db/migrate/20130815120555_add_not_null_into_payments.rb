# -*- encoding : utf-8 -*-
class AddNotNullIntoPayments < ActiveRecord::Migration
  def up
    change_column :payments, :user_id, :integer, null: false
    change_column :payments, :service_id, :integer, null: false
  end

  def down
    change_column :payments, :user_id, :integer, null: true
    change_column :payments, :service_id, :integer, null: true
  end
end
