# -*- encoding : utf-8 -*-
class FillPermitNightDelivery < ActiveRecord::Migration
  def up
    User.all.each do |user|
      user.update_attribute(:permit_night_delivery, false)
    end

    zubarev = User.find_by_email('zubarev@mail.ru')
    if zubarev
      zubarev.update_attribute(:permit_night_delivery, true)
    end
  end

  def down
  end
end
