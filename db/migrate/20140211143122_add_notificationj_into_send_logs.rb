# -*- encoding : utf-8 -*-
class AddNotificationjIntoSendLogs < ActiveRecord::Migration
  def change
    add_column :send_logs, :notification, :text, null: true
  end
end
