# -*- encoding : utf-8 -*-
class AddBCodeIntoDeliveryErrors < ActiveRecord::Migration
  def up
    SendLogDeliveryError.create!(
        eng_name: 'ERROR_INVALID_DESTINATION_NUMBER',
        name: 'Заблокирован на основании глобального стоп листа.',
        code: '00b',
        channel: Channel.gold
    )
  end

  def down
  end
end
