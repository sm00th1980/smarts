# -*- encoding : utf-8 -*-
class FillGroupIdsIntoAutodeliveries < ActiveRecord::Migration
  def up
    AutoDelivery.all.each do |delivery|
      delivery.group_ids = delivery._group_ids.split(',') if delivery._group_ids
      delivery.save!
    end
  end

  def down
    AutoDelivery.all.each do |delivery|
      delivery.group_ids = []
      delivery.save!
    end
  end
end
