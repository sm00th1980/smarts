# -*- encoding : utf-8 -*-
class AddUniqIndexToMtsRanges < ActiveRecord::Migration
  def change
    add_index :mts_ranges, :begin_phone, unique: true
    add_index :mts_ranges, :end_phone, unique: true
    add_index :mts_ranges, [:begin_phone, :end_phone], unique: true
  end
end
