# -*- encoding : utf-8 -*-
class DropColumnSuccessIntoPayemnts < ActiveRecord::Migration
  def up
    remove_column :payments, :success
  end

  def down
    add_column :payments, :success, :boolean, default: false, null: false
  end
end
