# -*- encoding : utf-8 -*-
class CreateLogs < ActiveRecord::Migration
  def change
    create_table :logs do |t|
      t.integer :user_id
      t.integer :phone_b
      t.string  :status
      t.float   :price
      t.text    :content
      t.string  :direction

      t.timestamps
    end
  end
end
