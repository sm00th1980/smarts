# -*- encoding : utf-8 -*-
class ChangeUserTypeForNasybullina < ActiveRecord::Migration
  def up
    User.find_by_email('s.nasybullina@sisamara.ru').update_attribute(:demo, true)
  end

  def down
    User.find_by_email('s.nasybullina@sisamara.ru').update_attribute(:demo, false)
  end
end
