# -*- encoding : utf-8 -*-
class CreateCellOperatorPhoneRanges < ActiveRecord::Migration
  def change
    create_table :cell_operator_phone_ranges do |t|
      t.integer :cell_operator_id
      t.integer  :begin_phone, :limit => 8, null: false
      t.integer  :end_phone, :limit => 8, null: false
      t.text :region, null: false
      t.text :cell_operator_text, null: false

      t.timestamps
    end
  end
end
