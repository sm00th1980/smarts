# -*- encoding : utf-8 -*-
class AddLogStatusStoppped < ActiveRecord::Migration
  def up
    LogStatus.create(name: 'Остановлена', internal_name: 'stopped')
  end

  def down
    LogStatus.find_by_internal_name('stopped').destroy
  end
end
