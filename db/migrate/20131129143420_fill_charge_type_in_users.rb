# -*- encoding : utf-8 -*-
class FillChargeTypeInUsers < ActiveRecord::Migration
  def up
    User.all.each do |user|
      user.charge_type = ChargeType.by_sent
      user.save!
    end
  end

  def down
    User.all.each do |user|
      user.charge_type = nil
      user.save!
    end
  end
end
