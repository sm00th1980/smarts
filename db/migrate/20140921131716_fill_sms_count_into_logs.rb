# -*- encoding : utf-8 -*-
class FillSmsCountIntoLogs < ActiveRecord::Migration
  def up
    ActiveRecord::Base.connection.execute("update logs set sms_count = (select sms_count from send_logs where log_id = logs.id limit 1)")
  end

  def down
  end
end
