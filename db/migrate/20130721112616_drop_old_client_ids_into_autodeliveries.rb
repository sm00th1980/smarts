# -*- encoding : utf-8 -*-
class DropOldClientIdsIntoAutodeliveries < ActiveRecord::Migration
  def up
    remove_column :auto_deliveries, :_client_ids
  end

  def down
  end
end
