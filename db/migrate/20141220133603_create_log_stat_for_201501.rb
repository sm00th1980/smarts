# -*- encoding : utf-8 -*-
class CreateLogStatFor201501 < ActiveRecord::Migration
  def up
    Log.recalculate(Date.parse('2015-01-01'), Date.parse('2015-01-01'))
  end

  def down
  end
end
