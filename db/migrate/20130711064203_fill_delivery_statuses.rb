# -*- encoding : utf-8 -*-
class FillDeliveryStatuses < ActiveRecord::Migration
  def up
    SendLogDeliveryStatus.create!(name: 'Доставлено',            internal_name: 'delivered')
    SendLogDeliveryStatus.create!(name: 'Не доставлено',         internal_name: 'failed')
    SendLogDeliveryStatus.create!(name: 'Ошибка при доставке',   internal_name: 'error')
    SendLogDeliveryStatus.create!(name: 'В очереди на доставку', internal_name: 'wait')
    SendLogDeliveryStatus.create!(name: 'Ошибка при проверке',   internal_name: 'check_failed')
    SendLogDeliveryStatus.create!(name: 'Неизвестен',            internal_name: 'unknown')
  end

  def down
    SendLogDeliveryStatus.delete_all
  end
end
