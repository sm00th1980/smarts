# -*- encoding : utf-8 -*-
class Recalc201503WithNewMtsRanges < ActiveRecord::Migration
  def up
    begin_date = Date.parse('2015-03-01')
    end_date = Date.today
    channel = Channel.gold

    Log.where('date(created_at) >= ?', begin_date).where('price > 0').find_each do |log|
      log.send_logs.where(channel: channel).where('price > 0').find_each do |send_log|
        send_log.price = Message.calc_price_per_sms(channel, log.user, send_log.phone)
        send_log.cell_operator_group = CellOperatorGroup.get_by_phone(send_log.phone)

        send_log.save!
      end
    end

    Calculator::Logs.recalculate(begin_date, end_date)
  end
end
