# -*- encoding : utf-8 -*-
class UpdateIndexIntoRanges < ActiveRecord::Migration
  def up
    remove_index "cell_operator_phone_ranges", name: "index"
    add_index :cell_operator_phone_ranges, [:begin_phone, :end_phone], :name => "index", :unique => true
  end

  def down
  end
end


