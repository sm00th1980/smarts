# -*- encoding : utf-8 -*-
class CreateLogTypes < ActiveRecord::Migration
  def up
    create_table :log_types do |t|
      t.string :name
      t.string :internal_name

      t.timestamps
    end

    LogType.api
    LogType.manual
    LogType.autodelivery
  end

  def down
    drop_table :log_types
  end
end
