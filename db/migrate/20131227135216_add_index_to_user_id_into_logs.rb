# -*- encoding : utf-8 -*-
class AddIndexToUserIdIntoLogs < ActiveRecord::Migration
  def change
    add_index :logs, [:user_id]
  end
end
