# -*- encoding : utf-8 -*-
class DropGroupIdsIntoAutodeliveries < ActiveRecord::Migration
  def up
    remove_column :auto_deliveries, :_group_ids
  end

  def down
  end
end
