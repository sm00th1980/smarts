# -*- encoding : utf-8 -*-
class AddLogTypeIdIntoLogs < ActiveRecord::Migration
  def change
    add_column :logs, :log_type_id, :integer
  end
end
