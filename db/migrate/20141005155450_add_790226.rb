# -*- encoding : utf-8 -*-
class Add790226 < ActiveRecord::Migration
  def up
    old = CellOperatorPhoneRange.find_by_begin_phone_and_end_phone(79022530000, 79022799999)
    old.end_phone = 79022599999
    old.save!

    ural = CellOperator.find_by_name('УралСвязьИнформ')
    CellOperatorPhoneRange.create!(begin_phone: 79022600000, end_phone: 79022699999, cell_operator: ural, region: 'Свердловская область')
  end

  def down
  end
end
