# -*- encoding : utf-8 -*-
class FillCurrentBalances < ActiveRecord::Migration
  def up
    Balance.fill_prev_period
    Balance.fill_current_period
  end

  def down
  end
end
