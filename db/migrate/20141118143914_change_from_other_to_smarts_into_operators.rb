# -*- encoding : utf-8 -*-
class ChangeFromOtherToSmartsIntoOperators < ActiveRecord::Migration
  def up
    CellOperator.find_by_name('Средневолжская межрегиональная ассоциация радиотелекоммуникационных систем').update_attribute(:cell_operator_group_id, CellOperatorGroup.smarts.id)
    CellOperator.find_by_name('Астрахань GSM').update_attribute(:cell_operator_group_id, CellOperatorGroup.smarts.id)
    CellOperator.find_by_name('СМАРТС-Иваново').update_attribute(:cell_operator_group_id, CellOperatorGroup.smarts.id)
    CellOperator.find_by_name('Пенза-GSM').update_attribute(:cell_operator_group_id, CellOperatorGroup.smarts.id)
    CellOperator.find_by_name('СМАРТС-Уфа').update_attribute(:cell_operator_group_id, CellOperatorGroup.smarts.id)
    CellOperator.find_by_name('Шупашкар-GSM').update_attribute(:cell_operator_group_id, CellOperatorGroup.smarts.id)
    CellOperator.find_by_name('Ярославль-GSM').update_attribute(:cell_operator_group_id, CellOperatorGroup.smarts.id)
  end

  def down
  end
end
