# -*- encoding : utf-8 -*-
class AddPermitAlphanumericToUsers < ActiveRecord::Migration
  def change
    add_column :users, :permit_alphanumeric, :boolean, default: true, null: false
  end
end
