# -*- encoding : utf-8 -*-
class AddAnyErrors < ActiveRecord::Migration
  def up
    SendLogDeliveryError.create!(eng_name:'REJECT_TO_MEGAFON', name: 'Рассылка на Мегафон запрещена', code:-8, channel: Channel.any)
    SendLogDeliveryError.create!(eng_name:'NACK/11/Invalid Destination Address', name: 'NACK/11/Invalid Destination Address', code: -7, channel: Channel.any)
    SendLogDeliveryError.create!(eng_name:'ERROR_UNKNOWN', name: 'СМС центр вернул неизвестный код ошибки', code: -6, channel: Channel.any)
    SendLogDeliveryError.create!(eng_name:'RETRIES_EXCEEDED', name: 'Превышено число попыток отправки. Скорость smpp линка недостаточна(throttling_error_58).', code: -5, channel: Channel.any)
    SendLogDeliveryError.create!(eng_name:'LOW_BALANCE', name: 'Сообщение не отослано. Текущий баланс слишком низкий.', code: -4, channel: Channel.any)
    SendLogDeliveryError.create!(eng_name:'SMS_TIMEOUT_EXPIRED', name: 'Истёк срок жизни СМС сообщения.', code: -3, channel: Channel.any)
    SendLogDeliveryError.create!(eng_name:'ERROR_DELIVERY_TO_KANNEL', name: 'Ошибка доставки через сервис kannel', code: -2, channel: Channel.any)
    SendLogDeliveryError.create!(eng_name:'STATUS_UNKNOWN', name: 'Сообщение отослано. Статус доставки неизвестен. Не пришло уведомление от СМС центра о доставке.', code: -1, channel: Channel.any)
    SendLogDeliveryError.create!(eng_name:'SUCCESS_DELIVERY', name: 'Сообщение доставлено без ошибок.', code: 0, channel: Channel.any)
  end

  def down
  end
end
