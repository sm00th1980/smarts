# -*- encoding : utf-8 -*-
class RenameResultsIntoSendLogs < ActiveRecord::Migration
  def up
    rename_column :send_logs, :results, :check_statuses
  end

  def down
    rename_column :send_logs, :check_statuses, :results
  end
end
