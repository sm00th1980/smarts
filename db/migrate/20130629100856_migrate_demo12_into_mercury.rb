# -*- encoding : utf-8 -*-
class MigrateDemo12IntoMercury < ActiveRecord::Migration
  def up
    user = User.find_by_email('mercury@youct.ru')

    user.update_attribute(:demo, false)

    Log.where(user_id: user.id).map{|log| log.price=0; log.save!}

    Payment.where(:user_id => user.id).delete_all
    Payment.create!(:user_id => user.id,  :amount => 100, :service => 'bank', :date => Time.now, :status => 'success', :approved => Time.now)
  end

  def down
  end
end
