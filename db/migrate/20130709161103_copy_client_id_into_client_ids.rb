# -*- encoding : utf-8 -*-
class CopyClientIdIntoClientIds < ActiveRecord::Migration
  def up
    Log.all.each do |log|
      log.client_ids = [log.client_id] if log.client_id.present?
      log.save!
    end
  end

  def down
    Log.all.each do |log|
      log.client_ids = []
      log.save!
    end
  end
end
