# -*- encoding : utf-8 -*-
class AddNotNullIntoDeliveryErrorIdIntoSendLogs < ActiveRecord::Migration
  def up
    change_column :send_logs, :delivery_error_id, :integer, null: false
  end

  def down
    change_column :send_logs, :delivery_error_id, :integer, null: true
  end
end
