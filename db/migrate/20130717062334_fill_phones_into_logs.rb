# -*- encoding : utf-8 -*-
class FillPhonesIntoLogs < ActiveRecord::Migration
  include PhoneHelper

  def up
    SendLog.all.each do |send_log|
      if send_log.client_id.nil?
        send_log.log.update_attribute(:phones, [send_log.phone])
      else
        if normalize_phone_number(send_log.client.phone) == normalize_phone_number(send_log.phone)
          send_log.log.update_attribute(:phones, [])
        else
          send_log.log.update_attribute(:phones, [send_log.phone])
        end
      end
    end
  end

  def down
    Log.all.each do |log|
      log.update_attribute(:phones, [])
    end
  end
end
