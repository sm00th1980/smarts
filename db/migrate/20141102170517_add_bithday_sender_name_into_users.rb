# -*- encoding : utf-8 -*-
class AddBithdaySenderNameIntoUsers < ActiveRecord::Migration
  def change
    add_column :users, :birthday_sender_name, :text
  end
end
