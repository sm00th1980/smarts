# -*- encoding : utf-8 -*-
class RenameMinutesIntoAutodeliveries < ActiveRecord::Migration
  def change
    rename_column :auto_deliveries, :minutes, :minute
  end
end
