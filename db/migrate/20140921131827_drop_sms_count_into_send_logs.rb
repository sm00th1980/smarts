# -*- encoding : utf-8 -*-
class DropSmsCountIntoSendLogs < ActiveRecord::Migration
  def up
    remove_column :send_logs, :sms_count
  end

  def down
  end
end
