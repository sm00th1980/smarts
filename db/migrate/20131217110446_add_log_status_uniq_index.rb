# -*- encoding : utf-8 -*-
class AddLogStatusUniqIndex < ActiveRecord::Migration
  def change
    add_index :log_statuses, :internal_name, :uniq => true
  end
end
