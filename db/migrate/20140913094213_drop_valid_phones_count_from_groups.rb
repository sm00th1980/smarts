# -*- encoding : utf-8 -*-
class DropValidPhonesCountFromGroups < ActiveRecord::Migration
  def up
    remove_column :groups, :valid_phones_count
  end

  def down
    add_column :groups, :valid_phones_count, :integer, null: false, default: 0
  end
end
