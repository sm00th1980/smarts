# -*- encoding : utf-8 -*-
class AddNewCellOperatorGroups < ActiveRecord::Migration
  def up
    CellOperatorGroup.create!(name: 'Мотив', internal_name: :motiv, price_per_sms: 0.49)
    CellOperatorGroup.create!(name: 'НСС', internal_name: :nss, price_per_sms: 0.51)

    CellOperatorGroup.other.update_attribute(:price_per_sms, 0.09)
  end

  def down
  end
end
