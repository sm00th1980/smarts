# -*- encoding : utf-8 -*-
class CreateBlackLists < ActiveRecord::Migration
  def change
    create_table :black_lists do |t|
      t.integer :user_id, null: false
      t.column :phone, :bigint, null: false

      t.timestamps
    end
  end
end
