# -*- encoding : utf-8 -*-
class RemoveLogsWithoutPrice < ActiveRecord::Migration
  def up
    SendLog.where(:price => nil).each do |send_log|
      send_log.log.delete
      send_log.delete
    end
  end

  def down
  end
end
