# -*- encoding : utf-8 -*-
class ResetPriceForUndelivered < ActiveRecord::Migration
  def up
    SendLog.all.each do |send_log|
      if not send_log.delivered? and send_log.log.user.charge_by_delivered? and send_log.price != 0
        send_log.price = 0
        send_log.save!
      end
    end
  end

  def down
  end
end
