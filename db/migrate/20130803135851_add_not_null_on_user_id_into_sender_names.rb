# -*- encoding : utf-8 -*-
class AddNotNullOnUserIdIntoSenderNames < ActiveRecord::Migration
  def up
    change_column :sender_names, :user_id, :integer, :null => false
  end

  def down
    change_column :sender_names, :user_id, :integer, :null => true
  end
end
