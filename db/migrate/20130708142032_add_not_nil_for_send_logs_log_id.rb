# -*- encoding : utf-8 -*-
class AddNotNilForSendLogsLogId < ActiveRecord::Migration
  def up
    change_column :send_logs, :log_id, :integer, null: false
  end

  def down
    change_column :send_logs, :log_id, :integer, null: true
  end
end
