# -*- encoding : utf-8 -*-
class AddUniqIntoCellOperatorPhoneRanges < ActiveRecord::Migration
  def change
    add_index :cell_operator_phone_ranges, [:cell_operator_id, :begin_phone, :end_phone], unique: true, name: 'index'
  end
end
