# -*- encoding : utf-8 -*-
class RenameRejectToMegafonInternalName < ActiveRecord::Migration
  def up
    SendLogDeliveryStatus.find_by_kannel_id(-3).update_attribute(:internal_name, 'reject to megafon via gold channel')
    SendLogDeliveryStatus.find_by_kannel_id(-3).update_attribute(:name, 'Рассылка на Мегафон через золотой канал запрещена.')
  end

  def down
    SendLogDeliveryStatus.find_by_kannel_id(-3).update_attribute(:internal_name, 'reject_to_megafon_via_gold_channel')
    SendLogDeliveryStatus.find_by_kannel_id(-3).update_attribute(:name, 'Рассылка на Мегафон через золотой канал запрещена')
  end
end
