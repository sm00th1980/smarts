# -*- encoding : utf-8 -*-
class FillDeliveryErrorIntoSendLogs < ActiveRecord::Migration
  def up
    ActiveRecord::Base.connection.execute("update send_logs set delivery_error_id = %s where delivery_status_id =  %s" % [SendLogDeliveryError.no_error.id, SendLogDeliveryStatus.delivered.id])
    ActiveRecord::Base.connection.execute("update send_logs set delivery_error_id = %s where delivery_status_id <> %s" % [SendLogDeliveryError.unknown.id, SendLogDeliveryStatus.delivered.id])
  end

  def down
    ActiveRecord::Base.connection.execute("update send_logs set delivery_error_id = NULL")
  end
end
