# -*- encoding : utf-8 -*-
class AddNewErrorRejectToSmartsViaGoldChannel < ActiveRecord::Migration
  def up
    SendLogDeliveryError.create!(eng_name: 'REJECT_TO_SMARTS_VIA_GOLD_CHANNEL', name: 'Рассылка на Смартс через золотой канал запрещена', code: '-9', channel: Channel.gold)
  end

  def down
  end
end
