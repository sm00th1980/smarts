# -*- encoding : utf-8 -*-
class ChangeChannelIntoSendLogs < ActiveRecord::Migration
  def up
    SendLog.where('channel_id is null').find_each do |send_log|
      send_log.channel_id = Channel.bronze.id
      send_log.save!
    end

    change_column :send_logs, :channel_id, :integer, null: false
  end

  def down
  end
end
