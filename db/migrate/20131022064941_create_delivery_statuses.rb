# -*- encoding : utf-8 -*-
class CreateDeliveryStatuses < ActiveRecord::Migration
  def up
    SendLogDeliveryStatus.null
    SendLogDeliveryStatus.delivered
    SendLogDeliveryStatus.accepted
    SendLogDeliveryStatus.created
    SendLogDeliveryStatus.expired
    SendLogDeliveryStatus.undeliverable
    SendLogDeliveryStatus.unknown
    SendLogDeliveryStatus.rejected
    SendLogDeliveryStatus.deleted
    SendLogDeliveryStatus.terminated
    SendLogDeliveryStatus.response_timeout
    SendLogDeliveryStatus.deliver_receipt_timeout
    SendLogDeliveryStatus.unknown_reason
    SendLogDeliveryStatus.undeliverable_message
  end

  def down
  end
end
