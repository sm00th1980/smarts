# -*- encoding : utf-8 -*-
class CreateChargeTypes < ActiveRecord::Migration
  def up
    create_table :charge_types do |t|
      t.string :name
      t.string :internal_name

      t.timestamps
    end

    ChargeType.create(name: 'По отправке', internal_name: :by_sent)
    ChargeType.create(name: 'По доставке', internal_name: :by_delivered)
  end

  def down
    drop_table :charge_types
  end
end
