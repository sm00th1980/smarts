# -*- encoding : utf-8 -*-
class AddUniqIndexToPaymentTypesAndChargeTypes < ActiveRecord::Migration
  def change
    add_index :payment_types, [:internal_name], uniq:true
    add_index :charge_types, [:internal_name], uniq:true
  end
end
