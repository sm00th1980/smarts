# -*- encoding : utf-8 -*-
class DropColumnReasonIntoSenderNames < ActiveRecord::Migration
  def up
    remove_column :sender_names, :invalid_reason
  end

  def down
    add_column :sender_names, :invalid_reason, :text
  end
end
