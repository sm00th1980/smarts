# -*- encoding : utf-8 -*-
class AddTemplateIdIntoAutodeliveries < ActiveRecord::Migration
  def change
    add_column :auto_deliveries, :template_id, :integer
  end
end
