# -*- encoding : utf-8 -*-
class FlushRedis < ActiveRecord::Migration
  def up
    $redis.flushall
  end

  def down
  end
end
