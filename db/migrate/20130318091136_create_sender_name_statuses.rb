# -*- encoding : utf-8 -*-
class CreateSenderNameStatuses < ActiveRecord::Migration
  def change
    create_table :sender_name_statuses do |t|
      t.string :name
      t.string :internal_name

      t.timestamps
    end
  end
end
