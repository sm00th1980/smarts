# -*- encoding : utf-8 -*-
class AddCheckToPhoneIntoSendLogs < ActiveRecord::Migration
  def up
    ActiveRecord::Base.connection.execute("ALTER TABLE send_logs ADD CHECK (phone between %i and %i)" % [79000000000, 79999999999])
    change_column :send_logs, :phone, :bigint, null: false
  end
end
