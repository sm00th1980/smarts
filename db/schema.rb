# -*- encoding : utf-8 -*-
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150417182825) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "auto_deliveries", force: :cascade do |t|
    t.string   "name",              limit: 255
    t.text     "description"
    t.boolean  "active"
    t.date     "start_date"
    t.date     "stop_date"
    t.integer  "user_id",                       null: false
    t.text     "content"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "period_id",                     null: false
    t.integer  "minute"
    t.integer  "week_day_id"
    t.integer  "client_ids",                                 array: true
    t.integer  "group_ids",                                  array: true
    t.integer  "hour"
    t.integer  "template_id"
    t.text     "sender_name_value"
  end

  create_table "auto_delivery_periods", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "period",     limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "balances", force: :cascade do |t|
    t.integer  "period",                   null: false
    t.integer  "user_id",                  null: false
    t.float    "payments",   default: 0.0, null: false
    t.float    "charges",    default: 0.0, null: false
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "balances", ["user_id", "period"], name: "index_balances_on_user_id_and_period", unique: true, using: :btree

  create_table "black_lists", force: :cascade do |t|
    t.integer  "user_id",              null: false
    t.integer  "phone",      limit: 8, null: false
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "black_lists", ["user_id", "phone"], name: "index_black_lists_on_user_id_and_phone", unique: true, using: :btree

  create_table "cell_operator_groups", force: :cascade do |t|
    t.string   "name",          limit: 255, null: false
    t.string   "internal_name", limit: 255, null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "cell_operator_groups", ["internal_name"], name: "index_cell_operator_groups_on_internal_name", unique: true, using: :btree
  add_index "cell_operator_groups", ["name"], name: "index_cell_operator_groups_on_name", unique: true, using: :btree

  create_table "cell_operator_phone_ranges", force: :cascade do |t|
    t.integer  "cell_operator_id",           null: false
    t.integer  "begin_phone",      limit: 8, null: false
    t.integer  "end_phone",        limit: 8, null: false
    t.text     "region",                     null: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "cell_operator_phone_ranges", ["begin_phone", "end_phone"], name: "index", unique: true, using: :btree

  create_table "cell_operators", force: :cascade do |t|
    t.string   "name",                   limit: 255, null: false
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "cell_operator_group_id",             null: false
  end

  add_index "cell_operators", ["name"], name: "index_cell_operators_on_name", unique: true, using: :btree

  create_table "channels", force: :cascade do |t|
    t.string   "name",          limit: 255, null: false
    t.string   "internal_name", limit: 255, null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "charge_types", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.string   "internal_name", limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "charge_types", ["internal_name"], name: "index_charge_types_on_internal_name", using: :btree

  create_table "clients", force: :cascade do |t|
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
    t.string   "fio",                     limit: 255
    t.text     "description"
    t.integer  "phone",                   limit: 8,                   null: false
    t.integer  "group_id",                                            null: false
    t.date     "birthday"
    t.boolean  "birthday_congratulation",             default: false
  end

  add_index "clients", ["group_id", "phone"], name: "index_clients_on_group_id_and_phone", unique: true, using: :btree
  add_index "clients", ["group_id"], name: "index_clients_on_group_id", using: :btree

  create_table "groups", force: :cascade do |t|
    t.string   "name",                 limit: 255
    t.datetime "created_at",                                                null: false
    t.datetime "updated_at",                                                null: false
    t.integer  "user_id",                                                   null: false
    t.decimal  "gold_price_per_sms",               precision: 15, scale: 2
    t.decimal  "bronze_price_per_sms",             precision: 15, scale: 2
  end

  create_table "log_statuses", force: :cascade do |t|
    t.text     "name"
    t.text     "internal_name"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "log_statuses", ["internal_name"], name: "index_log_statuses_on_internal_name", using: :btree

  create_table "log_types", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.string   "internal_name", limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "log_valid_phones", force: :cascade do |t|
    t.integer  "log_id",                null: false
    t.integer  "valid_phone", limit: 8, null: false
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "log_valid_phones", ["log_id"], name: "index_log_valid_phones_on_log_id", using: :btree

  create_table "logs", force: :cascade do |t|
    t.integer  "user_id",                           null: false
    t.text     "content"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.text     "sender_name_value"
    t.boolean  "fake",              default: false
    t.integer  "status_id",                         null: false
    t.text     "check_errors",                                   array: true
    t.integer  "group_ids",                                      array: true
    t.datetime "started_at"
    t.datetime "finished_at"
    t.integer  "log_type_id",                       null: false
    t.float    "price",             default: 0.0,   null: false
    t.integer  "sms_sent_success",  default: 0,     null: false
    t.integer  "sms_sent_failure",  default: 0,     null: false
    t.integer  "prev_log_id"
    t.integer  "sms_count",         default: 1,     null: false
    t.integer  "phones",                                         array: true
  end

  add_index "logs", ["user_id"], name: "index_logs_on_user_id", using: :btree

  create_table "mts_ranges", force: :cascade do |t|
    t.integer  "begin_phone", limit: 8, null: false
    t.integer  "end_phone",   limit: 8, null: false
    t.text     "region",                null: false
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "mts_ranges", ["begin_phone", "end_phone"], name: "index_mts_ranges_on_begin_phone_and_end_phone", unique: true, using: :btree
  add_index "mts_ranges", ["begin_phone"], name: "index_mts_ranges_on_begin_phone", unique: true, using: :btree
  add_index "mts_ranges", ["end_phone"], name: "index_mts_ranges_on_end_phone", unique: true, using: :btree

  create_table "operator_tariffs", force: :cascade do |t|
    t.integer  "operator_id",                            null: false
    t.decimal  "price_per_sms", precision: 15, scale: 2, null: false
    t.date     "begin_date",                             null: false
    t.date     "end_date"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  create_table "payment_services", force: :cascade do |t|
    t.string   "name",          limit: 255, null: false
    t.string   "internal_name", limit: 255, null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "payment_services", ["internal_name"], name: "index_payment_services_on_internal_name", using: :btree

  create_table "payment_types", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.string   "internal_name", limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "payment_types", ["internal_name"], name: "index_payment_types_on_internal_name", using: :btree

  create_table "payments", force: :cascade do |t|
    t.integer  "user_id",     null: false
    t.float    "amount",      null: false
    t.datetime "date",        null: false
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.datetime "approved_at"
    t.integer  "service_id",  null: false
    t.datetime "failed_at"
  end

  create_table "send_log_check_statuses", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.string   "internal_name", limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "send_log_check_statuses", ["internal_name"], name: "index_send_log_check_statuses_on_internal_name", using: :btree

  create_table "send_log_delivery_errors", force: :cascade do |t|
    t.text     "eng_name",               null: false
    t.text     "name",                   null: false
    t.string   "code",       limit: 255, null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "channel_id",             null: false
  end

  add_index "send_log_delivery_errors", ["code", "channel_id"], name: "index_send_log_delivery_errors_on_code_and_channel_id", unique: true, using: :btree

  create_table "send_log_delivery_statuses", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.string   "internal_name", limit: 255, null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "kannel_id",                 null: false
  end

  add_index "send_log_delivery_statuses", ["internal_name"], name: "index_send_log_delivery_statuses_on_internal_name", using: :btree
  add_index "send_log_delivery_statuses", ["kannel_id"], name: "index_send_log_delivery_statuses_on_kannel_id", unique: true, using: :btree

  create_table "send_logs", force: :cascade do |t|
    t.integer  "log_id",                           null: false
    t.integer  "phone",                  limit: 8, null: false
    t.float    "price",                            null: false
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.string   "check_statuses",                                array: true
    t.integer  "delivery_status_id",               null: false
    t.integer  "delivery_error_id",                null: false
    t.integer  "cell_operator_group_id",           null: false
    t.integer  "channel_id",                       null: false
  end

  add_index "send_logs", ["created_at"], name: "index_send_logs_on_created_at", using: :btree
  add_index "send_logs", ["log_id"], name: "index_send_logs_on_log_id", using: :btree

  create_table "sender_name_statuses", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.string   "internal_name", limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "sender_name_statuses", ["internal_name"], name: "index_sender_name_statuses_on_internal_name", using: :btree

  create_table "sender_names", force: :cascade do |t|
    t.string   "value",      limit: 255
    t.integer  "user_id",                null: false
    t.integer  "status_id",              null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.boolean  "active"
  end

  add_index "sender_names", ["value", "user_id"], name: "index_sender_names_on_value_and_user_id", unique: true, using: :btree

  create_table "tariffs", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.float    "price_per_sms"
    t.integer  "min_sms_count"
    t.integer  "max_sms_count"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "templates", force: :cascade do |t|
    t.text     "text"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text     "name"
  end

  create_table "user_statuses", force: :cascade do |t|
    t.string   "name",          null: false
    t.string   "internal_name", null: false
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "user_tariffs", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "tariff_id"
    t.date     "begin_date"
    t.date     "end_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_types", force: :cascade do |t|
    t.text     "name"
    t.text     "internal_name"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "user_types", ["internal_name"], name: "index_user_types_on_internal_name", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                             limit: 255, default: "",    null: false
    t.string   "encrypted_password",                limit: 255, default: "",    null: false
    t.string   "reset_password_token",              limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                                 default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",                limit: 255
    t.string   "last_sign_in_ip",                   limit: 255
    t.datetime "created_at",                                                    null: false
    t.datetime "updated_at",                                                    null: false
    t.datetime "blocked_at"
    t.datetime "deleted_at"
    t.string   "fio",                               limit: 255
    t.text     "description"
    t.text     "birthday_message"
    t.integer  "birthday_hour",                                 default: 9
    t.integer  "user_type_id",                                                  null: false
    t.integer  "charge_type_id",                                                null: false
    t.integer  "payment_type_id",                                               null: false
    t.boolean  "permit_night_delivery",                         default: false, null: false
    t.string   "contact_email",                     limit: 255
    t.integer  "discount",                                      default: 0,     null: false
    t.boolean  "permit_alpha_sender_name",                      default: true,  null: false
    t.boolean  "reject_to_megafon",                             default: false
    t.boolean  "permit_def_sender_name",                        default: true,  null: false
    t.text     "birthday_sender_name"
    t.boolean  "reject_to_smarts_via_gold_channel",             default: false, null: false
    t.integer  "alpha_megafon_channel_id",                      default: 1,     null: false
    t.boolean  "reject_to_mts",                                 default: false, null: false
    t.integer  "status_id",                                                     null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "week_days", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "eng_name",   limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

end
