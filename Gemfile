source 'https://rubygems.org'

gem 'rails', '~> 4.2.2'

# Gems used only for assets and not required
# in production environments by default.
gem 'coffee-rails', '~> 4.0.0'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
gem 'execjs'
gem 'therubyracer', :platforms => :ruby
gem 'sass-rails',   '~> 4.0.0'

gem 'uglifier', '>= 1.3.0'

# Add these gems
gem 'compass', '0.12.7'
gem 'compass-rails'
gem 'font-awesome-rails'
gem 'zurb-foundation', '3.2.5'
gem 'chosen-rails'
gem 'typhoeus'
gem 'jquery-rails'
gem 'devise'
gem 'nokogiri'
gem 'sidekiq', '3.1.0'
gem 'russian'
gem 'will_paginate'
gem 'switch_user'
gem 'whenever', require: false
gem 'pg'
gem 'yaml_db'
gem 'json', '~> 1.8.1'

gem 'hiredis'
gem 'yajl-ruby'

gem 'roo', '~> 1.13.2'

gem 'rubyzip'
gem 'draper', '~> 1.3'

gem 'validates_email_format_of'

gem 'rack-mini-profiler'
gem 'bunny'
gem 'sneakers'
gem 'rabbitmq_http_api_client', '>= 1.3.0'

group :development do
  gem 'capistrano', '2.15.4'
  gem 'rvm-capistrano', '1.3.4', require: false
  gem 'haml-rails'
  gem 'magic_encoding'
  gem 'capistrano_colors'
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'pry-rails'
  gem 'capistrano-unicorn', '0.1.10', require: false
  gem 'spring'
  gem 'traceroute'
end

group :test do
  gem 'minitest'
  gem 'minitest-reporters'
  gem 'database_cleaner'
  gem 'factory_girl_rails'
  gem 'faker'
  gem 'timecop'
  gem 'mocha'
end

gem 'simplecov', :require => false, :group => :test

group :production do
  gem 'exception_notification'
  gem 'unicorn'
end

gem 'rye'


#rails4 support
gem 'rails-observers'
gem 'protected_attributes'