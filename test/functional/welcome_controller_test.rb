# -*- encoding : utf-8 -*-
require 'test_helper'

class WelcomeControllerTest < ActionController::TestCase
  def setup
    clear_db

    @user = FactoryGirl.create(:user)
    @admin = FactoryGirl.create(:admin)
  end

  test "should redirect to login_page for unsigned user " do
    get :index
    assert_redirected_to new_user_session_path
  end


  test "should get root page for user" do
    signed(@user) do
      get :index
      assert_redirected_to sms_path
    end
  end

  test "should get users page for admin" do
    signed(@admin) do
      get :index
      assert_redirected_to users_path
    end
  end
end
