# -*- encoding : utf-8 -*-
require 'test_helper'

class ReportByDateControllerTest < ActionController::TestCase
  def setup
    Sidekiq::Testing.disable!

    clear_db
    @user = FactoryGirl.create(:user, contact_email: 'sm00th1980@mail.ru')
    @log = FactoryGirl.create(:log_with_chunks, user: @user)

    @params = {
        start_date: @log.created_at.at_beginning_of_month.to_date.strftime('%Y-%m-%d'),
        end_date: @log.created_at.at_end_of_month.to_date.strftime('%Y-%m-%d')
    }
  end

  #show
  test "should not pass to show for non_auth user" do
    get :show
    assert_redirected_to new_user_session_path

    assert_response :redirect
  end

  test "should show for user" do
    signed(@user) do
      get :show
      assert_response :success
    end
  end

  #generate
  test "should not pass to generate for non auth user" do
    post :generate, @params
    assert_response :redirect
    assert_redirected_to new_user_session_path
  end

  test "should generate report-by-date" do
    signed(@user) do
      check_generate_by_date_task_created do
        post :generate, @params
        assert_response :redirect
        assert_redirected_to show_report_by_date_path
        assert_equal I18n.t('report_by_date.generating') % [@params[:start_date].to_date.strftime('%Y-%m-%d'), @params[:end_date].to_date.strftime('%Y-%m-%d'), @user.contact_email], flash[:notice]

        assert_equal @params[:start_date].to_date, assigns(:start_date)
        assert_equal @params[:end_date].to_date, assigns(:end_date)
      end
    end
  end

  #invalid
  test "should not generate report-by-date for user without contact-email" do
    user = FactoryGirl.create(:user, contact_email: '')
    log = FactoryGirl.create(:log_with_chunks, user: user)
    params = {
        start_date: log.created_at.at_beginning_of_month.to_date.strftime('%Y-%m-%d'),
        end_date: log.created_at.at_end_of_month.to_date.strftime('%Y-%m-%d')
    }

    refute user.has_contact_email?

    signed(user) do
      check_generate_by_date_task_not_created do
        post :generate, params
        assert_response :redirect
        assert_redirected_to show_report_by_date_path
        assert_equal I18n.t('report_by_date.failure.contact_email_incorrect'), flash[:alert]

        assert_equal @params[:start_date].to_date, assigns(:start_date)
        assert_equal @params[:end_date].to_date, assigns(:end_date)
      end
    end

  end

  test "should not generate report-by-date with invalid start-date" do
    signed(@user) do
      check_generate_by_date_task_not_created do
        post :generate, @params.merge!({start_date: '-1'})
        assert_response :redirect
        assert_redirected_to show_report_by_date_path
        assert_equal I18n.t('report_by_date.failure.start_date_invalid'), flash[:alert]

        assert_nil assigns(:start_date)
        assert_nil assigns(:end_date)
      end
    end
  end

  test "should not generate report-by-date with invalid end-date" do
    signed(@user) do
      check_generate_by_date_task_not_created do
        post :generate, @params.merge!({end_date: '-1'})
        assert_response :redirect
        assert_redirected_to show_report_by_date_path
        assert_equal I18n.t('report_by_date.failure.end_date_invalid'), flash[:alert]

        assert_equal @params[:start_date].to_date, assigns(:start_date)
        assert_nil assigns(:end_date)
      end
    end
  end

  test "should not generate report-by-date with invalid start-date and end-date" do
    signed(@user) do
      check_generate_by_date_task_not_created do
        post :generate, @params.merge!({start_date: '-1', end_date: '-1'})
        assert_response :redirect
        assert_redirected_to show_report_by_date_path
        assert_equal I18n.t('report_by_date.failure.start_date_invalid'), flash[:alert]

        assert_nil assigns(:start_date)
        assert_nil assigns(:end_date)
      end
    end
  end

  private
  def check_generate_by_date_task_created(&block)
    $redis.flushall
    yield
    assert_not_nil $redis.lindex('main:queue:default', 0), "should create new task in sidekiq"
    assert_equal "- !ruby/class 'Report::Manager'", YAML.load($redis.lindex('main:queue:default', 0))["args"].first.split("\n")[1]
    assert_equal "- :generate_by_date", YAML.load($redis.lindex('main:queue:default', 0))["args"].first.split("\n")[2]
  end

  def check_generate_by_date_task_not_created(&block)
    $redis.flushall
    yield
    assert_nil $redis.lindex('main:queue:default', 0), "should not create new task in sidekiq"
  end

end
