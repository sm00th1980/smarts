# -*- encoding : utf-8 -*-
require 'test_helper'

class NightDeliveryControllerTest < ActionController::TestCase
  def setup
    clear_db

    @user = FactoryGirl.create(:user)

    @params = {
        permission: 'deactivate'
    }
  end

  #index
  test "should not show for unsigned" do
    get :show
    assert_response :redirect
    assert_redirected_to new_user_session_path
  end

  test "should show signed" do
    signed(@user) do
      get :show
      assert_response :success
    end
  end

  #update
  test "should not activate for unsigned" do
    get :update, @params
    assert_response :redirect
    assert_redirected_to new_user_session_path
    assert_nil assigns(:permission)
  end

  test "should deactivate for user" do
    @user.update_attribute(:permit_night_delivery, false)
    signed(@user) do
      get :update, @params.merge!(permission: 'deactivate')
      assert_response :redirect
      assert_equal I18n.t('permit_night_delivery.deactivated'), flash[:notice]
      assert @user.reload.permit_night_delivery?
      assert_redirected_to show_night_delivery_path
      assert assigns(:permission)
    end
  end

  test "should activate for user" do
    @user.update_attribute(:permit_night_delivery, true)
    signed(@user) do
      get :update, @params.merge!(permission: 'activate')
      assert_response :redirect
      assert_equal I18n.t('permit_night_delivery.activated'), flash[:notice]
      refute @user.reload.permit_night_delivery?
      assert_redirected_to show_night_delivery_path
      refute assigns(:permission)
    end
  end

  #update invalid
  test "should not activate for user without params" do
    signed(@user) do
      get :update
      assert_response :redirect
      assert_equal I18n.t('permit_night_delivery.failure.invalid') % '', flash[:alert]
      refute @user.reload.permit_night_delivery?
      assert_redirected_to show_night_delivery_path
      assert_nil assigns(:permission)
    end
  end

  test "should not activate for user with blank permission" do
    signed(@user) do
      get :update, @params.merge!(permission: '')
      assert_response :redirect
      assert_equal I18n.t('permit_night_delivery.failure.invalid') % '', flash[:alert]
      refute @user.reload.permit_night_delivery?
      assert_redirected_to show_night_delivery_path
      assert_nil assigns(:permission)
    end
  end

  test "should not activate for user with invalid permission" do
    signed(@user) do
      get :update, @params.merge!(permission: '-1')
      assert_response :redirect
      assert_equal I18n.t('permit_night_delivery.failure.invalid') % '-1', flash[:alert]
      refute @user.reload.permit_night_delivery?
      assert_redirected_to show_night_delivery_path
      assert_nil assigns(:permission)
    end
  end

end
