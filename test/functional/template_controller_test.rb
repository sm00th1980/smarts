# -*- encoding : utf-8 -*-
require 'test_helper'

class TemplateControllerTest < ActionController::TestCase

  def setup
    clear_db

    @user = FactoryGirl.create(:user)
    @template = FactoryGirl.create(:template, :user => @user)

    @new_name = "name_#{rand(100).to_s}"
    @new_text = "text_#{rand(100).to_s}"
  end

  #show
  test "should not get show for unsigned" do
    get :show, :id => @template
    assert_response :redirect
    assert_redirected_to new_user_session_path
    assert_nil assigns(:template)
  end

  test "should get show for user" do
    signed(@template.user) do
      get :show, :id => @template
      assert_response :success
      assert_not_nil assigns(:template)
      assert_equal @template, assigns(:template)
    end
  end

  test "should not get show signed for not exist template" do
    signed(@user) do
      get :show, :id => -1
      assert_response :redirect
      assert_nil assigns(:template)
      assert_redirected_to templates_path
      assert_equal I18n.t('template.not_exist') % -1, flash[:alert]
    end
  end

  ##update
  test "should update for user" do
    signed(@template.user) do
      post :update, :id => @template, :template => {:text => @new_text, :name => @new_name}
      assert_response :redirect
      assert_not_nil assigns(:template)
      assert_equal @template, assigns(:template)
      assert_redirected_to templates_path

      assert_equal I18n.t('template.success.updated'), flash[:notice]
      assert_equal @new_name, @template.reload.name
      assert_equal @new_text, @template.reload.text
      assert_equal @template.user, @template.reload.user
    end
  end

  #invalid update
  test "should not update unsigned" do
    post :update, :id => @template, :template => {:text => @new_text, :name => @new_name}
    assert_response :redirect
    assert_redirected_to new_user_session_path
    assert_nil assigns(:template)
  end

  test "should not update signed for not exist template" do
    signed(@template.user) do
      post :update, :id => -1, :template => {:text => @new_text}
      assert_response :redirect
      assert_nil assigns(:template)
      assert_redirected_to templates_path
      assert_equal I18n.t('template.not_exist') % -1, flash[:alert]
    end
  end

  test "should not update with blank name" do
    signed(@template.user) do
      post :update, :id => @template, :template => {:text => @new_text, :name => ''}
      assert_response :redirect
      assert_not_nil assigns(:template)
      assert_equal @template, assigns(:template)
      assert_redirected_to templates_path

      updated_template = @template.reload

      assert_equal I18n.t('template.failure.name_is_blank'), flash[:alert]
      assert_equal updated_template.name, @template.name
      assert_equal updated_template.text, @template.text
      assert_equal updated_template.user, @template.user
    end
  end

  test "should not update with blank text" do
    signed(@template.user) do
      post :update, :id => @template, :template => {:text => '', :name => @new_name}
      assert_response :redirect
      assert_not_nil assigns(:template)
      assert_equal @template, assigns(:template)
      assert_redirected_to templates_path

      updated_template = @template.reload

      assert_equal I18n.t('template.failure.text_is_blank'), flash[:alert]
      assert_equal updated_template.name, @template.name
      assert_equal updated_template.text, @template.text
      assert_equal updated_template.user, @template.user
    end
  end

  test "should not update with blank name and blank text" do
    signed(@template.user) do
      post :update, :id => @template, :template => {:text => '', :name => ''}
      assert_response :redirect
      assert_not_nil assigns(:template)
      assert_equal @template, assigns(:template)
      assert_redirected_to templates_path

      updated_template = @template.reload

      assert_equal I18n.t('template.failure.name_is_blank'), flash[:alert]
      assert_equal updated_template.name, @template.name
      assert_equal updated_template.text, @template.text
      assert_equal updated_template.user, @template.user
    end
  end

  #new
  test "should not get new for unsigned" do
    get :new
    assert_response :redirect
    assert_redirected_to new_user_session_path
  end

  test "should get new for user" do
    signed(@user) do
      get :new
      assert_response :success
    end
  end

  #create
  test "should not create unsigned" do
    check_not_create_template do
      post :create, :template => {:text => @new_text, :name => @new_name}
      assert_response :redirect
      assert_redirected_to new_user_session_path
    end
  end

  test "should create template for user" do
    signed(@user) do
      check_create_template do
        post :create, :template => {:text => @new_text, :name => @new_name}
        assert_response :redirect
        assert_redirected_to templates_path

        assert_equal I18n.t('template.success.created'), flash[:notice]

        new_template = Template.find_by_text(@new_text)
        assert_equal @user, new_template.user
      end
    end
  end

  test "should not create template with blank name" do
    signed(@user) do
      check_not_create_template do
        post :create, :template => {:text => '123', :name => ''}
        assert_response :redirect
        assert_redirected_to templates_path

        assert_equal I18n.t('template.failure.name_is_blank'), flash[:alert]
      end
    end
  end

  test "should not create template with blank text" do
    signed(@user) do
      check_not_create_template do
        post :create, :template => {:text => '', :name => '123'}
        assert_response :redirect
        assert_redirected_to templates_path

        assert_equal I18n.t('template.failure.text_is_blank'), flash[:alert]
      end
    end
  end

  test "should not create template with blank name and blank text" do
    signed(@user) do
      check_not_create_template do
        post :create, :template => {:text => '', :name => ''}
        assert_response :redirect
        assert_redirected_to templates_path

        assert_equal I18n.t('template.failure.name_is_blank'), flash[:alert]
      end
    end
  end

  #destroy
  test "should not delete template for unsigned" do
    post :destroy, :id => @template
    assert_response :redirect
    assert_redirected_to new_user_session_path
    assert Template.exists?(id: @template)
  end

  test "should delete template for user" do
    signed(@template.user) do
      post :destroy, :id => @template
      assert_response :redirect
      assert_redirected_to templates_path
      assert_equal I18n.t('template.success.deleted'), flash[:notice]
      assert_equal false, Template.exists?(id: @template)
    end
  end

  test "should not delete template for not exist template" do
    signed(@user) do
      assert_no_difference 'Template.count' do
        post :destroy, :id => -1

        assert_response :redirect
        assert_nil assigns(:template)
        assert_redirected_to templates_path
        assert_equal I18n.t('template.not_exist') % -1, flash[:alert]
      end
    end
  end

  private
  def check_not_create_template(&block)
    assert_no_difference "Template.count" do
      yield if block_given?
    end
  end

  def check_create_template(&block)
    assert_difference "Template.count" do
      yield if block_given?
    end
  end

end
