# -*- encoding : utf-8 -*-
require 'test_helper'

class GroupsControllerTest < ActionController::TestCase
  def setup
    clear_db

    @user = FactoryGirl.create(:user)
    @group = @user.groups.first
  end

  #index
  test "should not get index for unsigned" do
    get :index
    assert_response :redirect
    assert_nil assigns(:groups)
    assert_redirected_to new_user_session_path
  end

  test "should get index for user" do
    assert_operator @user.groups.count, :>, 0

    signed(@user) do
      get :index
      assert_response :success
      assert_not_nil assigns(:groups)
      assert_equal @user.groups.order('created_at desc').paginate(:page => 1, :per_page => GroupsController::PER_PAGE), assigns(:groups)
    end
  end

end
