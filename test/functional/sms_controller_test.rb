# -*- encoding : utf-8 -*-
require 'test_helper'

class SmsControllerTest < ActionController::TestCase
  include PhoneHelper

  def setup
    clear_db

    @user = FactoryGirl.create(:user)
    @sender_name_value_positive = FactoryGirl.create(:sender_name_accepted_with_verified_value, user: @user).value

    @user_with_negative_balance = FactoryGirl.create(:user_with_negative_balance)
    @sender_name_value_negative = FactoryGirl.create(:sender_name_accepted_with_verified_value, user: @user_with_negative_balance).value

    @phone_numbers = '89379903835,89379903836'

    stub_real_request
  end

  #index
  test "should not get index for unsigned" do
    get :index
    assert_response :redirect
    assert_redirected_to new_user_session_path

    post :index
    assert_response :redirect
    assert_redirected_to new_user_session_path
  end

  test "should get index signed" do
    signed do
      get :index
      assert_response :success
    end
  end

  #success sms send
  test "should not send_sms for unsigned" do
    post :send_sms
    assert_response :redirect
    assert_redirected_to new_user_session_path
  end

  test "should send sms for phone_numbers" do
    signed(@user) do
      assert_difference 'Log.count' do
        post :send_sms, :client => {:sender_name => @sender_name_value_positive, :sms_content => 'sms_content', :phone_numbers => @phone_numbers}
        assert_response :redirect
        assert_redirected_to sms_path
        assert_equal I18n.t('sms.started'), flash[:notice]

        log = Log.last
        assert_equal [], log.group_ids
        assert_equal @phone_numbers.split(',').map{|p| normalize_phone_number(p)}, log.phones
        assert_equal 'sms_content', log.content
        assert_equal @sender_name_value_positive, log.sender_name_value
      end
    end
  end

  test "should send sms for ids with group_id" do
    signed(@user) do
      assert_difference 'Log.count' do
        post :send_sms, :client => {:sender_name => @sender_name_value_positive, :sms_content => 'sms_content', :ids => ["group_id=#{@user.groups.first.id}"]}
        assert_response :redirect
        assert_redirected_to sms_path
        assert_equal I18n.t('sms.started'), flash[:notice]

        log = Log.last
        assert_equal [@user.groups.first.id], log.group_ids
        assert_equal [], log.phones
        assert_equal 'sms_content', log.content
        assert_equal @sender_name_value_positive, log.sender_name_value
      end
    end
  end

  test "should not send sms for ids with only client_id" do
    signed(@user) do
      assert_no_difference 'Log.count' do
        post :send_sms, :client => {:sender_name => @sender_name_value_positive, :sms_content => 'sms_content', :ids => ["client_id=#{@user.groups.first.clients.first.id}"]}
        assert_response :redirect
        assert_redirected_to sms_path
        assert_equal I18n.t('sms.failure.no_phones'), flash[:alert]
      end
    end
  end

  test "should send sms for ids with client_id and group_id" do
    signed(@user) do
      assert_difference 'Log.count' do
        post :send_sms, :client => {:sender_name => @sender_name_value_positive, :sms_content => 'sms_content', :ids => ["client_id=#{@user.groups.first.clients.first.id}", "group_id=#{@user.groups.first.id}"]}
        assert_response :redirect
        assert_redirected_to sms_path
        assert_equal I18n.t('sms.started'), flash[:notice]

        log = Log.last
        assert_equal [@user.groups.first.id], log.group_ids
        assert_equal [], log.phones
        assert_equal 'sms_content', log.content
        assert_equal @sender_name_value_positive, log.sender_name_value
      end
    end
  end

  #send with templates
  test "should send sms with template" do
    signed(@user) do
      assert_difference 'Log.count' do
        post :send_sms, :client => {:sender_name => @sender_name_value_positive, :template => @user.templates.first.id, :phone_numbers => @phone_numbers}
        assert_response :redirect
        assert_redirected_to sms_path
        puts flash[:alert]
        assert_equal I18n.t('sms.started'), flash[:notice]


        log = Log.last
        assert_equal [], log.group_ids
        assert_equal @phone_numbers.split(',').map{|p| normalize_phone_number(p)}, log.phones
        assert_equal @user.templates.first.text, log.content
        assert_equal @sender_name_value_positive, log.sender_name_value
      end
    end
  end

  test "should send sms with template and sms_content" do
    signed(@user) do
      assert_difference 'Log.count' do
        post :send_sms, :client => {:sender_name => @sender_name_value_positive, :sms_content => 'sms_content', :template => @user.templates.first.id, :phone_numbers => @phone_numbers}
        assert_response :redirect
        assert_redirected_to sms_path
        assert_equal I18n.t('sms.started'), flash[:notice]

        log = Log.last
        assert_equal [], log.group_ids
        assert_equal @phone_numbers.split(',').map{|p| normalize_phone_number(p)}, log.phones
        assert_equal 'sms_content', log.content
        assert_equal @sender_name_value_positive, log.sender_name_value
      end
    end
  end

  #failure send sms
  test "should try send sms without phone_numbers" do
    signed(@user) do
      assert_no_difference 'Log.count' do
        post :send_sms, :client => {:sender_name => @sender_name_value_positive, :sms_content => 'sms_content'}
        assert_response :redirect
        assert_redirected_to sms_path
        assert_equal I18n.t('sms.failure.no_phones'), flash[:alert]
      end
    end
  end

  test "should try send sms with invalid phone_numbers" do
    signed(@user) do
      assert_no_difference 'Log.count' do
        post :send_sms, :client => {:sender_name => @sender_name_value_positive, :sms_content => 'sms_content', :phone_numbers => '123-cf'}
        assert_response :redirect
        assert_redirected_to sms_path
        assert_redirected_to sms_path
        assert_equal I18n.t('sms.failure.no_phones'), flash[:alert]
      end
    end
  end

  test "should try send sms for user with negative balance" do
    signed(@user_with_negative_balance) do
      assert_difference 'Log.count' do
        post :send_sms, :client => {:sender_name => @sender_name_value_negative, :sms_content => 'sms_content', :phone_numbers => @phone_numbers}
        assert_response :redirect
        assert_redirected_to sms_path
        assert_equal I18n.t('sms.started'), flash[:notice]

        log = Log.last
        assert_equal [], log.group_ids
        assert_equal @phone_numbers.split(',').map{|p| normalize_phone_number(p)}, log.phones
        assert_equal 'sms_content', log.content
        assert_equal @sender_name_value_negative, log.sender_name_value
      end
    end
  end

  #invalid request
  test "should not send sms without params" do
    signed(@user) do
      assert_no_difference 'Log.count' do
        get :send_sms, {}
        assert_response :redirect
        assert_redirected_to sms_path
        assert_equal I18n.t('sms.failure.invalid_params'), flash[:alert]
      end
    end
  end

  test "should not send sms for user without message" do
    signed(@user) do
      assert_no_difference 'Log.count' do
        post :send_sms, :client => {:sender_name => @sender_name_value_positive, :phone_numbers => @phone_numbers}
        assert_response :redirect
        assert_redirected_to sms_path
        assert_equal I18n.t('sms.failure.no_message'), flash[:alert]
      end
    end
  end

  test "should not send sms for user with blank message" do
    signed(@user) do
      assert_no_difference 'Log.count' do
        post :send_sms, :client => {:sender_name => @sender_name_value_positive, :phone_numbers => @phone_numbers, :sms_content => ''}
        assert_response :redirect
        assert_redirected_to sms_path
        assert_equal I18n.t('sms.failure.no_message'), flash[:alert]
      end
    end
  end

  test "should not send sms for user with nil message" do
    signed(@user) do
      assert_no_difference 'Log.count' do
        post :send_sms, :client => {:sender_name => @sender_name_value_positive, :phone_numbers => @phone_numbers, :sms_content => nil}
        assert_response :redirect
        assert_redirected_to sms_path
        assert_equal I18n.t('sms.failure.no_message'), flash[:alert]
      end
    end
  end

  test "should not send sms for user with invalid sender name" do
    signed(@user) do
      assert_no_difference 'Log.count' do
        post :send_sms, :client => {:sender_name => 'invalid sender name', :sms_content => 'sms_content', :phone_numbers => @phone_numbers}
        assert_response :redirect
        assert_redirected_to sms_path
        assert_equal I18n.t('sms.failure.invalid_sender_name'), flash[:alert]
      end
    end
  end

  test "should not send sms for user with blank sender name" do
    signed(@user) do
      assert_no_difference 'Log.count' do
        post :send_sms, :client => {:sender_name => '', :sms_content => 'sms_content', :phone_numbers => @phone_numbers}
        assert_response :redirect
        assert_redirected_to sms_path
        assert_equal I18n.t('sms.failure.invalid_sender_name'), flash[:alert]
      end
    end
  end

  test "should not send sms for user with nil sender name" do
    signed(@user) do
      assert_no_difference 'Log.count' do
        post :send_sms, :client => {:sender_name => nil, :sms_content => 'sms_content', :phone_numbers => @phone_numbers}
        assert_response :redirect
        assert_redirected_to sms_path
        assert_equal I18n.t('sms.failure.invalid_sender_name'), flash[:alert]
      end
    end
  end

  test "should not send sms for user with not his own sender name" do
    another_user = FactoryGirl.create(:user)
    sender_name_value_another_user = FactoryGirl.create(:sender_name_accepted_with_verified_value, user: another_user).value

    signed(@user) do
      assert_no_difference 'Log.count' do
        post :send_sms, :client => {:sender_name => sender_name_value_another_user, :sms_content => 'sms_content', :phone_numbers => @phone_numbers}
        assert_response :redirect
        assert_redirected_to sms_path
        assert_equal I18n.t('sms.failure.invalid_sender_name'), flash[:alert]
      end
    end
  end

  test "should not send sms with not exist template" do
    signed(@user) do
      assert_no_difference 'Log.count' do
        post :send_sms, :client => {:sender_name => @sender_name_value_positive, :template => -1, :phone_numbers => @phone_numbers}
        assert_response :redirect
        assert_redirected_to sms_path
        assert_equal I18n.t('sms.failure.no_message'), flash[:alert]
      end
    end
  end

  test "should not send sms with blank template" do
    signed(@user) do
      assert_no_difference 'Log.count' do
        post :send_sms, :client => {:sender_name => @sender_name_value_positive, :template => '', :phone_numbers => @phone_numbers}
        assert_response :redirect
        assert_redirected_to sms_path
        assert_equal I18n.t('sms.failure.no_message'), flash[:alert]
      end
    end
  end

  test "should not send sms with nil template" do
    signed(@user) do
      assert_no_difference 'Log.count' do
        post :send_sms, :client => {:sender_name => @sender_name_value_positive, :template => nil, :phone_numbers => @phone_numbers}
        assert_response :redirect
        assert_redirected_to sms_path
        assert_equal I18n.t('sms.failure.no_message'), flash[:alert]
      end
    end
  end

  # too many phones
  test "should not try send sms for user with too many phones" do
    phones = (0..SMSService.max_phones).map { Test::Helpers.random_phone_number }

    signed(@user) do
      assert_no_difference 'Log.count' do
        post :send_sms, :client => {:sender_name => @sender_name_value_positive, :sms_content => 'sms_content', :phone_numbers => phones.join(',')}
        assert_response :redirect
        assert_redirected_to sms_path
        assert_equal I18n.t('sms.failure.too_many_phones') % SMSService.max_phones, flash[:alert]
      end
    end
  end

end
