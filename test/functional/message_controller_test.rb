# -*- encoding : utf-8 -*-
require 'test_helper'

class MessageControllerTest < ActionController::TestCase
  def setup
    clear_db

    @params = [
        {sms_count: 1, content: "Стоматология НОВА  Низкие цены для Вас! пр. Строителей 156а\r\n413-314"},
        {sms_count: 2, content: "Денис, гостинница Апельсин в Ульяновске заказана на 11 апреля. 2000 рублей на нас троих будет её стоимость. Русланка."},
        {sms_count: 3, content: "Заявка № 31***-72 обработана. Государственный кадастровый учет приостановлен. Документы будут доставлены в офис приема-выдачи в течение 2 дней."},
        {sms_count: 4, content: "Только этой весной, в SPA-центре MatreshkaPlaza новые программы для коррекции проблемных зон! Антицеллюлитный, лимфодренажный и лифтинговые эффекты. Три процедуры в подарок! Торопись, предложение ограничено. Запись по телефону: 933-30-40"}
    ]
  end

  #sms_count
  test "should not get sms-count for unsigned" do
    @params.each do |_params|
      get :sms_count, {content: _params[:content]}
      assert_response :redirect
      assert_redirected_to new_user_session_path

      post :sms_count, {content: _params[:content]}
      assert_response :redirect
      assert_redirected_to new_user_session_path
    end
  end

  test "should get sms-count params signed" do
    @params.each do |_params|
      signed do
        get :sms_count, {content: _params[:content]}
        assert_response :success
        assert_json_sms_count _params[:sms_count]
      end
    end
  end

  #invalid requests
  test "should get error when no params" do
    signed do
      get :sms_count, {}
      assert_response :success
      assert_json_error I18n.t('message.failure.invalid_params')
    end
  end

  test "should get error when no content" do
    signed do
      get :sms_count, {bla_bla: 123}
      assert_response :success
      assert_json_error I18n.t('message.failure.invalid_params')
    end
  end

  test "should get error when content is blank" do
    signed do
      get :sms_count, {content: ''}
      assert_response :success
      assert_json_error I18n.t('message.failure.content_is_blank')
    end
  end

  private
  def assert_json_error(error_string)
    assert_equal({'error' => error_string}, JSON.parse(@response.body))
  end

  def assert_json_sms_count(sms_count)
    assert_equal({'sms_count' => sms_count}, JSON.parse(@response.body))
  end
end
