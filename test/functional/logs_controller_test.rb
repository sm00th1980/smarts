# -*- encoding : utf-8 -*-
require 'test_helper'

class LogsControllerTest < ActionController::TestCase
  include Devise::TestHelpers

  def setup
    clear_db
    @user = FactoryGirl.create(:user)
    FactoryGirl.create(:log_with_chunks, user: @user)
  end

  #index
  test "should not pass to index for non_auth user" do
    get :index
    assert_nil assigns(:logs)
    assert_redirected_to new_user_session_path
  end

  test "should get index for user" do
    Calculator::Logs.recalculate(Log.order(:created_at).first.created_at.to_date, Log.order(:created_at).last.created_at.to_date)

    signed(@user) do
      get :index
      assert_response :success
      assert_not_nil assigns(:logs)
      assert_equal @user.logs.sort, assigns(:logs).sort
    end
  end

end
