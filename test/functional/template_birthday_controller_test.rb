# -*- encoding : utf-8 -*-
require 'test_helper'

class TemplateBirthdayControllerTest < ActionController::TestCase
  def setup
    clear_db

    @new_text = "новый текст шаблона"
    @new_hour = 12
    @invalid_hour = 'vfdmkvfmd'

    @user = FactoryGirl.create(:user, birthday_message: 'vfdvfd')
    @sender_name_value = FactoryGirl.create(:sender_name_accepted_with_verified_value, user: @user).value
  end

  #show
  test "should not get show unsigned" do
    get :show
    assert_response :redirect
    assert_nil assigns(:birthday_message)
    assert_redirected_to new_user_session_path
  end

  test "should get show for signed" do
    signed(@user) do
      get :show
      assert_response :success
      assert_not_nil assigns(:birthday_message)
      assert_equal @user.birthday_message, assigns(:birthday_message)
    end
  end

  #update
  test "should not update unsigned" do
    post :update, :template_birthday => {:birthday_message => @new_text, :birthday_hour => @new_hour, :birthday_sender_name => @sender_name_value}
    assert_response :redirect
    assert_redirected_to new_user_session_path
  end

  test "should update for user" do
    signed(@user) do
      post :update, :template_birthday => {:birthday_message => @new_text, :birthday_hour => @new_hour, :birthday_sender_name => @sender_name_value}
      assert_response :redirect
      assert_redirected_to show_template_birthday_path

      assert_equal I18n.t('template_birthday.success.updated'), flash[:notice]
      assert_equal @new_text, User.find_by_id(@user.id).birthday_message
      assert_equal @new_hour, User.find_by_id(@user.id).birthday_hour
      assert_equal @sender_name_value, User.find_by_id(@user.id).birthday_sender_name
    end
  end

  test "should update for user with invalid_hour" do
    signed(@user) do
      post :update, :template_birthday => {:birthday_message => @new_text, :birthday_hour => @invalid_hour, :birthday_sender_name => @sender_name_value}
      assert_response :redirect
      assert_redirected_to show_template_birthday_path

      assert_equal I18n.t('template_birthday.success.updated'), flash[:notice]
      assert_equal @new_text, User.find_by_id(@user.id).birthday_message
      assert_equal Period::DEFAULT_HOUR, User.find_by_id(@user.id).birthday_hour
      assert_equal @sender_name_value, User.find_by_id(@user.id).birthday_sender_name
    end
  end

  test "should update for user with invalid sender_name" do
    signed(@user) do
      post :update, :template_birthday => {:birthday_message => @new_text, :birthday_hour => @invalid_hour, :birthday_sender_name => ''}
      assert_response :redirect
      assert_redirected_to show_template_birthday_path

      assert_equal I18n.t('template_birthday.failure.invalid_sender_name'), flash[:alert]
      assert_equal @new_text, User.find_by_id(@user.id).birthday_message
      assert_equal Period::DEFAULT_HOUR, User.find_by_id(@user.id).birthday_hour
      assert_nil User.find_by_id(@user.id).birthday_sender_name
    end
  end

  test "should update for user with moderating sender_name" do
    moderating_sender_name_value = FactoryGirl.create(:sender_name_moderating_with_verified_value, user: @user).value

    signed(@user) do
      post :update, :template_birthday => {:birthday_message => @new_text, :birthday_hour => @invalid_hour, :birthday_sender_name => moderating_sender_name_value}
      assert_response :redirect
      assert_redirected_to show_template_birthday_path

      assert_equal I18n.t('template_birthday.failure.invalid_sender_name'), flash[:alert]
      assert_equal @new_text, User.find_by_id(@user.id).birthday_message
      assert_equal Period::DEFAULT_HOUR, User.find_by_id(@user.id).birthday_hour
      assert_nil User.find_by_id(@user.id).birthday_sender_name
    end
  end

  test "should update for user with rejected sender_name" do
    rejected_sender_name_value = FactoryGirl.create(:sender_name_rejected_with_verified_value, user: @user).value

    signed(@user) do
      post :update, :template_birthday => {:birthday_message => @new_text, :birthday_hour => @invalid_hour, :birthday_sender_name => rejected_sender_name_value}
      assert_response :redirect
      assert_redirected_to show_template_birthday_path

      assert_equal I18n.t('template_birthday.failure.invalid_sender_name'), flash[:alert]
      assert_equal @new_text, User.find_by_id(@user.id).birthday_message
      assert_equal Period::DEFAULT_HOUR, User.find_by_id(@user.id).birthday_hour
      assert_nil User.find_by_id(@user.id).birthday_sender_name
    end
  end

  test "should update for user with not his own sender_name" do
    not_his_won_sender_name_value = FactoryGirl.create(:sender_name_accepted_with_verified_value, user: FactoryGirl.create(:user)).value

    signed(@user) do
      post :update, :template_birthday => {:birthday_message => @new_text, :birthday_hour => @invalid_hour, :birthday_sender_name => not_his_won_sender_name_value}
      assert_response :redirect
      assert_redirected_to show_template_birthday_path

      assert_equal I18n.t('template_birthday.failure.invalid_sender_name'), flash[:alert]
      assert_equal @new_text, User.find_by_id(@user.id).birthday_message
      assert_equal Period::DEFAULT_HOUR, User.find_by_id(@user.id).birthday_hour
      assert_nil User.find_by_id(@user.id).birthday_sender_name
    end
  end

end
