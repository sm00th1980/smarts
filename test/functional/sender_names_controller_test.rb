# -*- encoding : utf-8 -*-
require 'test_helper'

class SenderNamesControllerTest < ActionController::TestCase

  def setup
    clear_db

    @user = FactoryGirl.create(:user)
    FactoryGirl.create(:sender_name_accepted, user: @user)
    FactoryGirl.create(:sender_name_moderating, user: @user)
    FactoryGirl.create(:sender_name_rejected, user: @user)
  end

  #index
  test "should not get index for unsigned" do
    get :index
    assert_response :redirect
    assert_redirected_to new_user_session_path
    assert_nil assigns(:sender_names)
  end

  test "should get index signed" do
    signed(@user) do
      get :index
      assert_response :success
      assert_not_nil assigns(:sender_names)
      assert_equal @user.sender_names.order(:value), assigns(:sender_names)
    end
  end

end
