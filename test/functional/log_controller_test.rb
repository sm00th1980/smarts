# -*- encoding : utf-8 -*-
require 'test_helper'

class LogControllerTest < ActionController::TestCase
  include Devise::TestHelpers

  def setup
    clear_db
    @user = FactoryGirl.create(:user)
    @log = FactoryGirl.create(:log_with_chunks, user: @user)

    @send_log_success = @log.send_logs[0]
    @send_log_success.delivery_status = SendLogDeliveryStatus.delivered
    @send_log_success.save!

    @send_log_failure = @log.send_logs[-1]
    @send_log_failure.delivery_status = SendLogDeliveryStatus.rejected
    @send_log_failure.save!
  end

  #show
  test "should not show log for non_auth user" do
    get :show, id: @log
    assert_nil assigns(:send_logs)
    assert_redirected_to new_user_session_path
  end

  test "should show log for user" do
    signed(@user) do
      get :show, id: @log
      assert_response :success
      assert_not_nil assigns(:send_logs)
      assert_equal @log.send_logs.sort, assigns(:send_logs).sort
    end
  end

  test "should not show with blank id" do
    signed(@user) do
      get :show, id: ''
      assert_response :redirect
      assert_redirected_to logs_path

      assert_nil assigns(:send_logs)
      assert_equal I18n.t('log.failure.not_exist') % '', flash[:alert]
    end
  end

  test "should not show with not exist id" do
    signed(@user) do
      get :show, id: -1
      assert_response :redirect
      assert_redirected_to logs_path

      assert_nil assigns(:send_logs)
      assert_equal I18n.t('log.failure.not_exist') % -1, flash[:alert]
    end
  end

end
