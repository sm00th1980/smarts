# -*- encoding : utf-8 -*-
require 'test_helper'

class TariffsControllerTest < ActionController::TestCase
  def setup
    clear_db
  end

  #index
  test "should not get index for unsigned" do
    get :index
    assert_response :redirect
    assert_redirected_to new_user_session_path
    assert_nil assigns(:operators)
  end

  test "should get index for signed user" do
    signed do
      get :index
      assert_response :success
      assert_equal CellOperatorGroup.all.sort { |o1, o2| o1.name <=> o2.name }, assigns(:operators)
    end
  end
end
