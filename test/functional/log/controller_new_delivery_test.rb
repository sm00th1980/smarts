# -*- encoding : utf-8 -*-
require 'test_helper'

class Log::ControllerNewDeliveryTest < ActionController::TestCase
  tests LogController

  def setup
    Sidekiq::Testing.disable!
    clear_db

    @user = FactoryGirl.create(:user)
    @log = FactoryGirl.create(:log_with_not_delivered, user: @user)

    @params = {
        id: @log.id
    }

    stub_real_request
  end

  #new
  test "should not get new for unsigned" do
    new_delivery_not_created do
      get :new_delivery, @params
      assert_response :redirect
      assert_redirected_to new_user_session_path
    end
  end

  test "should create new delivery" do
    new_delivery_created do
      signed(@user) do
        get :new_delivery, @params
        assert_response :redirect
        assert_redirected_to logs_path

        assert_equal I18n.t('sms.started'), flash[:notice]
        assert_nil flash[:alert]
        assert_equal @log, assigns(:log)

        log = Log.last
        assert_equal LogType.by_not_delivered, log.type
        assert_equal @log.id, log.prev_log_id
        assert_equal @log.phones, log.phones
        assert_equal @log.group_ids, log.group_ids
        assert_equal @log.content, log.content
        assert_equal @log.sender_name_value, log.sender_name_value
      end
    end
  end

  #invalid
  test "should not create new delivery with not exist log" do
    new_delivery_not_created do
      signed(@user) do
        get :new_delivery, @params.merge!({id: -1})
        assert_response :redirect
        assert_redirected_to logs_path

        assert_equal I18n.t('log.failure.not_exist') % -1, flash[:alert]
        assert_nil flash[:notice]
        assert_nil assigns(:log)
      end
    end
  end

  test "should not create new delivery with invalid delivery_id" do
    new_delivery_not_created do
      signed(@user) do
        get :new_delivery, @params.merge!({id: 'vnfd'})
        assert_response :redirect
        assert_redirected_to logs_path

        assert_equal I18n.t('log.failure.not_exist') % 'vnfd', flash[:alert]
        assert_nil flash[:notice]
        assert_nil assigns(:log)
      end
    end
  end

  test "should not create new delivery for not own delivery_id" do
    user = FactoryGirl.create(:user)

    new_delivery_not_created do
      signed(user) do
        get :new_delivery, @params
        assert_response :redirect
        assert_redirected_to logs_path

        assert_equal I18n.t('log.failure.not_exist') % @params[:id], flash[:alert]
        assert_nil flash[:notice]
        assert_nil assigns(:log)
      end
    end
  end

  private
  def new_delivery_created(&block)
    assert_difference 'Log.count' do
      yield if block_given?
    end

    assert_equal LogStatus.fresh, Log.last.status
  end

  def new_delivery_not_created(&block)
    assert_no_difference 'Log.count' do
      yield if block_given?
    end
  end

end
