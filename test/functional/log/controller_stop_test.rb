# -*- encoding : utf-8 -*-
require 'test_helper'

class Log::ControllerStopTest < ActionController::TestCase
  tests LogController

  def setup
    clear_db

    @user = FactoryGirl.create(:user)

    log_fresh = FactoryGirl.create(:log_fresh_valid, user: @user, status: LogStatus.fresh)
    log_checking = FactoryGirl.create(:log_fresh_valid, user: @user, status: LogStatus.checking)
    log_ready_for_processing = FactoryGirl.create(:log_fresh_valid, user: @user, status: LogStatus.ready_for_processing)
    log_processing = FactoryGirl.create(:log_fresh_valid, user: @user, status: LogStatus.processing)

    log_completed = FactoryGirl.create(:log_fresh_valid, user: @user, status: LogStatus.completed)
    log_check_failed = FactoryGirl.create(:log_fresh_valid, user: @user, status: LogStatus.check_failed)
    log_stopped = FactoryGirl.create(:log_fresh_valid, user: @user, status: LogStatus.stopped)

    @stopable = [log_fresh, log_checking, log_ready_for_processing, log_processing]
    @non_stopable = [log_completed, log_check_failed, log_stopped]
  end

  test "should stop stopable logs" do
    signed(@user) do
      @stopable.each do |log|
        get :stop, id: log
        assert_response :redirect
        assert_redirected_to logs_path
        assert_equal I18n.t('log.stopping.success'), flash[:notice]
        assert_equal LogStatus.stopped, log.reload.status
      end
    end
  end

  test "should not stop non-stopable logs" do
    signed(@user) do
      @non_stopable.each do |log|
        _status = log.status
        get :stop, id: log
        assert_response :redirect
        assert_redirected_to logs_path
        assert_equal I18n.t('log.stopping.failure'), flash[:alert]
        assert_equal _status, log.reload.status
      end
    end
  end

end
