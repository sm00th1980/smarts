# -*- encoding : utf-8 -*-
require 'test_helper'

class Log::ControllerDeleteClientsTest < ActionController::TestCase
  tests LogController

  def setup
    clear_db
    @user = FactoryGirl.create(:user)
    FactoryGirl.create(:log_with_chunks, user: @user)
  end

  #delete_clients
  test 'should delete clients with undelivered message' do
    signed(@user) do
      log = FactoryGirl.create(:log_completed, user:@user)

      Log.any_instance.expects(:delete_clients).returns(5)
      get :delete_clients, id:log.id

      assert_response :redirect
      assert_redirected_to logs_path
      assert_equal I18n.t('log.delete_clients') % 5, flash[:notice]
    end
  end

end
