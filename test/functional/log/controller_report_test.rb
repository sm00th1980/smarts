# -*- encoding : utf-8 -*-
require 'test_helper'

class Log::ReportControllerTest < ActionController::TestCase
  tests LogController

  def setup
    clear_db

    @user = FactoryGirl.create(:user, contact_email: 'sm00th1980@mail.ru')
    @log = FactoryGirl.create(:log_with_chunks, user: @user, sender_name_value: '79379903835')

    @params = {id: @log.id}
  end

  #report
  test "should not get generate for unsigned" do
    get :report, @params
    assert_response :redirect
    assert_redirected_to new_user_session_path
  end

  test "should get generate signed" do
    signed(@user) do
      get :report, @params
      assert_response :redirect
      assert_redirected_to logs_path
      assert_equal I18n.t('report.generating') % @user.contact_email, flash[:notice]
      assert_equal Log.find(@log.id), assigns(:log)
    end
  end

  #invalid
  test "should not get generate without contact-email" do
    user = FactoryGirl.create(:user, contact_email: '')
    log = FactoryGirl.create(:log_with_chunks, user: user, sender_name_value: '79379903835')

    signed(user) do
      get :report, @params.merge!({id: log.id})
      assert_response :redirect
      assert_redirected_to logs_path
      assert_equal I18n.t('report.failure.contact_email_incorrect'), flash[:alert]
      assert_equal Log.find(log.id), assigns(:log)
    end
  end

  test "should not get generate with blank log-id" do
    signed(@user) do
      get :report, @params.merge!({id: ''})
      assert_response :redirect
      assert_redirected_to logs_path
      assert_equal I18n.t('log.failure.not_exist') % '', flash[:alert]
      assert_nil assigns(:log)
    end
  end

  test "should not get generate with not-exist log-id" do
    signed(@user) do
      get :report, @params.merge!({id: '-1'})
      assert_response :redirect
      assert_redirected_to logs_path
      assert_equal I18n.t('log.failure.not_exist') % '-1', flash[:alert]
      assert_nil assigns(:log)
    end
  end

end
