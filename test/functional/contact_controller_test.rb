# -*- encoding : utf-8 -*-
require 'test_helper'

class ContactControllerTest < ActionController::TestCase
  def setup
    clear_db

    @user = FactoryGirl.create(:user, contact_email: 'deyarov@gmail.com')

    @params = {
        contact_email: 'sm00th1980@mail.ru'
    }
  end

  #show
  test "should not get show for unsigned" do
    get :show
    assert_response :redirect
    assert_redirected_to new_user_session_path
  end

  test "should get show signed" do
    signed(@user) do
      get :show
      assert_response :success
    end
  end

  #update
  test "should not update for unsigned" do
    get :update, @params
    assert_response :redirect
    assert_redirected_to new_user_session_path
    assert_nil assigns(:contact_email)
  end

  test "should update contact-email for user" do
    signed(@user) do
      get :update, @params
      assert_response :redirect
      assert_equal I18n.t('contact.saved'), flash[:notice]
      assert_equal assigns(:contact_email), @params[:contact_email]
      assert_equal @params[:contact_email], @user.reload.contact_email
    end
  end

  #update invalid
  test "should not update when contact-email is blank" do
    signed(@user) do
      get :update, @params.merge!({contact_email: ''})
      assert_response :redirect
      assert_equal I18n.t('contact.failure.invalid') % '', flash[:alert]
      assert_nil assigns(:contact_email)
      assert_equal @user.contact_email, @user.reload.contact_email
    end
  end

  test "should not update when contact-email is not email" do
    signed(@user) do
      get :update, @params.merge!({contact_email: '-1'})
      assert_response :redirect
      assert_equal I18n.t('contact.failure.invalid') % '-1', flash[:alert]
      assert_nil assigns(:contact_email)
      assert_equal @user.contact_email, @user.reload.contact_email
    end
  end

end
