# -*- encoding : utf-8 -*-
require 'test_helper'

class ApiFaqControllerTest < ActionController::TestCase
  def setup
    clear_db
  end

  test 'should render api faq' do
    signed(FactoryGirl.create(:user)) do
      get :index
      assert_response :success
      assert_template 'index'
    end
  end

  test 'should redirect if not signed' do
    get :index
    assert_response :redirect
  end
end
