# -*- encoding : utf-8 -*-
require 'test_helper'

class ClientControllerTest < ActionController::TestCase
  include PhoneHelper

  def setup
    clear_db

    @user = FactoryGirl.create(:user)
    @user2 = FactoryGirl.create(:user)

    @params = {
        fio: 'Deyarov Ruslan',
        phone: Test::Helpers.random_phone_number,
        birthday: Date.today.strftime('%Y-%m-%d'),
        description: 'test',
        group_id: @user.groups.first.id
    }

    @partial_invalid_params = {
        fio: 'Deyarov Ruslan',
        phone: '123',
        birthday: 'vnfjkdnvf',
        description: 'test',
        group_id: @user.groups.first.id
    }
  end

  #birthday_congratulation
  test "should change birthday_congratulation to true" do
    client = @user.groups.first.clients.first
    client.birthday_congratulation = false
    client.birthday = Date.today - 30.years
    client.save

    signed(@user) do
      post :birthday_congratulation_update, id: client, client: {birthday_congratulation: 'true'}
      assert_response :success
      assert_equal 'saved', @response.body

      assert client.reload.birthday_congratulation
    end
  end

  test "should change birthday_congratulation to false" do
    client = @user.groups.first.clients.first
    client.birthday_congratulation = true
    client.save

    signed(@user) do
      post :birthday_congratulation_update, id: client, client: {birthday_congratulation: 'false'}
      assert_response :success
      assert_equal 'saved', @response.body
      refute client.reload.birthday_congratulation
    end
  end

  test "should not change birthday_congratulation for client with invalid birthday" do
    client = @user.groups.first.clients.first
    client.birthday_congratulation = false
    client.birthday = nil
    client.save

    signed(@user) do
      post :birthday_congratulation_update, id: client, client: {birthday_congratulation: 'true'}
      assert_response :success
      assert_equal 'invalid birthday', @response.body
      refute client.reload.birthday_congratulation
    end
  end

  test "should raise exception with invalid birthday_congratulation" do
    client = @user.groups.first.clients.first
    birthday_congratulation = 'bla-bla'

    signed(@user) do
      exception = assert_raises(RuntimeError) do
        post :birthday_congratulation_update, id: client, client: {birthday_congratulation: birthday_congratulation}
        assert_response :error
      end

      assert_equal "error on frontend - birthday_congratulation is <%s>, but should be true or false" % birthday_congratulation, exception.message
    end
  end

  #update
  test "should update client" do
    client = @user.groups.first.clients.first
    signed(@user) do
      post :update, id: client, client: @params.except!(:group_id)
      assert_response :redirect
      assert_redirected_to show_group_path(@user.groups.first)
      assert_equal I18n.t('client.success.updated'), flash[:notice]

      client.reload
      assert_equal @user, client.group.user
      assert_equal @user.groups.first, client.group

      assert_equal @params[:fio], client.fio
      assert_equal normalize_phone_number(@params[:phone]), client.phone
      assert_equal @params[:birthday].to_date, client.birthday
      assert_equal @params[:description], client.description
    end
  end

  test "should not update client for not exist client_id" do
    client = @user.groups.first.clients.first
    signed(@user) do
      post :update, id: -1, client: @params.except!(:group_id)
      assert_response :redirect
      assert_redirected_to groups_path
      assert_equal I18n.t('client.failure.not_exist') % -1, flash[:alert]
    end
  end

  test "should not update client with dublicate phone" do
    new_phone = '79379903835'
    client = @user.groups.first.clients.first
    old_phone = client.phone

    FactoryGirl.create(:client, group_id: @params[:group_id], phone: new_phone)

    signed(@user) do
      post :update, id: client, client: @params.except!(:group_id).merge!({phone: new_phone})
      assert_response :redirect
      assert_redirected_to show_group_path(client.group)
      assert_equal I18n.t('client.failure.phone_dublicate') % new_phone, flash[:alert]
      assert_equal old_phone, client.reload.phone, "номер телефона не должен измениться"
    end
  end

  test "should update client with same phone but other birthday" do
    client = @user.groups.first.clients.first
    orig_birthday = client.birthday

    signed(@user) do
      post :update, id: client, client: @params.except!(:group_id).merge!({phone: client.phone, birthday: orig_birthday + 1.day})
      assert_response :redirect
      assert_redirected_to show_group_path(client.group)
      assert_nil flash[:alert]
      assert_equal I18n.t('client.success.updated'), flash[:notice]

      assert_equal orig_birthday + 1.day, client.reload.birthday
    end
  end

  #create
  test "should create client with valid phone" do
    signed(@user) do
      client_created do
        post :create, client: @params.merge!({phone: '7(937)9903839'})
        assert_response :redirect
        assert_redirected_to show_group_path(@user.groups.first)
        assert_equal I18n.t('client.success.created'), flash[:notice]

        new_client = @user.groups.first.clients.last
        assert_equal @user, new_client.group.user
        assert_equal @user.groups.first, new_client.group
        assert_equal @params[:fio], new_client.fio
        assert_equal normalize_phone_number(@params[:phone]), new_client.phone
        assert_equal @params[:birthday].to_date, new_client.birthday
        assert_equal @params[:description], new_client.description
      end
    end
  end

  #invalid create
  test "should not create client with dublicate phone" do
    phone = '79379903835'

    FactoryGirl.create(:client, group_id: @params[:group_id], phone: phone)

    signed(@user) do
      client_not_created do
        post :create, client: @params.merge!({phone: phone})
        assert_response :redirect
        assert_redirected_to show_group_path(@params[:group_id])
        assert_equal I18n.t('client.failure.phone_dublicate') % phone, flash[:alert]
      end
    end
  end

  test "should not create client with not valid phone" do
    signed(@user) do
      client_not_created do
        post :create, client: @params.merge!({phone: '123'})
        assert_response :redirect
        assert_redirected_to show_group_path(@user.groups.first)
        assert_equal I18n.t('client.failure.phone_invalid') % '123', flash[:alert]
      end
    end
  end

  test "should not create client with partial invalid params" do
    signed(@user) do
      client_not_created do
        post :create, client: @partial_invalid_params
        assert_response :redirect
        assert_redirected_to show_group_path(@partial_invalid_params[:group_id])
        assert_equal I18n.t('client.failure.phone_invalid') % @partial_invalid_params[:phone], flash[:alert]
      end
    end
  end

  test "should not create client for unsigned" do
    client_not_created do
      post :create, client: @params
      assert_response :redirect
      assert_redirected_to new_user_session_path
    end
  end

  test "should not create client with not exist group_id" do
    signed(@user) do
      client_not_created do
        post :create, client: @params.merge!(group_id: -1)
        assert_response :redirect
        assert_redirected_to groups_path
        assert_equal I18n.t('group.not_exist') % -1, flash[:alert]
      end
    end
  end

  test "should not create client with not own group_id" do
    signed(@user) do
      client_not_created do
        post :create, client: @params.merge!(group_id: @user2.groups.first)
        assert_response :redirect
        assert_redirected_to groups_path
        assert_equal I18n.t('group.not_exist') % @user2.groups.first.id, flash[:alert]
      end
    end
  end

  test "should not create client with too long phone" do
    signed(@user) do
      client_not_created do
        post :create, client: @params.merge!(phone: '89277088889 89277060320 89277332139 89277602362 89276029019 89272959492 89279684469 89277101370 89270132011 89179252992 89277268036 89879110561 89272024947 89276070535 89276007114 89171453288 89272684588 89276047993 89608279814 89276599128 89272641280 89171549094 89277223551 89228784043 89608202894 89276179160 89063430099 89272673319')
        assert_response :redirect
        assert_redirected_to show_group_path(@partial_invalid_params[:group_id])
        assert_equal I18n.t('client.failure.phone_invalid') % '89277088889 89277060320 89277332139 89277602362 89276029019 89272959492 89279684469 89277101370 89270132011 89179252992 89277268036 89879110561 89272024947 89276070535 89276007114 89171453288 89272684588 89276047993 89608279814 89276599128 89272641280 89171549094 89277223551 89228784043 89608202894 89276179160 89063430099 89272673319', flash[:alert]
      end
    end
  end

  #destroy
  test "should not delete for unsigned" do
    client = @user.groups.first.clients.first
    client_not_deleted do
      post :destroy, id: client, client: {id: client}
      assert_response :redirect
      assert_redirected_to new_user_session_path
      assert_nil assigns(:client)
    end
  end

  test "should delete client" do
    client = @user.groups.first.clients.first
    client_deleted(client) do
      signed(@user) do
        post :destroy, id: client, client: {id: client}
        assert_response :redirect
        assert_redirected_to show_group_path(@user.groups.first)
        assert_equal I18n.t('client.success.deleted'), flash[:notice]
      end
    end
  end

  test "should not delete client with not exist id" do
    client_not_deleted do
      signed(@user) do
        post :destroy, id: -1, client: {id: -1}
        assert_response :redirect
        assert_redirected_to groups_path
        assert_equal I18n.t('client.failure.not_exist') % -1, flash[:alert]
      end
    end
  end

  test "should not delete client with invalid id" do
    client_not_deleted do
      signed(@user) do
        post :destroy, id: 'vfdvfd', client: {id: 'vfdvfd'}
        assert_response :redirect
        assert_redirected_to groups_path
        assert_equal I18n.t('client.failure.not_exist') % 'vfdvfd', flash[:alert]
      end
    end
  end

  test "should not delete not own client" do
    client = @user2.groups.first.clients.first
    client_not_deleted do
      signed(@user) do
        post :destroy, id: client, client: {id: client}
        assert_response :redirect
        assert_redirected_to groups_path
        assert_equal I18n.t('client.failure.not_exist') % client.id, flash[:alert]
      end
    end
  end

  private
  def client_deleted(client)
    assert_nothing_raised {
      assert_difference 'Client.count', -1 do
        yield

        assert_nil Client.find_by_id(client)
      end
    }
  end

  def client_not_deleted
    assert_nothing_raised {
      assert_no_difference 'Client.count' do
        yield
      end
    }
  end

  def client_created
    assert_nothing_raised {
      assert_difference 'Client.count' do
        yield
      end
    }
  end

  def client_not_created
    assert_nothing_raised {
      assert_no_difference 'Client.count' do
        yield
      end
    }
  end

end
