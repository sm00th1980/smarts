# -*- encoding : utf-8 -*-
require 'test_helper'

class AutodeliveryControllerTest < ActionController::TestCase
  def setup
    clear_db

    @auto_deliveries = [FactoryGirl.create(:auto_delivery), FactoryGirl.create(:auto_delivery_without_receivers), FactoryGirl.create(:auto_delivery_without_receivers_with_nil)]

    @ready_for_activation = [FactoryGirl.create(:auto_delivery_not_active)]
    @not_ready_for_activation = [
        FactoryGirl.create(:auto_delivery_without_start_date),
        FactoryGirl.create(:auto_delivery_without_receivers),
        FactoryGirl.create(:auto_delivery_without_receivers_with_nil),
        FactoryGirl.create(:auto_delivery_without_name),
        FactoryGirl.create(:auto_delivery_without_content),
        FactoryGirl.create(:auto_delivery_with_invalid_sender_name_value)
    ]

    @ready_for_deactivation = [
        FactoryGirl.create(:auto_delivery),
        FactoryGirl.create(:auto_delivery_active),
        FactoryGirl.create(:auto_delivery_without_overdue)
    ]

    @ready_for_run = [FactoryGirl.create(:auto_delivery), FactoryGirl.create(:auto_delivery_active), FactoryGirl.create(:auto_delivery_without_overdue)]
    @not_ready_for_run = [
        FactoryGirl.create(:auto_delivery_without_start_date),
        FactoryGirl.create(:auto_delivery_without_receivers),
        FactoryGirl.create(:auto_delivery_without_receivers_with_nil),
        FactoryGirl.create(:auto_delivery_without_name),
        FactoryGirl.create(:auto_delivery_without_content),
        FactoryGirl.create(:auto_delivery_with_overdue),
        FactoryGirl.create(:auto_delivery_with_invalid_sender_name_value)
    ]

    @not_exists = [-1, '']

    @valid_params_for_update_with_hour_period = {
        :period => FactoryGirl.create(:auto_delivery_period_every_hour).period,
        :description => 'description',
        :name => 'name',
        :minute => rand((0..59)),
        :start_date => (Time.now - 10.days).strftime('%Y-%m-%d'),
        :stop_date => (Time.now + 10.days).strftime('%Y-%m-%d'),
        :content => 'content'
    }

    @valid_params_for_update_with_day_period = {
        :period => FactoryGirl.create(:auto_delivery_period_every_day).period,
        :description => 'description',
        :name => 'name',
        :hour => rand((0..23)),
        :minute => rand((0..59)),
        :start_date => (Time.now - 10.days).strftime('%Y-%m-%d'),
        :stop_date => (Time.now + 10.days).strftime('%Y-%m-%d'),
        :content => 'content'
    }

    @valid_params_for_update_with_weekday_period = {
        :period => FactoryGirl.create(:auto_delivery_period_every_week).period,
        :description => 'description',
        :name => 'name',
        :week_day_id => WeekDay.days.map { |week_day| week_day.id }[rand(WeekDay.count)],
        :hour => rand((0..23)),
        :minute => rand((0..59)),
        :start_date => (Time.now - 10.days).strftime('%Y-%m-%d'),
        :stop_date => (Time.now + 10.days).strftime('%Y-%m-%d'),
        :content => 'content'
    }

    @valid_params_for_create = {
        :description => 'description',
        :name => 'name',
        :start_date => (Time.now - 10.days).strftime('%Y-%m-%d'),
        :stop_date => (Time.now + 10.days).strftime('%Y-%m-%d')
    }
  end

  #index
  test "should not get index for unsigned" do
    get :index
    assert_response :redirect
    assert_redirected_to new_user_session_path
    assert_nil assigns(:deliveries)
  end

  test "should get index" do
    @auto_deliveries.each do |auto_delivery|
      signed(auto_delivery.user) do
        get :index
        assert_response :success

        assert_not_nil assigns(:deliveries)
        assigns(:deliveries).each do |delivery|
          assert_equal auto_delivery.user, delivery.user
        end
      end
    end
  end

  #edit
  test "should not get edit for unsigned" do
    @auto_deliveries.each do |auto_delivery|
      get :show, :id => auto_delivery
      assert_response :redirect
      assert_redirected_to new_user_session_path
    end
  end

  test "should get edit" do
    @auto_deliveries.each do |auto_delivery|
      signed(auto_delivery.user) do
        get :show, :id => auto_delivery
        assert_response :success
      end
    end
  end

  test "should not get edit for not own" do
    @auto_deliveries.each do |auto_delivery|
      signed do
        get :show, :id => auto_delivery
        assert_response :redirect
        assert_redirected_to autodelivery_path
        assert_equal I18n.t('autodelivery.not_exist'), flash[:alert]
      end
    end
  end

  #activate
  test "should not activate for unsigned" do
    @ready_for_activation.each do |auto_delivery|
      refute auto_delivery.active?
      get :activate, :id => auto_delivery
      assert_response :redirect
      assert_redirected_to new_user_session_path

      refute auto_delivery.reload.active?
    end
  end

  test "should activate" do
    @ready_for_activation.each do |auto_delivery|
      signed(auto_delivery.user) do
        refute auto_delivery.active?
        get :activate, :id => auto_delivery
        assert_response :redirect
        assert_redirected_to autodelivery_path
        assert_equal I18n.t('autodelivery.activation'), flash[:notice]

        assert auto_delivery.reload.active?
      end
    end
  end

  test "should not activate not own" do
    @ready_for_activation.each do |auto_delivery|
      signed do
        refute auto_delivery.active?
        get :activate, :id => auto_delivery
        assert_response :redirect
        assert_redirected_to autodelivery_path
        assert_equal I18n.t('autodelivery.not_exist'), flash[:alert]

        refute auto_delivery.reload.active?
      end
    end
  end

  test "should not activate not ready for activation" do
    @not_ready_for_activation.each do |auto_delivery|
      signed(auto_delivery.user) do
        refute auto_delivery.active?
        get :activate, :id => auto_delivery
        assert_response :redirect
        assert_redirected_to autodelivery_path
        assert_equal I18n.t('autodelivery.failure.activation').split(':').first, flash[:alert].split(':').first

        refute auto_delivery.reload.active?
      end
    end
  end

  #deactivation
  test "should not deactivate for unsigned" do
    @ready_for_deactivation.each do |auto_delivery|
      assert auto_delivery.active?
      get :deactivate, :id => auto_delivery
      assert_response :redirect
      assert_redirected_to new_user_session_path

      assert auto_delivery.reload.active?
    end
  end

  test "should deactivate" do
    @ready_for_deactivation.each do |auto_delivery|
      signed(auto_delivery.user) do
        assert auto_delivery.active?
        get :deactivate, :id => auto_delivery
        assert_response :redirect
        assert_redirected_to autodelivery_path
        assert_equal I18n.t('autodelivery.deactivation'), flash[:notice]

        refute auto_delivery.reload.active?
      end
    end
  end

  test "should not deactivate not own" do
    @ready_for_deactivation.each do |auto_delivery|
      signed do
        assert auto_delivery.active?
        get :deactivate, :id => auto_delivery
        assert_response :redirect
        assert_redirected_to autodelivery_path
        assert_equal I18n.t('autodelivery.not_exist'), flash[:alert]

        assert auto_delivery.reload.active?
      end
    end
  end

  #run
  test "should not run for unsigned" do
    @ready_for_run.each do |auto_delivery|
      assert auto_delivery.valid_for_run?(true)
      get :run, :id => auto_delivery
      assert_response :redirect
      assert_redirected_to new_user_session_path
    end
  end

  test "should run" do
    @ready_for_run.each do |auto_delivery|
      signed(auto_delivery.user) do
        assert auto_delivery.valid_for_run?(true)
        get :run, :id => auto_delivery
        assert_response :redirect
        assert_redirected_to autodelivery_path
        assert_equal I18n.t('autodelivery.run'), flash[:notice]
      end
    end
  end

  test "should not run not ready for run" do
    @not_ready_for_run.each do |auto_delivery|
      signed(auto_delivery.user) do
        refute auto_delivery.valid_for_run?(true)
        get :run, :id => auto_delivery
        assert_response :redirect
        assert_redirected_to autodelivery_path
        assert_equal I18n.t('autodelivery.failure.run').split(':').first, flash[:alert].split(':').first
      end
    end
  end

  test "should not run not own" do
    @ready_for_run.each do |auto_delivery|
      signed do
        assert auto_delivery.valid_for_run?(true)
        get :run, :id => auto_delivery
        assert_response :redirect
        assert_redirected_to autodelivery_path
        assert_equal I18n.t('autodelivery.not_exist'), flash[:alert]
      end
    end
  end

  #delete
  test "should delete" do
    @auto_deliveries.each do |auto_delivery|
      signed(auto_delivery.user) do
        post :destroy, id: auto_delivery
        assert_response :redirect
        assert_redirected_to autodelivery_path
        assert_equal I18n.t('autodelivery.deleted'), flash[:notice]

        refute AutoDelivery.exists?(id: auto_delivery)
      end
    end
  end

  test "should not delete not exists" do
    @not_exists.each do |auto_delivery|
      signed do
        post :destroy, id: auto_delivery
        assert_response :redirect
        assert_redirected_to autodelivery_path
        assert_equal I18n.t('autodelivery.not_exist'), flash[:alert]
      end
    end
  end

  test "should not delete not own" do
    @auto_deliveries.each do |auto_delivery|
      signed do
        assert AutoDelivery.exists?(id: auto_delivery)
        post :destroy, :id => auto_delivery
        assert_response :redirect
        assert_redirected_to autodelivery_path
        assert_equal I18n.t('autodelivery.not_exist'), flash[:alert]

        assert AutoDelivery.exists?(id: auto_delivery)
      end
    end
  end

  #create
  test "should not create for unsigned" do
    assert_no_difference 'AutoDelivery.count' do
      post :create, :autodelivery => @valid_params_for_create
      assert_response :redirect
      assert_redirected_to new_user_session_path
    end
  end

  test "should create with valid params" do
    user = FactoryGirl.create(:user)
    assert_difference 'AutoDelivery.count' do
      signed(user) do
        post :create, :autodelivery => @valid_params_for_create
        assert_response :redirect

        autodelivery = AutoDelivery.last

        assert_redirected_to show_autodelivery_path(autodelivery)
        assert_equal I18n.t('autodelivery.created'), flash[:notice]

        new_auto_delivery = AutoDelivery.last
        assert_equal @valid_params_for_create[:name], new_auto_delivery.name
        assert_equal @valid_params_for_create[:description], new_auto_delivery.description
        assert_equal Date.parse(@valid_params_for_create[:start_date]), new_auto_delivery.start_date
        assert_equal Date.parse(@valid_params_for_create[:stop_date]), new_auto_delivery.stop_date
        assert_equal user, new_auto_delivery.user
        refute new_auto_delivery.active?

        assert_equal AutoDeliveryPeriod.every_day, new_auto_delivery.period
        assert_equal Period::DEFAULT_HOUR, new_auto_delivery.hour
        assert_equal Period::DEFAULT_MINUTE, new_auto_delivery.minute
      end
    end
  end

  test "should not create with blank name" do
    user = FactoryGirl.create(:user)
    assert_no_difference 'AutoDelivery.count' do
      signed(user) do
        post :create, :autodelivery => @valid_params_for_create.merge(:name => '')
        assert_response :redirect
        assert_redirected_to autodelivery_path
        assert_equal I18n.t('autodelivery.failure.blank_name'), flash[:alert]
      end
    end
  end

  test "should not create with nil name" do
    user = FactoryGirl.create(:user)
    assert_no_difference 'AutoDelivery.count' do
      signed(user) do
        post :create, :autodelivery => @valid_params_for_create.merge(:name => nil)
        assert_response :redirect
        assert_redirected_to autodelivery_path
        assert_equal I18n.t('autodelivery.failure.blank_name'), flash[:alert]
      end
    end
  end

  test "should not create without name" do
    user = FactoryGirl.create(:user)
    assert_no_difference 'AutoDelivery.count' do
      signed(user) do
        post :create, :autodelivery => @valid_params_for_create.except(:name)
        assert_response :redirect
        assert_redirected_to autodelivery_path
        assert_equal I18n.t('autodelivery.failure.blank_name'), flash[:alert]
      end
    end
  end

  #update
  test "should update with valid hour params" do
    @auto_deliveries.each do |auto_delivery|
      signed(auto_delivery.user) do

        auto_delivery.update_attribute(:sender_name_value, nil)

        template = FactoryGirl.create(:template, user: auto_delivery.user)

        params = @valid_params_for_update_with_hour_period.merge(
            client_ids: auto_delivery.user.clients.map { |client| "client_id=#{client.id}" },
            template_id: template.id,
            sender_name_value: FactoryGirl.create(:sender_name_accepted_with_verified_value, user: auto_delivery.user).value
        )

        post :update, :id => auto_delivery, :autodelivery => params
        assert_response :redirect
        assert_redirected_to autodelivery_path
        assert_nil flash[:alert]
        assert_equal I18n.t('autodelivery.updated'), flash[:notice]

        updated_auto_delivery = auto_delivery.reload
        assert_equal params[:name], updated_auto_delivery.name
        assert_equal params[:description], updated_auto_delivery.description
        assert_equal params[:content], updated_auto_delivery.content
        assert_equal Date.parse(params[:start_date]), updated_auto_delivery.start_date
        assert_equal Date.parse(params[:stop_date]), updated_auto_delivery.stop_date
        assert_equal params[:template_id], updated_auto_delivery.template_id
        assert_equal template.id, updated_auto_delivery.template_id
        assert_equal auto_delivery.user, updated_auto_delivery.user

        assert_equal params[:period], updated_auto_delivery.period.period
        assert_equal params[:minute], updated_auto_delivery.minute
        assert_nil updated_auto_delivery.hour
        assert_nil updated_auto_delivery.week_day_id

        assert_equal auto_delivery.user.clients.map { |client| client.id }, updated_auto_delivery.client_ids

        assert_equal params[:sender_name_value].to_s, updated_auto_delivery.sender_name_value.to_s
      end
    end
  end

  test "should update with valid day params" do

    @auto_deliveries.each do |auto_delivery|
      signed(auto_delivery.user) do

        auto_delivery.update_attribute(:sender_name_value, nil)
        template = FactoryGirl.create(:template, user: auto_delivery.user)

        params = @valid_params_for_update_with_day_period.merge(
            :client_ids => auto_delivery.user.clients.map { |client| "client_id=#{client.id}" },
            :template_id => template.id,
            sender_name_value: FactoryGirl.create(:sender_name_accepted_with_verified_value, user: auto_delivery.user).value
        )

        post :update, :id => auto_delivery, :autodelivery => params
        assert_response :redirect
        assert_redirected_to autodelivery_path
        assert_nil flash[:alert]
        assert_equal I18n.t('autodelivery.updated'), flash[:notice]

        updated_auto_delivery = auto_delivery.reload
        assert_equal params[:name], updated_auto_delivery.name
        assert_equal params[:description], updated_auto_delivery.description
        assert_equal params[:content], updated_auto_delivery.content
        assert_equal Date.parse(params[:start_date]), updated_auto_delivery.start_date
        assert_equal Date.parse(params[:stop_date]), updated_auto_delivery.stop_date
        assert_equal auto_delivery.user, updated_auto_delivery.user
        assert_equal params[:template_id], updated_auto_delivery.template_id
        assert_equal template.id, updated_auto_delivery.template_id

        assert_equal params[:period], updated_auto_delivery.period.period
        assert_equal params[:hour], updated_auto_delivery.hour
        assert_equal params[:minute], updated_auto_delivery.minute
        assert_nil updated_auto_delivery.week_day_id

        assert_equal auto_delivery.user.clients.map { |client| client.id }.sort, updated_auto_delivery.client_ids.sort
        assert_equal params[:sender_name_value].to_s, updated_auto_delivery.sender_name_value.to_s
      end
    end
  end

  test "should update with valid weekday params" do
    @auto_deliveries.each do |auto_delivery|
      signed(auto_delivery.user) do

        auto_delivery.update_attribute(:sender_name_value, nil)
        template = FactoryGirl.create(:template, user: auto_delivery.user)

        params = @valid_params_for_update_with_weekday_period.merge(
            :client_ids => auto_delivery.user.clients.map { |client| "client_id=#{client.id}" },
            :template_id => template.id,
            sender_name_value: FactoryGirl.create(:sender_name_accepted_with_verified_value, user: auto_delivery.user).value
        )

        post :update, :id => auto_delivery, :autodelivery => params
        assert_response :redirect
        assert_redirected_to autodelivery_path
        assert_nil flash[:alert]
        assert_equal I18n.t('autodelivery.updated'), flash[:notice]

        updated_auto_delivery = auto_delivery.reload
        assert_equal params[:name], updated_auto_delivery.name
        assert_equal params[:description], updated_auto_delivery.description
        assert_equal params[:content], updated_auto_delivery.content
        assert_equal Date.parse(params[:start_date]), updated_auto_delivery.start_date
        assert_equal Date.parse(params[:stop_date]), updated_auto_delivery.stop_date
        assert_equal auto_delivery.user, updated_auto_delivery.user
        assert_equal params[:template_id], updated_auto_delivery.template_id
        assert_equal template.id, updated_auto_delivery.template_id

        assert_equal params[:period], updated_auto_delivery.period.period
        assert_equal params[:week_day_id], updated_auto_delivery.week_day_id
        assert_equal params[:hour], updated_auto_delivery.hour
        assert_equal params[:minute], updated_auto_delivery.minute

        assert_equal auto_delivery.user.clients.map { |client| client.id }.sort, updated_auto_delivery.client_ids.sort
        assert_equal params[:sender_name_value].to_s, updated_auto_delivery.sender_name_value.to_s
      end
    end
  end

  test "should update with group_ids" do
    @auto_deliveries.each do |auto_delivery|
      signed(auto_delivery.user) do

        template = FactoryGirl.create(:template, user: auto_delivery.user)

        params = @valid_params_for_update_with_day_period.merge(
            :client_ids => auto_delivery.user.groups.map { |group| "group_id=#{group.id}" },
            :template_id => template.id
        )

        post :update, :id => auto_delivery, :autodelivery => params
        assert_response :redirect
        assert_redirected_to autodelivery_path
        assert_nil flash[:alert]
        assert_equal I18n.t('autodelivery.updated'), flash[:notice]

        updated_auto_delivery = auto_delivery.reload
        assert_equal params[:name], updated_auto_delivery.name
        assert_equal params[:description], updated_auto_delivery.description
        assert_equal params[:content], updated_auto_delivery.content
        assert_equal Date.parse(params[:start_date]), updated_auto_delivery.start_date
        assert_equal Date.parse(params[:stop_date]), updated_auto_delivery.stop_date
        assert_equal auto_delivery.user, updated_auto_delivery.user
        assert_equal params[:template_id], updated_auto_delivery.template_id
        assert_equal template.id, updated_auto_delivery.template_id

        assert_equal params[:period], updated_auto_delivery.period.period
        assert_equal params[:hour], updated_auto_delivery.hour
        assert_equal params[:minute], updated_auto_delivery.minute
        assert_nil updated_auto_delivery.week_day_id

        assert_equal auto_delivery.user.groups.map { |group| group.id }, updated_auto_delivery.group_ids
      end
    end
  end

  test "should update with group_ids and client_ids" do
    @auto_deliveries.each do |auto_delivery|
      signed(auto_delivery.user) do

        template = FactoryGirl.create(:template, user: auto_delivery.user)

        params = @valid_params_for_update_with_day_period.merge(
            :client_ids => ["group_id=#{auto_delivery.user.groups.first.id}", "client_id=#{auto_delivery.user.groups.last.clients.first.id}"],
            :template_id => template.id
        )

        post :update, :id => auto_delivery, :autodelivery => params
        assert_response :redirect
        assert_redirected_to autodelivery_path
        assert_nil flash[:alert]
        assert_equal I18n.t('autodelivery.updated'), flash[:notice]

        updated_auto_delivery = AutoDelivery.find_by(id: auto_delivery)
        assert_equal params[:name], updated_auto_delivery.name
        assert_equal params[:description], updated_auto_delivery.description
        assert_equal params[:content], updated_auto_delivery.content
        assert_equal Date.parse(params[:start_date]), updated_auto_delivery.start_date
        assert_equal Date.parse(params[:stop_date]), updated_auto_delivery.stop_date
        assert_equal auto_delivery.user, updated_auto_delivery.user
        assert_equal params[:template_id], updated_auto_delivery.template_id
        assert_equal template.id, updated_auto_delivery.template_id

        assert_equal params[:period], updated_auto_delivery.period.period
        assert_equal params[:hour], updated_auto_delivery.hour
        assert_equal params[:minute], updated_auto_delivery.minute
        assert_nil updated_auto_delivery.week_day_id

        assert_equal [auto_delivery.user.groups.first.id], updated_auto_delivery.group_ids
        assert_equal [auto_delivery.user.groups.last.clients.first.id], updated_auto_delivery.client_ids
      end
    end
  end


  test "should update with not exist template" do
    @auto_deliveries.each do |auto_delivery|
      signed(auto_delivery.user) do

        params = @valid_params_for_update_with_day_period.merge(
            :client_ids => ["group_id=#{auto_delivery.user.groups.first.id}", "client_id=#{auto_delivery.user.groups.last.clients.first.id}"],
            :template_id => -1
        )

        post :update, :id => auto_delivery, :autodelivery => params
        assert_response :redirect
        assert_redirected_to autodelivery_path
        assert_nil flash[:alert]
        assert_equal I18n.t('autodelivery.updated'), flash[:notice]

        updated_auto_delivery = AutoDelivery.find_by(id: auto_delivery)
        assert_equal params[:name], updated_auto_delivery.name
        assert_equal params[:description], updated_auto_delivery.description
        assert_equal params[:content], updated_auto_delivery.content
        assert_equal Date.parse(params[:start_date]), updated_auto_delivery.start_date
        assert_equal Date.parse(params[:stop_date]), updated_auto_delivery.stop_date
        assert_equal auto_delivery.user, updated_auto_delivery.user

        assert_nil updated_auto_delivery.template_id

        assert_equal params[:period], updated_auto_delivery.period.period
        assert_equal params[:hour], updated_auto_delivery.hour
        assert_equal params[:minute], updated_auto_delivery.minute
        assert_nil updated_auto_delivery.week_day_id

        assert_equal [auto_delivery.user.groups.first.id], updated_auto_delivery.group_ids
        assert_equal [auto_delivery.user.groups.last.clients.first.id], updated_auto_delivery.client_ids
      end
    end
  end

  test "should update with not his own template" do
    @auto_deliveries.each do |auto_delivery|
      signed(auto_delivery.user) do

        template = FactoryGirl.create(:template, user: FactoryGirl.create(:user))

        params = @valid_params_for_update_with_day_period.merge(
            :client_ids => ["group_id=#{auto_delivery.user.groups.first.id}", "client_id=#{auto_delivery.user.groups.last.clients.first.id}"],
            :template_id => template.id
        )

        post :update, :id => auto_delivery, :autodelivery => params
        assert_response :redirect
        assert_redirected_to autodelivery_path
        assert_nil flash[:alert]
        assert_equal I18n.t('autodelivery.updated'), flash[:notice]

        updated_auto_delivery = AutoDelivery.find_by(id: auto_delivery)
        assert_equal params[:name], updated_auto_delivery.name
        assert_equal params[:description], updated_auto_delivery.description
        assert_equal params[:content], updated_auto_delivery.content
        assert_equal Date.parse(params[:start_date]), updated_auto_delivery.start_date
        assert_equal Date.parse(params[:stop_date]), updated_auto_delivery.stop_date
        assert_equal auto_delivery.user, updated_auto_delivery.user

        assert_nil updated_auto_delivery.template_id

        assert_equal params[:period], updated_auto_delivery.period.period
        assert_equal params[:hour], updated_auto_delivery.hour
        assert_equal params[:minute], updated_auto_delivery.minute
        assert_nil updated_auto_delivery.week_day_id

        assert_equal [auto_delivery.user.groups.first.id], updated_auto_delivery.group_ids
        assert_equal [auto_delivery.user.groups.last.clients.first.id], updated_auto_delivery.client_ids
      end
    end
  end
end
