# -*- encoding : utf-8 -*-
require 'test_helper'

class Admin::UserUpdateControllerTest < ActionController::TestCase
  tests Admin::UserController

  def setup
    clear_db

    @admin = FactoryGirl.create(:admin)
    @user = FactoryGirl.create(:user)
    @user2 = FactoryGirl.create(:user)
    @user_charge_by_sent = FactoryGirl.create(:user_charge_by_sent)
    @user_charge_by_delivered = FactoryGirl.create(:user_charge_by_delivered)

    @user_prepaid = FactoryGirl.create(:user_prepaid)
    @user_postpaid = FactoryGirl.create(:user_postpaid)

    @params = {
        status: 'active',
        email: 'sm00th1980@mail.ru',
        fio: 'Deyarov Ruslan',
        description: 'New User',
        type_id: UserType.business.id,
        status_id: UserStatus.active.id,
        charge_type_id: ChargeType.by_sent.id,
        payment_type_id: PaymentType.prepaid.id,
        permit_night_delivery: 'reject',
        discount: 10,
        permit_alpha_sender_name: 'permit',
        permit_def_sender_name: 'permit',
        reject_to_megafon: 'permit',
        alpha_megafon_channel_id: Channel.gold.id,
        reject_to_smarts_via_gold_channel: 'permit',
        reject_to_mts: 'permit'
    }
  end

  #update
  test "should not update user for unsigned" do
    check_no_new_user do
      post :update, id: @user, user: @params
      assert_equal I18n.t('devise.failure.user.unauthenticated'), flash[:alert]
      check_no_assigns
      assert_redirected_to new_user_session_path
    end
  end

  test "should not update user for not admin" do
    check_no_new_user do
      signed(@user) do
        post :update, id: @user, user: @params
        assert_equal I18n.t('failure.admin_only'), flash[:alert]
        check_no_assigns
        assert_redirected_to root_path
      end
    end
  end

  #success update
  test "should update user for admin" do
    check_no_new_user do
      signed(@admin) do
        post :update, id: @user, user: @params
        assert_nil flash[:alert]
        assert_equal I18n.t('user.success.updated'), flash[:notice]
        assert_response :redirect
        assert_redirected_to users_path
        check_assigns(@params)

        #check user
        @user.reload
        assert_not_nil @user
        assert_equal @params[:fio], @user.fio
        assert_equal @params[:description], @user.description
        assert_equal @params[:type_id], @user.type.id
        assert_equal @params[:charge_type_id], @user.charge_type.id
        assert_equal @params[:payment_type_id], @user.payment_type.id
        refute @user.permit_night_delivery?
        assert_equal @params[:discount], @user.discount
        assert @user.permit_alpha_sender_name?
        assert @user.permit_def_sender_name?
        refute @user.reject_to_megafon?
        refute @user.reject_to_mts?

        assert @user.status.active?
      end
    end
  end

  test "should update user_charge_by_sent to user_charge_by_delivered for admin" do
    check_no_new_user do
      signed(@admin) do
        post :update, id: @user_charge_by_sent, user: @params.merge!({charge_type_id: ChargeType.by_delivered.id})
        assert_nil flash[:alert]
        assert_equal I18n.t('user.success.updated'), flash[:notice]
        assert_response :redirect
        assert_redirected_to users_path
        check_assigns(@params)

        #check user
        user = @user_charge_by_sent.reload
        assert_not_nil user
        assert_equal @params[:fio], user.fio
        assert_equal @params[:description], user.description
        assert_equal @params[:type_id], user.type.id
        assert_equal @params[:charge_type_id], user.charge_type.id
        assert_equal ChargeType.by_delivered.id, user.charge_type.id
        assert_equal @params[:payment_type_id], user.payment_type.id
        refute user.permit_night_delivery?
        assert_equal @params[:discount], user.discount
        assert user.permit_alpha_sender_name?
        assert user.permit_def_sender_name?
        refute user.reject_to_megafon?
        refute @user.reject_to_mts?

        assert user.status.active?
      end
    end
  end

  test "should update user_charge_by_delivered to user_charge_by_sent for admin" do
    check_no_new_user do
      signed(@admin) do
        post :update, id: @user_charge_by_delivered, user: @params.merge!({charge_type_id: ChargeType.by_sent.id})
        assert_nil flash[:alert]
        assert_equal I18n.t('user.success.updated'), flash[:notice]
        assert_response :redirect
        assert_redirected_to users_path
        check_assigns(@params)

        #check user
        user = @user_charge_by_delivered.reload
        assert_not_nil user
        assert_equal @params[:fio], user.fio
        assert_equal @params[:description], user.description
        assert_equal @params[:type_id], user.type.id
        assert_equal @params[:charge_type_id], user.charge_type.id
        assert_equal ChargeType.by_sent.id, user.charge_type.id
        assert_equal @params[:payment_type_id], user.payment_type.id
        refute user.permit_night_delivery?
        assert_equal @params[:discount], user.discount
        assert user.permit_alpha_sender_name?
        assert user.permit_def_sender_name?
        refute user.reject_to_megafon?

        assert user.status.active?
      end
    end
  end

  test "should update prepaid user to postpaid for admin" do
    check_no_new_user do
      signed(@admin) do
        post :update, id: @user_prepaid, user: @params.merge!({payment_type_id: PaymentType.postpaid.id})
        assert_nil flash[:alert]
        assert_equal I18n.t('user.success.updated'), flash[:notice]
        assert_response :redirect
        assert_redirected_to users_path
        check_assigns(@params)

        #check user
        user = @user_prepaid.reload
        assert_not_nil user
        assert_equal @params[:fio], user.fio
        assert_equal @params[:description], user.description
        assert_equal @params[:type_id], user.type.id
        assert_equal @params[:charge_type_id], user.charge_type.id

        assert_equal @params[:payment_type_id], user.payment_type.id
        assert_equal PaymentType.postpaid.id, user.payment_type.id

        refute user.permit_night_delivery?
        assert_equal @params[:discount], user.discount

        assert user.permit_alpha_sender_name?
        assert user.permit_def_sender_name?

        refute user.reject_to_megafon?

        assert user.status.active?
      end
    end
  end

  test "should update postpaid user to prepaid for admin" do
    check_no_new_user do
      signed(@admin) do
        post :update, id: @user_postpaid, user: @params.merge!({payment_type_id: PaymentType.prepaid.id})
        assert_nil flash[:alert]
        assert_equal I18n.t('user.success.updated'), flash[:notice]
        assert_response :redirect
        assert_redirected_to users_path
        check_assigns(@params)

        #check user
        user = @user_postpaid.reload
        assert_not_nil user
        assert_equal @params[:fio], user.fio
        assert_equal @params[:description], user.description
        assert_equal @params[:type_id], user.type.id
        assert_equal @params[:charge_type_id], user.charge_type.id

        assert_equal @params[:payment_type_id], user.payment_type.id
        assert_equal PaymentType.prepaid.id, user.payment_type.id

        refute user.permit_night_delivery?
        assert_equal @params[:discount], user.discount

        assert user.permit_alpha_sender_name?
        assert user.permit_def_sender_name?

        refute user.reject_to_megafon?

        assert user.status.active?
      end
    end
  end

  test "should block user for admin" do
    check_no_new_user do
      signed(@admin) do
        post :update, id: @user, user: @params.merge!({status_id: UserStatus.blocked.id})
        assert_nil flash[:alert]
        assert_equal I18n.t('user.success.updated'), flash[:notice]
        assert_response :redirect
        assert_redirected_to users_path
        check_assigns(@params)

        #check user
        user = @user.reload
        assert_not_nil user
        assert_equal @params[:fio], user.fio
        assert_equal @params[:description], user.description
        assert_equal @params[:type_id], user.type.id
        assert_equal @params[:charge_type_id], user.charge_type.id

        refute user.permit_night_delivery?
        assert_equal @params[:discount], user.discount

        assert user.permit_alpha_sender_name?
        assert user.permit_def_sender_name?

        refute user.reject_to_megafon?

        refute user.status.active?
        assert user.status.blocked?
      end
    end
  end

  test "should delete user for admin" do
    check_no_new_user do
      signed(@admin) do
        post :update, id: @user, user: @params.merge!({status_id: UserStatus.deleted.id})
        assert_nil flash[:alert]
        assert_equal I18n.t('user.success.updated'), flash[:notice]
        assert_response :redirect
        assert_redirected_to users_path
        check_assigns(@params)

        #check user
        user = @user.reload
        assert_not_nil user
        assert_equal @params[:fio], user.fio
        assert_equal @params[:description], user.description
        assert_equal @params[:type_id], user.type.id
        assert_equal @params[:charge_type_id], user.charge_type.id

        refute user.permit_night_delivery?
        assert_equal @params[:discount], user.discount

        assert user.permit_alpha_sender_name?
        assert user.permit_def_sender_name?

        refute user.reject_to_megafon?

        refute user.status.active?
        assert user.status.deleted?
      end
    end
  end

  test "should update user into demo for admin" do
    check_no_new_user do
      signed(@admin) do
        post :update, id: @user, user: @params.merge!({type_id: UserType.demo.id})
        assert_nil flash[:alert]
        assert_equal I18n.t('user.success.updated'), flash[:notice]
        assert_response :redirect
        assert_redirected_to users_path
        check_assigns(@params)

        #check user
        user = @user.reload
        assert_not_nil user
        assert_equal @params[:fio], user.fio
        assert_equal @params[:description], user.description
        assert_equal @params[:type_id], user.type.id
        assert_equal @params[:charge_type_id], user.charge_type.id

        refute user.permit_night_delivery?
        assert_equal @params[:discount], user.discount

        assert user.permit_alpha_sender_name?
        assert user.permit_def_sender_name?

        refute user.reject_to_megafon?

        assert user.status.active?
      end
    end
  end

  test "should update user into operator for admin" do
    check_no_new_user do
      signed(@admin) do
        post :update, id: @user, user: @params.merge!({type_id: UserType.operator.id})
        assert_nil flash[:alert]
        assert_equal I18n.t('user.success.updated'), flash[:notice]
        assert_response :redirect
        assert_redirected_to users_path
        check_assigns(@params)

        #check user
        user = @user.reload
        assert_not_nil user
        assert_equal @params[:fio], user.fio
        assert_equal @params[:description], user.description
        assert_equal @params[:type_id], user.type.id
        assert_equal @params[:charge_type_id], user.charge_type.id

        refute user.permit_night_delivery?
        assert_equal @params[:discount], user.discount

        assert user.permit_alpha_sender_name?
        assert user.permit_def_sender_name?

        refute user.reject_to_megafon?

        assert user.status.active?
      end
    end
  end

  test "should update user without change email" do
    check_no_new_user do
      signed(@admin) do
        post :update, id: @user, user: @params.merge!({type_id: UserType.operator.id, email: @user.email})
        assert_nil flash[:alert]
        assert_equal I18n.t('user.success.updated'), flash[:notice]
        assert_response :redirect
        assert_redirected_to users_path
        check_assigns(@params)

        #check user
        user = @user.reload
        assert_not_nil user
        assert_equal @params[:fio], user.fio
        assert_equal @params[:description], user.description
        assert_equal @params[:type_id], user.type.id
        assert_equal @params[:email], user.email
        assert_equal @params[:charge_type_id], user.charge_type.id

        refute user.permit_night_delivery?
        assert_equal @params[:discount], user.discount

        assert user.permit_alpha_sender_name?
        assert user.permit_def_sender_name?

        refute user.reject_to_megafon?

        assert user.status.active?
      end
    end
  end

  test "should update user for admin for permit_night_delivery from permit to reject" do
    @user.update_attribute(:permit_night_delivery, true)

    check_no_new_user do
      signed(@admin) do
        post :update, id: @user, user: @params.merge!({permit_night_delivery: 'reject'})
        assert_nil flash[:alert]
        assert_equal I18n.t('user.success.updated'), flash[:notice]
        assert_response :redirect
        assert_redirected_to users_path
        check_assigns(@params)

        #check user
        user = @user.reload
        assert_not_nil user
        assert_equal @params[:fio], user.fio
        assert_equal @params[:description], user.description
        assert_equal @params[:type_id], user.type.id
        assert_equal @params[:charge_type_id], user.charge_type.id
        assert_equal @params[:payment_type_id], user.payment_type.id
        refute user.permit_night_delivery?
        assert_equal @params[:discount], user.discount

        assert user.permit_alpha_sender_name?
        assert user.permit_def_sender_name?

        refute user.reject_to_megafon?

        assert user.status.active?
      end
    end
  end

  test "should update user for admin for permit_night_delivery from reject to permit" do
    @user.update_attribute(:permit_night_delivery, false)

    check_no_new_user do
      signed(@admin) do
        post :update, id: @user, user: @params.merge!({permit_night_delivery: 'permit'})
        assert_nil flash[:alert]
        assert_equal I18n.t('user.success.updated'), flash[:notice]
        assert_response :redirect
        assert_redirected_to users_path
        check_assigns(@params)

        #check user
        user = @user.reload
        assert_not_nil user
        assert_equal @params[:fio], user.fio
        assert_equal @params[:description], user.description
        assert_equal @params[:type_id], user.type.id
        assert_equal @params[:charge_type_id], user.charge_type.id
        assert_equal @params[:payment_type_id], user.payment_type.id
        assert user.permit_night_delivery?
        assert_equal @params[:discount], user.discount

        assert user.permit_alpha_sender_name?
        assert user.permit_def_sender_name?

        refute user.reject_to_megafon?

        assert user.status.active?
      end
    end
  end

  test "should update user for admin for reject_to_megafon_from permit to reject" do
    @user.update_attribute(:reject_to_megafon, false)

    check_no_new_user do
      signed(@admin) do
        post :update, id: @user, user: @params.merge!({reject_to_megafon: 'reject'})
        assert_nil flash[:alert]
        assert_equal I18n.t('user.success.updated'), flash[:notice]
        assert_response :redirect
        assert_redirected_to users_path
        check_assigns(@params)

        #check user
        user = @user.reload
        assert_not_nil user
        assert_equal @params[:fio], user.fio
        assert_equal @params[:description], user.description
        assert_equal @params[:type_id], user.type.id
        assert_equal @params[:charge_type_id], user.charge_type.id
        assert_equal @params[:payment_type_id], user.payment_type.id
        refute user.permit_night_delivery?
        assert_equal @params[:discount], user.discount

        assert user.permit_alpha_sender_name?
        assert user.permit_def_sender_name?

        assert user.reject_to_megafon?

        assert user.status.active?
      end
    end
  end

  test "should update user for admin for reject_to_megafon from reject to permit" do
    @user.update_attribute(:reject_to_megafon, true)

    check_no_new_user do
      signed(@admin) do
        post :update, id: @user, user: @params.merge!({reject_to_megafon: 'permit'})
        assert_nil flash[:alert]
        assert_equal I18n.t('user.success.updated'), flash[:notice]
        assert_response :redirect
        assert_redirected_to users_path
        check_assigns(@params)

        #check user
        user = @user.reload
        assert_not_nil user
        assert_equal @params[:fio], user.fio
        assert_equal @params[:description], user.description
        assert_equal @params[:type_id], user.type.id
        assert_equal @params[:charge_type_id], user.charge_type.id
        assert_equal @params[:payment_type_id], user.payment_type.id
        refute user.permit_night_delivery?
        assert_equal @params[:discount], user.discount

        assert user.permit_alpha_sender_name?
        assert user.permit_def_sender_name?

        refute user.reject_to_megafon?

        assert user.status.active?
      end
    end
  end

  ######

  test "should update user for admin for reject_to_mts_from permit to reject" do
    @user.update_attribute(:reject_to_mts, false)

    check_no_new_user do
      signed(@admin) do
        post :update, id: @user, user: @params.merge!({reject_to_mts: 'reject'})
        assert_nil flash[:alert]
        assert_equal I18n.t('user.success.updated'), flash[:notice]
        assert_response :redirect
        assert_redirected_to users_path
        check_assigns(@params)

        #check user
        user = @user.reload
        assert_not_nil user
        assert_equal @params[:fio], user.fio
        assert_equal @params[:description], user.description
        assert_equal @params[:type_id], user.type.id
        assert_equal @params[:charge_type_id], user.charge_type.id
        assert_equal @params[:payment_type_id], user.payment_type.id
        refute user.permit_night_delivery?
        assert_equal @params[:discount], user.discount

        assert user.permit_alpha_sender_name?
        assert user.permit_def_sender_name?

        assert user.reject_to_mts?

        assert user.status.active?
      end
    end
  end

  test "should update user for admin for reject_to_mts from reject to permit" do
    @user.update_attribute(:reject_to_mts, true)

    check_no_new_user do
      signed(@admin) do
        post :update, id: @user, user: @params.merge!({reject_to_mts: 'permit'})
        assert_nil flash[:alert]
        assert_equal I18n.t('user.success.updated'), flash[:notice]
        assert_response :redirect
        assert_redirected_to users_path
        check_assigns(@params)

        #check user
        user = @user.reload
        assert_not_nil user
        assert_equal @params[:fio], user.fio
        assert_equal @params[:description], user.description
        assert_equal @params[:type_id], user.type.id
        assert_equal @params[:charge_type_id], user.charge_type.id
        assert_equal @params[:payment_type_id], user.payment_type.id
        refute user.permit_night_delivery?
        assert_equal @params[:discount], user.discount

        assert user.permit_alpha_sender_name?
        assert user.permit_def_sender_name?

        refute user.reject_to_mts?

        assert user.status.active?
      end
    end
  end

  ######

  test "should update user for admin for reject_to_smarts_from permit to reject" do
    @user.update_attribute(:reject_to_smarts_via_gold_channel, false)

    check_no_new_user do
      signed(@admin) do
        post :update, id: @user, user: @params.merge!({reject_to_smarts_via_gold_channel: 'reject'})
        assert_nil flash[:alert]
        assert_equal I18n.t('user.success.updated'), flash[:notice]
        assert_response :redirect
        assert_redirected_to users_path
        check_assigns(@params)

        assert @user.reload.reject_to_smarts_via_gold_channel?
      end
    end
  end

  test "should update user for admin for reject_to_smarts_from reject to permit" do
    @user.update_attribute(:reject_to_smarts_via_gold_channel, true)

    check_no_new_user do
      signed(@admin) do
        post :update, id: @user, user: @params.merge!({reject_to_smarts_via_gold_channel: 'permit'})
        assert_nil flash[:alert]
        assert_equal I18n.t('user.success.updated'), flash[:notice]
        assert_response :redirect
        assert_redirected_to users_path
        check_assigns(@params)

        refute @user.reload.reject_to_smarts_via_gold_channel?
      end
    end
  end

  test "should update user for admin from alpha megafon fixed channel to multi" do
    @user.update_attribute(:alpha_megafon_channel, Channel.megafon_fixed)

    check_no_new_user do
      signed(@admin) do
        post :update, id: @user, user: @params.merge!({alpha_megafon_channel_id: Channel.megafon_multi.id})
        assert_nil flash[:alert]
        assert_equal I18n.t('user.success.updated'), flash[:notice]
        assert_response :redirect
        assert_redirected_to users_path
        check_assigns(@params)

        #check user
        user = @user.reload
        assert_not_nil user
        assert_equal @params[:fio], user.fio
        assert_equal @params[:description], user.description
        assert_equal @params[:type_id], user.type.id
        assert_equal @params[:charge_type_id], user.charge_type.id
        assert_equal @params[:payment_type_id], user.payment_type.id
        refute user.permit_night_delivery?
        assert_equal @params[:discount], user.discount

        assert user.permit_alpha_sender_name?
        assert user.permit_def_sender_name?

        refute user.reject_to_megafon?
        assert user.alpha_megafon_channel.megafon_multi?

        assert user.status.active?
      end
    end
  end

  test "should update user for admin from alpha megafon multi channel to fixed" do
    @user.update_attribute(:alpha_megafon_channel, Channel.megafon_multi)

    check_no_new_user do
      signed(@admin) do
        post :update, id: @user, user: @params.merge!({alpha_megafon_channel_id: Channel.megafon_fixed.id})
        assert_nil flash[:alert]
        assert_equal I18n.t('user.success.updated'), flash[:notice]
        assert_response :redirect
        assert_redirected_to users_path
        check_assigns(@params)

        #check user
        user = @user.reload
        assert_not_nil user
        assert_equal @params[:fio], user.fio
        assert_equal @params[:description], user.description
        assert_equal @params[:type_id], user.type.id
        assert_equal @params[:charge_type_id], user.charge_type.id
        assert_equal @params[:payment_type_id], user.payment_type.id
        refute user.permit_night_delivery?
        assert_equal @params[:discount], user.discount

        assert user.permit_alpha_sender_name?
        assert user.permit_def_sender_name?

        refute user.reject_to_megafon?
        assert user.alpha_megafon_channel.megafon_fixed?

        assert user.status.active?
      end
    end
  end

  test "should update user from zero-discount to non-zero-discount" do
    @user.update_attribute(:discount, 0)

    check_no_new_user do
      signed(@admin) do
        post :update, id: @user, user: @params.merge!({discount: 10})
        assert_nil flash[:alert]
        assert_equal I18n.t('user.success.updated'), flash[:notice]
        assert_response :redirect
        assert_redirected_to users_path
        check_assigns(@params)

        #check user
        user = @user.reload
        assert_not_nil user
        assert_equal @params[:fio], user.fio
        assert_equal @params[:description], user.description
        assert_equal @params[:type_id], user.type.id
        assert_equal @params[:charge_type_id], user.charge_type.id
        assert_equal @params[:payment_type_id], user.payment_type.id
        refute user.permit_night_delivery?
        assert_equal @params[:discount], user.discount

        assert user.permit_alpha_sender_name?
        assert user.permit_def_sender_name?

        refute user.reject_to_megafon?

        assert user.status.active?
      end
    end
  end

  test "should update user from non-zero-discount to zero-discount" do
    @user.update_attribute(:discount, 10)

    check_no_new_user do
      signed(@admin) do
        post :update, id: @user, user: @params.merge!({discount: 0})
        assert_nil flash[:alert]
        assert_equal I18n.t('user.success.updated'), flash[:notice]
        assert_response :redirect
        assert_redirected_to users_path
        check_assigns(@params)

        #check user
        user = @user.reload
        assert_not_nil user
        assert_equal @params[:fio], user.fio
        assert_equal @params[:description], user.description
        assert_equal @params[:type_id], user.type.id
        assert_equal @params[:charge_type_id], user.charge_type.id
        assert_equal @params[:payment_type_id], user.payment_type.id
        refute user.permit_night_delivery?
        assert_equal @params[:discount], user.discount

        assert user.permit_alpha_sender_name?
        assert user.permit_def_sender_name?

        refute user.reject_to_megafon?

        assert user.status.active?
      end
    end
  end

  test "should update user to reject alpha sender name" do
    check_no_new_user do
      signed(@admin) do
        post :update, id: @user, user: @params.merge!({permit_alpha_sender_name: 'reject'})
        assert_nil flash[:alert]
        assert_equal I18n.t('user.success.updated'), flash[:notice]
        assert_response :redirect
        assert_redirected_to users_path
        check_assigns(@params)

        #check user
        user = @user.reload
        assert_not_nil user
        assert_equal @params[:fio], user.fio
        assert_equal @params[:description], user.description
        assert_equal @params[:type_id], user.type.id
        assert_equal @params[:charge_type_id], user.charge_type.id
        assert_equal @params[:payment_type_id], user.payment_type.id
        refute user.permit_night_delivery?
        assert_equal @params[:discount], user.discount

        refute user.permit_alpha_sender_name?
        assert user.permit_def_sender_name?

        refute user.reject_to_megafon?

        assert user.status.active?
      end
    end
  end

  test "should update user to reject def sender name" do
    check_no_new_user do
      signed(@admin) do
        post :update, id: @user, user: @params.merge!({permit_def_sender_name: 'reject'})
        assert_nil flash[:alert]
        assert_equal I18n.t('user.success.updated'), flash[:notice]
        assert_response :redirect
        assert_redirected_to users_path
        check_assigns(@params)

        #check user
        user = @user.reload
        assert_not_nil user
        assert_equal @params[:fio], user.fio
        assert_equal @params[:description], user.description
        assert_equal @params[:type_id], user.type.id
        assert_equal @params[:charge_type_id], user.charge_type.id
        assert_equal @params[:payment_type_id], user.payment_type.id
        refute user.permit_night_delivery?
        assert_equal @params[:discount], user.discount

        assert user.permit_alpha_sender_name?
        refute user.permit_def_sender_name?

        refute user.reject_to_megafon?

        assert user.status.active?
      end
    end
  end

  #invalid email
  test "should not update user without email" do
    check_no_new_user do
      signed(@admin) do
        post :update, id: @user, user: @params.except(:email)
        assert_equal I18n.t('user.failure.login_is_not_email') % '', flash[:alert]
        assert_redirected_to show_user_path(@user)
        check_no_user_update(@user)
      end
    end
  end

  test "should not update user with blank email" do
    check_no_new_user do
      signed(@admin) do
        post :update, :id => @user, user: @params.merge({email: ''})
        assert_equal I18n.t('user.failure.login_is_not_email') % '', flash[:alert]
        assert_redirected_to show_user_path(@user)
        check_no_user_update(@user)
      end
    end
  end

  test "should not update user with already used email" do
    check_no_new_user do
      signed(@admin) do
        post :update, :id => @user, user: @params.merge({email: @user2.email})
        assert_equal I18n.t('user.failure.login_is_taken'), flash[:alert]
        assert_redirected_to show_user_path(@user)
        check_no_user_update(@user)
      end
    end
  end

  test "should not update user with not email" do
    check_no_new_user do
      signed(@admin) do
        post :update, :id => @user, user: @params.merge({email: 'sm00th1980'})
        assert_equal I18n.t('user.failure.login_is_not_email') % 'sm00th1980', flash[:alert]
        assert_redirected_to show_user_path(@user)
        check_no_user_update(@user)
      end
    end
  end

  test "should not update user with not exist user_id" do
    check_no_new_user do
      signed(@admin) do
        post :update, :id => -1, user: @params
        assert_equal I18n.t('user.failure.not_exist') % -1, flash[:alert]
        assert_redirected_to users_path
      end
    end
  end

  #invalid type_id
  test "should not update user without type_id" do
    check_no_new_user do
      signed(@admin) do
        post :update, id: @user, user: @params.except(:type_id)
        assert_equal I18n.t('user.failure.type_not_found') % nil, flash[:alert]
        assert_redirected_to show_user_path(@user)
        check_no_user_update(@user)
      end
    end
  end

  test "should not update user with blank type_id" do
    check_no_new_user do
      signed(@admin) do
        post :update, id: @user, user: @params.merge({type_id: ''})
        assert_equal I18n.t('user.failure.type_not_found') % nil, flash[:alert]
        assert_redirected_to show_user_path(@user)
        check_no_user_update(@user)
      end
    end
  end

  test "should not update user with not exist type_id" do
    check_no_new_user do
      signed(@admin) do
        post :update, id: @user, user: @params.merge({type_id: -1})
        assert_equal I18n.t('user.failure.type_not_found') % -1, flash[:alert]
        assert_redirected_to show_user_path(@user)
        check_no_user_update(@user)
      end
    end
  end

  #invalid status
  test "should not update user without status" do
    check_no_new_user do
      signed(@admin) do
        post :update, id: @user, user: @params.except(:status_id)
        assert_equal I18n.t('user.failure.status_not_found') % nil, flash[:alert]
        assert_redirected_to show_user_path(@user)
        check_no_user_update(@user)
      end
    end
  end

  test "should not update user with blank status" do
    check_no_new_user do
      signed(@admin) do
        post :update, id: @user, user: @params.merge({status_id: ''})
        assert_equal I18n.t('user.failure.status_not_found') % nil, flash[:alert]
        assert_redirected_to show_user_path(@user)
        check_no_user_update(@user)
      end
    end
  end

  test "should not update user with not exist status" do
    check_no_new_user do
      signed(@admin) do
        post :update, id: @user, user: @params.merge({status_id: -1})
        assert_equal I18n.t('user.failure.status_not_found') % -1, flash[:alert]
        assert_redirected_to show_user_path(@user)
        check_no_user_update(@user)
      end
    end
  end

  #invalid discount
  test "should not update user without discount in params" do
    check_no_new_user do
      signed(@admin) do
        post :update, id: @user, user: @params.except(:discount)
        assert_equal I18n.t('user.failure.discount_invalid') % '', flash[:alert]
        assert_redirected_to show_user_path(@user)
        check_no_user_update(@user)
      end
    end
  end

  test "should not update user with negative discount" do
    check_no_new_user do
      signed(@admin) do
        post :update, id: @user, user: @params.merge!({discount: -10})
        assert_equal I18n.t('user.failure.discount_invalid') % -10, flash[:alert]
        assert_redirected_to show_user_path(@user)
        check_no_user_update(@user)
      end
    end
  end

  test "should not update user with discount more 100 percent" do
    check_no_new_user do
      signed(@admin) do
        post :update, id: @user, user: @params.merge!({discount: 150})
        assert_equal I18n.t('user.failure.discount_invalid') % 150, flash[:alert]
        assert_redirected_to show_user_path(@user)
        check_no_user_update(@user)
      end
    end
  end

  test "should not update user with discount not number" do
    check_no_new_user do
      signed(@admin) do
        post :update, id: @user, user: @params.merge!({discount: 'abc'})
        assert_equal I18n.t('user.failure.discount_invalid') % 'abc', flash[:alert]
        assert_redirected_to show_user_path(@user)
        check_no_user_update(@user)
      end
    end
  end

  private
  def check_no_assigns
    [:email, :fio, :description, :type, :status, :charge_type, :payment_type, :discount].each do |field|
      assert_nil assigns(field)
    end
  end

  def check_assigns(params)
    [:email, :fio, :description, :discount].each do |field|
      assert_equal params[field], assigns(field)
    end
  end

  def check_no_new_user
    assert_no_difference 'User.count' do
      assert_no_difference 'UserTariff.count' do
        yield
      end
    end

    assert_response :redirect
  end

  def check_no_user_update(user)
    _user = @user.reload
    assert_not_nil _user
    assert_equal user.fio, _user.fio
    assert_equal user.description, _user.description
    assert_equal user.user_type_id, _user.type.id
    assert_equal user.status.active?, _user.status.active?
  end

end
