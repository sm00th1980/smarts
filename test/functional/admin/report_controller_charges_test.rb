# -*- encoding : utf-8 -*-
require 'test_helper'

class Admin::ReportControllerChargesTest < ActionController::TestCase
  tests Admin::ReportController

  def setup
    clear_db

    @admin = FactoryGirl.create(:admin)
    @user = FactoryGirl.create(:user)

    @dates = [
        {begin_date: Date.today.strftime('%Y-%m-%d'), end_date: (Date.today + 1.day).strftime('%Y-%m-%d')},
        {begin_date: (Date.today - 1.month).strftime('%Y-%m-%d'), end_date: (Date.today).strftime('%Y-%m-%d')},
        {begin_date: (Date.today - 1.year).strftime('%Y-%m-%d'), end_date: (Date.today + 1.year).strftime('%Y-%m-%d')}
    ]
  end

  #charges
  test "should not get index for unsigned" do
    get :charges
    check_failed_result new_user_session_path
  end

  test "should not get index for user" do
    signed(@user) do
      get :charges
      check_failed_result root_path
    end
  end

  test "should get index for admin" do
    signed(@admin) do
      get :charges
      check_success_result(Date.today.at_beginning_of_month, Date.today.at_end_of_month)
    end
  end

  test "should get index for admin for specific period" do
    @dates.each do |_date|
      signed(@admin) do
        get :charges, report: {begin_date: _date[:begin_date], end_date: _date[:end_date]}
        check_success_result(Date.parse(_date[:begin_date]), Date.parse(_date[:end_date]))
      end
    end
  end

  test "should get report in excel for admin" do
    @dates.each do |_date|
      signed(@admin) do
        get :charges_xls, report: {begin_date: _date[:begin_date], end_date: _date[:end_date]}
        check_success_result(Date.parse(_date[:begin_date]), Date.parse(_date[:end_date]))
      end
    end
  end

  test "should get report if begin_date is not exists" do
    signed(@admin) do
      get :charges, report: {end_date: Date.today - 1.year}
      check_success_result(Date.today.at_beginning_of_month, Date.today.at_end_of_month)
    end
  end

  test "should get report if end_date is not exists" do
    signed(@admin) do
      get :charges, report: {begin_date: Date.today - 1.year}
      check_success_result(Date.today.at_beginning_of_month, Date.today.at_end_of_month)
    end
  end

  test "should get report if begin_date is older than end_date" do
    signed(@admin) do
      get :charges, report: {begin_date: Date.today + 1.year, end_date: Date.today - 1.year}
      check_success_result(Date.today.at_beginning_of_month, Date.today.at_end_of_month)
    end
  end

  private
  def check_success_result(begin_date, end_date)
    assert_response :success

    assert_equal begin_date, assigns(:decorator).begin_date
    assert_equal end_date, assigns(:decorator).end_date

    assert_not_nil assigns(:reports)
  end

  def check_failed_result(redirect_path)
    assert_response :redirect
    assert_nil assigns(:reports)
    assert_nil assigns(:decorator)

    assert_redirected_to redirect_path
  end
end
