# -*- encoding : utf-8 -*-
require 'test_helper'

class Admin::UserCreateControllerTest < ActionController::TestCase
  tests Admin::UserController

  def setup
    clear_db

    @admin = FactoryGirl.create(:admin)
    @user = FactoryGirl.create(:user)

    @params = {
        status: 'active',
        email: 'sm00th1980@mail.ru',
        fio: 'Deyarov Ruslan',
        description: 'New User',
        password: '12345678',
        password_confirmation: '12345678',
        tariff_id: Tariff.default.id,
        status_id: UserStatus.active.id,
        type_id: UserType.business.id,
        charge_type_id: ChargeType.by_sent.id,
        payment_type_id: PaymentType.prepaid.id,
        permit_night_delivery: 'reject',
        discount: 0,
        permit_alpha_sender_name: 'permit',
        permit_def_sender_name: 'permit',
        reject_to_megafon: 'permit',
        alpha_megafon_channel_id: Channel.gold.id,
        reject_to_smarts_via_gold_channel: 'permit',
        reject_to_mts: 'permit'
    }
  end

  #create
  test "should not create user for unsigned" do
    check_no_new_user do
      post :create, user: @params
      assert_equal I18n.t('devise.failure.user.unauthenticated'), flash[:alert]
      check_no_assigns
      assert_redirected_to new_user_session_path
    end
  end

  test "should not create user for not admin" do
    check_no_new_user do
      signed(@user) do
        post :create, user: @params
        assert_equal I18n.t('failure.admin_only'), flash[:alert]
        check_no_assigns
        assert_redirected_to root_path
      end
    end
  end

  #success create
  test "should create user for admin" do
    check_new_user do
      signed(@admin) do
        post :create, user: @params
        assert_nil flash[:alert]
        assert_equal I18n.t('user.success.created'), flash[:notice]
        assert_response :redirect
        assert_redirected_to users_path
        check_assigns(@params)

        #check user
        new_user = User.find_by_email(@params[:email])
        assert_not_nil new_user
        assert_equal @params[:fio], new_user.fio
        assert_equal @params[:description], new_user.description
        assert_equal @params[:type_id], new_user.type.id
        assert_equal @params[:status_id], new_user.status.id
        assert_equal @params[:charge_type_id], new_user.charge_type.id
        assert_equal @params[:payment_type_id], new_user.payment_type.id
        refute new_user.permit_night_delivery?
        assert_equal @params[:discount], new_user.discount
        assert new_user.permit_alpha_sender_name?
        assert new_user.permit_def_sender_name?
        refute new_user.reject_to_megafon?

        #check user_tariff
        new_user_tariff = UserTariff.find_by(user: new_user)
        assert_not_nil new_user_tariff
        assert_equal @params[:tariff_id], new_user_tariff.tariff_id
        assert_equal Date.today, new_user_tariff.begin_date
        assert_nil new_user_tariff.end_date
      end
    end
  end

  test "should create postpaid user for admin" do
    check_new_user do
      signed(@admin) do
        post :create, user: @params.merge!({prepaid_type_id: PaymentType.postpaid.id})
        assert_nil flash[:alert]
        assert_equal I18n.t('user.success.created'), flash[:notice]
        assert_response :redirect
        assert_redirected_to users_path
        check_assigns(@params)

        #check user
        new_user = User.find_by_email(@params[:email])
        assert_not_nil new_user
        assert_equal @params[:fio], new_user.fio
        assert_equal @params[:description], new_user.description
        assert_equal @params[:type_id], new_user.type.id
        assert_equal @params[:status_id], new_user.status.id
        assert_equal @params[:charge_type_id], new_user.charge_type.id
        assert_equal @params[:payment_type_id], new_user.payment_type.id
        refute new_user.permit_night_delivery?
        assert_equal @params[:discount], new_user.discount
        assert new_user.permit_alpha_sender_name?
        assert new_user.permit_def_sender_name?
        refute new_user.reject_to_megafon?

        #check user_tariff
        new_user_tariff = UserTariff.find_by_user_id(new_user.id)
        assert_not_nil new_user_tariff
        assert_equal @params[:tariff_id], new_user_tariff.tariff_id
        assert_equal Date.today, new_user_tariff.begin_date
        assert_nil new_user_tariff.end_date
      end
    end
  end

  test "should create prepaid user for admin with permit_night_delivery" do
    check_new_user do
      signed(@admin) do
        post :create, user: @params.merge!({permit_night_delivery: 'permit'})
        assert_nil flash[:alert]
        assert_equal I18n.t('user.success.created'), flash[:notice]
        assert_response :redirect
        assert_redirected_to users_path
        check_assigns(@params)

        #check user
        new_user = User.find_by_email(@params[:email])
        assert_not_nil new_user
        assert_equal @params[:fio], new_user.fio
        assert_equal @params[:description], new_user.description
        assert_equal @params[:type_id], new_user.type.id
        assert_equal @params[:charge_type_id], new_user.charge_type.id
        assert_equal @params[:payment_type_id], new_user.payment_type.id
        assert new_user.permit_night_delivery?
        assert_equal @params[:discount], new_user.discount
        assert new_user.permit_alpha_sender_name?
        assert new_user.permit_def_sender_name?
        refute new_user.reject_to_megafon?

        #check user_tariff
        new_user_tariff = UserTariff.find_by_user_id(new_user.id)
        assert_not_nil new_user_tariff
        assert_equal @params[:tariff_id], new_user_tariff.tariff_id
        assert_equal Date.today, new_user_tariff.begin_date
        assert_nil new_user_tariff.end_date
      end
    end
  end

  test "should create prepaid user for admin with non permit_night_delivery" do
    check_new_user do
      signed(@admin) do
        post :create, user: @params.merge!({permit_night_delivery: 'reject'})
        assert_nil flash[:alert]
        assert_equal I18n.t('user.success.created'), flash[:notice]
        assert_response :redirect
        assert_redirected_to users_path
        check_assigns(@params)

        #check user
        new_user = User.find_by_email(@params[:email])
        assert_not_nil new_user
        assert_equal @params[:fio], new_user.fio
        assert_equal @params[:description], new_user.description
        assert_equal @params[:type_id], new_user.type.id
        assert_equal @params[:charge_type_id], new_user.charge_type.id
        assert_equal @params[:payment_type_id], new_user.payment_type.id
        refute new_user.permit_night_delivery?
        assert_equal @params[:discount], new_user.discount
        assert new_user.permit_alpha_sender_name?
        assert new_user.permit_def_sender_name?

        refute new_user.reject_to_megafon?

        #check user_tariff
        new_user_tariff = UserTariff.find_by_user_id(new_user.id)
        assert_not_nil new_user_tariff
        assert_equal @params[:tariff_id], new_user_tariff.tariff_id
        assert_equal Date.today, new_user_tariff.begin_date
        assert_nil new_user_tariff.end_date
      end
    end
  end

  test "should create user with non-zero discount" do
    check_new_user do
      signed(@admin) do
        post :create, user: @params.merge!({discount: 10})
        assert_nil flash[:alert]
        assert_equal I18n.t('user.success.created'), flash[:notice]
        assert_response :redirect
        assert_redirected_to users_path
        check_assigns(@params)

        #check user
        new_user = User.find_by_email(@params[:email])
        assert_not_nil new_user
        assert_equal @params[:fio], new_user.fio
        assert_equal @params[:description], new_user.description
        assert_equal @params[:type_id], new_user.type.id
        assert_equal @params[:charge_type_id], new_user.charge_type.id
        assert_equal @params[:payment_type_id], new_user.payment_type.id
        refute new_user.permit_night_delivery?
        assert_equal @params[:discount], new_user.discount
        assert new_user.permit_alpha_sender_name?
        assert new_user.permit_def_sender_name?
        refute new_user.reject_to_megafon?

        #check user_tariff
        new_user_tariff = UserTariff.find_by_user_id(new_user.id)
        assert_not_nil new_user_tariff
        assert_equal @params[:tariff_id], new_user_tariff.tariff_id
        assert_equal Date.today, new_user_tariff.begin_date
        assert_nil new_user_tariff.end_date
      end
    end
  end

  test "should create user with reject alpha sender name" do
    check_new_user do
      signed(@admin) do
        post :create, user: @params.merge!({permit_alpha_sender_name: 'reject'})
        assert_nil flash[:alert]
        assert_equal I18n.t('user.success.created'), flash[:notice]
        assert_response :redirect
        assert_redirected_to users_path
        check_assigns(@params)

        #check user
        new_user = User.find_by_email(@params[:email])
        assert_not_nil new_user
        assert_equal @params[:fio], new_user.fio
        assert_equal @params[:description], new_user.description
        assert_equal @params[:type_id], new_user.type.id
        assert_equal @params[:charge_type_id], new_user.charge_type.id
        assert_equal @params[:payment_type_id], new_user.payment_type.id
        refute new_user.permit_night_delivery?
        assert_equal @params[:discount], new_user.discount
        refute new_user.permit_alpha_sender_name?
        assert new_user.permit_def_sender_name?
        refute new_user.reject_to_megafon?

        #check user_tariff
        new_user_tariff = UserTariff.find_by_user_id(new_user.id)
        assert_not_nil new_user_tariff
        assert_equal @params[:tariff_id], new_user_tariff.tariff_id
        assert_equal Date.today, new_user_tariff.begin_date
        assert_nil new_user_tariff.end_date
      end
    end
  end

  test "should create user with reject def sender name" do
    check_new_user do
      signed(@admin) do
        post :create, user: @params.merge!({permit_def_sender_name: 'reject'})
        assert_nil flash[:alert]
        assert_equal I18n.t('user.success.created'), flash[:notice]
        assert_response :redirect
        assert_redirected_to users_path
        check_assigns(@params)

        #check user
        new_user = User.find_by_email(@params[:email])
        assert_not_nil new_user
        assert_equal @params[:fio], new_user.fio
        assert_equal @params[:description], new_user.description
        assert_equal @params[:type_id], new_user.type.id
        assert_equal @params[:charge_type_id], new_user.charge_type.id
        assert_equal @params[:payment_type_id], new_user.payment_type.id
        refute new_user.permit_night_delivery?
        assert_equal @params[:discount], new_user.discount
        assert new_user.permit_alpha_sender_name?
        refute new_user.permit_def_sender_name?
        refute new_user.reject_to_megafon?

        #check user_tariff
        new_user_tariff = UserTariff.find_by_user_id(new_user.id)
        assert_not_nil new_user_tariff
        assert_equal @params[:tariff_id], new_user_tariff.tariff_id
        assert_equal Date.today, new_user_tariff.begin_date
        assert_nil new_user_tariff.end_date
      end
    end
  end

  test "should create user with reject to megafon" do
    check_new_user do
      signed(@admin) do
        post :create, user: @params.merge!({reject_to_megafon: 'reject'})
        assert_nil flash[:alert]
        assert_equal I18n.t('user.success.created'), flash[:notice]
        assert_response :redirect
        assert_redirected_to users_path
        check_assigns(@params)

        #check user
        new_user = User.find_by_email(@params[:email])
        assert_not_nil new_user
        assert_equal @params[:fio], new_user.fio
        assert_equal @params[:description], new_user.description
        assert_equal @params[:type_id], new_user.type.id
        assert_equal @params[:charge_type_id], new_user.charge_type.id
        assert_equal @params[:payment_type_id], new_user.payment_type.id
        refute new_user.permit_night_delivery?
        assert_equal @params[:discount], new_user.discount
        assert new_user.permit_alpha_sender_name?
        assert new_user.permit_def_sender_name?
        assert new_user.reject_to_megafon?
        assert new_user.alpha_megafon_channel.gold?

        #check user_tariff
        new_user_tariff = UserTariff.find_by_user_id(new_user.id)
        assert_not_nil new_user_tariff
        assert_equal @params[:tariff_id], new_user_tariff.tariff_id
        assert_equal Date.today, new_user_tariff.begin_date
        assert_nil new_user_tariff.end_date
      end
    end
  end

  test "should create user with reject to mts" do
    check_new_user do
      signed(@admin) do
        post :create, user: @params.merge!({reject_to_mts: 'reject'})
        assert_nil flash[:alert]
        assert_equal I18n.t('user.success.created'), flash[:notice]
        assert_response :redirect
        assert_redirected_to users_path
        check_assigns(@params)

        #check user
        new_user = User.find_by_email(@params[:email])
        assert_not_nil new_user
        assert_equal @params[:fio], new_user.fio
        assert_equal @params[:description], new_user.description
        assert_equal @params[:type_id], new_user.type.id
        assert_equal @params[:charge_type_id], new_user.charge_type.id
        assert_equal @params[:payment_type_id], new_user.payment_type.id
        refute new_user.permit_night_delivery?
        assert_equal @params[:discount], new_user.discount
        assert new_user.permit_alpha_sender_name?
        assert new_user.permit_def_sender_name?
        refute new_user.reject_to_megafon?
        assert new_user.alpha_megafon_channel.gold?
        assert new_user.reject_to_mts?

        #check user_tariff
        new_user_tariff = UserTariff.find_by_user_id(new_user.id)
        assert_not_nil new_user_tariff
        assert_equal @params[:tariff_id], new_user_tariff.tariff_id
        assert_equal Date.today, new_user_tariff.begin_date
        assert_nil new_user_tariff.end_date
      end
    end
  end

  test "should create user with permit to mts" do
    check_new_user do
      signed(@admin) do
        post :create, user: @params.merge!({reject_to_mts: 'permit'})
        assert_nil flash[:alert]
        assert_equal I18n.t('user.success.created'), flash[:notice]
        assert_response :redirect
        assert_redirected_to users_path
        check_assigns(@params)

        #check user
        new_user = User.find_by_email(@params[:email])
        assert_not_nil new_user
        assert_equal @params[:fio], new_user.fio
        assert_equal @params[:description], new_user.description
        assert_equal @params[:type_id], new_user.type.id
        assert_equal @params[:charge_type_id], new_user.charge_type.id
        assert_equal @params[:payment_type_id], new_user.payment_type.id
        refute new_user.permit_night_delivery?
        assert_equal @params[:discount], new_user.discount
        assert new_user.permit_alpha_sender_name?
        assert new_user.permit_def_sender_name?
        refute new_user.reject_to_megafon?
        assert new_user.alpha_megafon_channel.gold?
        refute new_user.reject_to_mts?

        #check user_tariff
        new_user_tariff = UserTariff.find_by_user_id(new_user.id)
        assert_not_nil new_user_tariff
        assert_equal @params[:tariff_id], new_user_tariff.tariff_id
        assert_equal Date.today, new_user_tariff.begin_date
        assert_nil new_user_tariff.end_date
      end
    end
  end

  test "should create user with alpha megafon fixed channel" do
    check_new_user do
      signed(@admin) do
        post :create, user: @params.merge!({alpha_megafon_channel_id: Channel.megafon_fixed.id})
        assert_nil flash[:alert]
        assert_equal I18n.t('user.success.created'), flash[:notice]
        assert_response :redirect
        assert_redirected_to users_path
        check_assigns(@params)

        #check user
        new_user = User.find_by_email(@params[:email])
        assert_not_nil new_user
        assert_equal @params[:fio], new_user.fio
        assert_equal @params[:description], new_user.description
        assert_equal @params[:type_id], new_user.type.id
        assert_equal @params[:charge_type_id], new_user.charge_type.id
        assert_equal @params[:payment_type_id], new_user.payment_type.id
        refute new_user.permit_night_delivery?
        assert_equal @params[:discount], new_user.discount
        assert new_user.permit_alpha_sender_name?
        assert new_user.permit_def_sender_name?
        refute new_user.reject_to_megafon?
        assert new_user.alpha_megafon_channel.megafon_fixed?

        #check user_tariff
        new_user_tariff = UserTariff.find_by_user_id(new_user.id)
        assert_not_nil new_user_tariff
        assert_equal @params[:tariff_id], new_user_tariff.tariff_id
        assert_equal Date.today, new_user_tariff.begin_date
        assert_nil new_user_tariff.end_date
      end
    end
  end

  test "should create user with alpha megafon multi channel" do
    check_new_user do
      signed(@admin) do
        post :create, user: @params.merge!({alpha_megafon_channel_id: Channel.megafon_multi.id})
        assert_nil flash[:alert]
        assert_equal I18n.t('user.success.created'), flash[:notice]
        assert_response :redirect
        assert_redirected_to users_path
        check_assigns(@params)

        #check user
        new_user = User.find_by_email(@params[:email])
        assert_not_nil new_user
        assert_equal @params[:fio], new_user.fio
        assert_equal @params[:description], new_user.description
        assert_equal @params[:type_id], new_user.type.id
        assert_equal @params[:charge_type_id], new_user.charge_type.id
        assert_equal @params[:payment_type_id], new_user.payment_type.id
        refute new_user.permit_night_delivery?
        assert_equal @params[:discount], new_user.discount
        assert new_user.permit_alpha_sender_name?
        assert new_user.permit_def_sender_name?
        refute new_user.reject_to_megafon?
        assert new_user.alpha_megafon_channel.megafon_multi?

        #check user_tariff
        new_user_tariff = UserTariff.find_by_user_id(new_user.id)
        assert_not_nil new_user_tariff
        assert_equal @params[:tariff_id], new_user_tariff.tariff_id
        assert_equal Date.today, new_user_tariff.begin_date
        assert_nil new_user_tariff.end_date
      end
    end
  end

  test "should create user with reject to smarts via gold channel" do
    check_new_user do
      signed(@admin) do
        post :create, user: @params.merge!({reject_to_smarts_via_gold_channel: 'reject'})
        assert_nil flash[:alert]
        assert_equal I18n.t('user.success.created'), flash[:notice]
        assert_response :redirect
        assert_redirected_to users_path
        check_assigns(@params)

        #check user
        new_user = User.find_by_email(@params[:email])
        assert_not_nil new_user
        assert_equal @params[:fio], new_user.fio
        assert_equal @params[:description], new_user.description
        assert_equal @params[:type_id], new_user.type.id
        assert_equal @params[:charge_type_id], new_user.charge_type.id
        assert_equal @params[:payment_type_id], new_user.payment_type.id
        refute new_user.permit_night_delivery?
        assert_equal @params[:discount], new_user.discount
        assert new_user.permit_alpha_sender_name?
        assert new_user.permit_def_sender_name?
        refute new_user.reject_to_megafon?
        assert new_user.alpha_megafon_channel.gold?

        assert new_user.reject_to_smarts_via_gold_channel?

        #check user_tariff
        new_user_tariff = UserTariff.find_by_user_id(new_user.id)
        assert_not_nil new_user_tariff
        assert_equal @params[:tariff_id], new_user_tariff.tariff_id
        assert_equal Date.today, new_user_tariff.begin_date
        assert_nil new_user_tariff.end_date
      end
    end
  end

  test "should create user with permit to smarts via gold channel" do
    check_new_user do
      signed(@admin) do
        post :create, user: @params.merge!({reject_to_smarts_via_gold_channel: 'permit'})
        assert_nil flash[:alert]
        assert_equal I18n.t('user.success.created'), flash[:notice]
        assert_response :redirect
        assert_redirected_to users_path
        check_assigns(@params)

        #check user
        new_user = User.find_by_email(@params[:email])
        assert_not_nil new_user
        assert_equal @params[:fio], new_user.fio
        assert_equal @params[:description], new_user.description
        assert_equal @params[:type_id], new_user.type.id
        assert_equal @params[:charge_type_id], new_user.charge_type.id
        assert_equal @params[:payment_type_id], new_user.payment_type.id
        refute new_user.permit_night_delivery?
        assert_equal @params[:discount], new_user.discount
        assert new_user.permit_alpha_sender_name?
        assert new_user.permit_def_sender_name?
        refute new_user.reject_to_megafon?
        assert new_user.alpha_megafon_channel.gold?

        refute new_user.reject_to_smarts_via_gold_channel?

        #check user_tariff
        new_user_tariff = UserTariff.find_by_user_id(new_user.id)
        assert_not_nil new_user_tariff
        assert_equal @params[:tariff_id], new_user_tariff.tariff_id
        assert_equal Date.today, new_user_tariff.begin_date
        assert_nil new_user_tariff.end_date
      end
    end
  end

  #invalid email
  test "should not create user without email" do
    check_no_new_user do
      signed(@admin) do
        post :create, user: @params.except(:email)
        assert_equal I18n.t('user.failure.login_is_not_email') % '', flash[:alert]
        assert_redirected_to new_user_path
      end
    end
  end

  test "should not create user with blank email" do
    check_no_new_user do
      signed(@admin) do
        post :create, user: @params.merge({email: '' })
        assert_equal I18n.t('user.failure.login_is_not_email') % '', flash[:alert]
        assert_redirected_to new_user_path
      end
    end
  end

  test "should not create user with already used email" do
    check_no_new_user do
      signed(@admin) do
        post :create, user: @params.merge({email: 'sm00th1980' })
        assert_equal I18n.t('user.failure.login_is_not_email') % 'sm00th1980', flash[:alert]
        assert_redirected_to new_user_path
      end
    end
  end

  test "should not create user with not email" do
    check_no_new_user do
      signed(@admin) do
        post :create, user: @params.merge({email: @user.email })
        assert_equal I18n.t('user.failure.login_is_taken'), flash[:alert]
        assert_redirected_to new_user_path
      end
    end
  end


  #invalid password
  test "should not create user without password" do
    check_no_new_user do
      signed(@admin) do
        post :create, user: @params.except(:password)
        assert_equal I18n.t('user.failure.passwords_not_equal'), flash[:alert]
        assert_redirected_to new_user_path
      end
    end
  end

  test "should not create user with blank password" do
    check_no_new_user do
      signed(@admin) do
        post :create, user: @params.merge({password: '' })
        assert_equal I18n.t('user.failure.passwords_not_equal'), flash[:alert]
        assert_redirected_to new_user_path
      end
    end
  end

  test "should not create user without password_confirmation" do
    check_no_new_user do
      signed(@admin) do
        post :create, user: @params.except(:password_confirmation)
        assert_equal I18n.t('user.failure.passwords_not_equal'), flash[:alert]
        assert_redirected_to new_user_path
      end
    end
  end

  test "should not create user with blank password_confirmation" do
    check_no_new_user do
      signed(@admin) do
        post :create, user: @params.merge({password_confirmation: '' })
        assert_equal I18n.t('user.failure.passwords_not_equal'), flash[:alert]
        assert_redirected_to new_user_path
      end
    end
  end

  test "should not create user with not equal password and password_confirmation" do
    password = '12345678'
    confirmation = '87654321'

    check_no_new_user do
      signed(@admin) do
        post :create, user: @params.merge({password_confirmation: confirmation, password: password})
        assert_equal I18n.t('user.failure.passwords_not_equal'), flash[:alert]
        assert_redirected_to new_user_path
      end
    end
  end

  test "should not create user with passwords less than 7 symbols" do
    password = '12345'

    check_no_new_user do
      signed(@admin) do
        post :create, user: @params.merge({password_confirmation: password, password: password})
        assert_equal I18n.t('user.failure.password_too_short'), flash[:alert]
        assert_redirected_to new_user_path
      end
    end
  end

  #invalid tariff_id
  test "should not create user without tariff_id" do
    check_no_new_user do
      signed(@admin) do
        post :create, user: @params.except(:tariff_id)
        assert_equal I18n.t('user.failure.tariff_not_found') % nil, flash[:alert]
        assert_redirected_to new_user_path
      end
    end
  end

  test "should not create user with blank tariff_id" do
    check_no_new_user do
      signed(@admin) do
        post :create, user: @params.merge({tariff_id: ''})
        assert_equal I18n.t('user.failure.tariff_not_found') % nil, flash[:alert]
        assert_redirected_to new_user_path
      end
    end
  end

  test "should not create user with not exist tariff_id" do
    check_no_new_user do
      signed(@admin) do
        post :create, user: @params.merge({tariff_id: -1})
        assert_equal I18n.t('user.failure.tariff_not_found') % -1, flash[:alert]
        assert_redirected_to new_user_path
      end
    end
  end

  #invalid type_id
  test "should not create user without type_id" do
    check_no_new_user do
      signed(@admin) do
        post :create, user: @params.except(:type_id)
        assert_equal I18n.t('user.failure.type_not_found') % nil, flash[:alert]
        assert_redirected_to new_user_path
      end
    end
  end

  test "should not create user with blank type_id" do
    check_no_new_user do
      signed(@admin) do
        post :create, user: @params.merge({type_id: ''})
        assert_equal I18n.t('user.failure.type_not_found') % nil, flash[:alert]
        assert_redirected_to new_user_path
      end
    end
  end

  test "should not create user with not exist type_id" do
    check_no_new_user do
      signed(@admin) do
        post :create, user: @params.merge({type_id: -1})
        assert_equal I18n.t('user.failure.type_not_found') % -1, flash[:alert]
        assert_redirected_to new_user_path
      end
    end
  end

  #invalid discount
  test "should not create user without discount in params" do
    check_no_new_user do
      signed(@admin) do
        post :create, user: @params.except(:discount)
        assert_equal I18n.t('user.failure.discount_invalid') % '', flash[:alert]
        assert_redirected_to new_user_path
      end
    end
  end

  test "should not create user with negative discount" do
    check_no_new_user do
      signed(@admin) do
        post :create, user: @params.merge!({discount: -10})
        assert_equal I18n.t('user.failure.discount_invalid') % -10, flash[:alert]
        assert_redirected_to new_user_path
      end
    end
  end

  test "should not create user with discount more 100 percent" do
    check_no_new_user do
      signed(@admin) do
        post :create, user: @params.merge!({discount: 150})
        assert_equal I18n.t('user.failure.discount_invalid') % 150, flash[:alert]
        assert_redirected_to new_user_path
      end
    end
  end

  test "should not create user with discount not number" do
    check_no_new_user do
      signed(@admin) do
        post :create, user: @params.merge!({discount: 'abc'})
        assert_equal I18n.t('user.failure.discount_invalid') % 'abc', flash[:alert]
        assert_redirected_to new_user_path
      end
    end
  end

  private
  def check_no_assigns
    [:email, :fio, :description, :password, :password_confirmation, :type, :tariff, :status].each do |field|
      assert_nil assigns(field)
    end
  end

  def check_assigns(params)
    [:email, :fio, :description, :password, :password_confirmation].each do |field|
      assert_equal params[field], assigns(field)
    end
  end

  def check_no_new_user
    assert_no_difference 'User.count' do
      assert_no_difference 'UserTariff.count' do
        yield
      end
    end

    assert_response :redirect
  end

  def check_new_user
    assert_difference 'User.count' do
      assert_difference 'UserTariff.count' do
        yield
      end
    end
  end

end
