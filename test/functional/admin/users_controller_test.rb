# -*- encoding : utf-8 -*-
require 'test_helper'

class Admin::UsersControllerTest < ActionController::TestCase
  def setup
    clear_db

    @admin    = FactoryGirl.create(:admin)

    #type_id
    @user     = FactoryGirl.create(:user)
    @demo     = FactoryGirl.create(:demo)
    @operator = FactoryGirl.create(:user_operator)

    #status
    @user_active  = FactoryGirl.create(:user_active)
    @user_blocked = FactoryGirl.create(:user_blocked)
    @user_deleted = FactoryGirl.create(:user_deleted)
  end

  #show index
  test "should not show index user for unsigned" do
    get :index
    assert_response :redirect
    assert_nil assigns(:users)
    assert_redirected_to new_user_session_path
  end

  test "should not show index user for not admin" do
    signed(@user) do
      get :index
      assert_response :redirect
      assert_nil assigns(:users)
      assert_redirected_to root_path
    end
  end

  test "should show only active user for admin without filter" do
    signed(@admin) do
      get :index
      assert_response :success
      assert_equal [@user, @demo, @operator, @user_active].sort, assigns(:users).sort
    end
  end

  test "should show all user for admin with filters all" do
    signed(@admin) do
      get :index, user: {type: 'all', status: 'all'}
      assert_response :success
      assert_equal [@user, @demo, @operator, @user_active, @user_blocked, @user_deleted].sort, assigns(:users).sort
    end
  end

  test "should show only demo user" do
    signed(@admin) do
      get :index, user: {type: UserType.demo.id}
      assert_response :success
      assert_equal [@demo].sort, assigns(:users).sort
    end
  end

  test "should show only operator user" do
    signed(@admin) do
      get :index, user: {type: UserType.operator.id}
      assert_response :success
      assert_equal [@operator].sort, assigns(:users).sort
    end
  end

  test "should show only active user" do
    signed(@admin) do
      get :index, user: {status: UserStatus.active.id}
      assert_response :success
      assert_equal [@user, @demo, @operator, @user_active].sort, assigns(:users).sort
    end
  end

  test "should show only blocked user" do
    signed(@admin) do
      get :index, user: {status: UserStatus.blocked.id}
      assert_response :success
      assert_equal [@user_blocked].sort, assigns(:users).sort
    end
  end

  test "should show only deleted user" do
    signed(@admin) do
      get :index, user: {status: UserStatus.deleted.id}
      assert_response :success
      assert_equal [@user_deleted].sort, assigns(:users).sort
    end
  end

end
