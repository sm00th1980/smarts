# -*- encoding : utf-8 -*-
require 'test_helper'

class Admin::OperatorTariffControllerTest < ActionController::TestCase
  def setup
    clear_db

    @admin = FactoryGirl.create(:admin)
    @user = FactoryGirl.create(:user)


    @operator = CellOperatorGroup.first

    @params = {
        operator_id: @operator.id,
        operator_tariff: {
            price_per_sms: rand(100) + 1,
            begin_date: Date.today,
            end_date: nil
        }
    }

    OperatorTariff.delete_all
  end

  #index
  test "should not show list of operator tariffs for unsigned" do
    FactoryGirl.create(:mts_tariff)

    get :index, operator_id: @operator.id
    assert_response :redirect
    assert_nil assigns(:operator_tariffs)
    assert_redirected_to new_user_session_path

    OperatorTariff.delete_all
  end

  test "should not show list of operator tariffs for user" do
    FactoryGirl.create(:mts_tariff)

    signed(@user) do
      get :index, operator_id: @operator.id
      assert_response :redirect
      assert_nil assigns(:operator_tariffs)
      assert_redirected_to root_path
    end

    OperatorTariff.delete_all
  end

  test "should show list of operator tariffs for admin" do
    FactoryGirl.create(:mts_tariff)

    signed(@admin) do
      get :index, operator_id: @operator.id
      assert_response :success

      assert_equal OperatorTariff.where(operator_id: @operator).order(:begin_date), assigns(:operator_tariffs)
    end

    OperatorTariff.delete_all
  end

  #edit
  test "should not show edit for unsigned" do
    operator_tariff = FactoryGirl.create(:mts_tariff)

    get :edit, operator_tariff_id: operator_tariff
    assert_response :redirect
    assert_nil assigns(:operator_tariffs)
    assert_redirected_to new_user_session_path
  end

  test "should not show edit for user" do
    operator_tariff = FactoryGirl.create(:mts_tariff)

    signed(@user) do
      get :edit, operator_tariff_id: operator_tariff
      assert_response :redirect
      assert_nil assigns(:operator_tariffs)
      assert_redirected_to root_path
    end
  end

  test "should show edit for admin" do
    operator_tariff = FactoryGirl.create(:mts_tariff)

    signed(@admin) do
      get :edit, operator_tariff_id: operator_tariff
      assert_response :success
    end
  end

  #create
  test "should not create new tariff for unsigned" do
    assert_no_difference 'OperatorTariff.count' do
      post :create, @params

      assert_response :redirect
      assert_redirected_to new_user_session_path
    end
  end

  test "should not create new tariff for user" do
    assert_no_difference 'OperatorTariff.count' do
      signed(@user) do
        post :create, @params

        assert_response :redirect
        assert_redirected_to root_path
      end
    end
  end

  test "should create new tariff for admin" do
    assert_difference 'OperatorTariff.count' do
      signed(@admin) do
        post :create, @params

        assert_response :redirect
        assert_redirected_to admin_operator_tariffs_path(@params[:operator_id])
        assert_equal I18n.t('admin.operator_tariff.success.created'), flash[:notice]

        new_tariff = OperatorTariff.last

        assert_equal @params[:operator_id], new_tariff.operator.id
        assert_equal @params[:operator_tariff][:price_per_sms], new_tariff.price_per_sms
        assert_equal @params[:operator_tariff][:begin_date], new_tariff.begin_date
        assert_equal @params[:operator_tariff][:end_date], new_tariff.end_date
      end
    end
  end

  test "should not create same new tariff for admin twice" do
    OperatorTariff.create!(operator: CellOperatorGroup.find(@params[:operator_id]), price_per_sms: @params[:operator_tariff][:price_per_sms], begin_date: @params[:operator_tariff][:begin_date], end_date: @params[:operator_tariff][:end_date])
    assert_no_difference 'OperatorTariff.count' do
      signed(@admin) do
        post :create, @params

        assert_response :redirect
        assert_redirected_to admin_operator_tariffs_path(@params[:operator_id])
        assert_equal [I18n.t('validation.operator_tariff.cross_tariff')], flash[:alert]
      end
    end
  end

  #destroy
  test "should not destory tariff for unsigned" do
    tariff = OperatorTariff.create!(operator: CellOperatorGroup.find(@params[:operator_id]), price_per_sms: @params[:operator_tariff][:price_per_sms], begin_date: @params[:operator_tariff][:begin_date], end_date: @params[:operator_tariff][:end_date])

    assert_no_difference 'OperatorTariff.count' do
      post :destroy, operator_tariff_id: tariff.id

      assert_response :redirect
      assert_redirected_to new_user_session_path
    end
  end

  test "should not destroy tariff for user" do
    tariff = OperatorTariff.create!(operator: CellOperatorGroup.find(@params[:operator_id]), price_per_sms: @params[:operator_tariff][:price_per_sms], begin_date: @params[:operator_tariff][:begin_date], end_date: @params[:operator_tariff][:end_date])

    assert_no_difference 'OperatorTariff.count' do
      signed(@user) do
        post :destroy, operator_tariff_id: tariff.id

        assert_response :redirect
        assert_redirected_to root_path
      end
    end
  end

  test "should destroy tariff for admin" do
    tariff = OperatorTariff.create!(operator: CellOperatorGroup.find(@params[:operator_id]), price_per_sms: @params[:operator_tariff][:price_per_sms], begin_date: @params[:operator_tariff][:begin_date], end_date: @params[:operator_tariff][:end_date])

    assert_difference 'OperatorTariff.count', -1 do
      signed(@admin) do
        post :destroy, operator_tariff_id: tariff.id

        assert_response :redirect
        assert_redirected_to admin_operator_tariffs_path(@params[:operator_id])
        assert_equal I18n.t('admin.operator_tariff.success.delete'), flash[:notice]
      end
    end
  end

  test "should not destroy tariff for admin for not exist operator_tariff_id" do
    assert_no_difference 'OperatorTariff.count', -1 do
      signed(@admin) do
        post :destroy, operator_tariff_id: -1

        assert_response :redirect
        assert_redirected_to admin_operators_index_path
        assert_equal I18n.t('admin.operator_tariff.failure.not_exist'), flash[:alert]
      end
    end
  end

  #update
  test "should update tariff for admin" do
    tariff = OperatorTariff.create!(operator: CellOperatorGroup.find(@params[:operator_id]), price_per_sms: @params[:operator_tariff][:price_per_sms] + rand(100), begin_date: @params[:operator_tariff][:begin_date] - 1.year, end_date: @params[:operator_tariff][:end_date])

    new_params = {
        operator_tariff_id: tariff.id,
        operator_tariff: {
            price_per_sms: @params[:operator_tariff][:price_per_sms] + rand(100),
            begin_date: @params[:operator_tariff][:begin_date] - 1.year,
            end_date: @params[:operator_tariff][:begin_date] + 1.year
        }
    }

    assert_no_difference 'OperatorTariff.count' do
      signed(@admin) do
        post :update, new_params

        assert_response :redirect
        assert_redirected_to admin_operator_tariffs_path(@params[:operator_id])
        assert_equal I18n.t('admin.operator_tariff.success.updated'), flash[:notice]

        updated_tariff = tariff.reload
        assert_equal new_params[:operator_tariff][:price_per_sms], updated_tariff.price_per_sms
        assert_equal new_params[:operator_tariff][:begin_date], updated_tariff.begin_date
        assert_equal new_params[:operator_tariff][:end_date], updated_tariff.end_date
      end
    end
  end

  test "should not update tariff for admin for cross tariff" do
    OperatorTariff.delete_all

    tariff = OperatorTariff.create!(operator: CellOperatorGroup.find(@params[:operator_id]), price_per_sms: @params[:operator_tariff][:price_per_sms] + rand(100), begin_date: Date.today - 1.year, end_date: Date.today + 1.year)
    next_tariff = OperatorTariff.create!(operator: CellOperatorGroup.find(@params[:operator_id]), price_per_sms: @params[:operator_tariff][:price_per_sms] + rand(100), begin_date: Date.today + 1.year + 1.day, end_date: nil)

    new_params = {
        operator_tariff_id: tariff.id,
        operator_tariff: {
            price_per_sms: tariff.price_per_sms,
            begin_date: tariff.begin_date,
            end_date: next_tariff.begin_date + 1.year
        }
    }

    assert_no_difference 'OperatorTariff.count' do
      signed(@admin) do
        post :update, new_params

        assert_response :redirect
        assert_redirected_to admin_operator_tariffs_path(@params[:operator_id])
        assert_equal [I18n.t('validation.operator_tariff.cross_tariff')], flash[:alert]
      end
    end
  end

  #update
  test "should update tariff for admin for not exist id" do
    tariff = OperatorTariff.create!(operator: CellOperatorGroup.find(@params[:operator_id]), price_per_sms: @params[:operator_tariff][:price_per_sms] + rand(100), begin_date: @params[:operator_tariff][:begin_date] - 1.year, end_date: @params[:operator_tariff][:end_date])

    new_params = {
        operator_tariff_id: -1,
        operator_tariff: {
            price_per_sms: @params[:operator_tariff][:price_per_sms] + rand(100),
            begin_date: @params[:operator_tariff][:begin_date] - 1.year,
            end_date: @params[:operator_tariff][:begin_date] + 1.year
        }
    }

    assert_no_difference 'OperatorTariff.count' do
      signed(@admin) do
        post :update, new_params

        assert_response :redirect
        assert_redirected_to admin_operators_index_path
        assert_equal I18n.t('admin.operator_tariff.failure.not_exist'), flash[:alert]
      end
    end
  end

end
