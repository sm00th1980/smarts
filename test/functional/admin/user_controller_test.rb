# -*- encoding : utf-8 -*-
require 'test_helper'

class Admin::UserControllerTest < ActionController::TestCase

  def setup
    clear_db

    @user  = FactoryGirl.create(:user)
    @admin = FactoryGirl.create(:admin)
  end

  #show
  test "should not show edit user for unsigned" do
    get :show, id: @user
    assert_response :redirect
    assert_nil assigns(:user)
    assert_redirected_to new_user_session_path
  end

  test "should not user for not admin" do
    signed(@user) do
      get :show, id: @user
      assert_response :redirect
      assert_nil assigns(:user)
      assert_redirected_to root_path
    end
  end

  test "should show user for admin" do
    signed(@admin) do
      get :show, id: @user
      assert_response :success
      assert_not_nil assigns(:user)
      assert_equal @user, assigns(:user)
    end
  end

end
