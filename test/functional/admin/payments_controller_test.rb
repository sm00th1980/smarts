# -*- encoding : utf-8 -*-
require 'test_helper'

class Admin::PaymentsControllerTest < ActionController::TestCase
  def setup
    clear_db

    @admin = FactoryGirl.create(:admin)
    @user = FactoryGirl.create(:user)
    @demo = FactoryGirl.create(:demo)

    @payment_new_manual = FactoryGirl.create(:payment_new_manual, user: @user)
    @payment_new_manual_for_demo = FactoryGirl.create(:payment_new_manual, user: @demo)

    @params_bank = {
        amount: rand(10000),
        service_id: PaymentService.bank.id
    }

    @params_manual = {
        amount: rand(10000),
        service_id: PaymentService.manual.id
    }
  end

  #show index
  test "should not show index for unsigned" do
    get :index, id: @user.id
    assert_response :redirect
    assert_nil assigns(:payments)
    assert_redirected_to new_user_session_path
  end

  test "should not show index user for not admin" do
    signed(@user) do
      get :index, id: @user.id
      assert_response :redirect
      assert_nil assigns(:payments)
      assert_redirected_to root_path
    end
  end

  test "should show all payments by user for admin" do
    signed(@admin) do
      get :index, id: @user.id
      assert_response :success
      assert_equal @user.payments.sort, assigns(:payments).sort
    end
  end

  test "should not show with not exist id" do
    signed(@admin) do
      get :index, id: -1
      assert_response :redirect
      assert_redirected_to users_path
      assert_equal I18n.t('user.failure.not_exist') % -1, flash[:alert]
      assert_nil assigns(:payments)
    end
  end

  test "should not show without blank id" do
    signed(@admin) do
      get :index, id: ''
      assert_response :redirect
      assert_redirected_to users_path
      assert_equal I18n.t('user.failure.not_exist') % '', flash[:alert]
      assert_nil assigns(:payments)
    end
  end

  #create
  test "should create payment for service manual" do
    signed(@admin) do
      post :create, id: @user.id, payment: @params_manual
      assert_response :redirect
      assert_redirected_to index_payments_path(@user)
      assert_equal I18n.t('admin.payment.success.created'), flash[:notice]

      new_payment = @user.payments.last
      assert_equal @params_manual[:amount], new_payment.amount
      assert_equal @params_manual[:service_id], new_payment.service.id
      assert new_payment.approved?
    end
  end

  test "should create payment for service bank" do
    signed(@admin) do
      post :create, id: @user.id, payment: @params_bank
      assert_response :redirect
      assert_redirected_to index_payments_path(@user)
      assert_equal I18n.t('admin.payment.success.created'), flash[:notice]

      new_payment = @user.payments.last
      assert_equal @params_bank[:amount], new_payment.amount
      assert_equal @params_bank[:service_id], new_payment.service.id
      assert new_payment.approved?
    end
  end

  #invalid create -> invalid amount
  test "should not create payment without amount" do
    payment_not_created do
      signed(@admin) do
        post :create, id: @user.id, payment: @params_manual.except(:amount)
        assert_response :redirect
        assert_redirected_to index_payments_path(@user)
        assert_equal I18n.t('admin.payment.failure.amount_invalid'), flash[:alert]
      end
    end
  end

  test "should not create payment with blank amount" do
    payment_not_created do
      signed(@admin) do
        post :create, id: @user.id, payment: @params_manual.merge({amount: ''})
        assert_response :redirect
        assert_redirected_to index_payments_path(@user)
        assert_equal I18n.t('admin.payment.failure.amount_invalid'), flash[:alert]
      end
    end
  end

  test "should not create payment with negative amount" do
    payment_not_created do
      signed(@admin) do
        post :create, id: @user.id, payment: @params_manual.merge({amount: -123})
        assert_response :redirect
        assert_redirected_to index_payments_path(@user)
        assert_equal I18n.t('admin.payment.failure.amount_invalid'), flash[:alert]
      end
    end
  end

  test "should not create payment with invalid amount" do
    payment_not_created do
      signed(@admin) do
        post :create, id: @user.id, payment: @params_manual.merge({amount: 'vgmfjbng'})
        assert_response :redirect
        assert_redirected_to index_payments_path(@user)
        assert_equal I18n.t('admin.payment.failure.amount_invalid'), flash[:alert]
      end
    end
  end

  test "should not create payment with invalid2 amount" do
    payment_not_created do
      signed(@admin) do
        post :create, id: @user.id, payment: @params_manual.merge({amount: '3vfdv8f'})
        assert_response :redirect
        assert_redirected_to index_payments_path(@user)
        assert_equal I18n.t('admin.payment.failure.amount_invalid'), flash[:alert]
      end
    end
  end

  #invalid create -> invalid service
  test "should not create payment without service_id" do
    payment_not_created do
      signed(@admin) do
        post :create, id: @user.id, payment: @params_manual.except(:service_id)
        assert_response :redirect
        assert_redirected_to index_payments_path(@user)
        assert_equal I18n.t('admin.payment.failure.payment_service_not_exist') % '', flash[:alert]
      end
    end
  end

  test "should not create payment with blank service_id" do
    payment_not_created do
      signed(@admin) do
        post :create, id: @user.id, payment: @params_manual.merge({service_id: ''})
        assert_response :redirect
        assert_redirected_to index_payments_path(@user)
        assert_equal I18n.t('admin.payment.failure.payment_service_not_exist') % '', flash[:alert]
      end
    end
  end

  test "should not create payment with not exist service_id" do
    payment_not_created do
      signed(@admin) do
        post :create, id: @user.id, payment: @params_manual.merge({service_id: -1})
        assert_response :redirect
        assert_redirected_to index_payments_path(@user)
        assert_equal I18n.t('admin.payment.failure.payment_service_not_exist') % -1, flash[:alert]
      end
    end
  end

  #show new
  test "should not show new for unsigned" do
    get :new, id: @user.id
    assert_response :redirect
    assert_nil assigns(:user)
    assert_redirected_to new_user_session_path
  end

  test "should not show new for not admin" do
    signed(@user) do
      get :new, id: @user.id
      assert_response :redirect
      assert_nil assigns(:user)
      assert_redirected_to root_path
    end
  end

  test "should show new payment form for admin" do
    signed(@admin) do
      get :new, id: @user.id
      assert_response :success
      assert_equal @user, assigns(:user)
    end
  end

  test "should not show new with not exist id" do
    signed(@admin) do
      get :new, id: -1
      assert_response :redirect
      assert_redirected_to users_path
      assert_equal I18n.t('user.failure.not_exist') % -1, flash[:alert]
      assert_nil assigns(:user)
    end
  end

  test "should not show new without blank id" do
    signed(@admin) do
      get :new, id: ''
      assert_response :redirect
      assert_redirected_to users_path
      assert_equal I18n.t('user.failure.not_exist') % '', flash[:alert]
      assert_nil assigns(:user)
    end
  end

  #delete
  test "should not delete for unsigned" do
    payment_not_deleted do
      post :destroy, id: @payment_new_manual.id
      assert_response :redirect
      assert_nil assigns(:payment)
      assert_redirected_to new_user_session_path
    end
  end

  test "should not delete payment for user" do
    payment_not_deleted do
      signed(@user) do
        post :destroy, id: @payment_new_manual.id
        assert_response :redirect
        assert_nil assigns(:payment)
        assert_redirected_to root_path
      end
    end
  end

  test "should delete payment" do
    payment_deleted do
      signed(@admin) do
        post :destroy, id: @payment_new_manual.id
        assert_response :redirect
        assert_redirected_to index_payments_path(@payment_new_manual.user)
        assert_equal I18n.t('admin.payment.success.deleted'), flash[:notice]
        assert_equal @payment_new_manual, assigns(:payment)
      end
    end
  end

  test "should not delete payment with blank id" do
    payment_not_deleted do
      signed(@admin) do
        post :destroy, id: ''
        assert_response :redirect
        assert_equal I18n.t('admin.payment.failure.not_exist') % '', flash[:alert]
        assert_nil assigns(:payment)
        assert_redirected_to users_path
      end
    end
  end

  test "should not delete payment with not exist id" do
    payment_not_deleted do
      signed(@admin) do
        post :destroy, id: -1
        assert_response :redirect
        assert_equal I18n.t('admin.payment.failure.not_exist') % -1, flash[:alert]
        assert_nil assigns(:payment)
        assert_redirected_to users_path
      end
    end
  end

  #refuse
  test "should not refuse for unsigned" do
    payment_not_deleted do
      post :refuse, id: @payment_new_manual.id
      assert_response :redirect
      assert_nil assigns(:payment)
      assert_redirected_to new_user_session_path
    end
  end

  test "should not refuse payment for user" do
    payment_not_deleted do
      signed(@user) do
        post :refuse, id: @payment_new_manual.id
        assert_response :redirect
        assert_nil assigns(:payment)
        assert_redirected_to root_path
      end
    end
  end

  test "should refuse payment" do
    payment_not_deleted do
      signed(@admin) do
        post :refuse, id: @payment_new_manual.id
        assert_response :redirect
        assert_redirected_to index_payments_path(@payment_new_manual.user)
        assert_equal I18n.t('admin.payment.success.refused'), flash[:notice]
        assert_equal @payment_new_manual, assigns(:payment)
        assert @payment_new_manual.reload.failed?
      end
    end
  end

  test "should not refuse payment with blank id" do
    payment_not_deleted do
      signed(@admin) do
        post :refuse, id: ''
        assert_response :redirect
        assert_equal I18n.t('admin.payment.failure.not_exist') % '', flash[:alert]
        assert_nil assigns(:payment)
        assert_redirected_to users_path
      end
    end
  end

  test "should not refuse payment with not exist id" do
    payment_not_deleted do
      signed(@admin) do
        post :refuse, id: -1
        assert_response :redirect
        assert_equal I18n.t('admin.payment.failure.not_exist') % -1, flash[:alert]
        assert_nil assigns(:payment)
        assert_redirected_to users_path
      end
    end
  end

  #approve
  test "should not approve for unsigned" do
    payment_not_deleted do
      post :approve, id: @payment_new_manual.id
      assert_response :redirect
      assert_nil assigns(:payment)
      assert_redirected_to new_user_session_path
    end
  end

  test "should not approve payment for user" do
    payment_not_deleted do
      signed(@user) do
        post :approve, id: @payment_new_manual.id
        assert_response :redirect
        assert_nil assigns(:payment)
        assert_redirected_to root_path
      end
    end
  end

  test "should approve payment" do
    payment_not_deleted do
      signed(@admin) do
        post :approve, id: @payment_new_manual.id
        assert_response :redirect
        assert_redirected_to index_payments_path(@payment_new_manual.user)
        assert_equal I18n.t('admin.payment.success.approved'), flash[:notice]
        assert_equal @payment_new_manual, assigns(:payment)
        assert @payment_new_manual.reload.approved?
      end
    end
  end

  test "should approve payment for demo" do
    payment_not_deleted do
      signed(@admin) do
        post :approve, id: @payment_new_manual_for_demo.id
        assert_response :redirect
        assert_redirected_to index_payments_path(@payment_new_manual_for_demo.user)
        assert_equal I18n.t('admin.payment.success.approved'), flash[:notice]
        assert_equal @payment_new_manual_for_demo, assigns(:payment)
        assert @payment_new_manual_for_demo.reload.approved?
      end
    end
  end

  test "should not approve payment with blank id" do
    payment_not_deleted do
      signed(@admin) do
        post :approve, id: ''
        assert_response :redirect
        assert_equal I18n.t('admin.payment.failure.not_exist') % '', flash[:alert]
        assert_nil assigns(:payment)
        assert_redirected_to users_path
      end
    end
  end

  test "should not approve payment with not exist id" do
    payment_not_deleted do
      signed(@admin) do
        post :approve, id: -1
        assert_response :redirect
        assert_equal I18n.t('admin.payment.failure.not_exist') % -1, flash[:alert]
        assert_nil assigns(:payment)
        assert_redirected_to users_path
      end
    end
  end

  private
  def payment_not_created
    assert_no_difference 'Payment.count' do
      yield
    end
  end

  def payment_deleted
    assert_difference 'Payment.count', -1 do
      yield
    end
  end

  def payment_not_deleted
    assert_no_difference 'Payment.count' do
      yield
    end
  end
end
