# -*- encoding : utf-8 -*-
require 'test_helper'

class Admin::BlackListsControllerTest < ActionController::TestCase
  def setup
    clear_db

    @admin = FactoryGirl.create(:admin)
    @user = FactoryGirl.create(:user)

    @black_list_phone = FactoryGirl.create(:black_list, user: @admin)
  end

  #show index
  test "should not show index user for unsigned" do
    get :index
    assert_response :redirect
    assert_nil assigns(:black_lists)
    assert_redirected_to new_user_session_path
  end

  test "should not show index for not admin" do
    signed(@user) do
      get :index
      assert_response :redirect
      assert_nil assigns(:black_lists)
      assert_redirected_to root_path
    end
  end

  test "should show index for admin" do
    signed(@admin) do
      get :index
      assert_response :success

      assert_equal [@black_list_phone].sort, assigns(:black_lists).sort
    end
  end

  #create
  test "should not create black list for unsigned" do
    assert_no_difference 'BlackList.count' do
      post :create, phone: Test::Helpers.random_normal_phone_number

      assert_response :redirect
      assert_nil assigns(:black_lists)
      assert_redirected_to new_user_session_path
    end
  end

  test "should not create black list for user" do
    assert_no_difference 'BlackList.count' do
      signed(@user) do
        post :create, phone: Test::Helpers.random_normal_phone_number

        assert_response :redirect
        assert_nil assigns(:black_lists)
        assert_redirected_to root_path
      end
    end
  end

  test "should create black list for admin" do
    phone = Test::Helpers.random_normal_phone_number

    assert_difference 'BlackList.count' do
      signed(@admin) do
        post :create, phone: phone

        assert_response :redirect
        assert_redirected_to admin_black_lists_path
        assert_equal I18n.t('admin.black_list.success.created'), flash[:notice]

        assert_equal BlackList.last.phone, phone
      end
    end
  end

  test "should not create black list for admin with invalid-phone" do
    phone = '-1'

    assert_no_difference 'BlackList.count' do
      signed(@admin) do
        post :create, phone: phone

        assert_response :redirect
        assert_redirected_to admin_black_lists_path
        assert_equal I18n.t('admin.black_list.failure.invalid_phone') % phone, flash[:alert]
      end
    end
  end

  test "should not create black list for admin if it already exists" do
    assert_no_difference 'BlackList.count' do
      signed(@admin) do
        post :create, phone: @black_list_phone.phone

        assert_response :redirect
        assert_redirected_to admin_black_lists_path
        assert_equal I18n.t('admin.black_list.failure.already_exist') % @black_list_phone.phone, flash[:alert]
      end
    end
  end


  #destroy
  test "should not destroy black list for unsigned" do
    assert_no_difference 'BlackList.count' do
      post :destroy, id: @black_list_phone.id

      assert_response :redirect
      assert_nil assigns(:black_lists)
      assert_redirected_to new_user_session_path
    end
  end

  test "should not destroy black list for user" do
    assert_no_difference 'BlackList.count' do
      signed(@user) do
        post :destroy, id: @black_list_phone.id

        assert_response :redirect
        assert_nil assigns(:black_lists)
        assert_redirected_to root_path
      end
    end
  end

  test "should destroy black list for admin" do
    assert_difference 'BlackList.count', -1 do
      signed(@admin) do
        post :destroy, id: @black_list_phone.id

        assert_response :redirect
        assert_redirected_to admin_black_lists_path
        assert_equal I18n.t('admin.black_list.success.deleted'), flash[:notice]

        assert_nil BlackList.find_by_id(@black_list_phone.id)
      end
    end
  end

  test "should not destroy black list for admin by not exists id" do
    id = -1
    assert_no_difference 'BlackList.count' do
      signed(@admin) do
        post :destroy, id: id

        assert_response :redirect
        assert_redirected_to admin_black_lists_path
        assert_equal I18n.t('admin.black_list.failure.not_exist') % id, flash[:alert]
      end
    end
  end
end
