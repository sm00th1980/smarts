# -*- encoding : utf-8 -*-
require 'test_helper'

class Admin::TariffsControllerTest < ActionController::TestCase
  def setup
    clear_db

    @admin = FactoryGirl.create(:admin)
    @user = FactoryGirl.create(:user_without_tariff)

    @user_tariff_middle = FactoryGirl.create(:middle_user_tariff, user: @user)

    @valid_params = [{
                         id: @user_tariff_middle.tariff.id,
                         begin_date: (Date.today + 1.year).strftime('%Y-%m-%d'),
                         end_date: ''
                     }, {
                         id: Tariff.default.id,
                         begin_date: (Date.today - 1.month).strftime('%Y-%m-%d'),
                         end_date: (Date.today + 1.month).strftime('%Y-%m-%d')
                     }]


    @user_with_open_tariff = FactoryGirl.create(:user_without_tariff)
    @user_tariff_with_opened_end_date = FactoryGirl.create(:user_tariff_with_opened_end_date, user: @user_with_open_tariff)

    @user_with_closed_tariff = FactoryGirl.create(:user_without_tariff)
    @user_tariff_with_closed_end_date = FactoryGirl.create(:user_tariff_with_closed_end_date, user: @user_with_closed_tariff)
  end

  #index
  test "should not show index for unsigned" do
    get :index, id: @user.id
    assert_response :redirect
    assert_nil assigns(:user_tariffs)
    assert_redirected_to new_user_session_path
  end

  test "should not show index for not admin" do
    signed(@user) do
      get :index, id: @user.id
      assert_response :redirect
      assert_nil assigns(:user_tariffs)
      assert_redirected_to root_path
    end
  end

  test "should show all user_tariffs by user for admin" do
    signed(@admin) do
      get :index, id: @user.id
      assert_response :success
      assert_equal @user.user_tariffs.sort, assigns(:user_tariffs).sort
    end
  end

  test "should not show with not exist id" do
    signed(@admin) do
      get :index, id: -1
      assert_response :redirect
      assert_redirected_to users_path
      assert_equal I18n.t('user.failure.not_exist') % -1, flash[:alert]
      assert_nil assigns(:user_tariffs)
    end
  end

  test "should not show without blank id" do
    signed(@admin) do
      get :index, id: ''
      assert_response :redirect
      assert_redirected_to users_path
      assert_equal I18n.t('user.failure.not_exist') % '', flash[:alert]
      assert_nil assigns(:user_tariffs)
    end
  end

  #delete
  test "should not delete for unsigned" do
    user_tariff_not_deleted do
      post :destroy, id: @user_tariff_middle.id
      assert_response :redirect
      assert_nil assigns(:user_tariff)
      assert_redirected_to new_user_session_path
    end
  end

  test "should not delete payment for user" do
    user_tariff_not_deleted do
      signed(@user) do
        post :destroy, id: @user_tariff_middle.id
        assert_response :redirect
        assert_nil assigns(:user_tariff)
        assert_redirected_to root_path
      end
    end
  end

  test "should delete user_tariff" do
    user_tariff_deleted do
      signed(@admin) do
        post :destroy, id: @user_tariff_middle.id
        assert_response :redirect
        assert_redirected_to index_tariffs_path(@user_tariff_middle.user)
        assert_equal I18n.t('admin.user_tariff.success.deleted'), flash[:notice]
        assert_equal @user_tariff_middle, assigns(:user_tariff)
      end
    end
  end

  test "should not delete user_tariff with blank id" do
    user_tariff_not_deleted do
      signed(@admin) do
        post :destroy, id: ''
        assert_response :redirect
        assert_equal I18n.t('admin.user_tariff.failure.not_exist') % '', flash[:alert]
        assert_nil assigns(:user_tariff)
        assert_redirected_to users_path
      end
    end
  end

  test "should not delete user_tariff with not exist id" do
    user_tariff_not_deleted do
      signed(@admin) do
        post :destroy, id: -1
        assert_response :redirect
        assert_equal I18n.t('admin.user_tariff.failure.not_exist') % -1, flash[:alert]
        assert_nil assigns(:user_tariff)
        assert_redirected_to users_path
      end
    end
  end

  #new
  test "should not show new for unsigned" do
    get :new, id: @user.id
    assert_response :redirect
    assert_nil assigns(:user)
    assert_redirected_to new_user_session_path
  end

  test "should not show new for not admin" do
    signed(@user) do
      get :new, id: @user.id
      assert_response :redirect
      assert_nil assigns(:user)
      assert_redirected_to root_path
    end
  end

  test "should show new user_tariff form for admin" do
    signed(@admin) do
      get :new, id: @user.id
      assert_response :success
      assert_equal @user, assigns(:user)
    end
  end

  test "should not show new with not exist id" do
    signed(@admin) do
      get :new, id: -1
      assert_response :redirect
      assert_redirected_to users_path
      assert_equal I18n.t('user.failure.not_exist') % -1, flash[:alert]
      assert_nil assigns(:user)
    end
  end

  test "should not show new without blank id" do
    signed(@admin) do
      get :new, id: ''
      assert_response :redirect
      assert_redirected_to users_path
      assert_equal I18n.t('user.failure.not_exist') % '', flash[:alert]
      assert_nil assigns(:user)
    end
  end

  #create
  test "should create user_tariff" do

    @user.user_tariffs.destroy_all

    signed(@admin) do
      @valid_params.each do |valid_param|
        user_tariff_created do
          post :create, id: @user.id, tariff: valid_param
          assert_response :redirect

          assert_redirected_to index_tariffs_path(@user)
          assert_equal I18n.t('admin.user_tariff.success.created'), flash[:notice]

          new_user_tariff = @user.reload.user_tariffs.last
          assert_equal valid_param[:id], new_user_tariff.tariff.id
          assert_equal Date.parse(valid_param[:begin_date]), new_user_tariff.begin_date

          if valid_param[:end_date] == ''
            assert_nil new_user_tariff.end_date
          else
            assert_equal Date.parse(valid_param[:end_date]), new_user_tariff.end_date
          end

        end
      end
    end
  end

  #invalid create - invalid tariff_id
  test "should not create user_tariff without tariff_id" do
    signed(@admin) do
      user_tariff_not_created do
        post :create, id: @user.id, tariff: @valid_params.first.except(:id)
        assert_response :redirect
        assert_redirected_to new_tariffs_path(@user)
        assert_equal I18n.t('admin.user_tariff.failure.tariff_not_found') % '', flash[:alert]
      end
    end
  end

  test "should not create user_tariff with blank tariff_id" do
    signed(@admin) do
      user_tariff_not_created do
        post :create, id: @user.id, tariff: @valid_params.first.merge({id: ''})
        assert_response :redirect
        assert_redirected_to new_tariffs_path(@user)
        assert_equal I18n.t('admin.user_tariff.failure.tariff_not_found') % '', flash[:alert]
      end
    end
  end

  test "should not create user_tariff with not exist tariff_id" do
    signed(@admin) do
      user_tariff_not_created do
        post :create, id: @user.id, tariff: @valid_params.first.merge({id: -1})
        assert_response :redirect
        assert_redirected_to new_tariffs_path(@user)
        assert_equal I18n.t('admin.user_tariff.failure.tariff_not_found') % -1, flash[:alert]
      end
    end
  end

  #invalid create - invalid begin_date
  test "should not create user_tariff without begin_date" do
    signed(@admin) do
      user_tariff_not_created do
        post :create, id: @user.id, tariff: @valid_params.first.except(:begin_date)
        assert_response :redirect
        assert_redirected_to new_tariffs_path(@user)
        assert_equal I18n.t('admin.user_tariff.failure.begin_date_invalid') % '', flash[:alert]
      end
    end
  end

  test "should not create user_tariff with blank begin_date" do
    signed(@admin) do
      user_tariff_not_created do
        post :create, id: @user.id, tariff: @valid_params.first.merge({begin_date: ''})
        assert_response :redirect
        assert_redirected_to new_tariffs_path(@user)
        assert_equal I18n.t('admin.user_tariff.failure.begin_date_invalid') % '', flash[:alert]
      end
    end
  end

  test "should not create user_tariff with invalid begin_date" do
    signed(@admin) do
      user_tariff_not_created do
        post :create, id: @user.id, tariff: @valid_params.first.merge({begin_date: 'nvfjd'})
        assert_response :redirect
        assert_redirected_to new_tariffs_path(@user)
        assert_equal I18n.t('admin.user_tariff.failure.begin_date_invalid') % 'nvfjd', flash[:alert]
      end
    end
  end

  #invalid create - invalid end_date
  test "should not create user_tariff without end_date" do
    signed(@admin) do
      user_tariff_not_created do
        post :create, id: @user.id, tariff: @valid_params.first.except(:end_date)
        assert_response :redirect
        assert_redirected_to new_tariffs_path(@user)
        assert_equal I18n.t('admin.user_tariff.failure.end_date_invalid') % '', flash[:alert]
      end
    end
  end

  test "should not create user_tariff with invalid end_date" do
    signed(@admin) do
      user_tariff_not_created do
        post :create, id: @user.id, tariff: @valid_params.first.merge({end_date: 'nvfjd'})
        assert_response :redirect
        assert_redirected_to new_tariffs_path(@user)
        assert_equal I18n.t('admin.user_tariff.failure.end_date_invalid') % 'nvfjd', flash[:alert]
      end
    end
  end

  test "should not create user_tariff with end_date less then begin_date" do
    begin_date = (Date.today + 1.year).strftime('%Y-%m-%d')
    end_date = (Date.today - 1.year).strftime('%Y-%m-%d')

    signed(@admin) do
      user_tariff_not_created do
        post :create, id: @user.id, tariff: @valid_params.first.merge({begin_date: begin_date, end_date: end_date})
        assert_response :redirect
        assert_redirected_to new_tariffs_path(@user)
        assert_equal I18n.t('admin.user_tariff.failure.begin_date_should_be_less_then_end_date'), flash[:alert]
      end
    end
  end

  #check crossing for closed tariff
  test "should not create user_tariff when cross1 with closed user_tariff" do
    begin_date = (@user_tariff_with_closed_end_date.begin_date + 1.day).strftime('%Y-%m-%d')
    end_date = (@user_tariff_with_closed_end_date.end_date - 1.day).strftime('%Y-%m-%d')

    check_cross_tariff(@user_tariff_with_closed_end_date.user, begin_date, end_date)
  end

  test "should not create user_tariff when cross2 with closed user_tariff" do
    begin_date = (@user_tariff_with_closed_end_date.begin_date - 1.day).strftime('%Y-%m-%d')
    end_date = (@user_tariff_with_closed_end_date.end_date + 1.day).strftime('%Y-%m-%d')

    check_cross_tariff(@user_tariff_with_closed_end_date.user, begin_date, end_date)
  end

  test "should not create user_tariff when cross3 with closed user_tariff" do
    begin_date = (@user_tariff_with_closed_end_date.begin_date + 1.day).strftime('%Y-%m-%d')
    end_date = (@user_tariff_with_closed_end_date.end_date + 1.day).strftime('%Y-%m-%d')

    check_cross_tariff(@user_tariff_with_closed_end_date.user, begin_date, end_date)
  end

  test "should not create user_tariff when cross4 with closed user_tariff" do
    begin_date = (@user_tariff_with_closed_end_date.begin_date - 1.day).strftime('%Y-%m-%d')
    end_date = (@user_tariff_with_closed_end_date.end_date - 1.day).strftime('%Y-%m-%d')

    check_cross_tariff(@user_tariff_with_closed_end_date.user, begin_date, end_date)
  end

  test "should not create user_tariff when cross5 with closed user_tariff" do
    begin_date = (@user_tariff_with_closed_end_date.begin_date - 1.day).strftime('%Y-%m-%d')
    end_date = ''

    check_cross_tariff(@user_tariff_with_closed_end_date.user, begin_date, end_date)
  end

  test "should not create user_tariff when cross6 with closed user_tariff" do
    begin_date = (@user_tariff_with_closed_end_date.begin_date + 1.day).strftime('%Y-%m-%d')
    end_date = ''

    check_cross_tariff(@user_tariff_with_closed_end_date.user, begin_date, end_date)
  end

  test "should create user_tariff when new_closed_tariff started and ended after closed_user_tariff" do
    begin_date = (@user_tariff_with_closed_end_date.end_date + 1.day).strftime('%Y-%m-%d')
    end_date   = (@user_tariff_with_closed_end_date.end_date + 2.day).strftime('%Y-%m-%d')

    check_create_tariff(@user_tariff_with_closed_end_date.user, begin_date, end_date)
  end

  test "should create user_tariff when new_closed_tariff started and ended before closed_user_tariff" do
    begin_date = (@user_tariff_with_closed_end_date.begin_date - 1.week).strftime('%Y-%m-%d')
    end_date   = (@user_tariff_with_closed_end_date.begin_date - 1.week + 1.day).strftime('%Y-%m-%d')

    check_create_tariff(@user_tariff_with_closed_end_date.user, begin_date, end_date)
  end

  test "should create user_tariff when new_opened_tariff started after closed_user_tariff" do
    begin_date = (@user_tariff_with_closed_end_date.end_date + 1.week).strftime('%Y-%m-%d')
    end_date   = ''

    check_create_tariff(@user_tariff_with_closed_end_date.user, begin_date, end_date)
  end

  #check crossing for open tariff
  test "should not create user_tariff when new tariff after open user_tariff" do
    begin_date = (@user_tariff_with_opened_end_date.begin_date + 1.day).strftime('%Y-%m-%d')
    end_date = (@user_tariff_with_opened_end_date.begin_date + 2.day).strftime('%Y-%m-%d')

    check_cross_tariff(@user_tariff_with_opened_end_date.user, begin_date, end_date)
  end

  test "should not create user_tariff when new tariff cross open user_tariff" do
    begin_date = (@user_tariff_with_opened_end_date.begin_date - 1.day).strftime('%Y-%m-%d')
    end_date = (@user_tariff_with_opened_end_date.begin_date + 1.day).strftime('%Y-%m-%d')

    check_cross_tariff(@user_tariff_with_opened_end_date.user, begin_date, end_date)
  end

  test "should not create user_tariff when open_new_tariff start before previous open_tariff" do
    begin_date = (@user_tariff_with_opened_end_date.begin_date - 1.day).strftime('%Y-%m-%d')
    end_date = ''

    check_cross_tariff(@user_tariff_with_opened_end_date.user, begin_date, end_date)
  end

  test "should create user_tariff when closed_new_tariff ended before previous open_tariff" do
    begin_date = (@user_tariff_with_opened_end_date.begin_date - 1.week).strftime('%Y-%m-%d')
    end_date = (@user_tariff_with_opened_end_date.begin_date - 1.week + 1.day).strftime('%Y-%m-%d')

    check_create_tariff(@user_tariff_with_opened_end_date.user, begin_date, end_date)
  end

  private
  def check_cross_tariff(user, begin_date, end_date)
    signed(@admin) do
      user_tariff_not_created do
        post :create, id: user.id, tariff: @valid_params.first.merge({begin_date: begin_date, end_date: end_date})
        assert_response :redirect
        assert_redirected_to new_tariffs_path(user)
        assert_equal I18n.t('admin.user_tariff.failure.user_tariffs_cross'), flash[:alert]
      end
    end
  end


  def check_create_tariff(user, begin_date, end_date)
    valid_params = @valid_params.first
    signed(@admin) do
      user_tariff_created do
        post :create, id: user.id, tariff: valid_params.merge!({begin_date: begin_date, end_date: end_date})
        assert_response :redirect

        assert_redirected_to index_tariffs_path(user)
        assert_equal I18n.t('admin.user_tariff.success.created'), flash[:notice]

        new_user_tariff = user.user_tariffs.last
        assert_equal valid_params[:id], new_user_tariff.tariff.id
        assert_equal Date.parse(valid_params[:begin_date]), new_user_tariff.begin_date

        if valid_params[:end_date] == ''
          assert_nil new_user_tariff.end_date
        else
          assert_equal Date.parse(valid_params[:end_date]), new_user_tariff.end_date
        end

      end
    end
  end

  def user_tariff_not_created
    assert_no_difference 'UserTariff.count' do
      yield
    end
  end

  def user_tariff_created
    assert_difference 'UserTariff.count' do
      yield
    end
  end

  def user_tariff_deleted
    assert_difference 'UserTariff.count', -1 do
      yield
    end
  end

  def user_tariff_not_deleted
    assert_no_difference 'UserTariff.count' do
      yield
    end
  end
end
