# -*- encoding : utf-8 -*-
require 'test_helper'

class Admin::SenderNamesControllerTest < ActionController::TestCase
  def setup
    clear_db

    @admin = FactoryGirl.create(:admin)
    @user = FactoryGirl.create(:user)

    @sender_name_accepted = FactoryGirl.create(:sender_name_accepted, :user => @user)
    @sender_name_moderating = FactoryGirl.create(:sender_name_moderating, :user => @user)
    @sender_name_rejected = FactoryGirl.create(:sender_name_rejected, :user => @user)
  end

  #index
  test "should not get index for unsigned" do
    get :index
    assert_response :redirect
    assert_nil assigns(:sender_names)

    assert_redirected_to new_user_session_path
  end

  test "should not get index for user" do
    signed(@user) do
      get :index
      assert_response :redirect
      assert_nil assigns(:sender_names)

      assert_redirected_to root_path
    end
  end

  test "should get index for admin" do
    signed(@admin) do
      get :index
      assert_response :success
      assert_equal [@sender_name_moderating], assigns(:sender_names)

      assigns(:sender_names).each do |sender_name|
        assert sender_name.moderating?
      end

      assert_template 'admin/sender_names/index'
    end
  end

  #accept
  test "should not accept for unsigned" do
    get :accept, :id => @sender_name_moderating
    assert_response :redirect
    assert_nil assigns(:sender_name)

    assert_redirected_to new_user_session_path
    assert @sender_name_moderating.reload.moderating?
  end

  test "should not accept for user" do
    signed(@user) do
      get :accept, :id => @sender_name_moderating
      assert_response :redirect
      assert_nil assigns(:sender_name)

      assert_redirected_to root_path
      assert @sender_name_moderating.reload.moderating?
    end
  end

  test "should accept for admin" do
    signed(@admin) do
      get :accept, :id => @sender_name_moderating
      assert_response :redirect
      assert_equal @sender_name_moderating, assigns(:sender_name)

      assert_redirected_to admin_sender_names_path
      assert @sender_name_moderating.reload.accepted?
    end
  end

  test "should not accept for admin for not exist sender name" do
    signed(@admin) do
      get :accept, :id => -1
      assert_response :redirect
      assert_nil assigns(:sender_name)
      assert_equal I18n.t('sender_name.not_exist') % -1, flash[:alert]
      assert_redirected_to admin_sender_names_path
    end
  end

  #reject
  test "should not reject for unsigned" do
    get :reject, :id => @sender_name_moderating
    assert_response :redirect
    assert_nil assigns(:sender_name)

    assert_redirected_to new_user_session_path
    assert @sender_name_moderating.reload.moderating?
  end

  test "should not reject for user" do
    signed(@user) do
      get :reject, :id => @sender_name_moderating
      assert_response :redirect
      assert_nil assigns(:sender_name)

      assert_redirected_to root_path
      assert @sender_name_moderating.reload.moderating?
    end
  end

  test "should reject for admin" do
    signed(@admin) do
      get :reject, :id => @sender_name_moderating
      assert_response :redirect
      assert_equal @sender_name_moderating, assigns(:sender_name)

      assert_redirected_to admin_sender_names_path
      assert @sender_name_moderating.reload.rejected?
    end
  end

  test "should not reject for admin for not exist sender name" do
    signed(@admin) do
      get :reject, :id => -1
      assert_response :redirect
      assert_nil assigns(:sender_name)
      assert_equal I18n.t('sender_name.not_exist') % -1, flash[:alert]
      assert_redirected_to admin_sender_names_path
    end
  end

end
