# -*- encoding : utf-8 -*-
require 'test_helper'

class Admin::OperatorsControllerTest < ActionController::TestCase

  def setup
    clear_db

    @admin = FactoryGirl.create(:admin)
    @user = FactoryGirl.create(:user)
  end

  #show index
  test "should not show list of operators for unsigned" do
    get :index
    assert_response :redirect
    assert_nil assigns(:operators)
    assert_redirected_to new_user_session_path
  end

  test "should not show list of operators for user" do
    signed(@user) do
      get :index
      assert_response :redirect
      assert_nil assigns(:operators)
      assert_redirected_to root_path
    end
  end

  test "should show list of operators for admin" do
    signed(@admin) do
      get :index
      assert_response :success

      assert_equal CellOperatorGroup.all.sort { |o1, o2| o1.name <=> o2.name }, assigns(:operators)
    end
  end

end
