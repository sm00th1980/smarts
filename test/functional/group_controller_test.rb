# -*- encoding : utf-8 -*-
require 'test_helper'

class GroupControllerTest < ActionController::TestCase
  include PhoneHelper

  def setup
    Sidekiq::Testing.inline!

    clear_db

    @user = FactoryGirl.create(:user)
    @group = @user.groups.first

    #удаляем предварительно директорию с загруженными файлами
    FileUtils.rm_rf([Rails.root, GroupParser::UPLOAD_DIR].join)

    #csv
    @file_6_rows = fixture_file_upload("files/csv/6_rows.csv", 'application/xml')
    @file_12_rows = fixture_file_upload("files/csv/12_rows.csv", 'application/xml')
    @file_with_russian_name = fixture_file_upload("files/csv/русское_имя_файла.csv", 'application/xml')
    @file_with_invalid_rows = fixture_file_upload("files/csv/invalid.csv", 'application/xml')

    #xlsx
    @file_6_rows_xlsx = fixture_file_upload("files/xlsx/6_rows.xlsx", 'application/xml')

    #xls
    @file_6_rows_xls = fixture_file_upload("files/xls/6_rows.xls", 'application/xml')
  end

  def teardown
    #удаляем загруженных клиентов
    @group.clients.destroy_all

    @file_6_rows.close
    @file_12_rows.close
    @file_with_russian_name.close
    @file_with_invalid_rows.close
    @file_6_rows_xlsx.close
    @file_6_rows_xls.close
  end

  def stub_shows(type='phone desc', page=1)
    @clients = @group.clients
    Group.any_instance.stubs(:clients).returns(@clients)
    @clients.stubs(:order).with(type).returns(@clients)
    @clients.stubs(:paginate).with(:page => page, :per_page => GroupController::PER_PAGE).returns(@clients)
    @clients.stubs(:total_pages).returns(0)
  end

  %w(phone fio birthday_congratulation birthday description).each { |c|
    test "should order column #{c} desc" do
      signed @user do
        stub_shows "#{c} desc"
        get :show, :id => @group, column: c, order: 'desc'
        assert_not_nil assigns(:clients)
      end
    end
  }

  #birthday
  test "birthday_congratulation should update all group's user congratulation to true" do
    signed @user do
      request.env["HTTP_REFERER"] = 'back'
      Group.any_instance.expects(:delay).returns(@group)
      @group.expects(:set_users_birthday_congratulation_to).with(true)

      put :birthday_congratulation, id: @group.id, flag:'true'
      assert_redirected_to 'back'
      assert_equal I18n.t('group.all_clients_set_to_true_birthday_congratulation') , flash[:notice]
    end
  end

  test "birthday_congratulation should update all group's user congratulation to false" do
    signed @user do
      request.env["HTTP_REFERER"] = 'back'
      Group.any_instance.expects(:delay).returns(@group)
      @group.expects(:set_users_birthday_congratulation_to).with(false)

      put :birthday_congratulation, id: @group.id, flag:'false'
      assert_redirected_to 'back'
      assert_equal I18n.t('group.all_clients_set_to_false_birthday_congratulation') , flash[:notice]
    end
  end

  test "birthday_congratulation should update to false with bad-flag" do
    signed @user do
      request.env["HTTP_REFERER"] = 'back'

      put :birthday_congratulation, id: @group.id, flag:'bad_flag'
      assert_redirected_to 'back'
      assert_equal I18n.t('group.all_clients_set_to_false_birthday_congratulation') , flash[:notice]
    end
  end

  test "birthday_congratulation should return group_not_exist" do
    signed @user do
      request.env["HTTP_REFERER"] = 'back'
      put :birthday_congratulation, id: -1, flag:'true'

      assert_redirected_to groups_path
      assert_equal I18n.t('group.not_exist') % -1, flash[:alert]
    end
  end

  #show
  test 'should work with search' do
    stub_shows
    @clients.expects(:search).with('testtting').returns(@clients)
    signed(@group.user) do
      get :show, :id => @group, search:'testtting'
      assert_not_nil assigns(:clients)
    end
  end


  test 'should order table asc' do
    signed(@group.user) do
      stub_shows "phone asc"
      get :show, :id => @group, column: 'phone', order: 'asc'
      assert_not_nil assigns(:clients)
    end
  end

  test 'should order if page is empty' do
    signed(@group.user) do
      stub_shows "phone asc"
      get :show, :id => @group, column: 'phone', order: 'asc', page:''
      assert_not_nil assigns(:clients)
    end
  end

  test "should not show group for unsigned" do
    get :show, :id => @group
    assert_response :redirect
    assert_nil assigns(:clients)
    assert_redirected_to new_user_session_path
  end

  test "should show group for user" do
    assert_operator @group.clients.count, :>, 0

    stub_shows
    @clients.expects(:search).never

    signed(@group.user) do
      get :show, :id => @group
      assert_response :success
      assert_not_nil assigns(:clients)
    end
  end

  test "should not show group with not exist group_id" do
    signed(@group.user) do
      get :show, :id => -1
      assert_response :redirect
      assert_redirected_to groups_path
      assert_nil assigns(:clients)
      assert_equal I18n.t('group.not_exist') % -1, flash[:alert]
    end
  end

  test "should not show group if group is belongs to another user" do
    @user2 = FactoryGirl.create(:user)
    @group2 = @user2.groups.first

    signed(@group.user) do
      get :show, id: @group2
      assert_response :redirect
      assert_redirected_to groups_path
      assert_nil assigns(:clients)
      assert_equal I18n.t('group.not_exist') % @group2.id, flash[:alert]
    end
  end

  #destroy
  test "should not delete group for unsigned" do
    assert_no_difference 'Group.count' do
      post :destroy, :id => @group
      assert_response :redirect
      assert_nil assigns(:group)
      assert_redirected_to new_user_session_path
    end
  end

  test "should delete group for user" do
    assert_difference 'Group.count', -1 do
      signed(@group.user) do
        post :destroy, :id => @group
        assert_response :redirect
        assert_equal @group, assigns(:group)
        refute Group.exists?(id: @group)
      end
    end
  end

  test "should not delete group if group is belongs to another user" do
    @user2 = FactoryGirl.create(:user)
    @group2 = @user2.groups.first

    assert_no_difference 'Group.count' do
      signed(@group.user) do
        post :destroy, :id => @group2
        assert_response :redirect
        assert_redirected_to groups_path
        assert_nil assigns(:group)
        assert_equal I18n.t('group.not_exist') % @group2.id, flash[:alert]
        assert Group.exists?(id: @group2)
      end
    end
  end

  #create
  test "should not create group for unsigned" do
    assert_no_difference 'Group.count' do
      post :create, :group => {:name => 'new_name'}
      assert_response :redirect
      assert_redirected_to new_user_session_path
    end
  end

  test "should create group for user" do
    assert_difference 'Group.count' do
      signed(@user) do
        post :create, :group => {:name => 'new_name'}
        assert_response :redirect
        assert_redirected_to groups_path

        new_group = Group.find_by(name: 'new_name', user: @user)
        assert_not_nil new_group
      end
    end
  end

  #update
  test "should not update group for unsigned" do
    old_group_name = @group.name
    assert_no_difference 'Group.count' do
      post :update, id: @group, :group => {:name => 'new_name'}
      assert_response :redirect
      assert_redirected_to new_user_session_path
      assert_equal old_group_name, @group.reload.name
      assert_not_equal 'new_name', @group.reload.name
    end
  end

  test "should update group for user" do
    old_group_name = @group.name
    assert_no_difference 'Group.count' do
      signed(@user) do
        post :update, id: @group, :group => {:name => 'new_name'}
        assert_response :redirect
        assert_redirected_to show_group_path(@group)
        assert_not_equal old_group_name, @group.reload.name
        assert_equal 'new_name', @group.reload.name
      end
    end
  end

  test "should not update group if group belongs to another user" do
    @user2 = FactoryGirl.create(:user)
    @group2 = @user2.groups.first

    old_group_name = @group2.name
    assert_no_difference 'Group.count' do
      signed(@user) do
        post :update, id: @group2, :group => {:name => 'new_name'}
        assert_response :redirect
        assert_redirected_to groups_path
        assert_equal I18n.t('group.not_exist') % @group2.id, flash[:alert]
        assert_equal old_group_name, @group2.reload.name
        assert_not_equal 'new_name', @group2.reload.name
      end
    end
  end

  test "should not update group if group not exists" do
    assert_no_difference 'Group.count' do
      signed(@user) do
        post :update, id: -1, :group => {:name => 'new_name'}
        assert_response :redirect
        assert_redirected_to groups_path
        assert_equal I18n.t('group.not_exist') % -1, flash[:alert]
      end
    end
  end

  #upload
  test "should not get upload form for unsigned" do
    get :show_upload, :id => @group
    assert_response :redirect
    assert_redirected_to new_user_session_path
  end

  test "should not post upload for unsigned" do
    post :upload, :id => @group, :clients_file => @file_6_rows
    assert_response :redirect
    assert_redirected_to new_user_session_path
  end

  test "should get upload form" do
    signed(@user) do
      get :show_upload, :id => @group
      assert_response :success
    end
  end

  #upload csv
  test "should upload file with 6 rows" do
    signed(@user) do
      assert_difference "@group.clients.count", 6 do
        post :upload, :id => @group, :clients_file => @file_6_rows
      end
      assert_response :redirect
      assert_redirected_to groups_path

      assert File.exists? Rails.root.join('public', 'uploads', @file_6_rows.original_filename)
      assert_equal I18n.t('group.upload_started'), flash[:notice]
    end
  end

  test "should upload file with 12 rows" do
    signed(@user) do
      assert_difference "@group.clients.count", 12 do
        post :upload, :id => @group, :clients_file => @file_12_rows
      end
      assert_response :redirect
      assert_redirected_to groups_path

      assert File.exists? Rails.root.join('public', 'uploads', @file_12_rows.original_filename)
      assert_equal I18n.t('group.upload_started'), flash[:notice]
    end
  end

  test "should upload file with russian name" do
    signed(@user) do
      assert_difference "@group.clients.count", 7 do
        post :upload, :id => @group, :clients_file => @file_with_russian_name
      end
      assert_response :redirect
      assert_redirected_to groups_path

      assert File.exists? Rails.root.join('public', 'uploads', @file_with_russian_name.original_filename)
      assert_equal I18n.t('group.upload_started'), flash[:notice]
    end
  end

  test "should upload file with invalid rows" do
    signed(@user) do
      assert_no_difference "@group.clients.count" do
        post :upload, :id => @group, :clients_file => @file_with_invalid_rows
      end
      assert_response :redirect
      assert_redirected_to groups_path

      assert File.exists? Rails.root.join('public', 'uploads', @file_with_invalid_rows.original_filename)
      assert_equal I18n.t('group.upload_started'), flash[:notice]
    end
  end

  #upload xlsx
  test "should upload xlsx file with 6 rows" do
    @group.clients.destroy_all
    signed(@user) do
      assert_difference "@group.clients.count", 6 do
        post :upload, :id => @group, :clients_file => @file_6_rows_xlsx
      end
      assert_response :redirect
      assert_redirected_to groups_path

      assert File.exists? Rails.root.join('public', 'uploads', @file_6_rows_xlsx.original_filename)
      assert_equal I18n.t('group.upload_started'), flash[:notice]

      rows = []

      file = Rails.root.join('public', 'uploads', @file_6_rows_xlsx.original_filename)

      spreadsheet = Roo::Excelx.new(file.to_s, {}, :ignore)
      (1..spreadsheet.last_row).each do |i|
        _phone = spreadsheet.row(i)[0].is_a?(Float) ? spreadsheet.row(i)[0].to_i.to_s : spreadsheet.row(i)[0].to_s
        rows << [normalize_phone_number(_phone), spreadsheet.row(i)[1]]
      end

      assert_equal rows.sort, @group.reload.clients.map{|c| [c.phone, c.fio]}.sort
    end
  end

  #upload xls
  test "should upload xls file with 6 rows" do
    @group.clients.destroy_all
    signed(@user) do
      assert_difference "@group.clients.count", 6 do
        post :upload, :id => @group, :clients_file => @file_6_rows_xls
      end
      assert_response :redirect
      assert_redirected_to groups_path

      assert File.exists? Rails.root.join('public', 'uploads', @file_6_rows_xls.original_filename)
      assert_equal I18n.t('group.upload_started'), flash[:notice]

      rows = []

      file = Rails.root.join('public', 'uploads', @file_6_rows_xls.original_filename)

      spreadsheet = Roo::Excel.new(file.to_s, {}, :ignore)
      (1..spreadsheet.last_row).each do |i|
        _phone = spreadsheet.row(i)[0].is_a?(Float) ? spreadsheet.row(i)[0].to_i.to_s : spreadsheet.row(i)[0].to_s
        rows << [normalize_phone_number(_phone), spreadsheet.row(i)[1]]
      end

      assert_equal rows.sort, @group.reload.clients.map{|c| [c.phone, c.fio]}.sort
    end
  end
end
