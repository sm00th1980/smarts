# -*- encoding : utf-8 -*-
require 'test_helper'

class SessionsControllerTest < ActionController::TestCase

  def setup
    @request.env["devise.mapping"] = Devise.mappings[:user]

    @user = FactoryGirl.create(:user)
    @admin = FactoryGirl.create(:admin)

    @user_blocked = FactoryGirl.create(:user_blocked)
    @admin_blocked = FactoryGirl.create(:admin, status: UserStatus.blocked)

    @user_deleted = FactoryGirl.create(:user_deleted)
    @admin_deleted = FactoryGirl.create(:admin, status: UserStatus.deleted)
  end

  test "should show login page" do
    get :new
    assert_response :success
  end

  test "should show root page for user" do
    post :create, params(@user)
    check_login(@user)
  end

  test "should show root page for admin" do
    post :create, params(@admin)
    check_login(@admin)
  end

  #invalid password
  test "should not show root page for user with invalid password" do
    post :create, params_with_incorrect_password(@user)
    check_not_login
  end

  test "should not show root page for admin with invalid password" do
    post :create, params_with_incorrect_password(@admin)
    check_not_login
  end

  test "should not show root page without params" do
    post :create, {}
    check_not_login
  end

  #blocked
  test "should not show root page for blocked user" do
    assert @user_blocked.status.blocked?
    post :create, params(@user_blocked)
    check_not_login
  end

  test "should not show root page for blocked admin" do
    assert @admin_blocked.status.blocked?
    post :create, params(@admin_blocked)
    check_not_login
  end

  #deleted
  test "should not show root page for deleted user" do
    assert @user_deleted.status.deleted?
    post :create, params(@user_deleted)
    check_not_login
  end

  test "should not show root page for deleted admin" do
    assert @admin_deleted.status.deleted?
    post :create, params(@admin_deleted)
    check_not_login
  end

  #logout
  test "should logout for user" do
    sign_in @user
    assert warden.authenticated?(:user)
    delete :destroy
    check_not_login_after_logout
  end

  test "should logout for admin" do
    sign_in @admin
    assert warden.authenticated?(:user)
    delete :destroy
    check_not_login_after_logout
  end

  private
  def check_login(user)
    if user.admin?
      assert_redirected_to users_path
    else
      assert_redirected_to sms_path
    end

    assert_nil flash[:alert]
    assert_equal I18n.t('devise.sessions.user.signed_in'), flash[:notice]
    assert warden.authenticated?(:user)
  end

  def check_not_login
    assert_redirected_to new_user_session_path
    assert_equal I18n.t('devise.failure.user.invalid'), flash[:alert]
    assert_nil flash[:notice]
    refute warden.authenticated?(:user)
  end

  def check_not_login_after_logout
    assert_redirected_to new_user_session_path
    assert_equal I18n.t('devise.sessions.user.signed_out'), flash[:notice]
    assert_nil flash[:alert]
    refute warden.authenticated?(:user)
  end

  def params(user)
    {user: {"email" => user.email, "password" => user.password} }
  end

  def params_with_incorrect_password(user)
    {user: {"email" => user.email, "password" => (user.password + rand(100).to_s)} }
  end

end
