# -*- encoding : utf-8 -*-
require 'test_helper'

class Sms::SmsControllerDeletedUserTest < ActionController::TestCase
  tests SmsController

  def setup
    clear_db

    @user_deleted = FactoryGirl.create(:user_deleted)
    @phone_numbers = '89379903835,89379903836'

    stub_real_request
  end

  #index
  test "should not get index for deleted user" do
    signed(@user_deleted) do
      get :index
      assert_response :redirect
      assert_redirected_to new_user_session_path
    end
  end

  #sms send
  test "should not send_sms for deleted user" do
    assert_no_difference 'Log.count' do
      signed(@user_deleted) do
        post :send_sms, :client => {:sender_name => @user_deleted.active_sender_name_value, :sms_content => 'sms_content', :phone_numbers => @phone_numbers}
        assert_response :redirect
        assert_redirected_to new_user_session_path

        assert_equal I18n.t('devise.failure.user.invalid'), flash[:alert]
      end
    end
  end

end
