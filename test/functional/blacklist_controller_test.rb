# -*- encoding : utf-8 -*-
require 'test_helper'

class BlacklistControllerTest < ActionController::TestCase
  def setup
    clear_db

    @user = FactoryGirl.create(:user)
    @black_list = FactoryGirl.create(:black_list, user: @user)
  end

  #show
  test "should not show for unsigned" do
    get :show
    assert_response :redirect
    assert_nil assigns(:blocked_phones)
    assert_redirected_to new_user_session_path
  end

  test "should show index for user" do
    signed(@user) do
      get :show
      assert_response :success

      assert_equal [@black_list].sort, assigns(:blocked_phones).sort
    end
  end

  #create
  test "should not create black list for unsigned" do
    assert_no_difference 'BlackList.count' do
      post :create, phone: Test::Helpers.random_normal_phone_number

      assert_response :redirect
      assert_nil assigns(:black_lists)
      assert_redirected_to new_user_session_path
    end
  end

  test "should create black list for user" do
    phone = Test::Helpers.random_normal_phone_number

    assert_difference 'BlackList.count' do
      signed(@user) do
        post :create, phone: phone

        assert_response :redirect
        assert_redirected_to show_blacklist_path
        assert_equal I18n.t('user.black_list.success.created'), flash[:notice]

        assert_equal BlackList.last.phone, phone
        assert_equal BlackList.last.user, @user
      end
    end
  end

  test "should not create black list for user with invalid-phone" do
    phone = '-1'

    assert_no_difference 'BlackList.count' do
      signed(@user) do
        post :create, phone: phone

        assert_response :redirect
        assert_redirected_to show_blacklist_path
        assert_equal I18n.t('user.black_list.failure.invalid_phone') % phone, flash[:alert]
      end
    end
  end

  test "should not create black list for user if it already exists" do
    assert_no_difference 'BlackList.count' do
      signed(@user) do
        post :create, phone: @black_list.phone

        assert_response :redirect
        assert_redirected_to show_blacklist_path
        assert_equal I18n.t('user.black_list.failure.already_exist') % @black_list.phone, flash[:alert]
      end
    end
  end

  #destroy
  test "should not destroy black list for unsigned" do
    assert_no_difference 'BlackList.count' do
      post :destroy, id: @black_list.id

      assert_response :redirect
      assert_nil assigns(:black_lists)
      assert_redirected_to new_user_session_path
    end
  end

  test "should destroy black list for user" do
    assert_difference 'BlackList.count', -1 do
      signed(@user) do
        post :destroy, id: @black_list.id

        assert_response :redirect
        assert_redirected_to show_blacklist_path
        assert_equal I18n.t('user.black_list.success.deleted'), flash[:notice]

        assert_nil BlackList.find_by_id(@black_list.id)
      end
    end
  end

  test "should not destroy black list for user by not exists id" do
    id = -1
    assert_no_difference 'BlackList.count' do
      signed(@user) do
        post :destroy, id: id

        assert_response :redirect
        assert_redirected_to show_blacklist_path
        assert_equal I18n.t('user.black_list.failure.not_exist') % id, flash[:alert]
      end
    end
  end
end
