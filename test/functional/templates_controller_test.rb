# -*- encoding : utf-8 -*-
require 'test_helper'

class TemplatesControllerTest < ActionController::TestCase

  def setup
    clear_db

    @user = FactoryGirl.create(:user)
    @template = FactoryGirl.create(:template, :user => @user)
  end

  #index
  test "should not get index unsigned" do
    get :index
    assert_response :redirect
    assert_nil assigns(:templates)
    assert_redirected_to new_user_session_path
  end

  test "should get index signed" do
    signed(@user) do
      get :index
      assert_response :success

      assert_not_nil assigns(:templates)
      assert_equal @user.templates, assigns(:templates)
    end
  end

end
