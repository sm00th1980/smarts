# -*- encoding : utf-8 -*-
require 'test_helper'

class Group::LoadFileTest < ActionController::TestCase
  include PhoneHelper

  tests GroupController


  def setup
    Sidekiq::Testing.inline!

    clear_db

    @user = FactoryGirl.create(:user)
    @group = @user.groups.first
    @group.clients.destroy_all

    #удаляем предварительно директорию с загруженными файлами
    FileUtils.rm_rf([Rails.root, GroupParser::UPLOAD_DIR].join)

    #xls
    @file_6_6_rows_xls = fixture_file_upload("files/xls/6_6_rows.xls", 'application/xml')

    #xlsx
    @file_6_6_6_rows_xlsx = fixture_file_upload("files/xlsx/6_6_6_rows.xlsx", 'application/xml')

    #csv with birthday
    @file_6_rows_csv = fixture_file_upload("files/csv/6_rows_with_birthday.csv", 'application/xml')

    #xls with birthday
    @file_6_rows_xls_with_birthday = fixture_file_upload("files/xls/6_rows_with_birthday.xls", 'application/xml')

    #xlsx with birthday
    @file_6_rows_xlsx_with_birthday = fixture_file_upload("files/xlsx/6_rows_with_birthday.xlsx", 'application/xml')
  end

  def teardown
    #удаляем загруженных клиентов
    @group.clients.destroy_all
  end

  #upload csv with bithday
  test "should upload csv file with 6 rows and birthdays" do
    signed(@user) do
      assert_difference "@group.clients.count", 6 do
        post :upload, :id => @group, :clients_file => @file_6_rows_csv
      end
      assert_response :redirect
      assert_redirected_to groups_path

      assert File.exists? Rails.root.join('public', 'uploads', @file_6_rows_csv.original_filename)
      assert_equal I18n.t('group.upload_started'), flash[:notice]

      @group.reload.clients.each do |client|
        assert_not_nil client.birthday
      end

      birthdays = []
      CSV.foreach(Rails.root.join('public', 'uploads', @file_6_rows_csv.original_filename), :col_sep => ';', :encoding => GroupParser.encoding) do |row|
        birthdays << Date.parse(row.last)
      end

      assert_equal birthdays.sort, @group.reload.clients.map{|client| client.birthday}.sort
    end
  end

  #upload xls with 2 pages
  test "should upload xls file with 6 and 6 rows" do
    signed(@user) do
      assert_difference "@group.clients.count", 12 do
        post :upload, :id => @group, :clients_file => @file_6_6_rows_xls
      end
      assert_response :redirect
      assert_redirected_to groups_path

      assert File.exists? Rails.root.join('public', 'uploads', @file_6_6_rows_xls.original_filename)
      assert_equal I18n.t('group.upload_started'), flash[:notice]

      rows = []

      file = Rails.root.join('public', 'uploads', @file_6_6_rows_xls.original_filename)

      xls = Roo::Excel.new(file.to_s, {}, :ignore)
      (0...xls.sheets.size).each do |sheet_index|

        sheet = xls.sheet(sheet_index)
        (1..sheet.last_row).each do |i|
          _phone = sheet.row(i)[0].is_a?(Float) ? sheet.row(i)[0].to_i.to_s : sheet.row(i)[0].to_s
          rows << [normalize_phone_number(_phone), sheet.row(i)[1]]
        end
      end

      assert_equal rows.sort, @group.reload.clients.map { |c| [c.phone, c.fio] }.sort
    end
  end

  #upload xlsx with 3 pages
  test "should upload xlsx file with 6 and 6 and 6 rows" do
    signed(@user) do
      assert_difference "@group.clients.count", 18 do
        post :upload, :id => @group, :clients_file => @file_6_6_6_rows_xlsx
      end
      assert_response :redirect
      assert_redirected_to groups_path

      assert File.exists? Rails.root.join('public', 'uploads', @file_6_6_6_rows_xlsx.original_filename)
      assert_equal I18n.t('group.upload_started'), flash[:notice]

      rows = []

      file = Rails.root.join('public', 'uploads', @file_6_6_6_rows_xlsx.original_filename)

      xls = Roo::Excelx.new(file.to_s, {}, :ignore)
      (0...xls.sheets.size).each do |sheet_index|

        sheet = xls.sheet(sheet_index)
        (1..sheet.last_row).each do |i|
          _phone = sheet.row(i)[0].is_a?(Float) ? sheet.row(i)[0].to_i.to_s : sheet.row(i)[0].to_s
          rows << [normalize_phone_number(_phone), sheet.row(i)[1]]
        end
      end

      assert_equal rows.sort, @group.reload.clients.map { |c| [c.phone, c.fio] }.sort
    end
  end

  #upload xls with birthday
  test "should upload xls file with 6 rows and birthdays" do
    signed(@user) do
      assert_difference "@group.clients.count", 6 do
        post :upload, :id => @group, :clients_file => @file_6_rows_xls_with_birthday
      end
      assert_response :redirect
      assert_redirected_to groups_path

      assert File.exists? Rails.root.join('public', 'uploads', @file_6_rows_xls_with_birthday.original_filename)
      assert_equal I18n.t('group.upload_started'), flash[:notice]

      rows = []

      file = Rails.root.join('public', 'uploads', @file_6_rows_xls_with_birthday.original_filename)

      xls = Roo::Excel.new(file.to_s, {}, :ignore)
      (0...xls.sheets.size).each do |sheet_index|

        sheet = xls.sheet(sheet_index)
        (1..sheet.last_row).each do |i|
          _phone = sheet.row(i)[0].is_a?(Float) ? sheet.row(i)[0].to_i.to_s : sheet.row(i)[0].to_s
          rows << [normalize_phone_number(_phone), sheet.row(i)[1], sheet.row(i)[2]]
        end
      end

      assert_equal rows.sort, @group.reload.clients.map { |c| [c.phone, c.fio, c.birthday] }.sort
    end
  end

  #upload xlsx with birthday
  test "should upload xlsx file with 6 rows and birthdays" do
    signed(@user) do
      assert_difference "@group.clients.count", 6 do
        post :upload, :id => @group, :clients_file => @file_6_rows_xlsx_with_birthday
      end
      assert_response :redirect
      assert_redirected_to groups_path

      assert File.exists? Rails.root.join('public', 'uploads', @file_6_rows_xlsx_with_birthday.original_filename)
      assert_equal I18n.t('group.upload_started'), flash[:notice]

      rows = []

      file = Rails.root.join('public', 'uploads', @file_6_rows_xlsx_with_birthday.original_filename)

      xls = Roo::Excelx.new(file.to_s, {}, :ignore)
      (0...xls.sheets.size).each do |sheet_index|

        sheet = xls.sheet(sheet_index)
        (1..sheet.last_row).each do |i|
          _phone = sheet.row(i)[0].is_a?(Float) ? sheet.row(i)[0].to_i.to_s : sheet.row(i)[0].to_s
          rows << [normalize_phone_number(_phone), sheet.row(i)[1], sheet.row(i)[2]]
        end
      end

      assert_equal rows.sort, @group.reload.clients.map { |c| [c.phone, c.fio, c.birthday] }.sort
    end
  end
end
