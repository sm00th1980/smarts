# -*- encoding : utf-8 -*-
require 'test_helper'

class MessagePriceControllerTest < ActionController::TestCase
  def setup
    clear_db
    @user = FactoryGirl.create(:user)
    sender_name_value = FactoryGirl.create(:sender_name_accepted_with_verified_value, user: @user).value

    @params = {
        sender_name_value: sender_name_value,
        content: "Стоматология НОВА  Низкие цены для Вас!",
        phones: '',
        group_ids: ["group_id=#{@user.groups.first.id}"],
        template_id: ''
    }
  end

  test "should not get sms-price for unsigned" do
    get :calc, @params
    assert_response :redirect
    assert_redirected_to new_user_session_path
  end

  #valid calc
  test "should get sms-price with correct params" do
    Calculator::Groups.recalculate

    signed(@user) do
      get :calc, @params
      assert_response :success

      _sms_count = Message.sms_count(@params[:content])

      total_sms_price = 0
      @user.groups.first.clients.each do |client|
        _channel = Channel.channel(@params[:sender_name_value], @user.alpha_megafon_channel, client.phone)
        total_sms_price += _sms_count * Message.calc_price_per_sms(_channel, @user, client.phone)
      end

      assert_json_sms_price total_sms_price
    end
  end

  test "should get sms-price with phones only without group_ids" do
    _phones = '79379903835, 79379903836'
    Calculator::Groups.recalculate

    signed(@user) do
      get :calc, @params.except!(:group_ids).merge!({phones: _phones})
      assert_response :success

      _sms_count = Message.sms_count(@params[:content])

      total_sms_price = 0
      _phones.split(',').each do |phone|
        _channel = Channel.channel(@params[:sender_name_value], @user.alpha_megafon_channel, phone)
        total_sms_price += _sms_count * Message.calc_price_per_sms(_channel, @user, phone)
      end

      assert_json_sms_price total_sms_price
    end
  end

  test "should get sms-price with template_id and without content" do
    template = FactoryGirl.create(:template, user: @user)
    Calculator::Groups.recalculate

    signed(@user) do
      get :calc, @params.except!(:content).merge!({template_id: template.id})
      assert_response :success

      _sms_count = Message.sms_count(template.text)

      total_sms_price = 0
      @user.groups.first.clients.each do |client|
        _channel = Channel.channel(@params[:sender_name_value], @user.alpha_megafon_channel, client.phone)
        total_sms_price += _sms_count * Message.calc_price_per_sms(_channel, @user, client.phone)
      end

      assert_json_sms_price total_sms_price
    end
  end

  #invalid content
  test "should get zero price without content" do
    signed(@user) do
      get :calc, @params.except!(:content)
      assert_response :success

      assert_json_sms_price 0
    end
  end

  test "should get zero price with blank content" do
    signed(@user) do
      get :calc, @params.merge!({content: ''})
      assert_response :success

      assert_json_sms_price 0
    end
  end

  #invalid sender_name
  test "should get zero price without sender_name" do
    signed(@user) do
      get :calc, @params.except!(:sender_name_value)
      assert_response :success

      assert_json_sms_price 0
    end
  end

  test "should get zero price with blank sender_name_value" do
    signed(@user) do
      get :calc, @params.merge!({sender_name_value: ''})
      assert_response :success

      assert_json_sms_price 0
    end
  end

  #invalid group_ids and phones
  test "should get zero price without group_ids and phones" do
    signed(@user) do
      get :calc, @params.except!(:group_ids, :phones)
      assert_response :success

      assert_json_sms_price 0
    end
  end

  test "should get zero price with blank group_ids and blank phones" do
    signed(@user) do
      get :calc, @params.merge!({group_ids: [], phones: ''})
      assert_response :success

      assert_json_sms_price 0
    end
  end

  private
  def assert_json_sms_price(sms_price)
    assert_equal({'sms_price' => sms_price}, JSON.parse(@response.body))
  end
end
