# -*- encoding : utf-8 -*-
require 'test_helper'

class PasswordControllerTest < ActionController::TestCase
  def setup
    clear_db
    @user = FactoryGirl.create(:user)

    @params = {
        new_password: "new_password",
        confirmation_new_password: "new_password"
    }
  end

  #index
  test "should not pass to new for non-auth" do
    get :new
    assert_redirected_to new_user_session_path

    post :new
    assert_redirected_to new_user_session_path
  end

  test "should get index" do
    signed(@user) do
      get :new
      assert_response :success
    end
  end

  #update
  test "should update password for user" do
    refute @user.valid_password?(@params[:new_password])

    signed(@user) do
      post :update, @params
      assert_nil flash[:alert]
      assert_equal I18n.t('user.success.password.updated'), flash[:notice]
      assert_response :redirect
      assert_redirected_to new_user_session_path
      assert_equal @params[:new_password], assigns(:new_password)
      assert @user.reload.valid_password?(@params[:new_password])
    end
  end

  #invalid update
  test "should not update password for non-auth" do
    post :update, @params
    assert_redirected_to new_user_session_path
  end

  test "should not update password with blank password" do
    signed(@user) do
      post :update, @params.merge!({new_password: ''})
      assert_nil flash[:notice]
      assert_equal I18n.t('user.failure.password_empty'), flash[:alert]
      assert_response :redirect
      assert_redirected_to new_password_path
      assert_nil assigns(:new_password)

      refute @user.reload.valid_password?('')
      refute @user.reload.valid_password?(@params[:confirmation_new_password])
    end
  end

  test "should not update password with blank confirmation" do
    signed(@user) do
      post :update, @params.merge!({confirmation_new_password: ''})
      assert_nil flash[:notice]
      assert_equal I18n.t('user.failure.password_empty'), flash[:alert]
      assert_response :redirect
      assert_redirected_to new_password_path
      assert_nil assigns(:new_password)

      refute @user.reload.valid_password?('')
      refute @user.reload.valid_password?(@params[:new_password])
    end
  end

  test "should not update password with blank password and blank confirmation" do
    signed(@user) do
      post :update, @params.merge!({new_password: '', confirmation_new_password: ''})
      assert_nil flash[:notice]
      assert_equal I18n.t('user.failure.password_empty'), flash[:alert]
      assert_response :redirect
      assert_redirected_to new_password_path
      assert_nil assigns(:new_password)

      refute @user.reload.valid_password?('')
    end
  end

  test "should not update password with too short password" do
    signed(@user) do
      post :update, @params.merge!({new_password: '123456', confirmation_new_password: '123456'})
      assert_nil flash[:notice]
      assert_equal I18n.t('user.failure.password_too_short'), flash[:alert]
      assert_response :redirect
      assert_redirected_to new_password_path
      assert_nil assigns(:new_password)

      refute @user.reload.valid_password?('123456')
    end
  end

  test "should not update password when password and confirmation not equal" do
    new_password = "new_password_#{rand(1000)}"
    confirmation = "confirmation_new_password_#{rand(1000)}"

    signed(@user) do
      post :update, @params.merge!({new_password: new_password, confirmation_new_password: confirmation})
      assert_nil flash[:notice]
      assert_equal I18n.t('user.failure.passwords_not_equal'), flash[:alert]
      assert_response :redirect
      assert_redirected_to new_password_path
      assert_nil assigns(:new_password)

      refute @user.reload.valid_password?(new_password)
      refute @user.reload.valid_password?(confirmation)
    end
  end

end
