# -*- encoding : utf-8 -*-
require 'test_helper'

class ApiControllerInvalidTest < ActionController::TestCase
  tests ApiController

  def setup
    clear_db

    @user = FactoryGirl.create(:user_postpaid)
    sender_name_value = FactoryGirl.create(:sender_name_accepted_with_verified_value, user: @user).value


    @params = {:format => 'xml', :phones => ['79379903835'], :sender_name => sender_name_value, :text => 'привет', :login => @user.email, :password => @user.password}
    @params_report = {:format => 'xml', :login => @user.email, :password => @user.password}

    stub_real_request
  end

  test "should not send with phones not array1" do
    check_not_send_sms do
      get :send_sms, @params.merge({:phones => '9372253457'})
      assert_response :success
      assert_equal I18n.t('api.failure.phones_not_exist'), from_xml(@response.body)
    end
  end

  test "should not send with phones not array2" do
    check_not_send_sms do
      get :send_sms, @params.merge({:phones => '79381085191'})
      assert_response :success
      assert_equal I18n.t('api.failure.phones_not_exist'), from_xml(@response.body)
    end
  end

  test "should not send with phones not array3" do
    check_not_send_sms do
      get :send_sms, @params.merge({:phones => {"79272268617,79272774721"=>nil}})
      assert_response :success
      assert_equal I18n.t('api.failure.phones_not_exist'), from_xml(@response.body)
    end
  end

  test "should not send with phones not array4" do
    check_not_send_sms do
      get :send_sms, @params.merge({:phones => {"0"=>"+7 (938) 108-51-91", "1"=>"+7 (938) 108-51-91"}})
      assert_response :success
      assert_equal I18n.t('api.failure.phones_not_exist'), from_xml(@response.body)
    end
  end

  test "should not send with phones not array5" do
    check_not_send_sms do
      get :send_sms, @params.merge({:phones => {"0"=>"79381085191"}})
      assert_response :success
      assert_equal I18n.t('api.failure.phones_not_exist'), from_xml(@response.body)
    end
  end

  test "should not send with blank text" do
    check_not_send_sms do
      get :send_sms, @params.merge({:text => ''})
      assert_response :success
      assert_equal I18n.t('api.text_blank'), from_xml(@response.body)
    end
  end

  test "should not send with invalid sender name" do
    check_not_send_sms do
      get :send_sms, @params.merge({:sender_name => ''})
      assert_response :success
      assert_equal I18n.t('api.sender_name_invalid'), from_xml(@response.body)
    end
  end

  test "should not send without sender name" do
    check_not_send_sms do
      get :send_sms, @params.except(:sender_name)
      assert_response :success
      assert_equal I18n.t('api.sender_name_invalid'), from_xml(@response.body)
    end
  end

  test "should not send with not his own sender name" do
    user = FactoryGirl.create(:user)
    sender_name_value = FactoryGirl.create(:sender_name_accepted_with_verified_value, user: user).value

    check_not_send_sms do
      get :send_sms, @params.merge({:sender_name => sender_name_value})
      assert_response :success
      assert_equal I18n.t('api.sender_name_invalid'), from_xml(@response.body)
    end
  end

  #test "should not send with text not in utf8" do
  #  check_not_send_sms do
  #    get :send_sms, @params.merge({:text => "\xD2\xE5\xF1\xF2\xEE\xE2\xEE\xE5"})
  #    assert_response :success
  #    assert_equal I18n.t('api.text_blank'), from_xml(@response.body)
  #  end
  #end

  private
  def from_xml(xml)
    Nokogiri::XML::Document.parse(xml).children.first.children[1].children.first.text
  end

  def check_not_send_sms(&block)
    assert_no_difference "Log.count" do
      yield if block_given?
    end
  end

end
