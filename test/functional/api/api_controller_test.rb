# -*- encoding : utf-8 -*-
require 'test_helper'

class ApiControllerTest < ActionController::TestCase
  tests ApiController

  def setup
    clear_db

    @user = FactoryGirl.create(:user_postpaid)
    sender_name_value = FactoryGirl.create(:sender_name_accepted_with_verified_value, user: @user).value

    @params = {:format => 'xml', :phones => ['79379903835'], :sender_name => sender_name_value, :text => 'привет', :login => @user.email, :password => @user.password}
    @params_report = {:format => 'xml', :login => @user.email, :password => @user.password}

    stub_real_request
  end

  #invalid send
  test "should check routing for api" do
    assert_generates "/api/send", :controller => "api", :action => "send_sms"
    assert_generates "/api/report", :controller => "api", :action => "report_sms"
  end

  test "should not send without login and password" do
    check_not_send_sms do
      get :send_sms, @params.except(:login, :password)
      assert_response :success
      assert_equal I18n.t('devise.failure.user.invalid'), from_xml(@response.body)
    end
  end

  test "should not send with nil login and nil password" do
    check_not_send_sms do
      get :send_sms, @params.merge({:login => nil, :password => nil})
      assert_response :success
      assert_equal I18n.t('devise.failure.user.invalid'), from_xml(@response.body)
    end
  end

  test "should not send with blank login and blank password" do
    check_not_send_sms do
      get :send_sms, @params.merge({:login => '', :password => ''})
      assert_response :success
      assert_equal I18n.t('devise.failure.user.invalid'), from_xml(@response.body)
    end
  end

  test "should not send without phones" do
    check_not_send_sms do
      get :send_sms, @params.except(:phones)
      assert_response :success
      assert_equal I18n.t('api.failure.phones_not_exist'), from_xml(@response.body)
    end
  end

  test "should not send with blank phones" do
    check_not_send_sms do
      get :send_sms, @params.merge({:phones => []})
      assert_response :success
      assert_equal I18n.t('api.failure.phones_blank'), from_xml(@response.body)
    end
  end

  test "should not send with blank phones2" do
    assert_no_difference "Log.count" do
      get :send_sms, @params.merge({:phones => ''})
      assert_response :success
      assert_equal I18n.t('api.failure.phones_not_exist'), from_xml(@response.body)
    end
  end

  test "should not send with invalid phones" do
    check_not_send_sms do
      get :send_sms, @params.merge({:phones => ['nfjkdnvjfd']})
      assert_response :success
      assert_equal I18n.t('api.failure.phones_all_invalid'), from_xml(@response.body)
    end
  end

  test "should not send with invalid2 phones" do
    check_not_send_sms do
      get :send_sms, @params.merge({:phones => ['937nfjv78c999']})
      assert_response :success
      assert_equal I18n.t('api.failure.phones_all_invalid'), from_xml(@response.body)
    end
  end

  test "should not send sms with nil sender_name" do
    check_not_send_sms do
      get :send_sms, @params.merge({:sender_name => nil})
      assert_response :success
      assert_equal I18n.t('api.sender_name_invalid'), from_xml(@response.body)
    end
  end

  test "should not send sms with blank sender_name" do
    check_not_send_sms do
      get :send_sms, @params.merge({:sender_name => ''})
      assert_response :success
      assert_equal I18n.t('api.sender_name_invalid'), from_xml(@response.body)
    end
  end

  test "should not send without sender_name" do
    check_not_send_sms do
      get :send_sms, @params.except(:sender_name)
      assert_response :success
      assert_equal I18n.t('api.sender_name_invalid'), from_xml(@response.body)
    end
  end

  test "should not send with invalid sender_name" do
    check_not_send_sms do
      get :send_sms, @params.merge({:sender_name => "#{rand(100)}"})
      assert_response :success
      assert_equal I18n.t('api.sender_name_invalid'), from_xml(@response.body)
    end
  end

  test "should not send with moderating sender_name" do
    sender_name = FactoryGirl.create(:sender_name_moderating, user: @user)

    check_not_send_sms do
      get :send_sms, @params.merge({:sender_name => sender_name.value})
      assert_response :success
      assert_equal I18n.t('api.sender_name_invalid'), from_xml(@response.body)
    end
  end

  test "should not send with rejected sender_name" do
    sender_name = FactoryGirl.create(:sender_name_rejected, user: @user)

    check_not_send_sms do
      get :send_sms, @params.merge({:sender_name => sender_name.value})
      assert_response :success
      assert_equal I18n.t('api.sender_name_invalid'), from_xml(@response.body)
    end
  end

  test "should not send with not his own sender_name" do
    user = FactoryGirl.create(:user)
    sender_name_value = FactoryGirl.create(:sender_name_accepted_with_verified_value, user: user).value

    check_not_send_sms do
      get :send_sms, @params.merge({:sender_name => sender_name_value})
      assert_response :success
      assert_equal I18n.t('api.sender_name_invalid'), from_xml(@response.body)
    end
  end

  test "should not send with nil text" do
    check_not_send_sms do
      get :send_sms, @params.merge({:text => nil})
      assert_response :success
      assert_equal I18n.t('api.text_blank'), from_xml(@response.body)
    end
  end

  test "should not send with blank text" do
    check_not_send_sms do
      get :send_sms, @params.merge({:text => ''})
      assert_response :success
      assert_equal I18n.t('api.text_blank'), from_xml(@response.body)
    end
  end

  test "should not send with too much phones" do
    phones = num_to_array(SMSService::max_phones + 1, lambda{Test::Helpers::random_phone_number})
    check_not_send_sms do
      get :send_sms, @params.merge({:phones => phones})
      assert_response :success
      assert_equal I18n.t('api.failure.phones_too_much') % SMSService::max_phones, from_xml(@response.body)
    end
  end

  #normal send
  test "should send sms" do
    check_send_sms do
      get :send_sms, @params
      assert_response :success
      assert_equal Log.last.id.to_s, from_xml(@response.body)
      assert_equal LogType.api, Log.last.type
    end
  end

  #partial normal send
  test "should send with valid and invalid phones" do
    check_send_sms do
      get :send_sms, @params.merge({:phones => ['937nfjv78c999', '89379903835', '79379903835']})
      assert_response :success
      assert_equal Log.last.id.to_s, from_xml(@response.body)
      assert_equal LogType.api, Log.last.type
      assert_equal [79379903835], Log.last.phones
    end
  end

  #invalid report
  test "should not report without login and password" do
    get :report_sms, @params_report.except(:login, :password)
    assert_response :success
    assert_equal I18n.t('devise.failure.user.invalid'), from_xml(@response.body)
  end

  test "should not report with nil ID" do
    get :report_sms, @params_report.merge({:id => nil})
    assert_response :success
    assert_equal I18n.t('api.log_blank'), from_xml(@response.body)
  end

  test "should not report with blank ID" do
    get :report_sms, @params_report.merge({:id => ''})
    assert_response :success
    assert_equal I18n.t('api.log_blank'), from_xml(@response.body)
  end

  test "should not report without ID" do
    get :report_sms, @params_report
    assert_response :success
    assert_equal I18n.t('api.log_blank'), from_xml(@response.body)
  end

  test "should not report with not exist ID" do
    get :report_sms, @params_report.merge({:id => -1})
    assert_response :success
    assert_equal I18n.t('api.log_not_found'), from_xml(@response.body)
  end

  test "should not report with log_id of another_user" do
    log = FactoryGirl.create(:log_completed, user: FactoryGirl.create(:user))
    get :report_sms, @params_report.merge({:id => log.id})
    assert_response :success
    assert_equal I18n.t('api.log_not_found'), from_xml(@response.body)
  end

  #normal report
  test "should report" do
    FactoryGirl.create(:log_completed, user_id: @user.id)

    logs = Log.where(user_id: @user.id)
    assert_operator logs.count, :>, 0

    logs.each do |log|
      get :report_sms, @params_report.merge({:id => log.id})
      assert_response :success
      assert @response.body.present?

      assert_not_nil from_report(@response.body)
      assert_equal log.send_logs.map { |send_log| {id: send_log.id.to_s, status: send_log.delivery_status.name, phone: send_log.phone} }, from_report(@response.body)
    end
  end

  private
  def from_xml(xml)
    Nokogiri::XML::Document.parse(xml).children.first.children[1].children.first.text
  end

  def from_report(xml)
    send_logs = []

    Nokogiri::XML::Document.parse(xml).children.first.children.each do |el|
      send_logs << {id: el.attributes['id'].value, status: el.attributes['status'].value, phone: el.attributes['phone'].value} if el.attributes['id']
    end

    send_logs
  end

  def check_not_send_sms(&block)
    assert_no_difference "Log.count" do
      yield if block_given?
    end
  end

  def check_send_sms(&block)
    assert_difference "Log.count" do
      yield if block_given?
    end
  end
end

