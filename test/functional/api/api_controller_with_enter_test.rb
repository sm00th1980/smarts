# -*- encoding : utf-8 -*-
require 'test_helper'

class ApiControllerWithEnterTest < ActionController::TestCase
  tests ApiController

  def setup
    clear_db

    @user = FactoryGirl.create(:user_postpaid_with_negative_balance) #postpaid user с отрицательным балансом
    sender_name_value = FactoryGirl.create(:sender_name_accepted_with_verified_value, user: @user).value

    @params_with_1_enter = {
        :format => 'xml',
        :phones => ['79379903835'],
        :sender_name => sender_name_value,
        :text => "1\\r\\n2",
        :login => @user.email,
        :password => @user.password
    }

    @params_with_2_enter = {
        :format => 'xml',
        :phones => ['79379903835'],
        :sender_name => sender_name_value,
        :text => "1\\r\\n2\\r\\n3",
        :login => @user.email,
        :password => @user.password
    }

    stub_real_request
  end

  test "should replace 1 enter" do
    check_send_sms do
      get :send_sms, @params_with_1_enter
      assert_response :success
      assert_equal Log.last.id.to_s, from_xml(@response.body)
      assert_equal LogType.api, Log.last.type

      assert_equal "1\r\n2", assigns(:content)
    end
  end

  test "should replace 2 enter" do
    check_send_sms do
      get :send_sms, @params_with_2_enter
      assert_response :success
      assert_equal Log.last.id.to_s, from_xml(@response.body)
      assert_equal LogType.api, Log.last.type

      assert_equal "1\r\n2\r\n3", assigns(:content)
    end
  end

  private
  def from_xml(xml)
    Nokogiri::XML::Document.parse(xml).children.first.children[1].children.first.text
  end

  def check_send_sms(&block)
    assert_difference "Log.count" do
      yield if block_given?
    end
  end
end

