# -*- encoding : utf-8 -*-
require 'test_helper'

class ApiControllerBlockedUserTest < ActionController::TestCase
  tests ApiController

  def setup
    clear_db

    user_blocked = FactoryGirl.create(:user_blocked)
    @params = {
        :format => 'xml',
        :phones => ['79379903835'],
        :sender_name => user_blocked.active_sender_name_value,
        :text => 'привет', :login => user_blocked.email, :password => user_blocked.password
    }

    stub_real_request
  end

  #sms send
  test "should not send sms for blocked user" do
    check_not_send_sms do
      get :send_sms, @params
      assert_response :success
      assert_equal I18n.t('devise.failure.user.invalid'), from_xml(@response.body)
    end
  end

  private
  def from_xml(xml)
    Nokogiri::XML::Document.parse(xml).children.first.children[1].children.first.text
  end

  def check_not_send_sms(&block)
    assert_no_difference "Log.count" do
      yield if block_given?
    end
  end

end
