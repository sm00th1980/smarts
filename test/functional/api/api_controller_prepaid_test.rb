# -*- encoding : utf-8 -*-
require 'test_helper'

class ApiControllerPrepaidTest < ActionController::TestCase
  tests ApiController

  def setup
    clear_db

    @user_with_negative_balance = FactoryGirl.create(:user_with_negative_balance) #prepaid user с отрицательным балансом
    sender_name_value_negative = FactoryGirl.create(:sender_name_accepted_with_verified_value, user: @user_with_negative_balance).value


    @params_negative = {
        :format => 'xml',
        :phones => ['79379903835'],
        :sender_name => sender_name_value_negative,
        :text => 'привет',
        :login => @user_with_negative_balance.email,
        :password => @user_with_negative_balance.password
    }

    @user_with_positive_balance = FactoryGirl.create(:user_prepaid_with_positive_balance) #prepaid user с положительным балансом
    sender_name_value_positive = FactoryGirl.create(:sender_name_accepted_with_verified_value, user: @user_with_positive_balance).value

    @params_positive = {
        :format => 'xml',
        :phones => ['79379903835'],
        :sender_name => sender_name_value_positive,
        :text => 'привет',
        :login => @user_with_positive_balance.email,
        :password => @user_with_positive_balance.password
    }

    stub_real_request
  end

  test "should not send for prepaid user with negative balance" do
    recalculate_logs(@user_with_negative_balance) do
      check_not_send_sms do
        get :send_sms, @params_negative
        assert_response :success
        assert_equal I18n.t('api.failure.balance_too_low'), from_xml(@response.body)
      end
    end
  end

  test "should send for prepaid user with positive balance" do
    recalculate_logs(@user_with_positive_balance) do
      check_send_sms do
        get :send_sms, @params_positive
        assert_response :success
        assert_equal Log.last.id.to_s, from_xml(@response.body)
        assert_equal LogType.api, Log.last.type
      end
    end
  end

  private
  def from_xml(xml)
    Nokogiri::XML::Document.parse(xml).children.first.children[1].children.first.text
  end

  def check_send_sms(&block)
    assert_difference "Log.count" do
      yield if block_given?
    end
  end

  def check_not_send_sms(&block)
    assert_no_difference "Log.count" do
      yield if block_given?
    end
  end
end

