# -*- encoding : utf-8 -*-
require 'test_helper'

class ApiControllerPostpaidTest < ActionController::TestCase
  tests ApiController

  def setup
    clear_db

    @user = FactoryGirl.create(:user_postpaid_with_negative_balance) #postpaid user с отрицательным балансом
    sender_name_value = FactoryGirl.create(:sender_name_accepted_with_verified_value, user: @user).value

    @params = {
        :format => 'xml',
        :phones => ['79379903835'],
        :sender_name => sender_name_value,
        :text => 'привет',
        :login => @user.email,
        :password => @user.password
    }

    stub_real_request
  end

  test "should send for postpaid user with negative balance" do
    check_send_sms do
      get :send_sms, @params
      assert_response :success
      assert_equal Log.last.id.to_s, from_xml(@response.body)
      assert_equal LogType.api, Log.last.type
    end
  end

  private
  def from_xml(xml)
    Nokogiri::XML::Document.parse(xml).children.first.children[1].children.first.text
  end

  def check_send_sms(&block)
    assert_difference "Log.count" do
      yield if block_given?
    end
  end
end

