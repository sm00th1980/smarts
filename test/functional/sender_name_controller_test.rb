# -*- encoding : utf-8 -*-
require 'test_helper'

class SenderNameControllerTest < ActionController::TestCase

  def setup
    clear_db

    @user = FactoryGirl.create(:user)
    @sender_name_accepted = FactoryGirl.create(:sender_name_accepted, :user => @user)
    @sender_name_moderating = FactoryGirl.create(:sender_name_moderating, :user => @user)
    @sender_name_rejected = FactoryGirl.create(:sender_name_rejected, :user => @user)

    @sender_name_active = FactoryGirl.create(:sender_name_accepted, :user => @user)
    @sender_name_active.activate

    @user2 = FactoryGirl.create(:user)
    @sender_name_accepted2 = FactoryGirl.create(:sender_name_accepted, :user => @user2)
    @sender_name_active2 = FactoryGirl.create(:sender_name_accepted, :user => @user2)
    @sender_name_active2.activate

    @accepted_value = '79379903835'
    @rejected_value = '793799038354r344r43r43'
  end

  #use
  test "should not use for unsigned" do
    get :use, id: @sender_name_accepted
    assert_response :redirect
    refute @sender_name_accepted.reload.active?
    assert_redirected_to new_user_session_path
  end

  test "should use with accepted sender_name" do
    signed(@user) do
      get :use, id: @sender_name_accepted
      assert_response :redirect
      assert_equal I18n.t('sender_name.activate_success'), flash[:notice]
      assert @sender_name_accepted.reload.active?
      assert_redirected_to sender_names_path
    end
  end

  test "should not use with not exist sender_name" do
    signed(@user) do
      get :use, id: -1
      assert_response :redirect
      assert_equal I18n.t('sender_name.not_exist') % -1, flash[:alert]
      assert_redirected_to sender_names_path
    end
  end

  test "should not use with moderating sender_name" do
    signed(@user) do
      get :use, id: @sender_name_moderating
      assert_response :redirect
      assert_equal I18n.t('sender_name.on_moderating'), flash[:alert]
      refute @sender_name_rejected.reload.active?
      assert_redirected_to sender_names_path
    end
  end

  test "should not use with rejected sender_name" do
    signed(@user) do
      get :use, id: @sender_name_rejected
      assert_response :redirect
      assert_equal I18n.t('sender_name.verified_failed'), flash[:alert]
      refute @sender_name_rejected.reload.active?
      assert_redirected_to sender_names_path
    end
  end

  test "should not use with not own sender_name" do
    signed(@user) do
      get :use, id: @sender_name_accepted2
      assert_response :redirect
      assert_equal I18n.t('sender_name.not_exist') % @sender_name_accepted2.id, flash[:alert]
      refute @sender_name_accepted2.reload.active?
      assert_redirected_to sender_names_path
    end
  end

  #destroy
  test "should not destroy for unsigned" do
    post :destroy, id: @sender_name_accepted
    assert_response :redirect
    assert_redirected_to new_user_session_path
    assert SenderName.exists?(id: @sender_name_accepted)
  end

  test "should destroy for user" do
    signed(@user) do
      post :destroy, id: @sender_name_accepted
      assert_response :redirect
      assert_redirected_to sender_names_path
      assert_equal I18n.t('sender_name.delete_success'), flash[:notice]
      refute SenderName.exists?(id: @sender_name_accepted)
    end
  end

  test "should not destroy for user for not owner" do
    signed(@user) do
      post :destroy, id: @sender_name_active2
      assert_equal I18n.t('sender_name.not_exist') % @sender_name_active2.id, flash[:alert]
      assert SenderName.exists?(id: @sender_name_active2)
      assert_redirected_to sender_names_path
    end
  end

  #create
  test "should not create for unsigned" do
    assert_no_difference 'SenderName.count' do
      post :create, :value => @sender_name_accepted.value
      assert_response :redirect
      assert_redirected_to new_user_session_path
    end
  end

  test "should create for accepted value" do
    signed(@user) do
      post :create, :value => @accepted_value
      assert_response :redirect
      assert_equal I18n.t('sender_name.create_success'), flash[:notice]
      assert_redirected_to sender_names_path

      assert SenderName.find_by(value: @accepted_value, user: @user).moderating?
    end
  end

  test "should not create for rejected value" do
    assert_no_difference 'SenderName.count' do
      signed(@user) do
        post :create, :value => @rejected_value
        assert_response :redirect
        assert_equal I18n.t('sender_name.verified_failed'), flash[:alert]
        assert_redirected_to sender_names_path
      end
    end
  end

  test "should not create for already exist sender name" do
    assert_no_difference 'SenderName.count' do
      signed(@user) do
        post :create, :value => @sender_name_accepted.value
        assert_response :redirect
        assert_equal I18n.t('sender_name.already_exist') % @sender_name_accepted.value, flash[:alert]
        assert_redirected_to sender_names_path
      end
    end
  end

end
