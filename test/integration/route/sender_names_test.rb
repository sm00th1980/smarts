# -*- encoding : utf-8 -*-
require 'test_helper'

class RouteSenderNamesTest < ActionDispatch::IntegrationTest

  test "/sender_names get route should exists" do
    assert_routing({ method: 'get', path: '/sender_names' }, { controller: 'sender_names', action: 'index' })
  end

end
