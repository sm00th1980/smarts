# -*- encoding : utf-8 -*-
require 'test_helper'

class RouteNightDeliveryTest < ActionDispatch::IntegrationTest

  test "/night_delivery get route should exists" do
    assert_routing({ method: 'get', path: '/night_delivery' }, { controller: 'night_delivery', action: 'show' })
  end

  test "/night_delivery post route should exists" do
    assert_routing({ method: 'post', path: '/night_delivery' }, { controller: 'night_delivery', action: 'update' })
  end

end
