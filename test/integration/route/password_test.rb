# -*- encoding : utf-8 -*-
require 'test_helper'

class RoutePasswordTest < ActionDispatch::IntegrationTest

  test "/change_password get route should exists" do
    assert_routing({ method: 'get', path: '/change_password' }, { controller: 'password', action: 'new' })
  end

  test "/change_password post route should exists" do
    assert_routing({ method: 'post', path: '/change_password' }, { controller: 'password', action: 'update' })
  end

end
