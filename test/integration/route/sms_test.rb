# -*- encoding : utf-8 -*-
require 'test_helper'

class RouteSmsTest < ActionDispatch::IntegrationTest

  test "/sms get route should exists" do
    assert_routing({ method: 'get', path: '/sms' }, { controller: 'sms', action: 'index' })
  end

  test "/send_sms get route should exists" do
    assert_routing({ method: 'get', path: '/send_sms' }, { controller: 'sms', action: 'send_sms' })
  end

  test "/send_sms post route should exists" do
    assert_routing({ method: 'post', path: '/send_sms' }, { controller: 'sms', action: 'send_sms' })
  end

end
