# -*- encoding : utf-8 -*-
require 'test_helper'

class RouteBlacklistTest < ActionDispatch::IntegrationTest

  #new
  test "/blacklist/new get route should exists" do
    assert_routing({ method: 'get', path: '/blacklist/new' }, { controller: 'blacklist', action: 'new' })
  end

  #show
  test "/blacklist get route should exists" do
    assert_routing({ method: 'get', path: '/blacklist' }, { controller: 'blacklist', action: 'show' })
  end

  #create
  test "/blacklist post route should exists" do
    assert_routing({ method: 'post', path: '/blacklist' }, { controller: 'blacklist', action: 'create' })
  end

  #delete
  test "/blacklist/:id delete route should exists" do
    assert_routing({ method: 'delete', path: '/blacklist/67' }, { controller: 'blacklist', action: 'destroy', id: '67' })
  end

end
