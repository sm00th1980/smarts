# -*- encoding : utf-8 -*-
require 'test_helper'

class RouteTemplateTest < ActionDispatch::IntegrationTest

  #new
  test "/template/new get route should exists" do
    assert_routing({ method: 'get', path: '/template/new' }, { controller: 'template', action: 'new' })
  end

  #show
  test "/template/:id get route should exists" do
    assert_routing({ method: 'get', path: '/template/53' }, { controller: 'template', action: 'show', id: '53' })
  end

  #update
  test "/template/:id post route should exists" do
    assert_routing({ method: 'post', path: '/template/13' }, { controller: 'template', action: 'update', id: '13' })
  end

  #create
  test "/template post route should exists" do
    assert_routing({ method: 'post', path: '/template' }, { controller: 'template', action: 'create' })
  end

  #delete
  test "/template/:id delete route should exists" do
    assert_routing({ method: 'delete', path: '/template/67' }, { controller: 'template', action: 'destroy', id: '67' })
  end

end
