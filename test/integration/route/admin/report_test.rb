# -*- encoding : utf-8 -*-
require 'test_helper'

class RouteAdminReportTest < ActionDispatch::IntegrationTest

  #charges
  test "/admin/report/charges get route should exists" do
    assert_routing({ method: 'get', path: '/admin/report/charges' }, { controller: 'admin/report', action: 'charges' })
  end

  test "/admin/report/charges post route should exists" do
    assert_routing({ method: 'post', path: '/admin/report/charges' }, { controller: 'admin/report', action: 'charges' })
  end

  #charges/xls
  test "/admin/report/charges/xls get route should exists" do
    assert_routing({ method: 'get', path: '/admin/report/charges/xls' }, { controller: 'admin/report', action: 'charges_xls' })
  end

  #payments
  test "/admin/report/payments get route should exists" do
    assert_routing({ method: 'get', path: '/admin/report/payments' }, { controller: 'admin/report', action: 'payments' })
  end

  test "/admin/report/payments post route should exists" do
    assert_routing({ method: 'post', path: '/admin/report/payments' }, { controller: 'admin/report', action: 'payments' })
  end

end
