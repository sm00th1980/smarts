# -*- encoding : utf-8 -*-
require 'test_helper'

class RouteAdminPaymentsTest < ActionDispatch::IntegrationTest

  test "/admin/payments/:id get route should exists" do
    assert_routing({ method: 'get', path: '/admin/payments/31' }, { controller: "admin/payments", action: "index", id: "31" })
  end

  test "/admin/payments/:id/mew get route should exists" do
    assert_routing({ method: 'get', path: '/admin/payments/31/new' }, { controller: "admin/payments", action: "new", id: "31" })
  end

  test "/admin/payments/31/refuse post route should exists" do
    assert_routing({ method: 'post', path: '/admin/payments/31/refuse' }, { controller: "admin/payments", action: "refuse", id: "31" })
  end

  test "/admin/payments/31/approve route" do
    assert_routing({ method: 'post', path: '/admin/payments/31/approve' }, { controller: "admin/payments", action: "approve", id: "31" })
  end

  test "/admin/payments/31/create route" do
    assert_routing({ method: 'post', path: '/admin/payments/31/create' }, { controller: "admin/payments", action: "create", id: "31" })
  end

  test "/admin/payments/31/delete route" do
    assert_routing({ method: 'delete', path: '/admin/payments/31/delete' }, { controller: "admin/payments", action: "destroy", id: "31" })
  end

end
