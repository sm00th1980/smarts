# -*- encoding : utf-8 -*-
require 'test_helper'

class RouteAdminUsersTest < ActionDispatch::IntegrationTest

  #index
  test "/admin/users get route should exists" do
    assert_routing({ method: 'get', path: '/admin/users' }, { controller: 'admin/users', action: 'index' })
  end

end
