# -*- encoding : utf-8 -*-
require 'test_helper'

class RouteAdminUserTest < ActionDispatch::IntegrationTest

  #new
  test "/admin/user/new get route should exists" do
    assert_routing({ method: 'get', path: '/admin/user/new' }, { controller: 'admin/user', action: 'new' })
  end

  #create
  test "/admin/user/new post route should exists" do
    assert_routing({ method: 'post', path: '/admin/user/new' }, { controller: 'admin/user', action: 'create' })
  end

  #show
  test "/admin/user/:id get route should exists" do
    assert_routing({ method: 'get', path: '/admin/user/31' }, { controller: 'admin/user', action: 'show', id: '31' })
  end

  #update
  test "/admin/user/:id post route should exists" do
    assert_routing({ method: 'post', path: '/admin/user/22' }, { controller: 'admin/user', action: 'update', id: '22' })
  end

end
