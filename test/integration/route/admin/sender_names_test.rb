# -*- encoding : utf-8 -*-
require 'test_helper'

class RouteAdminSenderNamesTest < ActionDispatch::IntegrationTest

  #index
  test "/admin/sender_names get route should exists" do
    assert_routing({ method: 'get', path: '/admin/sender_names' }, { controller: 'admin/sender_names', action: 'index' })
  end

  #accept
  test "/admin/sender_names/:id/accept get route should exists" do
    assert_routing({ method: 'get', path: '/admin/sender_names/5/accept' }, { controller: 'admin/sender_names', action: 'accept', id: '5' })
  end

  #reject
  test "/admin/sender_names/:id/reject get route should exists" do
    assert_routing({ method: 'get', path: '/admin/sender_names/5/reject' }, { controller: 'admin/sender_names', action: 'reject', id: '5' })
  end

end
