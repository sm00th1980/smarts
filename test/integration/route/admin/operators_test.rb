# -*- encoding : utf-8 -*-
require 'test_helper'

class RouteAdminOperatorsTest < ActionDispatch::IntegrationTest

  #index
  test "/admin/operators get route should exists" do
    assert_routing({ method: 'get', path: '/admin/operators' }, { controller: 'admin/operators', action: 'index' })
  end

end
