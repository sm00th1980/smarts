# -*- encoding : utf-8 -*-
require 'test_helper'

class RouteAdminTariffsTest < ActionDispatch::IntegrationTest

  #index
  test "/admin/tariffs/:id get route should exists" do
    assert_routing({ method: 'get', path: '/admin/tariffs/22' }, { controller: 'admin/tariffs', action: 'index', id: '22' })
  end

  #destroy
  test "/admin/tariffs/:id/delete get route should exists" do
    assert_routing({ method: 'delete', path: '/admin/tariffs/69/delete' }, { controller: 'admin/tariffs', action: 'destroy', id: '69' })
  end

  #new
  test "/admin/tariffs/:id/new get route should exists" do
    assert_routing({ method: 'get', path: '/admin/tariffs/69/new' }, { controller: 'admin/tariffs', action: 'new', id: '69' })
  end

  #create
  test "/admin/tariffs/:id/create get route should exists" do
    assert_routing({ method: 'post', path: '/admin/tariffs/69/create' }, { controller: 'admin/tariffs', action: 'create', id: '69' })
  end

end
