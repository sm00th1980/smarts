# -*- encoding : utf-8 -*-
require 'test_helper'

class RouteAdminBlackListsTest < ActionDispatch::IntegrationTest

  #index
  test "/admin/black_lists get route should exists" do
    assert_routing({ method: 'get', path: '/admin/black_lists' }, { controller: 'admin/black_lists', action: 'index' })
  end

  #new
  test "/admin/black_lists/new get route should exists" do
    assert_routing({ method: 'get', path: '/admin/black_lists/new' }, { controller: 'admin/black_lists', action: 'new' })
  end

  #create
  test "/admin/black_lists post route should exists" do
    assert_routing({ method: 'post', path: '/admin/black_lists' }, { controller: 'admin/black_lists', action: 'create' })
  end

  #destroy
  test "/admin/black_lists/4 delete route should exists" do
    assert_routing({ method: 'delete', path: '/admin/black_lists/4' }, { controller: 'admin/black_lists', action: 'destroy', id: '4' })
  end

end
