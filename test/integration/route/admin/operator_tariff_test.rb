# -*- encoding : utf-8 -*-
require 'test_helper'

class RouteAdminOperatorTariffTest < ActionDispatch::IntegrationTest

  #index
  test "/admin/operator/:operator_id/tariffs get route should exists" do
    assert_routing({ method: 'get', path: '/admin/operator/22/tariffs' }, { controller: 'admin/operator_tariff', action: 'index', operator_id: '22' })
  end

  #new
  test "/admin/operator/:operator_id/tariff/new get route should exists" do
    assert_routing({ method: 'get', path: '/admin/operator/69/tariff/new' }, { controller: 'admin/operator_tariff', action: 'new', operator_id: '69' })
  end

  #create
  test "/admin/operator/:operator_id/tariff/create post route should exists" do
    assert_routing({ method: 'post', path: '/admin/operator/76/tariff/create' }, { controller: 'admin/operator_tariff', action: 'create', operator_id: '76' })
  end

  #edit
  test "/admin/operator/tariff/:operator_tariff_id/edit get route should exists" do
    assert_routing({ method: 'get', path: '/admin/operator/tariff/5/edit' }, { controller: 'admin/operator_tariff', action: 'edit', operator_tariff_id: '5' })
  end

  #update
  test "/admin/operator/tariff/:operator_tariff_id/update post route should exists" do
    assert_routing({ method: 'post', path: '/admin/operator/tariff/6/update' }, { controller: 'admin/operator_tariff', action: 'update', operator_tariff_id: '6' })
  end

  #delete
  test "/admin/operator/tariff/:operator_tariff_id delete route should exists" do
    assert_routing({ method: 'delete', path: '/admin/operator/tariff/7' }, { controller: 'admin/operator_tariff', action: 'destroy', operator_tariff_id: '7' })
  end

end
