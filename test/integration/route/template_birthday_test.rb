# -*- encoding : utf-8 -*-
require 'test_helper'

class RouteTemplateBirthdayTest < ActionDispatch::IntegrationTest

  #show
  test "/template_birthday get route should exists" do
    assert_routing({ method: 'get', path: '/template_birthday' }, { controller: 'template_birthday', action: 'show' })
  end

  #update
  test "/template_birthday post route should exists" do
    assert_routing({ method: 'post', path: '/template_birthday' }, { controller: 'template_birthday', action: 'update' })
  end

end
