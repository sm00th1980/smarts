# -*- encoding : utf-8 -*-
require 'test_helper'

class RouteTemplatesTest < ActionDispatch::IntegrationTest

  test "/templates get route should exists" do
    assert_routing({ method: 'get', path: '/templates' }, { controller: 'templates', action: 'index' })
  end

end
