# -*- encoding : utf-8 -*-
require 'test_helper'

class RouteClientTest < ActionDispatch::IntegrationTest

  #new
  test "/client/new get route should exists" do
    assert_routing({ method: 'get', path: '/client/new' }, { controller: 'client', action: 'new' })
  end

  #show
  test "/client/:id get route should exists" do
    assert_routing({ method: 'get', path: '/client/13' }, { controller: 'client', action: 'show', id: '13' })
  end

  #create
  test "/client post route should exists" do
    assert_routing({ method: 'post', path: '/client' }, { controller: 'client', action: 'create' })
  end

  #update
  test "/client/:id post route should exists" do
    assert_routing({ method: 'post', path: '/client/13' }, { controller: 'client', action: 'update', id: '13' })
  end

  #delete
  test "/client/:id delete route should exists" do
    assert_routing({ method: 'delete', path: '/client/67' }, { controller: 'client', action: 'destroy', id: '67' })
  end

  #birthday_congratulation
  test "/client/:id/birthday_congratulation post route should exists" do
    assert_routing({ method: 'post', path: '/client/13/birthday_congratulation' }, { controller: 'client', action: 'birthday_congratulation_update', id: '13' })
  end

end
