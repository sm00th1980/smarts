# -*- encoding : utf-8 -*-
require 'test_helper'

class RouteSenderNameTest < ActionDispatch::IntegrationTest

  #new
  test "/sender_name/new get route should exists" do
    assert_routing({ method: 'get', path: '/sender_name/new' }, { controller: 'sender_name', action: 'new' })
  end

  #/use
  test "/sender_name/:id/use get route should exists" do
    assert_routing({ method: 'get', path: '/sender_name/13/use' }, { controller: 'sender_name', action: 'use', id: '13' })
  end

  #create
  test "/sender_name post route should exists" do
    assert_routing({ method: 'post', path: '/sender_name' }, { controller: 'sender_name', action: 'create' })
  end

  #delete
  test "/sender_name/:id delete route should exists" do
    assert_routing({ method: 'delete', path: '/sender_name/67' }, { controller: 'sender_name', action: 'destroy', id: '67' })
  end

end
