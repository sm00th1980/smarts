# -*- encoding : utf-8 -*-
require 'test_helper'

class RouteSessionTest < ActionDispatch::IntegrationTest

  #login
  test "/login get route should exists" do
    assert_routing({ method: 'get', path: '/login' }, { controller: 'sessions', action: 'new' })
  end

  test "/login post route should exists" do
    assert_routing({ method: 'post', path: '/login' }, { controller: 'sessions', action: 'create' })
  end

  #logout
  test "/logout delete route should exists" do
    assert_routing({ method: 'delete', path: '/logout' }, { controller: 'sessions', action: 'destroy' })
  end

end
