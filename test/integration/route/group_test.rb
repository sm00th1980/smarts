# -*- encoding : utf-8 -*-
require 'test_helper'

class RouteGroupTest < ActionDispatch::IntegrationTest

  test "/group/:id/upload get route should exists" do
    assert_routing({ method: 'get', path: '/group/13/upload' }, { controller: 'group', action: 'show_upload', id: '13' })
  end

  test "/group/:id/upload post route should exists" do
    assert_routing({ method: 'post', path: '/group/13/upload' }, { controller: 'group', action: 'upload', id: '13' })
  end

  test "/group/:id/birthday_congratulation post route should exists" do
    assert_routing({ method: 'post', path: '/group/13/birthday_congratulation' }, { controller: 'group', action: 'birthday_congratulation', id: '13' })
  end

  #show
  test "/group/:id get route should exists" do
    assert_routing({ method: 'get', path: '/group/56' }, { controller: 'group', action: 'show', id: '56' })
  end

  #new
  test "/group/new get route should exists" do
    assert_routing({ method: 'get', path: '/group/new' }, { controller: 'group', action: 'new' })
  end

  #create
  test "/group post route should exists" do
    assert_routing({ method: 'post', path: '/group' }, { controller: 'group', action: 'create' })
  end

  #delete
  test "/group/:id delete route should exists" do
    assert_routing({ method: 'delete', path: '/group/67' }, { controller: 'group', action: 'destroy', id: '67' })
  end

  #update
  test "/group/:id post route should exists" do
    assert_routing({ method: 'post', path: '/group/67' }, { controller: 'group', action: 'update', id: '67' })
  end

end
