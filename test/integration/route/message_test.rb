# -*- encoding : utf-8 -*-
require 'test_helper'

class RouteMessageTest < ActionDispatch::IntegrationTest

  test "/message/sms_count get route should exists" do
    assert_routing({ method: 'get', path: '/message/sms_count' }, { controller: 'message', action: 'sms_count' })
  end

  test "/message_price/calc get route should exists" do
    assert_routing({ method: 'get', path: '/message_price/calc' }, { controller: 'message_price', action: 'calc' })
  end

end
