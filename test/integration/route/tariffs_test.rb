# -*- encoding : utf-8 -*-
require 'test_helper'

class RouteTariffsTest < ActionDispatch::IntegrationTest

  test "/tariffs get route should exists" do
    assert_routing({ method: 'get', path: '/tariffs' }, { controller: 'tariffs', action: 'index' })
  end

end
