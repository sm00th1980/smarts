# -*- encoding : utf-8 -*-
require 'test_helper'

class RouteLogTest < ActionDispatch::IntegrationTest

  #show
  test "/log/:id get route should exists" do
    assert_routing({ method: 'get', path: '/log/123' }, { controller: 'log', action: 'show', id: '123' })
  end

  #stop
  test "/log/:id/stop get route should exists" do
    assert_routing({ method: 'get', path: '/log/123/stop' }, { controller: 'log', action: 'stop', id: '123' })
  end

  #report
  test "/log/:id/report get route should exists" do
    assert_routing({ method: 'get', path: '/log/123/report' }, { controller: 'log', action: 'report', id: '123' })
  end

  #delete_clients
  test "/log/:id/delete_clients get route should exists" do
    assert_routing({ method: 'get', path: '/log/123/delete_clients' }, { controller: 'log', action: 'delete_clients', id: '123' })
  end

  #new_delivery
  test "/log/:id/new_delivery get route should exists" do
    assert_routing({ method: 'get', path: '/log/123/new_delivery' }, { controller: 'log', action: 'new_delivery', id: '123' })
  end

end
