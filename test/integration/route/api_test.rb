# -*- encoding : utf-8 -*-
require 'test_helper'

class RouteApiTest < ActionDispatch::IntegrationTest

  #send
  test "/api/send get route should exists" do
    assert_routing({ method: 'get', path: '/api/send' }, { controller: 'api', action: 'send_sms' })
  end

  #report
  test "/api/report get route should exists" do
    assert_routing({ method: 'get', path: '/api/report' }, { controller: 'api', action: 'report_sms' })
  end

end
