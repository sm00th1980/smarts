# -*- encoding : utf-8 -*-
require 'test_helper'

class RouteReportByDateTest < ActionDispatch::IntegrationTest

  test "/report_by_date get route should exists" do
    assert_routing({ method: 'get', path: '/report_by_date' }, { controller: 'report_by_date', action: 'show' })
  end

  test "/report_by_date post route should exists" do
    assert_routing({ method: 'post', path: '/report_by_date' }, { controller: 'report_by_date', action: 'generate' })
  end

end
