# -*- encoding : utf-8 -*-
require 'test_helper'

class RouteContactTest < ActionDispatch::IntegrationTest

  test "/contact get route should exists" do
    assert_routing({ method: 'get', path: '/contact' }, { controller: 'contact', action: 'show' })
  end

  test "/contact post route should exists" do
    assert_routing({ method: 'post', path: '/contact' }, { controller: 'contact', action: 'update' })
  end

end
