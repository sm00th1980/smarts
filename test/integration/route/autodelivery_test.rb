# -*- encoding : utf-8 -*-
require 'test_helper'

class RouteAutodeliveryTest < ActionDispatch::IntegrationTest

  test "/autodelivery/:id/activate get route should exists" do
    assert_routing({ method: 'get', path: '/autodelivery/13/activate' }, { controller: 'autodelivery', action: 'activate', id: '13' })
  end

  test "/autodelivery/:id/deactivate get route should exists" do
    assert_routing({ method: 'get', path: '/autodelivery/13/deactivate' }, { controller: 'autodelivery', action: 'deactivate', id: '13' })
  end

  test "/autodelivery/:id/run get route should exists" do
    assert_routing({ method: 'get', path: '/autodelivery/13/run' }, { controller: 'autodelivery', action: 'run', id: '13' })
  end

  #index
  test "/autodelivery get route should exists" do
    assert_routing({ method: 'get', path: '/autodelivery' }, { controller: 'autodelivery', action: 'index' })
  end

  #show
  test "/autodelivery/:id get route should exists" do
    assert_routing({ method: 'get', path: '/autodelivery/56' }, { controller: 'autodelivery', action: 'show', id: '56' })
  end

  #new
  test "/autodelivery/new get route should exists" do
    assert_routing({ method: 'get', path: '/autodelivery/new' }, { controller: 'autodelivery', action: 'new' })
  end

  #create
  test "/autodelivery post route should exists" do
    assert_routing({ method: 'post', path: '/autodelivery' }, { controller: 'autodelivery', action: 'create' })
  end

  #delete
  test "/autodelivery/:id delete route should exists" do
    assert_routing({ method: 'delete', path: '/autodelivery/67' }, { controller: 'autodelivery', action: 'destroy', id: '67' })
  end

  #update
  test "/autodelivery/:id post route should exists" do
    assert_routing({ method: 'post', path: '/autodelivery/67' }, { controller: 'autodelivery', action: 'update', id: '67' })
  end

end
