# -*- encoding : utf-8 -*-
require 'test_helper'

class RouteWelcomeTest < ActionDispatch::IntegrationTest

  #root
  test "/ get route should exists" do
    assert_routing({ method: 'get', path: '/' }, { controller: 'welcome', action: 'index' })
  end

end
