# -*- encoding : utf-8 -*-
require 'test_helper'

class RouteGroupsTest < ActionDispatch::IntegrationTest

  test "/groups get route should exists" do
    assert_routing({ method: 'get', path: '/groups' }, { controller: 'groups', action: 'index' })
  end

end
