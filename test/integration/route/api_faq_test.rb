# -*- encoding : utf-8 -*-
require 'test_helper'

class RouteApiFaqTest < ActionDispatch::IntegrationTest

  #faq
  test "/api/faq route should exists" do
    assert_routing({ method: 'get', path: '/api/faq' }, { controller: 'api_faq', action: 'index' })
  end


end
