# -*- encoding : utf-8 -*-
require 'test_helper'

class RouteLogsTest < ActionDispatch::IntegrationTest

  test "/logs get route should exists" do
    assert_routing({ method: 'get', path: '/logs' }, { controller: 'logs', action: 'index' })
  end

end
