# -*- encoding : utf-8 -*-
require 'test_helper'

class UserStatusTest < ActiveSupport::TestCase

  def setup
    clear_db
  end

  test "should active status exist" do
    assert_nothing_raised do
      assert_not_nil UserStatus.active
      assert_equal 'active', UserStatus.active.internal_name
      assert UserStatus.active.active?
    end
  end

  test "should blocked status exist" do
    assert_nothing_raised do
      assert_not_nil UserStatus.blocked
      assert_equal 'blocked', UserStatus.blocked.internal_name
      assert UserStatus.blocked.blocked?
    end
  end

  test "should deleted status exist" do
    assert_nothing_raised do
      assert_not_nil UserStatus.deleted
      assert_equal 'deleted', UserStatus.deleted.internal_name
      assert UserStatus.deleted.deleted?
    end
  end

end
