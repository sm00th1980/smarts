# -*- encoding : utf-8 -*-
require 'simplecov'
SimpleCov.start

ENV["RAILS_ENV"] = "test"
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

require 'minitest/unit'
require 'mocha/mini_test'

require 'minitest/reporters'
MiniTest::Reporters.use!

require 'sidekiq/testing'
Sidekiq::Testing.inline!

class ActionController::TestCase
  include Devise::TestHelpers
end

module Test
  module Helpers
    extend PhoneHelper

    def self.random_phone_number
      "7(9#{(rand(90) + 10).to_s})#{rand(900) + 100}-#{rand(90) + 10}-#{rand(90) + 10}"
    end

    def self.random_normal_phone_number
      normalize_phone_number(random_phone_number)
    end
  end
end

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.(yml|csv) for all tests in alphabetical order.
  #
  # Note: You'll currently still have to declare fixtures explicitly in integration tests
  # -- they do not yet inherit this setting
  fixtures :all

  # Add more helper methods to be used by all tests here...

  def signed(user=FactoryGirl.create(:user), &block)
    sign_in user
    yield
    sign_out user
  end

  def admin_signed(&block)
    sign_in users(:admin)
    yield
    sign_out users(:admin)
  end

  #delivery
  def success_response
    '0: Accepted for delivery'
  end

  def error_response
    'error'
  end

  def day_time
    Time.now.change(hour: TimeHelper::BEGIN_HOUR, min: TimeHelper::BEGIN_MIN, sec: TimeHelper::BEGIN_SEC) + 1.hour
  end

  def night_time
    Time.now.change(hour: TimeHelper::END_HOUR, min: TimeHelper::END_MIN, sec: TimeHelper::END_SEC) + 1.hour
  end

  def stub_real_request(time=day_time, &block)
    Sidekiq::Testing.fake! do
      freeze(time) do
        url = Rails.configuration.kannel[:host]

        clear_expectation_by_url(url)

        body = success_response
        response = Typhoeus::Response.new(code: 200, body: body, total_time: Random.new.rand(1.0))
        Typhoeus.stub(url).and_return(response)

        yield if block_given?
      end
    end
  end

  def stub_real_request_with_error(time=day_time, &block)
    Sidekiq::Testing.fake! do
      freeze(time) do
        url = Rails.configuration.kannel[:host]

        clear_expectation_by_url(url)

        body = error_response
        response = Typhoeus::Response.new(code: 200, body: body, total_time: Random.new.rand(1.0))
        Typhoeus.stub(url).and_return(response)

        yield if block_given?
      end
    end
  end

  def clear_db
    ActiveRecord::Base.descendants.reject { |model| model.name == "LogStat" }.map { |model| model.delete_all }

    #заполняем справочники
    (SendLogDeliveryError.methods - Object.methods - ActiveRecord::Base.methods - [:reject_error_by_operator]).each do |method|
      SendLogDeliveryError.send(method)
    end

    (SendLogDeliveryStatus.methods - Object.methods - ActiveRecord::Base.methods - [:reject_status_by_operator]).each do |method|
      SendLogDeliveryStatus.send(method)
    end

    FactoryGirl.create(:cell_operator_mts)
    FactoryGirl.create(:cell_operator_beeline)
    FactoryGirl.create(:cell_operator_megafon)
    FactoryGirl.create(:cell_operator_tele2)
    FactoryGirl.create(:cell_operator_baikal)
    FactoryGirl.create(:cell_operator_ural)
    FactoryGirl.create(:cell_operator_enisey)
    FactoryGirl.create(:cell_operator_cdma)
    FactoryGirl.create(:cell_operator_motiv)
    FactoryGirl.create(:cell_operator_nss)
    FactoryGirl.create(:cell_operator_other)
    FactoryGirl.create(:cell_operator_smarts)
    FactoryGirl.create(:cell_operator_unknown)

    FactoryGirl.create(:mts_tariff)
    FactoryGirl.create(:beeline_tariff)
    FactoryGirl.create(:megafon_tariff)
    FactoryGirl.create(:tele2_tariff)
    FactoryGirl.create(:baikal_tariff)
    FactoryGirl.create(:ural_tariff)
    FactoryGirl.create(:enisey_tariff)
    FactoryGirl.create(:cdma_tariff)
    FactoryGirl.create(:motiv_tariff)
    FactoryGirl.create(:nss_tariff)
    FactoryGirl.create(:smarts_tariff)
    FactoryGirl.create(:other_tariff)

    #preparing reference
    Channel.working
    CellOperatorGroup.working
  end

  def clear_expectation_by_url(url)
    expectations = Typhoeus::Expectation.all.clone
    Typhoeus::Expectation.clear

    #восстанавливаем stub-ы не своего типа
    expectations.each do |expectation|
      Typhoeus.stub(expectation.base_url).and_return(expectation.responses.first) if not expectation.base_url == url
    end
  end

  def freeze(time=Time.now)
    Timecop.freeze(time)
    yield
    Timecop.return
  end

  def num_to_array(num, func)
    _array = []
    num.times { _array << func.call }
    _array
  end

  def to_boolean(str)
    str.downcase == 'true'
  end

  def recalculate_logs(user, &block)
    begin_date = Log.where(user_id: user.id).order(:created_at)[0].created_at.to_date
    end_date = Log.where(user_id: user.id).order('created_at DESC')[0].created_at.to_date

    Calculator::Logs.recalculate(begin_date, end_date)

    yield if block_given?
  end

  def check_new_task_created(class_name, method_name, params=nil, &block)
    Sidekiq::Testing.disable! do
      $redis.flushall
      yield
      assert_not_nil $redis.lindex('main:queue:default', 0), "should create new task in sidekiq"
      assert_equal "- !ruby/class '#{class_name}'", YAML.load($redis.lindex('main:queue:default', 0))["args"].first.split("\n")[1]
      assert_equal "- :#{method_name}", YAML.load($redis.lindex('main:queue:default', 0))["args"].first.split("\n")[2]

      if params
        assert_equal params.to_yaml.strip[5..-1][0..77], YAML.load($redis.lindex('main:queue:default', 0))["args"].first.split("\n")[3].strip[5..-1][0..77]
      end
    end
  end

end

DatabaseCleaner.strategy = :truncation
