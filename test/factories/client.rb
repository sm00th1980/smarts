# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :client do
    fio { Faker::Name.name }
    birthday {  Date.today - 1.day }
    phone { Test::Helpers.random_normal_phone_number }

    factory :client_with_today_birthday do
      birthday { Date.today - 18.year }
      birthday_congratulation true
    end
  end

end
