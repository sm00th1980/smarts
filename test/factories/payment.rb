# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :payment do
    date { Time.now - 1.day }
    created_at { Time.now - 1.day }
    amount 50.0

    factory :payment_new_manual do
      service { PaymentService.manual }
    end

    factory :payment_new_robokassa do
      service { PaymentService.robokassa }
    end

    factory :payment_approved_manual do
      approved_at { Time.now - 1.hour }
      service { PaymentService.manual }
    end

    factory :payment_failed_manual do
      failed_at { Time.now - 1.hour }
      service { PaymentService.manual }
    end

    factory :payment_approved_bank do
      approved_at { Time.now - 1.hour }
      service { PaymentService.bank }
    end

    factory :payment_failed_bank do
      failed_at { Time.now - 1.hour }
      service { PaymentService.bank }
    end

    factory :payment_approved_robokassa do
      approved_at { Time.now - 1.hour }
      service { PaymentService.robokassa }
    end

    factory :payment_failed_robokassa do
      failed_at { Time.now - 1.hour }
      service { PaymentService.robokassa }
    end

    factory :payment_approved_and_failed do
      approved_at { Time.now - 1.hour }
      failed_at { Time.now - 1.hour }
      service { PaymentService.robokassa }
    end
  end
end
