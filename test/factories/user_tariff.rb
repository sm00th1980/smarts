# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :user_tariff do

    factory :basic_user_tariff do
      association :tariff, factory: :basic_tariff
      begin_date { (Time.now - 1.year).to_date }
      end_date nil
    end

    factory :middle_user_tariff do
      association :tariff, factory: :middle_tariff
      begin_date { (Time.now - 1.year).to_date }
      end_date nil
    end

    factory :big_user_tariff do
      association :tariff, factory: :big_tariff
      begin_date { (Time.now - 1.year).to_date }
      end_date nil
    end

    factory :user_tariff_with_opened_end_date do
      association :tariff, factory: :middle_tariff
      begin_date { Date.today + 1.year + 1.day }
      end_date nil
    end

    factory :user_tariff_with_closed_end_date do
      association :tariff, factory: :middle_tariff
      begin_date { Date.today - 1.year }
      end_date   { Date.today + 1.year }
    end
  end
end
