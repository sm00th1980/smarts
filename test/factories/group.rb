# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :group do
    sequence(:name) { |n| "group_name_#{n}" }
    transient do
      clients_count 4
    end

    after(:create) do |group, evaluator|
      FactoryGirl.create_list(:client, evaluator.clients_count, group: group)
    end


    factory :group_with_phones do
      transient do
        clients_count 0
        phones ['79115649932']
      end

      after(:create) do |group, evaluator|
        evaluator.phones.each { |phone|
          FactoryGirl.create(:client, phone:phone, group:group)
        }
      end
    end
  end

end
