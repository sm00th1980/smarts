# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :cell_operator do
    sequence(:name) { |n| "name_#{n}" }

    factory :cell_operator_mts do
      group { FactoryGirl.create(:cell_operator_group_mts) }
      after(:create) do |cell_operator|
        FactoryGirl.create(:cell_operator_phone_range, cell_operator: cell_operator, begin_phone: 79198015896, end_phone: 79198015896)
      end
    end

    factory :cell_operator_beeline do
      group { FactoryGirl.create(:cell_operator_group_beeline) }
      after(:create) do |cell_operator|
        FactoryGirl.create(:cell_operator_phone_range, cell_operator: cell_operator, begin_phone: 79657250000, end_phone: 79657250000)
      end
    end

    factory :cell_operator_megafon do
      group { FactoryGirl.create(:cell_operator_group_megafon) }
      after(:create) do |cell_operator|
        FactoryGirl.create(:cell_operator_phone_range, cell_operator: cell_operator, begin_phone: 79379903835, end_phone: 79379903835)
      end
    end

    factory :cell_operator_tele2 do
      group { FactoryGirl.create(:cell_operator_group_tele2) }
      after(:create) do |cell_operator|
        FactoryGirl.create(:cell_operator_phone_range, cell_operator: cell_operator, begin_phone: 79005910000, end_phone: 79005910000)
      end
    end

    factory :cell_operator_baikal do
      group { FactoryGirl.create(:cell_operator_group_baikal) }
      after(:create) do |cell_operator|
        FactoryGirl.create(:cell_operator_phone_range, cell_operator: cell_operator, begin_phone: 79016300000, end_phone: 79016300000)
      end
    end

    factory :cell_operator_ural do
      group { FactoryGirl.create(:cell_operator_group_ural) }
      after(:create) do |cell_operator|
        FactoryGirl.create(:cell_operator_phone_range, cell_operator: cell_operator, begin_phone: 79012200000, end_phone: 79012200000)
      end
    end

    factory :cell_operator_enisey do
      group { FactoryGirl.create(:cell_operator_group_enisey) }
      after(:create) do |cell_operator|
        FactoryGirl.create(:cell_operator_phone_range, cell_operator: cell_operator, begin_phone: 79011110000, end_phone: 79011110000)
      end
    end

    factory :cell_operator_motiv do
      group { FactoryGirl.create(:cell_operator_group_motiv) }
      after(:create) do |cell_operator|
        FactoryGirl.create(:cell_operator_phone_range, cell_operator: cell_operator, begin_phone: 79000300000, end_phone: 79000499999)
      end
    end

    factory :cell_operator_nss do
      group { FactoryGirl.create(:cell_operator_group_nss) }
      after(:create) do |cell_operator|
        FactoryGirl.create(:cell_operator_phone_range, cell_operator: cell_operator, begin_phone: 79003100000, end_phone: 79003149999)
      end
    end

    factory :cell_operator_cdma do
      group { FactoryGirl.create(:cell_operator_group_cdma) }
      after(:create) do |cell_operator|
        FactoryGirl.create(:cell_operator_phone_range, cell_operator: cell_operator, begin_phone: 79011570000, end_phone: 79011570000)
      end
    end

    factory :cell_operator_other do
      group { FactoryGirl.create(:cell_operator_group_other) }
      after(:create) do |cell_operator|
        FactoryGirl.create(:cell_operator_phone_range, cell_operator: cell_operator, begin_phone: 79585360000, end_phone: 79585360000)
      end
    end

    factory :cell_operator_smarts do
      group { FactoryGirl.create(:cell_operator_group_smarts) }
      after(:create) do |cell_operator|
        FactoryGirl.create(:cell_operator_phone_range, cell_operator: cell_operator, begin_phone: 79022900000, end_phone: 79022999999)
      end
    end

    factory :cell_operator_unknown do
      group { FactoryGirl.create(:cell_operator_group_unknown) }
      after(:create) do |cell_operator|
        FactoryGirl.create(:cell_operator_phone_range, cell_operator: cell_operator, begin_phone: 1, end_phone: 99)
      end
    end

  end
end
