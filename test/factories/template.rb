# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :template do
    sequence(:name) { |n| "name_#{n}" }
    sequence(:text) { |n| "text_#{n}" }
    user

    factory :template_without_text do
      text ''
    end
  end
end
