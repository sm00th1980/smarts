# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :user do
    sequence(:email) { |n| "email_#{n}@youct.ru" }
    password '12345678'
    password_confirmation '12345678'
    birthday_message 'happy birthday'
    birthday_hour { Time.now.hour }
    charge_type { ChargeType.by_sent }
    payment_type { PaymentType.prepaid }
    permit_night_delivery false
    discount 0
    alpha_megafon_channel { Channel.gold }

    type { UserType.business }
    status { UserStatus.active }

    factory :admin do
      type { UserType.admin }
    end

    factory :demo do
      type { UserType.demo }
    end

    factory :user_operator do
      type { UserType.operator }
    end

    transient do
      groups_count 1
    end

    factory :user_with_permit_alpha_and_permit_def do
      permit_alpha_sender_name true
      permit_def_sender_name true
    end

    factory :user_with_permit_alpha_and_reject_def do
      permit_alpha_sender_name true
      permit_def_sender_name false
    end

    factory :user_with_reject_alpha_and_permit_def do
      permit_alpha_sender_name false
      permit_def_sender_name true
    end

    factory :user_with_reject_alpha_and_reject_def do
      permit_alpha_sender_name false
      permit_def_sender_name false
    end

    factory :user_with_reject_to_megafon do
      reject_to_megafon true
    end

    factory :user_with_reject_to_mts do
      reject_to_mts true
    end

    factory :user_with_reject_to_smarts_via_gold_channel do
      reject_to_smarts_via_gold_channel true
    end

    after(:create) do |user, evaluator|
      FactoryGirl.create_list(:group, evaluator.groups_count, user: user)
      FactoryGirl.create(:basic_user_tariff, user: user)

      FactoryGirl.create(:log_completed, user: user)
      FactoryGirl.create(:log_fresh, user: user)
      FactoryGirl.create(:log_processing, user: user)

      FactoryGirl.create(:payment_approved_manual, user: user)
      FactoryGirl.create(:template, user: user)

      Balance.fill(Date.today, Date.today)
    end

    factory :user_without_sender_names do
      after(:create) do |user|
        SenderName.where(user_id: user.id).delete_all
      end
    end

    factory :user_without_clients do
      after(:create) do |user|
        Client.where(:group_id => user.groups.map { |g| g.id }).destroy_all
        user.reload
      end
    end

    factory :user_with_basic_tariff do
      after(:create) do |user|
        UserTariff.where(:user_id => user).destroy_all
        FactoryGirl.create(:basic_user_tariff, user: user)
      end
    end

    factory :user_with_middle_tariff do
      after(:create) do |user|
        UserTariff.where(:user_id => user).destroy_all
        FactoryGirl.create(:middle_user_tariff, user: user)
      end
    end

    factory :user_with_big_tariff do
      after(:create) do |user|
        UserTariff.where(:user_id => user).destroy_all
        FactoryGirl.create(:big_user_tariff, user: user)
      end
    end

    factory :user_without_tariff do
      after(:create) do |user|
        UserTariff.where(:user_id => user).destroy_all
      end
    end

    factory :user_without_birthday_message do
      birthday_message nil
    end

    factory :user_with_birthday_message_with_percent_in_template do
      birthday_message 'Дорогая %{fio}, поздравляем Вас с Днем рождения! Удачи Вам во всех делах и настоящего женского счастья! Ваша скидка - 20% ещё 2 недели.'
    end

    factory :user_active do
      status { UserStatus.active }
      blocked_at nil
      deleted_at nil
    end

    factory :user_blocked do
      status { UserStatus.blocked }
      blocked_at { Time.now - 1.day }
      deleted_at nil
    end

    factory :user_deleted do
      status { UserStatus.deleted }
      blocked_at nil
      deleted_at { Time.now - 1.day }
    end

    factory :user_with_negative_balance do
      sequence(:email) { |n| "negative_balance_#{n}@youct.ru" }
      after(:create) do |user|
        sender_name_value = FactoryGirl.create(:sender_name_accepted_with_verified_value, user: user).value
        FactoryGirl.create(:log_with_negative_balance, user: user, sender_name_value: sender_name_value)

        Balance.fill(Date.today, Date.today)
      end
    end

    factory :user_charge_by_sent do
      charge_type { ChargeType.by_sent }
    end

    factory :user_charge_by_delivered do
      charge_type { ChargeType.by_delivered }
    end

    factory :user_postpaid_with_negative_balance do
      payment_type { PaymentType.postpaid }
      after(:create) do |user|
        sender_name_value = FactoryGirl.create(:sender_name_accepted_with_verified_value, user: user).value
        FactoryGirl.create(:log_with_negative_balance, user: user, sender_name_value: sender_name_value)

        Balance.fill(Date.today, Date.today)
      end
    end

    factory :user_prepaid do
      payment_type { PaymentType.prepaid }
    end

    factory :user_postpaid do
      payment_type { PaymentType.postpaid }
    end

    factory :user_with_black_lists do
      after(:create) do |user|
        FactoryGirl.create(:black_list, user: user)
      end
    end

    factory :user_prepaid_with_positive_balance do
      email 'posotive_balance@youct.ru'
      after(:create) do |user|
        FactoryGirl.create(:payment_approved_manual, user: user)

        Balance.fill(Date.today, Date.today)
      end
    end

    factory :user_with_discount do
      discount 10
    end

    factory :user_without_discount do
      discount 0
    end

    factory :user_with_discount_more_100_percent do
      discount 150
    end

    factory :user_with_negative_discount do
      discount -10
    end
  end
end
