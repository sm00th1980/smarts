# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :auto_delivery_period do

    factory :auto_delivery_period_every_hour do
      period 'every_hour'
    end

    factory :auto_delivery_period_every_day do
      period 'every_day'
    end

    factory :auto_delivery_period_every_week do
      period 'every_week'
    end
  end
end
