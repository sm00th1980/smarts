# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :cell_operator_phone_range do
    sequence(:region) { |n| "name_#{n}" }
  end
end
