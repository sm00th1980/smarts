# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :auto_delivery do
    name { Faker::Name.name }
    active true
    start_date { Date.today - 10.days }
    stop_date  { Date.today + 10.days }
    content    { Faker::Name.name }
    period_id  { FactoryGirl.create(:auto_delivery_period_every_day).id }
    hour 10
    minute 30
    user_id    { FactoryGirl.create(:user).id }

    factory :auto_delivery_every_hour_run_now do
      after(:create) do |delivery|
        delivery.update_attribute(:period_id, FactoryGirl.create(:auto_delivery_period_every_hour).id)

        delivery.update_attribute(:week_day_id, nil)
        delivery.update_attribute(:hour, nil)
        delivery.update_attribute(:minute, Time.now.min)
      end
    end

    factory :auto_delivery_every_day_run_now do
      after(:create) do |delivery|
        delivery.update_attribute(:period_id, FactoryGirl.create(:auto_delivery_period_every_day).id)

        delivery.update_attribute(:week_day_id, nil)
        delivery.update_attribute(:hour, Time.now.hour)
        delivery.update_attribute(:minute, Time.now.min)
      end
    end

    factory :auto_delivery_every_week_run_now do
      after(:create) do |delivery|
        delivery.update_attribute(:period_id, FactoryGirl.create(:auto_delivery_period_every_week).id)

        delivery.update_attribute(:week_day_id, WeekDay.days.map{|wday| [Date.today.send("#{wday.eng_name}?"), wday] }.select{|el| el.first}.first.last.id)
        delivery.update_attribute(:hour, Time.now.hour)
        delivery.update_attribute(:minute, Time.now.min)
      end
    end

    factory :auto_delivery_active do
      after(:create) { |delivery| delivery.update_attribute(:active, true) }
    end

    factory :auto_delivery_not_active do
      after(:create) { |delivery| delivery.update_attribute(:active, false) }
    end

    factory :auto_delivery_without_start_date do
      after(:create) do |delivery|
        delivery.update_attribute(:active, false)
        delivery.update_attribute(:start_date, nil)
      end
    end

    factory :auto_delivery_without_name do
      after(:create) do |delivery|
        delivery.update_attribute(:active, false)
        delivery.update_attribute(:name, nil)
      end
    end

    factory :auto_delivery_without_receivers do
      after(:create) do |delivery|
        delivery.update_attribute(:active, false)
        delivery.update_attribute(:client_ids, [])
        delivery.update_attribute(:group_ids, [])
      end
    end

    factory :auto_delivery_without_receivers_with_nil do
      after(:create) do |delivery|
        delivery.update_attribute(:active, false)
        delivery.update_attribute(:client_ids, nil)
        delivery.update_attribute(:group_ids, nil)
      end
    end

    factory :auto_delivery_without_content do
      after(:create) do |delivery|
        delivery.update_attribute(:active, false)
        delivery.update_attribute(:content, nil)
        delivery.update_attribute(:template_id, nil)
      end
    end

    factory :auto_delivery_with_invalid_sender_name do
      after(:create) do |delivery|
        delivery.update_attribute(:sender_name_value, nil)
      end
    end

    factory :auto_delivery_with_overdue do
      stop_date { Date.today - 1.day }
    end

    factory :auto_delivery_with_invalid_sender_name_value do
      after(:create) do |delivery|
        delivery.update_attribute(:active, false)
        delivery.update_attribute(:sender_name_value, nil)
      end
    end

    factory :auto_delivery_without_overdue do
      stop_date { Date.today + 1.day }
    end

    factory :auto_delivery_next_day do
      start_date { Date.today + 1.day }
      stop_date { Date.today + 1.day }
    end

    factory :auto_delivery_start_day_before_stop_next_day do
      start_date { Date.today - 1.day }
      stop_date { Date.today + 1.day }
    end

    after(:create) do |delivery|
      delivery.client_ids = delivery.user.clients.map{|client| client.id}
      delivery.group_ids  = delivery.user.groups.map{|group| group.id}
      delivery.sender_name_value = FactoryGirl.create(:sender_name_accepted_with_verified_value, user: delivery.user).value

      delivery.save!
    end
  end
end
