# -*- encoding : utf-8 -*-
require File.dirname(__FILE__) + '/../test_helper'

FactoryGirl.define do
  factory :sender_name do

    factory :sender_name_accepted_with_verified_value do
      status { SenderNameStatus.accepted }
      value  { Test::Helpers.random_normal_phone_number.to_s }
    end

    factory :sender_name_rejected_with_verified_value do
      status { SenderNameStatus.rejected }
      value  { Test::Helpers.random_normal_phone_number.to_s }
    end

    factory :sender_name_moderating_with_verified_value do
      status { SenderNameStatus.moderating }
      value  { Test::Helpers.random_normal_phone_number.to_s }
    end

    factory :sender_name_accepted_with_not_verified_value do
      status { SenderNameStatus.accepted }
      value { Faker::Company.name }
    end

    factory :sender_name_rejected_with_not_verified_value do
      status { SenderNameStatus.rejected }
      value { Faker::Company.name }
    end

    factory :sender_name_moderating_with_not_verified_value do
      status { SenderNameStatus.moderating }
      value { Faker::Company.name }
    end

    factory :sender_name_accepted do
      status { SenderNameStatus.accepted }
      value  { Test::Helpers.random_normal_phone_number.to_s }
    end

    factory :sender_name_rejected do
      status { SenderNameStatus.rejected }
      value  { Test::Helpers.random_normal_phone_number.to_s }
    end

    factory :sender_name_moderating do
      status { SenderNameStatus.moderating }
      value  { Test::Helpers.random_normal_phone_number.to_s }
    end

    factory :sender_name_on_moderating do
      status { SenderNameStatus.moderating }
      value  { Test::Helpers.random_normal_phone_number.to_s }
    end

  end
end
