# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :send_log do
    created_at Time.now - 1.day
    cell_operator_group { CellOperatorGroup.smarts || FactoryGirl.create(:cell_operator_group_smarts) }
  end
end
