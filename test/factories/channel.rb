# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :channel do

    factory :channel_gold do
      internal_name 'gold'
    end

    factory :channel_bronze do
      internal_name 'bronze'
    end

    factory :channel_unknown do
      internal_name 'unknown'
    end

    factory :channel_megafon_fixed do
      internal_name 'megafon_fixed'
    end

    factory :channel_megafon_multi do
      internal_name 'megafon_multi'
    end

    factory :channel_any do
      internal_name 'any'
    end

  end
end
