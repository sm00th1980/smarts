# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :cell_operator_group do
    factory :cell_operator_group_mts do
      name :mts
      internal_name :mts
    end

    factory :cell_operator_group_beeline do
      name :beeline
      internal_name :beeline
    end

    factory :cell_operator_group_megafon do
      name :megafon
      internal_name :megafon
    end

    factory :cell_operator_group_tele2 do
      name :tele2
      internal_name :tele2
    end

    factory :cell_operator_group_baikal do
      name :baikal
      internal_name :baikal
    end

    factory :cell_operator_group_ural do
      name :ural
      internal_name :ural
    end

    factory :cell_operator_group_enisey do
      name :enisey
      internal_name :enisey
    end

    factory :cell_operator_group_cdma do
      name :cdma
      internal_name :cdma
    end

    factory :cell_operator_group_motiv do
      name :motiv
      internal_name :motiv
    end

    factory :cell_operator_group_nss do
      name :nss
      internal_name :nss
    end

    factory :cell_operator_group_other do
      name :other
      internal_name :other
    end

    factory :cell_operator_group_smarts do
      name :smarts
      internal_name :smarts
    end

    factory :cell_operator_group_unknown do
      name :unknown
      internal_name :unknown
    end
  end
end
