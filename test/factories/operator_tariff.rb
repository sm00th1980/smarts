# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :operator_tariff do

    factory :mts_tariff do
      operator { CellOperatorGroup.mts }
      price_per_sms 0.31
      begin_date { Date.today }
    end

    factory :beeline_tariff do
      operator { CellOperatorGroup.beeline }
      price_per_sms 0.47
      begin_date { Date.today }
    end

    factory :megafon_tariff do
      operator { CellOperatorGroup.megafon }
      price_per_sms 1.04
      begin_date { Date.today }
    end

    factory :tele2_tariff do
      operator { CellOperatorGroup.tele2 }
      price_per_sms 0.01
      begin_date { Date.today }
    end

    factory :baikal_tariff do
      operator { CellOperatorGroup.baikal }
      price_per_sms 0.02
      begin_date { Date.today }
    end

    factory :ural_tariff do
      operator { CellOperatorGroup.ural }
      price_per_sms 0.03
      begin_date { Date.today }
    end

    factory :enisey_tariff do
      operator { CellOperatorGroup.enisey }
      price_per_sms 0.04
      begin_date { Date.today }
    end

    factory :cdma_tariff do
      operator { CellOperatorGroup.cdma }
      price_per_sms 0.05
      begin_date { Date.today }
    end

    factory :motiv_tariff do
      operator { CellOperatorGroup.motiv }
      price_per_sms 0.06
      begin_date { Date.today }
    end

    factory :nss_tariff do
      operator { CellOperatorGroup.nss }
      price_per_sms 0.07
      begin_date { Date.today }
    end

    factory :smarts_tariff do
      operator { CellOperatorGroup.smarts }
      price_per_sms 0.08
      begin_date { Date.today }
    end

    factory :other_tariff do
      operator { CellOperatorGroup.other }
      price_per_sms 0.09
      begin_date { Date.today }
    end


  end
end
