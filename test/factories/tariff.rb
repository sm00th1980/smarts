# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :tariff do

    factory :basic_tariff do
      name 'basic_tariff'
      price_per_sms 0.18
    end

    factory :middle_tariff do
      name 'middle_tariff'
      price_per_sms 0.16
    end

    factory :big_tariff do
      name 'big_tariff'
      price_per_sms 0.10
    end

  end
end
