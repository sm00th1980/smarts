# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :charge_type do

    factory :charge_type_by_sent do
      internal_name 'by_sent'
    end

    factory :charge_type_by_delivered do
      internal_name 'by_delivered'
    end

  end
end
