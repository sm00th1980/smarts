# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :payment_type do

    factory :payment_type_prepaid do
      internal_name 'prepaid'
    end

    factory :payment_type_postpaid do
      internal_name 'postpaid'
    end

  end
end
