# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :log do
    created_at Time.now - 1.day
    type { LogType.manual }

    after(:create) do |log|
      log.sms_count = Message.sms_count(log.content)
      log.save!
    end

    factory :log_fresh do
      status { LogStatus.fresh }
      content '"Центр школьной формы" рад сообщить о новогодних поступлениях брюк на флисе, сорочек с длинным рукавом для мальчиков (размеры с 26 по 37), а также о распродаже праздничных платьев для девочек. Мы ждем Вас в ТЦ "Каскад", ул. Партизанская, 56-а, секция 208, тел.:373-45-96.'
    end

    factory :log_fresh_with_long_content do
      status { LogStatus.fresh }
      content '"Центр школьной формы" рад сообщить о новогодних поступлениях брюк на флисе, сорочек с длинным рукавом для мальчиков (размеры с 26 по 37), а также о распродаже праздничных платьев для девочек. Мы ждем Вас в ТЦ "Каскад", ул. Партизанская, 56-а, секция 208, тел.:373-45-96.'
    end

    factory :log_completed do
      status { LogStatus.completed }

      factory :log_with_specific_send_log do
        transient do
          #phone:delivery_status
          send_log_conf({
                            '79115637183' => SendLogDeliveryStatus.delivered
                        })
        end
        after(:build) do |log, evaluator|
          evaluator.send_log_conf.each{ |phone, delivery_status|
            log.send_logs << FactoryGirl.build(:send_log,
                                               price:1,
                                               phone: phone,
                                               delivery_status: delivery_status,
                                               delivery_error: SendLogDeliveryError.bronze_no_error,
                                               channel: Channel.bronze
            )
          }

        end
      end
    end

    factory :log_processing do
      status { LogStatus.processing }
    end

    factory :log_with_negative_balance do
      created_at Time.now
      content '"Центр школьной формы"'
      status { LogStatus.completed }
      phones { (0..5).map { Test::Helpers.random_normal_phone_number } }
      after(:create) do |log|
        FactoryGirl.create(:send_log,
                           log: log,
                           phone: log.phones.first,
                           price: 100,
                           channel: Channel.bronze,
                           delivery_status: SendLogDeliveryStatus.delivered,
                           :delivery_error => SendLogDeliveryError.bronze_no_error)
      end
    end

    factory :just_send_log_via_bronze_channel do
      status { LogStatus.completed }
      phones { [Test::Helpers.random_normal_phone_number] }
      content 'test'
      after(:create) do |log|
        price = log.user.charge_by_delivered? ? 0 : log.user.price_per_sms
        FactoryGirl.create(:send_log, log: log, price: price, channel: Channel.bronze, phone: log.phones.first, delivery_status: SendLogDeliveryStatus.unknown, :delivery_error => SendLogDeliveryError.bronze_status_unknown)
      end
    end

    factory :just_send_log_via_gold_channel do
      status { LogStatus.completed }
      phones { [Test::Helpers.random_normal_phone_number] }
      content 'test'
      after(:create) do |log|
        channel = Channel.gold
        price = Message.price_per_sms(channel, log.user, log.phones.first)
        FactoryGirl.create(:send_log, log: log, price: price, channel: channel, phone: log.phones.first, delivery_status: SendLogDeliveryStatus.unknown, :delivery_error => SendLogDeliveryError.gold_status_unknown)
      end
    end

    factory :just_send_log_via_megafon_fixed_channel do
      status { LogStatus.completed }
      phones { [79379903835] }
      content 'test'
      after(:create) do |log|
        channel = Channel.megafon_fixed
        price = Message.price_per_sms(channel, log.user, log.phones.first)
        FactoryGirl.create(:send_log, log: log, price: price, channel: channel, phone: log.phones.first, delivery_status: SendLogDeliveryStatus.unknown, :delivery_error => SendLogDeliveryError.gold_status_unknown)
      end
    end

    factory :just_send_log_via_megafon_multi_channel do
      status { LogStatus.completed }
      phones { [79379903835] }
      content 'test'
      after(:create) do |log|
        channel = Channel.megafon_multi
        price = Message.price_per_sms(channel, log.user, log.phones.first)
        FactoryGirl.create(:send_log, log: log, price: price, channel: channel, phone: log.phones.first, delivery_status: SendLogDeliveryStatus.unknown, :delivery_error => SendLogDeliveryError.gold_status_unknown)
      end
    end

    factory :log_without_chunks do
      status { LogStatus.completed }
      phones { [Test::Helpers.random_normal_phone_number] }
      content 'message'

      after(:create) do |log|
        log.send_logs.each { |send_log| send_log.destroy }
        FactoryGirl.create(:send_log, log: log, price: 0.25, channel: Channel.gold, phone: log.phones.first, delivery_status: SendLogDeliveryStatus.delivered, :delivery_error => SendLogDeliveryError.gold_no_error)
      end
    end

    factory :log_with_chunks do
      status { LogStatus.completed }
      phones { [Test::Helpers.random_normal_phone_number] }
      content '"Центр школьной формы" рад сообщить о новогодних поступлениях брюк на флисе, сорочек с длинным рукавом для мальчиков (размеры с 26 по 37), а также о распродаже праздничных платьев для девочек. Мы ждем Вас в ТЦ "Каскад", ул. Партизанская, 56-а, секция 208, тел.:373-45-96.'
      after(:create) do |log|
        FactoryGirl.create(:send_log, log: log, channel: Channel.bronze, price: 0.25*log.sms_count, phone: log.phones.first, delivery_status: SendLogDeliveryStatus.delivered, :delivery_error => SendLogDeliveryError.bronze_no_error)
        FactoryGirl.create(:send_log, log: log, channel: Channel.bronze, price: 0.25*log.sms_count, phone: log.phones.first, delivery_status: SendLogDeliveryStatus.rejected, :delivery_error => SendLogDeliveryError.bronze_error_call_barred)
      end
    end

    factory :log_fresh_valid do
      content 'message'
      phones { (0..4).map { Test::Helpers.random_normal_phone_number } }
      status { LogStatus.fresh }
    end

    factory :log_with_return_in_content do
      status { LogStatus.completed }
      phones { [Test::Helpers.random_normal_phone_number] }
      content "Вакансия предоставлена Гипермаркетом Вакансий:\n88342245892 Водитель, ЗП достойная"
      after(:create) do |log|
        FactoryGirl.create(:send_log, log: log, channel: Channel.bronze, price: 0.25, phone: log.phones.first, delivery_status: SendLogDeliveryStatus.delivered, :delivery_error => SendLogDeliveryError.bronze_no_error)
      end
    end

    factory :log_with_windows_return_in_content do
      status { LogStatus.completed }
      phones { [Test::Helpers.random_normal_phone_number] }
      content "Вакансия предоставлена Гипермаркетом Вакансий:\r\n88342245892 Водитель, ЗП достойная"
      after(:create) do |log|
        FactoryGirl.create(:send_log, log: log, price: 0.25, channel: Channel.bronze, phone: log.phones.first, delivery_status: SendLogDeliveryStatus.delivered, :delivery_error => SendLogDeliveryError.bronze_no_error)
      end
    end

    factory :log_with_separator_in_content do
      status { LogStatus.completed }
      phones { [Test::Helpers.random_normal_phone_number] }
      content { "content#{Report::Generator::COLUMN_SEPARATOR}content" }
      after(:create) do |log|
        FactoryGirl.create(:send_log, log: log, price: 0.25, channel: Channel.bronze, phone: log.phones.first, delivery_status: SendLogDeliveryStatus.delivered, :delivery_error => SendLogDeliveryError.bronze_no_error)
      end
    end

    factory :log_with_not_delivered do
      status { LogStatus.completed }
      phones { [Test::Helpers.random_normal_phone_number, Test::Helpers.random_normal_phone_number] }
      group_ids { [] }
      content 'content'
      after(:create) do |log|
        FactoryGirl.create(:send_log, log: log, price: 0.50, channel: Channel.bronze, phone: log.phones.first, delivery_status: SendLogDeliveryStatus.delivered, :delivery_error => SendLogDeliveryError.bronze_no_error)
        FactoryGirl.create(:send_log, log: log, price: 0.05, channel: Channel.bronze, phone: log.phones.last, delivery_status: SendLogDeliveryStatus.rejected, :delivery_error => SendLogDeliveryError.bronze_error_call_barred)
      end
    end

    factory :log_with_all_delivered do
      status { LogStatus.completed }
      phones { [Test::Helpers.random_normal_phone_number, Test::Helpers.random_normal_phone_number] }
      group_ids { [] }
      content 'content'
      after(:create) do |log|
        FactoryGirl.create(:send_log, log: log, price: 0.50, channel: Channel.bronze, phone: log.phones.first, delivery_status: SendLogDeliveryStatus.delivered, :delivery_error => SendLogDeliveryError.bronze_no_error)
        FactoryGirl.create(:send_log, log: log, price: 0.50, channel: Channel.bronze, phone: log.phones.last, delivery_status: SendLogDeliveryStatus.delivered, :delivery_error => SendLogDeliveryError.bronze_no_error)
      end
    end

    factory :base_log_with_all_not_delivered do
      status { LogStatus.completed }
      phones { [Test::Helpers.random_normal_phone_number, Test::Helpers.random_normal_phone_number, Test::Helpers.random_normal_phone_number] }
      group_ids { [] }
      content 'content'
      after(:create) do |log|
        FactoryGirl.create(:send_log, log: log, price: 0.05, channel: Channel.bronze, phone: log.phones[0], delivery_status: SendLogDeliveryStatus.rejected, :delivery_error => SendLogDeliveryError.bronze_error_call_barred)
        FactoryGirl.create(:send_log, log: log, price: 0.05, channel: Channel.bronze, phone: log.phones[1], delivery_status: SendLogDeliveryStatus.rejected, :delivery_error => SendLogDeliveryError.bronze_error_call_barred)
        FactoryGirl.create(:send_log, log: log, price: 0.05, channel: Channel.bronze, phone: log.phones[2], delivery_status: SendLogDeliveryStatus.rejected, :delivery_error => SendLogDeliveryError.bronze_error_call_barred)
      end
    end

    factory :base_log_with_first_delivered do
      status { LogStatus.completed }
      phones { [Test::Helpers.random_normal_phone_number, Test::Helpers.random_normal_phone_number, Test::Helpers.random_normal_phone_number, Test::Helpers.random_normal_phone_number] }
      group_ids { [] }
      content 'content'
      after(:create) do |log|
        FactoryGirl.create(:send_log, log: log, price: 0.50, channel: Channel.bronze, phone: log.phones[0], delivery_status: SendLogDeliveryStatus.delivered, :delivery_error => SendLogDeliveryError.bronze_no_error)
        FactoryGirl.create(:send_log, log: log, price: 0.05, channel: Channel.bronze, phone: log.phones[1], delivery_status: SendLogDeliveryStatus.rejected, :delivery_error => SendLogDeliveryError.bronze_error_call_barred)
        FactoryGirl.create(:send_log, log: log, price: 0.05, channel: Channel.bronze, phone: log.phones[2], delivery_status: SendLogDeliveryStatus.rejected, :delivery_error => SendLogDeliveryError.bronze_error_call_barred)
        FactoryGirl.create(:send_log, log: log, price: 0.05, channel: Channel.bronze, phone: log.phones[3], delivery_status: SendLogDeliveryStatus.rejected, :delivery_error => SendLogDeliveryError.bronze_error_call_barred)
      end
    end

    factory :log_without_phones do
      status { LogStatus.fresh }
      phones []
      group_ids []
      content 'message'

      after(:create) do |log|
        log.send_logs.each { |send_log| send_log.destroy }
      end
    end

    factory :log_with_several_sms do
      status { LogStatus.completed }
      phones { [Test::Helpers.random_normal_phone_number] }
      content '"Центр школьной формы" рад сообщить о новогодних поступлениях брюк на флисе, сорочек с длинным рукавом для мальчиков (размеры с 26 по 37), а также о распродаже праздничных платьев для девочек. Мы ждем Вас в ТЦ "Каскад", ул. Партизанская, 56-а, секция 208, тел.:373-45-96.'
      after(:create) do |log|
        FactoryGirl.create(:send_log, log: log, price: 0.25, channel: Channel.bronze, phone: log.phones.first, delivery_status: SendLogDeliveryStatus.delivered, :delivery_error => SendLogDeliveryError.bronze_no_error)
        FactoryGirl.create(:send_log, log: log, price: 0.25, channel: Channel.bronze, phone: log.phones.first, delivery_status: SendLogDeliveryStatus.rejected, :delivery_error => SendLogDeliveryError.bronze_error_call_barred)
      end
    end

    factory :log_with_sent_to_mts_via_gold do
      status { LogStatus.completed }
      phones { [Test::Helpers.random_normal_phone_number] }
      content 'content'
      after(:create) do |log|
        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0.5,
                           phone: log.phones.first,
                           channel: Channel.gold,
                           delivery_status: SendLogDeliveryStatus.delivered,
                           delivery_error: SendLogDeliveryError.gold_no_error,
                           cell_operator_group: CellOperatorGroup.mts

        )

        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0.15,
                           phone: log.phones.first,
                           channel: Channel.gold,
                           delivery_status: SendLogDeliveryStatus.failured,
                           delivery_error: SendLogDeliveryError.gold_expired,
                           cell_operator_group: CellOperatorGroup.mts

        )
      end
    end

    factory :log_with_other_date_send_logs do
      status { LogStatus.completed }
      phones { [Test::Helpers.random_normal_phone_number] }
      content 'content'
      created_at { Time.now }
      after(:create) do |log|
        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0.25,
                           channel: Channel.gold,
                           phone: log.phones.first,
                           delivery_status: SendLogDeliveryStatus.delivered,
                           delivery_error: SendLogDeliveryError.gold_no_error,
                           cell_operator_group: CellOperatorGroup.mts,
                           created_at: Time.now + 1.day

        )

        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0.25,
                           channel: Channel.gold,
                           phone: log.phones.first,
                           delivery_status: SendLogDeliveryStatus.failured,
                           delivery_error: SendLogDeliveryError.gold_expired,
                           cell_operator_group: CellOperatorGroup.mts,
                           created_at: Time.now + 1.day

        )
      end
    end

    factory :log_with_non_zero_price_and_zero_price do
      status { LogStatus.completed }
      phones { [Test::Helpers.random_normal_phone_number] }
      content 'content'
      after(:create) do |log|
        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0.25,
                           channel: Channel.gold,
                           phone: log.phones.first,
                           delivery_status: SendLogDeliveryStatus.delivered,
                           delivery_error: SendLogDeliveryError.gold_no_error,
                           cell_operator_group: CellOperatorGroup.mts
        )

        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0,
                           channel: Channel.gold,
                           phone: log.phones.first,
                           delivery_status: SendLogDeliveryStatus.delivered,
                           delivery_error: SendLogDeliveryError.gold_no_error,
                           cell_operator_group: CellOperatorGroup.mts
        )

        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0.25,
                           channel: Channel.gold,
                           phone: log.phones.first,
                           delivery_status: SendLogDeliveryStatus.failured,
                           delivery_error: SendLogDeliveryError.gold_expired,
                           cell_operator_group: CellOperatorGroup.mts,
        )

        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0,
                           channel: Channel.gold,
                           phone: log.phones.first,
                           delivery_status: SendLogDeliveryStatus.failured,
                           delivery_error: SendLogDeliveryError.gold_expired,
                           cell_operator_group: CellOperatorGroup.mts
        )
      end
    end

    factory :log_with_zero_price_gold do
      status { LogStatus.completed }
      phones { [Test::Helpers.random_normal_phone_number] }
      content 'content'
      after(:create) do |log|
        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0,
                           channel: Channel.gold,
                           phone: log.phones.first,
                           delivery_status: SendLogDeliveryStatus.delivered,
                           delivery_error: SendLogDeliveryError.gold_no_error,
                           cell_operator_group: CellOperatorGroup.mts
        )

        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0,
                           channel: Channel.gold,
                           phone: log.phones.first,
                           delivery_status: SendLogDeliveryStatus.failured,
                           delivery_error: SendLogDeliveryError.gold_expired,
                           cell_operator_group: CellOperatorGroup.mts
        )
      end
    end

    factory :log_with_zero_price_bronze do
      status { LogStatus.completed }
      phones { [Test::Helpers.random_normal_phone_number] }
      content 'content'
      after(:create) do |log|
        FactoryGirl.create(:send_log,
                           log: log,
                           channel: Channel.bronze,
                           price: 0,
                           phone: log.phones.first,
                           delivery_status: SendLogDeliveryStatus.delivered,
                           delivery_error: SendLogDeliveryError.bronze_no_error,
                           cell_operator_group: CellOperatorGroup.smarts
        )

        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0,
                           channel: Channel.bronze,
                           phone: log.phones.first,
                           delivery_status: SendLogDeliveryStatus.failured,
                           delivery_error: SendLogDeliveryError.bronze_expired,
                           cell_operator_group: CellOperatorGroup.smarts
        )
      end
    end

    factory :log_with_sent_to_beeline_via_gold do
      status { LogStatus.completed }
      phones { [Test::Helpers.random_normal_phone_number] }
      content 'content'
      after(:create) do |log|
        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0.24,
                           phone: log.phones.first,
                           channel: Channel.gold,
                           delivery_status: SendLogDeliveryStatus.delivered,
                           delivery_error: SendLogDeliveryError.gold_no_error,
                           cell_operator_group: CellOperatorGroup.beeline

        )

        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0.28,
                           phone: log.phones.first,
                           channel: Channel.gold,
                           delivery_status: SendLogDeliveryStatus.failured,
                           delivery_error: SendLogDeliveryError.gold_expired,
                           cell_operator_group: CellOperatorGroup.beeline

        )
      end
    end

    factory :log_with_sent_to_megafon_via_gold do
      status { LogStatus.completed }
      phones { [Test::Helpers.random_normal_phone_number] }
      content 'content'
      after(:create) do |log|
        FactoryGirl.create(:send_log,
                           log: log,
                           price: 1.25,
                           phone: log.phones.first,
                           channel: Channel.gold,
                           delivery_status: SendLogDeliveryStatus.delivered,
                           delivery_error: SendLogDeliveryError.gold_no_error,
                           cell_operator_group: CellOperatorGroup.megafon

        )

        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0.35,
                           phone: log.phones.first,
                           channel: Channel.gold,
                           delivery_status: SendLogDeliveryStatus.failured,
                           delivery_error: SendLogDeliveryError.gold_expired,
                           cell_operator_group: CellOperatorGroup.megafon

        )
      end
    end

    factory :log_with_sent_over_megafon_via_megafon_fixed_channel do
      status { LogStatus.completed }
      phones { [Test::Helpers.random_normal_phone_number] }
      content 'content'
      after(:create) do |log|
        FactoryGirl.create(:send_log,
                           log: log,
                           price: 1.25,
                           phone: log.phones.first,
                           channel: Channel.megafon_fixed,
                           delivery_status: SendLogDeliveryStatus.delivered,
                           delivery_error: SendLogDeliveryError.gold_no_error,
                           cell_operator_group: CellOperatorGroup.megafon

        )

        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0.35,
                           phone: log.phones.first,
                           channel: Channel.megafon_fixed,
                           delivery_status: SendLogDeliveryStatus.failured,
                           delivery_error: SendLogDeliveryError.gold_expired,
                           cell_operator_group: CellOperatorGroup.megafon

        )
      end
    end

    factory :log_with_sent_over_megafon_via_megafon_multi_channel do
      status { LogStatus.completed }
      phones { [Test::Helpers.random_normal_phone_number] }
      content 'content'
      after(:create) do |log|
        FactoryGirl.create(:send_log,
                           log: log,
                           price: 1.25,
                           phone: log.phones.first,
                           channel: Channel.megafon_multi,
                           delivery_status: SendLogDeliveryStatus.delivered,
                           delivery_error: SendLogDeliveryError.gold_no_error,
                           cell_operator_group: CellOperatorGroup.megafon

        )

        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0.35,
                           phone: log.phones.first,
                           channel: Channel.megafon_multi,
                           delivery_status: SendLogDeliveryStatus.failured,
                           delivery_error: SendLogDeliveryError.gold_expired,
                           cell_operator_group: CellOperatorGroup.megafon

        )
      end
    end

    factory :log_with_sent_to_megafon_and_mts_via_megafon_fixed_and_megafon_multi_and_gold_channels do
      status { LogStatus.completed }
      phones { [Test::Helpers.random_normal_phone_number] }
      content 'content'
      after(:create) do |log|
        FactoryGirl.create(:send_log,
                           log: log,
                           price: 1.25,
                           phone: log.phones.first,
                           channel: Channel.gold,
                           delivery_status: SendLogDeliveryStatus.delivered,
                           delivery_error: SendLogDeliveryError.gold_no_error,
                           cell_operator_group: CellOperatorGroup.mts

        )

        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0.35,
                           phone: log.phones.first,
                           channel: Channel.gold,
                           delivery_status: SendLogDeliveryStatus.failured,
                           delivery_error: SendLogDeliveryError.gold_expired,
                           cell_operator_group: CellOperatorGroup.mts

        )

        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0.35,
                           phone: log.phones.first,
                           channel: Channel.megafon_fixed,
                           delivery_status: SendLogDeliveryStatus.delivered,
                           delivery_error: SendLogDeliveryError.gold_no_error,
                           cell_operator_group: CellOperatorGroup.megafon

        )

        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0.35,
                           phone: log.phones.first,
                           channel: Channel.megafon_fixed,
                           delivery_status: SendLogDeliveryStatus.failured,
                           delivery_error: SendLogDeliveryError.gold_expired,
                           cell_operator_group: CellOperatorGroup.megafon

        )

        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0.35,
                           phone: log.phones.first,
                           channel: Channel.megafon_multi,
                           delivery_status: SendLogDeliveryStatus.delivered,
                           delivery_error: SendLogDeliveryError.gold_no_error,
                           cell_operator_group: CellOperatorGroup.megafon

        )

        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0.35,
                           phone: log.phones.first,
                           channel: Channel.megafon_multi,
                           delivery_status: SendLogDeliveryStatus.failured,
                           delivery_error: SendLogDeliveryError.gold_expired,
                           cell_operator_group: CellOperatorGroup.megafon

        )
      end
    end

    factory :log_with_sent_to_tele2_via_gold do
      status { LogStatus.completed }
      phones { [Test::Helpers.random_normal_phone_number] }
      content 'content'
      after(:create) do |log|
        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0.45,
                           phone: log.phones.first,
                           channel: Channel.gold,
                           delivery_status: SendLogDeliveryStatus.delivered,
                           delivery_error: SendLogDeliveryError.gold_no_error,
                           cell_operator_group: CellOperatorGroup.tele2

        )

        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0.67,
                           phone: log.phones.first,
                           channel: Channel.gold,
                           delivery_status: SendLogDeliveryStatus.failured,
                           delivery_error: SendLogDeliveryError.gold_expired,
                           cell_operator_group: CellOperatorGroup.tele2

        )
      end
    end

    factory :log_with_sent_to_baikal_via_gold do
      status { LogStatus.completed }
      phones { [Test::Helpers.random_normal_phone_number] }
      content 'content'
      after(:create) do |log|
        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0.68,
                           phone: log.phones.first,
                           channel: Channel.gold,
                           delivery_status: SendLogDeliveryStatus.delivered,
                           delivery_error: SendLogDeliveryError.gold_no_error,
                           cell_operator_group: CellOperatorGroup.baikal

        )

        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0.69,
                           phone: log.phones.first,
                           channel: Channel.gold,
                           delivery_status: SendLogDeliveryStatus.failured,
                           delivery_error: SendLogDeliveryError.gold_expired,
                           cell_operator_group: CellOperatorGroup.baikal

        )
      end
    end

    factory :log_with_sent_to_ural_via_gold do
      status { LogStatus.completed }
      phones { [Test::Helpers.random_normal_phone_number] }
      content 'content'
      after(:create) do |log|
        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0.70,
                           phone: log.phones.first,
                           channel: Channel.gold,
                           delivery_status: SendLogDeliveryStatus.delivered,
                           delivery_error: SendLogDeliveryError.gold_no_error,
                           cell_operator_group: CellOperatorGroup.ural

        )

        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0.71,
                           phone: log.phones.first,
                           channel: Channel.gold,
                           delivery_status: SendLogDeliveryStatus.failured,
                           delivery_error: SendLogDeliveryError.gold_expired,
                           cell_operator_group: CellOperatorGroup.ural

        )
      end
    end

    factory :log_with_sent_to_enisey_via_gold do
      status { LogStatus.completed }
      phones { [Test::Helpers.random_normal_phone_number] }
      content 'content'
      after(:create) do |log|
        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0.72,
                           phone: log.phones.first,
                           channel: Channel.gold,
                           delivery_status: SendLogDeliveryStatus.delivered,
                           delivery_error: SendLogDeliveryError.gold_no_error,
                           cell_operator_group: CellOperatorGroup.enisey
        )

        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0.73,
                           phone: log.phones.first,
                           channel: Channel.gold,
                           delivery_status: SendLogDeliveryStatus.failured,
                           delivery_error: SendLogDeliveryError.gold_expired,
                           cell_operator_group: CellOperatorGroup.enisey
        )
      end
    end

    factory :log_with_sent_to_cdma_via_gold do
      status { LogStatus.completed }
      phones { [Test::Helpers.random_normal_phone_number] }
      content 'content'
      after(:create) do |log|
        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0.74,
                           phone: log.phones.first,
                           channel: Channel.gold,
                           delivery_status: SendLogDeliveryStatus.delivered,
                           delivery_error: SendLogDeliveryError.gold_no_error,
                           cell_operator_group: CellOperatorGroup.cdma
        )

        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0.75,
                           phone: log.phones.first,
                           channel: Channel.gold,
                           delivery_status: SendLogDeliveryStatus.failured,
                           delivery_error: SendLogDeliveryError.gold_expired,
                           cell_operator_group: CellOperatorGroup.cdma
        )
      end
    end

    factory :log_with_sent_to_other_via_gold do
      status { LogStatus.completed }
      phones { [Test::Helpers.random_normal_phone_number] }
      content 'content'
      after(:create) do |log|
        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0.76,
                           phone: log.phones.first,
                           channel: Channel.gold,
                           delivery_status: SendLogDeliveryStatus.delivered,
                           delivery_error: SendLogDeliveryError.gold_no_error,
                           cell_operator_group: CellOperatorGroup.other
        )

        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0.78,
                           phone: log.phones.first,
                           channel: Channel.gold,
                           delivery_status: SendLogDeliveryStatus.failured,
                           delivery_error: SendLogDeliveryError.gold_expired,
                           cell_operator_group: CellOperatorGroup.other
        )
      end
    end

    factory :log_with_sent_to_smarts_via_gold do
      status { LogStatus.completed }
      phones { [Test::Helpers.random_normal_phone_number] }
      content 'content'
      after(:create) do |log|
        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0.76,
                           phone: log.phones.first,
                           channel: Channel.gold,
                           delivery_status: SendLogDeliveryStatus.delivered,
                           delivery_error: SendLogDeliveryError.gold_no_error,
                           cell_operator_group: CellOperatorGroup.smarts
        )

        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0.78,
                           phone: log.phones.first,
                           channel: Channel.gold,
                           delivery_status: SendLogDeliveryStatus.failured,
                           delivery_error: SendLogDeliveryError.gold_expired,
                           cell_operator_group: CellOperatorGroup.smarts
        )
      end
    end

    factory :log_with_sent_to_motiv_via_gold do
      status { LogStatus.completed }
      phones { [Test::Helpers.random_normal_phone_number] }
      content 'content'
      after(:create) do |log|
        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0.76,
                           phone: log.phones.first,
                           channel: Channel.gold,
                           delivery_status: SendLogDeliveryStatus.delivered,
                           delivery_error: SendLogDeliveryError.gold_no_error,
                           cell_operator_group: CellOperatorGroup.motiv
        )

        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0.78,
                           phone: log.phones.first,
                           channel: Channel.gold,
                           delivery_status: SendLogDeliveryStatus.failured,
                           delivery_error: SendLogDeliveryError.gold_expired,
                           cell_operator_group: CellOperatorGroup.motiv
        )
      end
    end

    factory :log_with_sent_to_nss_via_gold do
      status { LogStatus.completed }
      phones { [Test::Helpers.random_normal_phone_number] }
      content 'content'
      after(:create) do |log|
        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0.76,
                           phone: log.phones.first,
                           channel: Channel.gold,
                           delivery_status: SendLogDeliveryStatus.delivered,
                           delivery_error: SendLogDeliveryError.gold_no_error,
                           cell_operator_group: CellOperatorGroup.nss
        )

        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0.78,
                           phone: log.phones.first,
                           channel: Channel.gold,
                           delivery_status: SendLogDeliveryStatus.failured,
                           delivery_error: SendLogDeliveryError.gold_expired,
                           cell_operator_group: CellOperatorGroup.nss
        )
      end
    end

    factory :log_with_sent_over_smarts do
      status { LogStatus.completed }
      phones { [Test::Helpers.random_normal_phone_number] }
      content 'content'
      after(:create) do |log|
        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0.79,
                           phone: log.phones.first,
                           channel: Channel.bronze,
                           delivery_status: SendLogDeliveryStatus.delivered,
                           delivery_error: SendLogDeliveryError.bronze_no_error,
                           cell_operator_group: CellOperatorGroup.smarts
        )

        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0,
                           phone: log.phones.first,
                           channel: Channel.bronze,
                           delivery_status: SendLogDeliveryStatus.delivered,
                           delivery_error: SendLogDeliveryError.bronze_no_error,
                           cell_operator_group: CellOperatorGroup.smarts
        )

        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0,
                           phone: log.phones.first,
                           channel: Channel.bronze,
                           delivery_status: SendLogDeliveryStatus.failured,
                           delivery_error: SendLogDeliveryError.bronze_error_data_missing,
                           cell_operator_group: CellOperatorGroup.smarts
        )

        FactoryGirl.create(:send_log,
                           log: log,
                           price: 0.08,
                           phone: log.phones.first,
                           channel: Channel.bronze,
                           delivery_status: SendLogDeliveryStatus.failured,
                           delivery_error: SendLogDeliveryError.bronze_error_data_missing,
                           cell_operator_group: CellOperatorGroup.smarts
        )
      end
    end

  end
end
