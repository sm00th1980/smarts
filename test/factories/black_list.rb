# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :black_list do
    phone { Test::Helpers.random_normal_phone_number }
    created_at Time.now
  end
end
