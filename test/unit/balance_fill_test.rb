# -*- encoding : utf-8 -*-
require 'test_helper'

class BalanceFillTest < ActiveSupport::TestCase

  def setup
    clear_db

    Balance.delete_all
  end

  def teardown
    clear_db
  end

  #previous period
  test "should fill previous period" do
    charges = 10
    payments = 20

    time = Time.now - 1.month

    user = FactoryGirl.create(:user)
    FactoryGirl.create(:log_completed, user: user, price: charges, created_at: time)
    FactoryGirl.create(:payment_approved_manual, user: user, amount: payments, approved_at: time)

    Balance.fill(time.to_date, time.to_date)
    balance = Balance.find_by(period: Balance.period(time.to_date)[:period], user_id: user.id)

    assert_equal payments, balance.payments.round(2)
    assert_equal charges, balance.charges.round(2)
  end

  #current period
  test "should fill current period" do
    charges = 10
    payments = 20

    time = Time.now

    user = FactoryGirl.create(:user)

    FactoryGirl.create(:log_completed, user: user, price: charges, created_at: time)
    FactoryGirl.create(:payment_approved_manual, user: user, amount: payments, approved_at: time)

    Balance.fill(time.to_date, time.to_date)
    balance = Balance.find_by(period: Balance.period(time.to_date)[:period], user_id: user.id)

    assert_equal user.payments.to_a.sum(&:amount), balance.payments.round(2)
    assert_equal user.logs.to_a.sum(&:price), balance.charges.round(2)
  end

end
