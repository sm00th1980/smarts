# -*- encoding : utf-8 -*-
require 'test_helper'

class BlackListTest < ActiveSupport::TestCase
  def setup
    clear_db

    #global
    FactoryGirl.create(:black_list, user: FactoryGirl.create(:admin))

    #personal
    @user = FactoryGirl.create(:user)
    FactoryGirl.create(:black_list, user: @user)
  end

  test "should return only global black listed phones" do
    assert_operator BlackList.global.count, :> , 0

    BlackList.global.each do |black_list|
      assert black_list.user.admin?
    end
  end

  test "should return only personal black listed phones" do
    assert_operator BlackList.personal(@user).count, :> , 0

    BlackList.personal(@user).each do |black_list|
      assert_equal @user, black_list.user
    end
  end
end
