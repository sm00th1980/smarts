# -*- encoding : utf-8 -*-
require 'test_helper'

class LogStat::ZeroPriceMixedTest < ActiveSupport::TestCase
  def setup
    clear_db

    user = FactoryGirl.create(:user)

    @log = FactoryGirl.create(:log_with_non_zero_price_and_zero_price, user: user)
    @operator = @log.send_logs.first.cell_operator_group

    @channel = Channel.gold
  end

  test "should created log_stat when zero price and non-zero price" do
    LogStat.calculate(@log.created_at.to_date, @log.created_at.to_date)

    log_stat = LogStat.last

    assert_equal price(@log, @operator, @channel), log_stat.price
    assert_equal sms_success(@log, @operator, @channel), log_stat.sms_sent_success
    assert_equal sms_failure(@log, @operator, @channel), log_stat.sms_sent_failure
  end

  private
  def price(log, cell_operator_group, channel)
    SendLog.
        select("sum(price) as price").
        where(channel_id: channel).
        where(log_id: log.id, cell_operator_group_id: cell_operator_group.id)[0].price.to_f.round(2)
  end

  def sms_success(log, cell_operator_group, channel)
    _sms_count = 0

    _sms_count += SendLog.
        where(delivery_status_id: SendLogDeliveryStatus.delivered).
        where('price > 0').
        where(channel_id: channel).
        where(log_id: log.id, cell_operator_group_id: cell_operator_group.id).count * log.sms_count

    _sms_count
  end

  def sms_failure(log, cell_operator_group, channel)
    _sms_count = 0

    _sms_count += SendLog.
        where('delivery_status_id <> ?', SendLogDeliveryStatus.delivered).
        where('price > 0').
        where(channel_id: channel).
        where(log_id: log.id, cell_operator_group_id: cell_operator_group.id).count * log.sms_count

    _sms_count
  end

end
