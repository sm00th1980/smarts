# -*- encoding : utf-8 -*-
require 'test_helper'

class LogStat::Gold2Test < ActiveSupport::TestCase
  def setup
    clear_db

    @log = FactoryGirl.create(:log_with_other_date_send_logs, user: FactoryGirl.create(:user))
    @operator = CellOperatorGroup.mts

    @channel = Channel.gold
  end


  test "should create log_stat" do
    LogStat.calculate(@log.created_at.to_date, @log.created_at.to_date)

    log_stat = LogStat.last

    assert_equal price(@log, @operator, @channel), log_stat.price
    assert_equal sms_success(@log, @operator, @channel), log_stat.sms_sent_success
    assert_equal sms_failure(@log, @operator, @channel), log_stat.sms_sent_failure
  end

  private
  def price(log, cell_operator_group, channel)
    SendLog.
        select("sum(price) as price").
        where(channel_id: channel).
        where('price > 0').
        where(log_id: log.id, cell_operator_group_id: cell_operator_group.id)[0].price.to_f.round(2)
  end

  def sms_success(log, cell_operator_group, channel)
    SendLog.
        where(delivery_status_id: SendLogDeliveryStatus.delivered).
        where(channel_id: channel).
        where('price > 0').
        where(log_id: log.id, cell_operator_group_id: cell_operator_group.id).count * log.sms_count
  end

  def sms_failure(log, cell_operator_group, channel)
    SendLog.
        where('delivery_status_id <> ?', SendLogDeliveryStatus.delivered).
        where(channel_id: channel).
        where('price > 0').
        where(log_id: log.id, cell_operator_group_id: cell_operator_group.id).count * log.sms_count
  end

end
