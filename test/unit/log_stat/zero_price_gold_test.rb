# -*- encoding : utf-8 -*-
require 'test_helper'

class LogStat::ZeroPriceGoldTest < ActiveSupport::TestCase
  def setup
    clear_db

    @log = FactoryGirl.create(:log_with_zero_price_gold, user: FactoryGirl.create(:user))
    @channel = Channel.gold
  end

  test "should not created log_stat when zero price" do
    LogStat.calculate(@log.created_at.to_date, @log.created_at.to_date)
    assert_equal 0, LogStat.count
  end

end
