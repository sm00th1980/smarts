# -*- encoding : utf-8 -*-
require 'test_helper'

class LogStat::MegafonMultiTest < ActiveSupport::TestCase
  def setup
    clear_db

    @user = FactoryGirl.create(:user)
    @log_megafon = FactoryGirl.create(:log_with_sent_over_megafon_via_megafon_multi_channel, user: @user)

    @channel = Channel.megafon_multi
  end

  test "should create log_stat" do
    LogStat.calculate(@log_megafon.created_at.to_date, @log_megafon.created_at.to_date)

    #megafon
    log_stat = LogStat.where(cell_operator_group_id: CellOperatorGroup.megafon).last

    assert_equal price(@log_megafon, CellOperatorGroup.megafon, @channel), log_stat.price
    assert_equal sms_success(@log_megafon, CellOperatorGroup.megafon, @channel), log_stat.sms_sent_success
    assert_equal sms_failure(@log_megafon, CellOperatorGroup.megafon, @channel), log_stat.sms_sent_failure
  end

  private
  def price(log, cell_operator_group, channel)
    SendLog.
        select("sum(price) as price").
        where(channel_id: channel).
        where(log_id: log.id, cell_operator_group_id: cell_operator_group.id)[0].price.to_f.round(2)
  end

  def sms_success(log, cell_operator_group, channel)
    SendLog.
        where(delivery_status_id: SendLogDeliveryStatus.delivered).
        where(channel_id: channel).
        where('price > 0').
        where(log_id: log.id, cell_operator_group_id: cell_operator_group.id).count * log.sms_count
  end

  def sms_failure(log, cell_operator_group, channel)
    SendLog.
        where('delivery_status_id <> ?', SendLogDeliveryStatus.delivered).
        where(channel_id: channel).
        where('price > 0').
        where(log_id: log.id, cell_operator_group_id: cell_operator_group.id).count * log.sms_count
  end

end
