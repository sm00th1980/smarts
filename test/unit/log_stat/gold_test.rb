# -*- encoding : utf-8 -*-
require 'test_helper'

class LogStat::GoldTest < ActiveSupport::TestCase
  def setup
    clear_db

    user = FactoryGirl.create(:user)

    @logs = {
        "mts" => FactoryGirl.create(:log_with_sent_to_mts_via_gold, user: user),
        "beeline" => FactoryGirl.create(:log_with_sent_to_beeline_via_gold, user: user),
        "megafon" => FactoryGirl.create(:log_with_sent_to_megafon_via_gold, user: user),
        "tele2" => FactoryGirl.create(:log_with_sent_to_tele2_via_gold, user: user),
        "baikal" => FactoryGirl.create(:log_with_sent_to_baikal_via_gold, user: user),
        "ural" => FactoryGirl.create(:log_with_sent_to_ural_via_gold, user: user),
        "enisey" => FactoryGirl.create(:log_with_sent_to_enisey_via_gold, user: user),
        "cdma" => FactoryGirl.create(:log_with_sent_to_cdma_via_gold, user: user),
        "other" => FactoryGirl.create(:log_with_sent_to_other_via_gold, user: user),
        "motiv" => FactoryGirl.create(:log_with_sent_to_motiv_via_gold, user: user),
        "nss" => FactoryGirl.create(:log_with_sent_to_nss_via_gold, user: user),
        "smarts" => FactoryGirl.create(:log_with_sent_to_smarts_via_gold, user: user)
    }

    @channel = Channel.gold
  end

  test "should create log_stat" do
    begin_date = @logs.values.map{|log| log.created_at.to_date}.sort.first
    end_date = @logs.values.map{|log| log.created_at.to_date}.sort.last

    LogStat.calculate(begin_date, end_date)

    CellOperatorGroup.working.each do |operator|
      log_stat = LogStat.where(cell_operator_group_id: operator).last

      assert_equal price(@logs[operator.internal_name], operator, @channel), log_stat.price
      assert_equal sms_success(@logs[operator.internal_name], operator, @channel), log_stat.sms_sent_success
      assert_equal sms_failure(@logs[operator.internal_name], operator, @channel), log_stat.sms_sent_failure
    end
  end

  private
  def price(log, cell_operator_group, channel)
    SendLog.
        select("sum(price) as price").
        where(channel_id: channel).
        where(log_id: log.id, cell_operator_group_id: cell_operator_group.id)[0].price.to_f.round(2)
  end

  def sms_success(log, cell_operator_group, channel)
    SendLog.
        where(delivery_status_id: SendLogDeliveryStatus.delivered).
        where(channel_id: channel).
        where('price > 0').
        where(log_id: log.id, cell_operator_group_id: cell_operator_group.id).count * log.sms_count
  end

  def sms_failure(log, cell_operator_group, channel)
    SendLog.
        where('delivery_status_id <> ?', SendLogDeliveryStatus.delivered).
        where(channel_id: channel).
        where('price > 0').
        where(log_id: log.id, cell_operator_group_id: cell_operator_group.id).count * log.sms_count
  end

end
