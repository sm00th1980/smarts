# -*- encoding : utf-8 -*-
require 'test_helper'

class LogStat::MegafonGoldTest < ActiveSupport::TestCase
  def setup
    clear_db

    @user = FactoryGirl.create(:user)

    @log = FactoryGirl.create(:log_with_sent_to_megafon_and_mts_via_megafon_fixed_and_megafon_multi_and_gold_channels, user: @user)
  end

  test "should create log_stat" do
    LogStat.calculate(@log.created_at.to_date, @log.created_at.to_date)

    #mts
    log_stat = LogStat.where(cell_operator_group_id: CellOperatorGroup.mts).last

    assert_equal price(@log, CellOperatorGroup.mts, Channel.gold), log_stat.price
    assert_equal sms_success(@log, CellOperatorGroup.mts, Channel.gold), log_stat.sms_sent_success
    assert_equal sms_failure(@log, CellOperatorGroup.mts, Channel.gold), log_stat.sms_sent_failure

    #megafon fixed
    log_stat = LogStat.where(cell_operator_group_id: CellOperatorGroup.megafon, channel_id: Channel.megafon_fixed).last

    assert_equal price(@log, CellOperatorGroup.megafon, Channel.megafon_fixed), log_stat.price
    assert_equal sms_success(@log, CellOperatorGroup.megafon, Channel.megafon_fixed), log_stat.sms_sent_success
    assert_equal sms_failure(@log, CellOperatorGroup.megafon, Channel.megafon_fixed), log_stat.sms_sent_failure

    #megafon multi
    log_stat = LogStat.where(cell_operator_group_id: CellOperatorGroup.megafon, channel_id: Channel.megafon_multi).last

    assert_equal price(@log, CellOperatorGroup.megafon, Channel.megafon_multi), log_stat.price
    assert_equal sms_success(@log, CellOperatorGroup.megafon, Channel.megafon_multi), log_stat.sms_sent_success
    assert_equal sms_failure(@log, CellOperatorGroup.megafon, Channel.megafon_multi), log_stat.sms_sent_failure
  end

  private
  def price(log, cell_operator_group, channel)
    SendLog.
        select("sum(price) as price").
        where(channel_id: channel).
        where(log_id: log.id, cell_operator_group_id: cell_operator_group.id)[0].price.to_f.round(2)
  end

  def sms_success(log, cell_operator_group, channel)
    SendLog.
        where(delivery_status_id: SendLogDeliveryStatus.delivered).
        where(channel_id: channel).
        where('price > 0').
        where(log_id: log.id, cell_operator_group_id: cell_operator_group.id).count * log.sms_count
  end

  def sms_failure(log, cell_operator_group, channel)
    SendLog.
        where('delivery_status_id <> ?', SendLogDeliveryStatus.delivered).
        where(channel_id: channel).
        where('price > 0').
        where(log_id: log.id, cell_operator_group_id: cell_operator_group.id).count * log.sms_count
  end

end
