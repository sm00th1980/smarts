# -*- encoding : utf-8 -*-
require 'test_helper'

class LogStat::BronzeTest < ActiveSupport::TestCase
  def setup
    clear_db

    user = FactoryGirl.create(:user)

    #bronze
    @log_smarts = FactoryGirl.create(:log_with_sent_over_smarts, user: user)

    @channel = Channel.bronze
  end

  test "should create log_stat" do
    LogStat.calculate(@log_smarts.created_at.to_date, @log_smarts.created_at.to_date)

    log_stat = LogStat.last

    assert_equal price(@log_smarts, @channel), log_stat.price
    assert_equal sms_success(@log_smarts, @channel), log_stat.sms_sent_success
    assert_equal sms_failure(@log_smarts, @channel), log_stat.sms_sent_failure
  end

  private
  def price(log, channel)
    SendLog.
        select("sum(price) as price").
        where(channel_id: channel).
        where(log_id: log)[0].price.to_f.round(2)
  end

  def sms_success(log, channel)
    SendLog.
        where(channel_id: channel).
        where(delivery_status_id: SendLogDeliveryStatus.delivered).
        where('price > 0').
        where(log_id: log.id).count * log.sms_count
  end

  def sms_failure(log, channel)
    SendLog.
        where(channel_id: channel).
        where('delivery_status_id <> ?', SendLogDeliveryStatus.delivered).
        where('price > 0').
        where(log_id: log.id).count * log.sms_count
  end

end
