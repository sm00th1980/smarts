# -*- encoding : utf-8 -*-
require 'test_helper'

class PaymentTypeTest < ActiveSupport::TestCase
  def setup
    clear_db
  end

  test "prepaid exist" do
    assert_nothing_raised do
      assert_not_nil PaymentType.prepaid
      assert_equal 'prepaid', PaymentType.prepaid.internal_name
    end
  end

  test "postpaid exist" do
    assert_nothing_raised do
      assert_not_nil PaymentType.postpaid
      assert_equal 'postpaid', PaymentType.postpaid.internal_name
    end
  end

  test "postpaid should be postpaid" do
    PaymentType.postpaid.postpaid?
  end

  test "prepaid should be prepaid" do
    PaymentType.prepaid.prepaid?
  end

end
