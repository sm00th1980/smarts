# -*- encoding : utf-8 -*-
require 'test_helper'

class Group::CalculatorCalculateTest < ActiveSupport::TestCase
  def setup
    clear_db

    user = FactoryGirl.create(:user)
    @group = FactoryGirl.create(:group, user: user, bronze_price_per_sms: nil, gold_price_per_sms: nil)
  end

  test "should correct calculate price" do
    assert_nil @group.reload.gold_price_per_sms
    assert_nil @group.reload.bronze_price_per_sms

    Calculator::Groups.recalculate

    gold_price_per_sms = @group.clients.map{|client| Message.calc_price_per_sms(Channel.gold, @group.user, client.phone).round(2)}.sum.round(2)
    assert_equal gold_price_per_sms, @group.reload.gold_price_per_sms

    bronze_price_per_sms = @group.clients.map{|client| Message.calc_price_per_sms(Channel.bronze, @group.user, client.phone).round(2)}.sum.round(2)
    assert_equal bronze_price_per_sms, @group.reload.bronze_price_per_sms
  end

end
