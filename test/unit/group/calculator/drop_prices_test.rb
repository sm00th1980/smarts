# -*- encoding : utf-8 -*-
require 'test_helper'

class Group::CalculatorDropPricesTest < ActiveSupport::TestCase
  def setup
    clear_db

    user = FactoryGirl.create(:user)
    @group = FactoryGirl.create(:group, user: user)
  end

  test "should drop prices" do
    @group.update_attribute(:bronze_price_per_sms, rand(100))
    @group.update_attribute(:gold_price_per_sms, rand(100))

    assert_not_nil @group.reload.gold_price_per_sms
    assert_not_nil @group.reload.bronze_price_per_sms

    Calculator::Groups.drop_prices([@group.id])

    assert_nil @group.reload.gold_price_per_sms
    assert_nil @group.reload.bronze_price_per_sms
  end

end
