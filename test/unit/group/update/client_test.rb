# -*- encoding : utf-8 -*-
require 'test_helper'

class Group::UpdateClientTest < ActiveSupport::TestCase
  def setup
    clear_db

    @user = FactoryGirl.create(:user)
  end

  test "should drop gold price after update phone on client into group" do
    group = FactoryGirl.create(:group, user: @user, gold_price_per_sms: rand(100))

    client = group.clients.last
    client.phone = 79379903835
    client.save!

    assert_nil group.reload.gold_price_per_sms
  end

  test "should drop bronze price after update phone on client into group" do
    group = FactoryGirl.create(:group, user: @user, bronze_price_per_sms: rand(100))

    client = group.clients.last
    client.phone = 79379903835
    client.save!

    assert_nil group.reload.bronze_price_per_sms
  end

end
