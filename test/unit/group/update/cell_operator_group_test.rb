# -*- encoding : utf-8 -*-
require 'test_helper'

class Group::UpdateCellOperatorGroupTest < ActiveSupport::TestCase
  def setup
    clear_db

    @user = FactoryGirl.create(:user)
    @megafon_tariff = OperatorTariff.find_by_operator_id(CellOperatorGroup.megafon)
  end

  test "should drop gold price after update cell operator group price per sms" do
    group = FactoryGirl.create(:group, user: @user, gold_price_per_sms: rand(100))

    Calculator::Groups.recalculate([group.id])

    assert_not_nil group.reload.gold_price_per_sms

    @megafon_tariff.price_per_sms += rand(100)
    @megafon_tariff.save!

    assert_nil group.reload.gold_price_per_sms
  end

end
