# -*- encoding : utf-8 -*-
require 'test_helper'

class Group::UpdateBronzeTariffTest < ActiveSupport::TestCase
  def setup
    clear_db

    @user_with_tariff = FactoryGirl.create(:user_with_big_tariff)
  end

  test "should recalculate bronze price after update user-tariff" do
    group = FactoryGirl.create(:group, user: @user_with_tariff, bronze_price_per_sms: rand(100))

    user_tariff = UserTariff.where(user_id: @user_with_tariff).first
    user_tariff.end_date = Date.today + 100.days
    user_tariff.save!

    assert_nil group.reload.bronze_price_per_sms
  end

end
