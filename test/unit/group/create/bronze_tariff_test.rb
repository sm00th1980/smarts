# -*- encoding : utf-8 -*-
require 'test_helper'

class Group::CreateBronzeTariffTest < ActiveSupport::TestCase
  def setup
    clear_db

    @user = FactoryGirl.create(:user)
  end

  test "should drop bronze price after create new user-tariff" do
    new_tariff = FactoryGirl.create(:tariff, price_per_sms: rand(100))
    group = FactoryGirl.create(:group, user: @user, bronze_price_per_sms: rand(100))

    UserTariff.create!(tariff: new_tariff, user: @user, begin_date: Date.today)
    assert_nil group.reload.bronze_price_per_sms
  end

end
