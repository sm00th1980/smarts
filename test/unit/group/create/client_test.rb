# -*- encoding : utf-8 -*-
require 'test_helper'

class Group::CreateClientTest < ActiveSupport::TestCase
  def setup
    clear_db

    @user = FactoryGirl.create(:user)
  end

  test "should drop gold price after add new client into group" do
    group = FactoryGirl.create(:group, user: @user)

    Calculator::Groups.recalculate([group.id])
    assert_not_nil group.reload.gold_price_per_sms

    Client.create!(group_id: group.id, phone: 79379903835)

    assert_nil group.reload.gold_price_per_sms
  end

end
