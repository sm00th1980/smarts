# -*- encoding : utf-8 -*-
require 'test_helper'

class Group::DropBronzeTariffTest < ActiveSupport::TestCase
  def setup
    clear_db

    @user_with_tariff = FactoryGirl.create(:user_with_big_tariff)
  end

  test "should drop bronze price on group after drop tariff" do
    group = FactoryGirl.create(:group, user: @user_with_tariff, bronze_price_per_sms: rand(100))

    @user_with_tariff.tariff.destroy

    assert_nil group.reload.bronze_price_per_sms
  end

end
