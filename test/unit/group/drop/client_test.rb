# -*- encoding : utf-8 -*-
require 'test_helper'

class Group::DropClientTest < ActiveSupport::TestCase
  def setup
    clear_db

    @group = FactoryGirl.create(:group, user: FactoryGirl.create(:user))
  end

  test "should drop gold price after destroy client into group" do
    @group.update_attribute(:gold_price_per_sms, rand(100))
    assert_not_nil @group.reload.gold_price_per_sms

    @group.clients.last.destroy
    assert_nil @group.reload.gold_price_per_sms
  end

  test "should drop bronze price after destroy client into group" do
    @group.update_attribute(:bronze_price_per_sms, rand(100))
    assert_not_nil @group.reload.bronze_price_per_sms

    @group.clients.last.destroy
    assert_nil @group.reload.bronze_price_per_sms
  end

end
