# -*- encoding : utf-8 -*-
require 'test_helper'

class Group::DropCellOperatorGroupTest < ActiveSupport::TestCase
  def setup
    clear_db

    @user = FactoryGirl.create(:user)
  end

  test "should drop gold price after drop cell operator group price per sms" do
    group = FactoryGirl.create(:group, user: @user, gold_price_per_sms: rand(100))

    CellOperatorGroup.megafon.destroy
    assert_nil group.reload.gold_price_per_sms
  end

end
