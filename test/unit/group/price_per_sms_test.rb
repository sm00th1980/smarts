# -*- encoding : utf-8 -*-
require 'test_helper'

class Group::PricePerSmsTest < ActiveSupport::TestCase
  def setup
    clear_db

    @user = FactoryGirl.create(:user)
  end

  test "should return zero price for invalid sender name" do
    @group = FactoryGirl.create(:group, user: @user)

    assert_equal 0, @group.price_per_sms(nil)
    assert_equal 0, @group.price_per_sms('')
    assert_equal 0, @group.price_per_sms(' ')
  end

  test "should return zero price for alpha sender name if gold price is nil" do
    alpha_sender_name_value = FactoryGirl.create(:sender_name_accepted, user: @user, value: 'ESET').value
    group = FactoryGirl.create(:group, user: @user, gold_price_per_sms: nil)

    assert_nil group.gold_price_per_sms
    assert_equal 0, group.price_per_sms(@alpha_sender_name_value)
  end

  test "should return zero price for def sender name if bronze price is nil" do
    def_sender_name_value = FactoryGirl.create(:sender_name_accepted, user: @user, value: '79379903835').value
    group = FactoryGirl.create(:group, user: @user, bronze_price_per_sms: nil)

    assert_nil group.bronze_price_per_sms
    assert_equal 0, group.price_per_sms(def_sender_name_value)
  end

end
