# -*- encoding : utf-8 -*-
require 'test_helper'

class Message::PricePerSmsMegafonFixedTest < ActiveSupport::TestCase
  def setup
    clear_db

    @users = [FactoryGirl.create(:user_charge_by_sent), FactoryGirl.create(:user_charge_by_delivered)]

    @mts_phone = CellOperator.where(cell_operator_group_id: CellOperatorGroup.mts).first.ranges[0].begin_phone
    @beeline_phone = CellOperator.where(cell_operator_group_id: CellOperatorGroup.beeline).first.ranges[0].begin_phone
    @megafon_phone = CellOperator.where(cell_operator_group_id: CellOperatorGroup.megafon).first.ranges[0].begin_phone
    @tele2_phone = CellOperator.where(cell_operator_group_id: CellOperatorGroup.tele2).first.ranges[0].begin_phone
    @baikal_phone = CellOperator.where(cell_operator_group_id: CellOperatorGroup.baikal).first.ranges[0].begin_phone
    @ural_phone = CellOperator.where(cell_operator_group_id: CellOperatorGroup.ural).first.ranges[0].begin_phone
    @enisey_phone = CellOperator.where(cell_operator_group_id: CellOperatorGroup.enisey).first.ranges[0].begin_phone
    @cdma_phone = CellOperator.where(cell_operator_group_id: CellOperatorGroup.cdma).first.ranges[0].begin_phone
    @other_phone = CellOperator.where(cell_operator_group_id: CellOperatorGroup.other).first.ranges[0].begin_phone
    @smarts_phone = CellOperator.where(cell_operator_group_id: CellOperatorGroup.smarts).first.ranges[0].begin_phone

    @my_own_number = 79379903835
    @not_exists_phone = -1

    @channels = [Channel.megafon_fixed, Channel.megafon_multi]
  end

  #smarts
  test "should price-per-sms be equal as smarts operator if smarts" do
    @users.each do |user|
      @channels.each do |channel|
        assert_equal CellOperatorGroup.smarts.price_per_sms, Message.price_per_sms(channel, user, @smarts_phone)
      end
    end
  end

  #mts
  test "should price-per-sms be equal mts price-per-sms if mts" do
    @users.each do |user|
      @channels.each do |channel|
        assert_equal CellOperatorGroup.mts.price_per_sms, Message.price_per_sms(channel, user, @mts_phone)
      end
    end
  end

  #beeline
  test "should price-per-sms be equal beeline price-per-sms if beeline" do
    @users.each do |user|
      @channels.each do |channel|
        assert_equal CellOperatorGroup.beeline.price_per_sms, Message.price_per_sms(channel, user, @beeline_phone)
      end
    end
  end

  #megafon
  test "should price-per-sms be equal megafon price-per-sms if megafon" do
    @users.each do |user|
      @channels.each do |channel|
        assert_equal CellOperatorGroup.megafon.price_per_sms, Message.price_per_sms(channel, user, @megafon_phone)
      end
    end
  end

  #tele2
  test "should price-per-sms be equal tele2 price-per-sms" do
    @users.each do |user|
      @channels.each do |channel|
        assert_equal CellOperatorGroup.tele2.price_per_sms, Message.price_per_sms(channel, user, @tele2_phone)
      end
    end
  end

  #baikal
  test "should price-per-sms be equal baikal price-per-sms" do
    @users.each do |user|
      @channels.each do |channel|
        assert_equal CellOperatorGroup.baikal.price_per_sms, Message.price_per_sms(channel, user, @baikal_phone)
      end
    end
  end

  #ural
  test "should price-per-sms be equal ural price-per-sms" do
    @users.each do |user|
      @channels.each do |channel|
        assert_equal CellOperatorGroup.ural.price_per_sms, Message.price_per_sms(channel, user, @ural_phone)
      end
    end
  end

  #enisey
  test "should price-per-sms be equal enisey price-per-sms" do
    @users.each do |user|
      @channels.each do |channel|
        assert_equal CellOperatorGroup.enisey.price_per_sms, Message.price_per_sms(channel, user, @enisey_phone)
      end
    end
  end

  #cdma
  test "should price-per-sms be equal cdma price-per-sms" do
    @users.each do |user|
      @channels.each do |channel|
        assert_equal CellOperatorGroup.cdma.price_per_sms, Message.price_per_sms(channel, user, @cdma_phone)
      end
    end
  end

  #other
  test "should price-per-sms be equal other price-per-sms" do
    @users.each do |user|
      @channels.each do |channel|
        assert_equal CellOperatorGroup.other.price_per_sms, Message.price_per_sms(channel, user, @other_phone)
      end
    end
  end

  #my own phone
  test "should price-per-sms be equal megafon for my-own-number" do
    @users.each do |user|
      @channels.each do |channel|
        assert_equal CellOperatorGroup.megafon.price_per_sms, Message.price_per_sms(channel, user, @my_own_number)
      end
    end
  end

  #not_exists_phone
  test "should price-per-sms be equal other for not exists phone" do
    @users.each do |user|
      @channels.each do |channel|
        assert_equal CellOperatorGroup.other.price_per_sms, Message.price_per_sms(channel, user, @not_exists_phone)
      end
    end
  end

end
