# -*- encoding : utf-8 -*-
require 'test_helper'

class Message::PricePerSmsAnyTest < ActiveSupport::TestCase
  def setup
    clear_db

    @user = FactoryGirl.create(:user)

    @mts_phone = CellOperator.where(cell_operator_group_id: CellOperatorGroup.mts).first.ranges[0].begin_phone
    @beeline_phone = CellOperator.where(cell_operator_group_id: CellOperatorGroup.beeline).first.ranges[0].begin_phone
    @megafon_phone = CellOperator.where(cell_operator_group_id: CellOperatorGroup.megafon).first.ranges[0].begin_phone
    @tele2_phone = CellOperator.where(cell_operator_group_id: CellOperatorGroup.tele2).first.ranges[0].begin_phone
    @baikal_phone = CellOperator.where(cell_operator_group_id: CellOperatorGroup.baikal).first.ranges[0].begin_phone
    @ural_phone = CellOperator.where(cell_operator_group_id: CellOperatorGroup.ural).first.ranges[0].begin_phone
    @enisey_phone = CellOperator.where(cell_operator_group_id: CellOperatorGroup.enisey).first.ranges[0].begin_phone
    @cdma_phone = CellOperator.where(cell_operator_group_id: CellOperatorGroup.cdma).first.ranges[0].begin_phone
    @other_phone = CellOperator.where(cell_operator_group_id: CellOperatorGroup.other).first.ranges[0].begin_phone
    @smarts_phone = CellOperator.where(cell_operator_group_id: CellOperatorGroup.smarts).first.ranges[0].begin_phone

    @my_own_number = 79379903835
    @not_exists_phone = -1

    @channel = Channel.any
  end

  #smarts
  test "should price-per-sms be 0 if smarts" do
    assert_equal 0, Message.price_per_sms(@channel, @user, @smarts_phone)
  end

  #mts
  test "should price-per-sms be 0 if mts" do
    assert_equal 0, Message.price_per_sms(@channel, @user, @mts_phone)
  end

  #beeline
  test "should price-per-sms be 0 if beeline" do
    assert_equal 0, Message.price_per_sms(@channel, @user, @beeline_phone)
  end

  #megafon
  test "should price-per-sms be 0 if megafon" do
    assert_equal 0, Message.price_per_sms(@channel, @user, @megafon_phone)
  end

  #tele2
  test "should price-per-sms be 0 if tele2" do
    assert_equal 0, Message.price_per_sms(@channel, @user, @tele2_phone)
  end

  #baikal
  test "should price-per-sms be 0 if baikal" do
    assert_equal 0, Message.price_per_sms(@channel, @user, @baikal_phone)
  end

  #ural
  test "should price-per-sms be 0 if ural" do
    assert_equal 0, Message.price_per_sms(@channel, @user, @ural_phone)
  end

  #enisey
  test "should price-per-sms be 0 if enisey" do
    assert_equal 0, Message.price_per_sms(@channel, @user, @enisey_phone)
  end

  #cdma
  test "should price-per-sms be 0 if cdma" do
    assert_equal 0, Message.price_per_sms(@channel, @user, @cdma_phone)
  end

  #other
  test "should price-per-sms be 0 if other" do
    assert_equal 0, Message.price_per_sms(@channel, @user, @other_phone)
  end

  #my own phone
  test "should price-per-sms be 0 if my-own-number" do
    assert_equal 0, Message.price_per_sms(@channel, @user, @my_own_number)
  end

  #not_exists_phone
  test "should price-per-sms be 0 if not-exists-phone" do
    assert_equal 0, Message.price_per_sms(@channel, @user, @not_exists_phone)
  end

end
