# -*- encoding : utf-8 -*-
require 'test_helper'

class Message::MessagePricePerSmsTest < ActiveSupport::TestCase
  def setup
    clear_db

    @users = [
        FactoryGirl.create(:user_charge_by_sent),
        FactoryGirl.create(:user_charge_by_delivered)
    ]

    @megafon_phone = CellOperator.where(cell_operator_group_id: CellOperatorGroup.megafon).first.ranges[0].begin_phone
  end

  test "should calc-price-per-sms be equal 0 if channel is nil" do
    @users.each do |user|
      assert_equal 0, Message.calc_price_per_sms(nil, user, @megafon_phone)
    end
  end

  test "should price-per-sms be equal 0 if channel blank" do
    @users.each do |user|
      assert_equal 0, Message.calc_price_per_sms('', user, @megafon_phone)
    end
  end

  test "should price-per-sms be equal 0 if channel is invalid" do
    @users.each do |user|
      assert_equal 0, Message.calc_price_per_sms('vfdbgf', user, @megafon_phone)
    end
  end

  #bronze
  test "should calc price-per-sms for bronze channel" do
    @users.each do |user|
      assert_equal user.price_per_sms, Message.calc_price_per_sms(Channel.bronze, user, @megafon_phone)
    end
  end

  #gold
  test "should calc price-per-sms for gold channel" do
    @users.each do |user|
      assert_equal CellOperatorGroup.megafon.price_per_sms, Message.calc_price_per_sms(Channel.gold, user, @megafon_phone)
    end
  end

  #unknown
  test "should calc price-per-sms for unknown channel" do
    @users.each do |user|
      assert_equal 0, Message.calc_price_per_sms(Channel.unknown, user, @megafon_phone)
    end
  end

  #megafon fixed
  test "should calc price-per-sms for megafon fixed channel" do
    @users.each do |user|
      assert_equal CellOperatorGroup.megafon.price_per_sms, Message.calc_price_per_sms(Channel.megafon_fixed, user, @megafon_phone)
    end
  end

  #megafon multi
  test "should calc price-per-sms for megafon multi channel" do
    @users.each do |user|
      assert_equal CellOperatorGroup.megafon.price_per_sms, Message.calc_price_per_sms(Channel.megafon_multi, user, @megafon_phone)
    end
  end
end
