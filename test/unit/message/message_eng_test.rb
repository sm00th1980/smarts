# -*- encoding : utf-8 -*-
require 'test_helper'

class Message::MessageEngTest < ActiveSupport::TestCase
  def setup
    base_message =  "0123456789A"

    @one_sms = [
        base_message*1,
        base_message*2,
        base_message*3,
        "vfnvjngjnjnjgnbjngfjbngfjknbgfnbjgfnbjgnfbjngfjbngjnbjgnfbjgnjbngjfbnjg",
        "Segodnya, 19.09 v clube Rise GROOVE U (Stambul / New York) i 2RabbitDJ's. Vhod do 24.00 free! Do 01.00 coctails po 99r. Spiski vk.com/rise1909. Reserve 2992995",
        "OK-PS3: Transformery:za Iskru-2390 Last of UsRU1550 Rul'-Nakladka190 b.u.: FightNightCh.290 DarknessII390 FarCry2ru290 BulletstormRU650 HomefrontRU400 WWE13-490",
        "OK-PS3:\r\nTransformery:za Iskru-2390\r\nLast of UsRU1550\r\nRul'-Nakladka190\r\nb.u.:\r\nFightNightCh.290\r\nDarknessII390\r\nFarCry2ru290\r\nBulletstormRU650\r\nHomefrontRU400\r\nWWE13-490"
    ]

    @two_sms = [
        "OK-PS3: Transformery:za Iskru-2390 Last of UsRU1550 Rul'-Nakladka190 b.u.: FightNightCh.290 DarknessII39vnfdnvkfdnjkvfdjkvndfvnjkdfnvjkfdkjvdfnjkvjdfkvnfjdkvkjfdnvfdkjnvkjfdnvkjfdnjkvfdkjvnjvnfjdnvfdnjvndfvdfjknf0",
        "OK-PS3: Transformery:za Iskru-2390 Last of UsRU1550 Rul'-Nakladka190 b.u.: FightNightCh.290 DarknessII39vnfdnvkfdnjkvfdjkvndfvnjkdfnvjkfdkjvdfnjkvjdfkvnfjdkvkjfdnvfdkjnvkjfdnvkjfdnjkvfdkjvnjvnfjvndenjwcbhjevbhjfbhvfbhvfdnvfdnjvndfvdfjknvnfnvkjgnkjbgfnkjbngjkfbkjfgnbgfjkbnfgjbnfgbngjfnbjkgfnjf0"
    ]

    @three_sms = [
        "OK-PS3: Transformery:za Iskru-2390 Last of UsRU1550 Rul'-Nakladka190 b.u.: FightNightCh.290 DarknessII39vnfdnvkfdnjkvfdjkvndfvnjkdfnvjkfdkjvdfnjkvjdfkvnfjdkvkjfdnvfdkjnvkjfdnvkjfdnjkvfdkjvnjvnfjvndenjwcbhjevbhjfbhvfbhvfdnvfdnjbngkfbgkfnbgfjknbfgjknbfgjkvndfvdfjknvnfnvkjgnkjbgfnkjbngjkfbkjfgnbgfjkbnfgfnjnjnfbnjkfnbjnjnjbnfgbngjfnbjkgfnjf0"
    ]
  end

  test "should count as one sms" do
    @one_sms.each do |content|
      assert_equal 1, Message.sms_count(content), "текст <#{content}> должен умешаться в 1 смс"
    end
  end

  test "should count as two sms" do
    @two_sms.each do |content|
      assert_equal 2, Message.sms_count(content), "текст <#{content}> должен умешаться в 2 смс"
    end
  end

  test "should count as three sms" do
    @three_sms.each do |content|
      assert_equal 3, Message.sms_count(content), "текст <#{content}> должен умешаться в 3 смс"
    end
  end

  test "should not raise exception if input data is invalid" do
    assert_nothing_raised {
      assert_equal 0, Message.sms_count('')
      assert_equal 0, Message.sms_count(nil)
    }
  end

end
