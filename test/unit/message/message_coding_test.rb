# -*- encoding : utf-8 -*-
require 'test_helper'

class Message::MessageCodingTest < ActiveSupport::TestCase
  def setup
    @eng_text = [
        "vfnvjngjnjnjgnbjngfjbngfjknbgfnbjgfnbjgnfbjngfjbngjnbjgnfbjgnjbngjfbnjg",
        "",
        nil
    ]

    @rus_text = [
        "Стоматология НОВА  Низкие цены для Вас! пр. Строителей 156а\r\n413-314",
        "Здравствуйте. На Ваш аппарат получен акт  для обмена. Орбита-Сервис\r\n",
        "Денис, гостинница Апельсин в Ульяновске заказана на 11 апреля. 2000 рублей на нас троих будет её стоимость. Русланка.",
        "Заявка № 31***-72 обработана. Государственный кадастровый учет приостановлен. Документы будут доставлены в офис приема-выдачи в течение 2 дней.",
        "Только этой весной, в SPA-центре MatreshkaPlaza новые программы для коррекции проблемных зон! Антицеллюлитный, лимфодренажный и лифтинговые эффекты. Три процедуры в подарок! Торопись, предложение ограничено. Запись по телефону: 933-30-40",
        "Denis, gostinniza Apelsin v Ulianovske zakazana na 11 aprelya. 2000 rubley na nas troih budet ee soimost. Ruslanka. ё"
    ]

  end

  test "should return ascii for eng text" do
    @eng_text.each do |content|
      #assert_equal Message::CODING_ASCII, Message.coding(content)
      assert_equal Message::CODING_UTF8, Message.coding(content)
    end
  end

  test "should return utf8 for rus text" do
    @rus_text.each do |content|
      assert_equal Message::CODING_UTF8, Message.coding(content)
    end
  end

end
