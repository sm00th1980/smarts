# -*- encoding : utf-8 -*-
require 'test_helper'

class Message::MessageHasNonAsciiTest < ActiveSupport::TestCase
  def setup
    #with russian
    @russian = [
        'ngfЯnvfjkd',
        'ngfЯnvЦfjkd',
        'ngfяnvfjkd',
        'ngfаnvfjkd',
        'ngfбnvfjkd',
        'Denis, gostinniza Apelsin v Ulianovske zakazana na 11 aprelya. 2000 rubley na nas troih budet ee soimost. Ruslanka. ё',
        "OK-PS3:\nDiabloIII:UltimateEvilRU-1990\nPlants vs. Zombies Garden Warfare-1290\nb.u:\nArmy of Two40thDay-600\nDevil’sCartel-800\nBattlefield4ru-1300\nCastlevania2-900\nResidentEvil5-500\nLastOfUsRU-1390\nLollipopRU-490"
    ]

    #no russian
    @no_russian = [
        '0123456789',
        'vfmdkvf',
        'Anfj789njfd89',
        'BJNJKJKNMLYUH'
    ]
  end

  test "should check russian" do
    @russian.each do  |message|
      assert Message.has_non_ascii_letter?(message)
    end
  end

  test "should check no russian" do
    @no_russian.each do  |message|
      refute Message.has_non_ascii_letter?(message)
    end
  end

end
