# -*- encoding : utf-8 -*-
require 'test_helper'

class Message::MessageRusTest < ActiveSupport::TestCase
  def setup
    @one_sms = [
        "Стоматология НОВА  Низкие цены для Вас! пр. Строителей 156а\r\n413-314",
        "Здравствуйте. На Ваш аппарат получен акт  для обмена. Орбита-Сервис\r\n",
        "МагазинOK акция к 1Сентября Heelys-15% образовательные программы-40%",
        "OK-PC:\r\nKing's Bounty: Темная сторона-490\r\nRisen3TLords-690\r\nFarCry2-190",
        "OK-PC:\nKing's Bounty: Темная сторона-490\nRisen3TLords-690\nFarCry2-190",
        "Денис, гостинница Апельсин в Ульяновске заказана на 11 апреля. 2000 ру"
    ]

    @two_sms = [
        "Денис, гостинница Апельсин в Ульяновске заказана на 11 апреля. 2000 руб",
        "Денис, гостинница Апельсин в Ульяновске заказана на 11 апреля. 2000 рублей на нас троих будет её стоимость. Русланка."
    ]

    @three_sms = [
        "Заявка № 31***-72 обработана. Государственный кадастровый учет приостановлен. Документы будут доставлены в офис приема-выдачи в течение 2 дней.",
        "OK-PS3:\nDiabloIII:UltimateEvilRU-1990\nPlants vs. Zombies Garden Warfare-1290\nb.u:\nArmy of Two40thDay-600\nDevil’sCartel-800\nBattlefield4ru-1300\nCastlevania2-900\nResidentEvil5-500\nLastOfUsRU-1390\nLollipopRU-490"
    ]

    @four_sms = [
        "Только этой весной, в SPA-центре MatreshkaPlaza новые программы для коррекции проблемных зон! Антицеллюлитный, лимфодренажный и лифтинговые эффекты. Три процедуры в подарок! Торопись, предложение ограничено. Запись по телефону: 933-30-40"
    ]

    @two_sms_with_one_russian_letter = [
        "Denis, gostinniza Apelsin v Ulianovske zakazana na 11 aprelya. 2000 rubley na nas troih budet ee soimost. Ruslanka. ё"
    ]
  end

  test "should count as one sms" do
    @one_sms.each do |content|
      assert_equal 1, Message.sms_count(content), "текст <#{content}> должен умешаться в 1 смс"
    end
  end

  test "should count as two sms" do
    @two_sms.each do |content|
      assert_equal 2, Message.sms_count(content), "текст <#{content}> должен умешаться в 2 смс"
    end
  end

  test "should count as three sms" do
    @three_sms.each do |content|
      assert_equal 3, Message.sms_count(content), "текст <#{content}> должен умешаться в 3 смс"
    end
  end

  test "should count as four sms" do
    @four_sms.each do |content|
      assert_equal 4, Message.sms_count(content), "текст <#{content}> должен умешаться в 4 смс"
    end
  end

  test "should not raise exception if input data is invalid" do
    assert_nothing_raised {
      assert_equal 0, Message.sms_count('')
      assert_equal 0, Message.sms_count(nil)
    }
  end

  test "should count as two sms with one russian letter" do
    @two_sms_with_one_russian_letter.each do |content|
      assert_equal 2, Message.sms_count(content), "текст <#{content}> должен умешаться в 2 смс"
    end
  end

end
