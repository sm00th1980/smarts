# -*- encoding : utf-8 -*-
require 'test_helper'

class ReportManagerTest < ActionMailer::TestCase

  def setup
    clear_db

    @log = FactoryGirl.create(:log_with_chunks, user: FactoryGirl.create(:user_charge_by_delivered), sender_name_value: '79379903835')
  end

  test "should create task for report generate" do
    check_new_task_created('Report::Manager', 'generate') do
      Report::Manager.delay.generate(@log)
    end
  end

  test "should create task for report generate by date" do
    check_new_task_created('Report::Manager', 'generate_by_date') do
      Report::Manager.delay.generate_by_date(@log.user, Date.today, Date.today)
    end
  end
end
