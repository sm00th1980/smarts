# -*- encoding : utf-8 -*-
require 'test_helper'

class Report::UsersTest < ActionMailer::TestCase
  def setup
    clear_db

    @user_with_contact_email = FactoryGirl.create(:user, contact_email: 'sm00th1980@mail.ru')

    #invalid
    @user_with_nil_contact_email = FactoryGirl.create(:user, contact_email: nil)
    @user_with_blank_contact_email = FactoryGirl.create(:user, contact_email: '')
    @user_with_incorrect_contact_email1 = FactoryGirl.create(:user, contact_email: '-1')
    @user_with_incorrect_contact_email2 = FactoryGirl.create(:user, contact_email: 'sm00th1980@')
  end

  test "should has contact-email for correct contact-email" do
    assert @user_with_contact_email.has_contact_email?
  end

  test "should not have contact-email for invalid contact-emails" do
    refute @user_with_nil_contact_email.has_contact_email?
    refute @user_with_blank_contact_email.has_contact_email?
    refute @user_with_incorrect_contact_email1.has_contact_email?
    refute @user_with_incorrect_contact_email2.has_contact_email?
  end
end
