# -*- encoding : utf-8 -*-
require 'test_helper'
require 'zip'

class Report::GeneratorTest < ActiveSupport::TestCase
  def setup
    clear_db

    #удаляем предварительно директорию с отчётами
    FileUtils.rm_rf(Rails.root.join(Report::Generator::REPORT_DIR))

    user = FactoryGirl.create(:user_charge_by_delivered)

    @log = FactoryGirl.create(:log_with_chunks, user: user, sender_name_value: '79379903835')

    #invalid logs
    @log_with_separator_in_content = FactoryGirl.create(:log_with_separator_in_content, user: user, sender_name_value: '79379903835')
    @log_with_return_in_content = FactoryGirl.create(:log_with_return_in_content, user: user, sender_name_value: '79379903835')
    @log_with_windows_return_in_content = FactoryGirl.create(:log_with_windows_return_in_content, user: user, sender_name_value: '79379903835')
  end

  def teardown
    FileUtils.rm_rf(Rails.root.join(Report::Generator::REPORT_DIR))
  end

  test "should generate correct CSV report" do
    generator = Report::Generator.new(@log)
    generator.save_report

    refute File.exists? generator.path
    assert File.exists? generator.zippath

    unzip_file(generator.zippath, generator.report_dir) do |file| #unzip report
      i = 0
      send_logs = @log.send_logs.sort { |x, y| x.created_at <=> y.created_at }
      CSV.foreach(file, :col_sep => Report::Generator::COLUMN_SEPARATOR, :encoding => Report::Generator::ENCODING) do |row|
        if i == 0
          #заголовок
          assert generator.header[0], row[0]
          assert generator.header[1], row[1]
          assert generator.header[2], row[2]
          assert generator.header[3], row[3]
          assert generator.header[4], row[4]
          assert generator.header[5], row[5]
          assert generator.header[6], row[6]
          assert generator.header[7], row[7]
          assert generator.header[8], row[8]
        else
          assert_equal send_logs[i-1].created_at.strftime('%Y-%m-%d, %H:%M:%S'), row[0]
          assert_equal send_logs[i-1].phone, row[1].to_i
          assert_equal send_logs[i-1].log.content, row[2]
          assert_equal send_logs[i-1].log.sender_name_value, row[3]
          assert_equal send_logs[i-1].price.to_s, row[4]
          assert_equal send_logs[i-1].log.sms_count.to_s, row[5]
          assert_equal send_logs[i-1].cell_operator_group.name, row[6]
          assert_equal send_logs[i-1].delivery_status.name, row[7]
          assert_equal send_logs[i-1].delivery_error.name, row[8]
        end

        i+=1
      end

      assert_operator send_logs.size + 1, :>, 1
      assert_equal send_logs.size + 1, i
    end
  end

  #invalid
  test "should generate CSV report without separator in content" do
    generator = Report::Generator.new(@log_with_separator_in_content)
    generator.save_report

    refute File.exists? generator.path
    assert File.exists? generator.zippath

    unzip_file(generator.zippath, generator.report_dir) do |file| #unzip report
      i = 0
      send_logs = @log_with_separator_in_content.send_logs.sort { |x, y| x.created_at <=> y.created_at }
      CSV.foreach(file, :col_sep => Report::Generator::COLUMN_SEPARATOR, :encoding => Report::Generator::ENCODING) do |row|
        if i == 0
          #заголовок
          assert generator.header[0], row[0]
          assert generator.header[1], row[1]
          assert generator.header[2], row[2]
          assert generator.header[3], row[3]
          assert generator.header[4], row[4]
          assert generator.header[5], row[5]
          assert generator.header[6], row[6]
          assert generator.header[7], row[7]
          assert generator.header[8], row[8]
        else
          assert_equal send_logs[i-1].created_at.strftime('%Y-%m-%d, %H:%M:%S'), row[0]
          assert_equal send_logs[i-1].phone, row[1].to_i
          assert_equal send_logs[i-1].log.content.delete(Report::Generator::COLUMN_SEPARATOR), row[2]
          assert_equal send_logs[i-1].log.sender_name_value, row[3]
          assert_equal send_logs[i-1].price.to_s, row[4]
          assert_equal send_logs[i-1].log.sms_count.to_s, row[5]
          assert_equal send_logs[i-1].cell_operator_group.name, row[6]
          assert_equal send_logs[i-1].delivery_status.name, row[7]
          assert_equal send_logs[i-1].delivery_error.name, row[8]

          refute row[2].split.include?(Report::Generator::COLUMN_SEPARATOR)
        end

        i+=1
      end

      assert_operator send_logs.size + 1, :>, 1
      assert_equal send_logs.size + 1, i
    end
  end

  test "should generate CSV report without return in content" do
    generator = Report::Generator.new(@log_with_return_in_content)
    generator.save_report

    refute File.exists? generator.path
    assert File.exists? generator.zippath

    unzip_file(generator.zippath, generator.report_dir) do |file| #unzip report
      i = 0
      send_logs = @log_with_return_in_content.send_logs.sort { |x, y| x.created_at <=> y.created_at }
      CSV.foreach(file, :col_sep => Report::Generator::COLUMN_SEPARATOR, :encoding => Report::Generator::ENCODING) do |row|
        if i == 0
          #заголовок
          assert generator.header[0], row[0]
          assert generator.header[1], row[1]
          assert generator.header[2], row[2]
          assert generator.header[3], row[3]
          assert generator.header[4], row[4]
          assert generator.header[5], row[5]
          assert generator.header[6], row[6]
          assert generator.header[7], row[7]
          assert generator.header[8], row[8]
        else
          assert_equal send_logs[i-1].created_at.strftime('%Y-%m-%d, %H:%M:%S'), row[0]
          assert_equal send_logs[i-1].phone, row[1].to_i
          assert_equal send_logs[i-1].log.content.delete("\n"), row[2]
          assert_equal send_logs[i-1].log.sender_name_value, row[3]
          assert_equal send_logs[i-1].price.to_s, row[4]
          assert_equal send_logs[i-1].log.sms_count.to_s, row[5]
          assert_equal send_logs[i-1].cell_operator_group.name, row[6]
          assert_equal send_logs[i-1].delivery_status.name, row[7]
          assert_equal send_logs[i-1].delivery_error.name, row[8]

          refute row[2].split.include?("\n")
        end
        i+=1
      end

      assert_operator send_logs.size + 1, :>, 1
      assert_equal send_logs.size + 1, i
    end
  end

  test "should generate CSV report without windows return in content" do
    generator = Report::Generator.new(@log_with_windows_return_in_content)
    generator.save_report

    refute File.exists? generator.path
    assert File.exists? generator.zippath

    unzip_file(generator.zippath, generator.report_dir) do |file| #unzip report
      i = 0
      send_logs = @log_with_windows_return_in_content.send_logs.sort { |x, y| x.created_at <=> y.created_at }
      CSV.foreach(file, :col_sep => Report::Generator::COLUMN_SEPARATOR, :encoding => Report::Generator::ENCODING) do |row|
        if i == 0
          #заголовок
          assert generator.header[0], row[0]
          assert generator.header[1], row[1]
          assert generator.header[2], row[2]
          assert generator.header[3], row[3]
          assert generator.header[4], row[4]
          assert generator.header[5], row[5]
          assert generator.header[6], row[6]
          assert generator.header[7], row[7]
          assert generator.header[8], row[8]
        else
          assert_equal send_logs[i-1].created_at.strftime('%Y-%m-%d, %H:%M:%S'), row[0]
          assert_equal send_logs[i-1].phone, row[1].to_i
          assert_equal send_logs[i-1].log.content.delete("\r\n"), row[2]
          assert_equal send_logs[i-1].log.sender_name_value, row[3]
          assert_equal send_logs[i-1].price.to_s, row[4]
          assert_equal send_logs[i-1].log.sms_count.to_s, row[5]
          assert_equal send_logs[i-1].cell_operator_group.name, row[6]
          assert_equal send_logs[i-1].delivery_status.name, row[7]
          assert_equal send_logs[i-1].delivery_error.name, row[8]

          refute row[2].split.include?("\r")
          refute row[2].split.include?("\n")
        end
        i+=1
      end

      assert_operator send_logs.size + 1, :>, 1
      assert_equal send_logs.size + 1, i
    end
  end

  private
  def unzip_file (file, destination, &block)
    _paths = []
    Zip::File.open(file) do |zip_file|
      zip_file.each do |f|
        f_path=File.join(destination, f.name)
        FileUtils.mkdir_p(File.dirname(f_path))
        zip_file.extract(f, f_path) unless File.exist?(f_path)

        _paths << f_path
      end
    end

    yield(_paths.first) if block_given?

    #удаляем после себя разархивированные файлы
    _paths.each { |path| FileUtils.rm(path) }
  end

end
