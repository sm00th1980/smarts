# -*- encoding : utf-8 -*-
require 'test_helper'

class Report::SenderGenerateByDateTest < ActionMailer::TestCase
  def setup
    clear_db

    @user = FactoryGirl.create(:user_charge_by_delivered, contact_email: 'sm00th1980@mail.ru')
    @log = FactoryGirl.create(:log_with_chunks, user: @user, sender_name_value: '79379903835')

    @generator = Report::GeneratorByDate.new(@user, @log.created_at.to_date, @log.created_at.to_date)
    @filename = @generator.save_report
  end

  test "should send report-by-date by email" do
    #проверяем что файл для отправки существует
    assert File.exists? @generator.zippath

    Report::Manager.generate_by_date(@user, @log.created_at.to_date, @log.created_at.to_date).deliver_now
    refute ActionMailer::Base.deliveries.empty?
    email = ActionMailer::Base.deliveries.first

    assert_equal [Rails.configuration.mail_sender], email.from
    assert_equal [@user.contact_email], email.to
    assert_equal I18n.t('report.subject') % Rails.configuration.site_name, email.subject
    assert_equal @filename[:name], email.attachments[0].filename

    #проверяем удалился ли файл после отправки
    refute File.exists? @generator.zippath
  end

  test "should create task for send report" do
    check_new_task_created('Report::Manager', 'generate_by_date') do
      Report::Manager.delay.generate_by_date(@user, @log.created_at.to_date, @log.created_at.to_date)
    end
  end
end
