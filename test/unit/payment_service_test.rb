# -*- encoding : utf-8 -*-
require 'test_helper'

class PaymentServiceTest < ActiveSupport::TestCase
  def setup
    clear_db
  end

  test "bank exist" do
    assert_nothing_raised do
      assert_not_nil PaymentService.bank
      assert_equal 'bank', PaymentService.bank.internal_name
    end
  end

  test "manual exist" do
    assert_nothing_raised do
      assert_not_nil PaymentService.manual
      assert_equal 'manual', PaymentService.manual.internal_name
    end
  end

  test "robokassa exist" do
    assert_nothing_raised do
      assert_not_nil PaymentService.robokassa
      assert_equal 'robokassa', PaymentService.robokassa.internal_name
    end
  end

end
