# -*- encoding : utf-8 -*-
require 'test_helper'
require 'sidekiq/api'

class SMSServiceTest < ActiveSupport::TestCase
  include PhoneHelper

  def setup
    clear_db

    @user = FactoryGirl.create(:user)
    @user_with_negative_balance = FactoryGirl.create(:user_with_negative_balance)
    @user_with_negative_balance_sender_name_value = FactoryGirl.create(:sender_name_accepted_with_verified_value, user: @user_with_negative_balance).value

    @two_valid_phones = [79379903835, 79879537786]
    @one_valid_phones = [79379903835, 79903836]

    @invalid_phones = [79903835, 793703836, 123]

    @content = 'message'
    @sender_name_value = FactoryGirl.create(:sender_name_accepted_with_verified_value, user: @user).value
    @invalid_sender_name = rand(1000).to_s

    stub_real_request
  end

  test "should send sms for 2 valid phones and not fake" do
    [false, true].each do |fake|
      log = nil
      notification_log = notification('sms.send') do
        log = SMSService.send_sms(@user, @content, {group_ids: @user.group_ids, phones: @two_valid_phones}, @sender_name_value, fake, LogType.manual)

        assert_equal Log.where(user_id: @user.id, content: @content, sender_name_value: @sender_name_value, fake: fake).first, log
        assert_equal @two_valid_phones.sort, log.phones.sort
        assert_equal @user.group_ids.sort, log.group_ids.sort
        assert_equal LogStatus.fresh, log.status
        assert_equal LogType.manual, log.type
        assert_nil log.finished_at
        assert_equal Message.sms_count(@content), log.sms_count
      end

      assert_equal log, notification_log
    end
  end

  test "should send sms for one valid phones and not fake" do
    [false, true].each do |fake|
      log = nil
      notification_log = notification('sms.send') do
        log = SMSService.send_sms(@user, @content, {group_ids: @user.group_ids, phones: @one_valid_phones}, @sender_name_value, fake, LogType.manual)

        assert_equal Log.where(user_id: @user.id, content: @content, sender_name_value: @sender_name_value, fake: fake).first, log
        assert_equal @one_valid_phones.sort, log.phones.sort
        assert_equal @user.group_ids.sort, log.group_ids.sort
        assert_equal LogStatus.fresh, log.status
        assert_equal LogType.manual, log.type
        assert_nil log.finished_at
        assert_equal Message.sms_count(@content), log.sms_count

        #проверяем ценник
        SendLog.where(log_id: log.id) do |send_log|
          assert_not_nil send_log.price
          assert_equal @user.price_per_sms, send_log.price
        end
      end

      assert_equal log, notification_log
    end
  end

  test "should send fresh sms for real" do
    fake = false
    #подготавливаем доставку
    stub_real_request do
      Sidekiq::Testing.inline! do
        log = SMSService.send_sms(@user, @content, {group_ids: @user.group_ids, phones: @two_valid_phones}, @sender_name_value, fake, LogType.manual)
        log.reload

        assert_equal LogStatus.completed, log.status
        assert_not_nil log.finished_at

        assert_equal [], log.check_errors
        assert_equal LogType.manual, log.type
        assert_equal Message.sms_count(@content), log.sms_count

        assert_equal @two_valid_phones.count + @user.clients.count, SendLog.where(log_id: log.id).count
        assert_equal [@two_valid_phones, @user.client_phones].flatten.uniq.sort, log.valid_phones.map { |valid_phone| valid_phone.valid_phone }.sort

        SendLog.where(log_id: log.id) do |send_log|
          assert_equal [], send_log.check_statuses

          assert_not_nil send_log.price
          assert_equal @user.price_per_sms, send_log.price
        end
      end
    end
  end

  test "should not send sms for negative balance" do
    begin_date = Log.where(user_id: @user_with_negative_balance.id).order(:created_at)[0].created_at.to_date
    end_date = Log.where(user_id: @user_with_negative_balance.id).order('created_at DESC')[0].created_at.to_date

    Calculator::Logs.recalculate(begin_date, end_date)

    [true, false].each do |fake|
      log = nil
      assert_difference 'Log.count' do
        notification_log = notification('sms.send') do
          stub_real_request do
            Sidekiq::Testing.inline! do
              log = SMSService.send_sms(@user_with_negative_balance, @content, {group_ids: @user_with_negative_balance.group_ids, phones: @two_valid_phones}, @user_with_negative_balance_sender_name_value, fake, LogType.manual)
              log.reload

              assert_equal LogStatus.check_failed, log.status
              assert_not_nil log.finished_at
              assert_equal 0, SendLog.where(log_id: log.id).count
              assert_equal [I18n.t('sms.failure.balance_too_low')], log.check_errors
              assert_equal [], log.valid_phones
              assert_equal LogType.manual, log.type
              assert_equal Message.sms_count(@content), log.sms_count
            end
          end
        end

        assert_equal log, notification_log
      end
    end
  end

  test "should not send sms with blank content" do
    [true, false].each do |fake|
      [nil, ''].each do |content|
        log = nil
        assert_difference 'Log.count' do
          notification_log = notification('sms.send') do
            stub_real_request do
              Sidekiq::Testing.inline! do
                log = SMSService.send_sms(@user, content, {group_ids: @user.group_ids, phones: @two_valid_phones}, @sender_name_value, fake, LogType.manual)

                log.reload

                assert_equal LogStatus.check_failed, log.status
                assert_not_nil log.finished_at
                assert_equal 0, SendLog.where(log_id: log.id).count
                assert_equal [I18n.t('sms.failure.no_message')], log.check_errors
                assert_equal [], log.valid_phones
                assert_equal LogType.manual, log.type
                assert_equal Message.sms_count(content), log.sms_count
              end
            end
          end

          assert_equal log, notification_log
        end
      end
    end
  end

  test "should not send sms with invalid sender_name" do
    [true, false].each do |fake|
      log = nil
      assert_difference 'Log.count' do
        notification_log = notification('sms.send') do
          stub_real_request do
            Sidekiq::Testing.inline! do
              log = SMSService.send_sms(@user, @content, {group_ids: @user.group_ids, phones: @two_valid_phones}, @invalid_sender_name, fake, LogType.manual)

              log.reload

              assert_equal LogStatus.check_failed, log.status
              assert_not_nil log.finished_at
              assert_equal 0, SendLog.where(log_id: log.id).count
              assert_equal [I18n.t('sms.failure.invalid_sender_name')], log.check_errors
              assert_equal [], log.valid_phones
              assert_equal LogType.manual, log.type
              assert_equal Message.sms_count(@content), log.sms_count
            end
          end
        end

        assert_equal log, notification_log
      end
    end
  end

  test "should not send sms with moderating sender_name" do
    [FactoryGirl.create(:sender_name_moderating_with_verified_value, :user => @user), FactoryGirl.create(:sender_name_moderating_with_not_verified_value, :user => @user)].each do |moderating_sender_name|
      [true, false].each do |fake|
        log = nil
        assert_difference 'Log.count' do
          notification_log = notification('sms.send') do
            stub_real_request do
              Sidekiq::Testing.inline! do
                log = SMSService.send_sms(@user, @content, {group_ids: @user.group_ids, phones: @two_valid_phones}, moderating_sender_name, fake, LogType.manual)

                log.reload

                assert_equal LogStatus.check_failed, log.status
                assert_not_nil log.finished_at
                assert_equal 0, SendLog.where(log_id: log.id).count
                assert_equal [I18n.t('sms.failure.invalid_sender_name')], log.check_errors
                assert_equal [], log.valid_phones
                assert_equal LogType.manual, log.type
                assert_equal Message.sms_count(@content), log.sms_count
              end
            end
          end

          assert_equal log, notification_log
        end
      end
    end
  end

  test "should not send fresh sms with invalid_phones" do
    [true, false].each do |fake|
      log = nil
      assert_difference 'Log.count' do
        notification_log = notification('sms.send') do
          stub_real_request do
            Sidekiq::Testing.inline! do
              log = SMSService.send_sms(@user, @content, {phones: @invalid_phones}, @sender_name_value, fake, LogType.manual)

              log.reload

              assert_equal LogStatus.check_failed, log.status
              assert_not_nil log.finished_at
              assert_equal 0, SendLog.where(log_id: log.id).count
              assert_equal [I18n.t('sms.failure.no_phones')], log.check_errors
              assert_equal [], log.valid_phones
              assert_equal LogType.manual, log.type
              assert_equal Message.sms_count(@content), log.sms_count
            end
          end
        end

        assert_equal log, notification_log
      end
    end
  end

  #different queues
  test "should use api queue for log with api type" do
    log = FactoryGirl.create(:log_fresh, user: @user, type: LogType.api, phones: [79379903835], fake: false, sender_name_value: @sender_name_value)
    #подготавливаем доставку
    stub_real_request do
      $redis.flushall
      Sidekiq::Testing.disable! do
        refute Sidekiq::Stats.new.queues.has_key?("api")
        log.process
        assert Sidekiq::Stats.new.queues.has_key?("api")
        assert_equal 1, Sidekiq::Stats.new.queues["api"]
      end
    end
  end

  private
  def notification(notification)
    payload = nil
    subscription = ActiveSupport::Notifications.subscribe notification do |name, start, finish, id, _payload|
      payload = _payload[:log]
    end

    yield

    ActiveSupport::Notifications.unsubscribe(subscription)

    return payload
  end

end
