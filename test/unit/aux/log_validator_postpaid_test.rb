# -*- encoding : utf-8 -*-
require 'test_helper'

class Aux::LogValidatorPostpaidTest < ActiveSupport::TestCase
  def setup
    clear_db

    user = FactoryGirl.create(:user_postpaid_with_negative_balance)
    @log_fresh_with_user_negative_balance = FactoryGirl.create(:log_fresh_valid,
                                                               user: user,
                                                               group_ids: user.groups.pluck(:id),
                                                               phones: [],
                                                               sender_name_value: FactoryGirl.create(:sender_name_accepted_with_verified_value, user: user).value
    )

    @log_fresh_with_moderating_sender_name = FactoryGirl.create(:log_fresh_valid,
                                                                user: user,
                                                                sender_name_value: FactoryGirl.create(:sender_name_moderating, user: user).value
    )
  end

  test "should be valid for log-fresh-with-user-postpaid-with-negative-balance" do
    recalculate_logs(@log_fresh_with_user_negative_balance.user) do
      assert_operator @log_fresh_with_user_negative_balance.user.current_balance, :<, 0

      log_validator = Aux::LogValidator.new(@log_fresh_with_user_negative_balance)

      assert log_validator.log_valid?
      assert_equal [], @log_fresh_with_user_negative_balance.check_errors
      assert_equal @log_fresh_with_user_negative_balance.user.client_phones.sort, @log_fresh_with_user_negative_balance.valid_phones.map { |valid_phone| valid_phone.valid_phone }.sort
    end
  end

  #invalid
  test "should be invalid for log-fresh-moderating-sender-name" do
    log_validator = Aux::LogValidator.new(@log_fresh_with_moderating_sender_name)
    refute log_validator.log_valid?
    assert_equal [I18n.t('sms.failure.invalid_sender_name')], @log_fresh_with_moderating_sender_name.check_errors
    assert_equal [], @log_fresh_with_moderating_sender_name.valid_phones
  end
end
