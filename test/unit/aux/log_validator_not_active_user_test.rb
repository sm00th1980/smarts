# -*- encoding : utf-8 -*-
require 'test_helper'

class Aux::LogValidatorNotActiveUserTest < ActiveSupport::TestCase
  include PhoneHelper

  def setup
    clear_db

    #blocked
    user_blocked = FactoryGirl.create(:user_blocked)
    @log_blocked = FactoryGirl.create(:log_fresh_valid,
                                          user: user_blocked,
                                          sender_name_value: FactoryGirl.create(:sender_name_accepted_with_verified_value, user: user_blocked).value
    )

    #deleted
    user_deleted = FactoryGirl.create(:user_deleted)
    @log_deleted = FactoryGirl.create(:log_fresh_valid,
                                      user: user_deleted,
                                      sender_name_value: FactoryGirl.create(:sender_name_accepted_with_verified_value, user: user_deleted).value
    )

  end

  #blocked
  test "should be invalid for log-fresh when user is blocked" do
    log_validator = Aux::LogValidator.new(@log_blocked)
    refute log_validator.log_valid?
    assert_equal [I18n.t('sms.failure.invalid_user')], @log_blocked.check_errors
    assert_equal [], @log_blocked.valid_phones
  end

  #deleted
  test "should be invalid for log-fresh when user is deleted" do
    log_validator = Aux::LogValidator.new(@log_deleted)
    refute log_validator.log_valid?
    assert_equal [I18n.t('sms.failure.invalid_user')], @log_deleted.check_errors
    assert_equal [], @log_deleted.valid_phones
  end
end
