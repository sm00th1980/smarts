# -*- encoding : utf-8 -*-
require 'test_helper'

class Aux::LogValidatorPrepaidTest < ActiveSupport::TestCase
  include PhoneHelper

  def setup
    clear_db

    user = FactoryGirl.create(:user)

    #valid
    @log_fresh_valid = FactoryGirl.create(:log_fresh_valid,
                                          user: user,
                                          sender_name_value: FactoryGirl.create(:sender_name_accepted_with_verified_value, user: user).value
    )

    @log_fresh_valid_with_groups = FactoryGirl.create(:log_fresh_valid,
                                                      user: user,
                                                      group_ids: user.groups.pluck(:id),
                                                      phones: [],
                                                      sender_name_value: FactoryGirl.create(:sender_name_accepted_with_verified_value, user: user).value
    )

    @log_fresh_valid_with_groups_and_phones = FactoryGirl.create(:log_fresh_valid,
                                                                 user: user,
                                                                 group_ids: user.groups.pluck(:id),
                                                                 sender_name_value: FactoryGirl.create(:sender_name_accepted_with_verified_value, user: user).value
    )

    #invalid
    @log_fresh_with_invalid_sender_name = FactoryGirl.create(:log_fresh_valid,
                                                             user: user,
                                                             sender_name_value: nil
    )

    @log_fresh_with_invalid_content = FactoryGirl.create(:log_fresh_valid,
                                                         user: user,
                                                         content: nil,
                                                         sender_name_value: FactoryGirl.create(:sender_name_accepted_with_verified_value, user: user).value
    )

    user_with_negative_balance = FactoryGirl.create(:user_with_negative_balance)
    @log_fresh_with_user_negative_balance = FactoryGirl.create(:log_fresh_valid,
                                                               user: user_with_negative_balance,
                                                               sender_name_value: FactoryGirl.create(:sender_name_accepted_with_verified_value, user: user_with_negative_balance).value
    )

    @log_fresh_without_phones = FactoryGirl.create(:log_fresh_valid,
                                                   user: user,
                                                   phones: [],
                                                   sender_name_value: FactoryGirl.create(:sender_name_accepted_with_verified_value, user: user).value
    )

    user2 = FactoryGirl.create(:user)
    @log_with_other_groups = FactoryGirl.create(:log_fresh_valid,
                                                user: user2,
                                                group_ids: user.groups.pluck(:id),
                                                phones: [],
                                                sender_name_value: FactoryGirl.create(:sender_name_accepted_with_verified_value, user: user2).value
    )

    @log_fresh_with_moderating_sender_name = FactoryGirl.create(:log_fresh_valid,
                                                                user: user,
                                                                sender_name_value: FactoryGirl.create(:sender_name_moderating, user: user).value
    )
  end

  #valid
  test "should be valid for log-fresh-valid" do
    log_validator = Aux::LogValidator.new(@log_fresh_valid)
    assert log_validator.log_valid?
    assert_equal [], @log_fresh_valid.check_errors
    assert_equal @log_fresh_valid.phones.map { |p| normalize_phone_number(p) }.sort, @log_fresh_valid.valid_phones.map { |valid_phone| valid_phone.valid_phone }.sort
  end

  test "should be valid for log-fresh-valid-with-groups" do
    log_validator = Aux::LogValidator.new(@log_fresh_valid_with_groups)
    assert log_validator.log_valid?
    assert_equal [], @log_fresh_valid_with_groups.check_errors
    assert_equal @log_fresh_valid_with_groups.user.client_phones.sort, @log_fresh_valid_with_groups.valid_phones.map { |valid_phone| valid_phone.valid_phone }.sort
  end

  test "should be valid for log-fresh-valid-with-groups-and-phones" do
    phones = [@log_fresh_valid_with_groups_and_phones.user.client_phones, @log_fresh_valid_with_groups_and_phones.phones].flatten.uniq.map { |p| normalize_phone_number(p) }.sort

    log_validator = Aux::LogValidator.new(@log_fresh_valid_with_groups_and_phones)
    assert log_validator.log_valid?
    assert_equal [], @log_fresh_valid_with_groups_and_phones.check_errors
    assert_equal phones, @log_fresh_valid_with_groups_and_phones.valid_phones.map { |valid_phone| valid_phone.valid_phone }.sort
  end

  #invalid
  test "should be invalid for log-fresh-invalid-sender-name" do
    log_validator = Aux::LogValidator.new(@log_fresh_with_invalid_sender_name)
    refute log_validator.log_valid?
    assert_equal [I18n.t('sms.failure.invalid_sender_name')], @log_fresh_with_invalid_sender_name.check_errors
    assert_equal [], @log_fresh_with_invalid_sender_name.valid_phones
  end

  test "should be invalid for log-fresh-invalid-content" do
    log_validator = Aux::LogValidator.new(@log_fresh_with_invalid_content)
    refute log_validator.log_valid?
    assert_equal [I18n.t('sms.failure.no_message')], @log_fresh_with_invalid_content.check_errors
    assert_equal [], @log_fresh_with_invalid_content.valid_phones
  end

  test "should be invalid for log-fresh-with-user-with-negative-balance" do
    recalculate_logs(@log_fresh_with_user_negative_balance.user) do
      phones_count = @log_fresh_with_user_negative_balance.phones.count

      log_validator = Aux::LogValidator.new(@log_fresh_with_user_negative_balance)

      refute log_validator.log_valid?
      assert_equal [I18n.t('sms.failure.balance_too_low')], @log_fresh_with_user_negative_balance.check_errors
      assert_equal [], @log_fresh_with_user_negative_balance.valid_phones
    end
  end

  test "should be invalid for log-fresh-without-phones" do
    log_validator = Aux::LogValidator.new(@log_fresh_without_phones)
    refute log_validator.log_valid?
    assert_equal [I18n.t('sms.failure.no_phones')], @log_fresh_without_phones.check_errors
    assert_equal [], @log_fresh_without_phones.valid_phones
  end

  test "should be invalid for log-fresh-with-other-groups" do
    log_validator = Aux::LogValidator.new(@log_with_other_groups)
    refute log_validator.log_valid?
    assert_equal [I18n.t('sms.failure.no_phones')], @log_with_other_groups.check_errors
    assert_equal [], @log_with_other_groups.valid_phones
  end

  test "should be invalid for log-fresh-moderating-sender-name" do
    log_validator = Aux::LogValidator.new(@log_fresh_with_moderating_sender_name)
    refute log_validator.log_valid?
    assert_equal [I18n.t('sms.failure.invalid_sender_name')], @log_fresh_with_moderating_sender_name.check_errors
    assert_equal [], @log_fresh_with_moderating_sender_name.valid_phones
  end
end
