# -*- encoding : utf-8 -*-
require 'test_helper'

class Aux::LogValidatorInvalidSenderNameTest < ActiveSupport::TestCase
  include PhoneHelper

  def setup
    clear_db
    valid_phone = '79379903835'

    @log_with_invalid_sender_name = FactoryGirl.create(:log_fresh,
                                                       user: FactoryGirl.create(:user_with_negative_balance, payment_type: PaymentType.postpaid),
                                                       phones: [valid_phone],
                                                       sender_name_value: nil
    )

    user = FactoryGirl.create(:user_with_negative_balance, payment_type: PaymentType.postpaid)
    @log_with_rejected_sender_name = FactoryGirl.create(:log_fresh,
                                                   user: user,
                                                   phones: [valid_phone],
                                                   sender_name_value: FactoryGirl.create(:sender_name_rejected_with_verified_value, user: user).value
    )

    user = FactoryGirl.create(:user_with_negative_balance, payment_type: PaymentType.postpaid)
    @log_with_moderating_sender_name = FactoryGirl.create(:log_fresh,
                                                        user: user,
                                                        phones: [valid_phone],
                                                        sender_name_value: FactoryGirl.create(:sender_name_moderating_with_verified_value, user: user).value
    )

    @log_with_not_own_sender_name = FactoryGirl.create(:log_fresh,
                                                                    user: FactoryGirl.create(:user_with_negative_balance, payment_type: PaymentType.postpaid),
                                                                    phones: [valid_phone],
                                                                    sender_name_value: FactoryGirl.create(:sender_name_accepted_with_verified_value, user: FactoryGirl.create(:user)).value,
    )
  end

  test "log should be invalid for log with invalid sender_name" do
    recalculate_logs(@log_with_invalid_sender_name.user) do
      log_validator = Aux::LogValidator.new(@log_with_invalid_sender_name)
      refute log_validator.log_valid?

      @log_with_invalid_sender_name.reload

      assert_equal [I18n.t('sms.failure.invalid_sender_name')], @log_with_invalid_sender_name.check_errors
    end
  end

  test "log should be invalid for log with rejected sender name" do
    recalculate_logs(@log_with_rejected_sender_name.user) do
      log_validator = Aux::LogValidator.new(@log_with_rejected_sender_name)
      refute log_validator.log_valid?

      @log_with_rejected_sender_name.reload

      assert_equal [I18n.t('sms.failure.invalid_sender_name')], @log_with_rejected_sender_name.check_errors
    end
  end

  test "log should be invalid for log with moderating sender name" do
    recalculate_logs(@log_with_moderating_sender_name.user) do
      log_validator = Aux::LogValidator.new(@log_with_moderating_sender_name)
      refute log_validator.log_valid?

      @log_with_moderating_sender_name.reload

      assert_equal [I18n.t('sms.failure.invalid_sender_name')], @log_with_moderating_sender_name.check_errors
    end
  end

  test "log should be invalid for log with not own sender name" do
    recalculate_logs(@log_with_not_own_sender_name.user) do
      log_validator = Aux::LogValidator.new(@log_with_not_own_sender_name)
      refute log_validator.log_valid?

      @log_with_not_own_sender_name.reload

      assert_equal [I18n.t('sms.failure.invalid_sender_name')], @log_with_not_own_sender_name.check_errors
    end
  end

end
