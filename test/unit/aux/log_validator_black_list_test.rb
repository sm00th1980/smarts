# -*- encoding : utf-8 -*-
require 'test_helper'

class Aux::LogValidatorBlackListTest < ActiveSupport::TestCase
  def setup
    clear_db

    #global
    @global_black_listed_phone = FactoryGirl.create(:black_list, user: FactoryGirl.create(:admin)).phone.to_s
    @normal_phone = Test::Helpers.random_normal_phone_number

    #personal
    @user = FactoryGirl.create(:user_with_black_lists)
    @personal_black_listed_phone = @user.black_listed_phones.first.phone.to_s
  end

  #global black list
  test "should not pass global black-listed in phones" do
    user = FactoryGirl.create(:user)
    sender_name_value = FactoryGirl.create(:sender_name_accepted_with_verified_value, user: user).value
    log = FactoryGirl.create(:log_fresh_valid, user: user, group_ids: [], phones: [@global_black_listed_phone, @normal_phone], sender_name_value: sender_name_value)

    log_validator = Aux::LogValidator.new(log)

    assert log_validator.log_valid?
    valid_phones = log.valid_phones.map{|valid_phone| valid_phone.valid_phone.to_s}.sort

    refute valid_phones.include? @global_black_listed_phone
  end

  test "should not pass global black-listed in groups" do
    user = FactoryGirl.create(:user)
    sender_name_value = FactoryGirl.create(:sender_name_accepted_with_verified_value, user: user).value
    group = FactoryGirl.create(:group, user: user)
    client = FactoryGirl.create(:client, group: group, phone: @global_black_listed_phone)

    log = FactoryGirl.create(:log_fresh_valid, user: user, group_ids: [group.id], phones: [@normal_phone], sender_name_value: sender_name_value)

    log_validator = Aux::LogValidator.new(log)

    assert log_validator.log_valid?
    valid_phones = log.valid_phones.map { |valid_phone| valid_phone.valid_phone.to_s }.sort

    refute valid_phones.include? @global_black_listed_phone
  end

  #personal black list
  test "should not pass personal black-listed in phones" do
    sender_name_value = FactoryGirl.create(:sender_name_accepted_with_verified_value, user: @user).value
    log = FactoryGirl.create(:log_fresh_valid, user: @user, group_ids: [], phones: [@personal_black_listed_phone, @normal_phone], sender_name_value: sender_name_value)

    log_validator = Aux::LogValidator.new(log)

    assert log_validator.log_valid?
    valid_phones = log.valid_phones.map{|valid_phone| valid_phone.valid_phone.to_s}.sort

    refute valid_phones.include? @personal_black_listed_phone
  end

  test "should not pass personal black-listed in groups" do
    sender_name_value = FactoryGirl.create(:sender_name_accepted_with_verified_value, user: @user).value
    group = FactoryGirl.create(:group, user: @user)
    client = FactoryGirl.create(:client, group: group, phone: @personal_black_listed_phone)

    log = FactoryGirl.create(:log_fresh_valid, user: @user, group_ids: [group.id], phones: [@normal_phone], sender_name_value: sender_name_value)

    log_validator = Aux::LogValidator.new(log)

    assert log_validator.log_valid?
    valid_phones = log.valid_phones.map { |valid_phone| valid_phone.valid_phone.to_s }.sort

    refute valid_phones.include? @personal_black_listed_phone
  end

  #not own personal black list
  test "should pass not his own personal black-listed in phones" do
    user = FactoryGirl.create(:user)
    sender_name_value = FactoryGirl.create(:sender_name_accepted_with_verified_value, user: user).value

    log = FactoryGirl.create(:log_fresh_valid, user: user, group_ids: [], phones: [@personal_black_listed_phone], sender_name_value: sender_name_value)

    log_validator = Aux::LogValidator.new(log)

    assert log_validator.log_valid?
    valid_phones = log.valid_phones.map{|valid_phone| valid_phone.valid_phone.to_s}.sort

    assert valid_phones.include? @personal_black_listed_phone
  end

  test "should pass not his own personal black-listed in groups" do
    user = FactoryGirl.create(:user)
    sender_name_value = FactoryGirl.create(:sender_name_accepted_with_verified_value, user: user).value
    group = FactoryGirl.create(:group, user: user)
    client = FactoryGirl.create(:client, group: group, phone: @personal_black_listed_phone)

    log = FactoryGirl.create(:log_fresh_valid, user: user, group_ids: [group.id], phones: [], sender_name_value: sender_name_value)

    log_validator = Aux::LogValidator.new(log)

    assert log_validator.log_valid?
    valid_phones = log.valid_phones.map { |valid_phone| valid_phone.valid_phone.to_s }.sort

    assert valid_phones.include? @personal_black_listed_phone
  end
end
