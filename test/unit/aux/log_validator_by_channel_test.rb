# -*- encoding : utf-8 -*-
require 'test_helper'

class Aux::LogValidatorByChannelTest < ActiveSupport::TestCase
  include PhoneHelper

  def setup
    clear_db

    user_prepaid = FactoryGirl.create(:user_with_negative_balance, payment_type: PaymentType.prepaid)
    sender_name_value_prepaid = FactoryGirl.create(:sender_name_accepted_with_verified_value, user: user_prepaid).value

    user_postpaid = FactoryGirl.create(:user_with_negative_balance, payment_type: PaymentType.postpaid)
    sender_name_value_postpaid = FactoryGirl.create(:sender_name_accepted_with_verified_value, user: user_postpaid).value

    valid_phone = '79379903835'

    @log_prepaid  = FactoryGirl.create(:log_fresh, user: user_prepaid,  phones: [valid_phone], sender_name_value: sender_name_value_prepaid)
    @log_postpaid = FactoryGirl.create(:log_fresh, user: user_postpaid, phones: [valid_phone], sender_name_value: sender_name_value_postpaid)
  end

  test "log should be valid for postpaid user with low balance" do
    recalculate_logs(@log_postpaid.user) do
      log_validator = Aux::LogValidator.new(@log_postpaid)

      assert log_validator.log_valid?
      @log_postpaid.reload
      assert_equal [], @log_postpaid.check_errors


      total_summa = 0
      @log_postpaid.phones.each do |phone|
        _channel = Channel.channel(@log_postpaid.sender_name_value, @log_postpaid.user.alpha_megafon_channel, phone)
        total_summa += Message.sms_count(@log_postpaid.content) * Message.price_per_sms(_channel, @log_postpaid.user, phone)
      end
      assert_equal log_validator.total_summa, total_summa
    end
  end

  test "log should be invalid for prepaid user with low balance" do
    recalculate_logs(@log_prepaid.user) do
      log_validator = Aux::LogValidator.new(@log_prepaid)
      refute log_validator.log_valid?

      @log_prepaid.reload

      assert_equal [I18n.t('sms.failure.balance_too_low')], @log_prepaid.check_errors

      total_summa = 0
      @log_prepaid.phones.each do |phone|
        _channel = Channel.channel(@log_prepaid.sender_name_value, @log_prepaid.user.alpha_megafon_channel, phone)
        total_summa += Message.sms_count(@log_prepaid.content) * Message.price_per_sms(_channel, @log_prepaid.user, phone)
      end
      assert_equal log_validator.total_summa, total_summa
    end
  end

end
