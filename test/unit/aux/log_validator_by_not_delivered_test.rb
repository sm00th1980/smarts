# -*- encoding : utf-8 -*-
require 'test_helper'

class Aux::LogValidatorByNotDeliveredTest < ActiveSupport::TestCase
  include PhoneHelper

  def setup
    clear_db

    user = FactoryGirl.create(:user)
    @log = FactoryGirl.create(:log_with_not_delivered,
                              user: user,
                              sender_name_value: FactoryGirl.create(:sender_name_accepted_with_verified_value, user: user).value
    )

    @user_postpaid_with_negative_balance = FactoryGirl.create(:user_postpaid_with_negative_balance)
    @user_prepaid_with_negative_balance = FactoryGirl.create(:user_with_negative_balance)

    user2 = FactoryGirl.create(:user)
    @log_with_all_delivered = FactoryGirl.create(:log_with_all_delivered,
                                                 user: user2,
                                                 sender_name_value: FactoryGirl.create(:sender_name_accepted_with_verified_value, user: user2).value
    )

    user3 = FactoryGirl.create(:user)
    @log_without_phones = FactoryGirl.create(:log_without_phones,
                                             user: user3,
                                             sender_name_value: FactoryGirl.create(:sender_name_accepted_with_verified_value, user: user3).value
    )
  end

  test "should validate new log by not delivered" do
    new_log_by_not_delivered = FactoryGirl.create(:log_fresh, user: @log.user, sender_name_value: @log.sender_name_value, content: @log.content, phones: @log.phones, group_ids: @log.group_ids, type: LogType.by_not_delivered, prev_log_id: @log.id)

    assert new_log_by_not_delivered.by_not_delivered?

    log_validator = Aux::LogValidator.new(new_log_by_not_delivered)

    assert log_validator.log_valid?
    assert_equal SendLog.not_delivered([@log.id]).map { |send_log| normalize_phone_number(send_log.phone).to_i }.sort, new_log_by_not_delivered.valid_phones.map { |valid_phone| valid_phone.valid_phone.to_i }.sort
  end

  test "should validate new log by not delivered for postpaid user with negative balance" do
    recalculate_logs(@user_postpaid_with_negative_balance) do
      assert_operator @user_postpaid_with_negative_balance.current_balance, :<, 0
      new_log_by_not_delivered = FactoryGirl.create(:log_fresh,
                                                    user: @user_postpaid_with_negative_balance,
                                                    sender_name_value: FactoryGirl.create(:sender_name_accepted_with_verified_value, user: @user_postpaid_with_negative_balance).value,
                                                    content: @log.content,
                                                    phones: @log.phones,
                                                    group_ids: @log.group_ids,
                                                    type: LogType.by_not_delivered,
                                                    prev_log_id: @log.id
      )

      assert new_log_by_not_delivered.by_not_delivered?

      log_validator = Aux::LogValidator.new(new_log_by_not_delivered)

      assert log_validator.log_valid?
      assert_equal SendLog.not_delivered([@log.id]).map { |send_log| normalize_phone_number(send_log.phone).to_i }.sort, new_log_by_not_delivered.valid_phones.map { |valid_phone| valid_phone.valid_phone.to_i }.sort
      assert_equal [], new_log_by_not_delivered.check_errors
    end
  end

  test "should validate new log by not delivered based on log-by-not-delivered" do
    user = FactoryGirl.create(:user)
    sender_name_value = FactoryGirl.create(:sender_name_accepted_with_verified_value, user: user).value
    base_log = FactoryGirl.create(:base_log_with_all_not_delivered, user: user, sender_name_value: sender_name_value)

    assert_equal 3, base_log.send_logs.size

    base_log.send_logs.each do |send_log|
      refute send_log.delivered?
    end

    log_by_not_delivered1 = FactoryGirl.create(:log_completed,
                                               user: base_log.user,
                                               sender_name_value: base_log.sender_name_value,
                                               content: base_log.content,
                                               phones: base_log.phones,
                                               group_ids: base_log.group_ids,
                                               type: LogType.by_not_delivered,
                                               prev_log_id: base_log.id)

    deliver_first_sms(log_by_not_delivered1)

    log_by_not_delivered2 = FactoryGirl.create(:log_completed,
                                               user: base_log.user,
                                               sender_name_value: base_log.sender_name_value,
                                               content: base_log.content,
                                               phones: base_log.phones,
                                               group_ids: base_log.group_ids,
                                               type: LogType.by_not_delivered,
                                               prev_log_id: log_by_not_delivered1.id)

    deliver_second_sms(log_by_not_delivered2)

    log_by_not_delivered3 = FactoryGirl.create(:log_fresh,
                                               user: base_log.user,
                                               sender_name_value: base_log.sender_name_value,
                                               content: base_log.content,
                                               phones: base_log.phones,
                                               group_ids: base_log.group_ids,
                                               type: LogType.by_not_delivered,
                                               prev_log_id: log_by_not_delivered2.id)

    assert log_by_not_delivered3.by_not_delivered?

    log_validator = Aux::LogValidator.new(log_by_not_delivered3)

    assert log_validator.log_valid?
    assert_equal SendLog.not_delivered([log_by_not_delivered2.id]).map { |send_log| normalize_phone_number(send_log.phone).to_i }.sort, log_by_not_delivered3.valid_phones.map { |valid_phone| valid_phone.valid_phone.to_i }.sort
  end

  test "should validate new log by not delivered based on log-with-one-delivered" do
    user = FactoryGirl.create(:user)
    sender_name_value = FactoryGirl.create(:sender_name_accepted_with_verified_value, user: user).value
    base_log = FactoryGirl.create(:base_log_with_first_delivered, user: user, sender_name_value: sender_name_value)

    assert_equal 4, base_log.send_logs.size

    log_by_not_delivered1 = FactoryGirl.create(:log_completed,
                                               user: base_log.user,
                                               sender_name_value: base_log.sender_name_value,
                                               content: base_log.content,
                                               phones: base_log.phones,
                                               group_ids: base_log.group_ids,
                                               type: LogType.by_not_delivered,
                                               prev_log_id: base_log.id)

    deliver_first_sms2(log_by_not_delivered1)

    log_by_not_delivered2 = FactoryGirl.create(:log_completed,
                                               user: base_log.user,
                                               sender_name_value: base_log.sender_name_value,
                                               content: base_log.content,
                                               phones: base_log.phones,
                                               group_ids: base_log.group_ids,
                                               type: LogType.by_not_delivered,
                                               prev_log_id: log_by_not_delivered1.id)

    deliver_second_sms2(log_by_not_delivered2)

    log_by_not_delivered3 = FactoryGirl.create(:log_fresh,
                                               user: base_log.user,
                                               sender_name_value: base_log.sender_name_value,
                                               content: base_log.content,
                                               phones: base_log.phones,
                                               group_ids: base_log.group_ids,
                                               type: LogType.by_not_delivered,
                                               prev_log_id: log_by_not_delivered2.id)

    assert log_by_not_delivered3.by_not_delivered?

    log_validator = Aux::LogValidator.new(log_by_not_delivered3)

    assert log_validator.log_valid?
    assert_equal SendLog.not_delivered([log_by_not_delivered2.id]).map { |send_log| normalize_phone_number(send_log.phone).to_i }.sort, log_by_not_delivered3.valid_phones.map { |valid_phone| valid_phone.valid_phone.to_i }.sort
  end

  #invalid
  test "should not validate new log by not delivered for prepaid user with negative balance" do
    sender_name_value = FactoryGirl.create(:sender_name_accepted_with_verified_value, user: @user_prepaid_with_negative_balance).value

    recalculate_logs(@user_prepaid_with_negative_balance) do
      assert_operator @user_prepaid_with_negative_balance.current_balance, :<, 0
      new_log_by_not_delivered = FactoryGirl.create(:log_fresh, user: @user_prepaid_with_negative_balance, sender_name_value: sender_name_value, content: @log.content, phones: @log.phones, group_ids: @log.group_ids, type: LogType.by_not_delivered, prev_log_id: @log.id)

      assert new_log_by_not_delivered.by_not_delivered?

      log_validator = Aux::LogValidator.new(new_log_by_not_delivered)

      refute log_validator.log_valid?
      assert_equal [], new_log_by_not_delivered.valid_phones.map { |valid_phone| valid_phone.valid_phone.to_i }

      assert_equal [I18n.t('sms.failure.balance_too_low')], new_log_by_not_delivered.check_errors
    end
  end

  test "should not validate new log by not delivered with invalid-sender-name" do
    new_log_by_not_delivered = FactoryGirl.create(:log_fresh, user: @log.user, sender_name_value: nil, content: @log.content, phones: @log.phones, group_ids: @log.group_ids, type: LogType.by_not_delivered, prev_log_id: @log.id)

    assert new_log_by_not_delivered.by_not_delivered?

    log_validator = Aux::LogValidator.new(new_log_by_not_delivered)

    refute log_validator.log_valid?
    assert_equal [], new_log_by_not_delivered.valid_phones.map { |valid_phone| valid_phone.valid_phone.to_i }

    assert_equal [I18n.t('sms.failure.invalid_sender_name')], new_log_by_not_delivered.check_errors
  end

  test "should not validate new log by not delivered with invalid-content" do
    sender_name_value = FactoryGirl.create(:sender_name_accepted_with_verified_value, user: @log.user).value
    new_log_by_not_delivered = FactoryGirl.create(:log_fresh, user: @log.user, sender_name_value: sender_name_value, content: '', phones: @log.phones, group_ids: @log.group_ids, type: LogType.by_not_delivered, prev_log_id: @log.id)

    assert new_log_by_not_delivered.by_not_delivered?

    log_validator = Aux::LogValidator.new(new_log_by_not_delivered)

    refute log_validator.log_valid?
    assert_equal [], new_log_by_not_delivered.valid_phones.map { |valid_phone| valid_phone.valid_phone.to_i }

    assert_equal [I18n.t('sms.failure.no_message')], new_log_by_not_delivered.check_errors
  end

  test "should not validate new log by not delivered without phones" do
    sender_name_value = FactoryGirl.create(:sender_name_accepted_with_verified_value, user: @log.user).value
    new_log_by_not_delivered = FactoryGirl.create(:log_fresh, user: @log.user, sender_name_value: sender_name_value, content: @log.content, phones: [], group_ids: [], type: LogType.by_not_delivered, prev_log_id: @log_without_phones.id)

    assert new_log_by_not_delivered.by_not_delivered?

    log_validator = Aux::LogValidator.new(new_log_by_not_delivered)

    refute log_validator.log_valid?
    assert_equal [], new_log_by_not_delivered.valid_phones.map { |valid_phone| valid_phone.valid_phone.to_i }

    assert_equal [I18n.t('sms.failure.no_phones')], new_log_by_not_delivered.check_errors
  end

  test "should not validate new log by not delivered for log with all delivered" do
    @log_with_all_delivered.send_logs.each do |send_log|
      assert send_log.delivered?
    end

    new_log_by_not_delivered = FactoryGirl.create(:log_fresh,
                                                  user: @log_with_all_delivered.user,
                                                  sender_name_value: @log_with_all_delivered.sender_name_value,
                                                  content: @log_with_all_delivered.content,
                                                  phones: @log_with_all_delivered.phones,
                                                  group_ids: @log_with_all_delivered.group_ids,
                                                  type: LogType.by_not_delivered,
                                                  prev_log_id: @log_with_all_delivered.id
    )

    assert new_log_by_not_delivered.by_not_delivered?

    log_validator = Aux::LogValidator.new(new_log_by_not_delivered)

    refute log_validator.log_valid?
    assert_equal [], new_log_by_not_delivered.valid_phones.map { |valid_phone| valid_phone.valid_phone.to_i }

    assert_equal [I18n.t('sms.failure.no_phones')], new_log_by_not_delivered.check_errors
  end

  private
  def deliver_first_sms(log)
    _channel1, _channel2, _channel3 = [log.phones[0], log.phones[1], log.phones[2]].map{|phone| Channel.channel(log.sender_name_value, log.user.alpha_megafon_channel, phone)}

    FactoryGirl.create(:send_log, log: log, channel: _channel1, price: 0.50, phone: log.phones[0], delivery_status: SendLogDeliveryStatus.delivered, :delivery_error => SendLogDeliveryError.bronze_no_error)
    FactoryGirl.create(:send_log, log: log, channel: _channel2, price: 0.05, phone: log.phones[1], delivery_status: SendLogDeliveryStatus.rejected, :delivery_error => SendLogDeliveryError.bronze_error_call_barred)
    FactoryGirl.create(:send_log, log: log, channel: _channel3, price: 0.05, phone: log.phones[2], delivery_status: SendLogDeliveryStatus.rejected, :delivery_error => SendLogDeliveryError.bronze_error_call_barred)
  end

  def deliver_second_sms(log)
    _channel1, _channel2 = [log.phones[1], log.phones[2]].map{|phone| Channel.channel(log.sender_name_value, log.user.alpha_megafon_channel, phone)}

    FactoryGirl.create(:send_log, log: log, channel: _channel1, price: 0.05, phone: log.phones[1], delivery_status: SendLogDeliveryStatus.delivered, :delivery_error => SendLogDeliveryError.bronze_no_error)
    FactoryGirl.create(:send_log, log: log, channel: _channel2, price: 0.05, phone: log.phones[2], delivery_status: SendLogDeliveryStatus.rejected, :delivery_error => SendLogDeliveryError.bronze_error_call_barred)
  end

  def deliver_first_sms2(log)
    _channel1, _channel2, _channel3 = [log.phones[1], log.phones[2], log.phones[3]].map{|phone| Channel.channel(log.sender_name_value, log.user.alpha_megafon_channel, phone)}

    FactoryGirl.create(:send_log, log: log, channel: _channel1, price: 0.50, phone: log.phones[1], delivery_status: SendLogDeliveryStatus.delivered, :delivery_error => SendLogDeliveryError.bronze_no_error)
    FactoryGirl.create(:send_log, log: log, channel: _channel2, price: 0.05, phone: log.phones[2], delivery_status: SendLogDeliveryStatus.rejected, :delivery_error => SendLogDeliveryError.bronze_error_call_barred)
    FactoryGirl.create(:send_log, log: log, channel: _channel3, price: 0.05, phone: log.phones[3], delivery_status: SendLogDeliveryStatus.rejected, :delivery_error => SendLogDeliveryError.bronze_error_call_barred)
  end

  def deliver_second_sms2(log)
    _channel1, _channel2 = [log.phones[2], log.phones[3]].map{|phone| Channel.channel(log.sender_name_value, log.user.alpha_megafon_channel, phone)}

    FactoryGirl.create(:send_log, log: log, channel: _channel1, price: 0.05, phone: log.phones[2], delivery_status: SendLogDeliveryStatus.delivered, :delivery_error => SendLogDeliveryError.bronze_no_error)
    FactoryGirl.create(:send_log, log: log, channel: _channel2, price: 0.05, phone: log.phones[3], delivery_status: SendLogDeliveryStatus.rejected, :delivery_error => SendLogDeliveryError.bronze_error_call_barred)
  end

end
