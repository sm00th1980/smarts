# -*- encoding : utf-8 -*-
require 'test_helper'

class CellOperatorGroup::RejectTest < ActiveSupport::TestCase
  def setup
    clear_db

    @phones = {
        mts: 79198015896,
        beeline: 79657250000,
        megafon: 79379903835,
        tele2: 79005910000,
        baikal: 79016300000,
        ural: 79012200000,
        enisey: 79011110000,
        motiv: 79000300000,
        nss: 79003100000,
        cdma: 79011570000,
        other: 79585360000,
        smarts: 79022900000
    }

    @user_rejected_to_megafon = FactoryGirl.create(:user_with_reject_to_megafon)
    @user_rejected_to_mts = FactoryGirl.create(:user_with_reject_to_mts)
    @user_rejected_to_smarts = FactoryGirl.create(:user_with_reject_to_smarts_via_gold_channel)

    @channels = Channel.all
  end

  test "should reject to megafon for megafon rejected user" do
    user = @user_rejected_to_megafon
    operator = :megafon

    check(user, operator)
  end

  test "should reject to mts for mts rejected user" do
    user = @user_rejected_to_mts
    operator = :mts

    check(user, operator)
  end

  test "should reject to smarts via gold channel for smarts rejected user" do
    assert CellOperatorGroup.reject?(@user_rejected_to_smarts, Channel.gold, @phones[:smarts])
    refute CellOperatorGroup.reject?(@user_rejected_to_smarts, Channel.bronze, @phones[:smarts])
    refute CellOperatorGroup.reject?(@user_rejected_to_smarts, Channel.megafon_fixed, @phones[:smarts])
    refute CellOperatorGroup.reject?(@user_rejected_to_smarts, Channel.megafon_fixed, @phones[:smarts])
  end

  private
  def phones_except(operator)
    @phones.except(operator).values
  end

  def phone_only(operator)
    @phones[operator]
  end

  def operators
    @phones.keys
  end

  def check(user, operator)
    @channels.each do |channel|
      assert CellOperatorGroup.reject?(user, channel, phone_only(operator)), "should reject with phone = %s" % phone_only(operator)
      phones_except(operator).each do |phone|
        refute CellOperatorGroup.reject?(user, channel, phone), "should accept phone = %s" % phone
      end
    end
  end

end
