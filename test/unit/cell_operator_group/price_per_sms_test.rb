# -*- encoding : utf-8 -*-
require 'test_helper'

class CellOperatorGroup::PricePerSmsTest < ActiveSupport::TestCase
  def setup
    clear_db
    OperatorTariff.delete_all
  end

  test "should return price with opened end-date" do
    tariff = FactoryGirl.create(:mts_tariff, begin_date: Date.today - 1.year, end_date: nil)
    assert_equal tariff.price_per_sms, CellOperatorGroup.mts.price_per_sms
  end

  test "should return price with closed end-date" do
    tariff = FactoryGirl.create(:mts_tariff, begin_date: Date.today - 1.year, end_date: Date.today + 1.year)
    assert_equal tariff.price_per_sms, CellOperatorGroup.mts.price_per_sms
  end

  test "should return price with opened end-date plus-minus one day" do
    tariff = FactoryGirl.create(:mts_tariff, begin_date: Date.today - 1.year, end_date: nil)
    assert_equal tariff.price_per_sms, CellOperatorGroup.mts.price_per_sms(Date.today - 1.day)
    assert_equal tariff.price_per_sms, CellOperatorGroup.mts.price_per_sms(Date.today + 1.day)
  end

  test "should return price with closed end-date plus-minus one day" do
    tariff = FactoryGirl.create(:mts_tariff, begin_date: Date.today - 1.year, end_date: Date.today + 1.year)
    assert_equal tariff.price_per_sms, CellOperatorGroup.mts.price_per_sms(Date.today - 1.day)
    assert_equal tariff.price_per_sms, CellOperatorGroup.mts.price_per_sms(Date.today + 1.day)
  end

  test "should return 0 price if price-per-sms not found" do
    OperatorTariff.destroy_all
    assert_equal 0, CellOperatorGroup.mts.price_per_sms
  end

end
