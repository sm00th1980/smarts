# -*- encoding : utf-8 -*-
require 'test_helper'

class CellOperatorGroup::GetByPhoneTest < ActiveSupport::TestCase
  def setup
    clear_db

    @valid_phones = {
        mts:     79198015896,
        beeline: 79657250000,
        megafon: 79379903835,
        tele2:   79005910000,
        baikal:  79016300000,
        ural:    79012200000,
        enisey:  79011110000,
        motiv:   79000300000,
        nss:     79003100000,
        cdma:    79011570000,
        other:   79585360000,
        smarts:  79022900000
    }

    @invalid_phones = [
        123,
        nil,
        ' ',
        'mfkldmkd'
    ]
  end

  test "get by phone should work correctly with valid phones" do
    @valid_phones.each do |operator, phone|
      assert_equal CellOperatorGroup.send(operator), CellOperatorGroup.get_by_phone(phone.to_i)
      assert_equal CellOperatorGroup.send(operator), CellOperatorGroup.get_by_phone(phone.to_s)
    end
  end

  test "get by phone should work return other if phone is invalid" do
    @invalid_phones.each do |invalid_phone|
      assert_equal CellOperatorGroup.other, CellOperatorGroup.get_by_phone(invalid_phone)
    end
  end

end
