# -*- encoding : utf-8 -*-
require 'test_helper'

class CellOperatorGroup::TypeTest < ActiveSupport::TestCase
  def setup
    clear_db

    @mts     = OperatorTariff.find_by_operator_id(CellOperatorGroup.mts)
    @beeline = OperatorTariff.find_by_operator_id(CellOperatorGroup.beeline)
    @megafon = OperatorTariff.find_by_operator_id(CellOperatorGroup.megafon)
    @tele2   = OperatorTariff.find_by_operator_id(CellOperatorGroup.tele2)
    @baikal  = OperatorTariff.find_by_operator_id(CellOperatorGroup.baikal)
    @ural    = OperatorTariff.find_by_operator_id(CellOperatorGroup.ural)
    @enisey  = OperatorTariff.find_by_operator_id(CellOperatorGroup.enisey)
    @cdma    = OperatorTariff.find_by_operator_id(CellOperatorGroup.cdma)
    @motiv   = OperatorTariff.find_by_operator_id(CellOperatorGroup.motiv)
    @nss     = OperatorTariff.find_by_operator_id(CellOperatorGroup.nss)
    @smarts  = OperatorTariff.find_by_operator_id(CellOperatorGroup.smarts)
    @other   = OperatorTariff.find_by_operator_id(CellOperatorGroup.other)
  end

  test "mts group exist" do
    assert_nothing_raised do
      assert_not_nil CellOperatorGroup.mts
      assert_equal @mts.price_per_sms, CellOperatorGroup.mts.price_per_sms
      assert_equal 'mts', CellOperatorGroup.mts.internal_name
    end

    check CellOperatorGroup.mts, :mts?
  end

  test "beeline group exist" do
    assert_nothing_raised do
      assert_not_nil CellOperatorGroup.beeline
      assert_equal @beeline.price_per_sms, CellOperatorGroup.beeline.price_per_sms
      assert_equal 'beeline', CellOperatorGroup.beeline.internal_name
    end

    check CellOperatorGroup.beeline, :beeline?
  end

  test "megafon group exist" do
    assert_nothing_raised do
      assert_not_nil CellOperatorGroup.megafon
      assert_equal @megafon.price_per_sms, CellOperatorGroup.megafon.price_per_sms
      assert_equal 'megafon', CellOperatorGroup.megafon.internal_name
    end

    check CellOperatorGroup.megafon, :megafon?
  end

  test "tele2 group exist" do
    assert_nothing_raised do
      assert_not_nil CellOperatorGroup.tele2
      assert_equal @tele2.price_per_sms, CellOperatorGroup.tele2.price_per_sms
      assert_equal 'tele2', CellOperatorGroup.tele2.internal_name
    end

    check CellOperatorGroup.tele2, :tele2?
  end

  test "baikal group exist" do
    assert_nothing_raised do
      assert_not_nil CellOperatorGroup.baikal
      assert_equal @baikal.price_per_sms, CellOperatorGroup.baikal.price_per_sms
      assert_equal 'baikal', CellOperatorGroup.baikal.internal_name
    end

    check CellOperatorGroup.baikal, :baikal?
  end

  test "ural group exist" do
    assert_nothing_raised do
      assert_not_nil CellOperatorGroup.ural
      assert_equal @ural.price_per_sms, CellOperatorGroup.ural.price_per_sms
      assert_equal 'ural', CellOperatorGroup.ural.internal_name
    end

    check CellOperatorGroup.ural, :ural?
  end

  test "enisey group exist" do
    assert_nothing_raised do
      assert_not_nil CellOperatorGroup.enisey
      assert_equal @enisey.price_per_sms, CellOperatorGroup.enisey.price_per_sms
      assert_equal 'enisey', CellOperatorGroup.enisey.internal_name
    end

    check CellOperatorGroup.enisey, :enisey?
  end

  test "cdma group exist" do
    assert_nothing_raised do
      assert_not_nil CellOperatorGroup.cdma
      assert_equal @cdma.price_per_sms, CellOperatorGroup.cdma.price_per_sms
      assert_equal 'cdma', CellOperatorGroup.cdma.internal_name
    end

    check CellOperatorGroup.cdma, :cdma?
  end

  test "motiv group exist" do
    assert_nothing_raised do
      assert_not_nil CellOperatorGroup.motiv
      assert_equal @motiv.price_per_sms, CellOperatorGroup.motiv.price_per_sms
      assert_equal 'motiv', CellOperatorGroup.motiv.internal_name
    end

    check CellOperatorGroup.motiv, :motiv?
  end

  test "nss group exist" do
    assert_nothing_raised do
      assert_not_nil CellOperatorGroup.nss
      assert_equal @nss.price_per_sms, CellOperatorGroup.nss.price_per_sms
      assert_equal 'nss', CellOperatorGroup.nss.internal_name
    end

    check CellOperatorGroup.nss, :nss?
  end

  test "other group exist" do
    assert_nothing_raised do
      assert_not_nil CellOperatorGroup.other
      assert_equal @other.price_per_sms, CellOperatorGroup.other.price_per_sms
      assert_equal 'other', CellOperatorGroup.other.internal_name
    end

    check CellOperatorGroup.other, :other?
  end

  test "smarts group exist" do
    assert_nothing_raised do
      assert_not_nil CellOperatorGroup.smarts
      assert_equal @smarts.price_per_sms, CellOperatorGroup.smarts.price_per_sms
      assert_equal 'smarts', CellOperatorGroup.smarts.internal_name
    end

    check CellOperatorGroup.smarts, :smarts?
  end

  test "working group should exist" do
    assert_nothing_raised do
      assert_equal [
                       CellOperatorGroup.mts,
                       CellOperatorGroup.beeline,
                       CellOperatorGroup.megafon,
                       CellOperatorGroup.tele2,
                       CellOperatorGroup.baikal,
                       CellOperatorGroup.ural,
                       CellOperatorGroup.enisey,
                       CellOperatorGroup.cdma,
                       CellOperatorGroup.motiv,
                       CellOperatorGroup.nss,
                       CellOperatorGroup.smarts,
                       CellOperatorGroup.other
                   ].sort, CellOperatorGroup.working.sort
    end
  end

  private
  def check(checking_cell_operator_group, operator)

    _operators = [
        :mts?,
        :beeline?,
        :megafon?,
        :tele2?,
        :baikal?,
        :ural?,
        :enisey?,
        :cdma?,
        :motiv?,
        :nss?,
        :other?,
        :smarts?,
    ]

    assert checking_cell_operator_group.send(operator)

    _operators.reject{|el| el == operator}.each do |operator|
      refute checking_cell_operator_group.send(operator)
    end
  end
end
