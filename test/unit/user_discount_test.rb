# -*- encoding : utf-8 -*-
require 'test_helper'

class UserDiscountTest < ActiveSupport::TestCase

  def setup
    clear_db

    @user_without_discount = FactoryGirl.create(:user_without_discount)
    @user_with_discount    = FactoryGirl.create(:user_with_discount)

    #invalid
    @user_with_discount_more_100_percent = FactoryGirl.create(:user_with_discount_more_100_percent)
    @user_with_negative_discount = FactoryGirl.create(:user_with_negative_discount)
  end

  #without discount
  test "should price equal full-price for user without discount" do
    assert_equal 0, @user_without_discount.discount

    full_price = @user_without_discount.tariff.price_per_sms
    assert_not_nil full_price
    assert_operator full_price, :>, 0
    assert_equal full_price, @user_without_discount.price_per_sms
  end

  #with discount
  test "should price be less full-price on discount for user without discount" do
    assert_operator @user_with_discount.discount, :> , 0

    full_price = @user_with_discount.tariff.price_per_sms
    discount_price = (full_price - full_price * @user_with_discount.discount/100.0).round(2)
    assert_equal discount_price, @user_with_discount.price_per_sms
  end

  #invalid discount
  test "should price equal full-price for user with negative discount" do
    assert_operator @user_with_negative_discount.discount, :< , 0

    full_price = @user_with_negative_discount.tariff.price_per_sms
    assert_not_nil full_price
    assert_operator full_price, :>, 0
    assert_equal full_price, @user_with_negative_discount.price_per_sms
  end

  test "should price equal full-price for user with discount more 100 percents" do
    assert_operator @user_with_discount_more_100_percent.discount, :> , 100

    full_price = @user_with_discount_more_100_percent.tariff.price_per_sms
    assert_not_nil full_price
    assert_operator full_price, :>, 0
    assert_equal full_price, @user_with_discount_more_100_percent.price_per_sms
  end
end
