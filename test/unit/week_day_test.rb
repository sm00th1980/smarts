# -*- encoding : utf-8 -*-
require 'test_helper'

class WeekDayTest < ActiveSupport::TestCase
  def setup
    clear_db
  end

  test "monday exist" do
    assert_not_nil WeekDay.monday
    assert_equal 'monday', WeekDay.monday.eng_name
  end

  test "tuesday exist" do
    assert_not_nil WeekDay.tuesday
    assert_equal 'tuesday', WeekDay.tuesday.eng_name
  end

  test "wednesday exist" do
    assert_not_nil WeekDay.wednesday
    assert_equal 'wednesday', WeekDay.wednesday.eng_name
  end

  test "thursday exist" do
    assert_not_nil WeekDay.thursday
    assert_equal 'thursday', WeekDay.thursday.eng_name
  end

  test "friday exist" do
    assert_not_nil WeekDay.friday
    assert_equal 'friday', WeekDay.friday.eng_name
  end

  test "saturday exist" do
    assert_not_nil WeekDay.saturday
    assert_equal 'saturday', WeekDay.saturday.eng_name
  end

  test "sunday exist" do
    assert_not_nil WeekDay.sunday
    assert_equal 'sunday', WeekDay.sunday.eng_name
  end

  test "all week" do
    assert_equal [WeekDay.monday, WeekDay.tuesday, WeekDay.wednesday, WeekDay.thursday, WeekDay.friday, WeekDay.saturday, WeekDay.sunday], WeekDay.days
  end
end
