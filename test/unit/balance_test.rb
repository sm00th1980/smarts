# -*- encoding : utf-8 -*-
require 'test_helper'

class BalanceTest < ActiveSupport::TestCase

  test "should return correct period" do
    date = Date.today
    period = {
        period: date.at_beginning_of_month.strftime('%Y%m').to_i,
        begin_date: date.at_beginning_of_month,
        end_date: date.at_end_of_month
    }

    assert_equal period, Balance.period(date)
  end

end
