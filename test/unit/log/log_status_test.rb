# -*- encoding : utf-8 -*-
require 'test_helper'

class Log::LogStatusTest < ActiveSupport::TestCase
  def setup
    clear_db

    user = FactoryGirl.create(:user)

    @log_fresh                = FactoryGirl.create(:log_fresh_valid, user: user, status: LogStatus.fresh)
    @log_checking             = FactoryGirl.create(:log_fresh_valid, user: user, status: LogStatus.checking)
    @log_ready_for_processing = FactoryGirl.create(:log_fresh_valid, user: user, status: LogStatus.ready_for_processing)
    @log_processing           = FactoryGirl.create(:log_fresh_valid, user: user, status: LogStatus.processing)
    @log_completed            = FactoryGirl.create(:log_fresh_valid, user: user, status: LogStatus.completed)
    @log_check_failed         = FactoryGirl.create(:log_fresh_valid, user: user, status: LogStatus.check_failed)
    @log_stopped              = FactoryGirl.create(:log_fresh_valid, user: user, status: LogStatus.stopped)
  end

  #stopable
  test "stopable should be fresh or new or checking or ready_for_processing" do
    assert @log_fresh.stopable?
    assert @log_checking.stopable?
    assert @log_ready_for_processing.stopable?
    assert @log_processing.stopable?
  end

  test "stopable should not be completed or check_failed or stopped" do
    refute @log_completed.stopable?
    refute @log_check_failed.stopable?
    refute @log_stopped.stopable?
  end

  #ready for processing
  test "ready_for_processing should be processing or ready_for_processing" do
    assert @log_ready_for_processing.ready_for_processing?
    assert @log_processing.ready_for_processing?
  end

  test "ready_for_processing should not be fresh or checking or completed or check_failed or stopped" do
    refute @log_fresh.ready_for_processing?
    refute @log_checking.ready_for_processing?
    refute @log_completed.ready_for_processing?
    refute @log_check_failed.ready_for_processing?
    refute @log_stopped.ready_for_processing?
  end

  #stop!
  test "can stop stopable logs" do
    check_stopping(@log_fresh)
    check_stopping(@log_checking)
    check_stopping(@log_ready_for_processing)
    check_stopping(@log_processing)
  end

  test "can not stop non-stopable logs" do
    check_not_stopping(@log_completed)
    check_not_stopping(@log_check_failed)
    check_not_stopping(@log_stopped)
  end

  private
  def check_stopping(log)
    assert log.stop!
    assert_equal LogStatus.stopped, log.reload.status
  end

  def check_not_stopping(log)
    _status = log.status
    refute log.stop!
    assert_equal _status, log.reload.status
  end

end
