# -*- encoding : utf-8 -*-
require 'test_helper'

class ChargeTypeTest < ActiveSupport::TestCase
  def setup
    clear_db
  end

  test "by_sent exist" do
    assert_nothing_raised do
      assert_not_nil ChargeType.by_sent
      assert_equal 'by_sent', ChargeType.by_sent.internal_name
    end
  end

  test "by_delivered exist" do
    assert_nothing_raised do
      assert_not_nil ChargeType.by_delivered
      assert_equal 'by_delivered', ChargeType.by_delivered.internal_name
    end
  end
end
