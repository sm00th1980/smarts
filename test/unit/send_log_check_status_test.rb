# -*- encoding : utf-8 -*-
require 'test_helper'

class SendLogCheckStatusTest < ActiveSupport::TestCase
  def setup
    clear_db
  end

  test "invalid_phone status should exist" do
    assert_not_nil SendLogCheckStatus.invalid_phone
    assert_equal 'invalid_phone', SendLogCheckStatus.invalid_phone.internal_name
  end
end
