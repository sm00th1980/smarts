# -*- encoding : utf-8 -*-
require 'test_helper'

class SendLogDeliveryError::AnyTest < ActiveSupport::TestCase
  def setup
    clear_db
  end

  test "rejected to mts should exist" do
    assert_not_nil SendLogDeliveryError::Any.reject_to_mts
    assert_equal 'REJECT_TO_MTS', SendLogDeliveryError::Any.reject_to_mts.eng_name
    assert_equal '-9', SendLogDeliveryError::Any.reject_to_mts.code
  end

  test "rejected to megafon should exist" do
    assert_not_nil SendLogDeliveryError::Any.reject_to_megafon
    assert_equal 'REJECT_TO_MEGAFON', SendLogDeliveryError::Any.reject_to_megafon.eng_name
    assert_equal '-8', SendLogDeliveryError::Any.reject_to_megafon.code
  end

  test "nack_11 should exist" do
    assert_not_nil SendLogDeliveryError::Any.nack_11
    assert_equal 'NACK/11/Invalid Destination Address', SendLogDeliveryError::Any.nack_11.eng_name
    assert_equal '-7', SendLogDeliveryError::Any.nack_11.code
  end

  test "unknown error should exist" do
    assert_not_nil SendLogDeliveryError::Any.unknown_error
    assert_equal 'ERROR_UNKNOWN', SendLogDeliveryError::Any.unknown_error.eng_name
    assert_equal '-6', SendLogDeliveryError::Any.unknown_error.code
  end

  test "retries_exceeded should exist" do
    assert_not_nil SendLogDeliveryError::Any.retries_exceeded
    assert_equal 'RETRIES_EXCEEDED', SendLogDeliveryError::Any.retries_exceeded.eng_name
    assert_equal '-5', SendLogDeliveryError::Any.retries_exceeded.code
  end

  test "low_balance should exist" do
    assert_not_nil SendLogDeliveryError::Any.low_balance
    assert_equal 'LOW_BALANCE', SendLogDeliveryError::Any.low_balance.eng_name
    assert_equal '-4', SendLogDeliveryError::Any.low_balance.code
  end

  test "expired should exist" do
    assert_not_nil SendLogDeliveryError::Any.expired
    assert_equal 'SMS_TIMEOUT_EXPIRED', SendLogDeliveryError::Any.expired.eng_name
    assert_equal '-3', SendLogDeliveryError::Any.expired.code
  end

  test "kannel_failured should exist" do
    assert_not_nil SendLogDeliveryError::Any.kannel_failured
    assert_equal 'ERROR_DELIVERY_TO_KANNEL', SendLogDeliveryError::Any.kannel_failured.eng_name
    assert_equal '-2', SendLogDeliveryError::Any.kannel_failured.code
  end

  test "unknown status should exist" do
    assert_not_nil SendLogDeliveryError::Any.unknown_status
    assert_equal 'STATUS_UNKNOWN', SendLogDeliveryError::Any.unknown_status.eng_name
    assert_equal '-1', SendLogDeliveryError::Any.unknown_status.code
  end

  test "success delivered should exist" do
    assert_not_nil SendLogDeliveryError::Any.success_delivered
    assert_equal 'SUCCESS_DELIVERY', SendLogDeliveryError::Any.success_delivered.eng_name
    assert_equal '000', SendLogDeliveryError::Any.success_delivered.code
  end

end
