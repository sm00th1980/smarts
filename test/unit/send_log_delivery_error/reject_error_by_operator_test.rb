# -*- encoding : utf-8 -*-
require 'test_helper'

class SendLogDeliveryError::RejectErrorByOperatorTest < ActiveSupport::TestCase
  def setup
    clear_db
  end

  test "should return reject-to-megafon for megafon" do
    assert_equal SendLogDeliveryError::Any.reject_to_megafon, SendLogDeliveryError.reject_error_by_operator(CellOperatorGroup.megafon)
  end

  test "should return reject-to-smarts for smarts" do
    assert_equal SendLogDeliveryError.gold_reject_to_smarts, SendLogDeliveryError.reject_error_by_operator(CellOperatorGroup.smarts)
  end

  test "should return reject-to-mts for mts" do
    assert_equal SendLogDeliveryError::Any.reject_to_mts, SendLogDeliveryError.reject_error_by_operator(CellOperatorGroup.mts)
  end

  test "should be nil for others operators" do
    (CellOperatorGroup.working - [CellOperatorGroup.megafon, CellOperatorGroup.mts, CellOperatorGroup.smarts]).each do |operator|
      assert_nil SendLogDeliveryError.reject_error_by_operator(operator)
    end
  end

end
