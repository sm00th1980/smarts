# -*- encoding : utf-8 -*-
require 'test_helper'

class SendLogDeliveryError::GoldTest < ActiveSupport::TestCase
  def setup
    clear_db
  end

  test "rejected error to smarts via gold channel should exist" do
    assert_not_nil SendLogDeliveryError.gold_reject_to_smarts
    assert_equal 'REJECT_TO_SMARTS_VIA_GOLD_CHANNEL', SendLogDeliveryError.gold_reject_to_smarts.eng_name
    assert_equal '-9', SendLogDeliveryError.gold_reject_to_smarts.code
  end

  test "rejected error to megafon via gold channel should exist" do
    assert_not_nil SendLogDeliveryError.gold_reject_to_megafon
    assert_equal 'REJECT_TO_MEGAFON_VIA_GOLD_CHANNEL', SendLogDeliveryError.gold_reject_to_megafon.eng_name
    assert_equal '-8', SendLogDeliveryError.gold_reject_to_megafon.code
  end

  test "gold_error_nack_11 should exist" do
    assert_not_nil SendLogDeliveryError.gold_error_nack_11
    assert_equal 'NACK/11/Invalid Destination Address', SendLogDeliveryError.gold_error_nack_11.eng_name
    assert_equal '-7', SendLogDeliveryError.gold_error_nack_11.code
  end

  test "gold_error_unknown should exist" do
    assert_not_nil SendLogDeliveryError.gold_error_unknown
    assert_equal 'ERROR_UNKNOWN', SendLogDeliveryError.gold_error_unknown.eng_name
    assert_equal '-6', SendLogDeliveryError.gold_error_unknown.code
  end

  test "retries_exceeded should exist" do
    assert_not_nil SendLogDeliveryError.gold_retries_exceeded
    assert_equal 'RETRIES_EXCEEDED', SendLogDeliveryError.gold_retries_exceeded.eng_name
    assert_equal '-5', SendLogDeliveryError.gold_retries_exceeded.code
  end

  test "low_balance should exist" do
    assert_not_nil SendLogDeliveryError.gold_low_balance
    assert_equal 'LOW_BALANCE', SendLogDeliveryError.gold_low_balance.eng_name
    assert_equal '-4', SendLogDeliveryError.gold_low_balance.code
  end

  test "expired should exist" do
    assert_not_nil SendLogDeliveryError.gold_expired
    assert_equal 'SMS_TIMEOUT_EXPIRED', SendLogDeliveryError.gold_expired.eng_name
    assert_equal '-3', SendLogDeliveryError.gold_expired.code
  end

  test "kannel_failured should exist" do
    assert_not_nil SendLogDeliveryError.gold_kannel_failured
    assert_equal 'ERROR_DELIVERY_TO_KANNEL', SendLogDeliveryError.gold_kannel_failured.eng_name
    assert_equal '-2', SendLogDeliveryError.gold_kannel_failured.code
  end

  test "gold_status_unknown should exist" do
    assert_not_nil SendLogDeliveryError.gold_status_unknown
    assert_equal 'STATUS_UNKNOWN', SendLogDeliveryError.gold_status_unknown.eng_name
    assert_equal '-1', SendLogDeliveryError.gold_status_unknown.code
  end

  test "no_error should exist" do
    assert_not_nil SendLogDeliveryError.gold_no_error
    assert_equal 'SUCCESS_DELIVERY', SendLogDeliveryError.gold_no_error.eng_name
    assert_equal '000', SendLogDeliveryError.gold_no_error.code
  end

  test "gold_error_unknown_or_no_delivery_report should exist" do
    assert_not_nil SendLogDeliveryError.gold_error_unknown_or_no_delivery_report
    assert_equal 'ERROR_UNKNOWN_OR_NO_DELIVERY_REPORT', SendLogDeliveryError.gold_error_unknown_or_no_delivery_report.eng_name
    assert_equal '001', SendLogDeliveryError.gold_error_unknown_or_no_delivery_report.code
  end

  test "gold_error_phone_out_of_network should exist" do
    assert_not_nil SendLogDeliveryError.gold_error_phone_out_of_network
    assert_equal 'ERROR_PHONE_OUT_OF_NETWORK', SendLogDeliveryError.gold_error_phone_out_of_network.eng_name
    assert_equal '002', SendLogDeliveryError.gold_error_phone_out_of_network.code
  end

  test "gold_error_phone_is_blocked_or_roaming should exist" do
    assert_not_nil SendLogDeliveryError.gold_error_phone_is_blocked_or_roaming
    assert_equal 'ERROR_PHONE_IS_BLOCKED_OR_ROAMING', SendLogDeliveryError.gold_error_phone_is_blocked_or_roaming.eng_name
    assert_equal '003', SendLogDeliveryError.gold_error_phone_is_blocked_or_roaming.code
  end

  test "gold_error_gsm_transport_level_is_corrupted should exist" do
    assert_not_nil SendLogDeliveryError.gold_error_gsm_transport_level_is_corrupted
    assert_equal 'ERROR_GSM_TRANSPORT_LEVEL_IS_CORRUPTED', SendLogDeliveryError.gold_error_gsm_transport_level_is_corrupted.eng_name
    assert_equal '004', SendLogDeliveryError.gold_error_gsm_transport_level_is_corrupted.code
  end

  test "gold_error_phone_out_of_memory should exist" do
    assert_not_nil SendLogDeliveryError.gold_error_phone_out_of_memory
    assert_equal 'ERROR_PHONE_OUT_OF_MEMORY', SendLogDeliveryError.gold_error_phone_out_of_memory.eng_name
    assert_equal '005', SendLogDeliveryError.gold_error_phone_out_of_memory.code
  end

  test "gold_error_incompatible_terminal should exist" do
    assert_not_nil SendLogDeliveryError.gold_error_incompatible_terminal
    assert_equal 'ERROR_INCOMPATIBLE_TERMINAL', SendLogDeliveryError.gold_error_incompatible_terminal.eng_name
    assert_equal '006', SendLogDeliveryError.gold_error_incompatible_terminal.code
  end

  test "gold_error_gsm_switch_is_not_available should exist" do
    assert_not_nil SendLogDeliveryError.gold_error_gsm_switch_is_not_available
    assert_equal 'ERROR_GSM_SWITCH_IS_NOT_AVAILABLE', SendLogDeliveryError.gold_error_gsm_switch_is_not_available.eng_name
    assert_equal '007', SendLogDeliveryError.gold_error_gsm_switch_is_not_available.code
  end

  test "gold_error_phone_number_is_not_valid should exist" do
    assert_not_nil SendLogDeliveryError.gold_error_phone_number_is_not_valid
    assert_equal 'ERROR_PHONE_NUMBER_IS_NOT_VALID', SendLogDeliveryError.gold_error_phone_number_is_not_valid.eng_name
    assert_equal '008', SendLogDeliveryError.gold_error_phone_number_is_not_valid.code
  end

  test "gold_error_message_was_rejected_by_dublicating should exist" do
    assert_not_nil SendLogDeliveryError.gold_error_message_was_rejected_by_dublicating
    assert_equal 'ERROR_MESSAGE_WAS_REJECTED_BY_DUBLICATING', SendLogDeliveryError.gold_error_message_was_rejected_by_dublicating.eng_name
    assert_equal '009', SendLogDeliveryError.gold_error_message_was_rejected_by_dublicating.code
  end

  test "gold_error_message_was_rejected_by_spam_filter should exist" do
    assert_not_nil SendLogDeliveryError.gold_error_message_was_rejected_by_spam_filter
    assert_equal 'ERROR_MESSAGE_WAS_REJECTED_BY_SPAM_FILTER', SendLogDeliveryError.gold_error_message_was_rejected_by_spam_filter.eng_name
    assert_equal '010', SendLogDeliveryError.gold_error_message_was_rejected_by_spam_filter.code
  end

  test "gold_error_gsm_routing should exist" do
    assert_not_nil SendLogDeliveryError.gold_error_gsm_routing
    assert_equal 'ERROR_GSM_ROUTING', SendLogDeliveryError.gold_error_gsm_routing.eng_name
    assert_equal '011', SendLogDeliveryError.gold_error_gsm_routing.code
  end

  test "gold_error invalid destination number should exist" do
    assert_not_nil SendLogDeliveryError.gold_error_invalid_destination_number
    assert_equal 'ERROR_INVALID_DESTINATION_NUMBER', SendLogDeliveryError.gold_error_invalid_destination_number.eng_name
    assert_equal '00b', SendLogDeliveryError.gold_error_invalid_destination_number.code
  end

  test "gold_no_server_link_route should exist" do
    assert_not_nil SendLogDeliveryError.gold_no_server_link_route
    assert_equal 'NO_SERVER_LINK_ROUTE', SendLogDeliveryError.gold_no_server_link_route.eng_name
    assert_equal '3840', SendLogDeliveryError.gold_no_server_link_route.code
  end

  test "gold_spam_filter_action should exist" do
    assert_not_nil SendLogDeliveryError.gold_spam_filter_action
    assert_equal 'SPAM_FILTER_ACTION', SendLogDeliveryError.gold_spam_filter_action.eng_name
    assert_equal '3841', SendLogDeliveryError.gold_spam_filter_action.code
  end

  test "gold_limit_exceeded should exist" do
    assert_not_nil SendLogDeliveryError.gold_limit_exceeded
    assert_equal 'LIMIT_EXCEEDED', SendLogDeliveryError.gold_limit_exceeded.eng_name
    assert_equal '3842', SendLogDeliveryError.gold_limit_exceeded.code
  end

end
