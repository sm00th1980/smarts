# -*- encoding : utf-8 -*-
require 'test_helper'

class SendLogDeliveryError::BronzeTest < ActiveSupport::TestCase
  def setup
    clear_db
  end

  test "bronze_error_unknown should exist" do
    assert_not_nil SendLogDeliveryError.bronze_error_unknown
    assert_equal 'ERROR_UNKNOWN', SendLogDeliveryError.bronze_error_unknown.eng_name
    assert_equal '-6', SendLogDeliveryError.bronze_error_unknown.code
  end

  test "retries_exceeded should exist" do
    assert_not_nil SendLogDeliveryError.bronze_retries_exceeded
    assert_equal 'RETRIES_EXCEEDED', SendLogDeliveryError.bronze_retries_exceeded.eng_name
    assert_equal '-5', SendLogDeliveryError.bronze_retries_exceeded.code
  end

  test "low_balance should exist" do
    assert_not_nil SendLogDeliveryError.bronze_low_balance
    assert_equal 'LOW_BALANCE', SendLogDeliveryError.bronze_low_balance.eng_name
    assert_equal '-4', SendLogDeliveryError.bronze_low_balance.code
  end

  test "expired should exist" do
    assert_not_nil SendLogDeliveryError.bronze_expired
    assert_equal 'SMS_TIMEOUT_EXPIRED', SendLogDeliveryError.bronze_expired.eng_name
    assert_equal '-3', SendLogDeliveryError.bronze_expired.code
  end

  test "kannel_failured should exist" do
    assert_not_nil SendLogDeliveryError.bronze_kannel_failured
    assert_equal 'ERROR_DELIVERY_TO_KANNEL', SendLogDeliveryError.bronze_kannel_failured.eng_name
    assert_equal '-2', SendLogDeliveryError.bronze_kannel_failured.code
  end

  test "unknown should exist" do
    assert_not_nil SendLogDeliveryError.bronze_status_unknown
    assert_equal 'STATUS_UNKNOWN', SendLogDeliveryError.bronze_status_unknown.eng_name
    assert_equal '-1', SendLogDeliveryError.bronze_status_unknown.code
  end

  test "no_error should exist" do
    assert_not_nil SendLogDeliveryError.bronze_no_error
    assert_equal 'SUCCESS_DELIVERY', SendLogDeliveryError.bronze_no_error.eng_name
    assert_equal '000', SendLogDeliveryError.bronze_no_error.code
  end

  test "error_unknown_subscriber should exist" do
    assert_not_nil SendLogDeliveryError.bronze_error_unknown_subscriber
    assert_equal 'ERROR_UNKNOWN_SUBSCRIBER', SendLogDeliveryError.bronze_error_unknown_subscriber.eng_name
    assert_equal '001', SendLogDeliveryError.bronze_error_unknown_subscriber.code
  end

  test "error_unidentified_subscriber should exist" do
    assert_not_nil SendLogDeliveryError.bronze_error_unidentified_subscriber
    assert_equal 'ERROR_UNIDENTIFIED_SUBSCRIBER', SendLogDeliveryError.bronze_error_unidentified_subscriber.eng_name
    assert_equal '005', SendLogDeliveryError.bronze_error_unidentified_subscriber.code
  end

  test "error_absent_subscriber_sm should exist" do
    assert_not_nil SendLogDeliveryError.bronze_error_absent_subscriber_sm
    assert_equal 'ERROR_ABSENT_SUBSCRIBER_SM', SendLogDeliveryError.bronze_error_absent_subscriber_sm.eng_name
    assert_equal '006', SendLogDeliveryError.bronze_error_absent_subscriber_sm.code
  end

  test "error_illegal_subscriber should exist" do
    assert_not_nil SendLogDeliveryError.bronze_error_illegal_subscriber
    assert_equal 'ERROR_ILLEGAL_SUBSCRIBER', SendLogDeliveryError.bronze_error_illegal_subscriber.eng_name
    assert_equal '009', SendLogDeliveryError.bronze_error_illegal_subscriber.code
  end

  test "error_teleservice_not_provisioned should exist" do
    assert_not_nil SendLogDeliveryError.bronze_error_teleservice_not_provisioned
    assert_equal 'ERROR_TELESERVICE_NOT_PROVISIONED', SendLogDeliveryError.bronze_error_teleservice_not_provisioned.eng_name
    assert_equal '011', SendLogDeliveryError.bronze_error_teleservice_not_provisioned.code
  end

  test "error_illegal_equipment should exist" do
    assert_not_nil SendLogDeliveryError.bronze_error_illegal_equipment
    assert_equal 'ERROR_ILLEGAL_EQUIPMENT', SendLogDeliveryError.bronze_error_illegal_equipment.eng_name
    assert_equal '012', SendLogDeliveryError.bronze_error_illegal_equipment.code
  end

  test "error_call_barred should exist" do
    assert_not_nil SendLogDeliveryError.bronze_error_call_barred
    assert_equal 'ERROR_CALL_BARRED', SendLogDeliveryError.bronze_error_call_barred.eng_name
    assert_equal '013', SendLogDeliveryError.bronze_error_call_barred.code
  end

  test "error_facility_not_supported should exist" do
    assert_not_nil SendLogDeliveryError.bronze_error_facility_not_supported
    assert_equal 'ERROR_FACILITY_NOT_SUPPORTED', SendLogDeliveryError.bronze_error_facility_not_supported.eng_name
    assert_equal '021', SendLogDeliveryError.bronze_error_facility_not_supported.code
  end

  test "error_absent_subscriber should exist" do
    assert_not_nil SendLogDeliveryError.bronze_error_absent_subscriber
    assert_equal 'ERROR_ABSENT_SUBSCRIBER', SendLogDeliveryError.bronze_error_absent_subscriber.eng_name
    assert_equal '027', SendLogDeliveryError.bronze_error_absent_subscriber.code
  end

  test "error_incompatible_terminal should exist" do
    assert_not_nil SendLogDeliveryError.bronze_error_incompatible_terminal
    assert_equal 'ERROR_INCOMPATIBLE_TERMINAL', SendLogDeliveryError.bronze_error_incompatible_terminal.eng_name
    assert_equal '028', SendLogDeliveryError.bronze_error_incompatible_terminal.code
  end

  test "error_subscriber_busy_for_mt_sms should exist" do
    assert_not_nil SendLogDeliveryError.bronze_error_subscriber_busy_for_mt_sms
    assert_equal 'ERROR_SUBSCRIBER_BUSY_FOR_MT_SMS', SendLogDeliveryError.bronze_error_subscriber_busy_for_mt_sms.eng_name
    assert_equal '031', SendLogDeliveryError.bronze_error_subscriber_busy_for_mt_sms.code
  end

  test "error_sm_delivery_failure should exist" do
    assert_not_nil SendLogDeliveryError.bronze_error_sm_delivery_failure
    assert_equal 'ERROR_SM_DELIVERY_FAILURE', SendLogDeliveryError.bronze_error_sm_delivery_failure.eng_name
    assert_equal '032', SendLogDeliveryError.bronze_error_sm_delivery_failure.code
  end

  test "error_message_waiting_list_full should exist" do
    assert_not_nil SendLogDeliveryError.bronze_error_message_waiting_list_full
    assert_equal 'ERROR_MESSAGE_WAITING_LIST_FULL', SendLogDeliveryError.bronze_error_message_waiting_list_full.eng_name
    assert_equal '033', SendLogDeliveryError.bronze_error_message_waiting_list_full.code
  end

  test "error_system_failure should exist" do
    assert_not_nil SendLogDeliveryError.bronze_error_system_failure
    assert_equal 'ERROR_SYSTEM_FAILURE', SendLogDeliveryError.bronze_error_system_failure.eng_name
    assert_equal '034', SendLogDeliveryError.bronze_error_system_failure.code
  end

  test "error_data_missing should exist" do
    assert_not_nil SendLogDeliveryError.bronze_error_data_missing
    assert_equal 'ERROR_DATA_MISSING', SendLogDeliveryError.bronze_error_data_missing.eng_name
    assert_equal '035', SendLogDeliveryError.bronze_error_data_missing.code
  end

  test "error_unexpected_data_value should exist" do
    assert_not_nil SendLogDeliveryError.bronze_error_unexpected_data_value
    assert_equal 'ERROR_UNEXPECTED_DATA_VALUE', SendLogDeliveryError.bronze_error_unexpected_data_value.eng_name
    assert_equal '036', SendLogDeliveryError.bronze_error_unexpected_data_value.code
  end

  test "error_resource_limitation should exist" do
    assert_not_nil SendLogDeliveryError.bronze_error_resource_limitation
    assert_equal 'ERROR_RESOURCE_LIMITATION', SendLogDeliveryError.bronze_error_resource_limitation.eng_name
    assert_equal '051', SendLogDeliveryError.bronze_error_resource_limitation.code
  end

  test "error_position_method_failure should exist" do
    assert_not_nil SendLogDeliveryError.bronze_error_position_method_failure
    assert_equal 'ERROR_POSITION_METHOD_FAILURE', SendLogDeliveryError.bronze_error_position_method_failure.eng_name
    assert_equal '054', SendLogDeliveryError.bronze_error_position_method_failure.code
  end

  test "error_unknown_alphabet should exist" do
    assert_not_nil SendLogDeliveryError.bronze_error_unknown_alphabet
    assert_equal 'ERROR_UNKNOWN_ALPHABET', SendLogDeliveryError.bronze_error_unknown_alphabet.eng_name
    assert_equal '071', SendLogDeliveryError.bronze_error_unknown_alphabet.code
  end

  test "error_ussd_busy should exist" do
    assert_not_nil SendLogDeliveryError.bronze_error_ussd_busy
    assert_equal 'ERROR_USSD_BUSY', SendLogDeliveryError.bronze_error_ussd_busy.eng_name
    assert_equal '072', SendLogDeliveryError.bronze_error_ussd_busy.code
  end

  test "hlr_timeout should exist" do
    assert_not_nil SendLogDeliveryError.bronze_hlr_timeout
    assert_equal 'HLR_TIMEOUT', SendLogDeliveryError.bronze_hlr_timeout.eng_name
    assert_equal '201', SendLogDeliveryError.bronze_hlr_timeout.code
  end

  test "vlr_timeout should exist" do
    assert_not_nil SendLogDeliveryError.bronze_vlr_timeout
    assert_equal 'VLR_TIMEOUT', SendLogDeliveryError.bronze_vlr_timeout.eng_name
    assert_equal '202', SendLogDeliveryError.bronze_vlr_timeout.code
  end

  test "no_server_link_route should exist" do
    assert_not_nil SendLogDeliveryError.bronze_no_server_link_route
    assert_equal 'NO_SERVER_LINK_ROUTE', SendLogDeliveryError.bronze_no_server_link_route.eng_name
    assert_equal '3840', SendLogDeliveryError.bronze_no_server_link_route.code
  end

  test "spam_filter_action should exist" do
    assert_not_nil SendLogDeliveryError.bronze_spam_filter_action
    assert_equal 'SPAM_FILTER_ACTION', SendLogDeliveryError.bronze_spam_filter_action.eng_name
    assert_equal '3841', SendLogDeliveryError.bronze_spam_filter_action.code
  end

  test "limit_exceeded should exist" do
    assert_not_nil SendLogDeliveryError.bronze_limit_exceeded
    assert_equal 'LIMIT_EXCEEDED', SendLogDeliveryError.bronze_limit_exceeded.eng_name
    assert_equal '3842', SendLogDeliveryError.bronze_limit_exceeded.code
  end

end
