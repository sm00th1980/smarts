# -*- encoding : utf-8 -*-
require 'test_helper'

class LogStatusTest < ActiveSupport::TestCase
  def setup
    clear_db
  end

  test "fresh status should exist" do
     assert_not_nil LogStatus.fresh
     assert_equal 'new', LogStatus.fresh.internal_name
  end

  test "checking status should exist" do
    assert_not_nil LogStatus.checking
    assert_equal 'checking', LogStatus.checking.internal_name
  end

  test "ready_for_processing status should exist" do
    assert_not_nil LogStatus.ready_for_processing
    assert_equal 'ready_for_processing', LogStatus.ready_for_processing.internal_name
  end

  test "processing status should exist" do
    assert_not_nil LogStatus.processing
    assert_equal 'processing', LogStatus.processing.internal_name
  end

  test "completed status should exist" do
    assert_not_nil LogStatus.completed
    assert_equal 'completed', LogStatus.completed.internal_name
  end

  test "check_failed status should exist" do
    assert_not_nil LogStatus.check_failed
    assert_equal 'check_failed', LogStatus.check_failed.internal_name
  end

  test "stopped status should exist" do
    assert_not_nil LogStatus.stopped
    assert_equal 'stopped', LogStatus.stopped.internal_name
  end

end
