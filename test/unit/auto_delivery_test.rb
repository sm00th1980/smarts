# -*- encoding : utf-8 -*-
require 'test_helper'

class AutoDeliveryTest < ActiveSupport::TestCase
  def setup
    clear_db

    stub_real_request

    @autodelivery = FactoryGirl.create(:auto_delivery)

    @autodelivery_active = FactoryGirl.create(:auto_delivery_active)

    #invalid
    @autodelivery_without_start_date = FactoryGirl.create(:auto_delivery_without_start_date)
    @autodelivery_without_name = FactoryGirl.create(:auto_delivery_without_name)
    @autodelivery_not_active = FactoryGirl.create(:auto_delivery_not_active)
    @autodelivery_without_receivers = FactoryGirl.create(:auto_delivery_without_receivers)
    @autodelivery_without_content = FactoryGirl.create(:auto_delivery_without_content)
    @autodelivery_with_overdue = FactoryGirl.create(:auto_delivery_with_overdue)
    @autodelivery_with_invalid_sender_name = FactoryGirl.create(:auto_delivery_with_invalid_sender_name)

    @autodelivery_with_blocked_user = FactoryGirl.create(:auto_delivery_active, user: FactoryGirl.create(:user_blocked))
    @autodelivery_with_deleted_user = FactoryGirl.create(:auto_delivery_active, user: FactoryGirl.create(:user_deleted))

    #not overdued
    @auto_delivery_next_day = FactoryGirl.create(:auto_delivery_next_day)
    @auto_delivery_start_day_before_stop_next_day = FactoryGirl.create(:auto_delivery_start_day_before_stop_next_day)
  end

  test "should create log with auto-delivery type after run autodelivery" do
    autodelivery_every_hour_run_now = FactoryGirl.create(:auto_delivery_every_hour_run_now)
    time = Time.local(Date.today.year, Date.today.month, Date.today.day, 0, autodelivery_every_hour_run_now.minute, 0)

    freeze(time) do
      autodelivery_every_hour_run_now.run
    end

    log = Log.last

    assert_equal LogType.autodelivery, log.type
    assert_equal autodelivery_every_hour_run_now.sender_name_value, log.sender_name_value
    assert_equal autodelivery_every_hour_run_now.content, log.content
    assert_equal autodelivery_every_hour_run_now.user, log.user
  end

  test "should can run each valid autodelivery" do
    AutoDelivery.all.each do |delivery|
      assert_nil delivery.run
      assert_nothing_raised { delivery.run }
    end
  end

  test "should check valid for run every hour" do
    freeze(Time.now) do
      autodelivery_every_hour_run_now = FactoryGirl.create(:auto_delivery_every_hour_run_now)

      assert autodelivery_every_hour_run_now.valid_for_activation?
      assert autodelivery_every_hour_run_now.valid_for_run?
      assert_equal [], autodelivery_every_hour_run_now.activation_errors
    end
  end

  test "should check valid for run every day" do
    freeze(Time.now) do
      autodelivery_every_day_run_now = FactoryGirl.create(:auto_delivery_every_day_run_now)

      assert autodelivery_every_day_run_now.valid_for_activation?
      assert autodelivery_every_day_run_now.valid_for_run?
      assert_equal [], autodelivery_every_day_run_now.activation_errors
    end
  end

  test "should check valid for run every week" do
    freeze(Time.now) do
      autodelivery_every_week_run_now = FactoryGirl.create(:auto_delivery_every_week_run_now)

      assert autodelivery_every_week_run_now.valid_for_activation?
      assert autodelivery_every_week_run_now.valid_for_run?
      assert_equal [], autodelivery_every_week_run_now.activation_errors
    end
  end

  test "should activate not active delivery" do
    refute @autodelivery_not_active.active?
    @autodelivery_not_active.activate

    assert AutoDelivery.find(@autodelivery_not_active.id).active
  end

  test "should deactivate active delivery" do
    assert @autodelivery_active.active?
    @autodelivery_active.deactivate

    refute AutoDelivery.find(@autodelivery_not_active.id).active
  end

  #invalidate
  test "should invalidate without name" do
    refute @autodelivery_without_name.valid_for_activation?
    refute @autodelivery_without_name.valid_for_run?
    assert_equal [I18n.t('autodelivery.failure.blank_name')], @autodelivery_without_name.activation_errors
  end

  test "should invalidate not active" do
    assert @autodelivery_not_active.valid_for_activation?
    refute @autodelivery_not_active.valid_for_run?
    assert_equal [], @autodelivery_not_active.activation_errors
  end

  test "should invalidate without receivers" do
    refute @autodelivery_without_receivers.valid_for_activation?
    refute @autodelivery_without_receivers.valid_for_run?
    assert_equal [I18n.t('autodelivery.failure.no_receivers')], @autodelivery_without_receivers.activation_errors
  end

  test "should invalidate without content" do
    refute @autodelivery_without_content.valid_for_activation?
    refute @autodelivery_without_content.valid_for_run?
    assert_equal [I18n.t('autodelivery.failure.blank_content')], @autodelivery_without_content.activation_errors
  end

  test "should invalidate with invalid sender name" do
    refute @autodelivery_with_invalid_sender_name.valid_for_activation?
    refute @autodelivery_with_invalid_sender_name.valid_for_run?
    assert_equal [I18n.t('autodelivery.failure.invalid_sender_name_value')], @autodelivery_with_invalid_sender_name.activation_errors
  end

  test "should invalidate overdued" do
    refute @autodelivery_with_overdue.valid_for_activation?
    refute @autodelivery_with_overdue.valid_for_run?
    assert_equal [I18n.t('autodelivery.failure.overdued')], @autodelivery_with_overdue.activation_errors
  end

  test "should invalidate without start_date" do
    refute @autodelivery_without_start_date.valid_for_activation?
    refute @autodelivery_without_start_date.valid_for_run?
    assert_equal [I18n.t('autodelivery.failure.blank_start_date')], @autodelivery_without_start_date.activation_errors
  end

  test "should invalidate with blocked user" do
    refute @autodelivery_with_blocked_user.valid_for_activation?
    refute @autodelivery_with_blocked_user.valid_for_run?
    assert_equal [I18n.t('autodelivery.failure.invalid_user')], @autodelivery_with_blocked_user.activation_errors
  end

  test "should invalidate with deleted user" do
    refute @autodelivery_with_deleted_user.valid_for_activation?
    refute @autodelivery_with_deleted_user.valid_for_run?
    assert_equal [I18n.t('autodelivery.failure.invalid_user')], @autodelivery_with_deleted_user.activation_errors
  end


  test "should correct return has_receivers" do
    refute @autodelivery_without_receivers.has_receivers?
    assert @autodelivery.has_receivers?
  end

  test "should correct return receivers_count" do
    assert_equal 0, @autodelivery_without_receivers.receivers_count
    assert_equal @autodelivery.groups.map { |group| group.clients }.flatten.count + @autodelivery.clients.count, @autodelivery.receivers_count
  end

  test "should equal groups and group_ids size" do
    assert_equal @autodelivery.group_ids.size, @autodelivery.groups.size
  end

  test "should equal clients and client_ids size" do
    assert_equal @autodelivery.client_ids.size, @autodelivery.clients.size
  end

  test "should be valid for activation but not running for auto-delivery-next-day" do
    assert @auto_delivery_next_day.valid_for_activation?
    refute @auto_delivery_next_day.valid_for_run?
    assert_equal [], @auto_delivery_next_day.activation_errors
  end

  test "should be valid for activation but not running for auto-delivery-start-day-before-and-stop-next-day" do
    assert @auto_delivery_start_day_before_stop_next_day.valid_for_activation?
    refute @auto_delivery_start_day_before_stop_next_day.valid_for_run?
    assert_equal [], @auto_delivery_start_day_before_stop_next_day.activation_errors
  end

  #content
  test "should check valid when content exist" do
    freeze(Time.now) do
      autodelivery = FactoryGirl.create(:auto_delivery_every_week_run_now)
      autodelivery.content = 'content'
      autodelivery.template = nil
      autodelivery.save!

      assert autodelivery.valid_for_activation?
      assert autodelivery.valid_for_run?
      assert_equal [], autodelivery.activation_errors
    end
  end

  test "should check valid when template exist" do
    freeze(Time.now) do
      autodelivery = FactoryGirl.create(:auto_delivery_every_week_run_now)
      autodelivery.content = nil
      autodelivery.template = FactoryGirl.create(:template, user_id: autodelivery.user_id)
      autodelivery.save!

      assert autodelivery.valid_for_activation?
      assert autodelivery.valid_for_run?
      assert_equal [], autodelivery.activation_errors
    end
  end

  test "should invalidate with template without text" do
    autodelivery = FactoryGirl.create(:auto_delivery_every_week_run_now)
    autodelivery.content = nil
    autodelivery.template = FactoryGirl.create(:template_without_text, user_id: autodelivery.user_id)
    autodelivery.save!

    refute autodelivery.valid_for_activation?
    refute autodelivery.valid_for_run?
    assert_equal [I18n.t('autodelivery.failure.blank_content')], autodelivery.activation_errors
  end

  test "should invalidate with not exist template" do
    autodelivery = FactoryGirl.create(:auto_delivery_every_week_run_now)
    autodelivery.content = nil
    autodelivery.template_id = -1
    autodelivery.save!

    refute autodelivery.valid_for_activation?
    refute autodelivery.valid_for_run?
    assert_equal [I18n.t('autodelivery.failure.blank_content')], autodelivery.activation_errors
  end

end
