# -*- encoding : utf-8 -*-
require 'unit/operator_tariff/shared'

class OperatorTariff::OldTariffWithEndDateNewTariffWithEndDateTest < OperatorTariff::Shared
  def setup
    super

    @old_tariff = FactoryGirl.create(:mts_tariff, begin_date: Date.today - 1.year, end_date: Date.today + 1.year)
  end

  #failure
  test "should not create NT when NT begin-date is equal OT end-date" do
    params = {operator: @old_tariff.operator, price_per_sms: @old_tariff.price_per_sms, begin_date: @old_tariff.end_date, end_date: @old_tariff.end_date + 1.year}
    check_cross(params)
  end

  test "should not create NT when NT begin-date is between OT begin-date and end-date" do
    params = {operator: @old_tariff.operator, price_per_sms: @old_tariff.price_per_sms, begin_date: @old_tariff.end_date - 1.day, end_date: @old_tariff.end_date + 1.year}
    check_cross(params)
  end

  test "should not create NT when NT begin-date is equal OT begin-date and NT end-date is equal OT end-date" do
    params = {operator: @old_tariff.operator, price_per_sms: @old_tariff.price_per_sms, begin_date: @old_tariff.begin_date, end_date: @old_tariff.end_date}
    check_cross(params)
  end

  test "should not create NT when OT inside NT" do
    params = {operator: @old_tariff.operator, price_per_sms: @old_tariff.price_per_sms, begin_date: @old_tariff.begin_date - 1.day, end_date: @old_tariff.end_date + 1.day}
    check_cross(params)
  end

  test "should not create NT when NT begin-date is equal OT begin-date and NT end-date is older OT end-date" do
    params = {operator: @old_tariff.operator, price_per_sms: @old_tariff.price_per_sms, begin_date: @old_tariff.begin_date, end_date: @old_tariff.end_date + 1.day}
    check_cross(params)
  end

  test "should not create NT when NT begin-date is ealier OT begin-date and NT end-date is equal OT begin-date" do
    params = {operator: @old_tariff.operator, price_per_sms: @old_tariff.price_per_sms, begin_date: @old_tariff.begin_date - 1.year, end_date: @old_tariff.begin_date}
    check_cross(params)
  end

  test "should not create NT when NT begin-date is ealier OT begin-date and NT end-date is inside OT" do
    params = {operator: @old_tariff.operator, price_per_sms: @old_tariff.price_per_sms, begin_date: @old_tariff.begin_date - 1.year, end_date: @old_tariff.begin_date + 1.day}
    check_cross(params)
  end

  test "should not create NT when NT begin-date is ealier OT begin-date and NT end-date is equal OT end-date" do
    params = {operator: @old_tariff.operator, price_per_sms: @old_tariff.price_per_sms, begin_date: @old_tariff.begin_date - 1.year, end_date: @old_tariff.end_date}
    check_cross(params)
  end

  test "should not create NT when NT inside OT" do
    params = {operator: @old_tariff.operator, price_per_sms: @old_tariff.price_per_sms, begin_date: @old_tariff.begin_date + 1.day, end_date: @old_tariff.end_date - 1.day}
    check_cross(params)
  end

  #success
  test "should create NT when NT is older than OT" do
    params = {operator: @old_tariff.operator, price_per_sms: @old_tariff.price_per_sms, begin_date: @old_tariff.end_date + 1.year, end_date: @old_tariff.end_date + 2.year}
    check_not_cross(params)
  end

  test "should create NT when NT is ealier than OT" do
    params = {operator: @old_tariff.operator, price_per_sms: @old_tariff.price_per_sms, begin_date: @old_tariff.begin_date - 2.year, end_date: @old_tariff.begin_date - 1.year}
    check_not_cross(params)
  end

end
