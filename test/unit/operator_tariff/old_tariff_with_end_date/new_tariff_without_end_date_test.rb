# -*- encoding : utf-8 -*-
require 'unit/operator_tariff/shared'

class OperatorTariff::OldTariffWithEndDateNewTariffWithoutEndDateTest < OperatorTariff::Shared
  def setup
    super

    @old_tariff = FactoryGirl.create(:mts_tariff, begin_date: Date.today - 1.year, end_date: Date.today + 1.year)
  end

  test "should not create NT when NT begin-date is equal OT end-date" do
    params = {operator: @old_tariff.operator, price_per_sms: @old_tariff.price_per_sms, begin_date: @old_tariff.end_date, end_date: nil}
    check_cross(params)
  end

  test "should not create NT when NT begin-date is between OT begin-date and end-date" do
    params = {operator: @old_tariff.operator, price_per_sms: @old_tariff.price_per_sms, begin_date: Date.today, end_date: nil}
    check_cross(params)
  end

  test "should not create NT when NT begin-date is equal OT begin-date" do
    params = {operator: @old_tariff.operator, price_per_sms: @old_tariff.price_per_sms, begin_date: @old_tariff.begin_date, end_date: nil}
    check_cross(params)
  end

  test "should not create NT when NT begin-date is eqlier than OT begin-date" do
    params = {operator: @old_tariff.operator, price_per_sms: @old_tariff.price_per_sms, begin_date: @old_tariff.begin_date - 1.year, end_date: nil}
    check_cross(params)
  end

  #success
  test "should create NT when NT begin-date is older than OT end-date" do
    params = {operator: @old_tariff.operator, price_per_sms: @old_tariff.price_per_sms, begin_date: @old_tariff.end_date + 1.year, end_date: nil}
    check_not_cross(params)
  end

end
