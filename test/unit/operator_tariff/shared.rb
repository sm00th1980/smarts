# -*- encoding : utf-8 -*-
require 'test_helper'

class OperatorTariff::Shared < ActiveSupport::TestCase

  def setup
    clear_db

    OperatorTariff.delete_all
  end

  def teardown
    OperatorTariff.destroy_all
  end

  private
  def check_cross(params)
    assert_no_difference 'OperatorTariff.count' do
      new_tariff = OperatorTariff.new(params)
      refute new_tariff.valid?
      assert_equal [I18n.t('validation.operator_tariff.cross_tariff')], new_tariff.errors.messages[:base]
    end

    assert_no_difference 'OperatorTariff.count' do
      OperatorTariff.create(params)
    end

    assert_raise ActiveRecord::RecordInvalid do
      OperatorTariff.create!(params)
    end
  end

  def check_not_cross(params)
    check_new(params)
    check_create(params)
    check_create!(params)
  end

  def check_new(params)
    new_tariff = nil
    assert_difference 'OperatorTariff.count' do
      assert_nothing_raised do
        new_tariff = OperatorTariff.new(params)
        assert new_tariff.valid?
        assert new_tariff.errors.messages[:base].nil?
        new_tariff.save!
      end
    end

    new_tariff.destroy
  end

  def check_create(params)
    new_tariff = nil
    assert_difference 'OperatorTariff.count' do
      new_tariff = OperatorTariff.create(params)
    end

    new_tariff.destroy
  end

  def check_create!(params)
    new_tariff = nil
    assert_nothing_raised do
      new_tariff = OperatorTariff.create!(params)
    end

    new_tariff.destroy
  end

end
