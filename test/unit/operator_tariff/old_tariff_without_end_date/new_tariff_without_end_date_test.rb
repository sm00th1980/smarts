# -*- encoding : utf-8 -*-
require 'unit/operator_tariff/shared'

class OperatorTariff::OldTariffWithoutEndDateNewTariffWithoutEndDateTest < OperatorTariff::Shared
  def setup
    super

    @old_tariff = FactoryGirl.create(:mts_tariff, begin_date: Date.today, end_date: nil)
  end

  test "should not create new tariff when new tariff has begin-date is ealier then old tariff begin-date" do
    params = {operator: @old_tariff.operator, price_per_sms: @old_tariff.price_per_sms, begin_date: @old_tariff.begin_date - 1.year, end_date: nil}
    check_cross(params)
  end

  test "should not create new tariff when new tariff has begin-date is older then old tariff begin-date" do
    params = {operator: @old_tariff.operator, price_per_sms: @old_tariff.price_per_sms, begin_date: @old_tariff.begin_date + 1.year, end_date: nil}
    check_cross(params)
  end

  test "should not create new tariff when new tariff has begin-date is equal old tariff begin-date" do
    params = {operator: @old_tariff.operator, price_per_sms: @old_tariff.price_per_sms, begin_date: @old_tariff.begin_date, end_date: nil}
    check_cross(params)
  end

end
