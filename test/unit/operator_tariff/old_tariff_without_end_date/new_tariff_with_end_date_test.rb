# -*- encoding : utf-8 -*-
require 'unit/operator_tariff/shared'

class OperatorTariff::OldTariffWithoutEndDateNewTariffWithEndDateTest < OperatorTariff::Shared
  def setup
    super

    @old_tariff = FactoryGirl.create(:mts_tariff, begin_date: Date.today, end_date: nil)
  end

  #failure
  test "should not create NT when NT is older then OT" do
    params = {operator: @old_tariff.operator, price_per_sms: @old_tariff.price_per_sms, begin_date: @old_tariff.begin_date + 1.year, end_date: @old_tariff.begin_date + 2.year}
    check_cross(params)
  end

  test "should not create NT when NT begin-date is ealier then OT begin-date and NT end-date is older then OT begin-date" do
    params = {operator: @old_tariff.operator, price_per_sms: @old_tariff.price_per_sms, begin_date: @old_tariff.begin_date - 1.year, end_date: @old_tariff.begin_date + 1.year}
    check_cross(params)
  end

  test "should not create NT when NT begin-date is equal OT begin-date and NT end-date is older then OT begin-date" do
    params = {operator: @old_tariff.operator, price_per_sms: @old_tariff.price_per_sms, begin_date: @old_tariff.begin_date, end_date: @old_tariff.begin_date + 1.year}
    check_cross(params)
  end

  test "should not create NT when NT begin-date is ealier than OT begin-date and NT end-date is equal OT begin-date" do
    params = {operator: @old_tariff.operator, price_per_sms: @old_tariff.price_per_sms, begin_date: @old_tariff.begin_date - 1.year, end_date: @old_tariff.begin_date}
    check_cross(params)
  end

  #success
  test "should create NT when NT is ealier than OT" do
    params = {operator: @old_tariff.operator, price_per_sms: @old_tariff.price_per_sms, begin_date: @old_tariff.begin_date - 2.year, end_date: @old_tariff.begin_date - 1.year}
    check_not_cross(params)
  end

end
