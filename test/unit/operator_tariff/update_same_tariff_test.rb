# -*- encoding : utf-8 -*-
require 'unit/operator_tariff/shared'

class OperatorTariff::UpdateSameTariffTest < OperatorTariff::Shared

  test "should update tariff without end-date to without end-date" do
    tariff = FactoryGirl.create(:mts_tariff, begin_date: Date.today, end_date: nil)
    params = {price_per_sms: tariff.price_per_sms, begin_date: tariff.begin_date - 1.year, end_date: nil}
    check_updated(tariff, params)
  end

  test "should update tariff without end-date to with end-date" do
    tariff = FactoryGirl.create(:mts_tariff, begin_date: Date.today, end_date: nil)
    params = {price_per_sms: tariff.price_per_sms, begin_date: tariff.begin_date - 1.year, end_date: Date.today + 1.year}
    check_updated(tariff, params)
  end

  test "should update tariff with end-date to with end-date" do
    tariff = FactoryGirl.create(:mts_tariff, begin_date: Date.today, end_date: Date.today + 1.year)
    params = {price_per_sms: tariff.price_per_sms, begin_date: tariff.begin_date - 1.year, end_date: tariff.end_date + 1.year}
    check_updated(tariff, params)
  end

  test "should update tariff with end-date to without end-date" do
    tariff = FactoryGirl.create(:mts_tariff, begin_date: Date.today, end_date: Date.today + 1.year)
    params = {price_per_sms: tariff.price_per_sms, begin_date: tariff.begin_date - 1.year, end_date: nil}
    check_updated(tariff, params)
  end

  private
  def check_updated(tariff, params)
    tariff.price_per_sms = params[:price_per_sms]
    tariff.begin_date = params[:begin_date]
    tariff.end_date = params[:end_date]

    assert tariff.valid?
    assert tariff.errors.messages[:base].nil?

    assert_nothing_raised do
      tariff.save!
    end
  end

end
