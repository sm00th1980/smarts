# -*- encoding : utf-8 -*-
require 'test_helper'

class OperatorTariff::BeginDateAndEndDateTest < ActiveSupport::TestCase
  def setup
    clear_db
    OperatorTariff.delete_all
  end

  def teardown
    OperatorTariff.destroy_all
  end

  #failure
  test "should not create new tariff when end date is ealier than begin date" do
    begin_date = Date.today
    end_date = begin_date - 1.year

    assert_operator begin_date, :>, end_date
    params = {operator: CellOperatorGroup.mts, price_per_sms: rand(100)/100.to_f, begin_date: begin_date, end_date: end_date}
    check_not_valid(params)
  end

  #success
  test "should create new tariff when end-date is older than begin date" do
    begin_date = Date.today
    end_date = begin_date + 1.year

    assert_operator begin_date, :<, end_date
    params = {operator: CellOperatorGroup.mts, price_per_sms: rand(100)/100.to_f, begin_date: begin_date, end_date: end_date}
    check_valid(params)
  end

  test "should create new tariff when end-date is nil" do
    begin_date = Date.today
    end_date = nil

    params = {operator: CellOperatorGroup.mts, price_per_sms: rand(100)/100.to_f, begin_date: begin_date, end_date: end_date}
    check_valid(params)
  end

  test "should create new tariff when end-date is equal begin_date" do
    begin_date = Date.today
    end_date = begin_date

    assert_equal begin_date, end_date
    params = {operator: CellOperatorGroup.mts, price_per_sms: rand(100)/100.to_f, begin_date: begin_date, end_date: end_date}
    check_valid(params)
  end

  private
  def check_not_valid(params)
    assert_no_difference 'OperatorTariff.count' do
      new_tariff = OperatorTariff.new(params)
      refute new_tariff.valid?
      assert_equal [I18n.t('validation.operator_tariff.end_date_is_ealier_than_begin_date')], new_tariff.errors.messages[:base]
    end

    assert_no_difference 'OperatorTariff.count' do
      OperatorTariff.create(params)
    end

    assert_raise ActiveRecord::RecordInvalid do
      OperatorTariff.create!(params)
    end
  end

  def check_valid(params)
    check_new(params)
    check_create(params)
    check_create!(params)
  end

  def check_new(params)
    new_tariff = nil
    assert_difference 'OperatorTariff.count' do
      assert_nothing_raised do
        new_tariff = OperatorTariff.new(params)
        assert new_tariff.valid?
        assert new_tariff.errors.messages[:base].nil?
        new_tariff.save!
      end
    end

    new_tariff.destroy
  end

  def check_create(params)
    new_tariff = nil
    assert_difference 'OperatorTariff.count' do
      new_tariff = OperatorTariff.create(params)
    end

    new_tariff.destroy
  end

  def check_create!(params)
    new_tariff = nil
    assert_nothing_raised do
      new_tariff = OperatorTariff.create!(params)
    end

    new_tariff.destroy
  end

end
