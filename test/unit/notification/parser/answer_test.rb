# -*- encoding : utf-8 -*-
require 'test_helper'

class Notification::AnswerTest < ActiveSupport::TestCase

  test "should parse answer1" do
    assert_equal(
        "id:464759203534209126 sub:001 dlvrd:001 submit date:1411092346 done date:1411092346 stat:DELIVRD err:000 text:\u0005\u0000\u0003�\u0004\u0001\u0004\u001F\u0004>\u00047\u00044\u0004@\u00040\u00042",
        Notification::Parser.answer('smsid=123&type=1&answer=id%3A464759203534209126+sub%3A001+dlvrd%3A001+submit+date%3A1411092346+done+date%3A1411092346+stat%3ADELIVRD+err%3A000+text%3A%05%00%03%80%04%01%04%1F%04%3E%047%044%04%40%040%042')
    )
  end

  test "should parse answer2" do
    assert_equal(
        "id:467456007052722178 sub:001 dlvrd:001 submit date:1411241236 done date:1411241236 stat:DELIVRD err:000 text:\u0004\u0014\u00045\u0004\u0004L\u00043\u00048\u0000 \u0004A\u0004@\u00040",
        Notification::Parser.answer('smsid=63997172&type=1&answer=id%3A467456007052722178+sub%3A001+dlvrd%3A001+submit+date%3A1411241236+done+date%3A1411241236+stat%3ADELIVRD+err%3A000+text%3A%04%14%045%04%3D%04L%043%048%00+%04A%04%40%040')
    )
  end

  test "should parse answer3" do
    assert_equal(
        "id:466326155780161686 sub:001 dlvrd:001 submit date:1411181027 done date:1411181028 stat:UNDELIV err:000 text:\u0005\u0000\u0003�\u0002\u0001\u00001\u0000-\u00003\u0000 \u00044\u00045\u0004:",
        Notification::Parser.answer('smsid=63705974&type=2&answer=id%3A466326155780161686+sub%3A001+dlvrd%3A001+submit+date%3A1411181027+done+date%3A1411181028+stat%3AUNDELIV+err%3A000+text%3A%05%00%03%C1%02%01%001%00-%003%00+%044%045%04%3A')
    )
  end

end
