# -*- encoding : utf-8 -*-
require 'test_helper'

class Notification::CollectorByDeliveredViaBronzeInvalidTest < ActiveSupport::TestCase
  def setup
    clear_db

    user = FactoryGirl.create(:user_charge_by_delivered)
    @send_log = FactoryGirl.create(:just_send_log_via_bronze_channel, user: user).send_logs.first

    @key = 'kannel_1'
  end

  def teardown
    $redis.flushdb
  end

  test "should parse invalid-notification-1 and not update send-log" do
    str = invalid_notification1(@send_log.id)

    assert_difference "$redis.llen('%s')" % @key, -1 do
      assert_equal SendLogDeliveryStatus.unknown, @send_log.delivery_status
      assert_equal 0, @send_log.reload.price
      assert_equal SendLogDeliveryError.bronze_status_unknown, @send_log.delivery_error

      #обрабатываем полученную нотификацию
      Notification::Collector.new(@key).perform

      @send_log.reload

      assert_equal SendLogDeliveryStatus.unknown, @send_log.delivery_status
      assert_equal 0, @send_log.price
      assert_equal SendLogDeliveryError.bronze_error_unknown, @send_log.delivery_error
    end
  end

  test "should parse invalid-notification-2 and not update send-log" do
    str = invalid_notification2(@send_log.id)

    assert_difference "$redis.llen('%s')" % @key, -1 do
      assert_equal SendLogDeliveryStatus.unknown, @send_log.delivery_status
      assert_equal 0, @send_log.reload.price
      assert_equal SendLogDeliveryError.bronze_status_unknown, @send_log.delivery_error

      #обрабатываем полученную нотификацию
      Notification::Collector.new(@key).perform

      @send_log.reload

      assert_equal SendLogDeliveryStatus.unknown, @send_log.delivery_status
      assert_equal 0, @send_log.price
      assert_equal SendLogDeliveryError.bronze_error_unknown, @send_log.delivery_error
    end
  end

  test "should parse invalid-notification-3 and not update send-log" do
    str = invalid_notification3(@send_log.id)

    assert_difference "$redis.llen('%s')" % @key, -1 do
      assert_equal SendLogDeliveryStatus.unknown, @send_log.delivery_status
      assert_equal 0, @send_log.reload.price
      assert_equal SendLogDeliveryError.bronze_status_unknown, @send_log.delivery_error

      #обрабатываем полученную нотификацию
      Notification::Collector.new(@key).perform

      @send_log.reload

      assert_equal SendLogDeliveryStatus.unknown, @send_log.delivery_status
      assert_equal 0, @send_log.price
      assert_equal SendLogDeliveryError.bronze_status_unknown, @send_log.delivery_error
    end
  end

  test "should parse invalid-notification-4 and not update send-log" do
    str = invalid_notification4(@send_log.id)

    assert_difference "$redis.llen('%s')" % @key, -1 do
      assert_equal SendLogDeliveryStatus.unknown, @send_log.delivery_status
      assert_equal 0, @send_log.reload.price
      assert_equal SendLogDeliveryError.bronze_status_unknown, @send_log.delivery_error

      #обрабатываем полученную нотификацию
      Notification::Collector.new(@key).perform

      @send_log.reload

      assert_equal SendLogDeliveryStatus.unknown, @send_log.delivery_status
      assert_equal 0, @send_log.price
      assert_equal SendLogDeliveryError.bronze_status_unknown, @send_log.delivery_error
    end
  end

  private
  def invalid_notification1(smsid)
    fill("smsid=#{smsid}&type=&")
  end

  def invalid_notification2(smsid)
    fill("smsid=#{smsid}&typ")
  end

  def invalid_notification3(smsid)
    fill("")
  end

  def invalid_notification4(smsid)
    fill("-1")
  end

  def fill(str)
    choose_db do
      $redis.rpush(@key, str)
    end

    str
  end

  def choose_db(&block)
    $redis.select(Notification::Collector::NOTIFY_DB)
    yield
  end

end
