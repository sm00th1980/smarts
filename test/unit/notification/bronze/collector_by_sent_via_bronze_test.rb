# -*- encoding : utf-8 -*-
require 'test_helper'

class Notification::CollectorBySentViaBronzeTest < ActiveSupport::TestCase
  def setup
    clear_db

    user = FactoryGirl.create(:user_charge_by_sent)
    @send_log = FactoryGirl.create(:just_send_log_via_bronze_channel, user: user).send_logs.first

    @key = 'kannel_4'
  end

  def teardown
    $redis.flushdb
  end

  test "should parse delivered notification and update send-log" do
    delivered_notification(@send_log.id)

    assert_difference "$redis.llen('%s')" % @key, -1 do
      assert_equal SendLogDeliveryStatus.unknown, @send_log.delivery_status
      assert_equal @send_log.log.user.price_per_sms * @send_log.log.sms_count, @send_log.price
      assert_equal SendLogDeliveryError.bronze_status_unknown, @send_log.delivery_error

      #обрабатываем полученную нотификацию
      Notification::Collector.new(@key).perform

      @send_log.reload

      assert_equal SendLogDeliveryStatus.delivered, @send_log.delivery_status
      assert_equal @send_log.log.user.price_per_sms * @send_log.log.sms_count, @send_log.price
      assert_equal SendLogDeliveryError.bronze_no_error, @send_log.delivery_error
    end
  end

  test "should parse undelivered notification and update send-log" do
    undelivered_notification(@send_log.id)

    assert_difference "$redis.llen('%s')" % @key, -1 do
      assert_equal SendLogDeliveryStatus.unknown, @send_log.delivery_status
      assert_equal @send_log.log.user.price_per_sms * @send_log.log.sms_count, @send_log.price
      assert_equal SendLogDeliveryError.bronze_status_unknown, @send_log.delivery_error

      #обрабатываем полученную нотификацию
      Notification::Collector.new(@key).perform

      @send_log.reload

      assert_equal SendLogDeliveryStatus.failured, @send_log.delivery_status
      assert_equal @send_log.log.user.price_per_sms * @send_log.log.sms_count, @send_log.price
      assert_equal SendLogDeliveryError.bronze_error_unknown_subscriber, @send_log.delivery_error
    end
  end

  test "should parse rejected notification and update send-log" do
    rejected_notification(@send_log.id)

    assert_difference "$redis.llen('%s')" % @key, -1 do
      assert_equal SendLogDeliveryStatus.unknown, @send_log.delivery_status
      assert_equal @send_log.log.user.price_per_sms * @send_log.log.sms_count, @send_log.price
      assert_equal SendLogDeliveryError.bronze_status_unknown, @send_log.delivery_error

      #обрабатываем полученную нотификацию
      Notification::Collector.new(@key).perform

      @send_log.reload

      assert_equal SendLogDeliveryStatus.failured, @send_log.delivery_status
      assert_equal @send_log.log.user.price_per_sms * @send_log.log.sms_count, @send_log.price
      assert_equal SendLogDeliveryError.bronze_vlr_timeout, @send_log.delivery_error
    end
  end

  test "should parse ack notification and update send-log" do
    ack_notification(@send_log.id)

    assert_difference "$redis.llen('%s')" % @key, -1 do
      assert_equal SendLogDeliveryStatus.unknown, @send_log.delivery_status
      assert_equal @send_log.log.user.price_per_sms * @send_log.log.sms_count, @send_log.price
      assert_equal SendLogDeliveryError.bronze_status_unknown, @send_log.delivery_error

      #обрабатываем полученную нотификацию
      Notification::Collector.new(@key).perform

      @send_log.reload

      assert_equal SendLogDeliveryStatus.submit, @send_log.delivery_status
      assert_equal @send_log.log.user.price_per_sms * @send_log.log.sms_count, @send_log.price
      assert_equal SendLogDeliveryError.bronze_error_unknown, @send_log.delivery_error
    end
  end

  test "should parse nack-3840 notification and update send-log" do
    nack_3840_notification(@send_log.id)

    assert_difference "$redis.llen('%s')" % @key, -1 do
      assert_equal SendLogDeliveryStatus.unknown, @send_log.delivery_status
      assert_equal @send_log.log.user.price_per_sms * @send_log.log.sms_count, @send_log.price
      assert_equal SendLogDeliveryError.bronze_status_unknown, @send_log.delivery_error

      #обрабатываем полученную нотификацию
      Notification::Collector.new(@key).perform

      @send_log.reload

      assert_equal SendLogDeliveryStatus.rejected, @send_log.delivery_status
      assert_equal @send_log.log.user.price_per_sms * @send_log.log.sms_count, @send_log.price
      assert_equal SendLogDeliveryError.bronze_no_server_link_route, @send_log.delivery_error
    end
  end

  test "should parse nack-3841 notification and update send-log" do
    nack_3841_notification(@send_log.id)

    assert_difference "$redis.llen('%s')" % @key, -1 do
      assert_equal SendLogDeliveryStatus.unknown, @send_log.delivery_status
      assert_equal @send_log.log.user.price_per_sms * @send_log.log.sms_count, @send_log.price
      assert_equal SendLogDeliveryError.bronze_status_unknown, @send_log.delivery_error

      #обрабатываем полученную нотификацию
      Notification::Collector.new(@key).perform

      @send_log.reload

      assert_equal SendLogDeliveryStatus.rejected, @send_log.delivery_status
      assert_equal @send_log.log.user.price_per_sms * @send_log.log.sms_count, @send_log.price
      assert_equal SendLogDeliveryError.bronze_spam_filter_action, @send_log.delivery_error
    end
  end

  private
  def delivered_notification(smsid)
    fill("smsid=#{smsid}&type=1&answer=id%3A1361139222252600002+sub%3A001+dlvrd%3A001+submit+date%3A1402111017+done+date%3A1402111658+stat%3ADELIVRD+err%3A000+text%3A")
  end

  def undelivered_notification(smsid)
    fill("smsid=#{smsid}&type=2&answer=id%3A1361141427821500001+sub%3A001+dlvrd%3A001+submit+date%3A1402111624+done+date%3A1402111624+stat%3AUNDELIV+err%3A001+text%3A")
  end

  def rejected_notification(smsid)
    fill("smsid=#{smsid}&type=2&answer=id%3A1361141431183200002+sub%3A001+dlvrd%3A001+submit+date%3A1402111625+done+date%3A1402111625+stat%3AREJECTD+err%3A202+text%3A")
  end

  def ack_notification(smsid)
    fill("smsid=#{smsid}&type=8&answer=ACK%2F")
  end

  def nack_3840_notification(smsid)
    fill("smsid=#{smsid}&type=16&answer=NACK%2F3840%2FUnknown%2FReserved")
  end

  def nack_3841_notification(smsid)
    fill("smsid=#{smsid}&type=16&answer=NACK%2F3841%2FUnknown%2FReserved")
  end

  def fill(str)
    choose_db do
      $redis.rpush(@key, str)
    end
  end

  def choose_db(&block)
    $redis.select(Notification::Collector::NOTIFY_DB)
    yield
  end

end
