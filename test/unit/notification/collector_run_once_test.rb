# -*- encoding : utf-8 -*-
require 'test_helper'

class Notification::CollectorRunOnceTest < ActiveSupport::TestCase
  def setup
    @key = "kannel_#{rand(100_000_000)}"
    @lock_file = Notification::Collector.new(@key).lock_file
  end

  test "should not run if running already" do
    file = File.open(Rails.root.join(@lock_file).to_s, File::RDWR|File::CREAT, 0644)
    file.flock(File::LOCK_EX|File::LOCK_NB)

    exception = assert_raises(RuntimeError) do
      Notification::Collector.new(@key).perform
    end

    assert_equal I18n.t('collector.once_run_exception') % @key, exception.message

    file.close
  end

  test "should run if not lock" do
    file = File.open(Rails.root.join(@lock_file).to_s, File::RDWR|File::CREAT, 0644)
    file.flock(File::LOCK_EX|File::LOCK_NB)
    file.close

    assert_nothing_raised do
      Notification::Collector.new(@key).perform
    end
  end

  test "should be able lock after run" do
    Notification::Collector.new(@key).perform

    file = File.open(Rails.root.join(@lock_file).to_s, File::RDWR|File::CREAT, 0644)
    assert_equal 0, file.flock(File::LOCK_EX|File::LOCK_NB)
    file.close
  end

  test "should be able run new collector for different_key" do
    file = File.open(Rails.root.join(@lock_file).to_s, File::RDWR|File::CREAT, 0644)
    file.flock(File::LOCK_EX|File::LOCK_NB)

    different_key = "#{@key}_#{rand(1000).to_s}"

    assert_nothing_raised do
      Notification::Collector.new(different_key).perform
    end
  end
end
