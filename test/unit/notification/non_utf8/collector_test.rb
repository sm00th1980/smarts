# -*- encoding : utf-8 -*-
require 'test_helper'

class Notification::NonUtf8CollectorTest < ActiveSupport::TestCase
  def setup
    clear_db

    user = FactoryGirl.create(:user_charge_by_sent)
    @send_log = FactoryGirl.create(:just_send_log_via_gold_channel, user: user).send_logs.first

    @key = 'kannel_4'
  end

  def teardown
    $redis.flushdb
  end

  test "should parse delivered notification with non utf8 symbols and update send-log" do
    delivered_notification_with_non_utf8(@send_log.id)

    assert_difference "$redis.llen('%s')" % @key, -1 do
      assert_equal SendLogDeliveryStatus.unknown, @send_log.delivery_status
      assert_equal Message.price_per_sms(Channel.gold, @send_log.log.user, @send_log.phone) * Message.sms_count(@send_log.log.content), @send_log.reload.price
      assert_equal SendLogDeliveryError.gold_status_unknown, @send_log.delivery_error

      #обрабатываем полученную нотификацию
      Notification::Collector.new(@key).perform

      @send_log.reload

      assert_equal SendLogDeliveryStatus.delivered, @send_log.delivery_status
      assert_equal Message.price_per_sms(Channel.gold, @send_log.log.user, @send_log.phone) * Message.sms_count(@send_log.log.content), @send_log.reload.price
      assert_equal SendLogDeliveryError.gold_no_error, @send_log.delivery_error
    end
  end

  private
  def delivered_notification_with_non_utf8(smsid)
    fill("smsid=#{smsid}&type=1&answer=id%3A464759203534209126+sub%3A001+dlvrd%3A001+submit+date%3A1411092346+done+date%3A1411092346+stat%3ADELIVRD+err%3A000+text%3A%05%00%03%80%04%01%04%1F%04%3E%047%044%04%40%040%042")
  end

  def fill(str)
    choose_db do
      $redis.rpush(@key, str)
    end
  end

  def choose_db(&block)
    $redis.select(Notification::Collector::NOTIFY_DB)
    yield
  end

end
