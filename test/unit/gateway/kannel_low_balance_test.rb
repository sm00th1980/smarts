# -*- encoding : utf-8 -*-
require 'test_helper'
include GatewayHelper
include TimeHelper

class Gateway::KannelLowbalanceTest < ActiveSupport::TestCase
  def setup
    clear_db

    @user_prepaid = FactoryGirl.create(:user_with_negative_balance, payment_type: PaymentType.prepaid)
    @user_postpaid = FactoryGirl.create(:user_with_negative_balance, payment_type: PaymentType.postpaid)

    @valid_phone = '79379903835'
    content = 'message'

    @log_prepaid = FactoryGirl.create(:log_fresh, user: @user_prepaid, sender_name_value: @user_prepaid.active_sender_name_value, content: content, phones: [@valid_phone], fake: false)
    @log_postpaid = FactoryGirl.create(:log_fresh, user: @user_postpaid, sender_name_value: @user_postpaid.active_sender_name_value, content: content, phones: [@valid_phone], fake: false)
  end

  test "should not send sms for prepaid user with low balance" do
    begin_date = Log.where(user_id: @user_prepaid.id).order(:created_at)[0].created_at.to_date
    end_date = Log.where(user_id: @user_prepaid.id).order('created_at DESC')[0].created_at.to_date

    Calculator::Logs.recalculate(begin_date, end_date)

    stub_real_request do
      Gateway::Kannel.send_by_http(@valid_phone, @log_prepaid)
      send_log = SendLog.last

      assert_equal SendLogDeliveryStatus.low_balance, send_log.delivery_status
      assert_equal SendLogDeliveryError::Any.low_balance, send_log.delivery_error

      assert_equal 0, send_log.price
    end
  end

  test "should send sms for postpaid user with low balance" do
    stub_real_request do
      check_sent_sms do
        Gateway::Kannel.send_by_http(@valid_phone, @log_postpaid)
        send_log = SendLog.last

        assert_equal SendLogDeliveryStatus.unknown, send_log.delivery_status
        assert_equal SendLogDeliveryError::Any.unknown_status, send_log.delivery_error

        _channel = Channel.channel(@log_postpaid.sender_name_value, @log_postpaid.user.alpha_megafon_channel, @valid_phone)

        _price = Message.sms_count(@log_postpaid.content) * Message.price_per_sms(_channel, @log_postpaid.user, @valid_phone)

        assert_equal _price, send_log.price
      end
    end
  end

  def check_sent_sms(&block)
    assert_difference 'SendLog.count' do
      yield if block_given?
    end
  end
end
