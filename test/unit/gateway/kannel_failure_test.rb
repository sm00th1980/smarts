# -*- encoding : utf-8 -*-
require 'test_helper'
include GatewayHelper
include TimeHelper

class Gateway::KannelFailureTest < ActiveSupport::TestCase
  def setup
    clear_db

    user = FactoryGirl.create(:user)

    @valid_phone = '79379903835'
    content = 'message'

    @log_fresh = FactoryGirl.create(:log_fresh, user: user, sender_name_value: user.active_sender_name_value, content: content, phones: [@valid_phone], fake: false)
  end

  test "should send email if kannel failed" do
    params = {event: I18n.t('admin.kannel.failure')}
    stub_real_request_with_error do
      check_new_task_created('Admin', 'notify', params) do
        Gateway::Kannel.real_delivery(@log_fresh, rand(10), @valid_phone)
      end
    end

  end

end
