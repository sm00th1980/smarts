# -*- encoding : utf-8 -*-
require 'test_helper'
include GatewayHelper

class Gateway::KannelPriceTest < ActiveSupport::TestCase
  def setup
    clear_db

    @valid_phone = '79379903835'
    @content = 'message'
    @gold_channel_sender_name = 'goldchannel'
    @bronze_channel_sender_name = '79379903835'
  end

  #by delivered and gold channel
  test "should send sms with correct price for user by-delivered and gold channel" do
    stub_real_request do
      user = FactoryGirl.create(:user_charge_by_delivered)
      log_fresh = FactoryGirl.create(:log_fresh, user: user, sender_name_value: @gold_channel_sender_name, content: @content, phones: [@valid_phone], fake: false)

      Gateway::Kannel.send_by_http(@valid_phone, log_fresh)
      send_log = SendLog.where(log_id: log_fresh.id, phone: @valid_phone).first

      #проверка ценника
      assert_equal Message.price_per_sms(Channel.gold, user, @valid_phone), send_log.price
      assert_operator send_log.price, :>, 0
    end
  end

  #by sent and gold channel
  test "should send sms with correct price for user by-sent and gold channel" do
    stub_real_request do

      user = FactoryGirl.create(:user_charge_by_sent)
      log_fresh = FactoryGirl.create(:log_fresh, user: user, sender_name_value: @gold_channel_sender_name, content: @content, phones: [@valid_phone], fake: false)

      Gateway::Kannel.send_by_http(@valid_phone, log_fresh)
      send_log = SendLog.where(log_id: log_fresh.id, phone: @valid_phone).first

      #проверка ценника
      assert_equal Message.price_per_sms(Channel.gold, user, @valid_phone), send_log.price
      assert_operator send_log.price, :>, 0
    end
  end

  #by delivered and bronze channel
  test "should send sms with correct price for user by-delivered and bronze channel" do
    stub_real_request do

      user = FactoryGirl.create(:user_charge_by_delivered)
      log_fresh = FactoryGirl.create(:log_fresh, user: user, sender_name_value: @bronze_channel_sender_name, content: @content, phones: [@valid_phone], fake: false)

      Gateway::Kannel.send_by_http(@valid_phone, log_fresh)
      send_log = SendLog.where(log_id: log_fresh.id, phone: @valid_phone).first

      #проверка ценника
      assert_equal Message.price_per_sms(Channel.bronze, user, @valid_phone), send_log.price
      assert_equal 0, send_log.price
    end
  end

  #by sent and bronze channel
  test "should send sms with correct price for user by-sent and bronze channel" do
    stub_real_request do

      user = FactoryGirl.create(:user_charge_by_sent)
      log_fresh = FactoryGirl.create(:log_fresh, user: user, sender_name_value: @bronze_channel_sender_name, content: @content, phones: [@valid_phone], fake: false)

      Gateway::Kannel.send_by_http(@valid_phone, log_fresh)
      send_log = SendLog.where(log_id: log_fresh.id, phone: @valid_phone).first

      #проверка ценника
      assert_equal Message.price_per_sms(Channel.bronze, user, @valid_phone), send_log.price
      assert_operator send_log.price, :>, 0
    end
  end

end
