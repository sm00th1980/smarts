# -*- encoding : utf-8 -*-
require 'test_helper'
include GatewayHelper

class Gateway::BronzeByDeliveredTest < ActiveSupport::TestCase
  def setup
    clear_db

    user = FactoryGirl.create(:user_charge_by_delivered)
    @valid_phone = 79379903835
    content = 'message'
    def_sender_name_value = FactoryGirl.create(:sender_name_accepted_with_verified_value, user: user, value: '79379903835').value

    @log_fresh = FactoryGirl.create(:log_fresh, user: user, sender_name_value: def_sender_name_value, content: content, phones: [@valid_phone], fake: false)
    @log_fresh_with_group = FactoryGirl.create(:log_fresh, user: user, sender_name_value: def_sender_name_value, content: content, group_ids: [user.groups.first.id], fake: false)
    @log_fresh_with_long_content = FactoryGirl.create(:log_fresh_with_long_content, user: user, sender_name_value: def_sender_name_value, phones: [@valid_phone], fake: false)
  end

  test "should send sms via http" do
    stub_real_request do
      Gateway::Kannel.send_by_http(@valid_phone, @log_fresh)
      send_log = SendLog.where(log_id: @log_fresh.id, phone: @valid_phone.to_s).first

      #проверка ценника
      assert_equal 0, send_log.price

      assert_equal SendLogDeliveryStatus.unknown, send_log.delivery_status
      assert_equal SendLogDeliveryError::Any.unknown_status, send_log.delivery_error
      assert_equal CellOperatorGroup.get_by_phone(@valid_phone), send_log.cell_operator_group
      assert_equal Channel.bronze, send_log.channel
    end
  end

  test "should send sms via http with group_ids" do
    client = @log_fresh_with_group.clients.first
    phone = client.phone

    stub_real_request do
      Gateway::Kannel.send_by_http(phone, @log_fresh_with_group)
      send_log = SendLog.where(log_id: @log_fresh_with_group.id, phone: phone.to_s).first

      #проверка ценника
      assert_equal 0, send_log.price

      assert_equal SendLogDeliveryStatus.unknown, send_log.delivery_status
      assert_equal SendLogDeliveryError::Any.unknown_status, send_log.delivery_error
      assert_equal CellOperatorGroup.get_by_phone(phone), send_log.cell_operator_group
      assert_equal Channel.bronze, send_log.channel
    end
  end

  test "should not send sms via http with error" do
    stub_real_request_with_error do
      Gateway::Kannel.send_by_http(@valid_phone, @log_fresh)
      send_log = SendLog.where(log_id: @log_fresh.id, phone: @valid_phone.to_s).first

      #проверка ценника
      assert_equal 0, send_log.price

      assert_equal SendLogDeliveryStatus.failured, send_log.delivery_status
      assert_equal SendLogDeliveryError::Any.kannel_failured, send_log.delivery_error
      assert_equal CellOperatorGroup.get_by_phone(@valid_phone), send_log.cell_operator_group
      assert_equal Channel.bronze, send_log.channel
    end
  end

  test "should not send sms via http with group_ids and error" do
    client = @log_fresh_with_group.clients.first
    phone = client.phone

    stub_real_request_with_error do
      Gateway::Kannel.send_by_http(phone, @log_fresh_with_group)
      send_log = SendLog.where(log_id: @log_fresh_with_group.id, phone: phone.to_s).first

      #проверка ценника
      assert_equal 0, send_log.price

      assert_equal SendLogDeliveryStatus.failured, send_log.delivery_status
      assert_equal SendLogDeliveryError::Any.kannel_failured, send_log.delivery_error
      assert_equal CellOperatorGroup.get_by_phone(phone), send_log.cell_operator_group
      assert_equal Channel.bronze, send_log.channel
    end
  end

  test "should count sms correctly for long content" do
    stub_real_request do
      Gateway::Kannel.send_by_http(@valid_phone, @log_fresh_with_long_content)
      send_log = SendLog.where(log_id: @log_fresh_with_long_content.id, phone: @valid_phone.to_s).first

      #проверка sms_count
      assert_equal 0, send_log.price

      assert_equal Message.sms_count(send_log.log.content), @log_fresh_with_long_content.sms_count

      assert_equal SendLogDeliveryStatus.unknown, send_log.delivery_status
      assert_equal SendLogDeliveryError::Any.unknown_status, send_log.delivery_error
      assert_equal CellOperatorGroup.get_by_phone(@valid_phone), send_log.cell_operator_group
      assert_equal Channel.bronze, send_log.channel
    end
  end
end
