# -*- encoding : utf-8 -*-
require 'test_helper'
include GatewayHelper
include TimeHelper

class Gateway::KannelNightSentTest < ActiveSupport::TestCase
  def setup
    clear_db

    user_with_permit_night_delivery = FactoryGirl.create(:user, permit_night_delivery: true)
    user_with_not_permit_night_delivery = FactoryGirl.create(:user, permit_night_delivery: false)

    @valid_phone = '79379903835'
    content = 'message'
    sender_name_value = FactoryGirl.create(:user).active_sender_name_value

    @log_permit = FactoryGirl.create(:log_fresh, user: user_with_permit_night_delivery, sender_name_value: sender_name_value, content: content, phones: [@valid_phone], fake: false)
    @log_not_permit = FactoryGirl.create(:log_fresh, user: user_with_not_permit_night_delivery, sender_name_value: sender_name_value, content: content, phones: [@valid_phone], fake: false)

    @day_time = Time.now.change(hour: BEGIN_HOUR, min: BEGIN_MIN, sec: BEGIN_SEC) + 1.hour
    @night_time = Time.now.change(hour: END_HOUR, min: END_MIN, sec: END_SEC) + 1.hour
  end

  #night time
  test "should not send sms at night time if user not permit night delivery" do
    stub_real_request do
      check_not_sent_sms do
        freeze(@night_time) do
          Gateway::Kannel.send_by_http(@valid_phone, @log_not_permit)
        end
      end
    end
  end

  test "should send sms at night time if user permit night delivery" do
    stub_real_request do
      check_sent_sms do
        freeze(@night_time) do
          Gateway::Kannel.send_by_http(@valid_phone, @log_permit)
        end
      end
    end
  end

  #day time
  test "should send sms at day time if user not permit night delivery" do
    stub_real_request do
      check_sent_sms do
        freeze(@day_time) do
          Gateway::Kannel.send_by_http(@valid_phone, @log_not_permit)
        end
      end
    end
  end

  test "should send sms at day time if user permit night delivery" do
    stub_real_request do
      check_sent_sms do
        freeze(@day_time) do
          Gateway::Kannel.send_by_http(@valid_phone, @log_permit)
        end
      end
    end
  end

  private
  def check_not_sent_sms(&block)
    assert_no_difference 'SendLog.count' do
      yield if block_given?
    end
  end

  def check_sent_sms(&block)
    assert_difference 'SendLog.count' do
      yield if block_given?
    end
  end
end
