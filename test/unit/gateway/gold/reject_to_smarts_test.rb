# -*- encoding : utf-8 -*-
require 'test_helper'
include GatewayHelper

class Gateway::GoldRejectToSmartsTest < ActiveSupport::TestCase
  def setup
    clear_db

    @user = FactoryGirl.create(:user_with_reject_to_smarts_via_gold_channel)

    @def_sender_name_value   = FactoryGirl.create(:sender_name_accepted_with_verified_value, user: @user, value: '79379903835').value
    @alpha_sender_name_value = FactoryGirl.create(:sender_name_accepted_with_verified_value, user: @user, value: 'alpha').value

    @smarts_phone = '79022900000'

    @log_def   = FactoryGirl.create(:log_fresh, user: @user, sender_name_value: @def_sender_name_value,   content: 'message', phones: [@smarts_phone], fake: false)
    @log_alpha = FactoryGirl.create(:log_fresh, user: @user, sender_name_value: @alpha_sender_name_value, content: 'message', phones: [@smarts_phone], fake: false)
  end

  test "should send sms to smarts with def sender name" do
    stub_real_request do
      check_sent_sms do
        Gateway::Kannel.send_by_http(@smarts_phone, @log_def)
        send_log = SendLog.last

        assert_equal Message.price_per_sms(Channel.bronze, @user, @smarts_phone)*send_log.log.sms_count, send_log.price
        assert_equal SendLogDeliveryStatus.unknown, send_log.delivery_status
        assert_equal SendLogDeliveryError::Any.unknown_status, send_log.delivery_error
        assert_equal Channel.bronze, send_log.channel
        assert_equal @log_def, send_log.log
        assert send_log.cell_operator_group.smarts?
        assert_equal @smarts_phone.to_s, send_log.phone.to_s
      end
    end
  end

  test "should not send sms to megafon with alpha sender name" do
    stub_real_request do
      check_sent_sms do
        Gateway::Kannel.send_by_http(@smarts_phone, @log_alpha)
        send_log = SendLog.last

        assert_equal SendLogDeliveryStatus.reject_to_smarts, send_log.delivery_status
        assert_equal SendLogDeliveryError.gold_reject_to_smarts, send_log.delivery_error
        assert_equal 0, send_log.price
        assert_equal Channel.gold, send_log.channel
        assert_equal @log_alpha, send_log.log
        assert send_log.cell_operator_group.smarts?
        assert_equal @smarts_phone.to_s, send_log.phone.to_s
      end
    end
  end

  def check_sent_sms(&block)
    assert_difference 'SendLog.count' do
      yield if block_given?
    end
  end
end
