# -*- encoding : utf-8 -*-
require 'test_helper'

class BirthdayCongratulationTest < ActiveSupport::TestCase
  def setup
    clear_db

    @user_with_birthday_message = FactoryGirl.create(:user)
    @user_without_birthday_message = FactoryGirl.create(:user_without_birthday_message)

    @message_id = rand(10000)
    @phone = '79379903835'

    stub_real_request
  end

  test "should not raise exception when run" do
    assert_nothing_raised do
      BirthdayCongratulation.run
    end
  end

  test "should return birthday_message" do
    @user_with_birthday_message.clients.each do |client|
      assert_not_nil BirthdayCongratulation.birthday_message(client)
      assert_equal @user_with_birthday_message.birthday_message % {fio: client.fio}, BirthdayCongratulation.birthday_message(client)
    end
  end

  test "should return default birthday_message for user without birthday_message" do
    @user_without_birthday_message.clients.each do |client|
      assert_not_nil BirthdayCongratulation.birthday_message(client)
      assert_equal BirthdayCongratulation::DEFAULT_BIRTHDAY_MESSAGE, BirthdayCongratulation.birthday_message(client)
    end
  end

  test "should return correct with template with percent in text" do
    user = FactoryGirl.create(:user_with_birthday_message_with_percent_in_template)
    client = user.clients.first

    assert_equal BirthdayCongratulation.replace_percent_chars(user.birthday_message) % {fio: client.fio, percent: '%'}, BirthdayCongratulation.birthday_message(client)
  end

  #replace_percents
  test "should correct replace percents" do
    assert_equal '20%{percent}', BirthdayCongratulation.replace_percent_chars('20%')
    assert_equal '20%{fio}', BirthdayCongratulation.replace_percent_chars('20%{fio}')
    assert_equal '20%{fio} %{percent}', BirthdayCongratulation.replace_percent_chars('20%{fio} %')
    assert_equal 'Дорогая %{fio}, поздравляем Вас с Днем рождения! Удачи Вам во всех делах и настоящего женского счастья! Ваша скидка - 20%{percent} ещё 2 недели.',
                 BirthdayCongratulation.replace_percent_chars('Дорогая %{fio}, поздравляем Вас с Днем рождения! Удачи Вам во всех делах и настоящего женского счастья! Ваша скидка - 20% ещё 2 недели.')
  end

  test "should sent sms to client with today birtday" do
    clear_db
    user = FactoryGirl.create(:user)
    user.birthday_sender_name = FactoryGirl.create(:sender_name_accepted_with_verified_value, user: user).value
    user.save!

    client = FactoryGirl.create(:client_with_today_birthday, group: user.groups.first)

    stub_real_request do
      Sidekiq::Testing.inline! do
        freeze(Time.now.change(hour: user.birthday_hour)) do

          BirthdayCongratulation.run

          log = Log.last

          assert_equal user, log.user
          assert_equal user.birthday_message, log.content
          assert_equal LogType.autodelivery, log.type
          assert_equal user.birthday_sender_name, log.sender_name_value
          assert_equal LogStatus.completed, log.status
          assert_equal Message.sms_count(user.birthday_message), log.sms_count
          assert_equal false, log.fake
          assert_equal [client.phone.to_s], log.phones.map { |phone| phone.to_s }
          assert_equal [], log.group_ids
        end
      end
    end
  end

  test "should not sent sms to client with today birtday when user is blocked" do
    clear_db
    user = FactoryGirl.create(:user_blocked)
    user.birthday_sender_name = FactoryGirl.create(:sender_name_accepted_with_verified_value, user: user).value
    user.save!

    client = FactoryGirl.create(:client_with_today_birthday, group: user.groups.first)

    stub_real_request do
      freeze(Time.now.change(hour: user.birthday_hour)) do
        assert_no_difference 'Log.count' do
          BirthdayCongratulation.run
        end
      end
    end
  end
end
