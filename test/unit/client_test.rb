# -*- encoding : utf-8 -*-
require 'test_helper'

class ClientTest < ActiveSupport::TestCase
  def setup
    clear_db
    @client = FactoryGirl.create(:client, group: FactoryGirl.create(:group, user: FactoryGirl.create(:user)))
  end

  def create_group
    @group = FactoryGirl.build :group, user:FactoryGirl.create(:user)
    [
        ['Попов Сергей',     79327448033],
        ['Протопопов Антон', 79382885033],
        ['Антонов Андрей',   79327392399]
    ].each { |values|
      @group.clients << FactoryGirl.create(:client,
                                          group:@group,
                                          fio:  values[0],
                                          phone:values[1]
      )
    }
    @group.save!
    @group
  end

  test 'should find needed records with number' do
    create_group
    assert_equal @group.clients.search(79327).size, 2
    assert_equal @group.clients.search('79327').size, 2
  end

  test 'should find needed records with names' do
    create_group
    assert_equal @group.clients.search('попов').size, 2
  end


  test "with_today_birthday should return client with today birthday" do
    FactoryGirl.create(:client_with_today_birthday, group: FactoryGirl.create(:group, user: FactoryGirl.create(:user)))

     assert_equal 1, Client.with_today_birthday.count
     Client.with_today_birthday.each do |client|
       assert client.birthday_congratulation
       assert_equal Date.today.month, client.birthday.month
       assert_equal Date.today.day, client.birthday.day
     end
  end

  test "with_today_birthday should not return client with birthday_congratulation but with nil birthday" do
    FactoryGirl.create(:client_with_today_birthday, birthday_congratulation: true, birthday: nil, group: FactoryGirl.create(:group, user: FactoryGirl.create(:user)))

    assert_equal 0, Client.with_today_birthday.count
  end

  test "should be valid birthday_congratulation for correct birthday" do
    @client.birthday = Date.today - 30.years
    @client.save!

    assert @client.reload.birthday_valid?
  end

  test "should be invalid birthday_congratulation for incorrect birthday" do
    @client.birthday = nil
    @client.save!

    refute @client.reload.birthday_valid?
  end

end
