# -*- encoding : utf-8 -*-
require 'test_helper'

class LogTest < ActiveSupport::TestCase
  def setup
    clear_db

    @user1 = FactoryGirl.create(:user)
    @user2 = FactoryGirl.create(:user)

    #valid
    @log_with_own_groups = FactoryGirl.create(:log_fresh_valid, user: @user1, group_ids: @user1.groups.pluck(:id))

    #invalid
    @log_with_other_user_groups = FactoryGirl.create(:log_fresh_valid, user: @user2, group_ids: @user1.groups.pluck(:id))

    #several sms
    @log_with_several_sms = FactoryGirl.create(:log_with_several_sms, user: FactoryGirl.create(:user))
  end

  test "should be non empty groups for log-with-own-group" do
    assert_equal @user1.groups.sort, @log_with_own_groups.groups.sort
  end

  test "should be empty groups for log-with-other-user-group" do
    assert_equal [], @log_with_other_user_groups.groups
  end

  test "should sms-sent-success equal send_logs sms-count success after recalculate logs" do
    Calculator::Logs.recalculate(@log_with_several_sms.created_at.to_date, @log_with_several_sms.created_at.to_date)
    @log_with_several_sms.reload
    assert_equal @log_with_several_sms.send_logs.select{|send_log| send_log.delivered?}.count * @log_with_several_sms.sms_count, @log_with_several_sms.sms_sent_success
  end

  test "should sms-sent-failure equal send_logs sms-count failure after recalculate logs" do
    Calculator::Logs.recalculate(@log_with_several_sms.created_at.to_date, @log_with_several_sms.created_at.to_date)
    @log_with_several_sms.reload
    assert_equal @log_with_several_sms.send_logs.select{|send_log| !send_log.delivered?}.count * @log_with_several_sms.sms_count, @log_with_several_sms.sms_sent_failure
  end

  test "should total price of send-logs be equal price of log after recalculate logs" do
    Calculator::Logs.recalculate(@log_with_several_sms.created_at.to_date, @log_with_several_sms.created_at.to_date)
    @log_with_several_sms.reload
    assert_equal @log_with_several_sms.send_logs.map{|send_log| send_log.price}.sum, @log_with_several_sms.price
  end

  test 'should delete client with undelivered message from group' do
    user = FactoryGirl.create :user

    group  = FactoryGirl.create :group_with_phones,
                               phones: %w(79115698826 79115688826 79115687260 79843778288),
                               user:user

    log = FactoryGirl.create :log_with_specific_send_log,
                             send_log_conf: {
                                 '79115698826'=>SendLogDeliveryStatus.failured,
                                 '79115688260'=>SendLogDeliveryStatus.failured,
                                 '79115687260'=>SendLogDeliveryStatus.rejected,
                                 '79115688826'=>SendLogDeliveryStatus.delivered,
                                 '79843778288'=>SendLogDeliveryStatus.unknown
                             },
                             user:user

    assert_difference lambda{group.clients.count}, -3 do
      assert_equal 3, log.delete_clients
    end
  end

  test 'should not delete client user with same number from other user' do
    user = FactoryGirl.create :user
    user2 = FactoryGirl.create :user

    group  = FactoryGirl.create :group_with_phones,
                                phones: ['79115688826'],
                                user:user

    log = FactoryGirl.create :log_with_specific_send_log,
                             send_log_conf: {
                                 '79115688826'=>SendLogDeliveryStatus.failured
                             },
                             user:user2

    assert_difference lambda{group.clients.count}, 0 do
      assert_equal 0, log.delete_clients
    end
  end
end
