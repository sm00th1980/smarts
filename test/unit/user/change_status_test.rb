# -*- encoding : utf-8 -*-
require 'test_helper'

class User::ChangeStatusTest < ActiveSupport::TestCase

  def setup
    clear_db
  end

  test "should be blocked after save status as blocked" do
    freeze do
      user = FactoryGirl.create(:user_active)

      check_active?(user)

      user.status = UserStatus.blocked
      user.save!

      check_blocked?(user)
    end
  end

  test "should be deleted after save status as deleted" do
    freeze do
      user = FactoryGirl.create(:user_active)

      check_active?(user)

      user.status = UserStatus.deleted
      user.save!

      check_deleted?(user)
    end
  end

  test "should be active after save status as active if before deleted" do
    freeze do
      user = FactoryGirl.create(:user_deleted)

      check_deleted?(user)

      user.status = UserStatus.active
      user.save!

      check_active?(user)
    end
  end

  test "should be active after save status as active if before blocked" do
    freeze do
      user = FactoryGirl.create(:user_blocked)

      check_blocked?(user)

      user.status = UserStatus.active
      user.save!

      check_active?(user)
    end
  end


  private
  def check_active?(user)
    user.reload

    assert user.status.active?
    assert_nil user.deleted_at
    assert_nil user.blocked_at
  end

  def check_deleted?(user)
    user.reload

    assert user.status.deleted?
    assert_equal Time.zone.now.to_i, user.deleted_at.to_i
    assert_nil user.blocked_at
  end

  def check_blocked?(user)
    user.reload

    assert user.status.blocked?
    assert_nil user.deleted_at
    assert_equal Time.zone.now.to_i, user.blocked_at.to_i
  end

end
