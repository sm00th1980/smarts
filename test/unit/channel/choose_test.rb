# -*- encoding : utf-8 -*-
require 'test_helper'

class Channel::ChooseTest < ActiveSupport::TestCase
  def setup
    clear_db

    @def_sender_name_values = [
        79198015896, #mts
        79657250000, #beeline
        79379903835, #megafon
        79005910000, #tele2
        79016300000, #baikal
        79012200000, #ural
        79011110000, #enisey
        79000300000, #motiv
        79003100000, #nss
        79011570000, #cdma
        79585360000, #other
        79022900000 #smarts
    ]

    @alpha_sender_name_values = [
        'BRW-Mebel',
        'Ruslan',
        'Serafima',
        '9976898',
        'ANGELALUX',
        ' ANGELDELUX',
        'test1234',
        '8846224102',
        'frolov petr',
        'frolov-petr',
        'frolov_petr',
        'ruslan-24',
        'ruslan+24',
        'rus-rus-rus',
        'zolotco.ru',
        'zolotti.ru'
    ]

    @invalid_sender_name_values = [
        nil,
        '',
        ' ',
        '       ',
        'Одежда из Белоруссии. Победы 69.'
    ]

    @phones = {
        mts: 79198015896,
        beeline: 79657250000,
        megafon: 79379903835,
        tele2: 79005910000,
        baikal: 79016300000,
        ural: 79012200000,
        enisey: 79011110000,
        motiv: 79000300000,
        nss: 79003100000,
        cdma: 79011570000,
        other: 79585360000,
        smarts: 79022900000
    }
  end

  #gold
  test "should be gold channel for alpha sender_name and any phone and alpha megafon channel eq gold" do
    @phones.values.each do |phone|
      @alpha_sender_name_values.each do |alpha_sender_name_value|
        assert_equal Channel.gold, Channel.channel(alpha_sender_name_value, Channel.gold, phone)
      end
    end
  end

  test "should be gold channel for alpha sender_name and any phone and alpha megafon channel eq bronze" do
    @phones.values.each do |phone|
      @alpha_sender_name_values.each do |alpha_sender_name_value|
        assert_equal Channel.gold, Channel.channel(alpha_sender_name_value, Channel.bronze, phone)
      end
    end
  end

  #bronze
  test "should be bronze channel for def sender_name and any phone and any alpha megafon channel" do
    @phones.values.each do |phone|
      @def_sender_name_values.each do |def_sender_name_value|
        Channel.all.each do |channel|
          assert_equal Channel.bronze, Channel.channel(def_sender_name_value, channel, phone)
        end
      end
    end
  end

  #unknown channel
  test "should be unknown channel for invalid sender_name_value and any phone and any alpha megafon channel" do
    @phones.values.each do |phone|
      @invalid_sender_name_values.each do |invalid_sender_name_value|
        Channel.all.each do |channel|
          assert_equal Channel.unknown, Channel.channel(invalid_sender_name_value, channel, phone)
        end
      end
    end
  end

  #megafon fixed
  test "should be megafon fixed channel for alpha sender_name and alpha megafon channel eq fixed" do
    @alpha_sender_name_values.each do |alpha_sender_name_value|
      assert_equal Channel.megafon_fixed, Channel.channel(alpha_sender_name_value, Channel.megafon_fixed, @phones[:megafon])
    end
  end

  #megafon multi
  test "should be megafon multi channel for alpha sender_name and alpha megafon channel eq multi" do
    @alpha_sender_name_values.each do |alpha_sender_name_value|
      assert_equal Channel.megafon_multi, Channel.channel(alpha_sender_name_value, Channel.megafon_multi, @phones[:megafon])
    end
  end

end
