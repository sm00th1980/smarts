# -*- encoding : utf-8 -*-
require 'test_helper'

class Channel::TypeTest < ActiveSupport::TestCase
  def setup
    clear_db
  end

  test "gold channel exist" do
    assert_nothing_raised do
      assert_not_nil Channel.gold
      assert_equal 'gold', Channel.gold.internal_name
    end
  end

  test "bronze channel exist" do
    assert_nothing_raised do
      assert_not_nil Channel.bronze
      assert_equal 'bronze', Channel.bronze.internal_name
    end
  end

  test "megafon fixed channel exist" do
    assert_nothing_raised do
      assert_not_nil Channel.megafon_fixed
      assert_equal 'megafon_fixed', Channel.megafon_fixed.internal_name
    end
  end

  test "megafon multi channel exist" do
    assert_nothing_raised do
      assert_not_nil Channel.megafon_multi
      assert_equal 'megafon_multi', Channel.megafon_multi.internal_name
    end
  end

  test "unknown channel exist" do
    assert_nothing_raised do
      assert_not_nil Channel.unknown
      assert_equal 'unknown', Channel.unknown.internal_name
    end
  end

  test "any channel exist" do
    assert_nothing_raised do
      assert_not_nil Channel.any
      assert_equal 'any', Channel.any.internal_name
    end
  end

  test "gold channel should be gold" do
    assert Channel.gold.gold?
    refute Channel.gold.bronze?
    refute Channel.gold.megafon_fixed?
    refute Channel.gold.megafon_multi?
    refute Channel.gold.unknown?
    refute Channel.gold.any?
  end

  test "bronze channel should be bronze" do
    refute Channel.bronze.gold?
    assert Channel.bronze.bronze?
    refute Channel.bronze.unknown?
    refute Channel.bronze.megafon_fixed?
    refute Channel.bronze.megafon_multi?
    refute Channel.bronze.any?
  end

  test "megafon fixed channel should be megafon fixed" do
    refute Channel.megafon_fixed.gold?
    refute Channel.megafon_fixed.bronze?
    assert Channel.megafon_fixed.megafon_fixed?
    refute Channel.megafon_fixed.megafon_multi?
    refute Channel.megafon_fixed.unknown?
    refute Channel.megafon_fixed.any?
  end

  test "unknown channel should be unknown" do
    refute Channel.unknown.gold?
    refute Channel.unknown.bronze?
    refute Channel.unknown.megafon_fixed?
    refute Channel.unknown.megafon_multi?
    assert Channel.unknown.unknown?
    refute Channel.unknown.any?
  end

  test "any channel should be any" do
    refute Channel.any.gold?
    refute Channel.any.bronze?
    refute Channel.any.megafon_fixed?
    refute Channel.any.megafon_multi?
    refute Channel.any.unknown?
    assert Channel.any.any?
  end

end
