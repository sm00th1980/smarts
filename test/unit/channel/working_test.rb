# -*- encoding : utf-8 -*-
require 'test_helper'

class Channel::WorkingTest < ActiveSupport::TestCase

  test "working should has gold, bronze, megafon channels" do
    assert_equal Channel.working.sort, [Channel.gold, Channel.bronze, Channel.megafon_fixed, Channel.megafon_multi].sort
  end

end
