# -*- encoding : utf-8 -*-
require 'test_helper'

class Channel::ConfigurationTest < ActiveSupport::TestCase

  test "gold channel should has gold configuration" do
    assert_equal Rails.configuration.kannel[:gold], Channel.gold.configuration
    check_configuration(Channel.gold.configuration)
  end

  test "bronze channel should has bronze configuration" do
    assert_equal Rails.configuration.kannel[:bronze], Channel.bronze.configuration
    check_configuration(Channel.bronze.configuration)
  end

  test "megafon fixed channel should has megafon fixed configuration" do
    assert_equal Rails.configuration.kannel[:megafon_fixed], Channel.megafon_fixed.configuration
    check_configuration(Channel.megafon_fixed.configuration)
  end

  test "megafon multi channel should has megafon multi configuration" do
    assert_equal Rails.configuration.kannel[:megafon_multi], Channel.megafon_multi.configuration
    check_configuration(Channel.megafon_multi.configuration)
  end

  test "unknown channel should has empty configuration" do
    assert_equal({}, Channel.unknown.configuration)
  end

  private
  def check_configuration(configuration)
    assert configuration.has_key?(:host)
    assert configuration.has_key?(:user)
    assert configuration.has_key?(:password)
    assert configuration.has_key?(:smsc)
    assert configuration.has_key?(:callback_url)
  end

end
