# -*- encoding : utf-8 -*-
require 'test_helper'

class TariffTest < ActiveSupport::TestCase
  def setup
    clear_db
  end

  test "should return default tariff even all tariffs has been droppped before" do
    Tariff.delete_all

    default_tariff = Tariff.default

    assert_not_nil default_tariff
    assert_equal Tariff::DEFAULT_PRICE_PER_SMS, default_tariff.price_per_sms
  end

end
