# -*- encoding : utf-8 -*-
require 'test_helper'

class UserTypeTest < ActiveSupport::TestCase
  def setup
    clear_db
  end

  test "admin exist" do
    assert_nothing_raised do
      assert_not_nil UserType.admin
      assert_equal 'admin', UserType.admin.internal_name
    end
  end

  test "demo exist" do
    assert_nothing_raised do
      assert_not_nil UserType.demo
      assert_equal 'demo', UserType.demo.internal_name
    end
  end

  test "operator exist" do
    assert_nothing_raised do
      assert_not_nil UserType.operator
      assert_equal 'operator', UserType.operator.internal_name
    end
  end

  test "business exist" do
    assert_nothing_raised do
      assert_not_nil UserType.business
      assert_equal 'business', UserType.business.internal_name
    end
  end

end
