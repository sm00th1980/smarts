# -*- encoding : utf-8 -*-
require 'test_helper'

class AdminTest < ActionMailer::TestCase

  test "should send mail to admins" do
    text = rand(100).to_s

    Admin.notify({event: text}).deliver_now
    refute ActionMailer::Base.deliveries.empty?

    emails = ActionMailer::Base.deliveries.sort { |e1, e2| e1.to<=>e2.to }

    emails.each_index do |index|
      _email = emails[index]
      assert_equal [Rails.configuration.mail_sender], _email.from
      assert_equal [Rails.configuration.admin_emails.sort { |e1, e2| e1<=>e2 }[index]], _email.to.sort
      assert_equal I18n.t('admin.subject') % Rails.configuration.site_name, _email.subject
      assert_equal text.strip, _email.body.to_s.strip
    end
  end

  test "should create task for admin notify" do
    params = {event: rand(100).to_s}
    check_new_task_created('Admin', 'notify', params) do
      Admin.delay.notify(params)
    end
  end
end
