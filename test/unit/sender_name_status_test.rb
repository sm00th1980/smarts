# -*- encoding : utf-8 -*-
require 'test_helper'

class SenderNameStatusTest < ActiveSupport::TestCase
  def setup
    clear_db
  end

  test "moderating exist" do
    assert_equal 'moderating', SenderNameStatus.moderating.internal_name
  end

  test "accepted exist" do
    assert_equal 'accepted', SenderNameStatus.accepted.internal_name
  end

  test "rejected exist" do
    assert_equal 'rejected', SenderNameStatus.rejected.internal_name
  end

  test "should not create new statuses if they already exists" do
    SenderNameStatus.moderating
    assert_no_difference 'SenderNameStatus.count' do
      SenderNameStatus.moderating
    end

    SenderNameStatus.accepted
    assert_no_difference 'SenderNameStatus.count' do
      SenderNameStatus.accepted
    end

    SenderNameStatus.rejected
    assert_no_difference 'SenderNameStatus.count' do
      SenderNameStatus.rejected
    end
  end
end
