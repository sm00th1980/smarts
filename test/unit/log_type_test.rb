# -*- encoding : utf-8 -*-
require 'test_helper'

class LogTypeTest < ActiveSupport::TestCase
  def setup
    clear_db
  end

  test "api type should exist" do
    assert_not_nil LogType.api
    assert_equal 'api', LogType.api.internal_name
  end

  test "manual type should exist" do
    assert_not_nil LogType.manual
    assert_equal 'manual', LogType.manual.internal_name
  end

  test "autodelivery type should exist" do
    assert_not_nil LogType.autodelivery
    assert_equal 'autodelivery', LogType.autodelivery.internal_name
  end

  test "by-not-delivered type should exist" do
    assert_not_nil LogType.by_not_delivered
    assert_equal 'by_not_delivered', LogType.by_not_delivered.internal_name
  end
end
