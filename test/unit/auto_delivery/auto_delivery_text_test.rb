# -*- encoding : utf-8 -*-
require 'test_helper'

class AutoDeliveryTextTest < ActiveSupport::TestCase
  def setup
    clear_db

    user = FactoryGirl.create(:user)
    template = FactoryGirl.create(:template, user: user)
    template_without_text = FactoryGirl.create(:template_without_text, user: user)

    @autodelivery_with_content  = FactoryGirl.create(:auto_delivery, user: user, content: 'content', template: nil)
    @autodelivery_with_template = FactoryGirl.create(:auto_delivery, user: user, content: nil, template: template)
    @autodelivery_with_content_and_template = FactoryGirl.create(:auto_delivery, user: user, content: 'content', template: template)

    @autodelivery_with_not_exist_template = FactoryGirl.create(:auto_delivery, user: user, content: nil, template_id: -1)
    @autodelivery_with_template_without_text = FactoryGirl.create(:auto_delivery, user: user, content: nil, template: template_without_text)

    @autodelivery_with_content_and_not_exist_template = FactoryGirl.create(:auto_delivery, user: user, content: 'content', template_id: -1)
    @autodelivery_with_content_template_without_text = FactoryGirl.create(:auto_delivery, user: user, content: 'content', template: template_without_text)

    @autodelivery_without_content_and_not_own_template = FactoryGirl.create(:auto_delivery, user: user, content: nil, template: FactoryGirl.create(:template, user: FactoryGirl.create(:user)))
  end

  test "text should return content when content present" do
    assert_not_nil @autodelivery_with_content.text
    assert_equal @autodelivery_with_content.content, @autodelivery_with_content.text
  end

  test "text should return template-text when content blank and template present" do
    assert_not_nil @autodelivery_with_template.text
    assert_equal @autodelivery_with_template.template.text, @autodelivery_with_template.text
  end

  test "text should return content when content present and template present" do
    assert_not_nil @autodelivery_with_content_and_template.text
    assert_equal @autodelivery_with_content_and_template.content, @autodelivery_with_content_and_template.text
  end

  test "text should return nil when content blank and template not exist" do
    assert_nil @autodelivery_with_not_exist_template.text
  end

  test "text should return nil when content blank and template without text" do
    assert_nil @autodelivery_with_template_without_text.text
  end

  test "text should return content when content present and template not exist" do
    assert_not_nil @autodelivery_with_content_and_not_exist_template.text
    assert_equal @autodelivery_with_content_and_not_exist_template.content, @autodelivery_with_content_and_not_exist_template.text
  end

  test "text should return content when content present and template without text" do
    assert_not_nil @autodelivery_with_content_template_without_text.text
    assert_equal @autodelivery_with_content_template_without_text.content, @autodelivery_with_content_template_without_text.text
  end

  test "text should return nil when content blank and template not his own" do
    assert_nil @autodelivery_without_content_and_not_own_template.text
  end

end
