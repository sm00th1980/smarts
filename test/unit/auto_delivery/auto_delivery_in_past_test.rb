# -*- encoding : utf-8 -*-
require 'test_helper'

class AutoDeliveryInPastTest < ActiveSupport::TestCase
  def setup
    clear_db

    stub_real_request

    @autodelivery_every_hour_in_past = FactoryGirl.create(:auto_delivery_active,
                                                            start_date: Date.today - 1.week,
                                                            stop_date: Date.today - 1.week,
                                                            period_id: FactoryGirl.create(:auto_delivery_period_every_hour).id,
                                                            minute: 20
    )

    @autodelivery_every_day_in_past = FactoryGirl.create(:auto_delivery_active,
                                                           start_date: Date.today - 1.week,
                                                           stop_date: Date.today - 1.week,
                                                           period_id: FactoryGirl.create(:auto_delivery_period_every_day).id,
                                                           hour: 10,
                                                           minute: 0
    )

    @autodelivery_every_week_in_past = FactoryGirl.create(:auto_delivery_active,
                                                            start_date: Date.today - 1.week,
                                                            stop_date: Date.today - 1.week,
                                                            period_id: FactoryGirl.create(:auto_delivery_period_every_week).id,
                                                            week_day: WeekDay.monday,
                                                            hour: 10,
                                                            minute: 0
    )
  end

  test "should not be valid for run if autodelivery-every-hour in past" do
    refute @autodelivery_every_hour_in_past.active?

    time = Time.parse("%s 10:%s:00" % [@autodelivery_every_hour_in_past.start_date.strftime('%Y-%m-%d'), @autodelivery_every_hour_in_past.minute])
    freeze(time + 1.week) do
      refute @autodelivery_every_hour_in_past.valid_for_run?, "авто-рассылка-каждый-час в прошлом не должна быть запущена"
    end
  end

  test "should not be valid for run if autodelivery-every-day in past" do
    refute @autodelivery_every_day_in_past.active?

    time = Time.parse("%s %s:%s:00" % [@autodelivery_every_day_in_past.start_date.strftime('%Y-%m-%d'), @autodelivery_every_day_in_past.hour, @autodelivery_every_day_in_past.minute])
    freeze(time + 1.week) do
      refute @autodelivery_every_day_in_past.valid_for_run?, "авто-рассылка-каждый-день в прошлом не должна быть запущена"
    end
  end

  test "should not be valid for run if autodelivery-every-week in past" do
    refute @autodelivery_every_week_in_past.active?

    time = Time.parse("%s %s:%s:00" % [@autodelivery_every_week_in_past.start_date.strftime('%Y-%m-%d'), @autodelivery_every_week_in_past.hour, @autodelivery_every_week_in_past.minute])
    freeze(time + 1.week) do
      refute @autodelivery_every_week_in_past.valid_for_run?, "авто-рассылка-каждую-неделю в прошлом не должна быть запущена"
    end
  end

end
