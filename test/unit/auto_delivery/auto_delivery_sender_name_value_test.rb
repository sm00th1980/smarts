# -*- encoding : utf-8 -*-
require 'test_helper'

class AutoDeliverySenderNameValueTest < ActiveSupport::TestCase
  def setup
    clear_db

    stub_real_request

    @invalid_sender_name_values = [
        nil,
        '',
        ' '
    ]
  end

  test "should not be valid for run if invalid sender name" do
    @invalid_sender_name_values.each do |invalid_sender_name|
      autodelivery = FactoryGirl.create(:auto_delivery_every_day_run_now)
      autodelivery.update_attribute(:sender_name_value, invalid_sender_name)
      refute autodelivery.valid_for_run?
      assert_equal [I18n.t('autodelivery.failure.invalid_sender_name_value')], autodelivery.activation_errors
    end
  end

  test "should not be valid for activation if invalid sender name" do
    @invalid_sender_name_values.each do |invalid_sender_name|
      autodelivery = FactoryGirl.create(:auto_delivery_every_day_run_now)
      autodelivery.update_attribute(:sender_name_value, invalid_sender_name)
      refute autodelivery.valid_for_activation?
      assert_equal [I18n.t('autodelivery.failure.invalid_sender_name_value')], autodelivery.activation_errors
    end
  end

  #DEF sender name
  test "should be valid for run if def sender name accepted and verified" do
    user = FactoryGirl.create(:user_with_permit_alpha_and_permit_def)
    sender_name = FactoryGirl.create(:sender_name_accepted_with_verified_value, user: user, value: '79379903835')

    autodelivery = FactoryGirl.create(:auto_delivery_every_day_run_now, sender_name_value: sender_name.value, user: user)
    assert autodelivery.valid_for_run?
    assert_equal [], autodelivery.activation_errors
  end

  test "should not be valid for run if def sender name rejected" do
    user = FactoryGirl.create(:user_with_permit_alpha_and_reject_def)
    ['79379903835'].each do |invalid_sender_name|
      autodelivery = FactoryGirl.create(:auto_delivery_every_day_run_now)
      autodelivery.update_attribute(:sender_name_value, invalid_sender_name)
      autodelivery.update_attribute(:user, user)
      refute autodelivery.valid_for_run?
      assert_equal [I18n.t('autodelivery.failure.invalid_sender_name_value')], autodelivery.activation_errors
    end
  end

  test "should be valid for activation if def sender name accepted and verified" do
    user = FactoryGirl.create(:user_with_permit_alpha_and_permit_def)
    sender_name = FactoryGirl.create(:sender_name_accepted_with_verified_value, user: user, value: '79379903835')
    autodelivery = FactoryGirl.create(:auto_delivery_every_day_run_now)
    autodelivery.update_attribute(:sender_name_value, sender_name.value)
    autodelivery.update_attribute(:user, user)

    assert autodelivery.valid_for_activation?
    assert_equal [], autodelivery.activation_errors
  end

  test "should not be valid for activation if def sender name rejected" do
    user = FactoryGirl.create(:user_with_permit_alpha_and_reject_def)
    ['79379903835'].each do |invalid_sender_name|
      autodelivery = FactoryGirl.create(:auto_delivery_every_day_run_now)
      autodelivery.update_attribute(:sender_name_value, invalid_sender_name)
      autodelivery.update_attribute(:user, user)

      refute autodelivery.valid_for_activation?
      assert_equal [I18n.t('autodelivery.failure.invalid_sender_name_value')], autodelivery.activation_errors
    end
  end

  #ALPHA sender name
  test "should be valid for run if alpha sender name accepted and verified" do
    user = FactoryGirl.create(:user_with_permit_alpha_and_permit_def)
    sender_name = FactoryGirl.create(:sender_name_accepted_with_verified_value, user: user, value: 'ANGELALUX')

    autodelivery = FactoryGirl.create(:auto_delivery_every_day_run_now)
    autodelivery.update_attribute(:sender_name_value, sender_name.value)
    autodelivery.update_attribute(:user, user)

    assert autodelivery.valid_for_run?
    assert_equal [], autodelivery.activation_errors
  end

  test "should not be valid for run if alpha sender name rejected" do
    user = FactoryGirl.create(:user_with_reject_alpha_and_permit_def)
    ['ANGELALUX'].each do |invalid_sender_name|
      autodelivery = FactoryGirl.create(:auto_delivery_every_day_run_now)

      autodelivery.update_attribute(:sender_name_value, invalid_sender_name)
      autodelivery.update_attribute(:user, user)

      refute autodelivery.valid_for_run?
      assert_equal [I18n.t('autodelivery.failure.invalid_sender_name_value')], autodelivery.activation_errors
    end
  end

  test "should be valid for activation if alpha sender name accepted and verified" do
    user = FactoryGirl.create(:user_with_permit_alpha_and_permit_def)
    sender_name = FactoryGirl.create(:sender_name_accepted_with_verified_value, user: user, value: 'ANGELALUX')

    autodelivery = FactoryGirl.create(:auto_delivery_every_day_run_now)
    autodelivery.update_attribute(:sender_name_value, sender_name.value)
    autodelivery.update_attribute(:user, user)

    assert autodelivery.valid_for_activation?
    assert_equal [], autodelivery.activation_errors
  end

  test "should not be valid for activation if alpha sender name rejected" do
    user = FactoryGirl.create(:user_with_reject_alpha_and_permit_def)
    ['ANGELALUX'].each do |invalid_sender_name|

      autodelivery = FactoryGirl.create(:auto_delivery_every_day_run_now)
      autodelivery.update_attribute(:sender_name_value, invalid_sender_name)
      autodelivery.update_attribute(:user, user)

      refute autodelivery.valid_for_activation?
      assert_equal [I18n.t('autodelivery.failure.invalid_sender_name_value')], autodelivery.activation_errors
    end
  end

end
