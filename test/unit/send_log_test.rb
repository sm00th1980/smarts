# -*- encoding : utf-8 -*-
require 'test_helper'

class SendLogTest < ActiveSupport::TestCase
  def setup
    clear_db
    @log = FactoryGirl.create(:log_with_chunks, user: FactoryGirl.create(:user))

    @send_log_success = @log.send_logs[0]
    @send_log_success.delivery_status = SendLogDeliveryStatus.delivered
    @send_log_success.save!

    @send_log_failure = @log.send_logs[-1]
    @send_log_failure.delivery_status = SendLogDeliveryStatus.rejected
    @send_log_failure.save!

    recalculate_logs(@log.user)
  end

  #check price
  test "should decrement price on log when setting to zero price on send_log" do
    assert_operator @send_log_success.price, :>, 0
    assert_not_nil @send_log_success.price
    delta = -0.24

    assert_difference lambda { @log.reload.price }, delta do
      @send_log_success.price += delta
      @send_log_success.save!

      recalculate_logs(@log.user)
    end
  end

  test "should increment price on log when raise price on send_log" do
    assert_operator @send_log_success.price, :>, 0
    assert_not_nil @send_log_success.price
    delta = 0.20

    assert_difference lambda { @log.reload.price }, delta do
      @send_log_success.price += delta
      @send_log_success.save!

      recalculate_logs(@log.user)
    end
  end

  test "should decrement price on log when drop send_log" do
    assert_operator @send_log_success.price, :>, 0
    assert_not_nil @send_log_success.price
    delta = @send_log_success.price

    assert_difference lambda { @log.reload.price }, -1*delta do
      @send_log_success.destroy

      recalculate_logs(@log.user)
    end
  end

  #check sms_sent_success
  test "should decrement sms_sent_success on log when setting not-delivered on send_log" do
    assert_difference lambda { @log.reload.sms_sent_success }, -1*@send_log_success.log.sms_count do
      @send_log_success.delivery_status = SendLogDeliveryStatus.rejected
      @send_log_success.save!

      recalculate_logs(@log.user)
    end
  end

  test "should increment sms_sent_success on log when setting delivered on send_log" do
    assert_difference lambda { @log.reload.sms_sent_success }, @send_log_failure.log.sms_count do
      @send_log_failure.delivery_status = SendLogDeliveryStatus.delivered
      @send_log_failure.save!

      recalculate_logs(@log.user)
    end
  end

  test "should decrement sms_sent_success on log when drop send-log-success" do
    assert_difference lambda { @log.reload.sms_sent_success }, -1*@send_log_success.log.sms_count do
      @send_log_success.destroy

      recalculate_logs(@log.user)
    end
  end

  #check sms_sent_failure
  test "should decrement sms_sent_failure on log when setting delivered on send_log" do
    assert_difference lambda { @log.reload.sms_sent_failure }, -1*@send_log_failure.log.sms_count do
      @send_log_failure.delivery_status = SendLogDeliveryStatus.delivered
      @send_log_failure.save!

      recalculate_logs(@log.user)
    end
  end

  test "should increment sms_sent_failure on log when setting not-delivered on send_log" do
    assert_difference lambda { @log.reload.sms_sent_failure }, @send_log_success.log.sms_count do
      @send_log_success.delivery_status = SendLogDeliveryStatus.rejected
      @send_log_success.save!

      recalculate_logs(@log.user)
    end
  end

  test "should decrement sms_sent_failure on log when drop send-log-failure" do
    assert_difference lambda { @log.reload.sms_sent_failure }, -1*@send_log_failure.log.sms_count do
      @send_log_failure.destroy

      recalculate_logs(@log.user)
    end
  end

  test "each send_log should has callback_url" do
    assert_equal 'http://localhost:80/kannel/delivery_report?smsid=%s&type=%%d&answer=%%A' % [@send_log_failure.id], @send_log_failure.callback_url
    assert_equal 'http://localhost:80/kannel/delivery_report?smsid=%s&type=%%d&answer=%%A' % [@send_log_success.id], @send_log_success.callback_url
  end


end
