# -*- encoding : utf-8 -*-
require 'test_helper'

class GroupTest < ActiveSupport::TestCase
  def setup
    clear_db

    @group_with_normal_name = FactoryGirl.create(:group, name: 'Normal Name', user: FactoryGirl.create(:user))
    @group_with_nil_name = FactoryGirl.create(:group, name: nil, user: FactoryGirl.create(:user))
    @group_with_blank_name = FactoryGirl.create(:group, name: '', user: FactoryGirl.create(:user))
  end

  test "group with nil name should have name" do
    assert_equal I18n.t('group.without_name'), @group_with_nil_name.name
  end

  test "group with blank name should have name" do
    assert_equal I18n.t('group.without_name'), @group_with_blank_name.name
  end

  test "group with normal name should have normal name" do
    assert_equal 'Normal Name', @group_with_normal_name.name
  end

  [true, false].each { |flag|
    test "should set clients birthday_congratulation to #{flag}" do
      @group_with_normal_name.set_users_birthday_congratulation_to flag
      @group_with_normal_name.clients.each{ |client|
        assert_equal flag, client.birthday_congratulation
      }
    end
  }

end
