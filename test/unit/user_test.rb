# -*- encoding : utf-8 -*-
require 'test_helper'

class UserTest < ActiveSupport::TestCase

  def setup
    clear_db

    @user = FactoryGirl.create(:user)
    @user_with_negative_balance = FactoryGirl.create(:user_with_negative_balance)

    @user_with_several_sender_names = FactoryGirl.create(:user)
    @accepted_for_user_with_several_sender_names = FactoryGirl.create(:sender_name_accepted, :user => @user_with_several_sender_names)
    FactoryGirl.create(:sender_name_rejected,   :user => @user_with_several_sender_names)
    FactoryGirl.create(:sender_name_moderating, :user => @user_with_several_sender_names)

    @user_without_clients = FactoryGirl.create(:user_without_clients)

    @user_with_black_lists = FactoryGirl.create(:user_with_black_lists)

    #several sms
    @log_with_several_sms = FactoryGirl.create(:log_with_several_sms, user: FactoryGirl.create(:user))
  end

  test "each user should has price_per_sms" do
    User.all.each do |user|
      assert_not_nil user.price_per_sms
      assert user.price_per_sms > 0
    end
  end

  test "tariff for user with basic tariff" do
    user = FactoryGirl.create(:user_with_basic_tariff)
    assert_not_nil user.price_per_sms
    assert_not_nil user.tariff
    assert_equal Tariff::DEFAULT_PRICE_PER_SMS, user.price_per_sms
  end

  test "tariff for user with middle tariff" do
    user = FactoryGirl.create(:user_with_middle_tariff)
    assert_not_nil user.price_per_sms
    assert_not_nil user.tariff
    assert Tariff::DEFAULT_PRICE_PER_SMS > user.price_per_sms
  end

  test "tariff for user with big tariff" do
    user = FactoryGirl.create(:user_with_big_tariff)
    assert_not_nil user.price_per_sms
    assert_not_nil user.tariff
    assert Tariff::DEFAULT_PRICE_PER_SMS > user.price_per_sms
  end

  test "tariff for user without tariff" do
    user = FactoryGirl.create(:user_without_tariff)

    assert_equal 0, UserTariff.where(:user_id => user.id).count

    assert_not_nil user.price_per_sms
    assert_not_nil user.tariff
    assert_equal  Tariff::DEFAULT_PRICE_PER_SMS, user.price_per_sms
  end

  test "should return current_balance" do
    user = FactoryGirl.create(:user_with_basic_tariff)
    assert_not_nil user.current_balance
  end

  test "user should have accepted moderate_sender_names list" do
    assert_equal 1, @user_with_several_sender_names.accepted_sender_name_values.count
    assert_equal @accepted_for_user_with_several_sender_names.value.to_s, @user_with_several_sender_names.accepted_sender_name_values[0][:value].to_s
  end

  test "for each user should not raise exception when call demo?" do
    User.all.each do |user|
      assert_nothing_raised do
        user.demo?
      end
    end
  end

  test "for each user should not raise exception when call admin?" do
    User.all.each do |user|
      assert_nothing_raised do
        user.admin?
      end
    end
  end

  test "for each user should not raise exception when call operator?" do
    User.all.each do |user|
      assert_nothing_raised do
        user.operator?
      end
    end
  end

  test "for each user should not raise exception when call business?" do
    User.all.each do |user|
      assert_nothing_raised do
        user.business?
      end
    end
  end

  test "for each user should not raise exception when call different sms_sent" do
    Calculator::Logs.recalculate(Log.order(:created_at).first.created_at.to_date, Log.order(:created_at).last.created_at.to_date)

    assert_operator User.count, :>, 0

    User.all.each do |user|
      assert_nothing_raised do
        user.total_sms_sent
        user.sms_sent_success
        user.sms_sent_failure
      end

      assert_not_nil user.total_sms_sent
      assert_not_nil user.sms_sent_success
      assert_not_nil user.sms_sent_failure
    end
  end

  test "each user should sum delivered and not deliverd sms be equal total sent sms" do
    Calculator::Logs.recalculate(Log.order(:created_at).first.created_at.to_date, Log.order(:created_at).last.created_at.to_date)

    assert_operator User.count, :>, 0

    User.all.each do |user|
      assert_not_nil user.total_sms_sent
      assert_equal user.total_sms_sent, user.sms_sent_success + user.sms_sent_failure
    end
  end

  test "each user has correct total sms sent after recalculate logs" do
    User.find_each do |user|
      Calculator::Logs.recalculate(Log.where(:user_id => user.id).order(:created_at)[0].created_at.to_date, Log.where(:user_id => user.id).order('created_at DESC')[0].created_at.to_date)

      assert_not_nil user.total_sms_sent
      assert_equal SendLog.where(log_id: Log.where(:user_id => user.id)).map{|send_log| send_log.log.sms_count}.sum, user.total_sms_sent
    end
  end

  #sender_name
  test "should be nil active_sender_name id user have not any sender name" do
    user_without_sender_names = FactoryGirl.create(:user_without_sender_names)
    assert_nil user_without_sender_names.active_sender_name_value
  end

  #groups_with_clients
  test "should return group with clients" do
    assert_equal @user.groups, @user.groups_with_clients
  end

  test "should return empty_list for user without_clients" do
    assert_equal [], @user_without_clients.groups_with_clients
  end

  #clients
  test "should return list of clients" do
    assert_equal @user.groups_with_clients.map{|g| g.clients}.flatten, @user.clients
  end

  #black_lists
  test "should have black list numbers" do
    assert_operator @user_with_black_lists.black_listed_phones.count, :> , 0
  end

  #calculate sms-sent
  test "should sms-sent-success equal send_logs sms-count success after recalculate logs" do
    Calculator::Logs.recalculate(@log_with_several_sms.created_at.to_date, @log_with_several_sms.created_at.to_date)
    assert_equal @log_with_several_sms.send_logs.select{|send_log| send_log.delivered?}.map{|send_log| send_log.log.sms_count}.sum, @log_with_several_sms.user.sms_sent_success
  end

  test "should sms-sent-failure equal send_logs sms-count failure after recalculate logs" do
    Calculator::Logs.recalculate(@log_with_several_sms.created_at.to_date, @log_with_several_sms.created_at.to_date)
    assert_equal @log_with_several_sms.send_logs.select{|send_log| !send_log.delivered?}.map{|send_log| send_log.log.sms_count}.sum, @log_with_several_sms.user.sms_sent_failure
  end
end
