# -*- encoding : utf-8 -*-
require 'test_helper'

class AutoDeliveryPeriodTest < ActiveSupport::TestCase

  def setup
    clear_db

    @every_hour = FactoryGirl.create(:auto_delivery_period_every_hour)
    @every_day  = FactoryGirl.create(:auto_delivery_period_every_day)
    @every_week = FactoryGirl.create(:auto_delivery_period_every_week)
  end

  test "correctness every_hour" do
     assert @every_hour.every_hour?
     refute @every_hour.every_day?
     refute @every_hour.every_week?
  end

  test "correctness every_day" do
    refute @every_day.every_hour?
    assert @every_day.every_day?
    refute @every_day.every_week?
  end

  test "correctness every_week" do
    refute @every_week.every_hour?
    refute @every_week.every_day?
    assert @every_week.every_week?
  end

  test "period every-day should exist" do
    assert_not_nil AutoDeliveryPeriod.every_day
    assert_equal 'every_day', AutoDeliveryPeriod.every_day.period
  end

  test "period every-hour should exist" do
    assert_not_nil AutoDeliveryPeriod.every_hour
    assert_equal 'every_hour', AutoDeliveryPeriod.every_hour.period
  end

  test "period every-week should exist" do
    assert_not_nil AutoDeliveryPeriod.every_week
    assert_equal 'every_week', AutoDeliveryPeriod.every_week.period
  end

end
