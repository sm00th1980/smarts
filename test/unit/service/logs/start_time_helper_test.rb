# -*- encoding : utf-8 -*-
require 'test_helper'

class Service::Logs::StartTimeHelperTest < ActiveSupport::TestCase
  include Service::Logs::StartTime

  def setup
    @begin_hour = BEGIN_HOUR
    @begin_min = BEGIN_MIN
    @begin_sec = BEGIN_SEC

    @end_hour = END_HOUR
    @end_min = END_MIN
    @end_sec = END_SEC
  end

  #next_day_morning
  test "should raise error if time in day" do
    time = Time.new(2002, 10, 31, 16, 0, 0)
    check_raise_not_night { next_day_morning(time) }
  end

  test "should return next day morning if time in night before 0 hour" do
    time = Time.new(2002, 10, 31, 23, 0, 0)
    assert_equal (time+1.day).change(hour: @begin_hour, min: @begin_min, sec: @begin_sec), next_day_morning(time)
  end

  test "should return current day morning if time in night after 0 hour" do
    time = Time.new(2002, 10, 31, 1, 0, 0)
    assert_equal time.change(hour: @begin_hour, min: @begin_min, sec: @begin_sec), next_day_morning(time)
  end

  test "should return current day morning if time in midnight" do
    time = Time.new(2002, 10, 31, 0, 0, 0)
    assert_equal time.change(hour: @begin_hour, min: @begin_min, sec: @begin_sec), next_day_morning(time)
  end

  test "should raise error if time eq morning" do
    time = Time.new(2002, 10, 31, @begin_hour, @begin_min, @begin_sec)
    check_raise_not_night { next_day_morning(time) }
  end

  test "should return next day morning if time eq evening" do
    time = Time.new(2002, 10, 31, @end_hour, @end_min, @end_sec)
    assert_equal (time+1.day).change(hour: @begin_hour, min: @begin_min, sec: @begin_sec), next_day_morning(time)
  end

  #before_midnight?
  test "before-midnight should be true if before midnight" do
    time = Time.new(2002, 10, 31, 23, 0, 0)
    assert before_midnight?(time)
  end

  test "before-midnight should be false if after midnight" do
    time = Time.new(2002, 10, 31, 1, 0, 0)
    refute before_midnight?(time)
  end

  test "before-midnight should raise error if day" do
    time = Time.new(2002, 10, 31, 12, 0, 0)
    check_raise_not_night { before_midnight?(time) }
  end

  #begin_time
  test "begin_time should be in morning" do
    time = Time.new(2002, 10, 31, 12, 12, 13)
    assert_equal time.change(hour: @begin_hour, min: @begin_min, sec: @begin_sec), begin_time(time)
  end

  #end_time
  test "end_time should be in evening" do
    time = Time.new(2002, 10, 31, 12, 12, 13)
    assert_equal time.change(hour: @end_hour, min: @end_min, sec: @end_sec), end_time(time)
  end

  #night_time?
  test "should not be night-time eq begin-time" do
    time = Time.new(2002, 10, 31, @begin_hour, @begin_min, @begin_sec)
    refute night_time?(time)
  end

  test "should be night-time eq end-time" do
    time = Time.new(2002, 10, 31, @end_hour, @end_min, @end_sec)
    assert night_time?(time)
  end

  test "should not be night-time if day" do
    time = Time.new(2002, 10, 31, 12, 13, 14)
    refute night_time?(time)
  end

  test "should be night-time eq midnight" do
    time = Time.new(2002, 10, 31, 0, 0, 0)
    assert night_time?(time)
  end

  test "should be night-time eq before midnight" do
    time = Time.new(2002, 10, 31, 23, 12, 12)
    assert night_time?(time)
  end

  test "should be night-time eq after midnight" do
    time = Time.new(2002, 10, 31, 1, 12, 12)
    assert night_time?(time)
  end

  #start_time with permit_night_delivery=false
  test "should return next second if day with permit_night_delivery is false" do
    time = Time.new(2002, 10, 31, 12, 12, 12)
    assert_equal time + SMSC_PROCESS_TIME, start_time(time, false)
  end

  test "should return next morning if before midnight" do
    time = Time.new(2002, 10, 31, 23, 12, 12)
    assert_equal (time + 1.day).change(hour: @begin_hour, min: @begin_min, sec: @begin_sec), start_time(time, false)
  end

  test "should return next morning if almost midnight" do
    time = Time.new(2002, 10, 31, 23, 59, 59)
    assert_equal (time + 1.day).change(hour: @begin_hour, min: @begin_min, sec: @begin_sec), start_time(time, false)
  end

  test "should return current morning if after midnight" do
    time = Time.new(2002, 10, 31, 1, 12, 12)
    assert_equal time.change(hour: @begin_hour, min: @begin_min, sec: @begin_sec), start_time(time, false)
  end

  test "should return current morning if midnight" do
    time = Time.new(2002, 10, 31, 0, 0, 0)
    assert_equal time.change(hour: @begin_hour, min: @begin_min, sec: @begin_sec), start_time(time, false)
  end

  #start_time with permit_night_delivery=true
  test "should return next second if day with permit_night_delivery" do
    time = Time.new(2002, 10, 31, 12, 12, 12)
    assert_equal time + SMSC_PROCESS_TIME, start_time(time, true)
  end

  test "should return next morning if before midnight with permit_night_delivery" do
    time = Time.new(2002, 10, 31, 23, 12, 12)
    assert_equal time + SMSC_PROCESS_TIME, start_time(time, true)
  end

  test "should return next morning if almost midnight with permit_night_delivery" do
    time = Time.new(2002, 10, 31, 23, 59, 59)
    assert_equal time + SMSC_PROCESS_TIME, start_time(time, true)
  end

  test "should return current morning if after midnight with permit_night_delivery" do
    time = Time.new(2002, 10, 31, 1, 12, 12)
    assert_equal time + SMSC_PROCESS_TIME, start_time(time, true)
  end

  test "should return current morning if midnight with permit_night_delivery" do
    time = Time.new(2002, 10, 31, 0, 0, 0)
    assert_equal time + SMSC_PROCESS_TIME, start_time(time, true)
  end

  #start_times with permit_night_delivery=false
  test "should return correct sequence in day with permit_night_delivery is false" do
    current_time = Time.new(2002, 10, 31, 12, 13, 14)
    count = 3

    freeze(current_time) do
      _time = current_time + guard_delay(count)
      assert_equal [_time, _time+SMSC_PROCESS_TIME, _time+2*SMSC_PROCESS_TIME], start_times(count, false)
    end
  end

  test "should return correct sequence if before midnight with permit_night_delivery is false" do
    current_time = Time.new(2002, 10, 31, 23, 13, 14)
    count = 3

    freeze(current_time) do
      _time = (current_time + 1.day).change(hour: @begin_hour, min: @begin_min, sec: @begin_sec)
      assert_equal [_time, _time+SMSC_PROCESS_TIME, _time+2*SMSC_PROCESS_TIME], start_times(count, false)
    end
  end

  test "should return correct sequence if after midnight with permit_night_delivery is false" do
    current_time = Time.new(2002, 10, 31, 1, 13, 14)
    count = 3

    freeze(current_time) do
      _time = current_time.change(hour: @begin_hour, min: @begin_min, sec: @begin_sec)
      assert_equal [_time, _time+SMSC_PROCESS_TIME, _time+2*SMSC_PROCESS_TIME], start_times(count, false)
    end
  end

  test "should return correct sequence if midnight with permit_night_delivery is false" do
    current_time = Time.new(2002, 10, 31, 0, 0, 0)
    count = 3

    freeze(current_time) do
      _time = current_time.change(hour: @begin_hour, min: @begin_min, sec: @begin_sec)
      assert_equal [_time, _time+SMSC_PROCESS_TIME, _time+2*SMSC_PROCESS_TIME], start_times(count, false)
    end
  end

  test "should return correct sequence if almost midnight with permit_night_delivery is false" do
    current_time = Time.new(2002, 10, 31, 23, 59, 59)
    count = 3

    freeze(current_time) do
      _time = (current_time + 1.day).change(hour: @begin_hour, min: @begin_min, sec: @begin_sec)
      assert_equal [_time, _time+SMSC_PROCESS_TIME, _time+2*SMSC_PROCESS_TIME], start_times(count, false)
    end
  end

  test "should return correct sequence if almost morning with permit_night_delivery is false" do
    current_time = Time.new(2002, 10, 31, @begin_hour, @begin_min, @begin_sec) - 1.second
    count = 3

    freeze(current_time) do
      _time = current_time.change(hour: @begin_hour, min: @begin_min, sec: @begin_sec) - 1.second + guard_delay(count)
      assert_equal [_time, _time+SMSC_PROCESS_TIME, _time+2*SMSC_PROCESS_TIME], start_times(count, false)
    end
  end

  test "should return correct sequence if almost morning minus GUARD-DELAY with permit_night_delivery is false" do
    count = 3
    current_time = Time.new(2002, 10, 31, @begin_hour, @begin_min, @begin_sec) - 1.second - guard_delay(count)

    freeze(current_time) do
      _time = current_time.change(hour: @begin_hour, min: @begin_min, sec: @begin_sec)
      assert_equal [_time, _time+SMSC_PROCESS_TIME, _time+2*SMSC_PROCESS_TIME], start_times(count, false)
    end
  end

  test "should return correct sequence if almost evening with permit_night_delivery is false" do
    current_time = Time.new(2002, 10, 31, @end_hour, @end_min, @end_sec) - 1.second
    count = 3

    freeze(current_time) do
      _time = (current_time + 1.day).change(hour: @begin_hour, min: @begin_min, sec: @begin_sec)
      assert_equal [_time, _time+SMSC_PROCESS_TIME, _time+2*SMSC_PROCESS_TIME], start_times(count, false)
    end
  end

  test "should return correct sequence if almost evening minus GUARD-DELAY with permit_night_delivery is false" do
    count = 3
    current_time = Time.new(2002, 10, 31, @end_hour, @end_min, @end_sec) - SMSC_PROCESS_TIME - guard_delay(count)

    freeze(current_time) do
      _time = (current_time + 1.day).change(hour: @begin_hour, min: @begin_min, sec: @begin_sec)
      assert_equal [current_time+guard_delay(count), _time, _time+SMSC_PROCESS_TIME], start_times(count, false)
    end
  end

  #start_times with permit_night_delivery=true
  test "should return correct sequence in day with permit_night_delivery" do
    current_time = Time.new(2002, 10, 31, 12, 13, 14)
    count = 3

    freeze(current_time) do
      _time = current_time + guard_delay(count)
      assert_equal [_time, _time+SMSC_PROCESS_TIME, _time+2*SMSC_PROCESS_TIME], start_times(count, true)
    end
  end

  test "should return correct sequence if before midnight with permit_night_delivery" do
    current_time = Time.new(2002, 10, 31, 23, 13, 14)
    count = 3

    freeze(current_time) do
      _time = current_time + guard_delay(count)
      assert_equal [_time, _time+SMSC_PROCESS_TIME, _time+2*SMSC_PROCESS_TIME], start_times(count, true)
    end
  end

  test "should return correct sequence if after midnight with permit_night_delivery" do
    current_time = Time.new(2002, 10, 31, 1, 13, 14)
    count = 3

    freeze(current_time) do
      _time = current_time + guard_delay(count)
      assert_equal [_time, _time+SMSC_PROCESS_TIME, _time+2*SMSC_PROCESS_TIME], start_times(count, true)
    end
  end

  test "should return correct sequence if midnight with permit_night_delivery" do
    current_time = Time.new(2002, 10, 31, 0, 0, 0)
    count = 3

    freeze(current_time) do
      _time = current_time + guard_delay(count)
      assert_equal [_time, _time+SMSC_PROCESS_TIME, _time+2*SMSC_PROCESS_TIME], start_times(count, true)
    end
  end

  test "should return correct sequence if almost midnight with permit_night_delivery" do
    current_time = Time.new(2002, 10, 31, 23, 59, 59)
    count = 3

    freeze(current_time) do
      _time = current_time + guard_delay(count)
      assert_equal [_time, _time+SMSC_PROCESS_TIME, _time+2*SMSC_PROCESS_TIME], start_times(count, true)
    end
  end

  test "should return correct sequence if almost morning with permit_night_delivery" do
    current_time = Time.new(2002, 10, 31, @begin_hour, @begin_min, @begin_sec) - SMSC_PROCESS_TIME
    count = 3

    freeze(current_time) do
      _time = current_time + guard_delay(count)
      assert_equal [_time, _time+SMSC_PROCESS_TIME, _time+2*SMSC_PROCESS_TIME], start_times(count, true)
    end
  end

  test "should return correct sequence if almost morning minus GUARD-DELAY with permit_night_delivery" do
    count = 3
    current_time = Time.new(2002, 10, 31, @begin_hour, @begin_min, @begin_sec) - SMSC_PROCESS_TIME - guard_delay(count)

    freeze(current_time) do
      _time = current_time + guard_delay(count)
      assert_equal [_time, _time+SMSC_PROCESS_TIME, _time+2*SMSC_PROCESS_TIME], start_times(count, true)
    end
  end

  test "should return correct sequence if almost evening with permit_night_delivery" do
    current_time = Time.new(2002, 10, 31, @end_hour, @end_min, @end_sec) - SMSC_PROCESS_TIME
    count = 3

    freeze(current_time) do
      _time = current_time + guard_delay(count)
      assert_equal [_time, _time+SMSC_PROCESS_TIME, _time+2*SMSC_PROCESS_TIME], start_times(count, true)
    end
  end

  test "should return correct sequence if almost evening minus GUARD-DELAY with permit_night_delivery" do
    count = 3
    current_time = Time.new(2002, 10, 31, @end_hour, @end_min, @end_sec) - SMSC_PROCESS_TIME - guard_delay(count)

    freeze(current_time) do
      _time = current_time + guard_delay(count)
      assert_equal [_time, _time+SMSC_PROCESS_TIME, _time+2*SMSC_PROCESS_TIME], start_times(count, true)
    end
  end

  private
  def check_raise_not_night(&block)
    exception = assert_raises(RuntimeError) do
      yield
    end

    assert_equal 'Time should be a part of night', exception.message
  end

end
