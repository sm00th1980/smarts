# -*- encoding : utf-8 -*-
require 'test_helper'

class Service::Users::BillingTest < ActiveSupport::TestCase
  def setup
    clear_db

    @user_with_negative_balance = FactoryGirl.create(:user_with_negative_balance)
    @user_with_payment = FactoryGirl.create(:user)

    @begin_date = Log.where(user_id: @user_with_negative_balance.id).order(:created_at)[0].created_at.to_date
    @end_date = Log.where(user_id: @user_with_negative_balance.id).order('created_at DESC')[0].created_at.to_date
  end

  test "charges-sum should return correct value" do
    Calculator::Logs.recalculate(@begin_date, @end_date)

    charges = SendLog.select("sum(price) as price").where(log_id: Log.where(:user_id => @user_with_negative_balance.id))[0].price.to_f
    assert_operator charges, :>, 0
    assert_equal charges, @user_with_negative_balance.charges_sum
  end

  test "payments-sum should return correct value" do
    payments = Payment.approved.where(:user_id => @user_with_payment.id).to_a.sum(&:amount)
    assert_operator payments, :>, 0
    assert_equal payments, @user_with_payment.payments_sum
  end

  test "current_balance should be diff between payment and charges" do
    charges = @user_with_negative_balance.charges_sum
    payments = @user_with_negative_balance.payments_sum

    assert_equal (payments - charges), @user_with_negative_balance.current_balance
  end

  test "should correct calculate charges_sum" do
    Calculator::Logs.recalculate(@begin_date, @end_date)

    charges = SendLog.select("sum(price) as price").where(log_id: Log.where(:user_id => @user_with_negative_balance.id))[0].price.to_f
    assert_equal charges, @user_with_negative_balance.charges_sum
  end

  test "should charges_sum be equal balance only" do
    charges = 100

    user = FactoryGirl.create(:user)

    Balance.delete_all

    Balance.create!(period: Balance.period(Date.today)[:period], user_id: user.id, payments: 0, charges: charges)
    FactoryGirl.create(:log_completed, user: user, price: charges, created_at: Time.now)

    assert_equal charges, user.charges_sum
  end
end
