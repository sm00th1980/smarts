# -*- encoding : utf-8 -*-
require 'test_helper'

class PaymentTest < ActiveSupport::TestCase
  def setup
    clear_db

    @user = FactoryGirl.create(:user)
    @demo = FactoryGirl.create(:demo)

    @payment_new = FactoryGirl.create(:payment_new_manual, user: @user)
    @payment_new_for_demo = FactoryGirl.create(:payment_new_manual, user: @demo)

    @payment_approved_manual = FactoryGirl.create(:payment_approved_manual, user: @user)
    @payment_failed_manual   = FactoryGirl.create(:payment_failed_manual, user: @user)

    @payment_approved_bank = FactoryGirl.create(:payment_approved_bank, user: @user)
    @payment_failed_bank   = FactoryGirl.create(:payment_failed_bank, user: @user)

    @payment_approved_robokassa = FactoryGirl.create(:payment_approved_robokassa, user: @user)
    @payment_failed_robokassa   = FactoryGirl.create(:payment_failed_robokassa, user: @user)

    @payment_approved_and_failed = FactoryGirl.create(:payment_approved_and_failed, user: @user)
  end

  #manual
  test "approved manual" do
    assert @payment_approved_manual.approved?
    refute @payment_approved_manual.failed?
    assert_equal PaymentService.manual, @payment_approved_manual.service
  end

  test "failed manual" do
    refute @payment_failed_manual.approved?
    assert @payment_failed_manual.failed?
    assert_equal PaymentService.manual, @payment_failed_manual.service
  end

  #bank
  test "approved bank" do
    assert @payment_approved_bank.approved?
    refute @payment_approved_bank.failed?
    assert_equal PaymentService.bank, @payment_approved_bank.service
  end

  test "failed bank" do
    refute @payment_failed_bank.approved?
    assert @payment_failed_bank.failed?
    assert_equal PaymentService.bank, @payment_failed_bank.service
  end

  #robokassa
  test "approved robokassa" do
    assert @payment_approved_robokassa.approved?
    refute @payment_approved_robokassa.failed?
    assert_equal PaymentService.robokassa, @payment_approved_robokassa.service
  end

  test "failed robokassa" do
    refute @payment_failed_robokassa.approved?
    assert @payment_failed_robokassa.failed?
    assert_equal PaymentService.robokassa, @payment_failed_robokassa.service
  end

  #approved and failed
  test "approved and failed should be failed" do
    assert_not_nil @payment_approved_and_failed.approved_at
    assert_not_nil @payment_approved_and_failed.failed_at

    refute @payment_approved_and_failed.approved?
    assert @payment_approved_and_failed.failed?
  end

  #scope approved
  test "should return only approved payments" do
    Payment.approved.each do |payment|
      assert payment.approved?
      refute payment.failed?
    end
  end

  #new?
  test "new payment should not have approved_at and failed_at" do
    assert @payment_new.new?
    assert_nil @payment_new.approved_at
    assert_nil @payment_new.failed_at

    refute @payment_new.approved?
    refute @payment_new.failed?
  end

  test "approved or failed payment should not be new" do
    refute @payment_approved_manual.new?
    refute @payment_approved_bank.new?
    refute @payment_approved_robokassa.new?

    refute @payment_failed_manual.new?
    refute @payment_failed_bank.new?
    refute @payment_failed_robokassa.new?

    refute @payment_approved_and_failed.new?
  end

  #approve!
  test "should approve new payment" do
    @payment_new.approve!
    assert @payment_new.reload.approved?
    refute @payment_new.reload.failed?
  end

  test "should not approve new payment for demo_user" do
    @payment_new_for_demo.approve!
    refute @payment_new_for_demo.reload.approved?
    refute @payment_new_for_demo.reload.failed?
    assert @payment_new_for_demo.reload.new?
  end

  test "should approve new payment for demo_user if admin" do
    @payment_new_for_demo.approve!(true)
    assert @payment_new_for_demo.reload.approved?
    refute @payment_new_for_demo.reload.failed?
    refute @payment_new_for_demo.reload.new?
  end

  test "should not approve any failed payment" do
    @payment_failed_manual.approve!
    refute @payment_failed_manual.reload.approved?
    assert @payment_failed_manual.reload.failed?

    @payment_failed_bank.approve!
    refute @payment_failed_bank.reload.approved?
    assert @payment_failed_bank.reload.failed?

    @payment_failed_robokassa.approve!
    refute @payment_failed_robokassa.reload.approved?
    assert @payment_failed_robokassa.reload.failed?

    @payment_approved_and_failed.approve!
    refute @payment_approved_and_failed.reload.approved?
    assert @payment_approved_and_failed.reload.failed?
  end

  #fail!
  test "should fail new payment" do
    @payment_new.fail!
    refute @payment_new.reload.approved?
    assert @payment_new.reload.failed?
  end
end
