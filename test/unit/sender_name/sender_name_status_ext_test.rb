# -*- encoding : utf-8 -*-
require 'test_helper'

class SenderNameStatusExtTest < ActiveSupport::TestCase

  def setup
    clear_db

    @user = FactoryGirl.create(:user)

    @sender_name_accepted_with_verified_value   = FactoryGirl.create(:sender_name_accepted_with_verified_value,   :user => @user)
    @sender_name_rejected_with_verified_value   = FactoryGirl.create(:sender_name_rejected_with_verified_value,   :user => @user)
    @sender_name_moderating_with_verified_value = FactoryGirl.create(:sender_name_moderating_with_verified_value, :user => @user)

    @sender_name_accepted_with_not_verified_value   = FactoryGirl.create(:sender_name_accepted_with_not_verified_value,   :user => @user)
    @sender_name_rejected_with_not_verified_value   = FactoryGirl.create(:sender_name_rejected_with_not_verified_value,   :user => @user)
    @sender_name_moderating_with_not_verified_value = FactoryGirl.create(:sender_name_moderating_with_not_verified_value, :user => @user)
  end

  #verified
  test "should distinguish accepted sender name with verified" do
    assert @sender_name_accepted_with_verified_value.accepted?
    refute @sender_name_accepted_with_verified_value.rejected?
    refute @sender_name_accepted_with_verified_value.moderating?
  end

  test "should distinguish rejected sender name with verified" do
    refute @sender_name_rejected_with_verified_value.accepted?
    assert @sender_name_rejected_with_verified_value.rejected?
    refute @sender_name_rejected_with_verified_value.moderating?
  end

  test "should distinguish moderating sender name with verified" do
    refute @sender_name_moderating_with_verified_value.accepted?
    refute @sender_name_moderating_with_verified_value.rejected?
    assert @sender_name_moderating_with_verified_value.moderating?
  end

  #not verified
  test "should distinguish accepted sender name with not verified" do
    refute @sender_name_accepted_with_not_verified_value.accepted?
    assert @sender_name_accepted_with_not_verified_value.rejected?
    refute @sender_name_accepted_with_not_verified_value.moderating?
  end

  test "should distinguish rejected sender name with not verified" do
    refute @sender_name_rejected_with_not_verified_value.accepted?
    assert @sender_name_rejected_with_not_verified_value.rejected?
    refute @sender_name_rejected_with_not_verified_value.moderating?
  end

  test "should distinguish moderating sender name with not verified" do
    refute @sender_name_moderating_with_not_verified_value.accepted?
    assert @sender_name_moderating_with_not_verified_value.rejected?
    refute @sender_name_moderating_with_not_verified_value.moderating?
  end
end
