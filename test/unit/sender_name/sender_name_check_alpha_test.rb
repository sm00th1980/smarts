# -*- encoding : utf-8 -*-
require 'test_helper'

class SenderNameCheckAlphaTest < ActiveSupport::TestCase
  def setup
    @alpha_sender_names = [
        'BRW-Mebel',
        'Ruslan',
        'Serafima',
        '9976898',
        'ANGELALUX',
        ' ANGELDELUX',
        'test1234',
        '8846224102',
        'frolov petr',
        'frolov-petr',
        'frolov_petr',
        'ruslan-24',
        'ruslan+24',
        'rus-rus-rus',
        'zolotco.ru',
        'zolotti.ru'
    ]

    @not_alpha_sender_names = [
        '79379903835',
        '79379000001',
        '79277013327',
        '89379903835',
        '89379000001',
        '89277013327',
        '78462241023',
        '88462241023',
        'Одежда из Белоруссии. Победы 69.',
        '8(937)990-38-35',
        '',
        nil,
        ' ',
        'еуыеtest',
        'testрус',
        'рус123'
    ]
  end

  test "should check alpha sender_name is alpha" do
    @alpha_sender_names.each do |sender_name_value|
      assert SenderName.alpha?(sender_name_value)
    end
  end

  test "should check not alpha sender_name is not alpha" do
    @not_alpha_sender_names.each do |sender_name_value|
      refute SenderName.alpha?(sender_name_value)
    end
  end

end
