# -*- encoding : utf-8 -*-
require 'test_helper'

class SenderNameValidByValueTest < ActiveSupport::TestCase
  def setup
    clear_db

    @user_def_reject = FactoryGirl.create(:user_with_permit_alpha_and_reject_def)
    @user_def_permit = FactoryGirl.create(:user_with_permit_alpha_and_permit_def)

    @user_with_active_sender_name = FactoryGirl.create(:user)
    @sender_name_active = FactoryGirl.create(:sender_name_accepted, :user => @user_with_active_sender_name)
    @sender_name_active.activate

    @user_without_sender_name = FactoryGirl.create(:user)

    @sender_name_on_moderating = FactoryGirl.create(:sender_name_on_moderating, :user => @user_with_active_sender_name)
    @sender_name_rejected = FactoryGirl.create(:sender_name_rejected, :user => @user_with_active_sender_name)
  end

  test "should permit active_sender_names by value" do
    assert SenderName.valid_by_value?(@sender_name_active.value, @user_with_active_sender_name)
  end

  test "should reject not his own active_sender_names" do
    refute SenderName.valid_by_value?(@sender_name_active.value, @user_without_sender_name)
  end

  test "should reject sender name by value on moderating" do
    refute SenderName.valid_by_value?(@sender_name_on_moderating.value, @user_with_active_sender_name)
  end

  test "should reject sender name by value if it rejected" do
    refute SenderName.valid_by_value?(@sender_name_rejected.value, @user_with_active_sender_name)
  end

end
