# -*- encoding : utf-8 -*-
require 'test_helper'

class SenderNameVerifyWithRejectAlphaAndPermitDefTest < ActiveSupport::TestCase
  def setup
    clear_db

    @user = FactoryGirl.create(:user_with_reject_alpha_and_permit_def)

    @valid_sender_names = [
        '79379903835',
        '79379000001',
        '79277013327'
    ]

    @invalid_sender_names = [
        '89379903835',
        '89379000001',
        '89277013327',
        '78462241023',
        '88462241023',
        'Одежда из Белоруссии. Победы 69.',
        '8(937)990-38-35',
        '',
        nil,
        ' ',
        'еуыеtest',
        'testрус',
        'рус123',
        'BRW-Mebel',
        'Ruslan',
        'Serafima',
        '9976898',
        'ANGELALUX',
        ' ANGELDELUX',
        'test1234',
        '8846224102',
        'frolov petr',
        'frolov-petr',
        'frolov_petr',
        'ruslan-24',
        'ruslan+24',
        'rus-rus-rus',
        'zolotco.ru',
        'zolotti.ru'
    ]
  end

  test "should verified sender_names for reject alpha and permit def" do
    @valid_sender_names.each do |sender_name_value|
      assert SenderName.verified?(sender_name_value, @user)
    end
  end

  test "should not verified sender_names for reject alpha and permit def" do
    @invalid_sender_names.each do |sender_name_value|
      refute SenderName.verified?(sender_name_value, @user)
    end
  end

end
