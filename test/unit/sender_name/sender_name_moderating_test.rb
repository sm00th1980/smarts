# -*- encoding : utf-8 -*-
require 'test_helper'

class SenderNameModeratingTest < ActiveSupport::TestCase

  def setup
    clear_db

    @user = FactoryGirl.create(:user)

    @sender_name_accepted_with_verified_value   = FactoryGirl.create(:sender_name_accepted_with_verified_value,   :user => @user)
    @sender_name_rejected_with_verified_value   = FactoryGirl.create(:sender_name_rejected_with_verified_value,   :user => @user)
    @sender_name_moderating_with_verified_value = FactoryGirl.create(:sender_name_moderating_with_verified_value, :user => @user)

    @sender_name_accepted_with_not_verified_value   = FactoryGirl.create(:sender_name_accepted_with_not_verified_value,   :user => @user)
    @sender_name_rejected_with_not_verified_value   = FactoryGirl.create(:sender_name_rejected_with_not_verified_value,   :user => @user)
    @sender_name_moderating_with_not_verified_value = FactoryGirl.create(:sender_name_moderating_with_not_verified_value, :user => @user)
  end

  #accept verified
  test "should be accepted after accept accepted_sender_name with verified" do
    @sender_name_accepted_with_verified_value.accept
    assert @sender_name_accepted_with_verified_value.reload.accepted?
  end

  test "should be rejected after accept rejected_sender_name with verified" do
    @sender_name_rejected_with_verified_value.accept
    assert @sender_name_rejected_with_verified_value.reload.rejected?
  end

  test "should be accepted after accept moderating_sender_name with verified" do
    @sender_name_moderating_with_verified_value.accept
    assert @sender_name_moderating_with_verified_value.reload.accepted?
  end

  #accept not verified
  test "should be rejected after accept accepted_sender_name with not verified" do
    @sender_name_accepted_with_not_verified_value.accept
    assert @sender_name_accepted_with_not_verified_value.reload.rejected?
  end

  test "should be rejected after accept rejected_sender_name with not verified" do
    @sender_name_rejected_with_not_verified_value.accept
    assert @sender_name_rejected_with_not_verified_value.reload.rejected?
  end

  test "should be rejected after accept moderating_sender_name with not verified" do
    @sender_name_moderating_with_not_verified_value.accept
    assert @sender_name_moderating_with_not_verified_value.reload.rejected?
  end

  #reject with verified
  test "should be rejected after reject accepted_sender_name with verified" do
    @sender_name_accepted_with_verified_value.reject
    assert @sender_name_accepted_with_verified_value.reload.rejected?
  end

  test "should be rejected after reject rejected_sender_name with verified" do
    @sender_name_rejected_with_verified_value.reject
    assert @sender_name_rejected_with_verified_value.reload.rejected?
  end

  test "should be accepted after reject moderating_sender_name with verified" do
    @sender_name_moderating_with_verified_value.reject
    assert @sender_name_moderating_with_verified_value.reload.rejected?
  end

  #reject not verified
  test "should be rejected after reject accepted_sender_name with not verified" do
    @sender_name_accepted_with_not_verified_value.reject
    assert @sender_name_accepted_with_not_verified_value.reload.rejected?
  end

  test "should be rejected after reject rejected_sender_name with not verified" do
    @sender_name_rejected_with_not_verified_value.reject
    assert @sender_name_rejected_with_not_verified_value.reload.rejected?
  end

  test "should be rejected after reject moderating_sender_name with not verified" do
    @sender_name_moderating_with_not_verified_value.reject
    assert @sender_name_moderating_with_not_verified_value.reload.rejected?
  end
end
