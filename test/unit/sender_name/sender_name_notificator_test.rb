# -*- encoding : utf-8 -*-
require 'test_helper'

class SenderNameNotificatorTest < ActiveSupport::TestCase
  def setup
    clear_db

    @user = FactoryGirl.create(:user)

    @valid_value = '79379903835'
    @invalid_value = '793799038353278943'

    @sender_name = FactoryGirl.create(:sender_name_accepted, :user => @user)
    Sidekiq::Testing.fake!
  end

  #create
  test "should create sender_name for valid_value" do
    assert_difference 'SenderName.count' do
      new_sender_name = SenderName.create_with_notify(@user, @valid_value)

      assert new_sender_name.moderating?
      assert_equal SenderName.find_by_user_id_and_value(@user.id, @valid_value), new_sender_name
    end
  end

  test "should not create for invalid value" do
    assert_no_difference 'SenderName.count' do
      assert_nil SenderName.create_with_notify(@user, @invalid_value)
    end
  end

#update
  test "should update to moderating after change to valid value" do
    assert_no_difference 'SenderName.count' do
      assert @sender_name.update_with_notify(@valid_value)

      assert SenderName.find_by_user_id_and_value(@user.id, @valid_value).moderating?
    end
  end

  test "should update to rejected after change to invalid value" do
    assert_no_difference 'SenderName.count' do
      refute @sender_name.update_with_notify(@invalid_value)

      assert SenderName.find_by_user_id_and_value(@user.id, @invalid_value).rejected?
    end
  end

end
