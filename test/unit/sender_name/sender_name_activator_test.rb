# -*- encoding : utf-8 -*-
require 'test_helper'

class SenderNameActivatorTest < ActiveSupport::TestCase

  def setup
    clear_db

    @user = FactoryGirl.create(:user)

    @sender_name_accepted   = FactoryGirl.create(:sender_name_accepted,   :user => @user)
    @sender_name_rejected   = FactoryGirl.create(:sender_name_rejected,   :user => @user)
    @sender_name_moderating = FactoryGirl.create(:sender_name_moderating, :user => @user)
  end

  test "should activate accepted_sender_name" do
    refute @sender_name_accepted.reload.active?
    @sender_name_accepted.activate
    assert @sender_name_accepted.reload.active?
  end

  test "should not activate rejected_sender_name" do
    refute @sender_name_rejected.reload.active?
    @sender_name_rejected.activate
    refute @sender_name_rejected.reload.active?
  end

  test "should not activate moderating_sender_name" do
    refute @sender_name_moderating.reload.active?
    @sender_name_moderating.activate
    refute @sender_name_moderating.reload.active?
  end

  test "should deactivate previous active after activation new one" do
    sender_name_accepted2  = FactoryGirl.create(:sender_name_accepted, :user => @user)

    @sender_name_accepted.activate
    sender_name_accepted2.activate

    refute @sender_name_accepted.reload.active?
  end

end
