# -*- encoding : utf-8 -*-
require 'test_helper'

class SenderNameCheckDefTest < ActiveSupport::TestCase
  def setup
    @def_sender_names = [
        '79379903835',
        '79379000001',
        '79277013327'
    ]

    @not_def_sender_names = [
        '89379903835',
        '89379000001',
        '89277013327',
        '78462241023',
        '88462241023',
        'Одежда из Белоруссии. Победы 69.',
        '8(937)990-38-35',
        '',
        nil,
        ' ',
        'еуыеtest',
        'testрус',
        'рус123',
        'BRW-Mebel',
        'Ruslan',
        'Serafima',
        '9976898',
        'ANGELALUX',
        ' ANGELDELUX',
        'test1234',
        '8846224102',
        'frolov petr',
        'frolov-petr',
        'frolov_petr',
        'ruslan-24',
        'ruslan+24',
        'rus-rus-rus',
        'zolotco.ru',
        'zolotti.ru'
    ]
  end

  test "should check def sender_name is def" do
    @def_sender_names.each do |sender_name_value|
      assert SenderName.def?(sender_name_value)
    end
  end

  test "should check not def sender_name is not def" do
    @not_def_sender_names.each do |sender_name_value|
      refute SenderName.def?(sender_name_value)
    end
  end

end
