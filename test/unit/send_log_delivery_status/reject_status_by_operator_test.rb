# -*- encoding : utf-8 -*-
require 'test_helper'

class SendLogDeliveryStatus::RejectStatusByOperatorTest < ActiveSupport::TestCase
  def setup
    clear_db
  end

  test "should return reject-to-megafon for megafon" do
    assert_equal SendLogDeliveryStatus.reject_to_megafon, SendLogDeliveryStatus.reject_status_by_operator(CellOperatorGroup.megafon)
  end

  test "should return reject-to-smarts for smarts" do
    assert_equal SendLogDeliveryStatus.reject_to_smarts, SendLogDeliveryStatus.reject_status_by_operator(CellOperatorGroup.smarts)
  end

  test "should return reject-to-mts for mts" do
    assert_equal SendLogDeliveryStatus.reject_to_mts, SendLogDeliveryStatus.reject_status_by_operator(CellOperatorGroup.mts)
  end

  test "should be nil for others operators" do
    (CellOperatorGroup.working - [CellOperatorGroup.megafon, CellOperatorGroup.mts, CellOperatorGroup.smarts]).each do |operator|
      assert_nil SendLogDeliveryStatus.reject_status_by_operator(operator)
    end
  end

end
