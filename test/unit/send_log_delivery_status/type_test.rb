# -*- encoding : utf-8 -*-
require 'test_helper'

class SendLogDeliveryStatus::TypeTest < ActiveSupport::TestCase
  def setup
    clear_db
  end

  test "unknown status should exist" do
    assert_not_nil SendLogDeliveryStatus.unknown
    assert_equal 'unknown status', SendLogDeliveryStatus.unknown.internal_name
  end

  test "delivered status should exist" do
    assert_not_nil SendLogDeliveryStatus.delivered
    assert_equal 'delivery success', SendLogDeliveryStatus.delivered.internal_name
  end

  test "failured status should exist" do
    assert_not_nil SendLogDeliveryStatus.failured
    assert_equal 'delivery failure', SendLogDeliveryStatus.failured.internal_name
  end

  test "buffered status should exist" do
    assert_not_nil SendLogDeliveryStatus.buffered
    assert_equal 'message buffered', SendLogDeliveryStatus.buffered.internal_name
  end

  test "submit status should exist" do
    assert_not_nil SendLogDeliveryStatus.submit
    assert_equal 'smsc submit', SendLogDeliveryStatus.submit.internal_name
  end

  test "rejected status should exist" do
    assert_not_nil SendLogDeliveryStatus.rejected
    assert_equal 'smsc reject', SendLogDeliveryStatus.rejected.internal_name
  end

  test "rejected status to megafon should exist" do
    assert_not_nil SendLogDeliveryStatus.reject_to_megafon
    assert_equal 'reject to megafon', SendLogDeliveryStatus.reject_to_megafon.internal_name
  end

  test "rejected status to smarts should exist" do
    assert_not_nil SendLogDeliveryStatus.reject_to_smarts
    assert_equal 'reject to smarts', SendLogDeliveryStatus.reject_to_smarts.internal_name
  end

  test "rejected status to mts should exist" do
    assert_not_nil SendLogDeliveryStatus.reject_to_mts
    assert_equal 'reject to mts', SendLogDeliveryStatus.reject_to_mts.internal_name
  end

end
