# -*- encoding : utf-8 -*-
require 'test_helper'

class SenderNameTest < ActiveSupport::TestCase
  def setup
    clear_db

    @user_with_active_sender_name = FactoryGirl.create(:user)
    @sender_name_active = FactoryGirl.create(:sender_name_accepted, :user => @user_with_active_sender_name)
    @sender_name_active.activate

    @user_without_sender_name = FactoryGirl.create(:user)

    @user_with_several_active_sender_names = FactoryGirl.create(:user)
    sender_name_active1  = FactoryGirl.create(:sender_name_accepted, user: @user_with_several_active_sender_names, value: '79379903835')
    @sender_name_active2 = FactoryGirl.create(:sender_name_accepted, user: @user_with_several_active_sender_names, value: '79379903836')

    sender_name_active1.activate
    @sender_name_active2.activate
  end

  test "should return active sender_name_value for user with active sender_name" do
    assert_equal @sender_name_active.value.to_s, SenderName.active_sender_name_value(@user_with_active_sender_name).to_s
  end

  test "should return default sender_name_value for user without sender_name" do
    assert_nil SenderName.active_sender_name_value(@user_without_sender_name)
  end

  test "should return last activated sender_name_value for user with several active sender_name" do
    assert_equal @sender_name_active2.value.to_s, SenderName.active_sender_name_value(@user_with_several_active_sender_names).to_s
  end

end
