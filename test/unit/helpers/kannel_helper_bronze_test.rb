# -*- encoding : utf-8 -*-
require 'test_helper'

class KannelHelperBronzeTest < ActionView::TestCase
  include KannelHelper

  def setup
    clear_db

    @parse = lambda { |ack| parse_error_code(ack, Channel.bronze) }
  end

  #unkwnown error
  test "should return error unknown on intermediate ack" do
    assert_equal SendLogDeliveryError.bronze_error_unknown, @parse.call('ACK/')
  end

  test "should return error unknown on intermediate nack" do
    assert_equal SendLogDeliveryError.bronze_error_unknown, @parse.call('NACK/')
    assert_equal SendLogDeliveryError.bronze_error_unknown, @parse.call('NACK/no SMSC')
  end

  test "should return error unknow on empty ack" do
    assert_equal SendLogDeliveryError.bronze_error_unknown, @parse.call('')
  end

  test "should return error unknow on nil ack" do
    assert_equal SendLogDeliveryError.bronze_error_unknown, @parse.call(nil)
  end

  test "should return error unknow on invalid ack" do
    assert_equal SendLogDeliveryError.bronze_error_unknown, @parse.call('-1vfjdiovjig')
  end

  test "should return error unknown on rejected with unknown err" do
    assert_equal SendLogDeliveryError.bronze_error_unknown, @parse.call('id:140603577649571902 sub:001 dlvrd:001 submit date:1407221729 done date:1407231729 stat:REJECTD err:255 text:')
  end

  test "should return error unknown on undelivr with unknown err" do
    assert_equal SendLogDeliveryError.bronze_error_unknown, @parse.call('id:140603577649571902 sub:001 dlvrd:001 submit date:1407221729 done date:1407231729 stat:UNDELIV err:255 text:')
  end

  test "should return unknown error on no_error code" do
    assert_equal SendLogDeliveryError.bronze_error_unknown, @parse.call('id:140603577649571902 sub:001 dlvrd:001 submit date:1407221729 done date:1407231729 stat:UNDELIV err:000 text:')
    assert_equal SendLogDeliveryError.bronze_error_unknown, @parse.call('id:140603577649571902 sub:001 dlvrd:001 submit date:1407221729 done date:1407231729 stat:REJECTD err:000 text:')
  end

  #extended errors
  test "should return throttle error code" do
    assert_equal SendLogDeliveryError.bronze_retries_exceeded, @parse.call('NACK/Retries Exceeded')
  end

  test "should return expired error code" do
    #samara
    assert_equal SendLogDeliveryError.bronze_expired, @parse.call('id:140603577649571902 sub:001 dlvrd:001 submit date:1407221729 done date:1407231729 stat:EXPIRED err:247 text:')

    #penza
    assert_equal SendLogDeliveryError.bronze_expired, @parse.call('id:000000002DD65BD0#1_1_79273755055 sub:000 dlvrd:000 submit date:1405291943 done date:1405301943 stat:EXPIRED err:255')

    assert_equal SendLogDeliveryError.bronze_expired, @parse.call('id:140603577649571902 sub:001 dlvrd:001 submit date:1407221729 done date:1407231729 stat:EXPIRED err:000 text:')
  end

  #known errors
  test "should return 0 error code" do
    #samara
    assert_equal SendLogDeliveryError.bronze_no_error, @parse.call('id:1407149789862fcb02 sub:001 dlvrd:001 submit date:1408041456 done date:1408041456 stat:DELIVRD err:000 text:')

    #penza
    assert_equal SendLogDeliveryError.bronze_no_error, @parse.call('id:000000003081E76F#1_1_79273957110 sub:000 dlvrd:000 submit date:1408041446 done date:1408041446 stat:DELIVRD Text: msc:79272909093 imsi:250020388616235')
  end

  test "should return 1 error code" do
    #samara
    assert_equal SendLogDeliveryError.bronze_error_unknown_subscriber, @parse.call('id:1406131939690f3402 sub:001 dlvrd:001 submit date:1407232012 done date:1407240012 stat:REJECTD err:001 text:')
    assert_equal SendLogDeliveryError.bronze_error_unknown_subscriber, @parse.call('id:1406131939690f3402 sub:001 dlvrd:001 submit date:1407232012 done date:1407240012 stat:UNDELIV err:001 text:')
    assert_equal SendLogDeliveryError.bronze_error_unknown_subscriber, @parse.call('id:1406131939690f3402 sub:001 dlvrd:001 submit date:1407232012 done date:1407240012 stat:EXPIRED err:001 text:')

    #penza
    assert_equal SendLogDeliveryError.bronze_error_unknown_subscriber, @parse.call('id:00000000307CA1D8#1_1_79534440970 sub:000 dlvrd:000 submit date:1408041226 done date:1408041226 stat:REJECTD err:001')
    assert_equal SendLogDeliveryError.bronze_error_unknown_subscriber, @parse.call('id:00000000307CA1D8#1_1_79534440970 sub:000 dlvrd:000 submit date:1408041226 done date:1408041226 stat:UNDELIV err:001')
    assert_equal SendLogDeliveryError.bronze_error_unknown_subscriber, @parse.call('id:00000000307CA1D8#1_1_79534440970 sub:000 dlvrd:000 submit date:1408041226 done date:1408041226 stat:EXPIRED err:001')
  end

  test "should return 5 error code" do
    #samara
    assert_equal SendLogDeliveryError.bronze_error_unidentified_subscriber, @parse.call('id:1406133184098d3701 sub:001 dlvrd:001 submit date:1407232033 done date:1407240034 stat:REJECTD err:005 text:')
    assert_equal SendLogDeliveryError.bronze_error_unidentified_subscriber, @parse.call('id:1406133184098d3701 sub:001 dlvrd:001 submit date:1407232033 done date:1407240034 stat:UNDELIV err:005 text:')
    assert_equal SendLogDeliveryError.bronze_error_unidentified_subscriber, @parse.call('id:1406133184098d3701 sub:001 dlvrd:001 submit date:1407232033 done date:1407240034 stat:EXPIRED err:005 text:')

    #penza
    assert_equal SendLogDeliveryError.bronze_error_unidentified_subscriber, @parse.call('id:000000002D9ACD0F#1_1_79061593860 sub:000 dlvrd:000 submit date:1405091402 done date:1405091402 stat:REJECTD err:005')
    assert_equal SendLogDeliveryError.bronze_error_unidentified_subscriber, @parse.call('id:000000002D9ACD0F#1_1_79061593860 sub:000 dlvrd:000 submit date:1405091402 done date:1405091402 stat:UNDELIV err:005')
    assert_equal SendLogDeliveryError.bronze_error_unidentified_subscriber, @parse.call('id:000000002D9ACD0F#1_1_79061593860 sub:000 dlvrd:000 submit date:1405091402 done date:1405091402 stat:EXPIRED err:005')
  end

  test "should return 6 error code" do
    #samara
    assert_equal SendLogDeliveryError.bronze_error_absent_subscriber_sm, @parse.call('id:140610581567528b01 sub:001 dlvrd:001 submit date:1407231256 done date:1407240035 stat:REJECTD err:006 text:')
    assert_equal SendLogDeliveryError.bronze_error_absent_subscriber_sm, @parse.call('id:140610581567528b01 sub:001 dlvrd:001 submit date:1407231256 done date:1407240035 stat:UNDELIV err:006 text:')
    assert_equal SendLogDeliveryError.bronze_error_absent_subscriber_sm, @parse.call('id:140610581567528b01 sub:001 dlvrd:001 submit date:1407231256 done date:1407240035 stat:EXPIRED err:006 text:')

    #penza - syntetic
    assert_equal SendLogDeliveryError.bronze_error_absent_subscriber_sm, @parse.call('id:000000002D9ACD0F#1_1_79061593860 sub:000 dlvrd:000 submit date:1405091402 done date:1405091402 stat:REJECTD err:006')
    assert_equal SendLogDeliveryError.bronze_error_absent_subscriber_sm, @parse.call('id:000000002D9ACD0F#1_1_79061593860 sub:000 dlvrd:000 submit date:1405091402 done date:1405091402 stat:UNDELIV err:006')
    assert_equal SendLogDeliveryError.bronze_error_absent_subscriber_sm, @parse.call('id:000000002D9ACD0F#1_1_79061593860 sub:000 dlvrd:000 submit date:1405091402 done date:1405091402 stat:EXPIRED err:006')
  end

  test "should return 9 error code" do
    #samara
    assert_equal SendLogDeliveryError.bronze_error_illegal_subscriber, @parse.call('id:14061023859331a501 sub:001 dlvrd:001 submit date:1407231159 done date:1407231300 stat:REJECTD err:009 text:')
    assert_equal SendLogDeliveryError.bronze_error_illegal_subscriber, @parse.call('id:14061023859331a501 sub:001 dlvrd:001 submit date:1407231159 done date:1407231300 stat:UNDELIV err:009 text:')
    assert_equal SendLogDeliveryError.bronze_error_illegal_subscriber, @parse.call('id:14061023859331a501 sub:001 dlvrd:001 submit date:1407231159 done date:1407231300 stat:EXPIRED err:009 text:')

    #penza - syntetic
    assert_equal SendLogDeliveryError.bronze_error_illegal_subscriber, @parse.call('id:000000002D611AA8#1_1_79277807283 sub:000 dlvrd:000 submit date:1404291041 done date:1404291041 stat:REJECTD err:009')
    assert_equal SendLogDeliveryError.bronze_error_illegal_subscriber, @parse.call('id:000000002D611AA8#1_1_79277807283 sub:000 dlvrd:000 submit date:1404291041 done date:1404291041 stat:UNDELIV err:009')
    assert_equal SendLogDeliveryError.bronze_error_illegal_subscriber, @parse.call('id:000000002D611AA8#1_1_79277807283 sub:000 dlvrd:000 submit date:1404291041 done date:1404291041 stat:EXPIRED err:009')
  end

  test "should return 11 error code" do
    #samara
    assert_equal SendLogDeliveryError.bronze_error_teleservice_not_provisioned, @parse.call('id:140614332882826101 sub:001 dlvrd:001 submit date:1407232322 done date:1407232322 stat:REJECTD err:011 text:')
    assert_equal SendLogDeliveryError.bronze_error_teleservice_not_provisioned, @parse.call('id:140614332882826101 sub:001 dlvrd:001 submit date:1407232322 done date:1407232322 stat:UNDELIV err:011 text:')
    assert_equal SendLogDeliveryError.bronze_error_teleservice_not_provisioned, @parse.call('id:140614332882826101 sub:001 dlvrd:001 submit date:1407232322 done date:1407232322 stat:EXPIRED err:011 text:')

    #penza
    assert_equal SendLogDeliveryError.bronze_error_teleservice_not_provisioned, @parse.call('id:000000002D615B72#1_1_79204059232 sub:000 dlvrd:000 submit date:1404291053 done date:1404291053 stat:REJECTD err:011')
    assert_equal SendLogDeliveryError.bronze_error_teleservice_not_provisioned, @parse.call('id:000000002D615B72#1_1_79204059232 sub:000 dlvrd:000 submit date:1404291053 done date:1404291053 stat:UNDELIV err:011')
    assert_equal SendLogDeliveryError.bronze_error_teleservice_not_provisioned, @parse.call('id:000000002D615B72#1_1_79204059232 sub:000 dlvrd:000 submit date:1404291053 done date:1404291053 stat:EXPIRED err:011')
  end

  test "should return 12 error code" do
    #samara
    assert_equal SendLogDeliveryError.bronze_error_illegal_equipment, @parse.call('id:1406097935838e6902 sub:001 dlvrd:001 submit date:1407231045 done date:1407231045 stat:REJECTD err:012 text:')
    assert_equal SendLogDeliveryError.bronze_error_illegal_equipment, @parse.call('id:1406097935838e6902 sub:001 dlvrd:001 submit date:1407231045 done date:1407231045 stat:UNDELIV err:012 text:')
    assert_equal SendLogDeliveryError.bronze_error_illegal_equipment, @parse.call('id:1406097935838e6902 sub:001 dlvrd:001 submit date:1407231045 done date:1407231045 stat:EXPIRED err:012 text:')

    #penza - syntetic
    assert_equal SendLogDeliveryError.bronze_error_illegal_equipment, @parse.call('id:000000002D615B72#1_1_79204059232 sub:000 dlvrd:000 submit date:1404291053 done date:1404291053 stat:REJECTD err:012')
    assert_equal SendLogDeliveryError.bronze_error_illegal_equipment, @parse.call('id:000000002D615B72#1_1_79204059232 sub:000 dlvrd:000 submit date:1404291053 done date:1404291053 stat:UNDELIV err:012')
    assert_equal SendLogDeliveryError.bronze_error_illegal_equipment, @parse.call('id:000000002D615B72#1_1_79204059232 sub:000 dlvrd:000 submit date:1404291053 done date:1404291053 stat:EXPIRED err:012')
  end

  test "should return 13 error code" do
    #samara
    assert_equal SendLogDeliveryError.bronze_error_call_barred, @parse.call('id:140613332831178e01 sub:001 dlvrd:001 submit date:1407232035 done date:1407240035 stat:REJECTD err:013 text:')
    assert_equal SendLogDeliveryError.bronze_error_call_barred, @parse.call('id:140613332831178e01 sub:001 dlvrd:001 submit date:1407232035 done date:1407240035 stat:UNDELIV err:013 text:')
    assert_equal SendLogDeliveryError.bronze_error_call_barred, @parse.call('id:140613332831178e01 sub:001 dlvrd:001 submit date:1407232035 done date:1407240035 stat:EXPIRED err:013 text:')

    #penza
    assert_equal SendLogDeliveryError.bronze_error_call_barred, @parse.call('id:000000002D615C1D#1_1_79292964061 sub:000 dlvrd:000 submit date:1404291053 done date:1404291053 stat:REJECTD err:013')
    assert_equal SendLogDeliveryError.bronze_error_call_barred, @parse.call('id:000000002D615C1D#1_1_79292964061 sub:000 dlvrd:000 submit date:1404291053 done date:1404291053 stat:UNDELIV err:013')
    assert_equal SendLogDeliveryError.bronze_error_call_barred, @parse.call('id:000000002D615C1D#1_1_79292964061 sub:000 dlvrd:000 submit date:1404291053 done date:1404291053 stat:EXPIRED err:013')
  end

  test "should return 21 error code" do
    #samara - syntetic
    assert_equal SendLogDeliveryError.bronze_error_facility_not_supported, @parse.call('id:140613332831178e01 sub:001 dlvrd:001 submit date:1407232035 done date:1407240035 stat:REJECTD err:021 text:')
    assert_equal SendLogDeliveryError.bronze_error_facility_not_supported, @parse.call('id:140613332831178e01 sub:001 dlvrd:001 submit date:1407232035 done date:1407240035 stat:UNDELIV err:021 text:')
    assert_equal SendLogDeliveryError.bronze_error_facility_not_supported, @parse.call('id:140613332831178e01 sub:001 dlvrd:001 submit date:1407232035 done date:1407240035 stat:EXPIRED err:021 text:')

    #penza - syntetic
    assert_equal SendLogDeliveryError.bronze_error_facility_not_supported, @parse.call('id:000000002D615C1D#1_1_79292964061 sub:000 dlvrd:000 submit date:1404291053 done date:1404291053 stat:REJECTD err:021')
    assert_equal SendLogDeliveryError.bronze_error_facility_not_supported, @parse.call('id:000000002D615C1D#1_1_79292964061 sub:000 dlvrd:000 submit date:1404291053 done date:1404291053 stat:UNDELIV err:021')
    assert_equal SendLogDeliveryError.bronze_error_facility_not_supported, @parse.call('id:000000002D615C1D#1_1_79292964061 sub:000 dlvrd:000 submit date:1404291053 done date:1404291053 stat:EXPIRED err:021')
  end

  test "should return 27 error code" do
    #samara
    assert_equal SendLogDeliveryError.bronze_error_absent_subscriber, @parse.call('id:14061058075621e701 sub:001 dlvrd:001 submit date:1407231256 done date:1407240035 stat:REJECTD err:027 text:')
    assert_equal SendLogDeliveryError.bronze_error_absent_subscriber, @parse.call('id:140603577649571902 sub:001 dlvrd:001 submit date:1407221729 done date:1407231729 stat:UNDELIV err:027 text:')
    assert_equal SendLogDeliveryError.bronze_error_absent_subscriber, @parse.call('id:140603577649571902 sub:001 dlvrd:001 submit date:1407221729 done date:1407231729 stat:EXPIRED err:027 text:')

    #penza
    assert_equal SendLogDeliveryError.bronze_error_absent_subscriber, @parse.call('id:000000002DD6530E#1_1_79063992171 sub:000 dlvrd:000 submit date:1405291939 done date:1405300433 stat:REJECTD err:027')
    assert_equal SendLogDeliveryError.bronze_error_absent_subscriber, @parse.call('id:000000002DD6530E#1_1_79063992171 sub:000 dlvrd:000 submit date:1405291939 done date:1405300433 stat:EXPIRED err:027')
    assert_equal SendLogDeliveryError.bronze_error_absent_subscriber, @parse.call('id:000000002DD6530E#1_1_79063992171 sub:000 dlvrd:000 submit date:1405291939 done date:1405300433 stat:EXPIRED err:027')
  end

  test "should return 28 error code" do
    #samara - syntetic
    assert_equal SendLogDeliveryError.bronze_error_incompatible_terminal, @parse.call('id:14061058075621e701 sub:001 dlvrd:001 submit date:1407231256 done date:1407240035 stat:REJECTD err:028 text:')
    assert_equal SendLogDeliveryError.bronze_error_incompatible_terminal, @parse.call('id:14061058075621e701 sub:001 dlvrd:001 submit date:1407231256 done date:1407240035 stat:UNDELIV err:028 text:')
    assert_equal SendLogDeliveryError.bronze_error_incompatible_terminal, @parse.call('id:14061058075621e701 sub:001 dlvrd:001 submit date:1407231256 done date:1407240035 stat:EXPIRED err:028 text:')

    #penza - syntetic
    assert_equal SendLogDeliveryError.bronze_error_incompatible_terminal, @parse.call('id:000000002DD6530E#1_1_79063992171 sub:000 dlvrd:000 submit date:1405291939 done date:1405300433 stat:REJECTD err:028')
    assert_equal SendLogDeliveryError.bronze_error_incompatible_terminal, @parse.call('id:000000002DD6530E#1_1_79063992171 sub:000 dlvrd:000 submit date:1405291939 done date:1405300433 stat:UNDELIV err:028')
    assert_equal SendLogDeliveryError.bronze_error_incompatible_terminal, @parse.call('id:000000002DD6530E#1_1_79063992171 sub:000 dlvrd:000 submit date:1405291939 done date:1405300433 stat:EXPIRED err:028')
  end

  test "should return 31 error code" do
    #samara
    assert_equal SendLogDeliveryError.bronze_error_subscriber_busy_for_mt_sms, @parse.call('id:1406097789616c2e01 sub:001 dlvrd:001 submit date:1407231043 done date:1407232337 stat:REJECTD err:031 text:')
    assert_equal SendLogDeliveryError.bronze_error_subscriber_busy_for_mt_sms, @parse.call('id:1406097789616c2e01 sub:001 dlvrd:001 submit date:1407231043 done date:1407232337 stat:UNDELIV err:031 text:')
    assert_equal SendLogDeliveryError.bronze_error_subscriber_busy_for_mt_sms, @parse.call('id:1406097789616c2e01 sub:001 dlvrd:001 submit date:1407231043 done date:1407232337 stat:EXPIRED err:031 text:')

    #penza - syntetic
    assert_equal SendLogDeliveryError.bronze_error_subscriber_busy_for_mt_sms, @parse.call('id:000000002DD6530E#1_1_79063992171 sub:000 dlvrd:000 submit date:1405291939 done date:1405300433 stat:REJECTD err:031')
    assert_equal SendLogDeliveryError.bronze_error_subscriber_busy_for_mt_sms, @parse.call('id:000000002DD6530E#1_1_79063992171 sub:000 dlvrd:000 submit date:1405291939 done date:1405300433 stat:UNDELIV err:031')
    assert_equal SendLogDeliveryError.bronze_error_subscriber_busy_for_mt_sms, @parse.call('id:000000002DD6530E#1_1_79063992171 sub:000 dlvrd:000 submit date:1405291939 done date:1405300433 stat:EXPIRED err:031')
  end

  test "should return 32 error code" do
    #samara
    assert_equal SendLogDeliveryError.bronze_error_sm_delivery_failure, @parse.call('id:1406124255592f4401 sub:001 dlvrd:001 submit date:1407231804 done date:1407240034 stat:REJECTD err:032 text:')
    assert_equal SendLogDeliveryError.bronze_error_sm_delivery_failure, @parse.call('id:1406124255592f4401 sub:001 dlvrd:001 submit date:1407231804 done date:1407240034 stat:UNDELIV err:032 text:')
    assert_equal SendLogDeliveryError.bronze_error_sm_delivery_failure, @parse.call('id:1406124255592f4401 sub:001 dlvrd:001 submit date:1407231804 done date:1407240034 stat:EXPIRED err:032 text:')

    #penza
    assert_equal SendLogDeliveryError.bronze_error_sm_delivery_failure, @parse.call('id:000000002D6159AF#1_1_79204560439 sub:000 dlvrd:000 submit date:1404291052 done date:1404291053 stat:REJECTD err:032')
    assert_equal SendLogDeliveryError.bronze_error_sm_delivery_failure, @parse.call('id:000000002D6159AF#1_1_79204560439 sub:000 dlvrd:000 submit date:1404291052 done date:1404291053 stat:UNDELIV err:032')
    assert_equal SendLogDeliveryError.bronze_error_sm_delivery_failure, @parse.call('id:000000002D6159AF#1_1_79204560439 sub:000 dlvrd:000 submit date:1404291052 done date:1404291053 stat:EXPIRED err:032')
  end

  test "should return 33 error code" do
    #samara - syntetic
    assert_equal SendLogDeliveryError.bronze_error_message_waiting_list_full, @parse.call('id:1406124255592f4401 sub:001 dlvrd:001 submit date:1407231804 done date:1407240034 stat:REJECTD err:033 text:')
    assert_equal SendLogDeliveryError.bronze_error_message_waiting_list_full, @parse.call('id:1406124255592f4401 sub:001 dlvrd:001 submit date:1407231804 done date:1407240034 stat:UNDELIV err:033 text:')
    assert_equal SendLogDeliveryError.bronze_error_message_waiting_list_full, @parse.call('id:1406124255592f4401 sub:001 dlvrd:001 submit date:1407231804 done date:1407240034 stat:EXPIRED err:033 text:')

    #penza
    assert_equal SendLogDeliveryError.bronze_error_message_waiting_list_full, @parse.call('id:000000002D615774#1_1_79271095725 sub:000 dlvrd:000 submit date:1404291052 done date:1404291052 stat:REJECTD err:033')
    assert_equal SendLogDeliveryError.bronze_error_message_waiting_list_full, @parse.call('id:000000002D615774#1_1_79271095725 sub:000 dlvrd:000 submit date:1404291052 done date:1404291052 stat:UNDELIV err:033')
    assert_equal SendLogDeliveryError.bronze_error_message_waiting_list_full, @parse.call('id:000000002D615774#1_1_79271095725 sub:000 dlvrd:000 submit date:1404291052 done date:1404291052 stat:EXPIRED err:033')
  end

  test "should return 34 error code" do
    #samara
    assert_equal SendLogDeliveryError.bronze_error_system_failure, @parse.call('id:1406117711185e3901 sub:001 dlvrd:001 submit date:1407231615 done date:1407231815 stat:REJECTD err:034 text:')
    assert_equal SendLogDeliveryError.bronze_error_system_failure, @parse.call('id:1406117711185e3901 sub:001 dlvrd:001 submit date:1407231615 done date:1407231815 stat:UNDELIV err:034 text:')
    assert_equal SendLogDeliveryError.bronze_error_system_failure, @parse.call('id:1406117711185e3901 sub:001 dlvrd:001 submit date:1407231615 done date:1407231815 stat:EXPIRED err:034 text:')

    #penza
    assert_equal SendLogDeliveryError.bronze_error_system_failure, @parse.call('id:000000002D615028#1_1_79270907528 sub:000 dlvrd:000 submit date:1404291051 done date:1404291053 stat:REJECTD err:034')
    assert_equal SendLogDeliveryError.bronze_error_system_failure, @parse.call('id:000000002D615028#1_1_79270907528 sub:000 dlvrd:000 submit date:1404291051 done date:1404291053 stat:UNDELIV err:034')
    assert_equal SendLogDeliveryError.bronze_error_system_failure, @parse.call('id:000000002D615028#1_1_79270907528 sub:000 dlvrd:000 submit date:1404291051 done date:1404291053 stat:EXPIRED err:034')
  end

  test "should return 35 error code" do
    #samara - syntetic
    assert_equal SendLogDeliveryError.bronze_error_data_missing, @parse.call('id:1406117711185e3901 sub:001 dlvrd:001 submit date:1407231615 done date:1407231815 stat:REJECTD err:035 text:')
    assert_equal SendLogDeliveryError.bronze_error_data_missing, @parse.call('id:1406117711185e3901 sub:001 dlvrd:001 submit date:1407231615 done date:1407231815 stat:UNDELIV err:035 text:')
    assert_equal SendLogDeliveryError.bronze_error_data_missing, @parse.call('id:1406117711185e3901 sub:001 dlvrd:001 submit date:1407231615 done date:1407231815 stat:EXPIRED err:035 text:')

    #penza - syntetic
    assert_equal SendLogDeliveryError.bronze_error_data_missing, @parse.call('id:000000002D615028#1_1_79270907528 sub:000 dlvrd:000 submit date:1404291051 done date:1404291053 stat:REJECTD err:035')
    assert_equal SendLogDeliveryError.bronze_error_data_missing, @parse.call('id:000000002D615028#1_1_79270907528 sub:000 dlvrd:000 submit date:1404291051 done date:1404291053 stat:UNDELIV err:035')
    assert_equal SendLogDeliveryError.bronze_error_data_missing, @parse.call('id:000000002D615028#1_1_79270907528 sub:000 dlvrd:000 submit date:1404291051 done date:1404291053 stat:EXPIRED err:035')
  end

  test "should return 36 error code" do
    #samara
    assert_equal SendLogDeliveryError.bronze_error_unexpected_data_value, @parse.call('id:1406143448981c2902 sub:001 dlvrd:001 submit date:1407232324 done date:1407232324 stat:REJECTD err:036 text:')
    assert_equal SendLogDeliveryError.bronze_error_unexpected_data_value, @parse.call('id:1406143448981c2902 sub:001 dlvrd:001 submit date:1407232324 done date:1407232324 stat:UNDELIV err:036 text:')
    assert_equal SendLogDeliveryError.bronze_error_unexpected_data_value, @parse.call('id:1406143448981c2902 sub:001 dlvrd:001 submit date:1407232324 done date:1407232324 stat:EXPIRED err:036 text:')

    #penza - syntetic
    assert_equal SendLogDeliveryError.bronze_error_unexpected_data_value, @parse.call('id:000000002D615028#1_1_79270907528 sub:000 dlvrd:000 submit date:1404291051 done date:1404291053 stat:REJECTD err:036')
    assert_equal SendLogDeliveryError.bronze_error_unexpected_data_value, @parse.call('id:000000002D615028#1_1_79270907528 sub:000 dlvrd:000 submit date:1404291051 done date:1404291053 stat:UNDELIV err:036')
    assert_equal SendLogDeliveryError.bronze_error_unexpected_data_value, @parse.call('id:000000002D615028#1_1_79270907528 sub:000 dlvrd:000 submit date:1404291051 done date:1404291053 stat:EXPIRED err:036')
  end

  test "should return 51 error code" do
    #samara - syntetic
    assert_equal SendLogDeliveryError.bronze_error_resource_limitation, @parse.call('id:1406143448981c2902 sub:001 dlvrd:001 submit date:1407232324 done date:1407232324 stat:REJECTD err:051 text:')
    assert_equal SendLogDeliveryError.bronze_error_resource_limitation, @parse.call('id:1406143448981c2902 sub:001 dlvrd:001 submit date:1407232324 done date:1407232324 stat:UNDELIV err:051 text:')
    assert_equal SendLogDeliveryError.bronze_error_resource_limitation, @parse.call('id:1406143448981c2902 sub:001 dlvrd:001 submit date:1407232324 done date:1407232324 stat:EXPIRED err:051 text:')

    #penza - syntetic
    assert_equal SendLogDeliveryError.bronze_error_resource_limitation, @parse.call('id:000000002D615028#1_1_79270907528 sub:000 dlvrd:000 submit date:1404291051 done date:1404291053 stat:REJECTD err:051')
    assert_equal SendLogDeliveryError.bronze_error_resource_limitation, @parse.call('id:000000002D615028#1_1_79270907528 sub:000 dlvrd:000 submit date:1404291051 done date:1404291053 stat:UNDELIV err:051')
    assert_equal SendLogDeliveryError.bronze_error_resource_limitation, @parse.call('id:000000002D615028#1_1_79270907528 sub:000 dlvrd:000 submit date:1404291051 done date:1404291053 stat:EXPIRED err:051')
  end

  test "should return 54 error code" do
    #samara - syntetic
    assert_equal SendLogDeliveryError.bronze_error_position_method_failure, @parse.call('id:1406143448981c2902 sub:001 dlvrd:001 submit date:1407232324 done date:1407232324 stat:REJECTD err:054 text:')
    assert_equal SendLogDeliveryError.bronze_error_position_method_failure, @parse.call('id:1406143448981c2902 sub:001 dlvrd:001 submit date:1407232324 done date:1407232324 stat:UNDELIV err:054 text:')
    assert_equal SendLogDeliveryError.bronze_error_position_method_failure, @parse.call('id:1406143448981c2902 sub:001 dlvrd:001 submit date:1407232324 done date:1407232324 stat:EXPIRED err:054 text:')

    #penza - syntetic
    assert_equal SendLogDeliveryError.bronze_error_position_method_failure, @parse.call('id:000000002D615028#1_1_79270907528 sub:000 dlvrd:000 submit date:1404291051 done date:1404291053 stat:REJECTD err:054')
    assert_equal SendLogDeliveryError.bronze_error_position_method_failure, @parse.call('id:000000002D615028#1_1_79270907528 sub:000 dlvrd:000 submit date:1404291051 done date:1404291053 stat:UNDELIV err:054')
    assert_equal SendLogDeliveryError.bronze_error_position_method_failure, @parse.call('id:000000002D615028#1_1_79270907528 sub:000 dlvrd:000 submit date:1404291051 done date:1404291053 stat:EXPIRED err:054')
  end

  test "should return 71 error code" do
    #samara - syntetic
    assert_equal SendLogDeliveryError.bronze_error_unknown_alphabet, @parse.call('id:1406143448981c2902 sub:001 dlvrd:001 submit date:1407232324 done date:1407232324 stat:REJECTD err:071 text:')
    assert_equal SendLogDeliveryError.bronze_error_unknown_alphabet, @parse.call('id:1406143448981c2902 sub:001 dlvrd:001 submit date:1407232324 done date:1407232324 stat:UNDELIV err:071 text:')
    assert_equal SendLogDeliveryError.bronze_error_unknown_alphabet, @parse.call('id:1406143448981c2902 sub:001 dlvrd:001 submit date:1407232324 done date:1407232324 stat:EXPIRED err:071 text:')

    #penza - syntetic
    assert_equal SendLogDeliveryError.bronze_error_unknown_alphabet, @parse.call('id:000000002D615028#1_1_79270907528 sub:000 dlvrd:000 submit date:1404291051 done date:1404291053 stat:REJECTD err:071')
    assert_equal SendLogDeliveryError.bronze_error_unknown_alphabet, @parse.call('id:000000002D615028#1_1_79270907528 sub:000 dlvrd:000 submit date:1404291051 done date:1404291053 stat:UNDELIV err:071')
    assert_equal SendLogDeliveryError.bronze_error_unknown_alphabet, @parse.call('id:000000002D615028#1_1_79270907528 sub:000 dlvrd:000 submit date:1404291051 done date:1404291053 stat:EXPIRED err:071')
  end

  test "should return 72 error code" do
    #samara - syntetic
    assert_equal SendLogDeliveryError.bronze_error_ussd_busy, @parse.call('id:1406143448981c2902 sub:001 dlvrd:001 submit date:1407232324 done date:1407232324 stat:REJECTD err:072 text:')
    assert_equal SendLogDeliveryError.bronze_error_ussd_busy, @parse.call('id:1406143448981c2902 sub:001 dlvrd:001 submit date:1407232324 done date:1407232324 stat:UNDELIV err:072 text:')
    assert_equal SendLogDeliveryError.bronze_error_ussd_busy, @parse.call('id:1406143448981c2902 sub:001 dlvrd:001 submit date:1407232324 done date:1407232324 stat:EXPIRED err:072 text:')

    #penza - syntetic
    assert_equal SendLogDeliveryError.bronze_error_ussd_busy, @parse.call('id:000000002D615028#1_1_79270907528 sub:000 dlvrd:000 submit date:1404291051 done date:1404291053 stat:REJECTD err:072')
    assert_equal SendLogDeliveryError.bronze_error_ussd_busy, @parse.call('id:000000002D615028#1_1_79270907528 sub:000 dlvrd:000 submit date:1404291051 done date:1404291053 stat:UNDELIV err:072')
    assert_equal SendLogDeliveryError.bronze_error_ussd_busy, @parse.call('id:000000002D615028#1_1_79270907528 sub:000 dlvrd:000 submit date:1404291051 done date:1404291053 stat:EXPIRED err:072')
  end

  test "should return 201 error code" do
    #samara
    assert_equal SendLogDeliveryError.bronze_hlr_timeout, @parse.call('id:140610226985385602 sub:001 dlvrd:001 submit date:1407231157 done date:1407240031 stat:REJECTD err:201 text:')
    assert_equal SendLogDeliveryError.bronze_hlr_timeout, @parse.call('id:140610226985385602 sub:001 dlvrd:001 submit date:1407231157 done date:1407240031 stat:UNDELIV err:201 text:')
    assert_equal SendLogDeliveryError.bronze_hlr_timeout, @parse.call('id:140610226985385602 sub:001 dlvrd:001 submit date:1407231157 done date:1407240031 stat:EXPIRED err:201 text:')

    #penza - syntetic
    assert_equal SendLogDeliveryError.bronze_hlr_timeout, @parse.call('id:000000002D615028#1_1_79270907528 sub:000 dlvrd:000 submit date:1404291051 done date:1404291053 stat:REJECTD err:201')
    assert_equal SendLogDeliveryError.bronze_hlr_timeout, @parse.call('id:000000002D615028#1_1_79270907528 sub:000 dlvrd:000 submit date:1404291051 done date:1404291053 stat:UNDELIV err:201')
    assert_equal SendLogDeliveryError.bronze_hlr_timeout, @parse.call('id:000000002D615028#1_1_79270907528 sub:000 dlvrd:000 submit date:1404291051 done date:1404291053 stat:EXPIRED err:201')
  end

  test "should return 202 error code" do
    #samara
    assert_equal SendLogDeliveryError.bronze_vlr_timeout, @parse.call('id:1406121731828e0f02 sub:001 dlvrd:001 submit date:1407231722 done date:1407231733 stat:REJECTD err:202 text:')
    assert_equal SendLogDeliveryError.bronze_vlr_timeout, @parse.call('id:1406121731828e0f02 sub:001 dlvrd:001 submit date:1407231722 done date:1407231733 stat:UNDELIV err:202 text:')
    assert_equal SendLogDeliveryError.bronze_vlr_timeout, @parse.call('id:1406121731828e0f02 sub:001 dlvrd:001 submit date:1407231722 done date:1407231733 stat:EXPIRED err:202 text:')

    #penza - syntetic
    assert_equal SendLogDeliveryError.bronze_vlr_timeout, @parse.call('id:000000002D615028#1_1_79270907528 sub:000 dlvrd:000 submit date:1404291051 done date:1404291053 stat:REJECTD err:202')
    assert_equal SendLogDeliveryError.bronze_vlr_timeout, @parse.call('id:000000002D615028#1_1_79270907528 sub:000 dlvrd:000 submit date:1404291051 done date:1404291053 stat:UNDELIV err:202')
    assert_equal SendLogDeliveryError.bronze_vlr_timeout, @parse.call('id:000000002D615028#1_1_79270907528 sub:000 dlvrd:000 submit date:1404291051 done date:1404291053 stat:EXPIRED err:202')
  end

  test "should return 3840 error code" do
    #samara - syntetic
    assert_equal SendLogDeliveryError.bronze_no_server_link_route, @parse.call('NACK/3840')
    assert_equal SendLogDeliveryError.bronze_no_server_link_route, @parse.call('NACK/3840/Unknown/Reserved')

    #penza - syntetic
    assert_equal SendLogDeliveryError.bronze_no_server_link_route, @parse.call('NACK/3840')
    assert_equal SendLogDeliveryError.bronze_no_server_link_route, @parse.call('NACK/3840/Unknown/Reserved')
  end

  test "should return 3841 error code" do
    #samara
    assert_equal SendLogDeliveryError.bronze_spam_filter_action, @parse.call('NACK/3841/Unknown/Reserved')

    #penza - syntetic
    assert_equal SendLogDeliveryError.bronze_spam_filter_action, @parse.call('NACK/3841/Unknown/Reserved')
  end

  test "should return 3842 error code" do
    #samara - syntetic
    assert_equal SendLogDeliveryError.bronze_limit_exceeded, @parse.call('NACK/3842')

    #penza - syntetic
    assert_equal SendLogDeliveryError.bronze_limit_exceeded, @parse.call('NACK/3842')
  end

  test "should return unknown error when undelivered and err eq 0" do
    assert_equal SendLogDeliveryError.bronze_error_unknown, @parse.call("id:466326155780161686 sub:001 dlvrd:001 submit date:1411181027 done date:1411181028 stat:UNDELIV err:000 text:\u0005\u0000\u0003�\u0002\u0001\u00001\u0000-\u00003\u0000 \u00044\u00045\u0004:")
  end

end
