# -*- encoding : utf-8 -*-
require 'test_helper'

class TemplateBirthdayHelperTest < ActionView::TestCase
  def setup
    @valid_hours = [10, 11, 0, 12, 14]
    @invalid_hours = [-1, 'fdmkvfdvf', '', nil, '12637', 12344]
  end

  test "should validate for valid hours" do
    @valid_hours.each do |hour|
      assert_equal hour, valid_hour(hour)
    end
  end

  test "should return default_hour for invalid hours" do
    @invalid_hours.each do |hour|
      assert_equal Period::DEFAULT_HOUR, valid_hour(hour)
    end
  end
end
