# -*- encoding : utf-8 -*-
require 'test_helper'

class PhoneHelperTest < ActionView::TestCase

  def setup
    @@valid_phone_numbers = [
        '+7(937)990-38-35',
        '7937990-38-35',
        '89379903835',
        '8 937 990 38 35',
        '+7 937 990 38 35'
    ]

    @@invalid_phone_numbers = [
        '123',
        '',
        nil,
        '3782hfhfuvff8',
        '--vnfjkd--',
        '--45--',
        '----',
        '78462241023',
        '79379903835.0'
    ]
  end

  test "should check valid phone number" do
    @@valid_phone_numbers.each do |valid_phone_number|
      assert phone_number_valid?(valid_phone_number)
    end
  end

  test "should check invalid phone number" do
    @@invalid_phone_numbers.each do |invalid_phone_number|
      assert_equal false, phone_number_valid?(invalid_phone_number)
    end
  end

  test "should normalize valid phone number" do
    @@valid_phone_numbers.each do |valid_phone_number|
      assert_equal 79379903835, normalize_phone_number(valid_phone_number)
    end
  end

  test "should normalize invalid phone number" do
    @@invalid_phone_numbers.each do |invalid_phone_number|
      assert_nil normalize_phone_number(invalid_phone_number)
    end
  end
end
