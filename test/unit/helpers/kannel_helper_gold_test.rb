# -*- encoding : utf-8 -*-
require 'test_helper'

class KannelHelperGoldTest < ActionView::TestCase
  include KannelHelper

  def setup
    clear_db

    @parse = lambda { |ack| parse_error_code(ack, Channel.gold) }
  end

  #unkwnown error
  test "should return unknown error on intermediate ack" do
    assert_equal SendLogDeliveryError.gold_error_unknown, @parse.call('ACK/')
  end

  test "should return unknown error on intermediate nack" do
    assert_equal SendLogDeliveryError.gold_error_unknown, @parse.call('NACK/')
    assert_equal SendLogDeliveryError.gold_error_unknown, @parse.call('NACK/no SMSC')
  end

  test "should return unknow error on empty ack" do
    assert_equal SendLogDeliveryError.gold_error_unknown, @parse.call('')
  end

  test "should return unknow error on nil ack" do
    assert_equal SendLogDeliveryError.gold_error_unknown, @parse.call(nil)
  end

  test "should return unknow error on invalid ack" do
    assert_equal SendLogDeliveryError.gold_error_unknown, @parse.call('-1vfjdiovjig')
  end

  test "should return unknown error on rejected with unknown err" do
    assert_equal SendLogDeliveryError.gold_error_unknown, @parse.call('id:804907996 sub:001 dlvrd:001 submit date:1407232228 done date:1407232228 stat:REJECTD err:255 Text:Dogovor ?11540 popolnen na 1000.00r. vk.com/mknet')
  end

  test "should return unknown error on undelivr with unknown err" do
    assert_equal SendLogDeliveryError.gold_error_unknown, @parse.call('id:804907996 sub:001 dlvrd:001 submit date:1407232228 done date:1407232228 stat:UNDELIV err:255 Text:Dogovor ?11540 popolnen na 1000.00r. vk.com/mknet')
  end

  test "should return unknown error on no_error code" do
    assert_equal SendLogDeliveryError.gold_error_unknown, @parse.call('id:804907996 sub:001 dlvrd:001 submit date:1407232228 done date:1407232228 stat:REJECTD err:000 Text:Dogovor ?11540 popolnen na 1000.00r. vk.com/mknet')
    assert_equal SendLogDeliveryError.gold_error_unknown, @parse.call('id:804907996 sub:001 dlvrd:001 submit date:1407232228 done date:1407232228 stat:UNDELIV err:000 Text:Dogovor ?11540 popolnen na 1000.00r. vk.com/mknet')
  end

  #extended errors
  test "should return nack_11 on nack/11" do
    assert_equal SendLogDeliveryError.gold_error_nack_11, @parse.call('NACK/11/Invalid Destination Address')
  end

  test "should return retries expired on retried exceeded" do
    assert_equal SendLogDeliveryError.gold_retries_exceeded, @parse.call('NACK/Retries Exceeded')
  end

  test "should return expired" do
    assert_equal SendLogDeliveryError.gold_expired, @parse.call('id:804907996 sub:001 dlvrd:001 submit date:1407232228 done date:1407232228 stat:EXPIRED err:255 Text:Dogovor ?11540 popolnen na 1000.00r. vk.com/mknet')
    assert_equal SendLogDeliveryError.gold_expired, @parse.call('id:804907996 sub:001 dlvrd:001 submit date:1407232228 done date:1407232228 stat:EXPIRED err:012 Text:Dogovor ?11540 popolnen na 1000.00r. vk.com/mknet')
    assert_equal SendLogDeliveryError.gold_expired, @parse.call('id:804907996 sub:001 dlvrd:001 submit date:1407232228 done date:1407232228 stat:EXPIRED err:102 Text:Dogovor ?11540 popolnen na 1000.00r. vk.com/mknet')
    assert_equal SendLogDeliveryError.gold_expired, @parse.call('id:804907996 sub:001 dlvrd:001 submit date:1407232228 done date:1407232228 stat:EXPIRED err:000 Text:Dogovor ?11540 popolnen na 1000.00r. vk.com/mknet')
  end

  #no error
  test "should return 0 error code" do
    #samara
    assert_equal SendLogDeliveryError.gold_no_error, @parse.call('id:804918106 sub:001 dlvrd:001 submit date:1407240020 done date:1407240020 stat:DELIVRD err:000 Text:Po dogovoru ?13012 vzyat doveritelnyj platezh. vk.com/mkne')

    #penza
    assert_equal SendLogDeliveryError.gold_no_error, @parse.call('id:816294540 sub:001 dlvrd:001 submit date:1407311603 done date:1407311604 stat:DELIVRD err:000 Text:Aktsiya "Menyajsya" obmenyaj staruyu igrushku na 10% SKIDKU')
  end

  #known errors
  test "should return 1 error code for err:001" do
    #samara
    assert_equal SendLogDeliveryError.gold_error_unknown_or_no_delivery_report, @parse.call('id:804882705 sub:001 dlvrd:001 submit date:1407232027 done date:1407232127 stat:UNDELIV err:001 Text:89608264208 (Natalya), Cerdobskaya, 9, 6000 rub., Komnata')
    assert_equal SendLogDeliveryError.gold_error_unknown_or_no_delivery_report, @parse.call('id:804882705 sub:001 dlvrd:001 submit date:1407232027 done date:1407232127 stat:EXPIRED err:001 Text:89608264208 (Natalya), Cerdobskaya, 9, 6000 rub., Komnata')
    assert_equal SendLogDeliveryError.gold_error_unknown_or_no_delivery_report, @parse.call('id:804882705 sub:001 dlvrd:001 submit date:1407232027 done date:1407232127 stat:REJECTD err:001 Text:89608264208 (Natalya), Cerdobskaya, 9, 6000 rub., Komnata')

    #penza
    assert_equal SendLogDeliveryError.gold_error_unknown_or_no_delivery_report, @parse.call('id:814488913 sub:001 dlvrd:001 submit date:1407301708 done date:1407301809 stat:UNDELIV err:001 Text:Kafe-bar "7nebo" informiruet Vas o smene nomera dostavki: 7')
    assert_equal SendLogDeliveryError.gold_error_unknown_or_no_delivery_report, @parse.call('id:724609866 sub:001 dlvrd:001 submit date:1406111756 done date:1406111827 stat:EXPIRED err:001 Text:PIVNOJ BUM na prazdniki!!!Zakazhi SHASHLYK!!!V PODAROK-PIVO')
    assert_equal SendLogDeliveryError.gold_error_unknown_or_no_delivery_report, @parse.call('id:724609866 sub:001 dlvrd:001 submit date:1406111756 done date:1406111827 stat:REJECTD err:001 Text:PIVNOJ BUM na prazdniki!!!Zakazhi SHASHLYK!!!V PODAROK-PIVO')
  end

  test "should return 2 error code for err:002" do
    #samara
    assert_equal SendLogDeliveryError.gold_error_phone_out_of_network, @parse.call('id:804910217 sub:001 dlvrd:001 submit date:1407232248 done date:1407232349 stat:UNDELIV err:002 Text:Po dogovoru ?4170 vzyat doveritelnyj platezh. vk.com/mknet')
    assert_equal SendLogDeliveryError.gold_error_phone_out_of_network, @parse.call('id:804910217 sub:001 dlvrd:001 submit date:1407232248 done date:1407232349 stat:EXPIRED err:002 Text:Po dogovoru ?4170 vzyat doveritelnyj platezh. vk.com/mknet')
    assert_equal SendLogDeliveryError.gold_error_phone_out_of_network, @parse.call('id:804910217 sub:001 dlvrd:001 submit date:1407232248 done date:1407232349 stat:REJECTD err:002 Text:Po dogovoru ?4170 vzyat doveritelnyj platezh. vk.com/mknet')

    #penza
    assert_equal SendLogDeliveryError.gold_error_phone_out_of_network, @parse.call('id:814489032 sub:001 dlvrd:001 submit date:1407301708 done date:1407301854 stat:UNDELIV err:002 Text:Kafe-bar "7nebo" informiruet Vas o smene nomera dostavki: 7')
    assert_equal SendLogDeliveryError.gold_error_phone_out_of_network, @parse.call('id:702920824 sub:001 dlvrd:001 submit date:1405301056 done date:1405301157 stat:EXPIRED err:002 Text:V eti vyhodnye vsem yunym pokupatelyam PODARKI ot CROCS ! K')
    assert_equal SendLogDeliveryError.gold_error_phone_out_of_network, @parse.call('id:702920824 sub:001 dlvrd:001 submit date:1405301056 done date:1405301157 stat:REJECTD err:002 Text:V eti vyhodnye vsem yunym pokupatelyam PODARKI ot CROCS ! K')
  end

  test "should return 3 error code for err:003" do
    #samara
    assert_equal SendLogDeliveryError.gold_error_phone_is_blocked_or_roaming, @parse.call('id:804908820 sub:001 dlvrd:001 submit date:1407232235 done date:1407232235 stat:UNDELIV err:003 Text:Dogovor ?6017 popolnen na 96.02r. vk.com/mknet')
    assert_equal SendLogDeliveryError.gold_error_phone_is_blocked_or_roaming, @parse.call('id:804908820 sub:001 dlvrd:001 submit date:1407232235 done date:1407232235 stat:EXPIRED err:003 Text:Dogovor ?6017 popolnen na 96.02r. vk.com/mknet')
    assert_equal SendLogDeliveryError.gold_error_phone_is_blocked_or_roaming, @parse.call('id:804908820 sub:001 dlvrd:001 submit date:1407232235 done date:1407232235 stat:REJECTD err:003 Text:Dogovor ?6017 popolnen na 96.02r. vk.com/mknet')

    #penza
    assert_equal SendLogDeliveryError.gold_error_phone_is_blocked_or_roaming, @parse.call('id:814488709 sub:001 dlvrd:001 submit date:1407301708 done date:1407301853 stat:UNDELIV err:003 Text:Kafe-bar "7nebo" informiruet Vas o smene nomera dostavki: 7')
    assert_equal SendLogDeliveryError.gold_error_phone_is_blocked_or_roaming, @parse.call('id:698609099 sub:001 dlvrd:001 submit date:1405281650 done date:1405281650 stat:EXPIRED err:003 Text:Progolodalis?Zakazhite VKUSNYJ SHASHLYK!!!Ot 1000r-PODARKI')
    assert_equal SendLogDeliveryError.gold_error_phone_is_blocked_or_roaming, @parse.call('id:698609099 sub:001 dlvrd:001 submit date:1405281650 done date:1405281650 stat:REJECTD err:003 Text:Progolodalis?Zakazhite VKUSNYJ SHASHLYK!!!Ot 1000r-PODARKI')
  end

  test "should return 4 error code for err:004" do
    #samara
    assert_equal SendLogDeliveryError.gold_error_gsm_transport_level_is_corrupted, @parse.call('id:804883315 sub:001 dlvrd:001 submit date:1407232028 done date:1407232328 stat:UNDELIV err:004 Text:89270020474 (Aleksandra), Silina, 14, 5000 rub., Komnata')
    assert_equal SendLogDeliveryError.gold_error_gsm_transport_level_is_corrupted, @parse.call('id:804883315 sub:001 dlvrd:001 submit date:1407232028 done date:1407232328 stat:EXPIRED err:004 Text:89270020474 (Aleksandra), Silina, 14, 5000 rub., Komnata')
    assert_equal SendLogDeliveryError.gold_error_gsm_transport_level_is_corrupted, @parse.call('id:804883315 sub:001 dlvrd:001 submit date:1407232028 done date:1407232328 stat:REJECTD err:004 Text:89270020474 (Aleksandra), Silina, 14, 5000 rub., Komnata')

    #penza
    assert_equal SendLogDeliveryError.gold_error_gsm_transport_level_is_corrupted, @parse.call('id:812979936 sub:001 dlvrd:001 submit date:1407300925 done date:1407301025 stat:UNDELIV err:004 Text:Zhara!!!skidki tayut!!! -50% na vse leto 2014 goda!TTs PASS')
    assert_equal SendLogDeliveryError.gold_error_gsm_transport_level_is_corrupted, @parse.call('id:698613012 sub:001 dlvrd:001 submit date:1405281700 done date:1405281700 stat:EXPIRED err:004 Text:Progolodalis?Zakazhite VKUSNYJ SHASHLYK!!!Ot 1000r-PODARKI')
    assert_equal SendLogDeliveryError.gold_error_gsm_transport_level_is_corrupted, @parse.call('id:698613012 sub:001 dlvrd:001 submit date:1405281700 done date:1405281700 stat:REJECTD err:004 Text:Progolodalis?Zakazhite VKUSNYJ SHASHLYK!!!Ot 1000r-PODARKI')
  end

  test "should return 5 error code for err:005" do
    #samara
    assert_equal SendLogDeliveryError.gold_error_phone_out_of_memory, @parse.call('id:804885185 sub:001 dlvrd:001 submit date:1407232034 done date:1407232135 stat:UNDELIV err:005 Text:KUPIM VASHE AVTO ZA NALICHNYE. Avtosalon "KAR-ON-LAJN". t.8')
    assert_equal SendLogDeliveryError.gold_error_phone_out_of_memory, @parse.call('id:804885185 sub:001 dlvrd:001 submit date:1407232034 done date:1407232135 stat:EXPIRED err:005 Text:KUPIM VASHE AVTO ZA NALICHNYE. Avtosalon "KAR-ON-LAJN". t.8')
    assert_equal SendLogDeliveryError.gold_error_phone_out_of_memory, @parse.call('id:804885185 sub:001 dlvrd:001 submit date:1407232034 done date:1407232135 stat:REJECTD err:005 Text:KUPIM VASHE AVTO ZA NALICHNYE. Avtosalon "KAR-ON-LAJN". t.8')

    #penza
    assert_equal SendLogDeliveryError.gold_error_phone_out_of_memory, @parse.call('id:813021706 sub:001 dlvrd:001 submit date:1407300931 done date:1407301114 stat:UNDELIV err:005 Text:Zhara!!!skidki tayut!!! -50% na vse leto 2014 goda!TTs Pere')
    assert_equal SendLogDeliveryError.gold_error_phone_out_of_memory, @parse.call('id:813021706 sub:001 dlvrd:001 submit date:1407300931 done date:1407301114 stat:EXPIRED err:005 Text:Zhara!!!skidki tayut!!! -50% na vse leto 2014 goda!TTs Pere')
    assert_equal SendLogDeliveryError.gold_error_phone_out_of_memory, @parse.call('id:813021706 sub:001 dlvrd:001 submit date:1407300931 done date:1407301114 stat:REJECTD err:005 Text:Zhara!!!skidki tayut!!! -50% na vse leto 2014 goda!TTs Pere')
  end

  test "should return 6 error code for err:006" do
    #samara
    assert_equal SendLogDeliveryError.gold_error_incompatible_terminal, @parse.call('id:804576690 sub:001 dlvrd:001 submit date:1407231445 done date:1407231629 stat:UNDELIV err:006 Text:KUPIM VASHE AVTO ZA NALICHNYE. Avtosalon "KAR-ON-LAJN". t.8')
    assert_equal SendLogDeliveryError.gold_error_incompatible_terminal, @parse.call('id:804576690 sub:001 dlvrd:001 submit date:1407231445 done date:1407231629 stat:EXPIRED err:006 Text:KUPIM VASHE AVTO ZA NALICHNYE. Avtosalon "KAR-ON-LAJN". t.8')
    assert_equal SendLogDeliveryError.gold_error_incompatible_terminal, @parse.call('id:804576690 sub:001 dlvrd:001 submit date:1407231445 done date:1407231629 stat:REJECTD err:006 Text:KUPIM VASHE AVTO ZA NALICHNYE. Avtosalon "KAR-ON-LAJN". t.8')

    #penza
    assert_equal SendLogDeliveryError.gold_error_incompatible_terminal, @parse.call('id:814487945 sub:001 dlvrd:001 submit date:1407301708 done date:1407301853 stat:UNDELIV err:006 Text:Kafe-bar "7nebo" informiruet Vas o smene nomera dostavki: 7')
    assert_equal SendLogDeliveryError.gold_error_incompatible_terminal, @parse.call('id:698606862 sub:001 dlvrd:001 submit date:1405281645 done date:1405281645 stat:EXPIRED err:006 Text:Progolodalis?Zakazhite VKUSNYJ SHASHLYK!!!Ot 1000r-PODARKI')
    assert_equal SendLogDeliveryError.gold_error_incompatible_terminal, @parse.call('id:698606862 sub:001 dlvrd:001 submit date:1405281645 done date:1405281645 stat:REJECTD err:006 Text:Progolodalis?Zakazhite VKUSNYJ SHASHLYK!!!Ot 1000r-PODARKI')
  end

  test "should return 7 error code for err:007" do
    #samara - syntetic
    assert_equal SendLogDeliveryError.gold_error_gsm_switch_is_not_available, @parse.call('id:804907996 sub:001 dlvrd:001 submit date:1407232228 done date:1407232228 stat:UNDELIV err:007 Text:Dogovor ?11540 popolnen na 1000.00r. vk.com/mknet')
    assert_equal SendLogDeliveryError.gold_error_gsm_switch_is_not_available, @parse.call('id:804907996 sub:001 dlvrd:001 submit date:1407232228 done date:1407232228 stat:EXPIRED err:007 Text:Dogovor ?11540 popolnen na 1000.00r. vk.com/mknet')
    assert_equal SendLogDeliveryError.gold_error_gsm_switch_is_not_available, @parse.call('id:804907996 sub:001 dlvrd:001 submit date:1407232228 done date:1407232228 stat:REJECTD err:007 Text:Dogovor ?11540 popolnen na 1000.00r. vk.com/mknet')

    #penza
    assert_equal SendLogDeliveryError.gold_error_gsm_switch_is_not_available, @parse.call('id:812187295 sub:001 dlvrd:001 submit date:1407291415 done date:1407291421 stat:UNDELIV err:007 Text:Odezhda iz Turtsii Novinki skidki do 20% TTs Forpost 0etazh')
    assert_equal SendLogDeliveryError.gold_error_gsm_switch_is_not_available, @parse.call('id:812187295 sub:001 dlvrd:001 submit date:1407291415 done date:1407291421 stat:EXPIRED err:007 Text:Odezhda iz Turtsii Novinki skidki do 20% TTs Forpost 0etazh')
    assert_equal SendLogDeliveryError.gold_error_gsm_switch_is_not_available, @parse.call('id:812187295 sub:001 dlvrd:001 submit date:1407291415 done date:1407291421 stat:REJECTD err:007 Text:Odezhda iz Turtsii Novinki skidki do 20% TTs Forpost 0etazh')
  end

  test "should return 8 error code for err:008" do
    #samara
    assert_equal SendLogDeliveryError.gold_error_phone_number_is_not_valid, @parse.call('id:804907996 sub:001 dlvrd:001 submit date:1407232228 done date:1407232228 stat:UNDELIV err:008 Text:Dogovor ?11540 popolnen na 1000.00r. vk.com/mknet')
    assert_equal SendLogDeliveryError.gold_error_phone_number_is_not_valid, @parse.call('id:804907996 sub:001 dlvrd:001 submit date:1407232228 done date:1407232228 stat:EXPIRED err:008 Text:Dogovor ?11540 popolnen na 1000.00r. vk.com/mknet')
    assert_equal SendLogDeliveryError.gold_error_phone_number_is_not_valid, @parse.call('id:804907996 sub:001 dlvrd:001 submit date:1407232228 done date:1407232228 stat:REJECTD err:008 Text:Dogovor ?11540 popolnen na 1000.00r. vk.com/mknet')

    #penza
    assert_equal SendLogDeliveryError.gold_error_phone_number_is_not_valid, @parse.call('id:814488689 sub:001 dlvrd:001 submit date:1407301708 done date:1407301852 stat:UNDELIV err:008 Text:Kafe-bar "7nebo" informiruet Vas o smene nomera dostavki: 7')
    assert_equal SendLogDeliveryError.gold_error_phone_number_is_not_valid, @parse.call('id:814488689 sub:001 dlvrd:001 submit date:1407301708 done date:1407301852 stat:EXPIRED err:008 Text:Kafe-bar "7nebo" informiruet Vas o smene nomera dostavki: 7')
    assert_equal SendLogDeliveryError.gold_error_phone_number_is_not_valid, @parse.call('id:814488689 sub:001 dlvrd:001 submit date:1407301708 done date:1407301852 stat:REJECTD err:008 Text:Kafe-bar "7nebo" informiruet Vas o smene nomera dostavki: 7')
  end

  test "should return 9 error code for err:009" do
    #samara - syntetic
    assert_equal SendLogDeliveryError.gold_error_message_was_rejected_by_dublicating, @parse.call('id:804907996 sub:001 dlvrd:001 submit date:1407232228 done date:1407232228 stat:UNDELIV err:009 Text:Dogovor ?11540 popolnen na 1000.00r. vk.com/mknet')
    assert_equal SendLogDeliveryError.gold_error_message_was_rejected_by_dublicating, @parse.call('id:804907996 sub:001 dlvrd:001 submit date:1407232228 done date:1407232228 stat:EXPIRED err:009 Text:Dogovor ?11540 popolnen na 1000.00r. vk.com/mknet')
    assert_equal SendLogDeliveryError.gold_error_message_was_rejected_by_dublicating, @parse.call('id:804907996 sub:001 dlvrd:001 submit date:1407232228 done date:1407232228 stat:REJECTD err:009 Text:Dogovor ?11540 popolnen na 1000.00r. vk.com/mknet')

    #penza - syntetic
    assert_equal SendLogDeliveryError.gold_error_message_was_rejected_by_dublicating, @parse.call('id:814488913 sub:000 dlvrd:000 submit date:1404291041 done date:1404291041 stat:UNDELIV err:009')
    assert_equal SendLogDeliveryError.gold_error_message_was_rejected_by_dublicating, @parse.call('id:814488913 sub:000 dlvrd:000 submit date:1404291041 done date:1404291041 stat:EXPIRED err:009')
    assert_equal SendLogDeliveryError.gold_error_message_was_rejected_by_dublicating, @parse.call('id:814488913 sub:000 dlvrd:000 submit date:1404291041 done date:1404291041 stat:REJECTD err:009')
  end

  test "should return 10 error code for err:010" do
    #samara - syntetic
    assert_equal SendLogDeliveryError.gold_error_message_was_rejected_by_spam_filter, @parse.call('id:804907996 sub:001 dlvrd:001 submit date:1407232228 done date:1407232228 stat:UNDELIV err:010 Text:Dogovor ?11540 popolnen na 1000.00r. vk.com/mknet')
    assert_equal SendLogDeliveryError.gold_error_message_was_rejected_by_spam_filter, @parse.call('id:804907996 sub:001 dlvrd:001 submit date:1407232228 done date:1407232228 stat:EXPIRED err:010 Text:Dogovor ?11540 popolnen na 1000.00r. vk.com/mknet')
    assert_equal SendLogDeliveryError.gold_error_message_was_rejected_by_spam_filter, @parse.call('id:804907996 sub:001 dlvrd:001 submit date:1407232228 done date:1407232228 stat:REJECTD err:010 Text:Dogovor ?11540 popolnen na 1000.00r. vk.com/mknet')

    #penza - syntetic
    assert_equal SendLogDeliveryError.gold_error_message_was_rejected_by_spam_filter, @parse.call('id:814488913 sub:000 dlvrd:000 submit date:1404291041 done date:1404291041 stat:UNDELIV err:010')
    assert_equal SendLogDeliveryError.gold_error_message_was_rejected_by_spam_filter, @parse.call('id:814488913 sub:000 dlvrd:000 submit date:1404291041 done date:1404291041 stat:EXPIRED err:010')
    assert_equal SendLogDeliveryError.gold_error_message_was_rejected_by_spam_filter, @parse.call('id:814488913 sub:000 dlvrd:000 submit date:1404291041 done date:1404291041 stat:REJECTD err:010')
  end

  test "should return 11 error code for err:011" do
    #samara
    assert_equal SendLogDeliveryError.gold_error_gsm_routing, @parse.call('id:804907996 sub:001 dlvrd:001 submit date:1407232228 done date:1407232228 stat:UNDELIV err:011 Text:Dogovor ?11540 popolnen na 1000.00r. vk.com/mknet')
    assert_equal SendLogDeliveryError.gold_error_gsm_routing, @parse.call('id:804907996 sub:001 dlvrd:001 submit date:1407232228 done date:1407232228 stat:EXPIRED err:011 Text:Dogovor ?11540 popolnen na 1000.00r. vk.com/mknet')
    assert_equal SendLogDeliveryError.gold_error_gsm_routing, @parse.call('id:804907996 sub:001 dlvrd:001 submit date:1407232228 done date:1407232228 stat:REJECTD err:011 Text:Dogovor ?11540 popolnen na 1000.00r. vk.com/mknet')

    #penza - syntetic
    assert_equal SendLogDeliveryError.gold_error_gsm_routing, @parse.call('id:812187295 sub:001 dlvrd:001 submit date:1407291415 done date:1407291421 stat:UNDELIV err:011 Text:Odezhda iz Turtsii Novinki skidki do 20% TTs Forpost 0etazh')
    assert_equal SendLogDeliveryError.gold_error_gsm_routing, @parse.call('id:812187295 sub:001 dlvrd:001 submit date:1407291415 done date:1407291421 stat:EXPIRED err:011 Text:Odezhda iz Turtsii Novinki skidki do 20% TTs Forpost 0etazh')
    assert_equal SendLogDeliveryError.gold_error_gsm_routing, @parse.call('id:812187295 sub:001 dlvrd:001 submit date:1407291415 done date:1407291421 stat:REJECTD err:011 Text:Odezhda iz Turtsii Novinki skidki do 20% TTs Forpost 0etazh')
  end

  test "should return 3840 error code" do
    assert_equal SendLogDeliveryError.gold_no_server_link_route, @parse.call('NACK/3840/Unknown/Reserved')
  end

  test "should return 3841 error code" do
    assert_equal SendLogDeliveryError.gold_spam_filter_action, @parse.call('NACK/3841/Unknown/Reserved')
  end

  test "should return 3842 error code" do
    assert_equal SendLogDeliveryError.gold_limit_exceeded, @parse.call('NACK/3842/Unknown/Reserved')
  end

  test "should return b error code for err:00b" do
    assert_equal SendLogDeliveryError.gold_error_invalid_destination_number, @parse.call('id:465111362935193601 sub:001 dlvrd:001 submit date:1411112119 done date:1411112119 stat:UNDELIV err:00b text:.t.e.s.t. .w.i.t.h.')
    assert_equal SendLogDeliveryError.gold_error_invalid_destination_number, @parse.call('id:465111362935193601 sub:001 dlvrd:001 submit date:1411112119 done date:1411112119 stat:EXPIRED err:00b text:.t.e.s.t. .w.i.t.h.')
    assert_equal SendLogDeliveryError.gold_error_invalid_destination_number, @parse.call('id:465111362935193601 sub:001 dlvrd:001 submit date:1411112119 done date:1411112119 stat:REJECTD err:00b text:.t.e.s.t. .w.i.t.h.')

    assert_equal SendLogDeliveryError.gold_error_invalid_destination_number, @parse.call('id:465111362935193601 sub:001 dlvrd:001 submit date:1411112119 done date:1411112119 stat:UNDELIV err:00B text:.t.e.s.t. .w.i.t.h.')
    assert_equal SendLogDeliveryError.gold_error_invalid_destination_number, @parse.call('id:465111362935193601 sub:001 dlvrd:001 submit date:1411112119 done date:1411112119 stat:EXPIRED err:00B text:.t.e.s.t. .w.i.t.h.')
    assert_equal SendLogDeliveryError.gold_error_invalid_destination_number, @parse.call('id:465111362935193601 sub:001 dlvrd:001 submit date:1411112119 done date:1411112119 stat:REJECTD err:00B text:.t.e.s.t. .w.i.t.h.')
  end

  test "should return unknown error when undelivered and err eq 0" do
    assert_equal SendLogDeliveryError.gold_error_unknown, @parse.call("id:466326155780161686 sub:001 dlvrd:001 submit date:1411181027 done date:1411181028 stat:UNDELIV err:000 text:\u0005\u0000\u0003�\u0002\u0001\u00001\u0000-\u00003\u0000 \u00044\u00045\u0004:")
  end

end
