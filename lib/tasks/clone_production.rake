#remote
db = "youct_production"
db_user = 'youct'
db_password = 'YxV6QhKj'
db_host = 'localhost'
db_port = 5432
pg_dump = "/usr/bin/pg_dump"
archive_name = "%s_%s.backup" % [db, Time.now.strftime('%Y-%m-%d')]
archive_path = "/srv/www/%s" % [archive_name]
bzip = "/bin/bzip2"

#ssh
host = 'sms-info.smarts.ru'
user = 'sm00th'
port = 222
scp = '/usr/bin/scp'

#local
local_db = 'youct_dev'
local_host = 'localhost'

namespace :db do
  desc 'Clone production database into development database'
  task :clone_production => :environment do
    Rye::Cmd.add_command :dump_remote_db, "#{pg_dump} -Fc --host=#{db_host} --port=#{db_port} --compress=9 --encoding=utf-8 --username=#{db_user} -f #{archive_path} #{db}"
    Rye::Cmd.add_command :zip_remote_dump, "#{bzip} -9 #{archive_path}"
    Rye::Cmd.add_command :remove_remote_dump, "rm -f #{archive_path}.bz2"

    rbox = Rye::Box.new(host, :user => user, :password_prompt => false, :port => port)
    rbox.remove_remote_dump
    rbox.dump_remote_db
    rbox.zip_remote_dump

    #copy dump
    system("scp -P #{port} #{host}:#{archive_path}.bz2 ./#{archive_name}.bz2")

    #remove remote dump
    rbox.remove_remote_dump

    #unzip dump
    system("bunzip2 #{archive_name}.bz2")

    #recreate dev database
    system("pg_restore -c -F c -U postgres -d #{local_db} -h #{local_host} #{archive_name}")
    system("rm -f #{archive_name}")
  end
end