namespace :sneakers do

  desc "Run sneakers with spawned workers"
  task :spawn => :environment do
    require 'sneakers/spawner'
    Sneakers::Spawner.spawn
  end

end
