# -*- encoding : utf-8 -*-
class NightDeliveryController < AuthController

  before_filter :find_permission!, :only => :update

  def update
    current_user.permit_night_delivery = @permission
    current_user.save!

    if @permission
      flash[:notice] = I18n.t('permit_night_delivery.deactivated')
    else
      flash[:notice] = I18n.t('permit_night_delivery.activated')
    end

    redirect_to show_night_delivery_path
  end

  private
  def sanitize_params
    params.permit(:permission)
  end

  def find_permission!
    permission = sanitize_params[:permission]

    if ['activate', 'deactivate'].include? permission
      @permission = false if permission == 'activate'
      @permission = true if permission == 'deactivate'
    else
      flash[:alert] = I18n.t('permit_night_delivery.failure.invalid') % permission
      redirect_to show_night_delivery_path
    end

  end
end
