# -*- encoding : utf-8 -*-
class ReportByDateController < AuthController
  before_filter :find_start_date!, :find_end_date!, :check_contact_email!, :only => :generate

  def show
    flash[:notice] = I18n.t('report_by_date.failure.contact_email_incorrect') if not current_user.has_contact_email?
  end

  def generate
    Report::Manager.delay.generate_by_date(current_user, @start_date, @end_date)
    flash[:notice] = I18n.t('report_by_date.generating') % [@start_date.strftime('%Y-%m-%d'), @end_date.strftime('%Y-%m-%d'), current_user.contact_email]
    redirect_to show_report_by_date_path
  end

  private
  def to_date(date)
    if date.present?
      begin
        _date = date.to_date
      rescue
      else
        return _date
      end
    end

    nil
  end

  def sanitize_params
    params.permit(:start_date, :end_date)
  end

  def find_start_date!
    @start_date = to_date(sanitize_params[:start_date])
    if @start_date.nil?
      flash[:alert] = I18n.t('report_by_date.failure.start_date_invalid')
      redirect_to show_report_by_date_path
    end
  end

  def find_end_date!
    @end_date = to_date(sanitize_params[:end_date])
    if @end_date.nil?
      flash[:alert] = I18n.t('report_by_date.failure.end_date_invalid')
      redirect_to show_report_by_date_path
    end
  end

  def check_contact_email!
    if not current_user.has_contact_email?
      flash[:alert] = I18n.t('report_by_date.failure.contact_email_incorrect')
      redirect_to show_report_by_date_path
    end
  end

end
