# -*- encoding : utf-8 -*-
class TemplateController < AuthController
  before_filter :find_template!, :only => [:show, :update, :destroy]
  before_filter :check_name!, :check_text!, :only => [:create, :update]

  def create
    Template.create!(:name => @name, :text => @text, :user => current_user)

    flash[:notice] = I18n.t('template.success.created')
    redirect_to templates_path
  end

  def update
    @template.name = @name
    @template.text = @text

    @template.save!

    flash[:notice] = I18n.t('template.success.updated')
    redirect_to templates_path
  end

  def destroy
    @template.delete

    flash[:notice] = I18n.t('template.success.deleted')
    redirect_to templates_path
  end

  private
  def find_template!
    @template = Template.find_by(id: sanitize_params[:id], user_id: current_user.id)

    if not @template
      flash[:alert] = I18n.t('template.not_exist') % sanitize_params[:id]
      redirect_to templates_path
    end
  end

  def sanitize_params
    params.permit(:id, :template => [:text, :name])
  end

  def text
    sanitize_params[:template][:text]
  end

  def name
    sanitize_params[:template][:name]
  end

  def check_name!
    if name.blank?
      flash[:alert] = I18n.t('template.failure.name_is_blank')
      redirect_to templates_path
    else
      @name = name
    end
  end

  def check_text!
    if text.blank?
      flash[:alert] = I18n.t('template.failure.text_is_blank')
      redirect_to templates_path
    else
      @text = text
    end
  end

end
