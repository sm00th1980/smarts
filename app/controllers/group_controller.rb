# -*- encoding : utf-8 -*-
class GroupController < AuthController
  include GroupHelper

  PER_PAGE = 15

  before_filter :find_group!, :only => [:upload, :show_upload, :show, :destroy, :birthday_congratulation]
  before_filter :sorting_conf, :only => [:show]

  def show
    if params[:page].blank?
      params[:page] = 1
    end
    @clients = @group.
        clients
      .order("#{@column} #{@order}")
      .paginate(:page => params[:page], :per_page => PER_PAGE)
    unless params[:search].blank?
      @clients = @clients.search params[:search]
    end

  end

  def birthday_congratulation
    @group.delay.set_users_birthday_congratulation_to(to_boolean(sanitize_params[:flag]))
    flash[:notice] = to_boolean(sanitize_params[:flag]) ? I18n.t('group.all_clients_set_to_true_birthday_congratulation') : I18n.t('group.all_clients_set_to_false_birthday_congratulation')

    redirect_to :back
  end

  def destroy
    @group.destroy
    redirect_to groups_path
  end

  def update
    group = Group.find_by_id_and_user_id(sanitize_params[:id], current_user)

    if group.blank?
      flash[:alert] = I18n.t('group.not_exist') % sanitize_params[:id]
      redirect_to groups_path
    else
      group.name = sanitize_params[:group][:name]
      group.save!

      redirect_to show_group_path(group)
    end
  end

  def create
    new_group = Group.create do |group|
      group.name = params[:group][:name]
      group.user = current_user
    end

    redirect_to groups_path
  end

  def upload
    #сохраняем файл на диске
    save_file

    GroupParser.delay(:queue => 'upload').perform(filename, @group.id) #загружаем файл асинхронно


    if supported_formats.include? file_format(filename)
      flash[:notice] = I18n.t('group.upload_started')
    else
      flash[:alert] = I18n.t('group.unknown_file_format') % supported_formats.join(',')
    end

    redirect_to groups_path
  end

  private
  def sorting_conf
    columns = %w(phone fio birthday_congratulation birthday description)
    @column = columns.include?(params[:column]) ? params[:column] : columns[0]
    @order = (params[:order] == 'asc') ? 'asc' : 'desc'
  end

  def save_file
    GroupParser.create_dir

    filename = Rails.root.join('public', 'uploads', sanitize_params[:clients_file].original_filename)
    File.open(filename, 'wb') do |file|
      file.write(sanitize_params[:clients_file].read)
    end
  end

  def sanitize_params
    params.permit(:clients_file, :id, :page, :encoding, :flag, :group => [:name])
  end

  def find_group!
    @group = Group.find_by_id_and_user_id(sanitize_params[:id], current_user.id)
    if @group.blank?
      flash[:alert] = I18n.t('group.not_exist') % sanitize_params[:id]
      redirect_to groups_path
    end
  end

  def to_boolean(str)
    str.downcase == 'true'
  end

  def filename
    sanitize_params[:clients_file].original_filename
  end

end
