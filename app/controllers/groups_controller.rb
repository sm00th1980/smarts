# -*- encoding : utf-8 -*-
class GroupsController < AuthController
  PER_PAGE = 15

  def index
    @groups = current_user.groups.order('created_at desc').paginate(page: sanitize_params[:page], :per_page => PER_PAGE)
  end

  private
  def sanitize_params
    params.permit(:page)
  end

end
