# -*- encoding : utf-8 -*-
class NonAuthController < ActionController::Base
  layout 'application'

  protect_from_forgery
end
