# -*- encoding : utf-8 -*-
class AuthController < ApplicationController
  before_filter :check_user!

  include ApplicationHelper

  private
  def check_user!
    if current_user.nil? or not current_user.status.active?
      sign_out current_user if current_user.present?
      flash[:alert] = I18n.t('devise.failure.user.invalid')
      redirect_to new_user_session_path
    end
  end
end
