# -*- encoding : utf-8 -*-
class SmsController < AuthController
  include PhoneHelper

  before_filter :check_empty_params, :only => :send_sms
  before_filter :check_message, :only => :send_sms
  before_filter :check_sender_name_value, :only => :send_sms
  before_filter :receivers, :only => :send_sms
  before_filter :message, :only => :send_sms

  def send_sms
    SMSService.send_sms(current_user, message, receivers, sanitize_params[:sender_name], fake, LogType.manual)
    flash[:notice] = I18n.t('sms.started')

    redirect_to sms_path
  end

  private
  def fake
    return true if current_user.operator? and sanitize_params[:fake] == '1'

    false
  end

  def extract_group_ids(ids)
    _group_ids = []

    if ids
      ids.uniq.each do |group_id|
        if group_id.present?
          type, id = group_id.split('=')

          if type == 'group_id'
            _group_ids << Integer(id) rescue nil
          end
        end
      end
    end

    _group_ids.compact.uniq
  end

  def extract_phones
    sanitize_params[:phone_numbers].split(',').map { |phone| normalize_phone_number(phone) }.compact.uniq if sanitize_params[:phone_numbers]
  end

  def message
    if sanitize_params[:sms_content].present?
      return sanitize_params[:sms_content]
    end

    if Template.exists?(sanitize_params[:template])
      return Template.find(sanitize_params[:template]).text
    end
  end

  def receivers
    _group_ids = extract_group_ids(sanitize_params[:ids])
    _phones = extract_phones

    if _phones.present? and _phones.size > SMSService.max_phones
      flash[:alert] = I18n.t('sms.failure.too_many_phones') % SMSService.max_phones
      redirect_to sms_path
    end

    if _group_ids.present? or _phones.present?
      return {group_ids: _group_ids, phones: _phones}
    else
      flash[:alert] = I18n.t('sms.failure.no_phones')
      redirect_to sms_path
    end
  end

  def sanitize_params
    params.permit(:client => [:sender_name, :phone_numbers, :template, :sms_content, :fake, :ids => []])[:client]
  end

  def check_empty_params
    if sanitize_params.nil?
      flash[:alert] = I18n.t('sms.failure.invalid_params')
      redirect_to sms_path
    end
  end

  def check_message
    if sanitize_params[:sms_content].present? or Template.exists?(sanitize_params[:template])
      return true
    end

    flash[:alert] = I18n.t('sms.failure.no_message')
    redirect_to sms_path
  end

  def check_sender_name_value
    if SenderName.valid_by_value?(sanitize_params[:sender_name], current_user)
      return true
    end

    flash[:alert] = I18n.t('sms.failure.invalid_sender_name')
    redirect_to sms_path
  end
end
