# -*- encoding : utf-8 -*-
class MessageController < AuthController
  before_filter :check_content!

  def sms_count
    render json: {sms_count: Message.sms_count(@content)}
  end

  private
  def check_content!
    _content = sanitize_params[:content]
    if _content.nil?
      render json: {error: I18n.t('message.failure.invalid_params')} and return
    else
      if _content.blank?
        render json: {error: I18n.t('message.failure.content_is_blank')} and return
      else
        @content = _content
      end
    end
  end

  def sanitize_params
    params.permit(:content)
  end
end
