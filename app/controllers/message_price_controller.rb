# -*- encoding : utf-8 -*-
class MessagePriceController < ApplicationController
  include PhoneHelper

  def calc
    _sms_count = Message.sms_count(content)

    total_sms_price = 0
    phones.each do |phone|
      _channel = Channel.channel(sender_name_value, current_user.alpha_megafon_channel, phone)
      total_sms_price += _sms_count * Message.calc_price_per_sms(_channel, current_user, phone)
    end

    groups.each do |group|
      total_sms_price += _sms_count * group.price_per_sms(sender_name_value)
    end

    render json: {sms_price: total_sms_price.to_f.round(2)}
  end

  private
  def content
    if sanitize_params[:content].present?
      return sanitize_params[:content]
    else
      template = Template.find_by_id_and_user_id(sanitize_params[:template_id], current_user)
      if template
        return template.text
      end
    end

    ''
  end

  def sanitize_params
    params.permit(:content, :sender_name_value, :phones, :template_id, :group_ids => [])
  end

  def phones
    return sanitize_params[:phones].split(',').select { |phone| normalize_phone_number(phone) }.compact.uniq if sanitize_params[:phones]
    []
  end

  def groups
    _group_ids = []

    if sanitize_params[:group_ids]
      sanitize_params[:group_ids].uniq.each do |group_id|
        if group_id.present?
          type, id = group_id.split('=')

          if type == 'group_id'
            _group_ids << Integer(id) rescue nil
          end
        end
      end
    end

    _group_ids.compact.uniq.map { |group_id| Group.find_by_id_and_user_id(group_id, current_user) }
  end

  def sender_name_value
    sanitize_params[:sender_name_value]
  end

end
