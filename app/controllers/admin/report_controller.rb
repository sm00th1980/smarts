# -*- encoding : utf-8 -*-
class Admin::ReportController < Admin::ApplicationController
  before_filter :channels, :only => [:charges, :charges_xls]
  before_filter :operators, :only => :charges_xls
  before_filter :decorate, :only => [:charges, :payments]
  before_filter :decorate_xls, :only => :charges_xls

  def charges
    @reports = reports(lambda { |user, begin_date, end_date| LogStat.stats(user, begin_date, end_date) }, @decorator.begin_date, @decorator.end_date)
    @total = @decorator.total(@channels, @reports)

    render 'admin/report/charges/index'
  end

  def charges_xls
    @reports = reports(lambda { |user, begin_date, end_date| LogStat.stats_by_operator(user, begin_date, end_date) }, @decorator.begin_date, @decorator.end_date)
    @total = @decorator.total(@channels, @operators, @reports)

    stream = render_to_string(template: 'admin/report/charges/index', formats: [:xls])
    send_data(stream, type: 'text/xml', filename: filename('charges', @decorator.begin_date, @decorator.end_date))
  end

  def payments
    @payments = AdminReportPaymentsDecorator.decorate_collection(Payment.approved.where(approved_at: [@decorator.begin_date..@decorator.end_date]))
    render 'admin/report/payments/index'
  end

  private
  def reports(stats, begin_date, end_date)
    LogStat.users_with_traffic(begin_date, end_date).map do |user|
      {
          stats: stats.call(user, begin_date, end_date),
          fio: user.fio,
          email: user.email,
          description: user.description
      }
    end
  end


  def filename(report_name, begin_date, end_date)
    "report_#{report_name}_#{begin_date.strftime('%Y-%m-%d')}_#{end_date.strftime('%Y-%m-%d')}.xls"
  end

  def decorate
    @decorator = AdminReportChargesDecorator.new(Object.new)
  end

  def decorate_xls
    @decorator = AdminReportChargesXlsDecorator.new(Object.new)
  end

  def channels
    @channels = Channel.working.sort
  end

  def operators
    @operators = CellOperatorGroup.working.sort
  end

end
