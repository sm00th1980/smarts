# -*- encoding : utf-8 -*-
class Admin::PaymentsController < Admin::ApplicationController
  include Admin::PaymentsHelper

  before_filter :find_user!, only: [:index, :create, :new]
  before_filter :find_service!, only: [:create]
  before_filter :find_amount!, only: [:create]
  before_filter :find_payment!, only: [:destroy, :approve, :refuse]

  def index
    @payments = Payment.where(user_id: @user.id).order(:date)
  end

  def create
    Payment.create!(
        amount: @amount,
        date: Time.now,
        service: @service,
        user: @user
    ).approve!

    flash[:notice] = I18n.t('admin.payment.success.created')
    redirect_to index_payments_path(@user)
  end

  def destroy
    user_id = @payment.user_id
    @payment.destroy
    flash[:notice] = I18n.t('admin.payment.success.deleted')
    redirect_to index_payments_path(user_id)
  end

  def approve
    @payment.approve!(true)
    flash[:notice] = I18n.t('admin.payment.success.approved')
    redirect_to index_payments_path(@payment.user)
  end

  def refuse
    @payment.fail!
    flash[:notice] = I18n.t('admin.payment.success.refused')
    redirect_to index_payments_path(@payment.user)
  end

end
