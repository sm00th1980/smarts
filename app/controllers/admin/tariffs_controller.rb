# -*- encoding : utf-8 -*-
class Admin::TariffsController < Admin::ApplicationController
  include Admin::TariffsHelper

  before_filter :find_user!, only: [:index, :new, :create]
  before_filter :find_user_tariff!, only: [:destroy]
  before_filter :find_tariff!, :find_begin_date!, :find_end_date!, :check_dates!, :check_cross_tariff!, only: [:create]

  def index
    @user_tariffs = UserTariff.where(user_id: @user.id).order(:begin_date)
  end

  def create
    UserTariff.create!(tariff: @tariff, begin_date: @begin_date, end_date: @end_date, user: @user)

    flash[:notice] = I18n.t('admin.user_tariff.success.created')
    redirect_to index_tariffs_path(@user)
  end

  def destroy
    user_id = @user_tariff.user_id
    @user_tariff.destroy
    flash[:notice] = I18n.t('admin.user_tariff.success.deleted')
    redirect_to index_tariffs_path(user_id)
  end

end
