# -*- encoding : utf-8 -*-
class Admin::OperatorTariffController < Admin::ApplicationController
  before_filter :decorate, only: [:index, :new, :create, :destroy, :edit, :update]

  def index
    @operator_tariffs = OperatorTariff.where(operator_id: @decorator.operator).order(:begin_date)
  end

  def create
    new_operator_tariff = OperatorTariff.new(operator: @decorator.operator, begin_date: @decorator.begin_date, end_date: @decorator.end_date, price_per_sms: @decorator.price_per_sms)

    if new_operator_tariff.save
      to_tariffs_with(notice: I18n.t('admin.operator_tariff.success.created'))
    else
      to_tariffs_with(alert: new_operator_tariff.errors.messages.values.flatten.uniq)
    end
  end

  def update
    with_operator_tariff do |operator_tariff|
      operator_tariff.update_attributes(begin_date: @decorator.begin_date, end_date: @decorator.end_date, price_per_sms: @decorator.price_per_sms)
      if operator_tariff.save
        to_tariffs_with(notice: I18n.t('admin.operator_tariff.success.updated'))
      else
        to_tariffs_with(alert: operator_tariff.errors.messages.values.flatten.uniq)
      end
    end
  end

  def destroy
    with_operator_tariff do |operator_tariff|
      operator_tariff.destroy
      to_tariffs_with(notice: I18n.t('admin.operator_tariff.success.delete'))
    end
  end

  private

  def decorate
    @decorator = AdminOperatorTariffDecorator.new(params)
  end

  def to_tariffs_with(notification)
    redirect_to admin_operator_tariffs_path(@decorator.operator_id), notification
  end

  def to_operators_with(notification)
    redirect_to admin_operators_index_path, notification
  end

  def with_operator_tariff
    if @decorator.operator_tariff.present?
      yield(@decorator.operator_tariff)
    else
      to_operators_with(alert: I18n.t('admin.operator_tariff.failure.not_exist'))
    end
  end

end
