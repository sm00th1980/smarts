# -*- encoding : utf-8 -*-
class Admin::ApplicationController < ApplicationController
  before_filter :admin_only

  def admin_only
    if current_user and current_user.admin?
      return nil
    end

    flash[:alert] = I18n.t('failure.admin_only')

    redirect_to root_path
  end
end
