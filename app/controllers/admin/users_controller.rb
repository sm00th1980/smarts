# -*- encoding : utf-8 -*-
class Admin::UsersController < Admin::ApplicationController
  before_filter :decorate, only: [:index]

  def index
    @users = User.normal.where(type: @decorator.type).where(status: @decorator.status).order(:email)
  end

  private
  def decorate
    @decorator = Admin::UsersDecorator.new(params)
  end
end
