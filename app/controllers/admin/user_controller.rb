# -*- encoding : utf-8 -*-
class Admin::UserController < Admin::ApplicationController
  include Admin::UsersHelper
  include Admin::UsersValidateParamsHelper

  before_filter :find_user!, only: [:update, :show, :update]
  before_filter :validate_params_for_create!, only: [:create]
  before_filter :validate_params_for_update!, only: [:update]

  def create
    new_user = User.create!(
        email: @email,
        fio: @fio,
        description: @description,
        password: @password,
        password_confirmation: @password_confirmation,
        type: @type,
        status: @status,
        charge_type: @charge_type,
        payment_type: @payment_type,
        permit_night_delivery: @permit_night_delivery,
        discount: @discount,
        permit_alpha_sender_name: @permit_alpha_sender_name,
        permit_def_sender_name: @permit_def_sender_name,
        reject_to_megafon: @reject_to_megafon,
        alpha_megafon_channel: @alpha_megafon_channel,
        reject_to_smarts_via_gold_channel: @reject_to_smarts_via_gold_channel,
        reject_to_mts: @reject_to_mts
    )

    UserTariff.create!(user: new_user, tariff: @tariff, begin_date: Date.today)

    flash[:notice] = I18n.t('user.success.created')
    redirect_to users_path
  end

  def update
    @user.email = @email
    @user.fio = @fio
    @user.description = @description
    @user.type = @type
    @user.status = @status
    @user.password = @password
    @user.charge_type = @charge_type
    @user.payment_type = @payment_type
    @user.permit_night_delivery = @permit_night_delivery
    @user.discount = @discount
    @user.permit_alpha_sender_name = @permit_alpha_sender_name
    @user.permit_def_sender_name = @permit_def_sender_name
    @user.reject_to_megafon = @reject_to_megafon
    @user.alpha_megafon_channel = @alpha_megafon_channel
    @user.reject_to_smarts_via_gold_channel = @reject_to_smarts_via_gold_channel
    @user.reject_to_mts = @reject_to_mts

    flash[:notice] = I18n.t('user.success.updated') if @user.save!
    redirect_to users_path
  end

  private
  def sanitize_params
    params.permit(:id, :user => [:email, :password, :password_confirmation, :fio, :description,
                                 :status_id, :type_id, :tariff_id, :charge_type_id, :payment_type_id, :alpha_megafon_channel_id,
                                 :permit_night_delivery, :discount, :permit_alpha_sender_name,
                                 :permit_def_sender_name, :reject_to_megafon, :reject_to_smarts_via_gold_channel,
                                 :reject_to_mts
    ])
  end

  def find_user!
    if not @user = User.find_by_id(sanitize_params[:id])
      flash[:alert] = I18n.t('user.failure.not_exist') % sanitize_params[:id]
      redirect_to users_path
    end
  end

end
