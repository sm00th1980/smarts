# -*- encoding : utf-8 -*-
class Admin::OperatorsController < Admin::ApplicationController

  def index
    @operators = CellOperatorGroup.all.sort { |o1, o2| o1.name <=> o2.name }
  end

end
