# -*- encoding : utf-8 -*-
class Admin::SenderNamesController < Admin::ApplicationController
  before_filter :find_sender_name!, :only => [:accept, :reject]

  def index
    @sender_names = SenderName.where(:status_id => SenderNameStatus.moderating)
  end

  def accept
    @sender_name.accept
    flash[:notice] = 'Имя отправителя одобрено'
    redirect_to admin_sender_names_path
  end

  def reject
    @sender_name.reject
    flash[:notice] = 'Имя отправителя отклонено'

    redirect_to admin_sender_names_path
  end

  private
  def find_sender_name!
    id = sanitize_params[:id]

    @sender_name = SenderName.find_by_id(id)
    if not @sender_name
      flash[:alert] = I18n.t('sender_name.not_exist') % id
      redirect_to admin_sender_names_path
    end
  end

  def sanitize_params
    params.permit(:id)
  end

end
