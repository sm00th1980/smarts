# -*- encoding : utf-8 -*-
class TariffsController < AuthController

  def index
    @operators = CellOperatorGroup.all.sort { |o1, o2| o1.name <=> o2.name }
  end

end
