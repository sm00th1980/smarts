# -*- encoding : utf-8 -*-
class SenderNameController < AuthController
  before_filter :find_sender_name!, only: [:use, :destroy]
  before_filter :check_exist_sender_name!, only: [:create]

  def create
    sender_name = SenderName.create_with_notify(current_user, sanitize_params[:value])

    if sender_name
      flash[:notice] = I18n.t('sender_name.create_success')
    else
      flash[:alert] = I18n.t('sender_name.verified_failed')
    end

    redirect_to sender_names_path
  end

  def destroy
    @sender_name.destroy
    flash[:notice] = I18n.t('sender_name.delete_success')

    redirect_to sender_names_path
  end

  def use
    if @sender_name.activate
      flash[:notice] = I18n.t('sender_name.activate_success')
    else
      if @sender_name.rejected?
        flash[:alert] = I18n.t('sender_name.verified_failed')
      else
        flash[:alert] = I18n.t('sender_name.on_moderating')
      end
    end

    redirect_to sender_names_path
  end

  private
  def find_sender_name!
    id = sanitize_params[:id]

    @sender_name = SenderName.find_by_id_and_user_id(id, current_user.id)

    if not @sender_name
      flash[:alert] = I18n.t('sender_name.not_exist') % id
      redirect_to sender_names_path
    end
  end

  def sanitize_params
    params.permit(:id, :value)
  end

  def check_exist_sender_name!
    _value = sanitize_params[:value]

    _sender_name = SenderName.find_by_value_and_user_id(_value, current_user.id)
    if _sender_name
      flash[:alert] = I18n.t('sender_name.already_exist') % _value
      redirect_to sender_names_path
    end
  end

end
