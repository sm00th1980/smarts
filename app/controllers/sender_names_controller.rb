# -*- encoding : utf-8 -*-
class SenderNamesController < AuthController

  def index
    @sender_names = current_user.sender_names.order(:value)
  end

end
