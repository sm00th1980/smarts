# -*- encoding : utf-8 -*-
class BlacklistController < AuthController
  include PhoneHelper

  before_filter :find_blocked_phone!, only: [:destroy]
  before_filter :find_phone!, only: [:create]

  def show
    @blocked_phones = BlackList.personal(current_user).order(:phone)
  end

  def create
    BlackList.create!(phone: @phone, user_id: current_user.id)

    flash[:notice] = I18n.t('user.black_list.success.created')
    redirect_to show_blacklist_path
  end

  def destroy
    @blocked_phone.destroy
    flash[:notice] = I18n.t('user.black_list.success.deleted')
    redirect_to show_blacklist_path
  end

  private
  def sanitize_params
    params.permit(:id, :phone)
  end

  def find_blocked_phone!
    if not @blocked_phone = BlackList.find_by_id_and_user_id(sanitize_params[:id], current_user.id)
      flash[:alert] = I18n.t('user.black_list.failure.not_exist') % sanitize_params[:id]
      redirect_to show_blacklist_path
    end
  end

  def find_phone!
    phone = normalize_phone_number(sanitize_params[:phone])
    if not phone
      flash[:alert] = I18n.t('user.black_list.failure.invalid_phone') % sanitize_params[:phone]
      redirect_to show_blacklist_path
    else
      if not BlackList.exists?(user_id: current_user.id, phone: phone)
        @phone = phone
      else
        flash[:alert] = I18n.t('user.black_list.failure.already_exist') % sanitize_params[:phone]
        redirect_to show_blacklist_path
      end
    end
  end
end
