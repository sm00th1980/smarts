# -*- encoding : utf-8 -*-
class AutodeliveryController < AuthController
  include TimeHelper

  before_filter :find_autodelivery!, :only => [:show, :update, :destroy, :activate, :deactivate, :run]
  before_filter :determine_period!, :only => [:update]
  before_filter :name!, :only => [:create, :update]

  def index
    @deliveries = AutoDelivery.where(:user_id => current_user.id).order('created_at DESC').paginate(:page => params[:page], :per_page => 15)
  end

  def update
    @autodelivery.name              = name!
    @autodelivery.description       = description
    @autodelivery.start_date        = start_date
    @autodelivery.stop_date         = stop_date
    @autodelivery.content           = content
    @autodelivery.template          = template
    @autodelivery.groups            = groups
    @autodelivery.clients           = clients
    @autodelivery.sender_name_value = sender_name_value

    @autodelivery.period      = @period

    if @period.every_hour?
      @autodelivery.week_day_id = nil
      @autodelivery.hour        = nil
      @autodelivery.minute      = @minute
    end

    if @period.every_day?
      @autodelivery.week_day_id = nil
      @autodelivery.hour        = @hour
      @autodelivery.minute      = @minute
    end

    if @period.every_week?
      @autodelivery.week_day_id = @week_day_id
      @autodelivery.hour        = @hour
      @autodelivery.minute      = @minute
    end

    @autodelivery.save!
    flash[:notice] = I18n.t('autodelivery.updated')

    redirect_to autodelivery_path
  end

  def create
    autodelivery = AutoDelivery.create!({
       :user_id     => current_user.id,
       :name        => name!,
       :description => description,
       :start_date  => start_date,
       :stop_date   => stop_date,
       :active      => false,
       :period      => AutoDeliveryPeriod.every_day,
       :hour        => Period::DEFAULT_HOUR,
       :minute      => Period::DEFAULT_MINUTE
    })

    flash[:notice] = I18n.t('autodelivery.created')

    redirect_to show_autodelivery_path(autodelivery)
  end

  def destroy
    @autodelivery.destroy
    flash[:notice] = I18n.t('autodelivery.deleted')
    redirect_to autodelivery_path
  end

  def activate
    if @autodelivery.valid_for_activation?
      @autodelivery.activate
      flash[:notice] = I18n.t('autodelivery.activation')
    else
      flash[:alert] = I18n.t('autodelivery.failure.activation') % @autodelivery.activation_errors.map { |k, v| "#{k} #{v}" }.join(',')
    end

    redirect_to autodelivery_path
  end

  def deactivate
    @autodelivery.deactivate
    flash[:notice] = I18n.t('autodelivery.deactivation')

    redirect_to autodelivery_path
  end

  def run
    if @autodelivery.valid_for_run?(true)
      @autodelivery.run(true)
      flash[:notice] = I18n.t('autodelivery.run')
    else
      flash[:alert] = I18n.t('autodelivery.failure.run') % @autodelivery.activation_errors.map { |k, v| "#{k} #{v}" }.join(',')
    end

    redirect_to autodelivery_path
  end

  private
  def to_date(date)
    if date.present?
      begin
        _date = date.to_date
      rescue
      else
        return _date
      end
    end

    nil
  end

  def sanitize_params
    params.permit(:id, autodelivery: [:period, :description, :hour, :minute, :week_day_id, :name, :start_date, :stop_date, :content, :template_id, :sender_name_value, :client_ids => []])
  end

  def find_autodelivery!
    @autodelivery = AutoDelivery.find_by_id_and_user_id(sanitize_params[:id], current_user.id)

    if not @autodelivery
      flash[:alert] = I18n.t('autodelivery.not_exist')
      redirect_to autodelivery_path
    end
  end

  def determine_period!
    @period = AutoDeliveryPeriod.find_by_period(period)

    if not @period
      flash[:alert] = I18n.t('autodelivery.failure.invalid_period')
      redirect_to autodelivery_index_path
    end

    @minute      = minute      if valid_minute?(minute)
    @hour        = hour        if valid_hour?(hour)
    @week_day_id = week_day_id if valid_week_day?(week_day_id)

    if @period.every_hour? and not @minute
      flash[:alert] = I18n.t('autodelivery.failure.invalid_period_time')
      redirect_to autodelivery_path
    end

    if @period.every_day? and not (@minute and @hour)
      flash[:alert] = I18n.t('autodelivery.failure.invalid_period_time')
      redirect_to autodelivery_path
    end

    if @period.every_week? and not (@minute and @hour and @week_day_id)
      flash[:alert] = I18n.t('autodelivery.failure.invalid_period_time')
      redirect_to autodelivery_path
    end
  end

  def name!
    _name = sanitize_params[:autodelivery][:name]

    if _name.blank?
      flash[:alert] = I18n.t('autodelivery.failure.blank_name')
      redirect_to autodelivery_path
    end

     _name
  end

  def start_date
    to_date(sanitize_params[:autodelivery][:start_date])
  end

  def stop_date
    to_date(sanitize_params[:autodelivery][:stop_date])
  end

  def content
    return sanitize_params[:autodelivery][:content] if sanitize_params[:autodelivery][:content].present?
  end

  def template
    template_id = sanitize_params[:autodelivery][:template_id] if sanitize_params[:autodelivery][:template_id].present?
    Template.find_by_id_and_user_id(template_id, current_user.id)
  end

  def description
    return sanitize_params[:autodelivery][:description] if sanitize_params[:autodelivery][:description].present?
  end

  def period
    sanitize_params[:autodelivery][:period]
  end

  def sender_name_value
    sanitize_params[:autodelivery][:sender_name_value]
  end

  def client_ids
    sanitize_params[:autodelivery][:client_ids]
  end

  def minute
    sanitize_params[:autodelivery][:minute]
  end

  def hour
    sanitize_params[:autodelivery][:hour]
  end

  def week_day_id
    sanitize_params[:autodelivery][:week_day_id]
  end

  def groups
    client_ids.select{|el| el.present? and 'group_id='  == el[0..8]}.map{|el| el.split('=').last} if client_ids.present?
  end

  def clients
    client_ids.select{|el| el.present? and 'client_id=' == el[0..9]}.map{|el| el.split('=').last} if client_ids.present?
  end
end
