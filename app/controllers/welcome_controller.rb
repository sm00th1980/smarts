# -*- encoding : utf-8 -*-
class WelcomeController < NonAuthController
  def index
    if user_signed_in?
      if current_user.admin?
        redirect_to users_path
      else
        redirect_to sms_path
      end
    else
      redirect_to new_user_session_path
    end
  end
end
