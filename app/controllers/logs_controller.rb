# -*- encoding : utf-8 -*-
class LogsController < AuthController

  def index
    @logs = Log.where(user_id: current_user.id).order('created_at DESC').paginate(:page => params[:page], :per_page => 15)
  end

end
