# -*- encoding : utf-8 -*-
class TemplatesController < AuthController

  def index
    @templates = current_user.templates
  end

end
