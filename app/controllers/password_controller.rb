# -*- encoding : utf-8 -*-
class PasswordController < AuthController
  PASSWORD_LENGTH = 8

  before_filter :check_new_password_and_confirmation!, only: [:update]

  def update
    current_user.password = @new_password
    current_user.save!

    flash[:notice] = I18n.t('user.success.password.updated')

    redirect_to new_user_session_path
  end

  private
  def sanitize_params
    params.permit(:new_password, :confirmation_new_password)
  end

  def check_new_password_and_confirmation!
    new_password = sanitize_params[:new_password]
    confirmation = sanitize_params[:confirmation_new_password]

    if new_password.blank? or confirmation.blank?
      flash[:alert] = I18n.t('user.failure.password_empty')
      redirect_to new_password_path and return
    end

    if new_password.size < PASSWORD_LENGTH or confirmation.size < PASSWORD_LENGTH
      flash[:alert] = I18n.t('user.failure.password_too_short')
      redirect_to new_password_path and return
    end

    if not new_password == confirmation
      flash[:alert] = I18n.t('user.failure.passwords_not_equal')
      redirect_to new_password_path and return
    end

    if flash[:alert].present?
      redirect_to new_password_path and return
    else
      @new_password = new_password
    end

  end

end
