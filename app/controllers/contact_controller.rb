# -*- encoding : utf-8 -*-
class ContactController < AuthController

  before_filter :find_contact_email!, :only => :update

  def update
    current_user.contact_email = @contact_email
    current_user.save!

    flash[:notice] = I18n.t('contact.saved')

    redirect_to show_contact_path
  end

  private
  def sanitize_params
    params.permit(:contact_email)
  end

  def find_contact_email!
    contact_email = sanitize_params[:contact_email]

    if ValidatesEmailFormatOf::validate_email_format(contact_email, {}).nil?
      @contact_email = contact_email
    else
      flash[:alert] = I18n.t('contact.failure.invalid') % contact_email
      redirect_to show_contact_path
    end

  end

end
