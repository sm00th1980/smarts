# -*- encoding : utf-8 -*-
class SessionsController < Devise::SessionsController

  def create
    if user and password_valid? and user.status.active?
      #auth
      sign_in user
      path = user.admin? ? users_path : sms_path
      redirect_to path, notice: I18n.t('devise.sessions.user.signed_in')
    else
      #non auth
      redirect_to new_user_session_path, alert: I18n.t('devise.failure.user.invalid')
    end
  end

  def destroy
    sign_out current_user if current_user.present?
    redirect_to new_user_session_path, notice: I18n.t('devise.sessions.user.signed_out')
  end

  private
  def password_valid?
    return true if user and user.valid_password?(password)
    false
  end

  def sanitize_params
    params.permit(user: [:email, :password])
  end

  def username
    sanitize_params[:user][:email] rescue nil
  end

  def password
    sanitize_params[:user][:password] rescue nil
  end

  def user
    User.find_by(email: username)
  end

end
