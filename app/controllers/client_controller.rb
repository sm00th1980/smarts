# -*- encoding : utf-8 -*-
class ClientController < AuthController
  include ClientHelper

  before_filter :find_client!, :only => [:birthday_congratulation_update, :destroy, :update, :show]
  before_filter :find_group!, :only => [:create, :new]
  before_filter :find_birthday_congratulation, :only => [:birthday_congratulation_update]
  before_filter :find_phone!, :only => [:create, :update]

  before_filter :check_dublicate_phone_before_create!, :only => [:create]
  before_filter :check_dublicate_phone_before_update!, :only => [:update]


  def birthday_congratulation_update
    if @client.birthday_valid?
      @client.birthday_congratulation = birthday_congratulation
      @client.save

      json = 'saved'
    else
      json = 'invalid birthday'
    end

    render :json => json
  end

  def create
    Client.create do |client|
      client.fio = fio
      client.phone = @phone
      client.birthday = birthday
      client.description = description
      client.group = @group
    end

    redirect_to show_group_path(@group), notice: I18n.t('client.success.created')
  end

  def destroy
    @client.destroy
    redirect_to show_group_path(group.id, {:page => page}), notice: I18n.t('client.success.deleted')
  end

  def update
    @client.fio = fio
    @client.phone = @phone
    @client.birthday = birthday
    @client.description = description

    @client.save

    redirect_to show_group_path(@client.group, {:page => page}), notice: I18n.t('client.success.updated')
  end

  private
  def check_dublicate_phone_before_create!
    phones = @group.clients.map{|client| normalize_phone_number(client.phone)}

    if phones.include?(normalize_phone_number(@phone))
      redirect_to show_group_path(@group), alert: I18n.t('client.failure.phone_dublicate') % @phone
    end
  end

  def check_dublicate_phone_before_update!
    phones = @client.group.clients.map{|client| normalize_phone_number(client.phone)}.reject{|phone| phone == @client.phone}

    if phones.include?(normalize_phone_number(@phone))
      redirect_to show_group_path(@client.group), alert: I18n.t('client.failure.phone_dublicate') % @phone
    end
  end
end
