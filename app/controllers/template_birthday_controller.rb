# -*- encoding : utf-8 -*-
class TemplateBirthdayController < AuthController
  include TemplateBirthdayHelper

  def show
    @birthday_message = current_user.birthday_message
  end

  def update
    current_user.birthday_message = birthday_message
    current_user.birthday_hour = birthday_hour
    current_user.birthday_sender_name = birthday_sender_name

    current_user.save!

    if birthday_sender_name.present?
      flash[:notice] = I18n.t('template_birthday.success.updated')
    else
      flash[:alert] = I18n.t('template_birthday.failure.invalid_sender_name')
    end

    redirect_to show_template_birthday_path
  end

  private
  def sanitize_params
    params.permit(:template_birthday => [:birthday_message, :birthday_hour, :birthday_sender_name])
  end

  def birthday_message
    sanitize_params[:template_birthday][:birthday_message]
  end

  def birthday_hour
    valid_hour(sanitize_params[:template_birthday][:birthday_hour])
  end

  def birthday_sender_name
    _sender_name_value = sanitize_params[:template_birthday][:birthday_sender_name]
    if SenderName.valid_by_value?(_sender_name_value, current_user)
      return _sender_name_value
    end

    nil
  end

end
