# -*- encoding : utf-8 -*-
class LogController < AuthController
  before_filter :find_log!, :only => [:show, :stop, :report, :new_delivery]
  before_filter :check_contact_email!, :only => :report

  def show
    @send_logs = SendLog.where(log_id: @log.id).order('created_at DESC').paginate(:page => params[:page], :per_page => 15)
  end

  def delete_clients
    log = Log.where(id: params[:id], user_id: current_user).first
    count = log.delete_clients
    flash[:notice] = I18n.t('log.delete_clients') % count
    redirect_to logs_path
  end

  def stop
    if @log.stop!
      flash[:notice] = t('log.stopping.success')
    else
      flash[:alert] = t('log.stopping.failure')
    end

    redirect_to logs_path
  end

  def report
    Report::Manager.delay.generate(@log)

    flash[:notice] = I18n.t('report.generating') % current_user.contact_email

    redirect_to logs_path
  end

  def new_delivery
    SMSService.send_sms(current_user,
                        @log.content,
                        {phones: @log.phones, group_ids: @log.group_ids},
                        @log.sender_name_value,
                        @log.fake,
                        LogType.by_not_delivered,
                        @log.id)

    flash[:notice] = I18n.t('sms.started')
    redirect_to logs_path
  end

  private
  def sanitize_params
    params.permit(:id)
  end

  def find_log!
    @log = Log.find_by_id_and_user_id(sanitize_params[:id], current_user.id)
    if @log.blank?
      flash[:alert] = I18n.t('log.failure.not_exist') % sanitize_params[:id]
      redirect_to logs_path
    end
  end

  def check_contact_email!
    if not current_user.has_contact_email?
      flash[:alert] = I18n.t('report.failure.contact_email_incorrect')
      redirect_to logs_path
    end
  end

end
