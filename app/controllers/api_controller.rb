# -*- encoding : utf-8 -*-
class ApiController < ActionController::Base
  include PhoneHelper

  before_filter :auth!

  before_filter :check_message, :check_sender_name_value, :check_phones, :check_balance, :only => :send_sms

  before_filter :check_report_params, :only => :report_sms
  before_filter :find_log, :only => :report_sms

  def send_sms
    @log = SMSService.send_sms(@user, @content, {:phones => check_phones}, sanitize_params[:sender_name], false, LogType.api)
  end

  private
  def sanitize_params
    params.permit(:login, :password, :sender_name, :text, :id, :phones => [])
  end

  def auth!
    @user = User.find_by_email(sanitize_params[:login])
    if not @user or not @user.valid_password?(sanitize_params[:password]) or not @user.status.active?
      render 'error', :locals => {:error => I18n.t('devise.failure.user.invalid')} and return
    end
  end

  def check_report_params
    if sanitize_params[:id].blank?
      render 'error', :locals => {:error => I18n.t('api.log_blank')} and return
    end
  end

  def find_log
    @log = Log.find_by_id_and_user_id(sanitize_params[:id], @user.id)
    if @log.nil?
      render 'error', :locals => {:error => I18n.t('api.log_not_found')} and return
    end
  end

  def check_phones

    phones = sanitize_params[:phones]

    if phones.nil?
      render 'error', :locals => {:error => I18n.t('api.failure.phones_not_exist')} and return
    end

    if phones == '' or phones.empty?
      render 'error', :locals => {:error => I18n.t('api.failure.phones_blank')} and return
    end

    phones = phones.map{|phone| normalize_phone_number(phone)}.uniq.compact

    if phones.empty?
      render 'error', :locals => {:error => I18n.t('api.failure.phones_all_invalid')} and return
    end

    if phones.size > SMSService.max_phones
      render 'error', :locals => {:error => I18n.t('api.failure.phones_too_much') % SMSService.max_phones} and return
    end

    phones
  end

  def check_message
    text_invalid = sanitize_params[:text].blank? rescue true
    if text_invalid
      render 'error', :locals => {:error => I18n.t('api.text_blank')} and return
    end

    @content = sanitize_params[:text].gsub("\\r\\n", "\r\n")
  end

  def check_sender_name_value
    unless SenderName.valid_by_value?(sanitize_params[:sender_name], @user)
      render 'error', :locals => {:error => I18n.t('api.sender_name_invalid')} and return
    end
  end

  def check_balance
    if @user.payment_type.prepaid?
      if @user.current_balance <= 0 or @user.current_balance < check_phones.size * @user.price_per_sms
        render 'error', :locals => {:error => I18n.t('api.failure.balance_too_low')} and return
      end
    end
  end

end
