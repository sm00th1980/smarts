# -*- encoding : utf-8 -*-

class Mailer < ActionMailer::Base

  def send_report(email, filename)
    send_email(email, Rails.configuration.mail_sender, I18n.t('report.subject') % Rails.configuration.site_name, '', :report, [filename])

    #запускаем нотификацию для удаления файла
    ActiveSupport::Notifications.instrument('report.sended', path: filename[:path])
  end

  def send_email(email, from, subject, message, template, files={})
    deliver_email(email, from, subject, {message: message}, template, files)
  end

  private
  def deliver_email(email, from, subject, locals, template, files)
    files.each do |file|
      attachments[file[:name]] = File.read(file[:path])
    end

    mail(:to => email, :from => from, :subject => subject) do |format|
      format.html { render template, :locals => locals }
    end
  end

end
