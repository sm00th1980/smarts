# -*- encoding : utf-8 -*-
module PhoneHelper

  private
  def phone_number_valid?(phone_number)
    if phone_number.present?
      _phone_number = phone_number.to_s.scan(/\d+/).join #оставляем только цифры

      return true if _phone_number.length == 11 and ['79', '89'].include? _phone_number[0..1]
    end

    false
  end

  def normalize_phone_number(phone_number)
    if phone_number_valid?(phone_number)
      _phone_number = phone_number.to_s.scan(/\d+/).join #оставляем только цифры

      if '8' == _phone_number.first
        return "7#{_phone_number[1..-1]}".to_i
      else
        return _phone_number.to_i
      end
    end
  end

end
