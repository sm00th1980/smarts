# -*- encoding : utf-8 -*-
module GroupHelper
  def supported_formats
    Group.supported_formats
  end

  def file_format(filename)
    _format = filename.split('.').last rescue nil
    return _format if supported_formats.include? _format
  end

  def sorting column #TODO: может стоит посмотреть на url_for?
    if column == @column
      order = (@order == 'asc')?'desc':'asc'
      icon = fa_icon('sort-'+@order)
    else
      order = @order
      icon = fa_icon('sort')
    end

    link_to icon,
            controller: :group,
            action:     :show,
            id:         params[:id],
            column:     column,
            order:      order,
            search:     CGI::escapeHTML(params['search'] || '')
  end

  def price_per_sms(price)
    if price.present?
      round_currency price
    else
      I18n.t('group.price_per_sms_is_calculating')
    end
  end
end
