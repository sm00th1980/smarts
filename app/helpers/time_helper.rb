# -*- encoding : utf-8 -*-
module TimeHelper

  def valid_minute?(minute)
    _minute = Integer(minute) rescue nil
    return true if (0..59).include? _minute

    false
  end

  def valid_hour?(hour)
    if hour.present?
      _hour = Integer(hour) rescue nil

      return true if (0..23).include?(_hour)
    end

    false
  end

  def valid_week_day?(week_day_id)
    WeekDay.exists?(id: week_day_id)
  end

  BEGIN_HOUR, BEGIN_MIN, BEGIN_SEC = 9, 0, 0 #начало дня
  END_HOUR, END_MIN, END_SEC = 20, 0, 0 #конец дня

  def night_time?(time)
    return false if time == begin_time(time)
    return true if time == end_time(time)

    !time.between?(begin_time(time), end_time(time))
  end

  def begin_time(time)
    time.change(hour: BEGIN_HOUR, min: BEGIN_MIN, sec: BEGIN_SEC)
  end

  def end_time(time)
    time.change(hour: END_HOUR, min: END_MIN, sec: END_SEC)
  end
end
