# -*- encoding : utf-8 -*-
module AutoDeliveryHelper
  def selected?(autodelivery, group)
    if autodelivery.has_receivers? and autodelivery.groups.include? group
      return 'selected'
    end
  end

  def autodelivery_status(autodelivery)
    if autodelivery.activation_errors.empty?
      css = 'success' if autodelivery.active?
    else
      if autodelivery.overdued?
        css = 'grey'
      else
        css = 'alert'
      end
    end

    "<span class='#{css} label'>#{autodelivery.status}</span>".html_safe
  end
end
