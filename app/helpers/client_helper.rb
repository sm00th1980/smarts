# -*- encoding : utf-8 -*-
module ClientHelper
  include PhoneHelper

  module Params
    def sanitize_params
      params.permit(:id, :group_id, :page, client: [:fio, :phone, :birthday, :description, :birthday_congratulation, :group_id])
    end

    def client_id
      sanitize_params[:id] rescue nil
    end

    def page
      sanitize_params[:page] rescue nil
    end

    def fio
      sanitize_params[:client][:fio] rescue nil
    end

    def phone
      sanitize_params[:client][:phone] rescue nil
    end

    def birthday
      sanitize_params[:client][:birthday] rescue nil
    end

    def description
      sanitize_params[:client][:description] rescue nil
    end

    def group_id
      sanitize_params[:client][:group_id] rescue sanitize_params[:group_id] rescue nil
    end

    def birthday_congratulation
      sanitize_params[:client][:birthday_congratulation] rescue false
    end
  end

  private
  include Params

  def find_client!
    _client = Client.find_by(id: client_id)
    if _client and _client.group.user == current_user
      @client = _client
    else
      redirect_to groups_path, alert: I18n.t('client.failure.not_exist') % client_id
    end
  end

  def group
    @client.group if @client
  end

  def find_group!
    @group = Group.find_by(id: group_id, user_id: current_user)
    if not @group
      redirect_to groups_path, alert: I18n.t('group.not_exist') % group_id
    end
  end

  def find_birthday_congratulation
    if ['true','false'].include? birthday_congratulation.downcase
      return true  if 'true'  == birthday_congratulation.downcase
      return false if 'false' == birthday_congratulation.downcase
    end

    raise "error on frontend - birthday_congratulation is <%s>, but should be true or false" % birthday_congratulation
  end

  def find_phone!
    if normalize_phone_number(phone).present?
      @phone = normalize_phone_number(phone)
    else
      redirect_to show_group_path(@group), alert: I18n.t('client.failure.phone_invalid') % phone
    end
  end
end
