# -*- encoding : utf-8 -*-
module LogStatHelper

  def with_leading_zero(str)
    if str.to_s.length == 1
      return "0#{str}"
    end

    str
  end

  def full_month(date)
    "#{date.year}#{with_leading_zero(date.month)}"
  end

end
