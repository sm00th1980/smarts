# -*- encoding : utf-8 -*-
module ApplicationHelper

  private
  def sender_name_values
    current_user.accepted_sender_name_values.collect { |s| [s[:value], s[:value]] }.unshift(['не известно', 'не известно'])
  end

end
