# -*- encoding : utf-8 -*-
module TemplateBirthdayHelper
  def valid_hour(hour)
    _hour = Integer(hour) rescue nil

    if (0..23).include? _hour
      return _hour
    else
      return Period::DEFAULT_HOUR
    end
  end
end
