# -*- encoding : utf-8 -*-
module Admin::ReportHelper
  def round_currency(number)
    number.to_f.round(2)
  end
end
