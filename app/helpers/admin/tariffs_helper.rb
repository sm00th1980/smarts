# -*- encoding : utf-8 -*-
module Admin::TariffsHelper
  module Params
    def sanitize_params
      params.permit(:id, tariff: [:id, :begin_date, :end_date])
    end

    def user_id
      sanitize_params[:id] rescue nil
    end

    def user_tariff_id
      sanitize_params[:id] rescue nil
    end

    def tariff_id
      sanitize_params[:tariff][:id] rescue nil
    end

    def begin_date
      sanitize_params[:tariff][:begin_date] rescue nil
    end

    def end_date
      sanitize_params[:tariff][:end_date] rescue nil
    end
  end

  private
  include Params

  def redirect_on_users(alert)
    flash[:alert] = alert
    redirect_to users_path
  end

  def redirect_on_tariffs(alert)
    flash[:alert] = alert
    redirect_to new_tariffs_path(@user)
  end

  def to_date(date)
    Date.parse(date) rescue nil
  end

  def find_user!
    if not @user = User.find_by_id(user_id)
      redirect_on_users(I18n.t('user.failure.not_exist') % user_id)
    end
  end

  def find_user_tariff!
    if not @user_tariff = UserTariff.find_by_id(user_tariff_id)
      redirect_on_users(I18n.t('admin.user_tariff.failure.not_exist') % user_tariff_id)
    end
  end

  def find_tariff!
    if not @tariff = Tariff.find_by_id(tariff_id)
      redirect_on_tariffs(I18n.t('admin.user_tariff.failure.tariff_not_found') % tariff_id)
    end
  end

  def find_begin_date!
    if not @begin_date = to_date(begin_date)
      redirect_on_tariffs(I18n.t('admin.user_tariff.failure.begin_date_invalid') % begin_date)
    end
  end

  def find_end_date!
    if end_date == ''
      @end_date = nil
    else
      if not @end_date = to_date(end_date)
        redirect_on_tariffs(I18n.t('admin.user_tariff.failure.end_date_invalid') % end_date)
      end
    end
  end

  def check_dates!
    if @begin_date and @end_date and @begin_date > @end_date
      redirect_on_tariffs(I18n.t('admin.user_tariff.failure.begin_date_should_be_less_then_end_date'))
    end
  end

  def check_cross_tariff!
    cross = false
    @user.user_tariffs.each do |user_tariff|

      if cross = cross_tariff?(user_tariff, @begin_date, @end_date)
        break
      end
    end

    if cross
      redirect_on_tariffs(I18n.t('admin.user_tariff.failure.user_tariffs_cross'))
    end
  end

  def cross_tariff?(user_tariff, new_begin_date, new_end_date)
    if user_tariff.end_date.nil?
      #opened user_tariff
      if new_end_date.present?
        if new_begin_date < user_tariff.begin_date and new_end_date < user_tariff.begin_date
          return false
        end
      end
    else
      #closed user_tariff
      if new_end_date.nil?
        if user_tariff.end_date < new_begin_date
          return false
        end
      else
        if user_tariff.end_date < new_begin_date
          return false
        end

        if new_end_date < user_tariff.begin_date
          return false
        end
      end
    end

    true
  end
end
