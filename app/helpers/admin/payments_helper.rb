# -*- encoding : utf-8 -*-
module Admin::PaymentsHelper
  module Params
    def sanitize_params
      params.permit(:id, payment: [:service_id, :amount])
    end

    def user_id
      sanitize_params[:id] rescue nil
    end

    def service_id
      sanitize_params[:payment][:service_id] rescue nil
    end

    def amount
      sanitize_params[:payment][:amount] rescue nil
    end

    def payment_id
      sanitize_params[:id] rescue nil
    end
  end

  private
  include Params

  def redirect_on_users(alert)
    flash[:alert] = alert
    redirect_to users_path
  end

  def redirect_on_payments(alert)
    flash[:alert] = alert
    redirect_to index_payments_path(@user)
  end

  def find_user!
    if not @user = User.find_by_id(user_id)
      redirect_on_users(I18n.t('user.failure.not_exist') % user_id)
    end
  end

  def find_service!
    if not @service = PaymentService.find_by_id(service_id)
      redirect_on_payments(I18n.t('admin.payment.failure.payment_service_not_exist') % service_id)
    end
  end

  def find_amount!
    _amount = Float(amount) rescue nil
    if _amount.present? and _amount > 0
      @amount = _amount
    else
      redirect_on_payments(I18n.t('admin.payment.failure.amount_invalid'))
    end
  end

  def find_payment!
    if not @payment = Payment.find_by_id(payment_id)
      redirect_on_users(I18n.t('admin.payment.failure.not_exist') % payment_id)
    end
  end

end
