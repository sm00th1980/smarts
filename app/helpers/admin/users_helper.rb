# -*- encoding : utf-8 -*-
module Admin::UsersHelper

  def user_statuses
    [
        {id: UserStatus.active.id, name: UserStatus.active.name},
        {id: UserStatus.blocked.id, name: UserStatus.blocked.name},
        {id: UserStatus.deleted.id, name: UserStatus.deleted.name}
    ]
  end

  def user_types
    [
        {id: UserType.business.id, name: UserType.business.name},
        {id: UserType.demo.id, name: UserType.demo.name},
        {id: UserType.operator.id, name: UserType.operator.name}
    ]
  end

end

