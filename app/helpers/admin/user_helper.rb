# -*- encoding : utf-8 -*-
module Admin::UserHelper

  def user_tariffs
    Tariff.all.map{|t| ["#{t.name}. Стоимость за СМС: #{number_to_currency(t.price_per_sms)}", t.id]}
  end

  def user_payment_types
    PaymentType.order(:internal_name).map{|payment_type| {id: payment_type.id, name: payment_type.name}}
  end

  def user_charge_types
    ChargeType.all.map{|charge_type| {id: charge_type.id, name: charge_type.name}}
  end

  def user_permit_night_delivery
    [
        {:id => 'reject', :name => 'Запрещена'},
        {:id => 'permit', :name => 'Разрешена'}
    ]
  end

  def user_permit_night_delivery_for_select(value)
    if value == true
      return 'permit'
    end

    'reject'
  end

  def user_permit_alpha_sender_name
    [
        {:id => 'permit', :name => 'Разрешено'},
        {:id => 'reject', :name => 'Запрещено'}
    ]
  end

  def user_permit_alpha_sender_name_for_select(value)
    if value == true
      return 'permit'
    end

    'reject'
  end

  def user_permit_def_sender_name
    [
        {:id => 'permit', :name => 'Разрешено'},
        {:id => 'reject', :name => 'Запрещено'}
    ]
  end

  def user_permit_def_sender_name_for_select(value)
    if value == true
      return 'permit'
    end

    'reject'
  end

  def user_reject_to_megafon
    [
        {:id => 'permit', :name => 'Рассылка разрешена'},
        {:id => 'reject', :name => 'Рассылка запрещена'}
    ]
  end

  def user_reject_to_megafon_for_select(value)
    if value == true
      return 'reject'
    end

    'permit'
  end

  def user_reject_to_mts
    [
        {:id => 'permit', :name => 'Рассылка разрешена'},
        {:id => 'reject', :name => 'Рассылка запрещена'}
    ]
  end

  def user_reject_to_mts_for_select(value)
    if value == true
      return 'reject'
    end

    'permit'
  end

  def user_alpha_megafon_channel
    [
        {:id => Channel.gold.id, :name => Channel.gold.name},
        {:id => Channel.megafon_fixed.id, :name => Channel.megafon_fixed.name},
        {:id => Channel.megafon_multi.id, :name => Channel.megafon_multi.name}
    ]
  end

  def user_alpha_megafon_channel_for_select(channel)
    if channel.megafon_fixed? or channel.megafon_multi?
      return channel.id
    end

    Channel.gold.id
  end

  def user_reject_to_smarts_via_gold_channel
    [
        {:id => 'permit', :name => 'Рассылка разрешена'},
        {:id => 'reject', :name => 'Рассылка запрещена'}
    ]
  end

  def user_reject_to_smarts_via_gold_channel_for_select(value)
    if value == true
      return 'reject'
    end

    'permit'
  end

end

