# -*- encoding : utf-8 -*-
require 'validates_email_format_of'

module Admin::UsersValidateParamsHelper
  private
  def email
    sanitize_params[:user][:email] rescue nil
  end

  def password
    sanitize_params[:user][:password] rescue nil
  end

  def password_confirmation
    sanitize_params[:user][:password_confirmation] rescue nil
  end

  def fio
    sanitize_params[:user][:fio] rescue nil
  end

  def description
    sanitize_params[:user][:description] rescue nil
  end

  def find_discount
    sanitize_params[:user][:discount] rescue nil
  end

  #tariff
  def tariff_id
    sanitize_params[:user][:tariff_id] rescue nil
  end

  def find_tariff
    Tariff.find_by(id: tariff_id)
  end

  #type
  def type_id
    sanitize_params[:user][:type_id] rescue nil
  end

  def find_type
    UserType.find_by(id: type_id) rescue nil
  end

  #status
  def status_id
    sanitize_params[:user][:status_id] rescue nil
  end

  def find_status
    UserStatus.find_by(id: status_id)
  end

  #charge_type
  def charge_type_id
    sanitize_params[:user][:charge_type_id] rescue nil
  end

  def find_charge_type
    ChargeType.find_by(id: charge_type_id) rescue nil
  end

  #payment_type
  def payment_type_id
    sanitize_params[:user][:payment_type_id] rescue nil
  end

  def find_payment_type
    PaymentType.find_by(id: payment_type_id) rescue nil
  end

  #alpha_megafon_channel
  def alpha_megafon_channel_id
    sanitize_params[:user][:alpha_megafon_channel_id] rescue nil
  end

  def find_alpha_megafon_channel
    Channel.find_by(id: alpha_megafon_channel_id) rescue nil
  end

  def find_permit_night_delivery
    sanitize_params[:user][:permit_night_delivery] rescue nil
  end

  def find_permit_alpha_sender_name
    sanitize_params[:user][:permit_alpha_sender_name] rescue nil
  end

  def find_permit_def_sender_name
    sanitize_params[:user][:permit_def_sender_name] rescue nil
  end

  def find_reject_to_megafon
    sanitize_params[:user][:reject_to_megafon] rescue nil
  end

  def find_reject_to_mts
    sanitize_params[:user][:reject_to_mts] rescue nil
  end

  def find_reject_to_smarts_via_gold_channel
    sanitize_params[:user][:reject_to_smarts_via_gold_channel] rescue nil
  end

  def validate_params!
    errors = []

    @fio = fio
    @description = description
    @type = find_type
    @charge_type = find_charge_type
    @payment_type = find_payment_type
    @discount = find_discount
    @status = find_status

    if find_permit_night_delivery and find_permit_night_delivery.downcase == 'permit'
      @permit_night_delivery = true
    else
      @permit_night_delivery = false
    end

    if find_permit_alpha_sender_name and find_permit_alpha_sender_name.downcase == 'permit'
      @permit_alpha_sender_name = true
    else
      @permit_alpha_sender_name = false
    end

    if find_permit_def_sender_name and find_permit_def_sender_name.downcase == 'permit'
      @permit_def_sender_name = true
    else
      @permit_def_sender_name = false
    end

    if find_reject_to_megafon and find_reject_to_megafon.downcase == 'reject'
      @reject_to_megafon = true
    else
      @reject_to_megafon = false
    end

    if find_reject_to_mts and find_reject_to_mts.downcase == 'reject'
      @reject_to_mts = true
    else
      @reject_to_mts = false
    end

    if find_alpha_megafon_channel and (find_alpha_megafon_channel.megafon_fixed? or find_alpha_megafon_channel.megafon_multi?)
      @alpha_megafon_channel = find_alpha_megafon_channel
    else
      @alpha_megafon_channel = Channel.gold
    end

    if find_reject_to_smarts_via_gold_channel and find_reject_to_smarts_via_gold_channel.downcase == 'reject'
      @reject_to_smarts_via_gold_channel = true
    else
      @reject_to_smarts_via_gold_channel = false
    end

    if not @type
      errors << I18n.t('user.failure.type_not_found') % type_id
    end

    if not @status
      errors << I18n.t('user.failure.status_not_found') % status_id
    end

    if not @charge_type
      errors << I18n.t('user.failure.charge_type_not_found') % charge_type_id
    end

    if not @payment_type
      errors << I18n.t('user.failure.payment_type_not_found') % payment_type_id
    end

    email_check = ValidatesEmailFormatOf::validate_email_format(email, {})
    if not email_check.nil?
      errors << I18n.t('user.failure.login_is_not_email') % email
    end

    if not valid_discount?(@discount)
      errors << I18n.t('user.failure.discount_invalid') % @discount
    else
      @discount = @discount.to_i
    end

    errors
  end

  def validate_params_for_create!
    errors = validate_params!

    redirect_on_errors_for_create(errors) do
      @email = email
      @tariff = find_tariff
      @password = password
      @password_confirmation = password_confirmation

      if User.exists?(:email => @email)
        errors << I18n.t('user.failure.login_is_taken')
      end

      if @password == @password_confirmation
        if @password.size <= 7
          errors << I18n.t('user.failure.password_too_short')
        end
      else
        errors << I18n.t('user.failure.passwords_not_equal')
      end

      if not @tariff
        errors << I18n.t('user.failure.tariff_not_found') % tariff_id
      end
      redirect_on_errors_for_create(errors)
    end
  end

  def redirect_on_errors_for_create(errors)
    if errors.any?
      flash[:alert] = errors.join('; ')
      redirect_to new_user_path
    else
      yield if block_given?
    end
  end

  def validate_params_for_update!
    errors = validate_params!

    @status = find_status
    @email = email
    redirect_on_errors_for_update(errors) do
      if User.where('id <> ?', @user.id).map { |user| user.email }.include? @email
        errors << I18n.t('user.failure.login_is_taken')
      end

      redirect_on_errors_for_update(errors)
    end
  end

  def redirect_on_errors_for_update(errors)
    if errors.any?
      flash[:alert] = errors.join('; ')
      redirect_to show_user_path(@user)
    else
      yield if block_given?
    end
  end

  def valid_discount?(discount)
    _discount = Integer(discount) rescue nil
    return true if _discount.present? and _discount >= 0 and _discount <= 100
    false
  end

end
