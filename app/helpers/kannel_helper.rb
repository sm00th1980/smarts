# -*- encoding : utf-8 -*-
module KannelHelper
  def parse_error_code(ack, channel)
    if ack == 'NACK/Retries Exceeded'
      return SendLogDeliveryError.bronze_retries_exceeded if channel.bronze?
      return SendLogDeliveryError.gold_retries_exceeded if channel.gold? or channel.megafon_fixed? or channel.megafon_multi?
    end

    if ack == 'NACK/11/Invalid Destination Address'
      return SendLogDeliveryError.bronze_error_nack_11 if channel.bronze?
      return SendLogDeliveryError.gold_error_nack_11 if channel.gold? or channel.megafon_fixed? or channel.megafon_multi?
    end

    if ['ACK/', 'NACK/', nil, ''].include? ack
      return SendLogDeliveryError.bronze_error_unknown if channel.bronze?
      return SendLogDeliveryError.gold_error_unknown if channel.gold? or channel.megafon_fixed? or channel.megafon_multi?
    end

    if ack[0..2] == 'id:'
      if ack.include? 'stat:DELIVRD'
        return SendLogDeliveryError.bronze_no_error if channel.bronze?
        return SendLogDeliveryError.gold_no_error if channel.gold? or channel.megafon_fixed? or channel.megafon_multi?
      end

      if ack.include?('stat:UNDELIV') or ack.include?('stat:REJECTD')
        if channel.bronze?
          error = SendLogDeliveryError.find_by(code: ack.split.select { |e| e[0..3] == 'err:' }.first.split(':').last.to_s.downcase, channel_id: channel.id)
          return (error.nil? or error == SendLogDeliveryError.bronze_no_error) ? SendLogDeliveryError.bronze_error_unknown : error
        end

        if channel.gold? or channel.megafon_fixed? or channel.megafon_multi?
          error = SendLogDeliveryError.find_by(code: ack.split.select { |e| e[0..3] == 'err:' }.first.split(':').last.to_s.downcase, channel_id: Channel.gold)
          return (error.nil? or error == SendLogDeliveryError.gold_no_error) ? SendLogDeliveryError.gold_error_unknown : error
        end
      end

      if ack.include?('stat:EXPIRED')
        if channel.bronze?
          #bronze
          error = SendLogDeliveryError.find_by(code: ack.split.select { |e| e[0..3] == 'err:' }.first.split(':').last.to_s.downcase, channel_id: channel.id)
          return (error.nil? or error == SendLogDeliveryError.bronze_no_error) ? SendLogDeliveryError.bronze_expired : error
        end

        if channel.gold? or channel.megafon_fixed? or channel.megafon_multi?
          #gold
          error = SendLogDeliveryError.find_by(code: ack.split.select { |e| e[0..3] == 'err:' }.first.split(':').last.to_s.downcase, channel_id: Channel.gold)
          return (error.nil? or error == SendLogDeliveryError.gold_no_error) ? SendLogDeliveryError.gold_expired : error
        end
      end
    end

    if ack[0..4] == 'NACK/'
      if channel.bronze?
        error = SendLogDeliveryError.find_by(code: ack.split('/')[1].to_s.downcase, channel_id: channel.id)
        return error.nil? ? SendLogDeliveryError.bronze_error_unknown : error
      end

      if channel.gold? or channel.megafon_fixed? or channel.megafon_multi?
        error = SendLogDeliveryError.find_by(code: ack.split('/')[1].to_s.downcase, channel_id: Channel.gold)
        return error.nil? ? SendLogDeliveryError.gold_error_unknown : error
      end
    end

    #код ошибки - по умолчанию
    return SendLogDeliveryError.bronze_error_unknown if channel.bronze?
    return SendLogDeliveryError.gold_error_unknown if channel.gold? or channel.megafon_fixed? or channel.megafon_multi?
  end
end
