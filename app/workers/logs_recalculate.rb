# -*- encoding : utf-8 -*-
require 'sneakers'
require 'json'

class LogsRecalculate
  include Sneakers::Worker
  from_queue Calculator::Logs.queue

  def work(msg)
    begin_date, end_date = JSON.parse(msg).values
    Calculator::Logs.recalculate(begin_date.to_date, end_date.to_date)
    ack!
  end
end
