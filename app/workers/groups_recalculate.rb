# -*- encoding : utf-8 -*-
require 'sneakers'
require 'json'

class GroupsRecalculate
  include Sneakers::Worker
  from_queue Calculator::Groups.queue

  def work(msg)
    group_ids = JSON.parse(msg)["group_ids"]
    Calculator::Groups.recalculate(group_ids)
    ack!
  end
end
