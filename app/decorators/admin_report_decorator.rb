# -*- encoding : utf-8 -*-
class AdminReportDecorator < ApplicationDecorator

  def begin_date
    check_date(begin_date_from_params, Date.today.at_beginning_of_month)
  end

  def end_date
    check_date(end_date_from_params, Date.today.at_end_of_month)
  end

  def sanitize_params
    h.params.permit(:report => [:begin_date, :end_date])[:report]
  end

  def submit_button
    h.content_tag(:input, nil, class: [:button, :full_width ], value: I18n.t('report.generate'), type: :submit)
  end

  def begin_datefield
    datefield(:begin_date, begin_date, I18n.t('report.begin_date'))
  end

  def end_datefield
    datefield(:end_date, end_date, I18n.t('report.end_date'))
  end

  def datepicker
    h.javascript_tag(h.render :partial => 'autodelivery/js/datepicker', :formats => [:js])
  end

  private
  def to_zero_procent(success_sms_sent, sms_sent)
    if sms_sent.to_i >= 1
      return (success_sms_sent.to_f/sms_sent.to_f * 100).round(0)
    end
    0
  end

  def price_per_sms(price, sms_sent)
    return price.to_f.round(2) if sms_sent.to_i == 0
    (price.to_f.round(2)/sms_sent.to_i).round(2)
  end

  def datefield(name, value, placeholder)
    h.text_field :report, name, {value: value, datepicker: :true, placeholder: placeholder}
  end

  def begin_date_from_params
    Date.parse(sanitize_params[:begin_date]) rescue nil
  end

  def end_date_from_params
    Date.parse(sanitize_params[:end_date]) rescue nil
  end

  def check_date(checking_date, default_date)
    if begin_date_from_params.present? and end_date_from_params.present? and begin_date_from_params <= end_date_from_params
      return checking_date
    end

    default_date
  end

end
