# -*- encoding : utf-8 -*-
class AdminReportChargesDecorator < AdminReportDecorator

  def xls_button
    h.link_to I18n.t('report.load_in_excel'), h.admin_report_charges_xls_path(report: {begin_date: begin_date, end_date: end_date}), class: [:button, :full_width]
  end

  def title_tab_for_channel(channel)
    h.content_tag(:dd, h.link_to("Канал: #{channel.name.capitalize}", "##{channel.internal_name}"), class: channel_title_class(channel))
  end

  def content_tab_for_channel(channel)
    h.content_tag(:li, content_for_channel(channel), class: channel_content_class(channel), id: "#{channel.internal_name}Tab")
  end

  def total(channels, reports)
    _total = {}

    channels.each do |channel|
      _total[channel] = { 
        sms_sent:         sms_sent_from(channel, reports),
        success_sms_sent: success_sms_sent_from(channel, reports),
        price:            price_from(channel, reports),
        percent:          percent_from(channel, reports),
        price_per_sms:    price_per_sms_from(channel, reports)
      }
    end

    _total
  end

  private
  def sms_sent_from(channel, reports)
    reports.map { |report| report[:stats][channel][:sms_sent] }.sum
  end

  def success_sms_sent_from(channel, reports)
    reports.map { |report| report[:stats][channel][:success_sms_sent] }.sum
  end

  def price_from(channel, reports)
    reports.map { |report| report[:stats][channel][:price] }.sum
  end

  def percent_from(channel, reports)
    to_zero_procent(success_sms_sent_from(channel, reports), sms_sent_from(channel, reports))
  end

  def price_per_sms_from(channel, reports)
    price_per_sms(price_from(channel, reports), sms_sent_from(channel, reports))
  end

  def channel_title_class(channel)
    if channel.gold?
      'active'
    else
      'hide-for-small'
    end
  end

  def channel_content_class(channel)
    if channel.gold?
      'active'
    else
      nil
    end
  end

  def content_for_channel(channel)
    h.render partial: 'admin/report/charges/tab_table', locals: {channel: channel}
  end

end
