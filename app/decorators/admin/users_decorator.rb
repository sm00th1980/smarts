# -*- encoding : utf-8 -*-
class Admin::UsersDecorator < ApplicationDecorator
  attr_reader :type, :status

  def initialize(params)
    @params = params

    @type = find_type
    @status = find_status
  end

  def create_button
    h.link_to 'Добавить пользователя', h.new_user_path, class: [:button]
  end

  def select_for_type
    h.select :user, :type, types.map { |s| [s[:name], s[:id]] }, selected: sanitize_type
  end

  def select_for_status
    h.select :user, :status, statuses.map { |s| [s[:name], s[:id]] }, selected: sanitize_status
  end

  def login_as_him(user)
    h.link_to user.email, h.switch_user_path(scope_identifier: "user_#{user.id}"), title: 'Войти как он'
  end

  def status_for_view(user)
    if user.status.deleted?
      h.content_tag(:span, "Удалён: #{user.deleted_at.strftime('%Y-%m-%d %H:%M:%S')}", class: [:alert, :label])
    else
      if user.status.blocked?
        h.content_tag(:span, "Заблокирован: #{user.blocked_at.strftime('%Y-%m-%d %H:%M:%S')}", class: [:alert, :label])
      else
        h.content_tag(:span, "Разблокирован", class: [:success, :label])
      end
    end
  end

  def permit_alpha_sender_name(user)
    if user.permit_alpha_sender_name?
      'Разрешено'
    else
      'Запрещено'
    end
  end

  def permit_def_sender_name(user)
    if user.permit_def_sender_name?
      'Разрешено'
    else
      'Запрещено'
    end
  end

  def created_at(user)
    user.created_at.strftime('%Y-%m-%d')
  end

  private
  def sanitize_params
    @params.permit(user: [:status, :type])[:user]
  end

  def sanitize_type
    sanitize_params[:type] rescue :all
  end

  def sanitize_status
    sanitize_params[:status] rescue UserStatus.active.id
  end

  def find_type
    _type = UserType.find_by(id: sanitize_type)

    #любой тип пользователя
    _type ||= UserType.pluck(:id)
  end

  def find_status
    _status = UserStatus.find_by(id: sanitize_status)

    #любой статус пользователя
    _status ||= UserStatus.pluck(:id)
  end

  def types
    [
        {id: :all, name: 'Все типы'},
        {id: UserType.business.id, name: UserType.business.name},
        {id: UserType.demo.id, name: UserType.demo.name},
        {id: UserType.operator.id, name: UserType.operator.name}
    ]
  end

  def statuses
    [
        {id: :all, name: 'Все статусы'},
        {id: UserStatus.active.id, name: UserStatus.active.name},
        {id: UserStatus.blocked.id, name: UserStatus.blocked.name},
        {id: UserStatus.deleted.id, name: UserStatus.deleted.name}
    ]
  end

end
