# -*- encoding : utf-8 -*-
class AdminReportPaymentsDecorator < Draper::Decorator
  delegate_all

  def approved_at
    I18n.l(model.approved_at, :format => '%d %B %Y, %H:%M:%S') if model.approved_at
  end

  def user_email
    "#{model.user.email}" if model.user
  end

  def user_fio
    "#{model.user.fio}" if model.user
  end

  def service_name
    model.service.name
  end

  def amount
    h.round_currency(model.amount)
  end

  def total
    'vfdvf'
  end

end
