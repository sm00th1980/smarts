# -*- encoding : utf-8 -*-
class AdminReportChargesXlsDecorator < AdminReportDecorator

  def total(channels, operators, reports)
    _total = {}

    channels.each do |channel|
      _total[channel] = {}
      operators.each do |operator|
        _total[channel][operator] = {
            sms_sent:         sms_sent_from(channel, operator, reports),
            success_sms_sent: success_sms_sent_from(channel, operator, reports),
            price:            price_from(channel, operator, reports),
            percent:          percent_from(channel, operator, reports),
            price_per_sms:    price_per_sms_from(channel, operator, reports)
        }
      end
    end

    _total
  end

  private
  def sms_sent_from(channel, operator, reports)
    reports.map { |report| report[:stats][channel][operator][:sms_sent] }.sum
  end

  def success_sms_sent_from(channel, operator, reports)
    reports.map { |report| report[:stats][channel][operator][:success_sms_sent] }.sum
  end

  def price_from(channel, operator, reports)
    reports.map { |report| report[:stats][channel][operator][:price] }.sum
  end

  def percent_from(channel, operator, reports)
    to_zero_procent(success_sms_sent_from(channel, operator, reports), sms_sent_from(channel, operator, reports))
  end

  def price_per_sms_from(channel, operator, reports)
    price_per_sms(price_from(channel, operator, reports), sms_sent_from(channel, operator, reports))
  end

end
