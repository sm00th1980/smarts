# -*- encoding : utf-8 -*-
class AdminOperatorTariffDecorator < ApplicationDecorator

  def initialize(params)
    @params = params

    @operator_id = parse_operator_id
  end

  def begin_date
    Date.parse(sanitize_params[:operator_tariff][:begin_date]) rescue nil
  end

  def end_date
    Date.parse(sanitize_params[:operator_tariff][:end_date]) rescue nil
  end

  def price_per_sms
    sanitize_params[:operator_tariff][:price_per_sms].to_f rescue nil
  end

  def operator
    CellOperatorGroup.find_by_id(sanitize_params[:operator_id])
  end

  def operator_tariff
    OperatorTariff.find_by_id(sanitize_params[:operator_tariff_id])
  end

  def operator_id
    @operator_id
  end

  def sanitize_params
    @params.permit(:operator_id, :operator_tariff_id, :operator_tariff => [:begin_date, :end_date, :price_per_sms])
  end

  def create_button
    h.content_tag(:input, nil, class: [:button, :full_width], value: "Создать новый тариф", type: :submit)
  end

  def update_button
    h.content_tag(:input, nil, class: [:button, :full_width], value: "Сохранить тариф", type: :submit)
  end

  def begin_datefield(form)
    datefield(form, :begin_date, operator_tariff.present? ? operator_tariff.begin_date : nil, 'Дата начала действия тарифа')
  end

  def end_datefield(form)
    datefield(form, :end_date, operator_tariff.present? ? operator_tariff.end_date : nil, 'Дата окончания действия тарифа')
  end

  def datepicker
    h.javascript_tag(h.render :partial => 'autodelivery/js/datepicker', :formats => [:js])
  end

  def price_per_sms_field(form)
    h.text_field form.object_name, :price_per_sms, {value: operator_tariff.present? ? operator_tariff.price_per_sms : nil}
  end

  private
  def datefield(form, name, value, placeholder)
    h.text_field form.object_name, name, {value: value, datepicker: :true, placeholder: placeholder}
  end

  def parse_operator_id
    if operator_tariff.present?
      operator_tariff.operator.id
    else
      operator.id if operator.present?
    end
  end

end
