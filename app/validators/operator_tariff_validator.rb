# -*- encoding : utf-8 -*-
class OperatorTariffValidator < ActiveModel::Validator

  def validate(record)
    validate_dates(record)
    validate_price_per_sms(record)
    validate_cross_tariff(record)
  end

  private
  def cross_tariff_error(new_tariff)
    new_tariff.errors.add(:base, I18n.t('validation.operator_tariff.cross_tariff'))
  end

  def validate_cross_tariff(new_tariff)

    #проверяем наличие незакрытого тарифа для данного оператора
    old_tariffs_without_end_date(new_tariff).find_each do |old_tariff|
      if new_tariff.begin_date < old_tariff.begin_date and new_tariff.end_date.present? and new_tariff.end_date < old_tariff.begin_date
        #nothing to do
      else
        cross_tariff_error(new_tariff)
      end
    end

    #проверям наличие закрытого ранее тарифа для данного оператора
    old_tariffs_with_end_date(new_tariff).find_each do |old_tariff|
      if new_tariff.end_date.nil?
        if new_tariff.begin_date <= old_tariff.end_date
          cross_tariff_error(new_tariff)
        end
      else
        if new_tariff.begin_date > old_tariff.end_date or new_tariff.end_date < old_tariff.begin_date
          #nothing to do
        else
          cross_tariff_error(new_tariff)
        end
      end
    end

  end

  def validate_dates(new_tariff)
    if new_tariff.begin_date.present? and new_tariff.end_date.present? and new_tariff.begin_date > new_tariff.end_date
      new_tariff.errors.add(:base, I18n.t('validation.operator_tariff.end_date_is_ealier_than_begin_date'))
    end
  end

  def validate_price_per_sms(new_tariff)
    if new_tariff.price_per_sms.blank? or new_tariff.price_per_sms.to_f <= 0
      new_tariff.errors.add(:base, I18n.t('validation.operator_tariff.price_per_sms_should_be_positive'))
    end
  end

  def old_tariffs_with_end_date(tariff)
    old_tariffs(tariff, false)
  end

  def old_tariffs_without_end_date(tariff)
    old_tariffs(tariff, true)
  end

  def old_tariffs(new_tariff, without_end_date)
    sql = lambda { OperatorTariff.where(operator_id: new_tariff.operator).where('begin_date is not null') }
    if without_end_date
      _old_tariffs = lambda { sql.call.where(end_date: nil) }
    else
      _old_tariffs = lambda { sql.call.where('end_date is not null') }
    end

    if new_tariff.new_record?
      _old_tariffs.call
    else
      _old_tariffs.call.where('id <> ?', new_tariff.id)
    end
  end

end
