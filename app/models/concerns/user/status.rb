# -*- encoding : utf-8 -*-
module User::Status
  extend ActiveSupport::Concern

  included do
    after_save :update_status
  end

  def update_status
    if status.blocked?
      self.update_column(:blocked_at, Time.zone.now)
      self.update_column(:deleted_at, nil)
    end

    if status.deleted?
      self.update_column(:blocked_at, nil)
      self.update_column(:deleted_at, Time.zone.now)
    end

    if status.active?
      self.update_column(:blocked_at, nil)
      self.update_column(:deleted_at, nil)
    end

  end

end
