# -*- encoding : utf-8 -*-
class BirthdayCongratulation

  DEFAULT_BIRTHDAY_MESSAGE = 'Поздравляем с Днём Рождения! Желаем счастья, здоровья!'

  def self.run
    #запуск поздравления с ДР
    Client.with_today_birthday.each do |client|
      if client.group.user.reload.status.active?
        if Time.now.hour == client.group.user.birthday_hour
          SMSService.send_sms(client.group.user, birthday_message(client), {:phones => [client.phone]}, client.group.user.birthday_sender_name, false, LogType.autodelivery)
        end
      end
    end
  end

  def self.birthday_message(client)
    _birthday_message = DEFAULT_BIRTHDAY_MESSAGE
    if client.group.user.birthday_message.present?
      _birthday_message = replace_percent_chars(client.group.user.birthday_message) % {fio: client.fio, percent: '%'}
    end

    _birthday_message
  end

  def self.replace_percent_chars(str)
    new_str = ''
    index = 0
    str.each_char do |current_char|
      next_char = str[index+1]
      if current_char == '%' and next_char != '{'
        new_str << '%{percent}'
      else
        new_str << current_char
      end

      index += 1
    end

    new_str
  end
end
