# -*- encoding : utf-8 -*-
class PaymentService < ActiveRecord::Base
  attr_accessible :name, :internal_name
  has_one :payment

  def self.manual
    find_or_create_by(name: 'внесен администратором', internal_name: :manual)
  end

  def self.robokassa
    find_or_create_by(name: 'пришёл от робокассы', internal_name: :robokassa)
  end

  def self.bank
    find_or_create_by(name: 'через расчётный счёт', internal_name: :bank)
  end

  def self.available_for_admin
    [manual, bank]
  end

end
