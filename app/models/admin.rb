# -*- encoding : utf-8 -*-
class Admin < ActionMailer::Base
  include Sidekiq::Worker

  def notify(params)
    emails = []
    Rails.configuration.admin_emails.each do |admin_email|
      emails << Mailer.send_email(admin_email, Rails.configuration.mail_sender, I18n.t('admin.subject') % Rails.configuration.site_name, params[:event], :notify_admin).deliver_now
    end

    emails
  end
end
