# -*- encoding : utf-8 -*-
class OperatorTariff < ActiveRecord::Base
  include ActiveModel::Validations

  attr_accessible :operator, :price_per_sms, :begin_date, :end_date

  belongs_to :operator, foreign_key: :operator_id, class_name: 'CellOperatorGroup'

  validates :operator, :price_per_sms, :begin_date, presence: true
  validates_with OperatorTariffValidator

  scope :with_closed_end_date, -> { where('end_date is not null') }
  scope :with_opened_end_date, -> { where('end_date is null') }

  def price_per_sms
    super.to_f.round(2)
  end

  after_save :recalculate_group
  after_destroy :recalculate_group

  private
  def recalculate_group
    ActiveSupport::Notifications.instrument('group.recalculate.required', group_ids: Group.pluck(:id))
  end

end
