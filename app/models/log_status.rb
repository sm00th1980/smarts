# -*- encoding : utf-8 -*-
class LogStatus < ActiveRecord::Base
  attr_accessible :name, :internal_name
  has_one :log

  def self.fresh
    find_or_create_by(name: 'Новая рассылка', internal_name: :new)
  end

  def self.checking
    find_or_create_by(name: 'Авто-проверка', internal_name: :checking)
  end

  def self.ready_for_processing
    find_or_create_by(name: 'Готова к запуску', internal_name: :ready_for_processing)
  end

  def self.processing
    find_or_create_by(name: 'Запущена', internal_name: :processing)
  end

  def self.completed
    find_or_create_by(name: 'Завершено', internal_name: :completed)
  end

  def self.check_failed
    find_or_create_by(name: 'Ошибка при проверке, запуск не возможен', internal_name: :check_failed)
  end

  def self.stopped
    find_or_create_by(name: 'Остановлена', internal_name: :stopped)
  end

end
