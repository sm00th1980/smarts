# -*- encoding : utf-8 -*-
class UserStatus < ActiveRecord::Base
  attr_accessible :name, :internal_name
  has_one :user

  validates :name, :internal_name, presence: true

  def self.active
    find_or_create_by(name: 'Активный', internal_name: :active)
  end

  def self.blocked
    find_or_create_by(name: 'Заблокированный', internal_name: :blocked)
  end

  def self.deleted
    find_or_create_by(name: 'Удалённый', internal_name: :deleted)
  end

  def active?
    self == self.class.active
  end

  def blocked?
    self == self.class.blocked
  end

  def deleted?
    self == self.class.deleted
  end

end
