# -*- encoding : utf-8 -*-
class LogValidPhones < ActiveRecord::Base
  attr_accessible :log_id, :valid_phone

  belongs_to :log

  def self.clean
    ActiveRecord::Base.connection.execute("delete from log_valid_phones where date(created_at) < '#{Date.today}'::date");
  end
end
