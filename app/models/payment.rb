# -*- encoding : utf-8 -*-
class Payment < ActiveRecord::Base
  belongs_to :user

  attr_accessible :user, :amount, :service, :date, :approved_at, :failed_at

  validates_presence_of :date
  validates_presence_of :user
  validates_presence_of :amount

  belongs_to :service, :class_name => 'PaymentService'

  scope :approved, -> { where('approved_at is not null and failed_at is null') }

  def approve!(admin=false)
    if admin
      update_attribute(:approved_at, Time.now)
    else
      if not user.demo? and failed_at.nil?
        update_attribute(:approved_at, Time.now)
      end
    end
  end

  def fail!
    update_attribute(:failed_at, Time.now)
  end

  def approved?
    if approved_at.present? and failed_at.nil?
      return true
    end

    false
  end

  def failed?
    if failed_at.nil?
      return false
    end

    true
  end

  def new?
    if approved? or failed?
      return false
    end

    true
  end


end
