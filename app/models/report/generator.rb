# -*- encoding : utf-8 -*-
require 'csv'
require 'zip'

class Report::Generator
  REPORT_DIR = 'tmp/reports'
  ENCODING = 'cp1251'
  COLUMN_SEPARATOR = '|'
  FILENAME_PREFIX = 'report_delivery_ID'
  TIME_FORMAT = '%Y-%m-%d, %H:%M:%S'

  def initialize(log)
    @log = log
  end

  def save_report
    create_dir
    CSV.open(path, "wb", encoding: ENCODING, col_sep: COLUMN_SEPARATOR) do |csv|
      raw_data.each { |row| csv << row }
    end

    #архивируем отчёт
    FileUtils.rm(zippath) if File.exists?(zippath)
    Zip::File.open(zippath, Zip::File::CREATE) {|zipfile| zipfile.add(name, path)}

    #удаляем отчёт
    FileUtils.rm(path)

    {name: zipname, path: zippath}
  end

  def path
    Rails.root.join(REPORT_DIR, name)
  end

  def zippath
    "#{path}.zip"
  end

  def report_dir
    Rails.root.join(REPORT_DIR)
  end

  def header
    ['Дата отправки', 'Телефон получателя', 'Текст сообщения', 'Имя отправителя', 'Стоимость', 'Количество СМС', 'Оператор', 'Статус доставки', 'Ошибка при доставке']
  end

  private
  def raw_data
    rows = []
    send_logs.each do |send_log|
      created_at = send_log.created_at.strftime(TIME_FORMAT)
      phone = send_log.phone
      text = safe_text(send_log.log.content)
      sender_name = send_log.log.sender_name_value
      price = send_log.price
      sms_count = send_log.log.sms_count
      cell_operator = send_log.cell_operator_group.name
      status = send_log.delivery_status.name
      error = send_log.delivery_error.name

      rows << [created_at, phone, text, sender_name, price, sms_count, cell_operator, status, error].map { |el| el.to_s }
    end

    rows.prepend(header)
  end

  def send_logs
    SendLog.where(log_id: @log.id).order(:created_at)
  end

  def create_dir
    #создание директории для генерируемых отчётов
    Dir.mkdir(report_dir) unless File.exists?(report_dir)
  end

  def name
    "#{FILENAME_PREFIX}#{@log.id}.csv"
  end

  def zipname
    "#{name}.zip"
  end

  def safe_text(text)
    #вырезаем разделитель колонок и перевод строки
    text.delete(COLUMN_SEPARATOR).delete("\n").delete("\r\n")
  end

end
