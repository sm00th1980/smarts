# -*- encoding : utf-8 -*-
class Report::GeneratorByDate < Report::Generator
  FILENAME_PREFIX = 'report_delivery_'

  def initialize(user, start_date, end_date)
    @log_ids, @start_date, @end_date = user.logs.pluck(:id), start_date, end_date
  end

  private
  def send_logs
    SendLog.where(log_id: @log_ids).where('? <= date(created_at) and date(created_at) <= ?', @start_date, @end_date).order(:created_at)
  end

  def name
    "#{FILENAME_PREFIX}#{@start_date}-#{@end_date}.csv"
  end

end
