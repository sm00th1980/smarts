# -*- encoding : utf-8 -*-
class Report::Manager < ActionMailer::Base
  include Sidekiq::Worker

  def generate(log)
    Mailer.send_report(log.user.contact_email, Report::Generator.new(log).save_report).deliver_now
  end

  def generate_by_date(user, start_date, end_date)
    Mailer.send_report(user.contact_email, Report::GeneratorByDate.new(user, start_date, end_date).save_report).deliver_now
  end

end
