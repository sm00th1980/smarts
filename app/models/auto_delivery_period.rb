# -*- encoding : utf-8 -*-
class AutoDeliveryPeriod < ActiveRecord::Base
  attr_accessible :name, :period

  has_many :auto_deliveries

  def every_hour?
    return true if period == 'every_hour'
    false
  end

  def every_day?
    return true if period == 'every_day'
    false
  end

  def every_week?
    return true if period == 'every_week'
    false
  end

  def self.every_day
    find_or_create_by(name: 'Каждый день', period: :every_day)
  end

  def self.every_hour
    find_or_create_by(name: 'Каждый час', period: :every_hour)
  end

  def self.every_week
    find_or_create_by(name: 'Каждую неделю', period: :every_week)
  end

end
