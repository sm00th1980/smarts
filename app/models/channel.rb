# -*- encoding : utf-8 -*-
class Channel < ActiveRecord::Base
  extend PhoneHelper
  include Service::Channel::Type

  attr_accessible :name, :internal_name

  has_many :send_logs
  has_many :log_stats
  has_one :send_log_delivery_error
  has_many :users


  def configuration
    return Rails.configuration.kannel[:gold] if gold?
    return Rails.configuration.kannel[:bronze] if bronze?
    return Rails.configuration.kannel[:megafon_fixed] if megafon_fixed?
    return Rails.configuration.kannel[:megafon_multi] if megafon_multi?

    {}
  end

  def self.channel(sender_name_value, channel, phone)
    #если sender_name - alpha-numeric - это gold channel или один из 2х каналов мегафона
    #если sender_name - федеральный номер - это бронзовый канал
    #если sender_name невалидно - возвращаем nil(неизвестный канал для отправки)

    if SenderName.def?(sender_name_value)
      return bronze
    end

    if SenderName.alpha?(sender_name_value)
      if CellOperatorGroup.get_by_phone(phone).megafon? and not channel.bronze?
        return channel
      end

      return gold
    end

    unknown
  end

end
