# -*- encoding : utf-8 -*-
class LogStatManager
  extend LogStatHelper

  def self.recreate_log_view
    _month_views = ActiveRecord::Base.connection.execute("SELECT matviewname FROM pg_matviews ORDER BY 1").map { |r| r["matviewname"] }

    sql = "CREATE OR REPLACE VIEW log_stats AS "
    sql << _month_views.map { |month_view| "SELECT * FROM #{month_view}" }.join(" UNION ALL ")

    ActiveRecord::Base.connection.execute(sql)
  end

  def self.recreate_month_view(date)
    if not exists_month_view?(date)
      _first_date = date.at_beginning_of_month.strftime('%Y-%m-%d')
      _last_date = date.at_end_of_month.strftime('%Y-%m-%d')

      sql ="
        CREATE MATERIALIZED VIEW #{month_view_name(date)} AS
        SELECT
	        date(logs.created_at) AS \"date\",
	        user_id,
	        SUM(round(send_logs.price::numeric,2)) AS \"price\",
	        cell_operator_group_id,
	        channel_id,
	        SUM(CASE WHEN delivery_status_id = #{SendLogDeliveryStatus.delivered.id} THEN sms_count ELSE 0 END) AS sms_sent_success,
	        SUM(CASE WHEN delivery_status_id <> #{SendLogDeliveryStatus.delivered.id} THEN sms_count ELSE 0 END) AS sms_sent_failure
        FROM
	        logs, send_logs
        WHERE
          send_logs.price > 0 AND
	        logs.id = send_logs.log_id AND
	        date(logs.created_at) >= '#{_first_date}'::date AND date(logs.created_at) <= '#{_last_date}'::date
        GROUP BY
	        date(logs.created_at),
	        user_id,
	        cell_operator_group_id,
	        channel_id
      "
    else
      sql = "REFRESH MATERIALIZED VIEW #{month_view_name(date)}"
    end

    ActiveRecord::Base.connection.execute(sql)
  end

  def self.prepare(dates)
    dates.map { |d| d.at_beginning_of_month }.uniq.sort.each do |date|
      recreate_month_view(date)
    end

    recreate_log_view
  end

  def self.exists_month_view?(date)
    _count = ActiveRecord::Base.connection.execute("SELECT count(*) FROM pg_matviews WHERE matviewname = '%s'" % month_view_name(date))[0]["count"].to_i
    if _count == 0
      return false
    end

    true
  end

  def self.month_view_name(date)
    "log_stat_#{full_month(date)}"
  end

end
