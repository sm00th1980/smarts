# -*- encoding : utf-8 -*-
class Message
  CHUNK_LENGTH_ENG = 160
  CHUNK_LENGTH_RUS = 140

  CODING_UTF8 = 2
  CODING_ASCII = 1

  def self.sms_count(text)
    if text.present?
      _text = text.gsub("\r\n", "\n")

      if has_non_ascii_letter?(text)
        (_text.encode("UTF-16BE").force_encoding("BINARY").bytesize/CHUNK_LENGTH_RUS.to_f).ceil
      else
        (_text.encode("ASCII").force_encoding("BINARY").bytesize/CHUNK_LENGTH_ENG.to_f).ceil
      end
    else
      0
    end
  end

  def self.coding(text)
    CODING_UTF8
  end

  def self.price_per_sms(channel, user, phone)
    _price = 0

    if channel.gold? or channel.megafon_fixed? or channel.megafon_multi? or user.charge_by_sent?
      _price = calc_price_per_sms(channel, user, phone)
    end

    _price.round(2)
  end

  def self.calc_price_per_sms(channel, user, phone)
    _price = 0

    if Channel.exists?(id: channel)
      if channel.gold? or channel.megafon_fixed? or channel.megafon_multi?
        _price = CellOperatorGroup.get_by_phone(phone).price_per_sms
      end

      _price = user.price_per_sms if channel.bronze?
    end

    _price.round(2)
  end

  def self.has_non_ascii_letter?(text)
    if text.present?
      begin
        text.encode("ASCII")
      rescue
        return true
      end
    end

    false
  end

end
