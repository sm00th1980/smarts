# -*- encoding : utf-8 -*-

class SMSService < ActionMailer::Base
  MAX_PHONES = 10 #максимальное число допустимых номеров телефонов пришедшее в phones

  include PhoneHelper
  include Sidekiq::Worker

  def self.send_sms(user, content, receivers, sender_name_value, fake, log_type, prev_log_id=nil)
    phones = (receivers.has_key? :phones and receivers[:phones].present?) ? receivers[:phones] : []
    group_ids = (receivers.has_key? :group_ids and receivers[:group_ids].present?) ? receivers[:group_ids] : []

    log = Log.create!(
        user: user,
        phones: phones,
        content: content,
        group_ids: group_ids,
        sender_name_value: sender_name_value,
        fake: fake,
        status: LogStatus.fresh,
        type: log_type,
        prev_log_id: prev_log_id,
        sms_count: Message.sms_count(content)
    )

    #запускаем нотификацию для реальной рассылки
    ActiveSupport::Notifications.instrument('sms.send', log: log)

    log
  end

  def self.max_phones
    MAX_PHONES
  end

  #async processing
  def process_fresh(log)
    log.process
  end
end
