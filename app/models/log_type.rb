# -*- encoding : utf-8 -*-
class LogType < ActiveRecord::Base
  attr_accessible :name, :internal_name
  has_one :log

  def self.api
    find_or_create_by(name: 'Через API', internal_name: 'api')
  end

  def self.manual
    find_or_create_by(name: 'Через веб интерфейс', internal_name: 'manual')
  end

  def self.autodelivery
    find_or_create_by(name: 'Через авто-рассылку', internal_name: 'autodelivery')
  end

  def self.by_not_delivered
    find_or_create_by(name: 'По недоставленным', internal_name: 'by_not_delivered')
  end

  def api?
    self == self.class.api
  end
end
