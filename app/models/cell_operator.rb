# -*- encoding : utf-8 -*-
class CellOperator < ActiveRecord::Base

  has_many :ranges, :class_name => 'CellOperatorPhoneRange'
  belongs_to :group, :class_name => 'CellOperatorGroup', :foreign_key => :cell_operator_group_id

end
