# -*- encoding : utf-8 -*-
class Client < ActiveRecord::Base
  include PhoneHelper

  attr_accessible :phone, :group_id, :fio, :birthday

  belongs_to :group

  scope :with_today_birthday, -> { where(birthday_congratulation: true).where('extract(month from birthday) = ? and extract(day from birthday) = ?', Date.today.month, Date.today.day) }

  def self.search query
    self.where('LOWER(fio) LIKE LOWER(?) OR phone::text LIKE ?', "%#{query}%", "#{query}%")
  end

  def birthday_valid?
    if birthday.present? and birthday.is_a?(Date)
      return true
    end

    false
  end

  after_save :recalculate_group
  after_destroy :recalculate_group

  private
  def recalculate_group
    ActiveSupport::Notifications.instrument('group.recalculate.required', group_ids: [self.group.id])
  end

end
