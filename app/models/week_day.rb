# -*- encoding : utf-8 -*-
class WeekDay < ActiveRecord::Base
  attr_accessible :name, :eng_name

  has_many :auto_deliveries

  def self.monday
    find_or_create_by(eng_name: :monday)
  end

  def self.tuesday
    find_or_create_by(eng_name: :tuesday)
  end

  def self.wednesday
    find_or_create_by(eng_name: :wednesday)
  end

  def self.thursday
    find_or_create_by(eng_name: :thursday)
  end

  def self.friday
    find_or_create_by(eng_name: :friday)
  end

  def self.saturday
    find_or_create_by(eng_name: :saturday)
  end

  def self.sunday
    find_or_create_by(eng_name: :sunday)
  end

  def self.days
    [monday, tuesday, wednesday, thursday, friday, saturday, sunday]
  end

end
