# -*- encoding : utf-8 -*-
class Template < ActiveRecord::Base
  attr_accessible :name, :text, :user
  validates :name, :presence => true

  belongs_to :user
end
