# -*- encoding : utf-8 -*-
class ChargeType < ActiveRecord::Base
  attr_accessible :name, :internal_name
  has_one :user

  def self.by_sent
    find_or_create_by(name: 'По отправке', internal_name: :by_sent)
  end

  def self.by_delivered
    find_or_create_by(name: 'По доставке', internal_name: :by_delivered)
  end

end
