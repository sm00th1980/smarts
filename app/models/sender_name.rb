# -*- encoding : utf-8 -*-
class SenderName < ActiveRecord::Base

  belongs_to :user

  attr_accessible :value, :user_id, :status_id, :active

  belongs_to :status, :class_name => 'SenderNameStatus'

  include Service::SenderName::Status
  include Service::SenderName::Moderating
  include Service::SenderName::Activator
  include Service::SenderName::Notificator

  validates :value, :presence => true

  def self.active_sender_name_value(user)
    _sender_names = self.where(:user_id => user.id, :active => true, :status_id => SenderNameStatus.accepted.id).order('created_at ASC')

    if _sender_names.count == 1 and self.verified?(_sender_names.first.value, user)
      return _sender_names.first.value
    end

    if _sender_names.count > 1 and self.verified?(_sender_names.last.value, user)
      return _sender_names.last.value
    end

    nil
  end

  def self.verified?(sender_name_value, user)
    return true if def?(sender_name_value) and user.permit_def_sender_name?
    return true if alpha?(sender_name_value) and user.permit_alpha_sender_name?

    false
  end

  def self.valid_by_value?(sender_name_value, user)
    if verified?(sender_name_value, user)
      if user.accepted_sender_name_values.map { |v| v[:value].to_s }.include? sender_name_value.to_s
        return true
      end
    end

    false
  end

  def self.def?(sender_name_value)
    #для DEF
    #длина = 11 символов
    #должен начинаться только 79

    return true if sender_name_value.to_s.length == 11 and sender_name_value.to_s.scan(/^79\d{9}*$/).join.present?
    false
  end

  def self.alpha?(sender_name_value)
    #ALPHA
    #длина <= 11 символов, не может быть DEF
    #исключаем также все номера на 78, 88, 89
    #могут быть буквы, цифры, символы: - , + , _ , пробел , символ точки

    if sender_name_value.to_s.length == 11
      return false if sender_name_value.to_s.scan(/^78\d{9}*$/).join.present?
      return false if sender_name_value.to_s.scan(/^88\d{9}*$/).join.present?
      return false if sender_name_value.to_s.scan(/^89\d{9}*$/).join.present?
    end

    if not def?(sender_name_value) and sender_name_value.to_s.length <= 11 and sender_name_value.to_s.scan(/^[a-zA-Z\-\_\+\.\d\s]*$/).join.present?
      return true
    end

    false
  end
end
