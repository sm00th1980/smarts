# -*- encoding : utf-8 -*-
class LogStat < ActiveRecord::Base
  attr_accessible :date, :cell_operator_group, :price, :sms_sent_success, :sms_sent_failure

  default_scope { order(:date) }

  belongs_to :cell_operator_group
  belongs_to :user
  belongs_to :channel

  extend Service::LogStat::Report

  def self.calculate(begin_date, end_date)
    LogStatManager.prepare (begin_date..end_date)
  end

end
