# -*- encoding : utf-8 -*-
class SendLogDeliveryStatus < ActiveRecord::Base
  attr_accessible :name, :internal_name, :kannel_id

  #1: delivery success, 2: delivery failure, 4: message buffered, 8: smsc submit, 16: smsc reject

  def self.reject_to_mts
    find_or_create_by(name: 'Рассылка на МТС запрещена.', internal_name: 'reject to mts', kannel_id: -5)
  end

  def self.reject_to_smarts
    find_or_create_by(name: 'Рассылка на Смартс запрещена.', internal_name: 'reject to smarts', kannel_id: -4)
  end

  def self.reject_to_megafon
    find_or_create_by(name: 'Рассылка на Мегафон запрещена.', internal_name: 'reject to megafon', kannel_id: -3)
  end

  def self.low_balance
    find_or_create_by(name: 'Сообщение не отослано. Текущий баланс слишком низкий.', internal_name: 'low balance', kannel_id: -2)
  end

  def self.unknown
    find_or_create_by(name: 'Сообщение отослано. Статус доставки неизвестен. Не пришло уведомление от СМС центра о доставке, либо уведомление не распознано.', internal_name: 'unknown status', kannel_id: -1)
  end

  def self.delivered
    find_or_create_by(name: 'Доставлено', internal_name: 'delivery success', kannel_id: 1)
  end

  def self.failured
    find_or_create_by(name: 'При доставке возникла ошибка', internal_name: 'delivery failure', kannel_id: 2)
  end

  def self.buffered
    find_or_create_by(name: 'Сообщение находится в очереди отправки СМС центра', internal_name: 'message buffered', kannel_id: 4)
  end

  def self.submit
    find_or_create_by(name: 'Сообщение принято к отправке СМС центром', internal_name: 'smsc submit', kannel_id: 8)
  end

  def self.rejected
    find_or_create_by(name: 'Отвергнуто СМС центром', internal_name: 'smsc reject', kannel_id: 16)
  end

  def self.reject_status_by_operator(operator)
    if operator.mts?
      return reject_to_mts
    end

    if operator.megafon?
      return reject_to_megafon
    end

    if operator.smarts?
      return reject_to_smarts
    end

    nil
  end

end
