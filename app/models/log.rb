# -*- encoding : utf-8 -*-
class Log < ActiveRecord::Base
  include Service::Logs::Status
  include Service::Logs::Validator
  include Service::Logs::Processor
  include Service::Logs::Billing
  include Service::Logs::ByNotDelivered

  #заказы на доставку
  attr_accessible :user,
                  :content,
                  :phones,
                  :status,
                  :group_ids,
                  :sender_name_value,
                  :fake,
                  :price,
                  :check_errors,
                  :started_at,
                  :finished_at,
                  :type,
                  :prev_log_id,
                  :sms_count

  belongs_to :user

  has_many :send_logs
  belongs_to :status, :class_name => 'LogStatus'
  belongs_to :type, :class_name => 'LogType', :foreign_key => :log_type_id

  has_many :valid_phones, :class_name => 'LogValidPhones'
  has_many :stats, class_name: 'LogStat'

  def groups
    Group.where(user_id: user).where(id: group_ids)
  end

  def clients
    groups.map { |group| group.clients }.flatten
  end

  #delete Client with undelivered message
  def delete_clients
    ActiveRecord::Base.connection.delete(
        %Q{
        DELETE
        FROM clients
        USING send_logs, groups
        WHERE (
          send_logs.phone = clients.phone AND
          clients.group_id = groups.id AND
          (
            send_logs.delivery_status_id = #{SendLogDeliveryStatus.failured.id} OR
            send_logs.delivery_status_id = #{SendLogDeliveryStatus.rejected.id} OR
            send_logs.delivery_status_id = #{SendLogDeliveryStatus.unknown.id}
          ) AND
          send_logs.log_id = #{self.id} AND
          groups.user_id = #{self.user.id}
        )
        }
    )
  end

end
