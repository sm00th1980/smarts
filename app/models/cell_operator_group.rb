# -*- encoding : utf-8 -*-
class CellOperatorGroup < ActiveRecord::Base
  extend PhoneHelper
  include Service::CellOperatorGroup::Type

  attr_accessible :name, :internal_name

  has_many :cell_operators
  has_many :send_logs
  has_many :log_stats, class_name: 'LogStat'
  has_many :tariffs, class_name: 'OperatorTariff', foreign_key: 'operator_id'

  def self.get_by_phone(phone)
    _phone = normalize_phone_number(phone)
    if _phone
      range = CellOperatorPhoneRange.where('begin_phone <= ? AND ? <= end_phone', _phone, _phone).first
      if range
        return range.cell_operator.group
      end
    end

    CellOperatorGroup.other
  end

  def self.reject?(user, channel, phone)
    operator = CellOperatorGroup.get_by_phone(phone)

    return true if operator.mts?     and user.reject_to_mts?
    return true if operator.megafon? and user.reject_to_megafon?
    return true if operator.smarts?  and user.reject_to_smarts_via_gold_channel? and channel.gold?

    false
  end

  def price_per_sms(date=Date.today)
    _tariffs_with_closed_end_date = tariffs.with_closed_end_date.where('? between begin_date and end_date', date)
    _tariffs_with_opened_end_date = tariffs.with_opened_end_date.where('? >= begin_date', date)

    if _tariffs_with_closed_end_date.count == 1 and _tariffs_with_opened_end_date.count == 0
      return _tariffs_with_closed_end_date.first.price_per_sms.to_f.round(2)
    end

    if _tariffs_with_closed_end_date.count == 0 and _tariffs_with_opened_end_date.count == 1
      return _tariffs_with_opened_end_date.first.price_per_sms.to_f.round(2)
    end

    0
  end

  after_save :recalculate_groups
  after_destroy :recalculate_groups

  private
  def recalculate_groups
    #запускаем нотификацию для требования запуска перерасчёта по всем группам
    ActiveSupport::Notifications.instrument('group.recalculate.required', group_ids: Group.pluck(:id))
  end

end
