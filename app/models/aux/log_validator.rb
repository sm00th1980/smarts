# -*- encoding : utf-8 -*-
class Aux::LogValidator
  include PhoneHelper

  def initialize(log)
    @log = log
  end

  def log_valid?
    _total_valid_phones = total_valid_phones

    if sender_name_valid? and content_valid? and good_balance? and _total_valid_phones.count > 0 and user_active?
      save_valid_phones(_total_valid_phones)
      return true
    else
      save_errors
    end

    false
  end

  def total_summa
    #рассчитываем сумму доставки
    summa = 0
    total_valid_phones.each do |phone|
      _channel = Channel.channel(@log.sender_name_value, @log.user.alpha_megafon_channel, phone)
      summa = summa + @log.sms_count * Message.price_per_sms(_channel, @log.user, phone)
    end

    summa
  end

  private
  def total_valid_phones
    all_phones = []
    if @log.by_not_delivered?
      all_phones = non_delivered_phones
    else
      all_phones = [valid_phones, client_phones].flatten.uniq.map { |phone| phone.to_i }
    end

    all_phones - BlackList.global.map.map{|b| b.phone } - BlackList.personal(@log.user).map{|b| b.phone }
  end

  def valid_phones
    @log.phones.map { |phone| normalize_phone_number(phone) }.compact.uniq
  end

  def client_phones
    @log.groups.map { |group| group.clients.map { |client| normalize_phone_number(client.phone) } }.compact.uniq
  end

  def non_delivered_phones
    SendLog.not_delivered([@log.prev_log_id]).map { |send_log| normalize_phone_number(send_log.phone).to_i }.uniq
  end

  def errors
    _errors = []

    if not good_balance? and total_valid_phones.count > 0
      _errors << I18n.t('sms.failure.balance_too_low')
    end

    _errors << I18n.t('sms.failure.invalid_sender_name') if not sender_name_valid?
    _errors << I18n.t('sms.failure.no_message') if not content_valid?
    _errors << I18n.t('sms.failure.no_phones') if total_valid_phones.count <= 0
    _errors << I18n.t('sms.failure.invalid_user') if not user_active?

    _errors
  end

  def sender_name_valid?
    SenderName.valid_by_value?(@log.sender_name_value, @log.user)
  end

  def content_valid?
    return true if @log.content.present?
    false
  end

  def good_balance?
    return true if @log.user.admin?

    if @log.user.payment_type.postpaid?
      return true
    else
      return true if @log.user.current_balance > 0 and @log.user.current_balance > total_summa
    end

    false
  end

  def save_valid_phones(valid_phones)
    valid_phones.each do |valid_phone|
      LogValidPhones.create!(log_id: @log.id, valid_phone: valid_phone)
    end
  end

  def user_active?
    return true if @log.reload.user.status.active?

    false
  end

  def save_errors
    @log.check_errors = errors
    @log.save!
  end
end
