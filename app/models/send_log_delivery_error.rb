# -*- encoding : utf-8 -*-
class SendLogDeliveryError < ActiveRecord::Base
  attr_accessible :eng_name, :name, :code, :channel

  extend Service::SendLogDeliveryError::Bronze
  extend Service::SendLogDeliveryError::Gold
  include Service::SendLogDeliveryError::Any

  belongs_to :channel

  def self.reject_error_by_operator(operator)
    if operator.mts?
      return SendLogDeliveryError::Any.reject_to_mts
    end

    if operator.megafon?
      return SendLogDeliveryError::Any.reject_to_megafon
    end

    if operator.smarts?
      return SendLogDeliveryError.gold_reject_to_smarts
    end

    nil
  end

end
