# -*- encoding : utf-8 -*-
class User < ActiveRecord::Base
  include User::Status

  include Service::Users::Billing
  include Service::Users::SenderName
  include Service::Users::Type
  include PhoneHelper
  include Service::Users::Report

  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable

  attr_accessible :email,
                  :password,
                  :password_confirmation,
                  :remember_me,
                  :fio,
                  :description,
                  :demo,
                  :blocked_at,
                  :deleted_at,
                  :type,
                  :status,
                  :charge_type,
                  :payment_type,
                  :permit_night_delivery,
                  :contact_email,
                  :discount,
                  :permit_alpha_sender_name,
                  :reject_to_megafon,
                  :permit_def_sender_name,
                  :birthday_sender_name,
                  :reject_to_smarts_via_gold_channel,
                  :alpha_megafon_channel,
                  :reject_to_mts

  scope :normal, -> { where.not(type: UserType.admin) } #все кроме админа
  scope :admins, -> { where(type: UserType.admin) } #все админы

  scope :active, -> { where(status: UserStatus.active) } #только активные
  scope :deleted, -> { where(status: UserStatus.deleted) } #только удалённые
  scope :blocked, -> { where(status: UserStatus.blocked) } #только заблокированные

  has_many :groups
  has_many :logs
  has_many :user_tariffs
  has_many :payments
  has_many :auto_deliveries
  has_many :sender_names
  has_many :templates
  belongs_to :type, class_name: 'UserType', :foreign_key => :user_type_id
  belongs_to :status, class_name: 'UserStatus'
  belongs_to :alpha_megafon_channel, class_name: 'Channel'

  belongs_to :charge_type
  belongs_to :payment_type

  has_many :black_listed_phones, :class_name => 'BlackList'
  has_many :balances

  validates :email, presence: true, uniqueness: true
  validates :type, :status, :charge_type, :payment_type, :discount,
            :alpha_megafon_channel, presence: true

  #validates_inclusion_of :field_name, :in => [true, false]

  def groups_with_clients
    groups.select{|group| group.clients.size > 0}.sort{|g1, g2| g1.created_at <=> g2.created_at}
  end

  def clients
    groups_with_clients.map{|group| [group.clients]}.flatten
  end

  def client_phones
    clients.map{|client| client.phone}.compact.uniq
  end

  def group_ids
    groups_with_clients.map{|group| group.id}.compact.uniq.sort
  end

end
