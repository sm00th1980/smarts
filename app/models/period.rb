# -*- encoding : utf-8 -*-
class Period

  DEFAULT_HOUR = 10 #10:00 утра по Москве
  DEFAULT_MINUTE = 0

  def self.hours
    _hours = []
    24.times do |hour|
      _hours << {hour: "в #{hour} часов", index: hour}
    end
    _hours
  end

  def self.minutes
    _minutes = []
    [0, 10, 20, 30, 40, 50].each do |minute|
      _minutes << {minute: minute, index: minute}
    end

    _minutes
  end

  def self.weekdays
    _weekdays = []
    WeekDay.days.each do |weekday|
      _weekdays << {weekday: weekday.name, index: weekday.id}
    end

    _weekdays
  end
end
