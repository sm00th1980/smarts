# -*- encoding : utf-8 -*-
class Group < ActiveRecord::Base
  attr_accessible :name
  has_many :clients, :dependent => :delete_all
  belongs_to :user
  include Sidekiq::Worker

  def group_for_select
    ["Группа <#{self.name}> - #{humans(self.clients.size)}", "group_id=#{self.id}"]
  end

  def humans(count)
    _h = Russian.p(count, "человек", "человека", "человек")
    "#{count} #{_h}"
  end

  def set_users_birthday_congratulation_to flag
    self.clients.update_all(birthday_congratulation: flag)
  end

  def self.supported_formats
    ['xls', 'xlsx', 'csv']
  end

  def name
    if super.blank?
      return I18n.t('group.without_name')
    end

    super
  end

  def price_per_sms(sender_name_value)
    return bronze_price_per_sms.to_f.round(2) if SenderName.def?(sender_name_value)
    return gold_price_per_sms.to_f.round(2) if SenderName.alpha?(sender_name_value)

    0
  end

end
