# -*- encoding : utf-8 -*-
class Gateway::Kannel < ActionMailer::Base
  extend GatewayHelper
  extend TimeHelper

  include Sidekiq::Worker

  #async processing
  def async_send(phone, log, start_send, end_send)
    log.reload
    if log.ready_for_processing? and not log.fake?
      Gateway::Kannel.send_by_http(phone, log, start_send, end_send)
    end
  end

  #sync
  def self.send_by_http(phone, log, start_send=false, end_send=false)
    log.reload

    #сохраняем статус рассылки как начата-рассылка
    if start_send
      log.status = LogStatus.processing
      log.started_at = Time.now
      log.save!
    end

    reject_to_operator(log, phone) do
      check_night_time(log.user) do
        _price = log.sms_count * Message.price_per_sms(Channel.channel(log.sender_name_value, log.user.alpha_megafon_channel, phone), log.user, phone)

        check_low_balance(log, _price, phone) do
          real_delivery(log, _price, phone)
        end
      end
    end

    #сохраняем статус рассылки как завершённый
    if end_send
      log.status = LogStatus.completed
      log.finished_at = Time.now
      log.save!
    end
  end

  def self.check_night_time(user, &block)
    if user.permit_night_delivery?
      #производим рассылку
      yield if block_given?
    else
      if not night_time?(Time.now)
        #производим рассылку
        yield if block_given?
      end
    end
  end

  def self.check_low_balance(log, price, phone, &block)
    if log.user.payment_type.postpaid?
      #производим рассылку если postpaid
      yield if block_given?
    else
      if log.user.payment_type.prepaid?
        if log.reload.user.current_balance >= price
          #производим рассылку
          yield if block_given?
        else
          #нет достаточного баланса - не производим рассылку
          create_send_log(log, 0, phone, SendLogDeliveryStatus.low_balance, SendLogDeliveryError::Any.low_balance)
        end
      end
    end
  end

  def self.reject_to_operator(log, phone, &block)
    operator = CellOperatorGroup.get_by_phone(phone)
    channel = Channel.channel(log.sender_name_value, log.user.alpha_megafon_channel, phone)

    if CellOperatorGroup.reject?(log.user, channel, phone)
      #не производим рассылку на данного оператора
      create_send_log(log, 0, phone, SendLogDeliveryStatus.reject_status_by_operator(operator), SendLogDeliveryError.reject_error_by_operator(operator))
    else
      #рассылка на данного оператора разрешена
      yield if block_given?
    end

  end

  def self.create_send_log(log, price, phone, delivery_status, delivery_error)
    _cell_operator_group = CellOperatorGroup.get_by_phone(phone)
    _channel = Channel.channel(log.sender_name_value, log.user.alpha_megafon_channel, phone)

    SendLog.create!(log_id: log.id, price: price, phone: phone, delivery_status: delivery_status, delivery_error: delivery_error, cell_operator_group: _cell_operator_group, channel: _channel)
  end

  def self.real_delivery(log, price, phone)
    #реальная рассылка

    send_log = create_send_log(log, price, phone, SendLogDeliveryStatus.unknown, SendLogDeliveryError::Any.unknown_status)

    response = Typhoeus.get(
        send_log.channel.configuration[:host],
        followlocation: true,
        params: {
            :smsc => send_log.channel.configuration[:smsc],
            :username => send_log.channel.configuration[:user],
            :password => send_log.channel.configuration[:password],
            :charset => 'UTF-8',
            :coding => Message.coding(log.content),
            :to => phone,
            :from => log.sender_name_value,
            :text => log.content,
            :'dlr-mask' => 19, #1: delivery success, 2: delivery failure, 4: message buffered, 8: smsc submit, 16: smsc reject
            :'dlr-url' => send_log.callback_url
        }
    )

    if response.body != success_response
      #обнуляем стоимость сообщения если отправка невозможна
      send_log.delivery_status = SendLogDeliveryStatus.failured
      send_log.delivery_error = SendLogDeliveryError::Any.kannel_failured
      send_log.price = 0

      Admin.delay.notify(:event => I18n.t('admin.kannel.failure'))
    end

    send_log.save!
  end
end

# response = Typhoeus.get(
#     '127.0.0.1:13003/cgi-bin/sendsms',
#     followlocation: true,
#     params: {
#         :smsc       => 'gold_samara',
#         :username   => 'gold_samara',
#         :password   => 's23dw2eI6x4C5f3',
#         :charset    => 'UTF-8',
#         :coding     => 1,
#         :to         => '79379903835',
#         :from       => 'penza-gsm',
#         :text       => "test",
#         :'dlr-mask' => 19,
#         :'dlr-url'  => SendLog.last.callback_url
#     }
# )
