# -*- encoding : utf-8 -*-
class AutoDelivery < ActiveRecord::Base
  include TimeHelper
  include Service::AutoDelivery::Validation
  include Service::AutoDelivery::Activation
  include Service::AutoDelivery::Receivers
  include Service::AutoDelivery::Text

  attr_accessible :user_id,
                  :name,
                  :description,
                  :start_date,
                  :stop_date,
                  :active,
                  :groups,
                  :clients,
                  :period,
                  :hour,
                  :minute,
                  :week_day_id,
                  :template,
                  :content,
                  :sender_name_value

  validates :name, :presence => true
  belongs_to :user
  belongs_to :period, :class_name => 'AutoDeliveryPeriod', :foreign_key => :period_id
  belongs_to :week_day
  belongs_to :template

  def status
    if valid_for_activation?
      if active?
        return I18n.t('autodelivery.in_work')
      else
        return I18n.t('autodelivery.ready_for_activation')
      end
    else
      activation_errors.join(', ')
    end
  end

  def run(manual=false)
    if valid_for_run?(manual)
      #рассылка для указанных групп
      _group_ids = groups.map{|group| group.id}
      SMSService.send_sms(user, text, {group_ids: _group_ids}, sender_name_value, false, LogType.autodelivery)
    end

    nil
  end

  def client_ids
    return super if super

    []
  end

  def group_ids
    return super if super

    []
  end

end
