# -*- encoding : utf-8 -*-
class Tariff < ActiveRecord::Base
  attr_accessible :name, :price_per_sms

  has_many :user_tariffs, dependent: :destroy

  DEFAULT_PRICE_PER_SMS = 0.18 #18 копеек

  def self.default
    #возвращаем самый дорогой тариф
    most_expensive_tariff = order(:price_per_sms).last
    return most_expensive_tariff if most_expensive_tariff

    self.create!(price_per_sms: DEFAULT_PRICE_PER_SMS)
  end

  after_save :recalculate_groups

  private
  def recalculate_groups
    _group_ids = []
    UserTariff.where(tariff_id: self).find_each do |user_tariff|
      _group_ids << user_tariff.user.group_ids
    end

    _group_ids = _group_ids.flatten.uniq

    #запускаем нотификацию для требования запуска перерасчёта
    ActiveSupport::Notifications.instrument('group.recalculate.required', group_ids: _group_ids)
  end

end
