# -*- encoding : utf-8 -*-
require 'csv'

class GroupParser < ActionMailer::Base
  include PhoneHelper
  include GroupHelper
  include Sidekiq::Worker

  UPLOAD_DIR = '/public/uploads'

  def self.create_dir
    #создание директории для загружаемых файлов
    dir = [Rails.root, UPLOAD_DIR].join
    Dir.mkdir(dir) unless File.exists?(dir)
  end

  def perform(filename, group_id)
    file = Rails.root.join('public', 'uploads', filename)

    self.class.create_dir #создаём директорию для загружаемых файлов

    parse_csv(file, group_id) if 'csv' == file_format(filename)
    parse_excel(file, file_format(filename), group_id) if ['xlsx', 'xls'].include? file_format(filename)
  end

  def self.encoding
    #по умолчанию windows-кодировка
    'windows-1251'
  end

  def parse_csv(file, group_id)
    total_rows, loaded_rows, updated_rows = 0, 0, 0

    begin
      CSV.foreach(file, :col_sep => ';', :encoding => self.class.encoding) do |row|

        total_rows = total_rows + 1

        for_only_valid(row) do
          phone, fio, birthday = extract_columns(row)

          if phone
            loaded, updated = load_client(phone, fio, birthday, group_id)
            loaded_rows = loaded_rows + loaded
            updated_rows = updated_rows + updated
          end
        end

      end
    rescue
    end

    return total_rows, loaded_rows, updated_rows
  end

  def parse_excel(file, ext, group_id)
    total_rows, loaded_rows, updated_rows = 0, 0, 0

    xls = Roo::Excelx.new(File.new(file).path, {}, :ignore) if ext == 'xlsx'
    xls = Roo::Excel.new(File.new(file).path, {}, :ignore) if ext == 'xls'

    (0...xls.sheets.size).each do |sheet_index|

      sheet = xls.sheet(sheet_index)

      (1..sheet.last_row).each do |i|
        total_rows = total_rows + 1
        _phone = sheet.row(i)[0].is_a?(Float) ? sheet.row(i)[0].to_i.to_s : sheet.row(i)[0].to_s
        row = [_phone, sheet.row(i)[1], sheet.row(i)[2].to_s] rescue nil

        for_only_valid(row) do
          phone, fio, birthday = extract_columns(row)

          if phone
            loaded, updated = load_client(phone, fio, birthday, group_id)
            loaded_rows = loaded_rows + loaded
            updated_rows = updated_rows + updated
          end
        end
      end
    end

    return total_rows, loaded_rows, updated_rows
  end

  def load_client(phone, fio, birthday, group_id)

    loaded = 0
    updated = 0

    client = Client.where(:phone => phone, :group_id => group_id).first

    if client
      #если такой уже номер есть - просто обновляем его имя-фамилию, день рождения
      client.fio = fio
      client.birthday = birthday
      client.save!

      updated = 1
    else
      Client.create!(:phone => phone, :fio => fio, :group_id => group_id, :birthday => birthday)
      loaded = 1
    end

    [loaded, updated]
  end

  def for_only_valid(row)
    if row and row.size >= 1 and phone_number_valid?(row.first)
      yield(row)
    end
  end

  def extract_columns(row)
    if row.size == 1
      phone = normalize_phone_number(row[0])
      fio = nil
      birthday = nil
    else
      phone = normalize_phone_number(row[0])
      fio = row[1]
      birthday = Date.parse(row[2]) rescue nil
    end

    [phone, fio, birthday]
  end
end
