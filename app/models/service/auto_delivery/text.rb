# -*- encoding : utf-8 -*-
module Service::AutoDelivery::Text
  def text
    return content if content.present?

    if Template.find_by_id_and_user_id(template_id, user_id).present?
      return template.text if template.text.present?
    end

    nil
  end
end
