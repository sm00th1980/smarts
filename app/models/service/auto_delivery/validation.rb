# -*- encoding : utf-8 -*-
module Service::AutoDelivery::Validation
  def valid_period?
    return true if period.every_hour? and valid_minute?(minute)
    return true if period.every_day?  and valid_hour?(hour) and valid_minute?(minute)
    return true if period.every_week? and valid_week_day?(week_day) and valid_hour?(hour) and valid_minute?(minute)

    false
  end

  def valid_for_run?(manual=false)
    if valid_for_activation?
      if active? and not overdued? and not manual and not in_future?
        return true if period.every_hour? and minute == Time.now.min
        return true if period.every_day?  and hour == Time.now.hour and minute == Time.now.min
        return true if period.every_week? and Time.now.send("#{week_day.eng_name}?") and hour == Time.now.hour and minute == Time.now.min
      end

      return true if active? and not overdued? and manual
    end

    false
  end

  def overdued?
    if stop_date and stop_date < Date.today
       return true
    end

    false
  end

  def active?
    return true if super and valid_for_activation?

    false
  end

  def in_future?
    return false if start_date <= Date.today

    true
  end

  def has_content?
    return true if text.present?

    false
  end

  def has_valid_sender_name?
    return true if SenderName.valid_by_value?(sender_name_value, user)

    false
  end

  def user_active?
    return true if user.status.active?

    false
  end
end
