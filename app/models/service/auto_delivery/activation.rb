# -*- encoding : utf-8 -*-
module Service::AutoDelivery::Activation
  include Service::AutoDelivery::Validation

  def valid_for_activation?
    if start_date.present? and name.present? and has_content? and has_receivers? and valid_period? and not overdued? and has_valid_sender_name? and user_active?
      return true
    end

    false
  end

  def activate
    self.active = true if valid_for_activation?
    save!
  end

  def deactivate
    self.active = false
    save!
  end

  def activation_errors
    _errors = []
    _errors << I18n.t('autodelivery.failure.blank_start_date') if start_date.blank?
    _errors << I18n.t('autodelivery.failure.blank_name') if name.blank?
    _errors << I18n.t('autodelivery.failure.blank_content') if not has_content?
    _errors << I18n.t('autodelivery.failure.no_receivers') if not has_receivers?
    _errors << I18n.t('autodelivery.failure.invalid_period') if not valid_period?
    _errors << I18n.t('autodelivery.failure.overdued') if overdued? and start_date.present?
    _errors << I18n.t('autodelivery.failure.invalid_sender_name_value') if not has_valid_sender_name?
    _errors << I18n.t('autodelivery.failure.invalid_user') if not user_active?

    _errors
  end
end
