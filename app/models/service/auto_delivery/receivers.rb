# -*- encoding : utf-8 -*-
module Service::AutoDelivery::Receivers
  def has_receivers?
    return true if client_ids.any? or group_ids.any?

    false
  end

  def receivers_count
    if has_receivers?
      _count = groups.map { |group| group.clients.size }.sum + clients.size
    else
      _count = 0
    end

    _count
  end

  def groups
    _groups = []
    group_ids.each do |group_id|
      _groups << Group.find_by_id_and_user_id(group_id, user_id)
    end

    _groups.compact.uniq
  end

  def groups=(_group_ids)
    _groups = []
    if _group_ids
      _group_ids.each do |group_id|
        _groups << Group.find_by_id_and_user_id(group_id, user_id)
      end

      self.update_attribute(:group_ids, _groups.compact.uniq.map{|group| group.id})
    else
      self.update_attribute(:group_ids, nil)
    end
  end

  def clients
    _clients = []
    client_ids.each do |client_id|
      _clients << Client.where(:group_id => user.groups).find_by_id(client_id)
    end

    _clients.compact.uniq
  end

  def clients=(_client_ids)
    _clients = []
    if _client_ids
      _client_ids.each do |client_id|
        _clients << Client.where(:group_id => user.groups).find_by_id(client_id)
      end

      self.update_attribute(:client_ids, _clients.compact.uniq.map{|client| client.id})
    else
      self.update_attribute(:client_ids, nil)
    end
  end
end
