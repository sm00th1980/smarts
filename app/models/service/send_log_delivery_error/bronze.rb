# -*- encoding : utf-8 -*-
module Service::SendLogDeliveryError::Bronze

  def bronze_error_unknown
    find_or_create_by(eng_name: 'ERROR_UNKNOWN', name: 'СМС центр вернул неизвестный код ошибки', code: '-6', channel: Channel.bronze)
  end

  def bronze_retries_exceeded
    find_or_create_by(eng_name: 'RETRIES_EXCEEDED', name: 'Превышено число попыток отправки. Скорость smpp линка недостаточна(throttling_error_58).', code: '-5', channel: Channel.bronze)
  end

  def bronze_low_balance
    find_or_create_by(eng_name: 'LOW_BALANCE', name: 'Сообщение не отослано. Текущий баланс слишком низкий.', code: '-4', channel: Channel.bronze)
  end

  def bronze_expired
    find_or_create_by(eng_name: 'SMS_TIMEOUT_EXPIRED', name: 'Истёк срок жизни СМС сообщения', code: '-3', channel: Channel.bronze)
  end

  def bronze_kannel_failured
    find_or_create_by(eng_name: 'ERROR_DELIVERY_TO_KANNEL', name: 'Ошибка доставки через сервис kannel', code: '-2', channel: Channel.bronze)
  end

  def bronze_status_unknown
    find_or_create_by(eng_name: 'STATUS_UNKNOWN', name: 'Сообщение отослано. Статус доставки неизвестен. Не пришло уведомление от СМС центра о доставке.', code: '-1', channel: Channel.bronze)
  end

  def bronze_no_error
    find_or_create_by(eng_name: 'SUCCESS_DELIVERY', name: 'Сообщение доставлено без ошибок.', code: '000', channel: Channel.bronze)
  end

  def bronze_error_unknown_subscriber
    find_or_create_by(eng_name: 'ERROR_UNKNOWN_SUBSCRIBER', name: 'Номера адресата нет в HLR. Сообщение не доставлено из-за отсутствия номера или не активной SIM-карты', code: '001', channel: Channel.bronze)
  end

  def bronze_error_unidentified_subscriber
    find_or_create_by(eng_name: 'ERROR_UNIDENTIFIED_SUBSCRIBER', name: 'Номер адресата имеется в HLR, но не зарегистрирован на коммутаторе, на который HLR ссылается.', code: '005', channel: Channel.bronze)
  end

  def bronze_error_absent_subscriber_sm
    find_or_create_by(eng_name: 'ERROR_ABSENT_SUBSCRIBER_SM', name: 'В момент доставки сообщения абонент выпал из сети', code: '006', channel: Channel.bronze)
  end

  def bronze_error_illegal_subscriber
    find_or_create_by(eng_name: 'ERROR_ILLEGAL_SUBSCRIBER', name: 'Используется запрещенная подпись отправителя', code: '009', channel: Channel.bronze)
  end

  def bronze_error_teleservice_not_provisioned
    find_or_create_by(eng_name: 'ERROR_TELESERVICE_NOT_PROVISIONED', name: 'Для адресата не активирована услуга обмена короткими текстовыми сообщениями', code: '011', channel: Channel.bronze)
  end

  def bronze_error_illegal_equipment
    find_or_create_by(eng_name: 'ERROR_ILLEGAL_EQUIPMENT', name: 'Номер адресата недопустим для обмена короткими текстовыми сообщениями (оборудование)', code: '012', channel: Channel.bronze)
  end

  def bronze_error_call_barred
    find_or_create_by(eng_name: 'ERROR_CALL_BARRED', name: 'Абонент заблокирован или поставлена добровольная блокировка', code: '013', channel: Channel.bronze)
  end

  def bronze_error_facility_not_supported
    find_or_create_by(eng_name: 'ERROR_FACILITY_NOT_SUPPORTED', name: 'Аппарат адресата не поддерживает обмен короткими текстовыми сообщениями', code: '021', channel: Channel.bronze)
  end

  def bronze_error_absent_subscriber
    find_or_create_by(eng_name: 'ERROR_ABSENT_SUBSCRIBER', name: 'Адресат не может принять короткое текстовое сообщение, т.к. телефон, на который отправлено SMS, в момент доставки был выключен или находился вне зоны действия сети. Сообщение сохраняется в центре коротких сообщений и доставляется в момент новой регистрации абонента в сети.', code: '027', channel: Channel.bronze)
  end

  def bronze_error_incompatible_terminal
    find_or_create_by(eng_name: 'ERROR_INCOMPATIBLE_TERMINAL', name: 'Аппарат абонента не поддерживает сервис смс', code: '028', channel: Channel.bronze)
  end

  def bronze_error_subscriber_busy_for_mt_sms
    find_or_create_by(eng_name: 'ERROR_SUBSCRIBER_BUSY_FOR_MT_SMS', name: 'В момент доставки адресат был занят приемом или передачей другого короткого сообщения', code: '031', channel: Channel.bronze)
  end

  def bronze_error_sm_delivery_failure
    find_or_create_by(eng_name: 'ERROR_SM_DELIVERY_FAILURE', name: 'Ошибка при доставке например, нарушение протокола передачи в результате сбоев, помех, возможна переполнена память на SIM карте', code: '032', channel: Channel.bronze)
  end

  def bronze_error_message_waiting_list_full
    find_or_create_by(eng_name: 'ERROR_MESSAGE_WAITING_LIST_FULL', name: 'Переполнена память аппарата для приема смс', code: '033', channel: Channel.bronze)
  end

  def bronze_error_system_failure
    find_or_create_by(eng_name: 'ERROR_SYSTEM_FAILURE', name: 'На коммутаторе оператора-получателя смс нет свободных каналов для отправки сообщений. SMS не доставлено из-за системной ошибки (неверный формат номера, номер не абонента стандарта GSM)', code: '034', channel: Channel.bronze)
  end

  def bronze_error_data_missing
    find_or_create_by(eng_name: 'ERROR_DATA_MISSING', name: 'Часть данных в процессе передачи смс была утеряна', code: '035', channel: Channel.bronze)
  end

  def bronze_error_unexpected_data_value
    find_or_create_by(eng_name: 'ERROR_UNEXPECTED_DATA_VALUE', name: 'Неожиданное значение данных (как правило возникает в результате не верно сформированной длины смс)', code: '036', channel: Channel.bronze)
  end

  def bronze_error_resource_limitation
    find_or_create_by(eng_name: 'ERROR_RESOURCE_LIMITATION', name: 'Ошибки USSD-сервиса', code: '051', channel: Channel.bronze)
  end

  def bronze_error_position_method_failure
    find_or_create_by(eng_name: 'ERROR_POSITION_METHOD_FAILURE', name: 'Ошибки USSD-сервиса', code: '054', channel: Channel.bronze)
  end

  def bronze_error_unknown_alphabet
    find_or_create_by(eng_name: 'ERROR_UNKNOWN_ALPHABET', name: 'Ошибки USSD-сервиса', code: '071', channel: Channel.bronze)
  end

  def bronze_error_ussd_busy
    find_or_create_by(eng_name: 'ERROR_USSD_BUSY', name: 'Ошибки USSD-сервиса', code: '072', channel: Channel.bronze)
  end

  def bronze_hlr_timeout
    find_or_create_by(eng_name: 'HLR_TIMEOUT', name: 'Нет ответа от HLR - неправильный номер, некорректная настройка на MSC оператора или роумингового партнера', code: '201', channel: Channel.bronze)
  end

  def bronze_vlr_timeout
    find_or_create_by(eng_name: 'VLR_TIMEOUT', name: 'Нет ответа от VLR - неправильный номер, некорректная настройка на MSC оператора или роумингового партнера, либо абонент покинул зону действия указанного MSC', code: '202', channel: Channel.bronze)
  end

  def bronze_no_server_link_route
    find_or_create_by(eng_name: 'NO_SERVER_LINK_ROUTE', name: 'Нет маршрута для серверного линка', code: '3840', channel: Channel.bronze)
  end

  def bronze_spam_filter_action
    find_or_create_by(eng_name: 'SPAM_FILTER_ACTION', name: 'Действие СПАМ-фильтров комплекса', code: '3841', channel: Channel.bronze)
  end

  def bronze_limit_exceeded
    find_or_create_by(eng_name: 'LIMIT_EXCEEDED', name: 'Превышение ежемесячного лимита смс трафика', code: '3842', channel: Channel.bronze)
  end
end
