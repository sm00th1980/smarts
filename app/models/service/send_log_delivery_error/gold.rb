# -*- encoding : utf-8 -*-
module Service::SendLogDeliveryError::Gold

  def gold_reject_to_smarts
    find_or_create_by(eng_name: 'REJECT_TO_SMARTS_VIA_GOLD_CHANNEL', name: 'Рассылка на Смартс через золотой канал запрещена', code: '-9', channel: Channel.gold)
  end

  def gold_reject_to_megafon
    find_or_create_by(eng_name: 'REJECT_TO_MEGAFON_VIA_GOLD_CHANNEL', name: 'Рассылка на Мегафон через золотой канал запрещена', code: '-8', channel: Channel.gold)
  end

  def gold_error_nack_11
    find_or_create_by(eng_name: 'NACK/11/Invalid Destination Address', name: 'NACK/11/Invalid Destination Address', code: '-7', channel: Channel.gold)
  end

  def gold_error_unknown
    find_or_create_by(eng_name: 'ERROR_UNKNOWN', name: 'СМС центр вернул неизвестный код ошибки', code: '-6', channel: Channel.gold)
  end

  def gold_retries_exceeded
    find_or_create_by(eng_name: 'RETRIES_EXCEEDED', name: 'Превышено число попыток отправки. Скорость smpp линка недостаточна(throttling_error_58).', code: '-5', channel: Channel.gold)
  end

  def gold_low_balance
    find_or_create_by(eng_name: 'LOW_BALANCE', name: 'Сообщение не отослано. Текущий баланс слишком низкий', code: '-4', channel: Channel.gold)
  end

  def gold_expired
    find_or_create_by(eng_name: 'SMS_TIMEOUT_EXPIRED', name: 'Истёк срок жизни СМС сообщения', code: '-3', channel: Channel.gold)
  end

  def gold_kannel_failured
    find_or_create_by(eng_name: 'ERROR_DELIVERY_TO_KANNEL', name: 'Ошибка доставки через сервис kannel', code: '-2', channel: Channel.gold)
  end

  def gold_status_unknown
    find_or_create_by(eng_name: 'STATUS_UNKNOWN', name: 'Сообщение отослано. Статус доставки неизвестен. Не пришло уведомление от СМС центра о доставке.', code: '-1', channel: Channel.gold)
  end

  def gold_no_error
    find_or_create_by(eng_name: 'SUCCESS_DELIVERY', name: 'Сообщение доставлено без ошибок', code: '000', channel: Channel.gold)
  end

  def gold_error_unknown_or_no_delivery_report
    find_or_create_by(eng_name: 'ERROR_UNKNOWN_OR_NO_DELIVERY_REPORT', name: 'В процессе доставки сообщения произошла неизвестная платформе ошибка, либо оператор не предоставил ошибку в отчете о доставке', code: '001', channel: Channel.gold)
  end

  def gold_error_phone_out_of_network
    find_or_create_by(eng_name: 'ERROR_PHONE_OUT_OF_NETWORK', name: 'Абонент находился вне доступа сети на протяжении всего времени попыток доставки SMS сообщения', code: '002', channel: Channel.gold)
  end

  def gold_error_phone_is_blocked_or_roaming
    find_or_create_by(eng_name: 'ERROR_PHONE_IS_BLOCKED_OR_ROAMING', name: 'Абонент заблокирован, либо абонент поставил блокировку на прием SMS, либо абонент находится в роуминге, при том, что у него не подключена услуга приема SMS в роуминге', code: '003', channel: Channel.gold)
  end

  def gold_error_gsm_transport_level_is_corrupted
    find_or_create_by(eng_name: 'ERROR_GSM_TRANSPORT_LEVEL_IS_CORRUPTED', name: 'В процессе доставки SMS-сообщения произошла ошибка на транспортном уровне сигнальной сети', code: '004', channel: Channel.gold)
  end

  def gold_error_phone_out_of_memory
    find_or_create_by(eng_name: 'ERROR_PHONE_OUT_OF_MEMORY', name: 'Память телефона абонента переполнена', code: '005', channel: Channel.gold)
  end

  def gold_error_incompatible_terminal
    find_or_create_by(eng_name: 'ERROR_INCOMPATIBLE_TERMINAL', name: 'У абонента не подключена услуга приема SMS-сообщений', code: '006', channel: Channel.gold)
  end

  def gold_error_gsm_switch_is_not_available
    find_or_create_by(eng_name: 'ERROR_GSM_SWITCH_IS_NOT_AVAILABLE', name: 'Коммутационное оборудование, на котором зарегистрирован абонент, не отвечает', code: '007', channel: Channel.gold)
  end

  def gold_error_phone_number_is_not_valid
    find_or_create_by(eng_name: 'ERROR_PHONE_NUMBER_IS_NOT_VALID', name: 'Некорректный номер абонента, либо телефон абонента был выключен на протяжении очень долгого периода времени', code: '008', channel: Channel.gold)
  end

  def gold_error_message_was_rejected_by_dublicating
    find_or_create_by(eng_name: 'ERROR_MESSAGE_WAS_REJECTED_BY_DUBLICATING', name: 'Сообщение было отброшено платформой, так как сработал механизмом отсечения дубликатов SMS-сообщений', code: '009', channel: Channel.gold)
  end

  def gold_error_message_was_rejected_by_spam_filter
    find_or_create_by(eng_name: 'ERROR_MESSAGE_WAS_REJECTED_BY_SPAM_FILTER', name: 'Сообщение было отброшено платформой, так как сработал один из фильтров SMS-сообщений. Например, сработал спам-фильтр', code: '010', channel: Channel.gold)
  end

  def gold_error_gsm_routing
    find_or_create_by(eng_name: 'ERROR_GSM_ROUTING', name: 'Ошибка маршрутизации в конфигурации платформы', code: '011', channel: Channel.gold)
  end

  def gold_error_invalid_destination_number
    find_or_create_by(eng_name: 'ERROR_INVALID_DESTINATION_NUMBER', name: 'Заблокирован на основании глобального стоп листа.', code: '00b', channel: Channel.gold)
  end

  def gold_no_server_link_route
    find_or_create_by(eng_name: 'NO_SERVER_LINK_ROUTE', name: 'Нет маршрута для серверного линка', code: '3840', channel: Channel.gold)
  end

  def gold_spam_filter_action
    find_or_create_by(eng_name: 'SPAM_FILTER_ACTION', name: 'Действие СПАМ-фильтров комплекса', code: '3841', channel: Channel.gold)
  end

  def gold_limit_exceeded
    find_or_create_by(eng_name: 'LIMIT_EXCEEDED', name: 'Превышение ежемесячного лимита смс трафика', code: '3842', channel: Channel.gold)
  end
end
