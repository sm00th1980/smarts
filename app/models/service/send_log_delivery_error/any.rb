# -*- encoding : utf-8 -*-
require 'channel'

module Service::SendLogDeliveryError::Any

  module Any
    def self.reject_to_mts
      SendLogDeliveryError.find_or_create_by(eng_name: 'REJECT_TO_MTS', name: 'Рассылка на МТС запрещена', code: '-9', channel: Channel.any)
    end

    def self.reject_to_megafon
      SendLogDeliveryError.find_or_create_by(eng_name: 'REJECT_TO_MEGAFON', name: 'Рассылка на Мегафон запрещена', code: '-8', channel: Channel.any)
    end

    def self.nack_11
      SendLogDeliveryError.find_or_create_by(eng_name: 'NACK/11/Invalid Destination Address', name: 'NACK/11/Invalid Destination Address', code: '-7', channel: Channel.any)
    end

    def self.unknown_error
      SendLogDeliveryError.find_or_create_by(eng_name: 'ERROR_UNKNOWN', name: 'СМС центр вернул неизвестный код ошибки', code: '-6', channel: Channel.any)
    end

    def self.retries_exceeded
      SendLogDeliveryError.find_or_create_by(eng_name: 'RETRIES_EXCEEDED', name: 'Превышено число попыток отправки. Скорость smpp линка недостаточна(throttling_error_58).', code: '-5', channel: Channel.any)
    end

    def self.low_balance
      SendLogDeliveryError.find_or_create_by(eng_name: 'LOW_BALANCE', name: 'Сообщение не отослано. Текущий баланс слишком низкий', code: '-4', channel: Channel.any)
    end

    def self.expired
      SendLogDeliveryError.find_or_create_by(eng_name: 'SMS_TIMEOUT_EXPIRED', name: 'Истёк срок жизни СМС сообщения', code: '-3', channel: Channel.any)
    end

    def self.kannel_failured
      SendLogDeliveryError.find_or_create_by(eng_name: 'ERROR_DELIVERY_TO_KANNEL', name: 'Ошибка доставки через сервис kannel', code: '-2', channel: Channel.any)
    end

    def self.unknown_status
      SendLogDeliveryError.find_or_create_by(eng_name: 'STATUS_UNKNOWN', name: 'Сообщение отослано. Статус доставки неизвестен. Не пришло уведомление от СМС центра о доставке.', code: '-1', channel: Channel.any)
    end

    def self.success_delivered
      SendLogDeliveryError.find_or_create_by(eng_name: 'SUCCESS_DELIVERY', name: 'Сообщение доставлено без ошибок.', code: '000', channel: Channel.any)
    end
  end

end
