# -*- encoding : utf-8 -*-
module Service::CellOperatorGroup::Type

  def self.included(base)
    base.extend(ClassMethods)
  end

  module ClassMethods
    def mts
      find_by_internal_name(:mts)
    end

    def beeline
      find_by_internal_name(:beeline)
    end

    def megafon
      find_by_internal_name(:megafon)
    end

    def tele2
      find_by_internal_name(:tele2)
    end

    def baikal
      find_by_internal_name(:baikal)
    end

    def ural
      find_by_internal_name(:ural)
    end

    def enisey
      find_by_internal_name(:enisey)
    end

    def cdma
      find_by_internal_name(:cdma)
    end

    def motiv
      find_by_internal_name(:motiv)
    end

    def nss
      find_by_internal_name(:nss)
    end

    def other
      find_by_internal_name(:other)
    end

    def smarts
      find_by_internal_name(:smarts)
    end

    def working
      [mts, beeline, megafon, tele2, baikal, ural, enisey, cdma, motiv, nss, smarts, other]
    end
  end

  def mts?
    self == self.class.mts
  end

  def beeline?
    self == self.class.beeline
  end

  def megafon?
    self == self.class.megafon
  end

  def tele2?
    self == self.class.tele2
  end

  def baikal?
    self == self.class.baikal
  end

  def ural?
    self == self.class.ural
  end

  def enisey?
    self == self.class.enisey
  end

  def cdma?
    self == self.class.cdma
  end

  def motiv?
    self == self.class.motiv
  end

  def nss?
    self == self.class.nss
  end

  def other?
    self == self.class.other
  end

  def smarts?
    self == self.class.smarts
  end

end
