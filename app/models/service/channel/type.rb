# -*- encoding : utf-8 -*-
module Service::Channel::Type

  def self.included(base)
    base.extend(ClassMethods)
  end

  module ClassMethods
    def gold
      find_or_create_by(name: 'Золотой', internal_name: :gold)
    end

    def bronze
      find_or_create_by(name: 'Бронзовый', internal_name: :bronze)
    end

    def unknown
      find_or_create_by(name: 'Неизвестный', internal_name: :unknown)
    end

    def megafon_fixed
      find_or_create_by(name: 'Мегафон "Фиксированная подпись"', internal_name: :megafon_fixed)
    end

    def megafon_multi
      find_or_create_by(name: 'Мегафон "Многоподпись"', internal_name: :megafon_multi)
    end

    def any
      find_or_create_by(name: 'Любой', internal_name: :any)
    end

    def working
      [gold, bronze, megafon_fixed, megafon_multi]
    end
  end

  def gold?
    self == self.class.gold
  end

  def bronze?
    self == self.class.bronze
  end

  def unknown?
    self == self.class.unknown
  end

  def megafon_fixed?
    self == self.class.megafon_fixed
  end

  def megafon_multi?
    self == self.class.megafon_multi
  end

  def any?
    self == self.class.any
  end
end
