# -*- encoding : utf-8 -*-
require 'channel'
require 'cell_operator_group'

module Service::LogStat::Report

  class Helper
    def self.format(value)
      {
          price: value[:price].to_f.round(2),
          sms_sent: value[:sms_sent].to_i || 0,
          success_sms_sent: value[:success_sms_sent].to_i || 0,
          percent: to_zero_procent(value[:success_sms_sent], value[:sms_sent]),
          price_per_sms: price_per_sms(value[:price], value[:sms_sent])
      }
    end

    def self.to_zero_procent(success, total)
      if total.to_i >= 1
        return (success.to_f/total.to_f * 100).round(0)
      end
      0
    end

    def self.price_per_sms(total_price, total_count)
      return total_price.to_f.round(2) if total_count.to_i == 0
      (total_price.to_f.round(2)/total_count.to_i).round(2)
    end

    def self.stats(user, channel, begin_date, end_date, cell_operator_group=nil)
      log_stat = lambda { LogStat.unscoped.select('
        sum(price) as price,
        sum(sms_sent_success + sms_sent_failure) as sms_sent,
        sum(sms_sent_success) as success_sms_sent
      ').
          where(user_id: user.id).
          where('date between ? and ?', begin_date, end_date).
          where(channel_id: channel.id)
      }

      if cell_operator_group.present?
        log_stat = log_stat.call.where(cell_operator_group_id: cell_operator_group.id)
      else
        log_stat = log_stat.call
      end

      format(log_stat.first)
    end
  end

  #public

  def stats(user, begin_date, end_date)
    _stats = {}
    Channel.working.each do |channel|
      _stats[channel] = Helper.stats(user, channel, begin_date, end_date)
    end

    _stats
  end

  def stats_by_operator(user, begin_date, end_date)
    _stats = {}
    Channel.working.each do |channel|
      _stats[channel] = {}
      CellOperatorGroup.working.each do |operator|
        _stats[channel][operator] = Helper.stats(user, channel, begin_date, end_date, operator)
      end
    end

    _stats
  end

  def users_with_traffic(begin_date, end_date)
    Log.select('user_id').
        where('date(created_at) >= ? and date(created_at) <= ?', begin_date, end_date).
        group('user_id').
        having('sum(sms_sent_success + sms_sent_failure) > 0').
        map { |el| User.find(el[:user_id].to_i) }
  end

end
