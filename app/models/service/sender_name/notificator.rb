# -*- encoding : utf-8 -*-
module Service::SenderName::Notificator

  def self.included(base)
    base.extend(ClassMethods)
  end

  module ClassMethods
    def create_with_notify(user, new_value)
      if SenderName.verified?(new_value, user)
        sender_name = SenderName.create!(value: new_value, user_id: user.id, status_id: SenderNameStatus.moderating.id)
        Admin.delay.notify(:event => I18n.t('sender_name.admin_notify.created') % [user.email, new_value])

        return sender_name
      end
    end
  end

  def update_with_notify(new_value)
    self.update_attribute(:value, new_value)

    if SenderName.verified?(new_value, self.user)
      self.update_attribute(:status, SenderNameStatus.moderating)

      Admin.delay.notify(:event => I18n.t('sender_name.admin_notify.updated') % [self.user.email, new_value])

      return true
    else
      self.update_attribute(:status, SenderNameStatus.rejected)
    end

    false
  end

end
