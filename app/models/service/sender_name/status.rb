# -*- encoding : utf-8 -*-
module Service::SenderName::Status
  def status
    if not SenderName.verified?(value, self.user)
      return SenderNameStatus.rejected
    end

    super
  end

  def accepted?
    status.internal_name.to_s == SenderNameStatus.accepted.internal_name.to_s
  end

  def rejected?
    status.internal_name.to_s == SenderNameStatus.rejected.internal_name.to_s
  end

  def moderating?
    status.internal_name.to_s == SenderNameStatus.moderating.internal_name.to_s
  end
end
