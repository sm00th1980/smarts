# -*- encoding : utf-8 -*-
module Service::SenderName::Activator
  def activate
    if self.accepted?
      deactivate_active(self.user)

      #активируем выбранный sender_name
      self.update_attribute(:active, true)

      return true
    end

    false
  end

  def deactivate_active(user)
    #деактивируем текущий активный sender_name
    SenderName.where(:user_id => user, :active => true).map { |sender_name| sender_name.update_attribute(:active, false) }
  end
end
