# -*- encoding : utf-8 -*-
module Service::SenderName::Moderating

  def accept
    if accepted? or moderating?
      self.update_attribute(:status, SenderNameStatus.accepted)
    else
      self.reject
    end
  end

  def reject
    self.update_attribute(:status, SenderNameStatus.rejected)
  end

end
