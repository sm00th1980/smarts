# -*- encoding : utf-8 -*-
require 'validates_email_format_of'

module Service::Users::Report

  def has_contact_email?
    if contact_email.present? and ValidatesEmailFormatOf::validate_email_format(contact_email, {}).nil?
      return true
    end

    false
  end

end
