# -*- encoding : utf-8 -*-
require 'sender_name.rb'
module Service::Users::SenderName
  def active_sender_name_value
    SenderName.active_sender_name_value(self)
  end

  def accepted_sender_name_values
    sender_names.select { |sender_name| sender_name.accepted? }
  end
end
