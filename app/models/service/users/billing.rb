# -*- encoding : utf-8 -*-
module Service::Users::Billing
  NDS = 0.18

  def current_balance
    (payments_sum - charges_sum).round(2)
  end

  def payments_sum(begin_date=nil, end_date=nil)
    payments = lambda { Payment.select('sum(amount) as amount').approved.where(user_id: self.id) }

    payments = payments.call.where('date(approved_at) between ? and ?', begin_date, end_date) if begin_date.present? and end_date.present?
    payments = payments.call.where('date(approved_at) >= ?', begin_date) if begin_date.present? and end_date.nil?
    payments = payments.call.where('date(approved_at) <= ?', end_date) if begin_date.nil? and end_date.present?
    payments = payments.call if begin_date.nil? and end_date.nil?

    payments[0][:amount].to_f.round(2)
  end

  def charges_sum
    Balance.select("sum(charges) as charges").where(:user_id => self.id)[0][:charges].to_f
  end

  def total_sms_sent(begin_date=nil, end_date=nil)
    sms_sent_success(begin_date, end_date) + sms_sent_failure(begin_date, end_date)
  end

  def sms_sent_success(begin_date=nil, end_date=nil)
    logs = lambda { LogStat.unscoped.select("sum(sms_sent_success) as sms_sent_success").where(user_id: self.id) }

    logs = logs.call.where('date between ? and ?', begin_date, end_date) if begin_date and end_date
    logs = logs.call.where('date >= ?', begin_date) if begin_date and end_date.nil?
    logs = logs.call.where('date <= ?', end_date) if begin_date.nil? and end_date
    logs = logs.call if begin_date.nil? and end_date.nil?

    logs[0][:sms_sent_success].to_i
  end

  def sms_sent_failure(begin_date=nil, end_date=nil)

    logs = lambda { LogStat.unscoped.select("sum(sms_sent_failure) as sms_sent_failure").where(user_id: self.id) }

    logs = logs.call.where('date between ? and ?', begin_date, end_date) if begin_date and end_date
    logs = logs.call.where('date >= ?', begin_date) if begin_date and end_date.nil?
    logs = logs.call.where('date <= ?', end_date) if begin_date.nil? and end_date
    logs = logs.call if begin_date.nil? and end_date.nil?

    logs[0][:sms_sent_failure].to_i
  end

  def price_per_sms
    if discount >= 0 and discount <= 100
      _discount = (tariff.price_per_sms * (discount/100.0)).round(2)
    else
      #скидка не применяется
      _discount = 0
    end

    (tariff.price_per_sms - _discount).round(2)
  end

  def tariff
    #ищем с незакрытой датой
    user_tariff = UserTariff.where(:user_id => self).where('begin_date <= ? AND end_date IS NULL', Time.now.to_date)
    return user_tariff.first.tariff if user_tariff.size == 1

    #ищем с закрытой датой
    user_tariff = UserTariff.where(:user_id => self).where('begin_date <= ? AND ? <= end_date', Time.now.to_date, Time.now.to_date)
    return user_tariff.first.tariff if user_tariff.size == 1

    #не найден тариф вообще -> возвращаем самый дорогой тариф
    Tariff.default
  end

  def charge_by_sent?
    return true if charge_type == ChargeType.by_sent
    false
  end

  def charge_by_delivered?
    return true if charge_type == ChargeType.by_delivered
    false
  end
end
