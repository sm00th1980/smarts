# -*- encoding : utf-8 -*-
module Service::Users::Type
  def admin?
    type == UserType.admin
  end

  def demo?
    type == UserType.demo
  end

  def operator?
    type == UserType.operator
  end

  def business?
    type == UserType.business
  end
end
