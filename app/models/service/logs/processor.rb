# -*- encoding : utf-8 -*-
module Service::Logs::Processor
  include TimeHelper
  include StartTime

  SMSC_CAPACITY = 5 #пропускная способность обработки СМС-сообщений СМС-центром(число сообщений/секунду)

  def process
    if validate
      start_delivery
    end
  end

  private
  def start_delivery
    #обрабатываем все корректные номера телефонов
    chunks = valid_phones.map { |valid_phone| valid_phone.valid_phone }.each_slice(SMSC_CAPACITY).to_a
    process_chunks(chunks)
  end

  def process_chunks(chunks)
    _start_times = start_times(chunks.size, self.user.permit_night_delivery)

    chunks.each_index do |index|
      #при первой отправке стартуем log
      first_chunk = chunks.first == chunks[index] ? true : false

      #в последней отправке финализируем log
      last_chunk = chunks.last == chunks[index] ? true : false

      process_chunk(chunks[index], _start_times[index], first_chunk, last_chunk)
    end
  end

  def process_chunk(phones, process_time, first_chunk, last_chunk)
    phones.each do |phone|
      start_send = (phones.first == phone and first_chunk) ? true : false
      end_send   = (phones.last == phone  and last_chunk)  ? true : false

      if self.type.api?
        Gateway::Kannel.delay(:queue => 'api').async_send(phone, self, start_send, end_send)
      else
        Gateway::Kannel.delay_until(process_time).async_send(phone, self, start_send, end_send)
      end

    end
  end
end
