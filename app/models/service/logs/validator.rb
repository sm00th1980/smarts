# -*- encoding : utf-8 -*-
module Service::Logs::Validator
  def validate
    _valid_result = false

    pre_check do
      log_validator = Aux::LogValidator.new(self)
      _valid_result = log_validator.log_valid?
    end

    post_check(_valid_result)

    _valid_result
  end

  def pre_check(&block)
    if self.fresh? or self.ready_for_processing?
      self.status = LogStatus.checking
      self.save!

      yield if block_given?
    end
  end

  def post_check(valid)
    if valid and self.checking?
      self.status = LogStatus.ready_for_processing
    else
      self.status = LogStatus.check_failed
      self.finished_at = Time.now
    end

    self.save!
  end

  def check_errors
    return [] if super.nil?
    super
  end

  def valid_phones
    return [] if super.nil?
    super
  end
end
