# -*- encoding : utf-8 -*-
module Service::Logs::StartTime
  include TimeHelper

  SMSC_PROCESS_TIME = 3.seconds #время в секундах обработки СМС-центром одного chunk-а
  GUARD_DELAY = 1.seconds #защитный интервал на который задерживаются рассылка - чтобы успели сформироваться все джобы

  def guard_delay(chunks_count)
    (chunks_count/10).ceil + GUARD_DELAY
  end

  def start_times(chunks_count, permit_night_delivery)
    now = Time.now + guard_delay(chunks_count)
    if !permit_night_delivery and night_time?(now)
      _times = [next_day_morning(now)]
    else
      _times = [now]
    end

    (chunks_count - 1).times do
      _times << start_time(_times.last, permit_night_delivery)
    end

    _times
  end

  private
  def start_time(prev_process_time, permit_night_delivery)
    _time = prev_process_time + SMSC_PROCESS_TIME
    if !permit_night_delivery and night_time?(_time)
      return next_day_morning(_time)
    end
    _time
  end

  def next_day_morning(time)
    if before_midnight?(time)
      (time+1.day).change(hour: BEGIN_HOUR, min: BEGIN_MIN, sec: BEGIN_SEC)
    else
      time.change(hour: BEGIN_HOUR, min: BEGIN_MIN, sec: BEGIN_SEC)
    end
  end

  def before_midnight?(time)
    check_night(time) do
      return true if time.hour >= END_HOUR
      false
    end
  end

  def check_night(time, &block)
    if night_time?(time)
      yield
    else
      raise 'Time should be a part of night'
    end
  end

end
