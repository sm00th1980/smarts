# -*- encoding : utf-8 -*-
module Service::Logs::Status
  def ready_for_processing?
    if [LogStatus.processing, LogStatus.ready_for_processing].include? self.reload.status
      return true
    end

    false
  end

  def fresh?
    self.reload.status == LogStatus.fresh
  end

  def checking?
    self.reload.status == LogStatus.checking
  end

  def stopped?
    self.reload.status == LogStatus.stopped
  end

  def processing?
    self.reload.status == LogStatus.processing
  end

  def completed?
    self.reload.status == LogStatus.completed
  end

  def stopable?
    if [LogStatus.fresh, LogStatus.checking, LogStatus.processing, LogStatus.ready_for_processing].include? self.reload.status
      return true
    end

    false
  end

  def stop!
    if self.reload.stopable?
      self.update_attribute(:status, LogStatus.stopped)
      return true
    end

    false
  end

end
