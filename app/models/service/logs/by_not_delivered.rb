# -*- encoding : utf-8 -*-
module Service::Logs::ByNotDelivered
  def by_not_delivered?
    type == LogType.by_not_delivered
  end
end
