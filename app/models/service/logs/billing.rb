# -*- encoding : utf-8 -*-
module Service::Logs::Billing
  def total_sms_sent
    sms_sent_success + sms_sent_failure
  end

  def delivery_speed
    _speed = 0
    _finished_at = finished_at.nil? ? Time.now : finished_at
    if started_at
      _speed = total_sms_sent/(_finished_at - started_at)
    end

    _speed.round(2)
  end
end
