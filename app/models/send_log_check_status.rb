# -*- encoding : utf-8 -*-
class SendLogCheckStatus < ActiveRecord::Base
  attr_accessible :name, :internal_name

  def self.invalid_phone
    find_or_create_by(internal_name: 'invalid_phone')
  end
end
