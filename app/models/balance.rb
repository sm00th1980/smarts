# -*- encoding : utf-8 -*-
class Balance < ActiveRecord::Base
  attr_accessible :period, :user_id, :payments, :charges
  belongs_to :user

  def self.fill(begin_date, end_date)
    (begin_date..end_date).map { |date| date.at_beginning_of_month }.uniq.sort.each do |date|
      #чистим данные
      Balance.where(period: period(date)[:period]).delete_all

      #заполняем данные по начислениям и платежам
      User.all.each do |user|
        payments = user.payments_sum(period(date)[:begin_date], period(date)[:end_date]).to_f
        charges = Log.select("sum(price) as price").where(user_id: user).where('date(created_at) >= ? and date(created_at) <= ?', period(date)[:begin_date], period(date)[:end_date])[0][:price].to_f

        if payments.round(2) > 0 or charges.round(2) > 0
          Balance.create!(period: period(date)[:period], user_id: user.id, payments: payments.round(2), charges: charges.round(2))
        end
      end
    end
  end

  def self.period(date)
    {
        period: date.at_beginning_of_month.strftime('%Y%m').to_i,
        begin_date: date.at_beginning_of_month,
        end_date: date.at_end_of_month
    }
  end

end
