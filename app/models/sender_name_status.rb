# -*- encoding : utf-8 -*-
class SenderNameStatus < ActiveRecord::Base
  has_one :sender_name
  attr_accessible :name, :internal_name

  def self.moderating
    find_or_create_by(name: 'на проверке', internal_name: :moderating)
  end

  def self.accepted
    find_or_create_by(name: 'проверено и одобрено', internal_name: :accepted)
  end

  def self.rejected
    find_or_create_by(name: 'проверено и отклонено', internal_name: :rejected)
  end
end
