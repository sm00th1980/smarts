# -*- encoding : utf-8 -*-
require 'uri'

class Notification::Parser

  def self.sms_id(str)
    URI.decode(str).encode('UTF-8', :invalid => :replace, :undef => :replace).split('&')[0].split('=').last.to_i rescue ''
  end

  def self.type_id(str)
    URI.decode(str).encode('UTF-8', :invalid => :replace, :undef => :replace).split('&')[1].split('=').last.to_i rescue ''
  end

  def self.answer(str)
    URI.decode(str).encode('UTF-8', :invalid => :replace, :undef => :replace).split('&')[2].split('=')[1..-1].join.split('+').join(' ') rescue ''
  end

end
