# -*- encoding : utf-8 -*-
require 'uri'

class Notification::Collector
  include KannelHelper

  DEFAULT_DB = 0
  NOTIFY_DB = 1
  LOCK_FILE_PREFIX = 'tmp/collector_%s.lock'

  def initialize(key)
    @key = key
  end

  def perform
    prepare do
      $redis.llen(@key).times do
        str = $redis.lpop(@key)

        sms_id = Notification::Parser.sms_id(str)
        type_id = Notification::Parser.type_id(str)
        answer = Notification::Parser.answer(str)

        if sms_id and type_id and answer
          update_send_log(sms_id, type_id, answer)
        end
      end
    end
  end

  def lock_file
    LOCK_FILE_PREFIX % @key
  end

  private
  def prepare(&block)
    check_not_running do
      $redis.select(NOTIFY_DB)
      yield if block_given?
      $redis.select(DEFAULT_DB)
    end
  end

  def check_not_running(&block)
    file = File.open(Rails.root.join(lock_file).to_s, File::RDWR|File::CREAT, 0644)
    if not file.flock(File::LOCK_NB|File::LOCK_EX)
      raise I18n.t('collector.once_run_exception') % @key
    else
      yield if block_given?
      file.close
    end
  end

  def find_delivery_status(type_id)
    delivery_status = SendLogDeliveryStatus.find_by(kannel_id: type_id)
    if delivery_status.nil?
      delivery_status = SendLogDeliveryStatus.unknown
    end

    delivery_status
  end

  def update_send_log(sms_id, type_id, answer)
    send_log = SendLog.find_by(id: sms_id)
    delivery_status = find_delivery_status(type_id)

    if send_log and send_log.log
      delivery_error = parse_error_code(answer, send_log.channel)
    else
      delivery_error = SendLogDeliveryError::Any.unknown_status
    end

    if send_log and delivery_status and delivery_error

      send_log.delivery_status = delivery_status
      send_log.delivery_error = delivery_error

      #меняем цену только для бронзового канала
      if send_log.channel.bronze?
        if send_log.log.user.charge_by_delivered?
          if send_log.delivered?
            send_log.price = send_log.log.user.price_per_sms * send_log.log.sms_count
          else
            send_log.price = 0
          end
        end
      end

      send_log.save!
    end
  end

end
