# -*- encoding : utf-8 -*-
class BlackList < ActiveRecord::Base
  attr_accessible :user_id, :phone

  belongs_to :user

  scope :global, -> {where(user_id: User.admins)}
  scope :personal, ->(user) { where(user_id: user) }
end
