# -*- encoding : utf-8 -*-
class UserType < ActiveRecord::Base
  attr_accessible :name, :internal_name
  has_one :user

  validates :name, :internal_name, presence: true

  def self.demo
    find_or_create_by(name: 'Демо', internal_name: 'demo')
  end

  def self.admin
    find_or_create_by(name: 'Админ', internal_name: 'admin')
  end

  def self.operator
    find_or_create_by(name: 'Служебный', internal_name: 'operator')
  end

  def self.business
    find_or_create_by(name: 'Коммерческий', internal_name: 'business')
  end

end
