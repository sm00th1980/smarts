# -*- encoding : utf-8 -*-

class Calculator::Groups < Calculator::Base

  WORKERS = 3

  def self.drop_prices(group_ids)
    # сброс ценников для групп

    Group.where(id: group_ids).update_all(gold_price_per_sms: nil, bronze_price_per_sms: nil)
  end

  # using queue

  def self.recalculate(group_ids=[])
    # перерасчёт ценников для групп -> может быть очень долгим

    if group_ids.empty?
      groups = Group.where('gold_price_per_sms is null or bronze_price_per_sms is null')
    else
      groups = Group.where(id: group_ids)
    end

    groups.find_each do |group|
      _gold_price_per_sms = 0
      _bronze_price_per_sms = 0

      group.clients.find_each do |client|
        _gold_price_per_sms += Message.calc_price_per_sms(Channel.gold, group.user, client.phone).round(2)
        _bronze_price_per_sms += Message.calc_price_per_sms(Channel.bronze, group.user, client.phone).round(2)
      end

      group.gold_price_per_sms = _gold_price_per_sms
      group.bronze_price_per_sms = _bronze_price_per_sms

      group.save!
    end
  end

  # OVERRIDE SECTION

  def self.queue
    'groups_recalculate'
  end

  def self.message(group_ids)
    {group_ids: group_ids}.to_json
  end

  def self.produce_recalculate
    group_ids_chunks.each { |chunk| rabbitmq_publish(queue, message(chunk)) }
  end

  def self.group_ids_chunks
    group_ids = Group.where('gold_price_per_sms is null or bronze_price_per_sms is null').pluck(:id).sort
    if group_ids.count < WORKERS
      [group_ids]
    else
      group_ids.each_slice((group_ids.size/WORKERS.to_f).round).to_a
    end
  end

end
