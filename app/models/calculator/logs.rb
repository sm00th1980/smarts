# -*- encoding : utf-8 -*-

class Calculator::Logs < Calculator::Base

  # using queue

  def self.recalculate(begin_date, end_date)
    #пересчитываем Logs
    Log.where('date(created_at) >= ? and date(created_at) <= ?', begin_date, end_date).each do |log|
      _log = SendLog.select(
          sql = %Q{
          sum(price) as price,
          count(case when delivery_status_id = #{SendLogDeliveryStatus.delivered.id} THEN id END) as sms_sent_success,
          count(case when delivery_status_id <>  #{SendLogDeliveryStatus.delivered.id} THEN id END) as sms_sent_failure
          }
      ).where(log_id: log.id)[0]

      log.price = _log.price.to_f.round(2)
      log.sms_sent_success = _log.sms_sent_success.to_i * log.sms_count
      log.sms_sent_failure = _log.sms_sent_failure.to_i * log.sms_count

      log.save!
    end

    #пересчитываем LogStat
    LogStat.calculate(begin_date, end_date)

    #пересчитываем балансы
    Balance.fill(begin_date, end_date)
  end

  # OVERRIDE SECTION

  def self.queue
    'logs_recalculate'
  end

  def self.message
    begin_date = Date.today.to_s
    end_date = Date.today.to_s

    {begin_date: begin_date, end_date: end_date}.to_json
  end

end
