# -*- encoding : utf-8 -*-
require "bunny"
require "rabbitmq/http/client"

class Calculator::Base

  def self.rabbitmq_publish(queue_name, message)
    connection = Bunny.new(hostname: Rails.configuration.rabbitmq[:hostname], port: Rails.configuration.rabbitmq[:port], user: Rails.configuration.rabbitmq[:login], pass: Rails.configuration.rabbitmq[:password])
    connection.start

    channel = connection.create_channel
    queue = channel.queue(queue_name, :durable => true)

    if non_ack_message_count(queue_name) == 0 #запускаем только в случае отсутствия работающих воркеров над данной очередью
      queue.publish(message)
    end

    connection.close
  end

  def self.produce_recalculate
    # здесь мы только генерируем сообщения на перерасчёт групп
    rabbitmq_publish(queue, message)
  end

  def self.queue
    raise "should override #{__method__}"
  end

  def self.message
    raise "should override #{__method__}"
  end

  def self.non_ack_message_count(queue_name)
    endpoint = "http://%s:%s" % [Rails.configuration.rabbitmq[:hostname], Rails.configuration.rabbitmq[:api_port]]
    client = RabbitMQ::HTTP::Client.new(endpoint, username: Rails.configuration.rabbitmq[:login], password: Rails.configuration.rabbitmq[:password])
    client.list_queues.select{|el| el["name"] == queue_name}.first["messages_unacknowledged"].to_i rescue 0
  end
end
