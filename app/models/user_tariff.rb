# -*- encoding : utf-8 -*-
class UserTariff < ActiveRecord::Base
  belongs_to :user
  belongs_to :tariff

  after_save :recalculate_groups
  after_destroy :recalculate_groups

  private
  def recalculate_groups
    ActiveSupport::Notifications.instrument('group.recalculate.required', group_ids: user.group_ids.flatten.uniq)
  end
end
