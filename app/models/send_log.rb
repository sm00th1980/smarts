# -*- encoding : utf-8 -*-
class SendLog < ActiveRecord::Base

  attr_accessible :log_id,
                  :phone,
                  :check_statuses,
                  :delivery_status,
                  :price,
                  :delivery_error,
                  :cell_operator_group,
                  :channel

  belongs_to :log
  belongs_to :delivery_status, :class_name => 'SendLogDeliveryStatus'
  belongs_to :delivery_error, :class_name => 'SendLogDeliveryError'
  belongs_to :cell_operator_group
  belongs_to :channel

  scope :delivered, ->(log_ids) { where(:log_id => log_ids, :delivery_status_id => SendLogDeliveryStatus.delivered.id) }
  scope :not_delivered, ->(log_ids) { where(:log_id => log_ids).where("delivery_status_id <> ?", SendLogDeliveryStatus.delivered.id) }

  def delivered?
    delivery_status == SendLogDeliveryStatus.delivered
  end

  def callback_url
    'http://%s?smsid=%s&type=%%d&answer=%%A' % [channel.configuration[:callback_url], id]
  end
end
