# -*- encoding : utf-8 -*-
class CellOperatorPhoneRange < ActiveRecord::Base
  attr_accessible :begin_phone, :end_phone, :region, :cell_operator

  belongs_to :cell_operator
end
