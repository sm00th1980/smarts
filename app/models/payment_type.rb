# -*- encoding : utf-8 -*-
class PaymentType < ActiveRecord::Base
  attr_accessible :name, :internal_name
  has_one :user

  def self.prepaid
    find_or_create_by(name: 'Предоплата(дебет)', internal_name: :prepaid)
  end

  def self.postpaid
    find_or_create_by(name: 'Постоплата(кредит)', internal_name: :postpaid)
  end

  def prepaid?
    self == self.class.prepaid
  end

  def postpaid?
    self == self.class.postpaid
  end
end
